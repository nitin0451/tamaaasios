
//
//  AppDelegate.swift
//  Tamaas
//
//  Created by Krescent Global on 12/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import IQKeyboardManager
import FBSDKCoreKit
import UserNotifications
import Alamofire
import FTIndicator
import YYWebImage
import CoreData
import Firebase
import Branch
import Fabric
import LinkedinSwift
import QuickbloxWebRTC
import PushKit
import FirebaseMessaging
import Intents
import CallKit
import Stripe

struct TimeIntervalConstant {
    static let answerTimeInterval: TimeInterval = 60.0
    static let dialingTimeInterval: TimeInterval = 5.0
}

struct AppDelegateConstant {
    static let enableStatsReports: UInt = 1
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , NotificationServiceDelegate , UNUserNotificationCenterDelegate  {
    
    //Variables
    var deviceTokenString = String()
    let defaults = UserDefaults.standard
    var window: UIWindow?
    var updatedPushToken: NSData?
    var currentVC = UIViewController()
    var callObserver: CXCallObserver!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
         application.applicationIconBadgeNumber = 0
        // change the language
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        Stripe.setDefaultPublishableKey("pk_live_CDx9Su8u2sumqubhU4BGv4kE002zerbKWa")
//pk_test_SeCHueq1QOzniEUyBn91Ude300SgOBy3ae
    //pk_live_CDx9Su8u2sumqubhU4BGv4kE002zerbKWa 
        //init
        constantVC.GlobalVariables.isAppLaunchedForImgCaching = true
        self.configureQuickBlox()
        callObserver = CXCallObserver()
        callObserver.setDelegate(self, queue: nil)
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        constantVC.GlobalVariables.isNeedToShowInitialConnectingBar = true
        AppType.isProduction = true
        UIApplication.shared.statusBarView?.backgroundColor = .white
        constantVC.GlobalVariables.isNeedToGetStories = true
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        UserDefaults.standard.set(false , forKey: "isNeedToRefreshMessageList")
     
        self.viewToLoad()
        
        //Update cache for updated profiles
        ProfileUpdateManager.shared.webserviceToGetProfileStatus()
     
        //Branch
        DispatchQueue.global(qos: .background).async {
        Branch.setUseTestBranchKey(true)
        Branch.getInstance().setDebug()
        let branch: Branch = Branch.getInstance()
        branch.initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
            if error == nil {
                SessionManager.printLOG(data: "\(String(describing: params))")
                guard let data = params as? [String: AnyObject] else { return }
                
                var senderPhNo:String = ""
                var stickerSection:String = ""
                
                guard let senderPhNo_ = data["$linkSendFrom"] as? String else {
                    return
                }
                guard let stickerSection_ = data["$stickerSection"] as? String else {
                    return
                }
                self.Webservice_updateSenderUnlockStickerStatus(phnNo: senderPhNo , section: stickerSection)
         
          }
        })
        }
        
        //firebase
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
        
        //fabric crashlytics
        Fabric.with([Crashlytics.self])
        
        //register push notification
        self.registerPushNotifications()
     
     //TODO
    
   
        return ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func registerPushNotifications() {
        
        if #available(iOS 10.0, *) {
        let app = UIApplication.shared
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.sound, .alert, .badge], completionHandler: { granted, error in
            if let error = error {
                debugPrint("\(String(describing: error.localizedDescription))")
                return
            }
            center.getNotificationSettings(completionHandler: { settings in
                if settings.authorizationStatus == .authorized {
                    DispatchQueue.main.async(execute: {
                        app.registerForRemoteNotifications()
                    })
                }
            })
        })
        }
        else {
            let userNotificationTypes: UIUserNotificationType = [.alert, .badge, .sound]
            let settings = UIUserNotificationSettings(types: userNotificationTypes, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    //MARK:- BRANCH
    func application(_ app: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        Branch.getInstance().continue(userActivity)
        return true
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        Branch.getInstance().application(app, open: url, options: options)
        if LinkedinSwiftHelper.shouldHandle(url) {
            return LinkedinSwiftHelper.application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        }
        
        if Auth.auth().canHandle(url) {
            return true
        }
        
        return ApplicationDelegate.shared.application(app, open: url, options: options)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        NotificationCenter.default.post(name: Notification.Name.UIApplicationWillResignActive, object: nil)
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0;
     
     if constantVC.ActiveSession.isCallActive == false {
          do {
               try AVAudioSession.sharedInstance().setActive(false, with: .notifyOthersOnDeactivation)
          } catch {
               print("error")
          }
     }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        ReachabilityManager.shared.stopMonitoring()
//            if QBChat.instance.isConnected == true {
//                return
//            }
//
//            connect { (error) in
//                  if let error = error {
//                      SVProgressHUD.showError(withStatus: error.localizedDescription)
//                      return
//                  }
//                  SVProgressHUD.showSuccess(withStatus: "Connected")
//            }
        // Logging in to chat.
        ServicesManager.instance().chatService.connect { (error) in
            if error == nil {
                print("CHAT CONNECTED")
            } else {
                print("CHAT NOT CONNECTED")
            }
        }
    }
    
    //MARK: - Connect/Disconnect
      func connect(completion: QBChatCompletionBlock? = nil) {
          let currentUser = Profile()

          if QBChat.instance.isConnected == true {
              completion?(nil)
          } else {
              QBSettings.autoReconnectEnabled = true
              QBChat.instance.connect(withUserID:currentUser.userData?.id ?? 0, password: currentUser.userData?.password ?? "", completion: completion)
            
        }
      }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        NotificationCenter.default.post(name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
        //network reachability manager
        ReachabilityManager.shared.startMonitoring()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        NotificationCenter.default.removeObserver(self, name: constantVC.NSNotification_name.NSNotification_profileUpdate, object: nil)
        NotificationCenter.default.removeObserver(self, name: constantVC.NSNotification_name.NSNotification_postLikeComment, object: nil)
        NotificationCenter.default.removeObserver(self, name: constantVC.NSNotification_name.NSNotification_bellUnreadCount , object: nil)
        // Logging out from chat.
        ServicesManager.instance().chatService.disconnect(completionBlock: nil)
    }
    
    
    //MARK:- PUSH NOTIFICATION
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        SessionManager.printLOG(data: "Failed to get token, error: \(error)")
        constantVC.GlobalVariables.updatedPushToken = nil
    }
   
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Auth.auth().setAPNSToken( deviceToken , type: .prod)
        constantVC.GlobalVariables.updatedPushToken = deviceToken
        Core.instance.registerForRemoteNotifications(withDeviceToken: deviceToken)
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let deviceTokenString_ = tokenParts.joined()
        WebserviceSigleton().callToAddFCMToken(token: deviceTokenString_)
        
        // Convert token to string
       /* let deviceTokenString_ = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        SessionManager.printLOG(data: "Received token data! \(deviceTokenString_)")
        
        self.deviceTokenString = deviceTokenString_
        constantVC.GlobalVariables.updatedPushToken = deviceToken
        
        //quickblox
        let deviceIdentifier: String = UIDevice.current.identifierForVendor!.uuidString
        
        if SessionManager.get_notificationActiveStatus() {
            let subscription = QBMSubscription()
            subscription.notificationChannel = .APNS
            subscription.deviceUDID = deviceIdentifier
            subscription.deviceToken = deviceToken
            QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse!, objects: [QBMSubscription]?) -> Void in
                
            }) { (response: QBResponse!) -> Void in
                
            }
        }*/
    }
   
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
    }
    
    func application(_ application: UIApplication, willContinueUserActivityWithType userActivityType: String) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification notification: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("wakrnajjijijij")
        
        if Auth.auth().canHandleNotification(notification) {
            completionHandler(.noData)
            return
        }
        
        if let flag = notification["flag"] as? String {
             if flag == "profile" {
                
                //Update cache for updated profiles
                ProfileUpdateManager.shared.webserviceToGetProfileStatus()
            }
        }
     
        
        // This notification is not auth related, developer should handle it.
    }
    
    // For iOS 8-
    func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?,
                     annotation: Any) -> Bool {
        if Auth.auth().canHandle(url) {
            return true
        }
        
        return true
        // URL not auth related, developer should handle it.
    }
    
    //MARK:- WEB SERVICES
    func Webservice_updateSenderUnlockStickerStatus(phnNo:String , section: String)
    {
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["phoneNumber": phnNo, "section": section , "UUID": UIDevice.current.identifierForVendor!.uuidString]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_UPDATE_USER_UNLOCK_STICKERS, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                SessionManager.printLOG(data: "Sender unlock stickers status updated!")
            }
        })
    }
    
    
    func webservice_getUlockStickers_status(){
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["phoneNumber":SessionManager.getPhone()]
        SessionManager.save_isUnlockStickers_sports(isHaving: false)
        SessionManager.save_isUnlockStickers_famous(isHaving: false)
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_USER_UNLOCK_STICKERS, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    if let userDetails = response["data"] as? [[String:Any]] {
                        for item in userDetails {
                            if let stickerSection = item["sticker"] as? String{
                                SessionManager.printLOG(data: stickerSection)
                                if stickerSection == "sports" {
                                    SessionManager.save_isUnlockStickers_sports(isHaving: true)
                                } else {
                                    SessionManager.save_isUnlockStickers_famous(isHaving: true)
                                }
                            }
                        }
                    }
                }
                else {
                    SessionManager.printLOG(data: "ERROR:- error while get unlock stickers")
                    SessionManager.save_isUnlockStickers_sports(isHaving: false)
                    SessionManager.save_isUnlockStickers_famous(isHaving: false)
                }
            }
        })
    }
    
    func webserviceToGetUser(phoneNo: String){
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["phoneNumber":phoneNo]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_USER_PHONE, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    if let userDetails = response["user_details"] as? [[String:Any]]{
                        for item in userDetails {
                            if let userName = item["name"]{
                                SessionManager.setUsername(userName: userName as! String)
                            }
                        }
                    }
                }
            }
        })
    }
    
    
    //MARK:- CUSTOM METHODS
    func viewToLoad(){
       
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
         if(defaults.value(forKey:constantVC.GeneralConstants.UserID) == nil){
            SessionManager.save_notificationActiveStatus(isHaving: true)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "InitViewController")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        else if (defaults.value(forKey:constantVC.GeneralConstants.Identity) != nil){
            
            constantVC.GlobalVariables.isAppLauched = true
            let phoneno = self.defaults.value(forKey:constantVC.GeneralConstants.Identity) as! String
            self.webserviceToGetUser(phoneNo: SessionManager.getPhone())
            
            DispatchQueue.global(qos: .background).async {
             //MessagingManager.sharedManager().presentRootViewController()
                let update_NS = Update_Get_allLinked_Contacts()
                update_NS.getContacts()
                ContactManager().getAddressBookAndUpdate { (success) in
                }
            }
           
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "tabBar")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        else{
            SessionManager.save_notificationActiveStatus(isHaving: true)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "RegisterPhoneVC")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        self.webservice_getUlockStickers_status()
    }
    
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "TamaaasDataModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK:- UNUserNotificationCenterDelegate
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
       if let userInfo:NSDictionary = notification.request.content.userInfo as? NSDictionary {
        
        if let aps = userInfo["aps"] as? NSDictionary {
            //for new user notification
            
            if let flag = aps.object(forKey: "flag") as? String {
                
                //new user
                if flag == "newUser" {
                    completionHandler([.alert, .badge, .sound])
                }
            }
        }
        
            //for unread count
            NotificationCenter.default.post(name: constantVC.NSNotification_name.NSNotification_bellUnreadCount , object: nil)
        
            if let flag = userInfo["flag"] as? String {
                if flag == "profile" {
                    if let fromNo = userInfo["FromNumber"] as? String {
                        //for profile update
                        NotificationCenter.default.post(name: constantVC.NSNotification_name.NSNotification_profileUpdate , object: fromNo)
                    }
                }
                //post
            } else {
                //chat
                guard let dialogID = userInfo["dialog_id"] as? String else {
                    return
                }
                
                guard !dialogID.isEmpty else {
                    return
                }
                
                let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
                if dialogWithIDWasEntered == dialogID {
                    return
                }
            }
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    
    
    // For handling tap and user actions
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        
        print(response.notification.request.content.userInfo)
        
        if let userInfo:NSDictionary = response.notification.request.content.userInfo as? NSDictionary {
            
            if let aps = userInfo["aps"] as? NSDictionary {
                //for new user notification
                
                if let flag = aps.object(forKey: "flag") as? String {
                    
                    //new user
                    if flag == "newUser" {
                        if let user_name = aps["user_name"] as? String , let userID = aps["quickbloxID"] as? String {
                            self.ConnectChatToNewUser(userID: userID , phoneNo: user_name)
                            return
                        }
                    }
                }
            }

            if let post_id = userInfo["post_id"] as? String {
                if let vc = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController {
                    vc.selectedIndex = 1
                    if let selectedVc = vc.selectedViewController?.childViewControllers[0] as? HomeViewController {
                        selectedVc.moveToPostDetail(postId: post_id)
                    }
                }
               
                //open particular post
//                let postVC = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "PostDetail_VC") as! PostDetail_VC
//                postVC.post_id = post_id
//                constantVC.ActiveDataSource.isNavigatePostDetails = true
//                self.window?.rootViewController = postVC
//                self.window?.makeKeyAndVisible()
            }
            
            //profile notification
            if let flag = userInfo["flag"] as? String {
                
               /* if flag == "profile" {
                    let notiVC = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "NavNotification_VC") as! UINavigationController
                    constantVC.ActiveDataSource.isNavigateNotification = true
                    self.window?.rootViewController = notiVC
                    self.window?.makeKeyAndVisible()
                }*/
               
            }
          
            guard let dialogID = userInfo["dialog_id"] as? String else {
                return
            }
            
            guard !dialogID.isEmpty else {
                return
            }
            
            let dialogWithIDWasEntered: String? = ServicesManager.instance().currentDialogID
            if dialogWithIDWasEntered == dialogID {
                return
            }
            
            ServicesManager.instance().notificationService.pushDialogID = dialogID
                
            // calling dispatch async for push notification handling to have priority in main queue
            DispatchQueue.main.async {
                ServicesManager.instance().notificationService.handlePushNotificationWithDelegate(delegate: self)
            }
        }
        completionHandler()
    }
    
    
    func ConnectChatToNewUser(userID: String , phoneNo:String){
        
        ServicesManager.instance().chatService.connect { (error) in
            if error == nil && userID != "" {
                ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: UInt(userID) ?? 0, phoneNo + "-" + SessionManager.getPhone(), isDoctor: false, fees: "") { (response, chatDialog) in
                    if response.isSuccess {
                        
                        if let dialog_ = chatDialog {
                            QuickBloxManager().sendSystemMsg_updateDialog_toOccupants(dialog: dialog_)
                            self.moveToChatController(chatDialog: dialog_)
                        }
                    }
                    else {
                        print(response.error.debugDescription)
                    }
                }
            } else {
                print(error.debugDescription)
            }
        }
        
    }
}


//extension UIApplication {
//    var statusBarView: UIView? {
//        if responds(to: Selector("statusBar")) {
//            return value(forKey: "statusBar") as? UIView
//        }
//        return nil
//    }
//}

//Nitin
extension UIApplication {
var statusBarView: UIView? {
    if #available(iOS 13.0, *) {
        let tag = 38482
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if let statusBar = keyWindow?.viewWithTag(tag) {
            return statusBar
        } else {
            guard let statusBarFrame = keyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
            let statusBarUIView = UIView(frame: statusBarFrame)
            statusBarUIView.tag = tag
            keyWindow?.addSubview(statusBarUIView)
            return statusBarUIView
        }
    } else if responds(to: Selector(("statusBar"))) {
        return value(forKey: "statusBar") as? UIView
    } else {
        return nil
    }
  }
}


extension UIApplication
{
    class func shared() -> UIApplication
    {
        return UIApplication.shared
    }
}

//MARK:- Custom Methods
extension AppDelegate {
    
    func configureQuickBlox() {

        // QuickbloxWebRTC settings
      //  Fabric.with([Crashlytics.self])
        QBSettings.applicationID = QuickBloxAccount.kQBApplicationID
        QBSettings.authKey = QuickBloxAccount.kQBAuthKey
        QBSettings.authSecret = QuickBloxAccount.kQBAuthSecret
        QBSettings.accountKey = QuickBloxAccount.kQBAccountKey
        
        //endpoints
        QBSettings.apiEndpoint = QuickBloxAccount.kQBApiEndPoint
        QBSettings.chatEndpoint = QuickBloxAccount.kQBchatEndPoint
        
        //server
        let urls = ["stun:turn.quickblox.com",
                    "turn:turn.quickblox.com:3478?transport=udp",
                    "turn:turn.quickblox.com:3478?transport=tcp"]
        
        if let server = QBRTCICEServer.init(urls: urls,
                                            username: "quickblox",
                                            password: "baccb97ba2d92d71e26eb9886da5f1e0") {
            QBRTCConfig.setICEServers([server])
        }
        
       
        
        QBSettings.carbonsEnabled = true
        QBSettings.autoReconnectEnabled = true
        QBSettings.logLevel = QBLogLevel.debug
        QBSettings.enableXMPPLogging()
        QBSettings.keepAliveInterval = 10.0
        QBRTCConfig.setAnswerTimeInterval(TimeIntervalConstant.answerTimeInterval)
        QBRTCConfig.setDialingTimeInterval(TimeIntervalConstant.dialingTimeInterval)
        QBRTCConfig.setLogLevel(QBRTCLogLevel.verbose)
        
        if AppDelegateConstant.enableStatsReports == 1 {
            QBRTCConfig.setStatsReportTimeInterval(1.0)
        }
        
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
        QBRTCClient.initializeRTC()
        
        // loading settings
        Settings.instance.load()
        
        //login
        QuickBloxManager().loginAndGetDialogs()
    }
}

// MARK: NotificationServiceDelegate protocol
extension AppDelegate {
    
    func notificationServiceDidStartLoadingDialogFromServer() {
    }
    
    func notificationServiceDidFinishLoadingDialogFromServer() {
    }
    
    func notificationServiceDidSucceedFetchingDialog(chatDialog: QBChatDialog!) {
        self.moveToChatController(chatDialog: chatDialog)
        /*let dialogWithIDWasEntered = ServicesManager.instance().currentDialogID
        if !dialogWithIDWasEntered.isEmpty {
            // some chat already opened, return to dialogs view controller first
            navigatonController.popViewController(animated: false);
        }
        
        navigatonController.pushViewController(chatController, animated: true)*/
    }
    
    func notificationServiceDidFailFetchingDialog() {
    }
    
    func moveToChatController(chatDialog: QBChatDialog){
        
        if let rootViewController = window?.rootViewController as? TabBarController {
            if let navigationVc = rootViewController.viewControllers?[0]as? UINavigationController{
                if let messageVc = navigationVc.viewControllers[0] as? MessageViewController {
                    messageVc.moveToChatVcfromNavigation(dialogue: chatDialog)
                }
            }
        } else {
            let chatController: ChatViewController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                   chatController.dialog = chatDialog
                   chatController.isUnreadCountAvailable = true
                   constantVC.openPrivateChat.isActiveOpenPrivateChat = true
                   
                   let nav: UINavigationController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "navChat") as! UINavigationController
                   nav.viewControllers = [chatController]
                   self.window?.rootViewController = nav
                   self.window?.makeKeyAndVisible()
                   self.window?.endEditing(true)
        }
    }
}



extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}


extension AppDelegate: CXCallObserverDelegate {
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        if call.hasEnded == true {
            print("Disconnected")
        }
        if call.isOutgoing == true && call.hasConnected == false {
            print("Dialing")
        }
        if call.isOutgoing == false && call.hasConnected == false && call.hasEnded == false {
            print("Incoming")
        }
        
        if call.hasConnected == true && call.hasEnded == false {
            print("Connected")
           // CallActiveInBackgroundManager.shared.invalidateCallTimer()
           // constantVC.ActiveSession.session?.hangUp(["hangup": "hang up"])
        }
    }
}
