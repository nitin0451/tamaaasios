//
//  ContactManager.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 11/29/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import Contacts
import FTIndicator
import PhoneNumberKit

class ContactManager: NSObject {
    
    //MARK:- Variables
    var contactsStore: CNContactStore?
    let phoneNumberKit = PhoneNumberKit()
    
   
    func getAddressBookAndUpdate(completionHandler: @escaping (Bool) -> Void) {
        
        if contactsStore == nil {
            //ContactStore is control for accessing the Contacts
            contactsStore = CNContactStore()
        }
        let error = NSError(domain: "EPContactPickerErrorDomain", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Contacts Access"])
        
        switch CNContactStore.authorizationStatus(for: CNEntityType.contacts) {
        case CNAuthorizationStatus.denied, CNAuthorizationStatus.restricted:
            //User has denied the current app to access the contacts.
          FTIndicator.showError(withMessage: "Unable to access contacts. Tamaaas does not have access to contacts. Kindly enable it in privacy settings")
            
        case CNAuthorizationStatus.notDetermined:
            //This case means the user is prompted for the first time for allowing contacts
            contactsStore?.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) -> Void in
                //At this point an alert is provided to the user to provide access to contacts. This will get invoked if a user responds to the alert
                if  (!granted ){
                    FTIndicator.showError(withMessage: "Unable to access contacts. Tamaaas does not have access to contacts. Kindly enable it in privacy settings")
                } else {
                    self.getAddressBookAndUpdate(completionHandler: { (success) in
                        if success {
                            completionHandler(true)
                        } else {
                            completionHandler(false)
                        }
                    })
                }
            })
            
        case  CNAuthorizationStatus.authorized:
            let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
            var name   = ""
            var phone  = ""
            
            do {
                
                // Get all the containers
                var allContainers: [CNContainer] = []
                do {
                    allContainers = (try contactsStore?.containers(matching: nil))!
                } catch {
                    print("Error fetching containers")
                }
                
                var results: [CNContact] = []
                
                // Iterate all containers and append their contacts to our results array
                for container in allContainers {
                    let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                    
                    do {
                        let containerResults = try contactsStore?.unifiedContacts(matching: fetchPredicate, keysToFetch: allowedContactKeys())
                        results.append(contentsOf: containerResults!)
                    } catch {
                        print("Error fetching results for container")
                    }
                }
                
                var addressBookQuick: NSMutableOrderedSet? = []
                constantVC.GlobalVariables.dictMyContacts = [:]
                
                for contact in results {
                    if contact.phoneNumbers.count > 0{
                        
                        for number in contact.phoneNumbers {
                            var str = number.value.stringValue
                           // str = str.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                            phone = str
                            
                            if let username = contact.givenName as? String{
                                name = username
                            }
                            
                            //parse and format for country code
                            do {
                                let parsedPhNo = try phoneNumberKit.parse(phone , withRegion: SessionManager.getCountryCode() , ignoreType: true)
                                if let formatPhNo = phoneNumberKit.format(parsedPhNo, toType: .e164) as? String{
                                    phone = formatPhNo
                                }
                            } catch {
                                print(error.localizedDescription)
                            }
                            
                            phone = phone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                           
                            //update address book to quick blox
                            var contactQuick = QBAddressBookContact()
                            contactQuick.name = name
                            contactQuick.phone = phone
                            addressBookQuick?.add(contactQuick)
                            
                            constantVC.GlobalVariables.dictMyContacts![phone] = name
                        }
                    }
                }
                
                SessionManager.save_dictMyContacts(dict: constantVC.GlobalVariables.dictMyContacts)
                QuickBloxManager().updateAddressBook(addressBookQuick: addressBookQuick! , completionHandler: { (success) in
                    if success {
                        completionHandler(true)
                    } else {
                        completionHandler(false)
                    }
                })
                
            }
            catch let error as NSError {
                completionHandler(false)
            }
        }
    }
    
    func allowedContactKeys() -> [CNKeyDescriptor]{
        
        return [CNContactNamePrefixKey as CNKeyDescriptor,
                CNContactGivenNameKey as CNKeyDescriptor,
                CNContactImageDataKey as CNKeyDescriptor,
                CNContactThumbnailImageDataKey as CNKeyDescriptor,
                CNContactImageDataAvailableKey as CNKeyDescriptor,
                CNContactPhoneNumbersKey as CNKeyDescriptor]
    }

}
