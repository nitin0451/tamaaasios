//
//  ChatMedia_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 3/5/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import AVKit

class ChatMedia_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var collectionViewmedia: UICollectionView!
    
    //MARK:- Variables
    var dialog: QBChatDialog!
    var mediaMsgs:[QBChatMessage] = []
    var videoPlayer: AVPlayer?
    
    
    //MARK:- UIView life-cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //init
        self.initView()
        self.configureNavigationBar()
        self.getMediaMessages()
        
    }
    
    //MARK:- Custom Methods
    func initView(){
        self.collectionViewmedia.delegate = self
        self.collectionViewmedia.dataSource = self
        self.collectionViewmedia.isHidden = true
    }
    
    func configureNavigationBar(){
        self.title = "Media".localized
        let backImg : UIImage? = UIImage.init(named: "blackBack")!.withRenderingMode(.alwaysOriginal)
        let backBtn   = UIBarButtonItem(image: backImg,  style: .plain, target: self, action: #selector(didPressBack(sender:)))
        navigationItem.leftBarButtonItem = backBtn
    }
    
    //MARK:- Media
    func getDialogId() ->String{
        if let id = self.dialog.id {
            return id
        }
        return ""
    }
    
    
     func getMediaMessages(){
        
        let extendedRequest = ["sort_desc" : "date_sent" , "attachments.type" : "image"]
        ServicesManager.instance().chatService.messages(withChatDialogID: self.getDialogId() , extendedRequest: extendedRequest).continueOnSuccessWith { (task) -> Any? in
            if let msgs = task.result {
                self.mediaMsgs.append(contentsOf: msgs as! [QBChatMessage])
                self.getVideoMessages()
            }
            return nil
        }
    }
    
    func getVideoMessages(){
        let extendedRequest = ["sort_desc" : "date_sent" , "attachments.type" : "video"]
        
        ServicesManager.instance().chatService.messages(withChatDialogID: self.getDialogId() , extendedRequest: extendedRequest).continueOnSuccessWith { (task) -> Any? in
            if let msgs = task.result {
                self.mediaMsgs.append(contentsOf: msgs as! [QBChatMessage])
                self.sortMessages()
            }
            return nil
        }
    }
    
    func sortMessages(){
        
        self.mediaMsgs = self.mediaMsgs.sorted(by: {
            $0.dateSent!.compare($1.dateSent!) == .orderedDescending
        })
        self.reloadMediaCollection()
    }
    
    func reloadMediaCollection(){
        self.collectionViewmedia.isHidden = false
        self.collectionViewmedia.reloadData()
    }
    
    //MARK:- UIButton Actions
    @objc func didPressBack(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didPressPlayVideo(_ button: UIButton) {
        let indexPath = IndexPath(row: button.tag , section: 0)
        if let attachment = self.mediaMsgs[indexPath.row].attachments?.first {
            self.playVideo(attachment: attachment , message: self.mediaMsgs[indexPath.row])
        }
    }
 
}

extension ChatMedia_VC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let Width = collectionView.bounds.width/3.0
        let Height = Width
        
        return CGSize(width: Width, height: Height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mediaMsgs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chatInfoCell", for: indexPath) as? chatInfo_collectionViewCell
        
        cell?.btnVideoPlay.tag = indexPath.row
        cell?.btnVideoPlay.addTarget(self , action: #selector(self.didPressPlayVideo(_:)), for: .touchDown)
        
        if let msg_ = self.mediaMsgs[indexPath.row] as? QBChatMessage {
            cell?.msg = msg_
        }
        
        if let attachment = self.mediaMsgs[indexPath.row].attachments?.first {
            cell?.attachment = attachment
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let attachment = self.mediaMsgs[indexPath.row].attachments?.first {
            if attachment.attachmentType == QMAttachmentType.contentTypeImage {
                let fileUrl = attachment.remoteURL(withToken: false)
                QMImagePreview.previewImage(with: fileUrl, in: self)
            }
            if attachment.attachmentType == QMAttachmentType.contentTypeVideo {
                self.playVideo(attachment: attachment , message: self.mediaMsgs[indexPath.row])
            }
            
        }
    }
    
    func playVideo(attachment: QBChatAttachment , message: QBChatMessage){
        
        var url = attachment.remoteURL(withToken: false)
        
        var playerItem = AVPlayerItem(url: attachment.remoteURL())
        
        if videoPlayer != nil {
            videoPlayer!.replaceCurrentItem(with: nil)
            videoPlayer!.replaceCurrentItem(with: playerItem)
        } else {
            videoPlayer = AVPlayer(playerItem: playerItem)
        }
        
        var playerVC = AVPlayerViewController()
        playerVC.player = videoPlayer
        
        self.present(playerVC , animated: true) {
            
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            } catch {
                print(error)
            }
            
            self.videoPlayer!.play()
        }
    }
}
