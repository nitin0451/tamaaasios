//
//  FirebaseOTP.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 3/13/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import Firebase
import FTIndicator
import PhoneNumberKit

class FirebaseOTP: NSObject {
    
    static let shared = FirebaseOTP()
    let phoneNumberKit = PhoneNumberKit()
    
    
    func sendVerificationOTP(phoneno:String , completionHandler:@escaping (Bool) -> ()){
        var phone = phoneno
        
        do {
            let parsedPhNo = try phoneNumberKit.parse(phoneno , withRegion: SessionManager.getCountryCode() , ignoreType: true)
            if let formatPhNo = phoneNumberKit.format(parsedPhNo, toType: .international) as? String{
                phone = formatPhNo
            }
        } catch {
            print(error.localizedDescription)
        }
        
        PhoneAuthProvider.provider().verifyPhoneNumber( phone , uiDelegate: nil) { (verificationID , error) in
            if let error = error {
                FTIndicator.showToastMessage(error.localizedDescription)
                completionHandler(false)
                return
            }
            
            if let id = verificationID {
                SessionManager.saveOTPVerificationID(code: id)
            }
            completionHandler(true)
        }
    }
    
    
    
    func verfiyOTP(verificationCode:String , completionHandler:@escaping (Bool) -> ()){
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: SessionManager.getOTPVerificationID() ,verificationCode: verificationCode)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                completionHandler(false)
                return
            }
           completionHandler(true)
        }
    }
    

}
