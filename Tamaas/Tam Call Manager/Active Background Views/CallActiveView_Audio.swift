//
//  CallActiveView_Audio.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 6/7/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class CallActiveView_Audio: UIView {
    
    //MARK:- Outlets
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblCallName: UILabel!
    
    

    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CallActiveView_Audio", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    
    @IBAction func didPress_completeView(_ sender: UIButton) {
        self.OpenVideoCallVC()
    }
    
    func OpenVideoCallVC(){
        
        //remove background active view
        CallActiveInBackgroundManager.shared.hideActiveAudioCallView()
        
        if let topController = UIApplication.topViewController() {
            if let callViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TamAudioCall_VC") as? TamAudioCall_VC {
                callViewController.modalPresentationStyle = .fullScreen
                callViewController.session = constantVC.ActiveSession.session
                callViewController.callUUID = constantVC.ActiveSession.activeCallUUID
                callViewController.opponentNo = constantVC.ActiveCall.receiverNo
                callViewController.userPhoto = constantVC.ActiveCall.receiverNo
                topController.present(callViewController , animated: true , completion: nil)
            }
        }
    }
    
}
