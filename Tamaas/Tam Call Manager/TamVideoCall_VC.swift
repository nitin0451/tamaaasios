//
//  TamVideoCall_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 12/26/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import Quickblox
import QuickbloxWebRTC
import SVProgressHUD
import FTIndicator
import CallKit


enum videoTrackState: Int {
    case LocalView = 1
    case otherVw = 2
}

class TamVideoCall_VC: UIViewController  {
    
    //MARK: - Internal Properties
   // private var timeDuration: TimeInterval = 0.0
    //private var callTimer: Timer?
    private var beepTimer: Timer?
    
    @IBOutlet weak var micBtn: UIButton!
    //Managers
    private let core = Core.instance
    private let settings = Settings.instance
    
    //Camera
    var session: QBRTCSession?
    var callUUID: UUID?
   // private var cameraCapture: QBRTCCameraCapture?
    
    //Containers
   // private var users = [User]()
  //  private var videoViews = [UInt: UIView]()
    
    //Views
    private var localVideoView: LocalVideoView?
  
    //States
    private var shouldGetStats = false
    private var didStartPlayAndRecord = false
    
    static var muteAudio = false
    private var muteVideo = false {
        didSet {
            session?.localMediaStream.videoTrack.isEnabled = !muteVideo
        }
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    private var state = CallViewControllerState.connected {
        didSet {
            switch state {
            case .disconnected:
                title = CallStateConstant.disconnected
            case .connecting:
                title = CallStateConstant.connecting
            case .connected:
                title = CallStateConstant.connected
            case .disconnecting:
                title = CallStateConstant.disconnecting
            }
        }
    }
   
    //MARK:- Outlets
    @IBOutlet weak var localVideo_view: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var vwTimer: UIView!
    @IBOutlet weak var occupantVideo_view: UIView!
    @IBOutlet weak var OutgoingCall_view: UIView!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var vwGreenDot: UIView!
    @IBOutlet weak var OpponentCollectionVw: UICollectionView!
    
    
    //MARK:- Variables
    //var videoCapture: QBRTCCameraCapture!
    var opponentNo:String = ""
    var isGroup:String = ""
    var callBy:String = ""
    var isUserBlocked:Bool = false
    var blockTimer = Timer()
    var blockSeconds = 60
    let cameraController = CameraController()
    var currentUser:QBUUser?
    var dialogID:String = ""
   
    
    //MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if !self.isUserBlocked {
            QBRTCClient.instance().add(self as QBRTCClientDelegate)
            QBRTCAudioSession.instance().addDelegate(self)
        }
        
    }
    
    deinit {
        debugPrint("deinit \(self)")
    }
    
    func showActiveCallView(){
        let currentConferenceUser = User(user: currentUser!)
        
        let audioSession = QBRTCAudioSession.instance()
        if audioSession.isInitialized == false {
            audioSession.initialize { configuration in
                // adding blutetooth support
                configuration.categoryOptions.insert(AVAudioSessionCategoryOptions.allowBluetooth)
                configuration.categoryOptions.insert(AVAudioSessionCategoryOptions.allowBluetoothA2DP)
                // adding airplay support
                configuration.categoryOptions.insert(AVAudioSessionCategoryOptions.allowAirPlay)
                debugPrint("\(configuration.categoryOptions)")
                guard let session = self.session else { return }
                if session.conferenceType == QBRTCConferenceType.video {
                    // setting mode to video chat to enable airplay audio and speaker only
                    configuration.mode = AVAudioSessionModeVideoChat
                }
            }
        }
        
        //update GUI
        state = .connecting
        
        
        guard let session = self.session else { return }
        if session.conferenceType == QBRTCConferenceType.video {
            #if (arch(x86_64) || arch(i386))
            // Simulator
            #else
            // Device
            constantVC.ActiveSession.cameraCapture = QBRTCCameraCapture(videoFormat: settings.videoFormat,
                                               position: settings.preferredCameraPostion)
            constantVC.ActiveSession.cameraCapture?.startSession(nil)
            session.localMediaStream.videoTrack.videoCapture = constantVC.ActiveSession.cameraCapture
            #endif
        }
        
        
        if self.isGroup == "true" {
            self.occupantVideo_view.isHidden = true
        } else {
            self.OpponentCollectionVw.isHidden = true
        }
        
        
        if let userID = SessionManager.get_QuickBloxID() {
            
            constantVC.ActiveCall.senderID = "\(userID)"
            let isInitiator = userID.uintValue == session.initiatorID.uintValue
            if isInitiator == true {
                startCall()
                
                self.setCallInfo(isRemoveCalling: false)
                
                //add local preview
                if session.conferenceType != .audio {
                    self.setLocalView(superView: self.localVideo_view)
                }
                
            } else {
                
                acceptCall()
                self.setCallInfo(isRemoveCalling: true)
                
                if self.isGroup == "true" {
                    //add local preview -- group
                    if session.conferenceType != .audio {
                        self.setLocalView(superView: self.localVideo_view)
                    }
                } else {
                    //add local preview
                    if session.conferenceType != .audio {
                        self.setLocalView(superView: self.occupantVideo_view)
                    }
                }
            }
        }
        
        title = CallStateConstant.connecting
        
        if CallKitManager.isCallKitAvailable() == true,
            session.initiatorID.uintValue == currentUser?.id {
            CallKitManager.instance.updateCall(with: callUUID, connectingAt: Date())
        }
        
        //init
        self.vwGreenDot.layer.cornerRadius = self.vwGreenDot.frame.size.width / 2
        self.OpponentCollectionVw.delegate = self
        self.OpponentCollectionVw.dataSource = self
       // self.configureTapGestures()
        self.vwGreenDot.isHidden = true
        
        //set caller data
        constantVC.ActiveCall.senderNo = SessionManager.getPhone()
        constantVC.ActiveCall.receiverNo = self.opponentNo
        constantVC.ActiveCall.isGroup = self.isGroup
        constantVC.ActiveCall.isAudio = "false"
        constantVC.ActiveCall.groupName = self.opponentNo
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true , animated: false)
        self.currentUser = ServicesManager.instance().currentUser
        self.automaticallyAdjustsScrollViewInsets = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_updateCallTimer), name: constantVC.NSNotification_name.NSNotification_updateTimer , object: nil)
        
        if !constantVC.ActiveSession.isBackgroundViewActive {
            //when user blocked
            if !self.isUserBlocked {
                self.showActiveCallView()
            } else {
                self.showBlockedView()
            }
        } else {
            
            if constantVC.ActiveSession.isCallActive {
                self.setCallInfo(isRemoveCalling: true)
                if constantVC.ActiveCall.isGroup != "true" {
                    self.setLocalView(superView: self.occupantVideo_view)
                    self.opponentView(superView: self.localVideo_view)
                } else {
                    self.setLocalView(superView: self.localVideo_view)
                    self.reloadContent()
                }
            } else {
                self.setCallInfo(isRemoveCalling: false)
                self.setLocalView(superView: self.localVideo_view)
            }
        }
        session?.localMediaStream.audioTrack.isEnabled = !TamVideoCall_VC.muteAudio
        
        if !(TamVideoCall_VC.muteAudio) {
            micBtn.setImage(UIImage(named: "muteunsel"), for: .normal)
        } else {
            micBtn.setImage(UIImage(named: "mutesel"), for: .normal)
        }
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationDidEnterBackground , object: nil)
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: Notification.Name.UIApplicationWillEnterForeground , object: nil)
    }
    
    
    @objc func handleNotification_updateCallTimer(_ notificationObj: NSNotification) {
        self.lblDuration.text = String(describing: CommonFunctions.shared.string(withTimeDuration: constantVC.ActiveSession.timeDuration))
    }
    
    @objc func appMovedToBackground() {
       // self.closeCall(withTimeout: false)
        
        let myViews = self.localVideo_view.subviews.filter{$0 is QBRTCRemoteVideoView}
        
        if let remoteVw = myViews[0] as? QBRTCRemoteVideoView {
            print(remoteVw.frame)
            remoteVw .removeFromSuperview()
        }
    }
    
    @objc func appMovedToForeground() {
        //self.ReActiveCall(withTimeout: false)
    }
    
    
    func configureCameraController() {
        
        cameraController.prepare {(error) in
            if let error = error {
                print(error)
            }
            try? self.cameraController.displayPreview(on: self.localVideo_view)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    @objc func updateBlockTimer() {
        self.blockSeconds -= 1
        if self.blockSeconds < 0 {
            
            if let beepTimer = beepTimer {
                beepTimer.invalidate()
                self.beepTimer = nil
                SoundProvider.stopSound()
            }
            self.dismiss(animated: true , completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !constantVC.ActiveSession.isBackgroundViewActive && !self.isUserBlocked {
            if constantVC.ActiveSession.cameraCapture?.hasStarted == false {
                session?.localMediaStream.videoTrack.videoCapture = constantVC.ActiveSession.cameraCapture
            }
            session?.localMediaStream.videoTrack.videoCapture = constantVC.ActiveSession.cameraCapture
            reloadContent()
        }
          UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
        
    }
   
    
    func setLocalView(superView: UIView){
        let cameraCapture = constantVC.ActiveSession.cameraCapture
        let localView = LocalVideoView(withPreviewLayer: cameraCapture!.previewLayer)
        localView.frame = superView.bounds
        superView.insertSubview(localView , at: 0)
       // localView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //superView.autoresizesSubviews = true
        localView.translatesAutoresizingMaskIntoConstraints = false
        localView.leftAnchor.constraint(equalTo: superView.leftAnchor).isActive = true
        localView.rightAnchor.constraint(equalTo: superView.rightAnchor).isActive = true
        localView.topAnchor.constraint(equalTo: superView.topAnchor).isActive = true
        localView.bottomAnchor.constraint(equalTo: superView.bottomAnchor).isActive = true
        localView.layoutIfNeeded()
    }
    
    
    func opponentView(superView: UIView){
        if constantVC.ActiveSession.remoteActiveUsers.count > 0 {
            if let user = constantVC.ActiveSession.remoteActiveUsers[0] as? User {
                if let opponentVw = userView(userID: user.userID) as? UIView {
                    
                    DispatchQueue.main.async {
                        opponentVw.frame = self.view.frame
                        superView.addSubview(opponentVw)
                    }
                }
            }
        }
    }
    
    //MARK:- User Blocked Views
    func showBlockedView(){
        //blocked
        self.vwGreenDot.isHidden = true
        
        if self.isGroup == "true" {
            self.lblUserName.text = "Calling " + opponentNo
        } else {
            self.lblUserName.text = "Calling " + SessionManager.getNameFromMyAddressBook(number: opponentNo)
        }
        
        //block camera session
        self.configureCameraController()
        
        //Begin play calling sound
        beepTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(),
                                         target: self,
                                         selector: #selector(playCallingSound(_:)),
                                         userInfo: nil, repeats: true)
        blockTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateBlockTimer)), userInfo: nil, repeats: true)
    }
    
    // MARK: - Actions
    func startCall() {
        //Begin play calling sound
        beepTimer = Timer.scheduledTimer(timeInterval: QBRTCConfig.dialingTimeInterval(),
                                         target: self,
                                         selector: #selector(playCallingSound(_:)),
                                         userInfo: nil, repeats: true)
        
        //Start call
        let userInfo = ["name": "Test", "callBy": callBy , "isGroup": isGroup , "dialogId": self.dialogID]
        session?.startCall(userInfo)
        
        //set local view
        guard let currentUser = core.currentUser else {
            return
        }
       
      //  self.localVideo_view.addSubview(userView(userID: currentUser.id)!)
    }
    
    func acceptCall() {
        SoundProvider.stopSound()
        //Accept call
        let userInfo = ["acceptCall": "userInfo"]
        session?.acceptCall(userInfo)
    }
    
    // MARK: - Timers actions
    @objc func playCallingSound(_ sender: Any?) {
        SoundProvider.playSound(type: .ringtone)
    }
    
    //MARK: - Internal Methods
   func closeCall(withTimeout timeout: Bool) {
    
    let user = constantVC.ActiveSession.remoteActiveUsers[0]
    
    if let view = self.localVideo_view.subviews[0] as? QBRTCRemoteVideoView {
        print(view)
        view.removeFromSuperview()
    }
    
    
//    if let remoteVideoTraсk = session?.remoteVideoTrack(withUserID: NSNumber(value: user.userID)) {
//        remoteVideoTraсk.isEnabled = false
//    }
    
    constantVC.ActiveSession.cameraCapture?.captureSession.stopRunning()
    constantVC.ActiveSession.cameraCapture?.stopSession({
        print("camera capture stopped!!")
    })
     state = .disconnected
     QBRTCClient.instance().remove(self as QBRTCClientDelegate)
    print("interruption", constantVC.ActiveSession.cameraCapture?.isRunning)
    
}
    
    func ReActiveCall(withTimeout timeout: Bool) {
        
        QBRTCClient.instance().add(self)
        constantVC.ActiveSession.cameraCapture?.startSession({
            print("camera capture started!!")
        })
    }
    
    private func userView(userID: UInt) -> UIView? {
        
        if let result = constantVC.ActiveSession.videoViews[userID] {
            return result
        }
        
        if let remoteVideoTraсk = session?.remoteVideoTrack(withUserID: NSNumber(value: userID)) {
                    
            QBRTCRemoteVideoView.preferMetal = false
            
            if self.isGroup == "true" {
                if userID == core.currentUser?.id {
                    let remoteView :QBRTCRemoteVideoView = QBRTCRemoteVideoView()
                    remoteView.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
                    remoteView.clipsToBounds = true
                    remoteView.setVideoTrack(remoteVideoTraсk)
                    return remoteView
                } else {
                    let remoteVideoView = QBRTCRemoteVideoView(frame: CGRect(x: 0, y: 0, width: 110 , height: 140))
                    remoteVideoView.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
                    constantVC.ActiveSession.videoViews[userID] = remoteVideoView
                    remoteVideoView.setVideoTrack(remoteVideoTraсk)
                    return remoteVideoView
                }
            } else {
                let remoteView :QBRTCRemoteVideoView = QBRTCRemoteVideoView()
                remoteView.videoGravity = AVLayerVideoGravity.resizeAspect.rawValue
                remoteView.clipsToBounds = true
                remoteView.setVideoTrack(remoteVideoTraсk)
                return remoteView
            }
            
            //Opponents
           /* let remoteVideoView = QBRTCRemoteVideoView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width , height: self.view.frame.height))
            remoteVideoView.videoGravity = AVLayerVideoGravity.resizeAspectFill.rawValue
            videoViews[userID] = remoteVideoView
            remoteVideoView.setVideoTrack(remoteVideoTraсk)*/
        }
        return nil
    }
    
    private func userCell(userID: UInt) -> UICollectionViewCell? {
        let indexPath = userIndexPath(userID: userID)
        guard let cell = OpponentCollectionVw.cellForItem(at: indexPath) as? UICollectionViewCell  else {
            return nil
        }
        return cell
    }
    
    private func createConferenceUser(userID: UInt) -> User {
        let user = QBUUser()
        user.id = userID
        user.fullName = ""
        
        ServicesManager.instance().usersService.getUserWithID( userID, forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
            
            if let no = task.result?.phone {
                user.fullName = SessionManager.getNameFromMyAddressBook(number:  no)
                return User(user: user)
            }
            return nil
        })
        
        return User(user: user)
    }
    
    private func userIndexPath(userID: UInt) -> IndexPath {
        guard let index = constantVC.ActiveSession.remoteActiveUsers.index(where: { $0.userID == userID }), index != NSNotFound else {
            return IndexPath(row: 0, section: 0)
        }
        return IndexPath(row: index, section: 0)
    }
    
    func reloadContent() {
        if isGroup == "true" {
            
            //videoViews.values.forEach{ $0.removeFromSuperview() }
            //videoViews.removeAll()
            OpponentCollectionVw.reloadData()
            
            if constantVC.ActiveSession.isCallActive {
                if constantVC.ActiveSession.remoteActiveUsers.count < 1 {
                    CallActiveInBackgroundManager.shared.invalidateCallTimer()
                    self.session?.hangUp(["hangup": "hang up"])
                }
            }
            
//            if let userID = SessionManager.get_QuickBloxID() {
//                let isInitiator = userID.uintValue == self.session?.initiatorID.uintValue
//                if isInitiator == true && constantVC.ActiveSession.isCallActive {
//                    if users.count < 1 {
//                        self.callTimer?.invalidate()
//                        self.session?.hangUp(["hangup": "hang up"])
//                    }
//                }
//            }
        }
    }
   
    func configureTapGestures(){
        let tap_localVideo = UITapGestureRecognizer(target: self, action: #selector(self.handleTap_localVideo(_:)))
        self.localVideo_view.tag = videoTrackState.LocalView.rawValue
        self.localVideo_view.addGestureRecognizer(tap_localVideo)
        self.localVideo_view.isUserInteractionEnabled = true
        
        let tap_occupantView = UITapGestureRecognizer(target: self, action: #selector(self.handleTap_remoteVideo(_:)))
        self.occupantVideo_view.addGestureRecognizer(tap_occupantView)
        self.occupantVideo_view.isUserInteractionEnabled = true
    }
    
    
    func setView_call(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            view.isHidden = hidden
        })
    }
    
    func showLocalView(superView: UIView){
        /*if let layer = constantVC.ActiveDataSource.ActiveVideoCapture?.previewLayer {
            let localView = localVideoView() LocalVideoView(previewlayer: layer)    //LocalVideoView(withPreviewLayer: layer)
            localView.frame = superView.bounds
            superView.addSubview(localView)
        }*/
    }
   
    
    func interchangeViews(){
        if self.localVideo_view.tag == videoTrackState.LocalView.rawValue {
            //interchange local view with occupant
            self.showLocalView(superView: self.occupantVideo_view)
            self.localVideo_view.tag = videoTrackState.otherVw.rawValue
        } else {
            //interchange occupant view with local
            self.showLocalView(superView: self.localVideo_view)
            self.localVideo_view.tag = videoTrackState.LocalView.rawValue
        }
    }
    
    
    @objc func handleTap_localVideo(_ sender: UITapGestureRecognizer) {
        
    }
    
    @objc func handleTap_remoteVideo(_ sender: UITapGestureRecognizer) {
        
        
    }
    
    func setCallInfo(isRemoveCalling:Bool){
        
        if isRemoveCalling {
            if self.isGroup == "true" {
                self.lblUserName.text = self.opponentNo
            } else {
                self.lblUserName.text = SessionManager.getNameFromMyAddressBook(number: opponentNo)
            }
        }
        else {
            if self.isGroup == "true" {
                self.lblUserName.text = "Calling " + opponentNo
            } else {
                self.lblUserName.text = "Calling " + SessionManager.getNameFromMyAddressBook(number: opponentNo)
            }
        }
    }
    
    func openChatWindow(){
        constantVC.openPrivateChat.isActiveOpenPrivateChat = true
        constantVC.openPrivateChat.isActiveOpenChatWithActiveCall = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        if constantVC.ActiveSession.activeDialog != nil {
            controller.dialog = constantVC.ActiveSession.activeDialog
            
            if let topVC = UIApplication.topViewController() {
                topVC.present(controller , animated: false , completion: {
                    CallActiveInBackgroundManager.shared.showActiveVideoCallView()
                })
            }
        } else {
            
            //get dialog from server
        ServicesManager.instance().chatService.loadDialog(withID: constantVC.ActiveCall.dialogID).continueOnSuccessWith { (task) -> Any? in
                if task.isCompleted{
                    if let dialog = task.result {
                        controller.dialog = dialog
                        constantVC.ActiveSession.activeDialog = dialog
                        
                        DispatchQueue.main.async {
                            if let topVC = UIApplication.topViewController() {
                                topVC.present(controller , animated: false , completion: {
                                    CallActiveInBackgroundManager.shared.showActiveVideoCallView()
                                })
                            }
                        }
                    }
                } else {
                    FTIndicator.showToastMessage("Error in open Chat Window!")
                }
                return nil
            }
        }
    }
    
    //MARK:- Outgoing Call Actions
    @IBAction func didPress_openChatWindow(_ sender: UIButton) {
        
        self.dismiss(animated: true , completion: {
            if CallActiveInBackgroundManager.shared.isActiveChatWindow() {
                CallActiveInBackgroundManager.shared.showActiveVideoCallView()
            } else {
                self.openChatWindow()
            }
        })
    }
    
    @IBAction func didPress_back(_ sender: UIButton) {
        self.dismiss(animated: true , completion: {
            CallActiveInBackgroundManager.shared.showActiveVideoCallView()
        })
    }
    
    @IBAction func Outgoing_endCall(_ sender: UIButton) {
        
        if !self.isUserBlocked {
            CallActiveInBackgroundManager.shared.invalidateCallTimer()
            self.session?.hangUp(["hangup": "hang up"])
        } else {
            self.blockSeconds = 0
            self.updateBlockTimer()
        }
    }
    
    
    @IBAction func Outgoing_Mic(_ sender: UIButton) {
        
        if let muteAudio = TamVideoCall_VC.muteAudio as? Bool {
            TamVideoCall_VC.muteAudio = !muteAudio
        }
        
        if !(TamVideoCall_VC.muteAudio) {
            sender.setImage(UIImage(named: "muteunsel"), for: .normal)
        } else {
            sender.setImage(UIImage(named: "mutesel"), for: .normal)
        }
    }
    
    @IBAction func Outgoing_flipCamera(_ sender: UIButton) {
        
        if self.isUserBlocked {
            do {
                try cameraController.switchCameras()
            }
                
            catch {
                print(error)
            }
            switch cameraController.currentCameraPosition {
            case .some(.front):
                sender.setImage(UIImage(named: "flipCamera_fill"), for: .normal)
                
            case .some(.rear):
                sender.setImage(UIImage(named: "flipCamera_white"), for: .normal)
                
            case .none:
                return
            }
        }
        else {
            guard let cameraCapture = constantVC.ActiveSession.cameraCapture else {
                return
            }
            
            constantVC.ActiveSession.cameraCapture?.position = cameraCapture.position == .back ? .front : .back
            
            if constantVC.ActiveSession.cameraCapture?.position == .back {
                sender.setImage(UIImage(named: "flipCamera_white"), for: .normal)
            } else {
                sender.setImage(UIImage(named: "flipCamera_fill"), for: .normal)
            }
        }
    }
    
}

extension TamVideoCall_VC: UICollectionViewDataSource {
    // MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if session?.conferenceType == QBRTCConferenceType.audio {
            return constantVC.ActiveSession.remoteActiveUsers.count
        } else {
            return constantVC.ActiveSession.remoteActiveUsers.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.isGroup == "false" {
            let cell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            
            cell.contentView.tag = indexPath.row
            
            var index = indexPath.row
            let user = constantVC.ActiveSession.remoteActiveUsers[index]
            
            if let opponentVw = userView(userID: user.userID) as? UIView {
                opponentVw.frame = cell.contentView.bounds
                cell.contentView.insertSubview(opponentVw, at: indexPath.row)
                let centerX = NSLayoutConstraint(item: opponentVw,
                                                 attribute: NSLayoutAttribute.centerX,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: cell.contentView,
                                                 attribute: NSLayoutAttribute.centerX,
                                                 multiplier: 1,
                                                 constant: 0);
                cell.contentView.addConstraint(centerX)
                let centerY = NSLayoutConstraint(item: opponentVw,
                                                 attribute: NSLayoutAttribute.centerY,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: cell.contentView,
                                                 attribute: NSLayoutAttribute.centerY,
                                                 multiplier: 1,
                                                 constant: 0);
                cell.contentView.addConstraint(centerY)
                let width = NSLayoutConstraint(item: opponentVw,
                                               attribute: NSLayoutAttribute.width,
                                               relatedBy: NSLayoutRelation.equal,
                                               toItem: cell.contentView,
                                               attribute: NSLayoutAttribute.width,
                                               multiplier: 1,
                                               constant: 0);
                cell.contentView.addConstraint(width)
                let height = NSLayoutConstraint(item: opponentVw,
                                                attribute: NSLayoutAttribute.height,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: cell.contentView,
                                                attribute: NSLayoutAttribute.height,
                                                multiplier: 1,
                                                constant: 0);
                cell.contentView.addConstraint(height)
            }
            
            return cell
        } else {
            //GROUP
            
            let cell:TamVideoCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TamVideoCollectionCell
            
            cell.contentView.tag = indexPath.row
            
            var index = indexPath.row
            let user = constantVC.ActiveSession.remoteActiveUsers[index]
            
            if let opponentVw = userView(userID: user.userID) as? UIView {
                opponentVw.frame = cell.contentView.bounds
                cell.contentView.insertSubview(opponentVw, at: indexPath.row)
                let centerX = NSLayoutConstraint(item: opponentVw,
                                                 attribute: NSLayoutAttribute.centerX,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: cell.contentView,
                                                 attribute: NSLayoutAttribute.centerX,
                                                 multiplier: 1,
                                                 constant: 0);
                cell.contentView.addConstraint(centerX)
                let centerY = NSLayoutConstraint(item: opponentVw,
                                                 attribute: NSLayoutAttribute.centerY,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: cell.contentView,
                                                 attribute: NSLayoutAttribute.centerY,
                                                 multiplier: 1,
                                                 constant: 0);
                cell.contentView.addConstraint(centerY)
                let width = NSLayoutConstraint(item: opponentVw,
                                               attribute: NSLayoutAttribute.width,
                                               relatedBy: NSLayoutRelation.equal,
                                               toItem: cell.contentView,
                                               attribute: NSLayoutAttribute.width,
                                               multiplier: 1,
                                               constant: 0);
                cell.contentView.addConstraint(width)
                let height = NSLayoutConstraint(item: opponentVw,
                                                attribute: NSLayoutAttribute.height,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: cell.contentView,
                                                attribute: NSLayoutAttribute.height,
                                                multiplier: 1,
                                                constant: 0);
                cell.contentView.addConstraint(height)
            }
            
            cell.lblUserName.text = user.userName
            return cell
        }
        
        
       
       /* var index = indexPath.row
       
        let user = users[index]
        let userID = NSNumber(value: user.userID)
        
        cell.didPressMuteButton = { [weak self] isMuted in
            let audioTrack = self?.session?.remoteAudioTrack(withUserID: userID)
            audioTrack?.isEnabled = !isMuted
        }
        
        cell.videoView = userView(userID: user.userID)
        cell.name = ""
        
        guard let currentUser = QBSession.current.currentUser, user.userID != currentUser.id else {
            return cell
        }
        
        if user.bitrate > 0.0 {
            cell.bitrate = user.bitrate
        } else {
            cell.connectionState = user.connectionState
        }
        
        let title = user.userName
        cell.name = title*/
        
    }
}

extension TamVideoCall_VC: UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:110 , height: 130);
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.OpponentCollectionVw?.collectionViewLayout.invalidateLayout();
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let user = constantVC.ActiveSession.remoteActiveUsers[indexPath.row]
        guard let currentUserID = session?.currentUserID,
            user.userID != currentUserID.uintValue else {
                return
        }
        guard let session = session else {
            return
        }
    }
}

extension TamVideoCall_VC: LocalVideoViewDelegate {
    // MARK: LocalVideoViewDelegate
    func localVideoView(_ localVideoView: LocalVideoView, pressedSwitchButton sender: UIButton?) {
        guard let cameraCapture = constantVC.ActiveSession.cameraCapture else {
            return
        }
        let newPosition: AVCaptureDevice.Position = cameraCapture.position == .back ? .front : .back
        guard cameraCapture.hasCamera(for: newPosition) == true else {
            return
        }
        let animation = CATransition()
        animation.duration = 0.75
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.subtype = kCATransitionFromLeft
        //animation.subtype = cameraCapture.position == .back ? .left : .right
        
        localVideoView.superview?.layer.add(animation, forKey: nil)
        cameraCapture.position = newPosition
    }
}

extension TamVideoCall_VC: QBRTCAudioSessionDelegate {
    //MARK: QBRTCAudioSessionDelegate
    func audioSession(_ audioSession: QBRTCAudioSession, didChangeCurrentAudioDevice updatedAudioDevice: QBRTCAudioDevice) {
        let isSpeaker = updatedAudioDevice == QBRTCAudioDevice.speaker
       // dynamicButton.pressed = isSpeaker
    }
}

extension TamVideoCall_VC: QBRTCClientDelegate {
    // MARK: QBRTCClientDelegate
    /**
     *  Called in case when connection state changed
     */
    func session(_ session: QBRTCBaseSession, connectionClosedForUser userID: NSNumber) {
        // remove user from the collection
        if let index = constantVC.ActiveSession.remoteActiveUsers.index(where: { $0.userID == userID.uintValue }) {
            constantVC.ActiveSession.remoteActiveUsers.remove(at: index)
        }
        
        if let videoView = constantVC.ActiveSession.videoViews[userID.uintValue] {
            videoView.removeFromSuperview()
            constantVC.ActiveSession.videoViews.removeValue(forKey: userID.uintValue)
        }
        if self.state !=  .disconnected && self.state !=  .disconnecting {
            reloadContent()
        }
    }
    
    /**
     *  Called in case when connection state changed
     */
    func session(_ session: QBRTCBaseSession, didChange state: QBRTCConnectionState, forUser userID: NSNumber) {
        guard let session = session as? QBRTCSession,
            session == self.session, let index = constantVC.ActiveSession.remoteActiveUsers.index(where: { $0.userID == userID.uintValue }) else {
                return
        }
        let user = constantVC.ActiveSession.remoteActiveUsers[index]
        user.connectionState = state
        if self.state !=  .disconnected && self.state !=  .disconnecting {
            reloadContent()
        }
    }
    
    /**
     *  Called in case when receive remote video track from opponent
     */
    func session(_ session: QBRTCBaseSession,
                 receivedRemoteVideoTrack videoTrack: QBRTCVideoTrack,
                 fromUser userID: NSNumber) {
        
        guard let session = session as? QBRTCSession,
            session == self.session else {
                return
        }
        
        constantVC.ActiveSession.isCallActive = true
        
        if userID != SessionManager.get_QuickBloxID() {
            let user = self.createConferenceUser(userID: UInt(userID.intValue))
            if !(self.isUserExist(userID: userID)) {
                constantVC.ActiveSession.remoteActiveUsers.append(user)
            }
        }
        
        
        TamFirebaseManager.shared.saveDocument(collection: "TestIOS", document: "New entry \(Date())") { (success) in
            
        }
        
        //maintain view for active and background
       if self.state ==  .connected || self.state ==  .connecting {
            if CallActiveInBackgroundManager.shared.isActiveVideoView() {
                if self.isGroup == "true" {
                    reloadContent()
                } else {
                    self.opponentView(superView: self.localVideo_view)
                }
            } else {
                CallActiveInBackgroundManager.shared.configureVideoCallBackgroundView()
            }
        }
    }
   
    func isUserExist(userID: NSNumber) ->Bool {
        for u in constantVC.ActiveSession.remoteActiveUsers {
            if u.userID == UInt(userID.intValue) {
                return true
            }
        }
        return false
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        
        if session == self.session {
            if CallKitManager.isCallKitAvailable() == true {
                CallKitManager.instance.endCall(with: callUUID) {
                    debugPrint("endCall")
                }
            }
            constantVC.ActiveSession.cameraCapture?.stopSession(nil)
            
            let audioSession = QBRTCAudioSession.instance()
            if audioSession.isInitialized == true,
                audioSession.audioSessionIsActivatedOutside(AVAudioSession.sharedInstance()) == false {
                debugPrint("Deinitializing QBRTCAudioSession in CallViewController.")
                audioSession.deinitialize()
            }
            
            if let beepTimer = beepTimer {
                beepTimer.invalidate()
                self.beepTimer = nil
                SoundProvider.stopSound()
            }
            
            if let callTimer = constantVC.ActiveSession.callTimer {
                callTimer.invalidate()
                constantVC.ActiveSession.callTimer = nil
            }
        }
    }
 
    /**
     *  Called in case when connection is established with opponent
     */
    func session(_ session: QBRTCBaseSession, connectedToUser userID: NSNumber) {
        guard let session = session as? QBRTCSession,
            session == self.session else {
                return
        }
        
        self.setCallInfo(isRemoveCalling: true)
        
        if isGroup != "true" {
            if let userID = SessionManager.get_QuickBloxID() {
                let isInitiator = userID.uintValue == session.initiatorID.uintValue
                if isInitiator == true {
                    if CallActiveInBackgroundManager.shared.isActiveVideoView() {
                        self.setLocalView(superView: self.occupantVideo_view)
                    } else {
                        CallActiveInBackgroundManager.shared.configureVideoCallBackgroundView()
                    }
                }
            }
        }
        
        if let beepTimer = beepTimer {
            beepTimer.invalidate()
            self.beepTimer = nil
            SoundProvider.stopSound()
        }
        
        if constantVC.ActiveSession.callTimer == nil {
            
            if CallKitManager.isCallKitAvailable(),
                let currentUser = core.currentUser,
                session.initiatorID.uintValue == currentUser.id {
                CallKitManager.instance.updateCall(with: callUUID, connectedAt: Date())
            }
            
            CallActiveInBackgroundManager.shared.configureCallTimer()
        }
    }
}



class TamVideoCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var vwVideoContent: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    
}
