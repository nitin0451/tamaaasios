//
//  MyAppointmentTableViewCell.swift
//  Tamaas
//
//  Created by Apple on 26/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class MyAppointmentTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonFeedback: UIButton!
    @IBOutlet weak var labelDoctorName: UILabel!
    @IBOutlet weak var labelDoctorSpeciality: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelMonth: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelDay: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
