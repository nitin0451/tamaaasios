//
//  MyAppointmentViewController.swift
//  Tamaas
//
//  Created by Apple on 26/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit
import ObjectMapper

class MyAppointmentViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView()
            tableView.register(UINib(nibName: "MyAppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: "MyAppointmentTableViewCell")
        }
    }
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @objc func buttonActionFeedback(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UITableViewDelegate,UITableViewDataSource
extension MyAppointmentViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAppointmentTableViewCell", for: indexPath) as! MyAppointmentTableViewCell
        cell.buttonFeedback.tag = indexPath.row
        cell.buttonFeedback.addTarget(self, action: #selector(self.buttonActionFeedback(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
        
    }
    
    
}
