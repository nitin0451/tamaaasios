//
//  DoctorCallingPricePopupView.swift
//  Tamaas
//
//  Created by Apple on 17/05/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class DoctorCallingPricePopupView: UIView {
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var localisationLabelAudioCall: UILabel!{
        didSet{
             localisationLabelAudioCall.text = "Free Audio call".localized
        }
    }
    @IBOutlet weak var localisationLabelScheduleCall: UILabel!{
        didSet{
            localisationLabelScheduleCall.text = "Schedule call as per your availability".localized
        }
    }
    @IBOutlet weak var localisationLabelQuickConnect: UILabel!{
        didSet{
             localisationLabelQuickConnect.text = "Quick Connect".localized
        }
    }
    @IBOutlet weak var localisationLabelVideoCall: UILabel!{
        didSet{
             localisationLabelVideoCall.text = "Video Call with doctor".localized
        }
    }
    @IBOutlet weak var buttonStartConsultancy: UIButton!{
        didSet{
            buttonStartConsultancy.setTitle("".localized, for: .normal)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
       
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }

    func loadViewFromNib() {
           
        let view = UINib(nibName: "DoctorCallingPricePopupView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
          
    }

}
