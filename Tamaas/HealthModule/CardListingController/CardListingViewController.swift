//
//  CardListingViewController.swift
//  DemoApp
//
//  Created by Apple on 01/04/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit
import ObjectMapper

typealias SourceCompletionHandler = (_ success:AnyObject) -> ()
class CardListingViewController: BaseHealthViewController {
    
    @IBOutlet weak var labelNoCardsAdded: UILabel!
    //MARK:- IBOutlet
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var heightConstraintNavigationView: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.register(UINib(nibName: "CardListingTableViewCell", bundle: nil), forCellReuseIdentifier: "CardListingTableViewCell")
        }
    }
    var fee: Int?
    var completionBlock : SourceCompletionHandler?
    
    //MARK:- Variables
    var cardListingModalArray: [CardListingModalClass]?{
        didSet{
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let _ = fee{
            self.viewNavigation.isHidden = false
            self.heightConstraintNavigationView.constant = 64
        } else {
            self.viewNavigation.isHidden = true
            self.heightConstraintNavigationView.constant = 0
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.getSavedCardListing()
    }
    
    func getSavedCardListing() {
        
        if Connectivity.isConnectedToInternet() {
                 
            self.showLoader(strForMessage: "loading..".localized)
            let webservice  = WebserviceSigleton ()
                 
            let dict: Dictionary<String,Any> = ["userid":SessionManager.getUserId(),"action": "stripe_get_ios"]
            print(dict)
            webservice.POSTServiceWithParametersNew(urlString: constantVC.WebserviceName.URL_SAVE_FETCH_CARDDETAIL, params: dict as Dictionary<String, AnyObject>, isHeaders: false) { (data, error) in
                
                self.dismissLoader()
                if let cardArray = data?["data"] as? [Dictionary<String,Any>] {
                    self.getCustomerId(cardData: cardArray)
                }
            }
            
        }

    }
    
    
    func getCustomerId(cardData: [Dictionary<String,Any>]) {
        
        if Connectivity.isConnectedToInternet() {
                 
            self.showLoader(strForMessage: "loading..".localized)
            let webservice  = WebserviceSigleton ()
                 
            let dict: Dictionary<String,Any> = ["userid":SessionManager.getUserId(),"action": "stripe_get"]
            print(dict)
            webservice.POSTServiceWithParametersNew(urlString: constantVC.WebserviceName.URL_SAVE_FETCH_CARDDETAIL, params: dict as Dictionary<String, AnyObject>, isHeaders: false) { (data, error) in
                
                self.dismissLoader()
                if let cardArray = data?["data"] as? [Dictionary<String,Any>] {

                    var tempData = [CardListingModalClass]()
                    for card in cardData {
                        if let obj = Mapper<CardListingModalClass>().map(JSONObject: card) {
                            if let index = cardArray.lastIndex(where: {$0["q_id"]as? String == obj.userid ?? ""}) {
                                let cardDict = cardArray[index]
                                if let custKey = cardDict["customer_key"] as? String{
                                    obj.customer_key = custKey
                                }
                                tempData.append(obj)
                            }
                        }
                    }
                    
                    self.labelNoCardsAdded.isHidden = false ? tempData.count == 0 : true
                    self.cardListingModalArray = tempData
                    
                }
                
            }
            
        }

    }
    
    func deleteSavedCard(cardId: String) {
        
        if Connectivity.isConnectedToInternet() {
                 
            self.showLoader(strForMessage: "loading..".localized)
            let webservice  = WebserviceSigleton ()
                 
            let dict: Dictionary<String,Any> = ["cardid":cardId,"action": "delete_card"]
            print(dict)
            webservice.POSTServiceWithParametersNew(urlString: constantVC.WebserviceName.URL_SAVE_FETCH_CARDDETAIL, params: dict as Dictionary<String, AnyObject>, isHeaders: false) { (data, error) in
                
                self.dismissLoader()
                if let msg = data?["msg"] as? String,msg == "success" {
                    if let index = self.cardListingModalArray?.lastIndex(where: {$0.id ?? "" == cardId}) {
                        self.cardListingModalArray?.remove(at: index)
                    }
                }
                
                self.labelNoCardsAdded.isHidden = false ? self.cardListingModalArray?.count ?? 0 == 0 : true

            }
            
        }
        
    }
    

    func payFromCard(customerKey: String) {
        
        if Connectivity.isConnectedToInternet() {
                 
            self.showLoader(strForMessage: "loading..".localized)
            let webservice  = WebserviceSigleton ()
            let dict: Dictionary<String,Any> = ["email": "","customerid":customerKey,"amount": self.fee ?? 0,"currency_code":"USD","item_name": "test","item_number": "123456"]
            print(dict)
            webservice.POSTServiceWithParametersNew(urlString: constantVC.WebserviceName.URL_SEND_PAYMENT_DETAIL, params: dict as Dictionary<String, AnyObject>, isHeaders: true, isFormData: true) { (data, error) in
                
                self.dismissLoader()
                if let _ = data?["success"] as? Dictionary<String,Any>{
                    self.dismiss(animated: true) {
                        guard let cb = self.completionBlock else  { return }
                         cb(true as AnyObject)
                    }
                }
                
            }
            
        }
        
    }
    
    @IBAction func buttonActionAddNewCard(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Health", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
        if let _ = fee{
            self.present(vc, animated: true, completion: nil)
        } else {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func buttonActionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    
    }
    
    @IBAction func buttonActionAddCard(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Health", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddCardViewController") as! AddCardViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MAKE:- UITableViewDelegate,UITableViewDataSource
extension CardListingViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cardListingModalArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CardListingTableViewCell", for: indexPath) as? CardListingTableViewCell else {return UITableViewCell()}
        if let data = self.cardListingModalArray?[indexPath.row]{
            cell.labelCardNo.text = "**** **** **** \(data.last_four ?? "")"
            cell.labelCardDetail.text = ""
        }
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let data = self.cardListingModalArray?[indexPath.row]{
            self.payFromCard(customerKey: data.customer_key ?? "")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            if let data = self.cardListingModalArray?[indexPath.row]{
                if let cardId = data.id {
                    self.deleteSavedCard(cardId: cardId)
                }
            }
        }
    }

}
