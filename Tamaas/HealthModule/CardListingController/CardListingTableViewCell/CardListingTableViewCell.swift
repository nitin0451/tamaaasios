//
//  CardListingTableViewCell.swift
//  DemoApp
//
//  Created by Apple on 01/04/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class CardListingTableViewCell: UITableViewCell {

    @IBOutlet weak var labelCardDetail: UILabel!
    @IBOutlet weak var labelCardNo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
