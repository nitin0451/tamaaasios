//
//  CardListingModal.swift
//  Tamaas
//
//  Created by Apple on 13/05/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import Foundation
import ObjectMapper

class CardListingModalClass: Mappable {
    
    var last_four : String?
    var id : String?
    var userid : String?
    var customer_key : String?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    open func mapping(map: Map) {
        last_four       <- map["last_four"]
        id              <- map["id"]
        userid          <- map["userid"]
        customer_key    <- map["customer_key"]

    }
    
}
