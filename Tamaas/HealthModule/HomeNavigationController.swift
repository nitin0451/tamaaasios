//
//  HomeNavigationController.swift
//  Tamaas
//
//  Created by osvinuser on 16/08/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class HomeNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRootViewController()
        // Do any additional setup after loading the view.
    }
    
    func addRootViewController(){
        let mainStoryBoard = UIStoryboard(name: "Health", bundle: nil)
        let homeController = mainStoryBoard.instantiateViewController(withIdentifier: "HealthHomeVC") as! HealthHomeVC
        self.viewControllers = [homeController]
       
    }
    
}
