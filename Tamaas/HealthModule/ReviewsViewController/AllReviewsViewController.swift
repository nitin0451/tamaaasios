//
//  ReviewsViewController.swift
//  Tamaas
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit
import ObjectMapper

class AllReviewsViewController: BaseHealthViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var labelNoReviewsAdded: UILabel!{
        didSet{
            labelNoReviewsAdded.text = "No reviews are added yet!!!".localized
        }
    }
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.register(UINib(nibName: "AllReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "AllReviewsTableViewCell")
        }
    }
    
    //MARK:- Variables
    var id: String? {
        didSet{
            self.getRatingList(id: id ?? "0")
        }
    }
    var allReviewsModalArray : [AllReviewsModalClass]? {
        didSet{
            self.tableView.reloadData()
        }
    }
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    func getRatingList(id: String){
       
        if Connectivity.isConnectedToInternet() {
             
            self.showLoader(strForMessage: "loading..".localized)
            let webservice  = WebserviceSigleton ()
                 
            let dict: Dictionary<String,Any> = ["userid":id]
            webservice.POSTServiceWithParametersNew(urlString: constantVC.WebserviceName.URL_GET_RATING_LIST, params: dict as Dictionary<String, AnyObject>, isHeaders: false) { (data, error) in
                
                self.dismissLoader()
                print(data as Any)
                if let ratingData = data?["ratingData"] as? [Dictionary<String,Any>]{
                    var tempData = [AllReviewsModalClass]()
                    for data in ratingData {
                        if let obj = Mapper<AllReviewsModalClass>().map(JSONObject: data) {
                            tempData.append(obj)
                        }
                    }
                    self.labelNoReviewsAdded.isHidden = false ? tempData.count == 0 : true
                    self.allReviewsModalArray = tempData
                }
                
            }
            
        }
        
    }
    
}


//MARK:- UITableViewDelegate,UITableViewDataSource
extension AllReviewsViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allReviewsModalArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllReviewsTableViewCell", for: indexPath) as! AllReviewsTableViewCell
        if let data = self.allReviewsModalArray?[indexPath.row] {
            cell.labelUserName.text = data.from?.name ?? ""
            cell.labelReview.text = data.rivew ?? ""
            //cell.labelDate = data.
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
