//
//  AllReviewsTableViewCell.swift
//  Tamaas
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class AllReviewsTableViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelUserName: UILabel!
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var labelReview: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
