//
//  AllRatingModal.swift
//  Tamaas
//
//  Created by Apple on 12/05/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import Foundation
import ObjectMapper

class AllReviewsModalClass : Mappable {
    
    var id : String?
    var from : FromDataModalClass?
    var rating : String?
    var rivew : String?
    var status : String?

    required init?(map: Map) {
        mapping(map: map)
    }
    
    open func mapping(map: Map) {
        id              <- map["id"]
        from            <- map["from"]
        rating          <- map["rating"]
        rivew           <- map["rivew"]
        status          <- map["status"]

    }
    
}

class FromDataModalClass : Mappable {
    
    var name : String?
    var phoneNumber : String?
    var photoURL : String?

    required init?(map: Map) {
        mapping(map: map)
    }
    
    open func mapping(map: Map) {
        name              <- map["name"]
        phoneNumber       <- map["phoneNumber"]
        photoURL          <- map["photoURL"]

    }
    
}
