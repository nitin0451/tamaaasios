//
//  AppointmentDetailViewController.swift
//  DemoApp
//
//  Created by Apple on 01/04/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class AppointmentDetailViewController: UIViewController {

    //MAKE:- IBOutlet
    @IBOutlet weak var buttonMakePayment: UIButton!
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.register(UINib(nibName: "AppointmentTableViewCell", bundle: nil), forCellReuseIdentifier: "AppointmentTableViewCell")
        }
    }
    
    //MARK:- Variables
    var headintArray = ["Patient Name","Email","Phone"]
    var paymentViewObj: PaymentSelectionView?

    //MAKE:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func buttonActionMakePayment(_ sender: Any) {
        
        self.paymentViewObj = PaymentSelectionView(frame: CGRect(x: 0, y: -20, width: self.view.frame.width, height: self.view.frame.height+20))
        self.paymentViewObj?.buttonPayViaCards.addTarget(self, action: #selector(self.buttonActionPayViaCards), for: .touchUpInside)
        self.paymentViewObj?.buttonPayViaInsurance.addTarget(self, action: #selector(self.buttonActionPayViaInsurance), for: .touchUpInside)

        UIView.animate(withDuration: 0.5) {
            self.view.addSubview(self.paymentViewObj ?? UIView())
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        }
    }
    
    @objc func buttonActionPayViaCards() {
        self.paymentViewObj?.removeFromSuperview()
        self.moveToCardListing()
    }
    
    @objc func buttonActionPayViaInsurance() {
        self.paymentViewObj?.removeFromSuperview()
        self.moveToCardListing()
    }
    
    func moveToCardListing() {
        let storyboard = UIStoryboard(name: "Health", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CardListingViewController") as! CardListingViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MAKE:- UITableViewDelegate,UITableViewDataSource
extension AppointmentDetailViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headintArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentTableViewCell", for: indexPath) as? AppointmentTableViewCell else {return UITableViewCell()}
        
        cell.labelTitle.text = headintArray[indexPath.row]
        cell.textFieldInfo.delegate = self
        switch indexPath.row {
        case 0:
            cell.textFieldInfo.keyboardType = .default
        case 1:
            cell.textFieldInfo.keyboardType = .emailAddress
        default:
            cell.textFieldInfo.keyboardType = .phonePad
        }
        
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
}

extension AppointmentDetailViewController: UITextFieldDelegate {
    
}
