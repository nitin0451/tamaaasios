//
//  NewsListingViewController.swift
//  Tamaas
//
//  Created by Apple on 14/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit
import ObjectMapper

class NewsListingViewController: BaseHealthViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsTableViewCell")
        }
    }
    
    //MARK:- Variablesr
    private var articlesModalClassArray : [ArticleModalClass]? {
        didSet{
               if articlesModalClassArray?.count ?? 0 > 0 {
                   
               }
           }
      }
    var searchActive = false
    var tempArticlesArray : [ArticleModalClass]?
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getHealthNews()
    }

    func getHealthNews() {
         
         if Connectivity.isConnectedToInternet() {
         
             self.showLoader(strForMessage: "loading..".localized)
             let webservice  = WebserviceSigleton ()
             webservice.GETServiceWithoutParameters(urlString: "http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=14e28b06e23d4eff88514fe7b317fb53") { (data, error) in
                        
                self.dismissLoader()
                if let articles = data?["articles"] as? [Dictionary<String,Any>] {
                    var tempObj = [ArticleModalClass]()
                    for article in articles {
                        if let obj = Mapper<ArticleModalClass>().map(JSONObject: article) {
                            tempObj.append(obj)
                        }
                    }
                    self.articlesModalClassArray = tempObj
                    self.tempArticlesArray = self.articlesModalClassArray
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.view.layoutSubviews()
                        self.view.layoutIfNeeded()
                    }
                }
             }
         }
     }
     
}

//MARK:- UITableViewDelegate,UITableViewDataSource
extension NewsListingViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articlesModalClassArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as! NewsTableViewCell
        if let data = self.articlesModalClassArray?[indexPath.row] {
            cell.labelHeading.text = data.title ?? ""
            cell.labelNews.text = data.descriptionText ?? ""
            if let imageUrl = data.urlToImage {
                cell.imageViewNews.isHidden = false
                cell.imageViewNews.sd_setShowActivityIndicatorView(true)
                cell.imageViewNews.sd_setIndicatorStyle(.gray)
                cell.imageViewNews.sd_setImage(with: URL(string: imageUrl), placeholderImage: nil)
            } else {
                cell.imageViewNews.isHidden = true
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let data = self.articlesModalClassArray?[indexPath.row] {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            vc.urlString = data.url ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- UITextFieldDelegate
extension NewsListingViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
        self.articlesModalClassArray = self.tempArticlesArray
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        self.articlesModalClassArray = self.tempArticlesArray
        searchBar.text = nil
        searchBar.resignFirstResponder()

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchActive = true
        
        let filterdata = searchText.isEmpty ? self.tempArticlesArray : self.articlesModalClassArray?.filter({$0.title?.contains(searchText) ?? false || $0.descriptionText?.contains(searchText) ?? false})
        self.articlesModalClassArray = filterdata
    }
}
