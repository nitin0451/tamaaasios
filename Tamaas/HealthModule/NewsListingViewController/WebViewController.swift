//
//  WebViewController.swift
//  OfficeDemo
//
//  Created by Apple on 18/04/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController,WKUIDelegate {

    //MARK:- IBOutlet
    @IBOutlet weak var webView: WKWebView!{
        didSet{
            webView.uiDelegate = self
        }
    }
    
    //MARK:- Variables
    var urlString:String?
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let str = urlString else {return}
        guard let url = URL(string: str) else {return}
        let myRequest = URLRequest(url: url)
        webView.load(myRequest)
    }

}
