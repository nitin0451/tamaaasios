//
//  FilterCollectionViewCell.swift
//  Tamaas
//
//  Created by Apple on 26/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class FilterCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewBackground: CustomDesignView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
