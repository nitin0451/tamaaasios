//
//  FilterViewController.swift
//  DemoApp
//
//  Created by Apple on 31/03/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var buttonReset: UIButton!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
             collectionView.register(UINib(nibName: "FilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FilterCollectionViewCell")
        }
    }
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        }
    }
    
    //MARK:- Variables
    let headerArray = ["Sort by","Consultation Fee ($)","Gender","Availablity"]
    let array1 = ["Distance","Consultataion Fees"]
    let array2 = ["Free", "10-15","15-30","30-60","60+"]
    let array3 = ["Male","Female"]
    let array4 = ["Tomorrow","Next 7 days"]
    let collectionViewHeaderFooterReuseIdentifier = "MyHeaderFooterClass"
    var indexArray = [IndexPath]()
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(MyHeaderFooterClass.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: collectionViewHeaderFooterReuseIdentifier)
    }

    //MARK:- viewDidLoad
    @IBAction func buttonActionReset(_ sender: Any) {
        
        
    }
    
    @IBAction func buttonActionAddNew(_ sender: Any) {
        
        
    }
    
}

//MARK:- UITableViewDelegate, UITableViewDataSource
extension FilterViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as? FilterTableViewCell else {return UITableViewCell()}
        let layout = cell.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        if indexPath.section == 0 {
            cell.titleArray = self.array1
            layout.scrollDirection = .horizontal
        } else if indexPath.section == 1 {
            layout.scrollDirection = .vertical
            cell.collectionView.isScrollEnabled = false
            cell.titleArray = self.array2
        } else if indexPath.section == 2 {
            layout.scrollDirection = .horizontal
            cell.titleArray = self.array3
        } else if indexPath.section == 3 {
            layout.scrollDirection = .horizontal
            cell.titleArray = self.array4
        }
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 120
        } else {
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        
        let titleLabel = UILabel(frame: CGRect(x: 20, y: 10, width: tableView.frame.width-40, height: 20))
        titleLabel.font = UIFont(name: "ProximaNova-Regular", size: 16.0)
        titleLabel.text = headerArray[section]
        
        headerView.addSubview(titleLabel)
        return headerView
    }
}


//MARK:- UICollectionViewDelegate,UICollectionViewDataSource
extension FilterViewController: UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return array1.count
        } else if section == 1 {
            return array2.count
        } else if section == 2 {
            return array3.count
        } else if section == 3 {
            return array4.count
        } else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
       // cell.viewBackground.layer.cornerRadius = cell.viewBackground.frame.width/2

        if indexPath.section == 0 {
            cell.labelTitle.text =  self.array1[indexPath.item]
        } else if indexPath.section == 1 {
            cell.labelTitle.text =  self.array2[indexPath.item]
        } else if indexPath.section == 2 {
            cell.labelTitle.text =  self.array3[indexPath.item]
        } else if indexPath.section == 3 {
            cell.labelTitle.text =  self.array4[indexPath.item]
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! FilterCollectionViewCell
        
        if self.indexArray.contains(indexPath) {
            if let index = self.indexArray.lastIndex(where: {$0 == indexPath}) {
                self.indexArray.remove(at: index)
            }
            cell.viewBackground.backgroundColor = .white
        } else {
            self.indexArray.append(indexPath)
            cell.viewBackground.backgroundColor = UIColor(red: 28.0/255.0, green: 87.0/255.0, blue: 255.0/255.0, alpha: 1)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: collectionViewHeaderFooterReuseIdentifier, for: indexPath)
            let titleLabel = UILabel(frame: CGRect(x: 5, y: 10, width: collectionView.frame.width-10, height: 20))
            titleLabel.font = UIFont(name: "ProximaNova-Regular", size: 16.0)
            titleLabel.text = headerArray[indexPath.section]
            headerView.addSubview(titleLabel)
            
            headerView.backgroundColor = UIColor.white
            return headerView
        default:
           return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
            return CGSize(width: collectionView.frame.width, height: 40.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: 100, height: 50)
    }
    
}

class MyHeaderFooterClass: UICollectionReusableView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
