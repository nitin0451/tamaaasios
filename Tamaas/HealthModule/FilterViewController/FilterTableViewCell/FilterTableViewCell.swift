//
//  FilterTableViewCell.swift
//  Tamaas
//
//  Created by Apple on 26/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {
    
    //MARK:- IBOutlet
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet{
            collectionView.register(UINib(nibName: "FilterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FilterCollectionViewCell")
        }
    }
    
    //MARK:- Variables
    var titleArray = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//MARK:- UICollectionViewDelegate,UICollectionViewDataSource
extension FilterTableViewCell: UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titleArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionViewCell", for: indexPath) as! FilterCollectionViewCell
        
        cell.labelTitle.text = self.titleArray[indexPath.item]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! FilterCollectionViewCell
        cell.viewBackground.backgroundColor = UIColor(red: 28.0/255.0, green: 87.0/255.0, blue: 255.0/255.0, alpha: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! FilterCollectionViewCell
        cell.viewBackground.backgroundColor = .white

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if titleArray.count == 5 {
            return CGSize(width: collectionView.frame.width/3.2, height: 50)
        } else {
            return CGSize(width: collectionView.frame.width/2.7, height: 50)
        }
    }
    
}
