//
//  DoctorDetailViewController.swift
//  DemoApp
//
//  Created by Apple on 30/03/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class DoctorDetailViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.estimatedRowHeight = 100
            tableView.register(UINib(nibName: "GeneralInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "GeneralInfoTableViewCell")
            tableView.register(UINib(nibName: "ConsultationFeeTableViewCell", bundle: nil), forCellReuseIdentifier: "ConsultationFeeTableViewCell")
            tableView.register(UINib(nibName: "LocationTableViewCell", bundle: nil), forCellReuseIdentifier: "LocationTableViewCell")
            tableView.register(UINib(nibName: "TimingTableViewCell", bundle: nil), forCellReuseIdentifier: "TimingTableViewCell")
            tableView.register(UINib(nibName: "ReviewsTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewsTableViewCell")
            tableView.register(UINib(nibName: "ServicesTableViewCell", bundle: nil), forCellReuseIdentifier: "ServicesTableViewCell")
        }
    }
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func buttonActionShare(_ sender: Any) {
        
    }
    
    @IBAction func buttonActionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonActionRequestAppointment(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Health", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AppointmentDetailViewController") as! AppointmentDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

//MARK:- UITableViewDelegate, UITableViewDataSource
extension DoctorDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "GeneralInfoTableViewCell", for: indexPath) as? GeneralInfoTableViewCell else {return UITableViewCell()}
                   
            cell.selectionStyle = .none
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ConsultationFeeTableViewCell", for: indexPath) as? ConsultationFeeTableViewCell else {return UITableViewCell()}
                   
            cell.selectionStyle = .none
            return cell
            
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "LocationTableViewCell", for: indexPath) as? LocationTableViewCell else {return UITableViewCell()}
                   
            cell.selectionStyle = .none
            return cell
            
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "TimingTableViewCell", for: indexPath) as? TimingTableViewCell else {return UITableViewCell()}
                   
            cell.selectionStyle = .none
            return cell
            
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewsTableViewCell", for: indexPath) as? ReviewsTableViewCell else {return UITableViewCell()}
                   
            cell.selectionStyle = .none
            return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesTableViewCell", for: indexPath) as? ServicesTableViewCell else {return UITableViewCell()}
                   
            cell.selectionStyle = .none
            return cell
            
        }
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 130
        } else if indexPath.row == 5 {
             return 160
        } else {
            return UITableViewAutomaticDimension
        }
    }
}
