//
//  ReviewsTableViewCell.swift
//  DemoApp
//
//  Created by Apple on 30/03/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class ReviewsTableViewCell: UITableViewCell {
    
    //MARK:- IBOutlet
    @IBOutlet weak var labelFirstReview: UILabel!
    @IBOutlet weak var labelFirstDate: UILabel!
    @IBOutlet weak var labelFirstNameInitial: UILabel!
    @IBOutlet weak var labelFirstName: UILabel!
    @IBOutlet weak var buttonWriteFeedback: UIButton!
    @IBOutlet weak var buttonAllFeedback: UIButton!
    @IBOutlet weak var labelSecondReview: UILabel!
    @IBOutlet weak var labelSecondDate: UILabel!
    @IBOutlet weak var labelSecondName: UILabel!
    @IBOutlet weak var labelSecondNameInitial: UILabel!
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet{
            collectionView.register(UINib(nibName: "ImagesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImagesCollectionViewCell")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//MARK:- UICollectionViewDelegate,UICollectionViewDataSource
extension ReviewsTableViewCell: UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCollectionViewCell", for: indexPath) as? ImagesCollectionViewCell else {return UICollectionViewCell()}
        
        cell.reviewImage.image = #imageLiteral(resourceName: "sport_13")
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.height-10, height: collectionView.frame.size.height-10)
        
    }
    
}
