//
//  GeneralInfoTableViewCell.swift
//  DemoApp
//
//  Created by Apple on 30/03/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class GeneralInfoTableViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            collectionView.register(UINib(nibName: "GeneralInfoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GeneralInfoCollectionViewCell")
        }
    }
    
    var imageArray = [UIImage(named: "medical"),UIImage(named: "location"),UIImage(named: "reviewBig"),UIImage(named: "thumbsUpGray")]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
//MARK:- UICollectionViewDelegate,UICollectionViewDataSource
extension GeneralInfoTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GeneralInfoCollectionViewCell", for: indexPath) as? GeneralInfoCollectionViewCell else {return UICollectionViewCell()}
        cell.imageViewMain.image = imageArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        return CGSize(width: collectionView.frame.width/2-10, height: 50)
    }
}
