//
//  GeneralInfoCollectionViewCell.swift
//  DemoApp
//
//  Created by Apple on 30/03/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class GeneralInfoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewMain: UIImageView!
    @IBOutlet weak var labelDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
