//
//  DoctorListingViewController.swift
//  DemoApp
//
//  Created by Apple on 29/03/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit
import ObjectMapper
import ObjectiveC
import YYWebImage
import Cosmos

private var AssociatedObjectHandle: UInt8 = 0

class DoctorListingViewController: BaseHealthViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var labelNoDocFound: UILabel! {
        didSet{
            labelNoDocFound.text = "No doctors found!!!".localized
        }
    }
    @IBOutlet weak var labelLocation: UILabel!{
        didSet{
            labelLocation.text = SessionManager.getCountry()
        }
    }
    @IBOutlet weak var buttonAllFilters: UIButton!
    @IBOutlet weak var buttonAvailableToday: UIButton!
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.estimatedRowHeight = 240
            tableView.register(UINib(nibName: "DoctorListingTableViewCell", bundle: nil), forCellReuseIdentifier: "DoctorListingTableViewCell")
        }
    }
    @IBOutlet weak var buttonUsa: UIButton!
    @IBOutlet weak var buttonLocal: UIButton!
    @IBOutlet weak var constraintViewLeading: NSLayoutConstraint!
    
    //MARK:- variables
    var viewObj : DoctorListingHeaderView?
    var doctorListingArray = [DoctorListingModal]()
    var symptomsMessage  = "" 
    var movingFor: String?

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDoctorListing(forAll: true)
    }
    
    //MARK:- Button Actions
    @IBAction func buttonAction_usa(_ sender: Any) {
        UIView.animate(withDuration: 0.25) {
            self.getDoctorListing(forAll: false)
            self.constraintViewLeading.constant = self.view.frame.width/2
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func buttonAction_local(_ sender: Any) {
        UIView.animate(withDuration: 0.25) {
            self.getDoctorListing(forAll: true)
            self.constraintViewLeading.constant = 0
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func buttonActionAvailableToday(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonActionBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func buttonActionAllFilters(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Health", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func buttonActionViewProfile(_ sender: UIButton) {
        self.performSegue(withIdentifier: "segueDetail", sender: nil)        
    }
    
    @objc func buttonActionRequestAppointment(_ sender : UIButton) {
        //Nitin
        if let data = sender.associatedAnyData as? DoctorListingModal {
            guard let quickBloxId = data.quickbloxID else {return}
            let phoneNo = data.phoneNumber ?? ""
            let fees = data.fee ?? 0
            let name = data.name ?? ""

            if let privateChatDialog = ServicesManager.instance().chatService.dialogsMemoryStorage.privateChatDialog(withOpponentID: UInt(quickBloxId) ?? 0) {
                constantVC.openPrivateChat.isActiveOpenPrivateChat = true

                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else { return}
                controller.modalPresentationStyle = .fullScreen
                controller.isPresented = true
                controller.symptomsMessage = self.symptomsMessage
                controller.movingFor = self.movingFor
                //QBChatDialog
                let dialog = privateChatDialog
                dialog.name = name
                controller.dialog = dialog
                self.present(controller , animated: true , completion: nil)
                
            } else {

                ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: UInt(quickBloxId)!,phoneNo + "-" + SessionManager.getPhone() , isDoctor: true, fees: String(fees)) { (response, chatDialog) in
                    
                      if response.isSuccess {
                        constantVC.openPrivateChat.isActiveOpenPrivateChat = true
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        guard let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController else {return}
                        controller.modalPresentationStyle = .fullScreen
                        controller.symptomsMessage = self.symptomsMessage
                        controller.movingFor = self.movingFor
                        
                        var data = chatDialog?.data
                        chatDialog?.name = name
                        controller.dialog = chatDialog
                        controller.isPresented = true
                        if let dialog_ = chatDialog {
                            QuickBloxManager().sendSystemMsg_updateDialog_toOccupants(dialog: dialog_)
                        }
                        self.present(controller , animated: true , completion: nil)

                    } else {
                        //print(response.error.debugDescription)
                    }
                }
            }
        }
    }
    
    @objc func buttonActionReviews(_ sender: UIButton) {

        if let id = sender.associatedAnyData as? Int {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllReviewsViewController") as! AllReviewsViewController
            vc.id = String(id)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getDoctorListing(forAll : Bool = true) {

        if Connectivity.isConnectedToInternet() {
        
            self.showLoader(strForMessage: "loading..".localized)
            let webservice  = WebserviceSigleton ()
            let dict: Dictionary<String,Any> = ["country":203,"userid":SessionManager.getUserId()]

            webservice.POSTServiceWithParametersNew(urlString: constantVC.WebserviceName.URL_GET_DOCTOR_LISTING, params: forAll == true ? nil : dict as Dictionary<String, AnyObject>) { (data, error) in
                
                self.dismissLoader()
                if let doctorData = data?["doctorData"] as? [Dictionary<String,AnyObject>] {
                    print(doctorData)
                    for data in doctorData {
                        if let obj = Mapper<DoctorListingModal>().map(JSON: data) {
                            self.doctorListingArray.append(obj)
                        }
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
}

//MARK:- UITableViewDelegate, UITableViewDataSource
extension DoctorListingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.doctorListingArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DoctorListingTableViewCell", for: indexPath) as? DoctorListingTableViewCell else {return UITableViewCell()}
        
        let data = self.doctorListingArray[indexPath.row]
        cell.buttonViewProfile.associatedAnyData = data.id ?? 0
        cell.buttonRequestAppointment.associatedAnyData = data
        cell.buttonReviews.associatedAnyData = data.id ?? 0
        cell.labelName.text = data.name ?? ""
        cell.labelAddress.text = data.address ?? ""
        let img = data.photoURL ?? ""
        let imgStr = constantVC.GeneralConstants.ImageUrl + img
        cell.imageViewProfile.yy_setImage(with: URL.init(string: imgStr) , placeholder: UIImage(named:"contact_big") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        if data.tag?.count ?? 0 > 0 {
            if let tag = data.tag?[0] {
                cell.labelDoctorType.text = tag.tagName ?? ""
            }
        }
        if let count = data.rivewCount{
            if count == 1 || count == 0 {
                cell.labelReviewsCount.text = "\(String(describing: count)) Review"
            } else {
                cell.labelReviewsCount.text = "\(String(describing: count)) Reviews"
            }
        }
        if let fee = data.fee {
            cell.labelFee.text = "$\(String(describing: fee)) Consultation Fees"
        }
        cell.ratingView.rating = Double(data.rating ?? "0")!
        cell.buttonViewProfile.addTarget(self, action: #selector(self.buttonActionViewProfile(_:)), for: .touchUpInside)
        cell.buttonRequestAppointment.addTarget(self, action: #selector(self.buttonActionRequestAppointment(_:)), for: .touchUpInside)
        cell.buttonReviews.addTarget(self, action: #selector(self.buttonActionReviews(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
        
    }//

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 50
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        self.viewObj = DoctorListingHeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
//        self.view.addSubview(self.viewObj ?? UIView())
//        return self.viewObj
//    }
//
    
}

extension UIButton {
    
    var associatedDictValue: Dictionary<String,Any>? {
        get{
            return objc_getAssociatedObject(self, &AssociatedObjectHandle) as?  Dictionary<String,Any>
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var assocaitedStringValue : String? {
        get {
            return objc_getAssociatedObject(self, &AssociatedObjectHandle) as? String
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    var associatedAnyData : Any? {
        get {
            return objc_getAssociatedObject(self, &AssociatedObjectHandle)
        }
        set {
            objc_setAssociatedObject(self, &AssociatedObjectHandle, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}
