//
//  DoctorListingTableViewCell.swift
//  DemoApp
//
//  Created by Apple on 29/03/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit
import Cosmos

class DoctorListingTableViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var buttonReviews: UIButton!
    @IBOutlet weak var labelFee: UILabel!
    @IBOutlet weak var buttonRequestAppointment: CustomButtonView!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelReviewsCount: UILabel!
    @IBOutlet weak var buttonViewProfile: UIButton!
    @IBOutlet weak var labelDoctorType: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewProfile: CustomImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
