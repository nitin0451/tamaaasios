//
//  DoctorListingHeaderView.swift
//  DemoApp
//
//  Created by Apple on 29/03/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class DoctorListingHeaderView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        
        let view = UINib(nibName: "DoctorListingHeaderView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
       
    }
}
