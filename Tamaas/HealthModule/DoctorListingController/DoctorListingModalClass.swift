//
//  DoctorListingModalClass.swift
//  Tamaas
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import Foundation
import ObjectMapper

class DoctorListingModal: Mappable {
    
    var address : String?
    var fee : Int?
    var gender : String?
    var rating : String?
    var rivewCount : Int?
    var id : Int?
    var name : String?
    var phoneNumber : String?
    var photoURL : String?
    var quickbloxID : String?
    var tag : [TagModal]?
    
    required init?(map: Map) {
        mapping(map: map)
    }
       
    // Mappable
    open func mapping(map: Map) {
        
        address         <- map["address"]
        fee             <- map["fee"]
        gender          <- map["gender"]
        rating          <- map["rating"]
        rivewCount      <- map["rivewCount"]
        id              <- map["id"]
        name            <- map["name"]
        phoneNumber     <- map["phoneNumber"]
        photoURL        <- map["photoURL"]
        quickbloxID     <- map["quickbloxID"]
        tag             <- map["tag"]
    }
}

class TagModal : Mappable {
    var tagName : String?

    required init?(map: Map) {
        mapping(map: map)
    }
       
    // Mappable
    open func mapping(map: Map) {
        tagName         <- map["tagName"]
    }
}
