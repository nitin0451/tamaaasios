//
//  ChatInnerView.swift
//  Tamaas
//
//  Created by Apple on 09/05/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import Foundation
import UIKit

class ChatInnerView: UIView {
    
    @IBOutlet weak var labelTitle: UILabel!{
        didSet{
            labelTitle.text = "Your Telemedicine Appointment\nDetails".localized
        }
    }
    @IBOutlet weak var labelMsg: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
       
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }

    func loadViewFromNib() {
           
        let view = UINib(nibName: "ChatInnerView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
          
    }

    func showAttributedMsg() {
        let str = "1. Send a message of your availability to the practitioner only. Wait for a response.\n\n 2. Once the time is confirmed, you need to initiate a Video or Voice Call for the appointment.\n\n 3. The consultation fee will be asked before the call.\n\n Stay Home. Stay Strong. Stay Safe."
        
        let att1 = "availability"
        let att2 = "response."
        let att3 = "Video or Voice Call"
        let att4 = "fee"
        let att5 = "Stay Home. Stay Strong. Stay Safe."
        
        let longRange1 = (str as NSString).range(of: att1)
        let longRange2 = (str as NSString).range(of: att2)
        let longRange3 = (str as NSString).range(of: att3)
        let longRange4 = (str as NSString).range(of: att4)
        let longRange5 = (str as NSString).range(of: att5)

        let attributedString = NSMutableAttributedString(string: str, attributes: [NSAttributedStringKey.font : UIFont(name: "CircularStd-Book", size: 13)!])

        attributedString.setAttributes([NSAttributedStringKey.font : UIFont(name: "CircularStd-Medium", size: 13)!, NSAttributedStringKey.foregroundColor : UIColor.darkGray], range: longRange1)
        attributedString.setAttributes([NSAttributedStringKey.font : UIFont(name: "CircularStd-Medium", size: 13)!, NSAttributedStringKey.foregroundColor : UIColor.darkGray], range: longRange2)
        attributedString.setAttributes([NSAttributedStringKey.font : UIFont(name: "CircularStd-Medium", size: 13)!, NSAttributedStringKey.foregroundColor : UIColor.darkGray], range: longRange3)
        attributedString.setAttributes([NSAttributedStringKey.font : UIFont(name: "CircularStd-Medium", size: 13)!, NSAttributedStringKey.foregroundColor : UIColor.darkGray], range: longRange4)
        attributedString.setAttributes([NSAttributedStringKey.font : UIFont(name: "CircularStd-Medium", size: 13)!, NSAttributedStringKey.foregroundColor : UIColor.darkGray], range: longRange5)

        self.labelMsg.attributedText = attributedString
        
    }
}
