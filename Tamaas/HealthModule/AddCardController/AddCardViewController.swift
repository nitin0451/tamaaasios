//
//  AddCardViewController.swift
//  DemoApp
//
//  Created by Apple on 01/04/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit
import FTIndicator
import Stripe

class AddCardViewController: BaseHealthViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var constraintHeightNavigationView: NSLayoutConstraint!
    @IBOutlet weak var textFieldCvv: UITextField!
    @IBOutlet weak var buttonSaveForFuture: UIButton!
    @IBOutlet weak var buttonAddMyCard: UIButton!
    @IBOutlet weak var textFieldMonthYear: UITextField!
    @IBOutlet weak var textFieldCardNo: UITextField!
    
    var paymentIntentClientSecret: String?

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.navigationController == nil {
            self.viewNavigation.isHidden = false
            self.constraintHeightNavigationView.constant = 64
        } else {
            self.viewNavigation.isHidden = true
            self.constraintHeightNavigationView.constant = 0
        }
    }
    
    //MARK:- Button Action
    @IBAction func buttonActionSaveForFuture(_ sender: UIButton) {
        
    }
    
    @IBAction func buttonActionBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func buttonActionAddMyCard(_ sender: UIButton) {
        if textFieldCardNo.text == "" || textFieldCvv.text == "" || textFieldMonthYear.text == "" {
            FTIndicator.showError(withMessage: "Please fill the complete card details.")
        } else {
            self.getTokenIdFromStripe()
        }
        
    }

    internal func getTokenIdFromStripe() {
        
        let cardParams = STPCardParams()
        cardParams.number = textFieldCardNo.text ?? ""
        cardParams.cvc = textFieldCvv.text ?? ""
      
        if let expiryDate = textFieldMonthYear.text {
            let expMonth: UInt = UInt(expiryDate.components(separatedBy: "/")[0])!
            let expYear: UInt = UInt(expiryDate.components(separatedBy: "/")[1])!
            cardParams.expMonth = expMonth
            cardParams.expYear = expYear
        }

        STPAPIClient.shared().createToken(withCard: cardParams) { (token, error) in
            
            if let error = error {
                // show the error to the user
                print(error)
            } else if let tokenStr = token {
                
                print(tokenStr.tokenId)
                print(tokenStr.card!.last4)
                self.saveCard(lastFour: tokenStr.card!.last4)
            }
        }
        
    }
    
    func saveCard(lastFour: String) {
        
        if Connectivity.isConnectedToInternet() {
                 
            self.showLoader(strForMessage: "loading..".localized)
            let webservice  = WebserviceSigleton ()
                 
            let dict: Dictionary<String,Any> = ["userid":SessionManager.getUserId(),"last_four":lastFour,"action": "stripe_cart_create"]
            print(dict)
            webservice.POSTServiceWithParametersNew(urlString: constantVC.WebserviceName.URL_SAVE_FETCH_CARDDETAIL, params: dict as Dictionary<String, AnyObject>, isHeaders: false) { (data, error) in
                
                self.dismissLoader()
                if let msg = data?["msg"] as? String,msg == "success"{
                    if self.navigationController == nil {
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                }
                
            }
            
        }
                
    }
        
}

//MARK:- UITextFieldDelegate
extension AddCardViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let characterSet = NSCharacterSet(charactersIn: "0123456789")
        var enterString = string
       
        enterString = enterString.replacingOccurrences(of: " ", with: "")
        enterString = enterString.replacingOccurrences(of: "/", with: "")
       
        if (enterString as NSString).rangeOfCharacter(from: characterSet.inverted).location != NSNotFound {
           return false
        }
            
        switch textField {
            
        case textFieldCardNo:
            
            var text: String = textField.text!
            text = (text as NSString).replacingCharacters(in: range, with: string)
            text = text.replacingOccurrences(of: " ", with: "")
            var newString: String = ""
            while text.count > 0 {
               let subString: String? = (text as NSString).substring(to: min(text.count, 4))
               newString = newString + (subString!)
               if (subString?.count ?? 0) == 4 {
                   newString = newString + (" ")
               }
               text = (text as NSString).substring(from: min(text.count, 4))
            }
            
            newString = newString.trimmingCharacters(in: characterSet.inverted)
            if newString.count >= 20 {
               return false
            }
            
            textField.text = newString

            return false
            
        case textFieldMonthYear:
            
            var text: String = textField.text!
            text = (text as NSString).replacingCharacters(in: range, with: string)
            text = text.replacingOccurrences(of: "/", with: "")
            var newString: String = ""
            while (text.count ) > 0 {
                let subString: String? = (text as NSString).substring(to: min((text.count ), 2))
                newString = newString + (subString)!
                if (subString!.count ) == 2 {
                    newString = newString + ("/")
                }
                text = (text as NSString).substring(from: min(text.count , 2))
            }
            newString = newString.trimmingCharacters(in: characterSet.inverted)
            
            print("\(newString)")
            if newString.count > 6 {
                return false
            }
            textField.text = newString

            return false
            
        default:
             return string == "" ? true : textField.text!.count < 3 ? true : false;
            
        }
        
    }
    
}
