//
//  NavigationView.swift
//  DemoApp
//
//  Created by Apple on 03/04/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class NavigationView: UIView {

    @IBOutlet weak var buttonBack: UIButton!
    @IBOutlet weak var buttonLocation: UIButton!
    @IBOutlet weak var labelLocation: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        
        let view = UINib(nibName: "NavigationView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
       
    }
}
