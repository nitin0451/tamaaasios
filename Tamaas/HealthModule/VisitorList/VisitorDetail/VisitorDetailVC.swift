//
//  VisitorDetailVC.swift
//  HealthModule
//
//  Created by osvinuser on 13/08/19.
//  Copyright © 2019 Mac-Mini- Nav. All rights reserved.
//

import UIKit

class VisitorDetailVC: BaseHealthViewController, UITextFieldDelegate, UIPickerViewDataSource {

    //MARK:- IBOutlet
    @IBOutlet weak var labelAgreement: UILabel!{
        didSet{
            labelAgreement.text = "By continuing, I agree that I am legally authorized to make medical decisions for the individual.".localized
        }
    }
    @IBOutlet weak var labelPatitentInfo: UILabel!{
        didSet{
            //labelAgreement.text = "By continuing, I agree that I am legally authorized to make medical decisions for the individual.".localized
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var continueBtn: UIButton!{
        didSet{
            continueBtn.setTitle("Continue", for: .normal)
        }
    }
    @IBOutlet weak var agreeBtn: UIButton!
    @IBOutlet weak var dataPickerHeight: NSLayoutConstraint!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var dataPicker: UIPickerView!

    //MARK:- Variables
    var movingFor : String?
    var selectedAge = ""
    var patientName = String()
    var shChild = Bool()
    let dateFormatter: DateFormatter = DateFormatter()
    let yearFormatter: DateFormatter = DateFormatter()
    var type = String()
    var isTick = false
    var selectedColor = UIColor(red: 28/255, green: 87/255, blue: 255/255, alpha: 1.0)
    var unSelectedColor = UIColor.white
    var shMale = false
    var selectedDate: String = "Date of birth".localized
    var currentYear = Int()
    var selectedYear = Int()
    var blurview = UIView()
    var arrChildData = ["0-3 months","4-8 months","9-12 months","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18"]
    var arrParentData = ["45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80","81","82","83","84","85","86","87","88","89","90","91","92","93","94","95","96","97","98","99","100","101","102","103","104","105","106","107","108","109","110","111","112","113","114","115","116","117","118","119","120"]
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.hidesBottomBarWhenPushed = true
        currentYear = Calendar.current.component(.year, from: Date())
        viewToLoad()
    }
    
    func viewToLoad(){
        continueBtn.backgroundColor = unSelectedColor
        continueBtn.setTitleColor(selectedColor, for: .normal)
        agreeBtn.setImage(UIImage(named: "emptyBox"), for: .normal)
        continueBtn.isUserInteractionEnabled = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap_dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tap)
    
        dataPickerHeight.constant = 0
        blurview.frame = self.view.frame
        blurview.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.view.addSubview(blurview)
        blurview.isHidden = true
        blurview.isUserInteractionEnabled = true
        dataPicker.delegate = self
        dataPicker.dataSource = self
     
    }
   
    @IBAction func donePicker(_ sender: Any) {
        dataPickerHeight.constant = 0
        tableView.reloadData()
        blurview.isHidden = true
    }
    
    @objc func dobAction() {
        self.view.endEditing(true)
        dataPicker.reloadAllComponents()
        dataPickerHeight.constant = 200
        blurview.isHidden = false
        view.bringSubview(toFront: datePickerView)
        
    }
    
    @objc func handleTap_dismissKeyboard(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func genderAction() {
        if shMale == true{
            shMale = false
        }else{
            shMale = true
        }
        if self.movingFor == "child" {
            UserDefaults.standard.set(shMale, forKey: "childGender")
        } else if self.movingFor == "parents"{
            UserDefaults.standard.set(shMale, forKey: "parentGender")
        }
      //  UserDefaults.standard.set(shMale, forKey: "shMale")
        tableView.reloadData()
    }
    
    @IBAction func agreeAction(_ sender: Any) {
        if isTick == false{
            isTick = true
            continueBtn.backgroundColor = selectedColor
            continueBtn.setTitleColor(unSelectedColor, for: .normal)
            agreeBtn.setImage(UIImage(named: "tickBox"), for: .normal)
            continueBtn.isUserInteractionEnabled = true
        }
        else{
            isTick = false
            continueBtn.backgroundColor = unSelectedColor
            continueBtn.setTitleColor(selectedColor, for: .normal)
            agreeBtn.setImage(UIImage(named: "emptyBox"), for: .normal)
            continueBtn.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func continueAction(_ sender: Any) {
        if Connectivity.isConnectedToInternet(){
            if selectedDate == "Date of birth" {
                showError(message: "Please Enter Date of birth")
            } else if patientName == "" {
                showError(message: "Please Enter Patient name")
            } else{
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SymptomsViewController") as! SymptomsViewController
                vc.movingFor = movingFor
                self.navigationController?.pushViewController(vc, animated: true)
               // self.navigationController?.popViewController(animated: true)
                //self.performSegue(withIdentifier: "bodyParts", sender: self)
            }
        }else{
            showError(message: "No Internet Connection!")
        }
    
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        patientName = textField.text!
        if shChild {
            UserDefaults.standard.set(patientName, forKey: "childName")
        } else {
            UserDefaults.standard.set(patientName, forKey: "parentsName")
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        patientName = textField.text!
        if shChild {
            UserDefaults.standard.set(patientName, forKey: "childName")
        } else {
            UserDefaults.standard.set(patientName, forKey: "parentsName")
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "bodyParts" {
            
            let viewController:BodyPartVC = segue.destination as! BodyPartVC
            selectedYear = currentYear - Int(selectedAge)!
            viewController.yearOfBirth = Int(selectedYear)
            print(selectedYear)
            if shChild == false{
                if shMale == true{
                     viewController.selector = 1
                }
                else{
                    viewController.selector  = 2
                }
            } else{
                if shMale == true{
                    viewController.selector  = 1
                }
                else{
                    viewController.selector  = 2
                }
            }
        }
    }
}

extension VisitorDetailVC:UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if shChild == true{
            return arrChildData.count
        } else{
            return arrParentData.count
        }
    }
     
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        if shChild == true{
            return arrChildData[row]
        } else{
            return arrParentData[row]
        }
    }
     
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
          
        if shChild == true{
            if row > 2{
                selectedDate = "\(arrChildData[row]) years"
                selectedAge = arrChildData[row]
            } else{
                selectedDate = "\(arrChildData[row])"
                selectedAge = "\(arrChildData[row])"
            }
            UserDefaults.standard.set(selectedAge, forKey: "childAge")
        } else{
            selectedDate = "\(arrParentData[row]) years"
            selectedAge = arrParentData[row]
            UserDefaults.standard.set(selectedAge, forKey: "parentsAge")
        }
     }
}

extension VisitorDetailVC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.size.height/3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        if indexPath.row == 0{
            cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath as IndexPath)
            let nameTf = cell?.contentView.viewWithTag(111) as? UITextField
            nameTf?.autocapitalizationType = .words
            nameTf?.placeholder = "Patitent Name".localized
            nameTf?.delegate = self
            
        } else if indexPath.row == 1{
            
            cell = tableView.dequeueReusableCell(withIdentifier: "genderCell", for: indexPath as IndexPath)
            
            var maleBtn : UIButton?
            var femaleBtn : UIButton?
            var genderLabel : UILabel?
            
            maleBtn = cell?.contentView.viewWithTag(11) as? UIButton
            femaleBtn = cell?.contentView.viewWithTag(22) as? UIButton
            genderLabel = cell?.contentView.viewWithTag(111) as? UILabel

            genderLabel?.text = "Gender".localized
            
            if let maleBtn = maleBtn{
                maleBtn.addTarget(self, action: #selector(genderAction), for: .touchDown)
            }
            if let femaleBtn = femaleBtn{
                femaleBtn.addTarget(self, action: #selector(genderAction), for: .touchDown)
            }
            if shMale == true{
                maleBtn?.setImage(UIImage(named:"manSel"), for: .normal)
                femaleBtn?.setImage(UIImage(named:"womanUnsel"), for: .normal)
            }else{
                maleBtn?.setImage(UIImage(named:"manUnsel"), for: .normal)
                femaleBtn?.setImage(UIImage(named:"womanSel"), for: .normal)
            }
        } else{
            cell = tableView.dequeueReusableCell(withIdentifier: "dobCell", for: indexPath as IndexPath)

            let dobBtn : UIButton = (cell?.contentView.viewWithTag(11) as? UIButton)!
            let dobLabel : UILabel = (cell?.contentView.viewWithTag(111) as? UILabel)!
            dobLabel.text = selectedDate
            dobBtn.addTarget(self, action: #selector(dobAction), for: .touchDown)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
