//
//  VisitorListVC.swift
//  HealthModule
//
//  Created by Mac-Mini- Nav on 12/08/19.
//  Copyright © 2019 Mac-Mini- Nav. All rights reserved.
//

import UIKit

class VisitorListVC: BaseHealthViewController{

    //MARK:- IBOutlet
    @IBOutlet weak var labelBottomMsgText: UILabel!{
        didSet{
            labelBottomMsgText.text = "If the patient is over 18, they need to create their own account, even if they are a dependent".localized
        }
    }
    @IBOutlet weak var labelSomeoneElseText: UILabel!{
        didSet{
            labelSomeoneElseText.text = "Someone Else?".localized
        }
    }
    @IBOutlet weak var labelWhoIsThisText: UILabel!{
        didSet{
            labelWhoIsThisText.text = "Who is this visit for?".localized
        }
    }
    @IBOutlet weak var tableView: UITableView!

    //MARK:- Variables
    var shChild = Bool()
    let defaults = UserDefaults.standard
    var currentYear = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.hidesBottomBarWhenPushed = true
        currentYear = Calendar.current.component(.year, from: Date())
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "visitorDetail" {
            let viewController:VisitorDetailVC = segue.destination as! VisitorDetailVC
            viewController.shChild = shChild
        } else if segue.identifier == "bodyParts"{
            let viewController:BodyPartVC = segue.destination as! BodyPartVC
            viewController.yearOfBirth = defaults.object(forKey: "selected_year") as! Int
            viewController.selector = defaults.object(forKey: "selector") as! Int
        }
    }
    
}

//MARK:- UITableViewDelegate , UITableViewDataSource
extension VisitorListVC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?	
        var imageView: UIImageView?
        var titleLabel : UILabel?
        var descLabel : UILabel?
        cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath as IndexPath)
        cell?.selectionStyle = .none
        imageView = cell?.contentView.viewWithTag(11) as? UIImageView
        titleLabel = cell?.contentView.viewWithTag(22) as? UILabel
        descLabel = cell?.contentView.viewWithTag(33) as? UILabel
        
        if let imageView = imageView {
            if indexPath.section == 0{
                let str_userImageURl = constantVC.GeneralConstants.ImageUrl + SessionManager.getPhone() + ".jpeg"
                imageView.yy_setImage(with: URL.init(string: str_userImageURl) , placeholder: UIImage(named:"contact_big") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
                imageView.isHidden = false
            }else{
                 imageView.isHidden = true
            }
        }

        if let titleLabel = titleLabel{
            if indexPath.section == 0{
                titleLabel.text = SessionManager.getUsername()
            }else if indexPath.section == 1{
                titleLabel.text = "My Child".localized
//                if let value = defaults.value(forKey: "childName") as? String {
//                    titleLabel.text = value
//                } else {
//                    titleLabel.text = "My Child"
//                }
            }else{
                titleLabel.text = "My Parents".localized

//                if let value = defaults.value(forKey: "parentsName") as? String {
//                    titleLabel.text = value
//                } else {
//                    titleLabel.text = "My Parents"
//                }
            }
        }
        
        if let descLabel = descLabel{
            if indexPath.section == 0{
                descLabel.text = ""
            }else if indexPath.section == 1{
//                if let value = defaults.value(forKey: "childAge") as? String {
//                    descLabel.text = value
//                } else {
//                    descLabel.text = "Must be under 18"
//                }
                descLabel.text = "Must be under 18".localized
            }else{
                descLabel.text = "Must be elderly parents who can not navigate the platform on their own.".localized

//                if let value = defaults.value(forKey: "parentsAge") as? String {
//                    descLabel.text = value
//                } else {
//                    descLabel.text = "Must be elderly parents who can not navigate the platform on their own"
//                }
            }
        }
       
       return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 {
            if defaults.object(forKey: "selected_year") == nil{
                self.performSegue(withIdentifier: "selfDetail", sender: self)
            } else{
                self.moveToSymptonsController(movingFor: "self")
            }
        } else if indexPath.section == 1 {
//            if let _ = defaults.value(forKey: "childName") as? String {
//                self.moveToSymptonsController(movingFor: "child")
//            } else {
//                self.moveToVisitorDetail(movingFor: "child", isChild: true)
//            }
            self.moveToVisitorDetail(movingFor: "child", isChild: true)

            
        } else if indexPath.section == 2 {
//            if let _ = defaults.value(forKey: "parentsName") as? String {
//                self.moveToSymptonsController(movingFor: "parents")
//            } else {
//                self.moveToVisitorDetail(movingFor: "parents", isChild: false)
//            }
            
            self.moveToVisitorDetail(movingFor: "parents", isChild: false)

        }
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SymptomsViewController") as! SymptomsViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
        
//        if Connectivity.isConnectedToInternet(){
//          if indexPath.row == 0{
//            if defaults.object(forKey: "selected_year") == nil{
//                self.performSegue(withIdentifier: "selfDetail", sender: self)
//            } else{
//                self.performSegue(withIdentifier: "bodyParts", sender: self)
//            }
//          } else{
//            shChild = indexPath.row == 1 ? true : false
//            self.performSegue(withIdentifier: "visitorDetail", sender: self)
//          }
//        } else{
//            showError(message: "No Internet Connection!")
//        }
    }
    
    func moveToSymptonsController(movingFor: String) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SymptomsViewController") as! SymptomsViewController
        vc.movingFor = movingFor
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func moveToVisitorDetail(movingFor: String, isChild: Bool) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VisitorDetailVC") as! VisitorDetailVC
        vc.shChild = isChild
        vc.movingFor = movingFor
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
