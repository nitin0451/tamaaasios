//
//  VisitorDetailVC.swift
//  HealthModule
//
//  Created by Mac-Mini- Nav on 12/08/19.
//  Copyright © 2019 Mac-Mini- Nav. All rights reserved.
//

import UIKit
import DropDown

class SelfDetailVC: BaseHealthViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var labelPatientInfo: UITableView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Variables
    var shMale = false
    var selectedAge = "25"
    let dropDownSingle = DropDown()
    var selectedYear = Int()
    var currentYear = Int()
    let defaults = UserDefaults.standard
    var ageArr = ["16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","66","57","58","59","60","61","62","63","64","65","66","67","68","69","70","71","72","73","74","75","76","77","78","79","80"]
    
    //MARK:- Variables
    override func viewDidLoad() {
        super.viewDidLoad()
        currentYear = Calendar.current.component(.year, from: Date())
         self.hidesBottomBarWhenPushed = true
    }
   
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        if defaults.object(forKey: "selected_year") == nil{
            selectedYear = currentYear - Int(selectedAge)!
            defaults.set(selectedYear, forKey: "selected_year")
            defaults.set(selectedAge, forKey: "selfAge")
        }
        if shMale == true{
            defaults.set(1, forKey: "selector")
        } else{
            defaults.set(2, forKey: "selector")
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SymptomsViewController") as! SymptomsViewController
        vc.movingFor = "self"
        self.navigationController?.pushViewController(vc, animated: true)
        
//        self.navigationController?.popViewController(animated: true)

//        self.navigationController?.popViewController(animated: true)
//        if Connectivity.isConnectedToInternet(){
//            self.performSegue(withIdentifier: "bodyParts", sender: self)
//        }else{
//            showError(message: "No Internet Connection!")
//        }
     
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "bodyParts" {
//            let viewController:BodyPartVC = segue.destination as! BodyPartVC
//
        if defaults.object(forKey: "selected_year") == nil{
            selectedYear = currentYear - Int(selectedAge)!
            defaults.set(selectedYear, forKey: "selected_year")
        }
        if shMale == true{
            defaults.set(1, forKey: "selector")
        } else{
            defaults.set(2, forKey: "selector")
        }
        self.navigationController?.popViewController(animated: true)
            
//        viewController.yearOfBirth = selectedYear
//            if shMale == true{
//                defaults.set(1, forKey: "selector")
//                viewController.selector = 1
//            } else{
//                defaults.set(2, forKey: "selector")
//                viewController.selector = 2
//            }
        }
    }
    
    @objc func ageAction(sender: UIButton) {
        self.dropDownSingle.backgroundColor = UIColor.white
        self.dropDownSingle.dismissMode  = .automatic
        dropDownSingle.selectionBackgroundColor = UIColor(red: 234/255, green: 245/255, blue: 255/255, alpha: 1.0)
            dropDownSingle.anchorView = sender
            dropDownSingle.dataSource = ageArr
            dropDownSingle.width = sender.frame.size.width
            dropDownSingle.selectionBackgroundColor = UIColor.clear
     
        dropDownSingle.selectionAction = { [unowned self] (index: Int, item: String) in
            self.selectedAge = item
            self.tableView.reloadData()
        }
         self.dropDownSingle.show()
    }
    
    @objc func genderAction() {
        
        if shMale == true{
            shMale = false
        }else{
            shMale = true
        }
        tableView.reloadData()
    }
}

extension SelfDetailVC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.size.height/3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        var maleBtn : UIButton?
        var femaleBtn : UIButton?
        if indexPath.row == 0{
             cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath as IndexPath)
            
            let ageLabel : UILabel = (cell?.contentView.viewWithTag(22) as? UILabel)!
            let ageBtn   : UIButton = (cell?.contentView.viewWithTag(33) as? UIButton)!
            
            ageLabel.text = self.selectedAge
            ageBtn.addTarget(self, action: #selector(ageAction(sender:)), for: .touchDown)
          
        }
        else{
             cell = tableView.dequeueReusableCell(withIdentifier: "genderCell", for: indexPath as IndexPath)
            maleBtn = cell?.contentView.viewWithTag(11) as? UIButton
            femaleBtn = cell?.contentView.viewWithTag(22) as? UIButton
             if let maleBtn = maleBtn{
                maleBtn.addTarget(self, action: #selector(genderAction), for: .touchDown)
            }
            if let femaleBtn = femaleBtn{
                 femaleBtn.addTarget(self, action: #selector(genderAction), for: .touchDown)
            }
            if shMale == true{
                maleBtn?.setImage(UIImage(named:"manSel"), for: .normal)
                femaleBtn?.setImage(UIImage(named:"womanUnsel"), for: .normal)
            }else{
                maleBtn?.setImage(UIImage(named:"manUnsel"), for: .normal)
                femaleBtn?.setImage(UIImage(named:"womanSel"), for: .normal)
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)  
    }

}
