
import UIKit
import DropDown
import IQKeyboardManager

class BodyPartVC: BaseHealthViewController, UISearchBarDelegate {
    
    //MARK:- IBOutlet
    @IBOutlet weak var labelHeadingText: UILabel!{
        didSet{
            labelDoYourSkinText.text = "What's your reason of your visit".localized
        }
    }
    @IBOutlet weak var labelDoYourSkinText: UILabel!{
        didSet{
            labelDoYourSkinText.text = "Do you have any Skin, Joints & General symptoms?".localized
        }
    }
    @IBOutlet weak var labelSelectBodyArea: UILabel!{
        didSet{
            //labelDoYourSkinText.text = "".localized
        }
    }
    @IBOutlet weak var labelHeadingMessage: UILabel!{
        didSet{
           // labelDoYourSkinText.text = "What's your reason of your visit".localized
        }
    }
    @IBOutlet weak var diagnosisHeight: NSLayoutConstraint!
    @IBOutlet weak var diagnosisView: UIView!
    @IBOutlet weak var diagnosisTable: UITableView!
    @IBOutlet weak var subview: UIView!
    @IBOutlet weak var subViewWidth: NSLayoutConstraint!
    @IBOutlet weak var selectedSymptView: UIView!
    @IBOutlet weak var proposedSympTableView: UITableView!
    @IBOutlet weak var proposedViewHeight: NSLayoutConstraint!{
        didSet{
            proposedViewHeight.constant = 0
        }
    }
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var tableView: ExpyTableView!{
        didSet{
            tableView.expandingAnimation = .fade
            tableView.collapsingAnimation = .fade
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.estimatedRowHeight = 44
            tableView.tableFooterView = UIView()
        }
    }
    @IBOutlet weak var subTableViewHeight: NSLayoutConstraint!{
        didSet{
            subTableViewHeight.constant = 0
        }
    }
    @IBOutlet weak var subTableView: UIView!
    @IBOutlet weak var searchBorderView: UIView!
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet{
            searchBar.delegate = self
            searchBar.placeholder = "Search your symptoms here..."
            searchBar.backgroundImage = UIImage()
        }
    }
    
    //MARK:- Variables
    var arrBodyId = [Int: String]()
    var arrSubParts = [Int: [HealthItem]]()
    let dropDown = DropDown()
    var issuesArr = [HealthItem]()
    var proposedArr = [HealthItem]()
    var filteredArr = [HealthItem]()
    var filteredStrArr = [String]()
    var arrHealthSymptoms = [Int: [HealthSymptomSelector]]()
    var arrDiagnosis = [HealthDiagnosis]()
    var locationId = Int()
    var selectedIndexPath: IndexPath?
    var selector = Int()
    var blurview = UIView()
    var selectionArray = [IndexPath]()
    var yearOfBirth = Int()
    var arrSymptoms = NSMutableArray()
    var arrSymptomName = NSMutableArray()
    var activityIndicatorView: UIActivityIndicatorView!
    var productTags: TKCollectionView!
    var issueInfo = HealthIssueInfo()
    
    //Nitin
    var symptomsMessage = "" 
    var movingFor: String?

     //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
   
        self.showTagView()
        initClient()
        loadSymptoms()
        dropDown.anchorView = searchBorderView
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
        self.arrSymptomName.add(self.filteredArr[index].name)
        self.arrSymptoms.add(self.filteredArr[index].id)

        if Connectivity.isConnectedToInternet(){
            GIFHUD.shared.show(withOverlay: true)
            self.client.loadDiagnosis({_, _, healthDiagnosis in
                    
                self.arrDiagnosis = healthDiagnosis as! [HealthDiagnosis]
                    
                if self.arrDiagnosis.count != 0{
                    self.diagnosisHeight.constant = self.view.frame.size.height - 100
                    self.diagnosisTable.reloadData()
                }else{
                    self.showError(message: "Result not found")
                }
                    
                    GIFHUD.shared.dismiss()
               }, selectedSymptoms: self.arrSymptoms, gender: Int32(self.selector), yearOfBirth: Int32(self.yearOfBirth))
          }else{
               self.showError(message: "No Internet Connection!")
          }
     }
         dropDown.direction = .bottom
//          let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap_dismissKeyboard(_:)))
//          self.view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnabled = false
        IQKeyboardManager.shared().isEnableAutoToolbar = false
    }
    
    override func viewWillLayoutSubviews() {
        subViewWidth.constant = subview.frame.size.height * 0.5
    }
    
    func initClient(){

        blurview.frame = self.view.frame
        blurview.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        self.view.addSubview(blurview)
        blurview.isHidden = true
        blurview.isUserInteractionEnabled = true
        GIFHUD.shared.hudSize = CGSize(width: self.view.frame.size.width, height: 340)
        GIFHUD.shared.setGif(named: "diagnosis.gif")
     
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        tableView.backgroundView = activityIndicatorView
        
    }
    
    func filter(searchText: String) -> [HealthItem] {
        filteredArr.removeAll()
        filteredStrArr = []
        issuesArr.forEach({ (HealthItem) -> () in
            if HealthItem.name.count >= searchText.count {
                let result = HealthItem.name.compare(searchText, options: [.caseInsensitive, .diacriticInsensitive], range: searchText.startIndex ..< searchText.endIndex)
                    if result == .orderedSame {
                        filteredArr.append(HealthItem)
                        filteredStrArr.append(HealthItem.name)
                    }
                }
            })
          return filteredArr
     }
     
    func startAnimating(){
        activityIndicatorView.startAnimating()
    }
    
    func stopAnimating(){
        activityIndicatorView.stopAnimating()
    }
    
    @IBAction func hideSymptomViewAc(_ sender: Any) {
        subTableViewHeight.constant = 0
        blurview.isHidden = true
    }
    
    func hideProposedViewAc() {
        proposedViewHeight.constant = 0
    }
    
    func loadSymptoms(){
        if Connectivity.isConnectedToInternet(){
               client.loadIssues({_, _, bodyParts in
                    self.issuesArr = bodyParts as! [HealthItem]
               })
          }
    }
    
    @objc func handleTap_dismissKeyboard(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
     
    func getSubBodyParts(){
        view.endEditing(true)
        if Connectivity.isConnectedToInternet(){
            tableView.reloadData()
            self.startAnimating()
            self.updateTableView()
            arrSymptoms = []
            arrSymptomName = []
            selectionArray = []
            if arrSubParts[self.locationId] == nil{
               client.loadBodySubLocations({ _, _, bodyParts in
                    self.arrSubParts[self.locationId] = bodyParts as! [HealthItem]
                    for i in 0..<bodyParts!.count  {
                         if self.arrHealthSymptoms[self.arrSubParts[self.locationId]![i].id] == nil{
                              let subLocationId = self.arrSubParts[self.locationId]![i].id
                              self.client.loadSubLocationSymptoms({ _, _, bodyParts in
                                   
                                   self.arrHealthSymptoms[subLocationId] = bodyParts as! [HealthSymptomSelector]
                                  
                                   if i == self.arrSubParts[self.locationId]!.count - 1{
                                        
                                        self.stopAnimating()
                                   }
                                   self.tableView.reloadData()
                              }, locationId: Int32(subLocationId), selectedSelectorStatus: Int32(self.selector))
                         }
                         else{
                              self.updateTableView()
                              self.stopAnimating()
                         }
                    }
               }, bodyLocationId: Int32(self.locationId))
            } else{
               self.updateTableView()
               tableView.reloadData()
               self.stopAnimating()
            }
        }else{
          showError(message: "No Internet Connection!")
        }
     
    }
    
    func updateTableView(){
        subTableViewHeight.constant = self.view.frame.size.height * 3/4
        blurview.isHidden = false
        view.bringSubview(toFront: subTableView)
        self.view.layoutIfNeeded()
    }

    @IBAction func backAction(_ sender: Any) {
        if diagnosisHeight.constant != 0{
            self.diagnosisHeight.constant = 0
        } else if proposedViewHeight.constant != 0{
            proposedViewHeight.constant = 0
        } else{
            self.navigationController?.popViewController(animated: true)
            hideSymptomViewAc(self)
        }
    }
    
    // MARK :- BodyActions
    @IBAction func headAction(_ sender: Any) {
        locationId = 6
//        self.titleLab.text = self.arrBodyId[6]
        self.titleLab.text = "Head, throat & neck"
        selectedIndexPath = IndexPath(row: 0, section: 0)
        getSubBodyParts()
    }
    
    @IBAction func chestAction(_ sender: Any) {
        locationId = 15
//        self.titleLab.text = self.arrBodyId[15]
        self.titleLab.text = "Chest & back"
        selectedIndexPath = IndexPath(row: 0, section: 0)
        getSubBodyParts()
    }
    
    @IBAction func lowerBodyAction(_ sender: Any) {
        locationId = 16
       // self.titleLab.text = self.arrBodyId[16]
        selectedIndexPath = IndexPath(row: 0, section: 0)
        self.titleLab.text = "Abdomen, pelvis & buttocks"
        getSubBodyParts()
    }
    
    @IBAction func legsAction(_ sender: Any) {
        locationId = 10
//        self.titleLab.text = self.arrBodyId[10]
        self.titleLab.text = "Legs"
        selectedIndexPath = IndexPath(row: 0, section: 0)
        getSubBodyParts()
    }
    
    @IBAction func rightHandAction(_ sender: Any) {
        locationId = 7
//        self.titleLab.text = self.arrBodyId[7]
        self.titleLab.text = "Arms & shoulder"
        selectedIndexPath = IndexPath(row: 0, section: 0)
        getSubBodyParts()
    }
    
    @IBAction func leftHandAction(_ sender: Any) {
        locationId = 7
        //      self.titleLab.text = self.arrBodyId[7]
        self.titleLab.text = "Arms & shoulder"
        selectedIndexPath = IndexPath(row: 0, section: 0)
        getSubBodyParts()
    }
    
    @IBAction func skinAction(_ sender: Any) {
        locationId = 17
        //      self.titleLab.text = self.arrBodyId[17]
        self.titleLab.text = "Skin, joints & general"
        selectedIndexPath = IndexPath(row: 0, section: 0)
        getSubBodyParts()
    }
    
    func loadProposedSymptoms(){
        productTags.tags = arrSymptomName as! [String]
        productTags.tagsCollectionView.reloadData()
        if Connectivity.isConnectedToInternet(){
              // GIFHUD.shared.show(withOverlay: true)
        client.loadProposedSymptoms({_, _, bodyParts in
              
            self.proposedArr = bodyParts as! [HealthItem]
            self.proposedViewHeight.constant = self.view.frame.size.height - 100
              
            self.proposedSympTableView.reloadData()

               // GIFHUD.shared.dismiss()
            }, selectedSymptoms: arrSymptoms, gender: Int32(selector), yearOfBirth: Int32(yearOfBirth))
        } else{
            showError(message: "No Internet Connection!")
        }
    
    }
    
    @IBAction func continueTapped(_ sender: UIButton) {
        hideSymptomViewAc(self)
        loadProposedSymptoms()
     
    }
    
    func showTagView(){
          
        productTags = TKCollectionView(tags:arrSymptomName as! [String],
                                         action: .removeTag,
                                         receiver: nil)
          // Set the current controller as the delegate of both collections
        productTags.delegate = self
          // Set the sender and receiver of the TextField
        add(productTags, toView: selectedSymptView)
     }
     
     @IBAction func proposedDiagnosisAc(_ sender: Any) {
        hideProposedViewAc()
        var symptoms = ""
        for name in arrSymptomName {
            let nn = name as! String
            symptoms = symptoms + "," + nn
        }
        symptoms = symptoms.deletingPrefix(",")
        let msg = "\(String(describing: self.symptomsMessage)) \n 3. Selected symptoms are : \(symptoms) "

        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DoctorListingViewController") as! DoctorListingViewController
        vc.symptomsMessage = msg
        self.navigationController?.pushViewController(vc, animated: true)
//          if Connectivity.isConnectedToInternet(){
//               GIFHUD.shared.show(withOverlay: true)
//               client.loadDiagnosis({_, _, healthDiagnosis in
//                    self.arrDiagnosis = healthDiagnosis as! [HealthDiagnosis]
//                    if self.arrDiagnosis.count != 0{
//                         self.diagnosisHeight.constant = self.view.frame.size.height - 100
//                         self.diagnosisTable.reloadData()
//                    }else{
//                         self.showError(message: "Result not found")
//                    }
//
//                    GIFHUD.shared.dismiss()
//               }, selectedSymptoms: arrSymptoms, gender: Int32(selector), yearOfBirth: Int32(yearOfBirth))
//          }else{
//               showError(message: "No Internet Connection!")
//          }
          
     }
     
    @objc func stopProgress() {
        GIFHUD.shared.dismiss()
    }
    
    @objc func symptomSelectionAc(indexPath:IndexPath){
    
        let subLocationId = self.arrSubParts[self.locationId]![indexPath.section].id
        if selectionArray.contains(indexPath){
            let index_ = selectionArray.index(of: indexPath)
            selectionArray.remove(at: index_!)
          
            let symptomsId = self.arrHealthSymptoms[subLocationId]![indexPath.row].id
            let symptomName = self.arrHealthSymptoms[subLocationId]![indexPath.row].name
            arrSymptoms.remove(Int32(symptomsId!))
            arrSymptomName.remove(symptomName)
        } else{
            let symptomsId = self.arrHealthSymptoms[subLocationId]![indexPath.row].id
            let symptomName = self.arrHealthSymptoms[subLocationId]![indexPath.row].name
            arrSymptoms.add(Int32(symptomsId!))
            arrSymptomName.add(symptomName)
            selectionArray.append(indexPath)
        }
        
        tableView.reloadData()
        
    }
    
    @objc func loadInfo(sender: UIButton) {
        if Connectivity.isConnectedToInternet(){
            let healthItem = arrDiagnosis[sender.tag].issue
          
            if let iD_ = healthItem!["ID"]{
               client.loadIssueInfo({_, _, healthIssueInfo in
                    self.issueInfo = healthIssueInfo!
                    self.performSegue(withIdentifier: "issueInfo", sender: self)
               }, issueId: iD_ as! Int32)
            }
        }else{
               showError(message: "No Internet Connection!")
          }
     }
    
     @objc func loadTags(sender: UIButton) {
          arrSymptomName.add(proposedArr[sender.tag].name)
          arrSymptoms.add(proposedArr[sender.tag].id)
          proposedArr.remove(at: sender.tag)
          productTags.tags = arrSymptomName as! [String]
          productTags.tagsCollectionView.reloadData()
          proposedSympTableView.reloadData()
     }
     
    func setSelectionImage(image: String , imageView: UIImageView){
        imageView.image = UIImage(named: image)
    }
     
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let issueInfoVC = segue.destination as? IssueInfoVC else { return }
        issueInfoVC.issueInfo = self.issueInfo
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != ""{
            filter(searchText: searchText)
            dropDown.dataSource = filteredStrArr
            dropDown.reloadAllComponents()
            dropDown.show()
        }
         
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        dropDown.hide()
    }
    
}

extension BodyPartVC: TKCollectionViewDelegate {
     
     func tagIsBeingAdded(name: String?) {
          print("added \(name!)")
     }
     
     func tagIsBeingRemoved(name: String?) {
          print("removed \(name!)")
          
        let index_ = arrSymptomName.index(of: name)
        let healthItem = HealthItem()
        healthItem.id = arrSymptoms[index_] as! Int
        healthItem.name = arrSymptomName[index_] as! String
        proposedArr.append(healthItem)
        arrSymptomName.removeObject(at: index_)
        arrSymptoms.removeObject(at: index_)
        proposedSympTableView.reloadData()
     }
    
}

extension BodyPartVC : UITableViewDataSource{
    // MARK: TableView Delegate
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if tableView.tag == 22{
            let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 150))
            let label = UILabel(frame: CGRect(x: 5, y: 5, width: tableView.frame.size.width - 10, height: 145))
            label.text = "This is not a medical diagnosis. These are possible diagnoses based on your symptom patterns. The following list of doctor specialists are recommended practitioners to help consult. If you think you may have a medical, life-threatening emergency, you need to  immediately contact 911.".localized
            label.numberOfLines = 0
            label.font = UIFont(name: "ProximaNova-Regular", size: 11)
            label.contentMode = .center
            customView.backgroundColor = UIColor(red: 252/255, green: 248/255, blue: 227/255, alpha: 1.0)
            
            customView.addSubview(label)
            return customView
        }
        else{
            let customView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0))
          
            return customView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView.tag == 22{
            return 150.0
        }
        return 0.1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
     if tableView.tag == 11{
          print(arrSubParts[self.locationId])
          if arrSubParts[self.locationId] == nil{
               return 0
          }
          
          print(arrSubParts[self.locationId]!.count)
          return arrSubParts[self.locationId]!.count
     }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 22{
            return self.arrDiagnosis.count
        }
        else if tableView.tag == 44{
          return self.proposedArr.count
        }
        else{
          print(self.arrHealthSymptoms)
          let subLocationId = self.arrSubParts[self.locationId]![section].id
            if self.arrHealthSymptoms[subLocationId] != nil{
               print(arrHealthSymptoms[subLocationId])
                return self.arrHealthSymptoms[subLocationId]!.count
            }
     }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 22 {
            return 135
        }
        else{
         return UITableViewAutomaticDimension
        }
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView.tag == 22{
            
           let cell = tableView.dequeueReusableCell(withIdentifier: "diagnosisCell") as! DiagnosisCell
           
            let titleLabel : UILabel = (cell.contentView.viewWithTag(11) as? UILabel)!
            let accuracyProgress : UIProgressView = ((cell.contentView.viewWithTag(22) as? UIProgressView)!)
            cell.infoBtn.tag = indexPath.row
            cell.infoBtn.addTarget(self, action: #selector(loadInfo(sender:)), for: .touchUpInside)
          
              let healthItem = arrDiagnosis[indexPath.row].issue
             
            if let accuracy = healthItem!["Accuracy"]{
               
                print((accuracy as? NSNumber)?.floatValue ?? 0)
                accuracyProgress.setProgress((accuracy as? NSNumber)!.floatValue/100.0, animated: false)
            }
            if let profName = healthItem!["ProfName"]{
                titleLabel.text = profName as? String
            }
            
            cell.selectionStyle = .none
            return cell
            
        }
        else if tableView.tag == 44{
           let cell = tableView.dequeueReusableCell(withIdentifier: "ProposedTVCell") as! ProposedTVCell
          cell.nameLabel.text = proposedArr[indexPath.row].name
          cell.addBtn.tag = indexPath.row
          cell.addBtn.addTarget(self, action: #selector(loadTags(sender:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
          return cell
        }
        else{

            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SpecificationTableViewCell.self)) as! SpecificationTableViewCell
            let subLocationId = self.arrSubParts[self.locationId]![indexPath.section].id
          
               cell.labelSpecification.text = self.arrHealthSymptoms[subLocationId]![indexPath.row].name
               print(self.arrHealthSymptoms[subLocationId]![indexPath.row].name)
               cell.layoutMargins = UIEdgeInsets.zero
               cell.hideSeparator()
          
           if selectionArray.contains(indexPath){
               cell.accessView.backgroundColor = UIColor(red: 28.0/255.0, green: 87.0/255.0, blue: 255.0/255.0, alpha: 1.0)
               cell.accessView.layer.borderWidth = 0
              
           }
           else{
              cell.accessView.backgroundColor = UIColor.clear
              cell.accessView.layer.borderWidth = 0.3
            }
                cell.selectionStyle = .none

               return cell
        }
    }
}
extension BodyPartVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     if tableView.tag != 44{
          symptomSelectionAc(indexPath: indexPath)
     }
         tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension BodyPartVC: ExpyTableViewDataSource {
     
     func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
          return true
     }
     
     func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PhoneNameTableViewCell.self)) as! PhoneNameTableViewCell
          cell.labelPhoneName.text = self.arrSubParts[self.locationId]![section].name
          cell.layoutMargins = UIEdgeInsets.zero
          cell.showSeparator()
            cell.selectionStyle = .none

          return cell
     }
}

//MARK: ExpyTableView delegate methods
extension BodyPartVC: ExpyTableViewDelegate {
     func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
          
          switch state {
          case .willExpand:
               print("WILL EXPAND")
          case .willCollapse:
               print("WILL COLLAPSE")
               
          case .didExpand:
               print("DID EXPAND")
               
          case .didCollapse:
               print("DID COLLAPSE")
          }
     }
}
