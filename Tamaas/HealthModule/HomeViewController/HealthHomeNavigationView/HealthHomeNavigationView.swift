//
//  HealthHomeNavigationView.swift
//  Tamaas
//
//  Created by Apple on 07/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class HealthHomeNavigationView: UIView {

    @IBOutlet weak var buttonEmergency: UIButton!
    
    override init(frame: CGRect) {
          super.init(frame: frame)
          loadViewFromNib()
      }
      
      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          loadViewFromNib()
      }
      
      func loadViewFromNib() {
          
          let view = UINib(nibName: "HealthHomeNavigationView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
          view.frame = bounds
          view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
          self.addSubview(view);
         
      }
}
