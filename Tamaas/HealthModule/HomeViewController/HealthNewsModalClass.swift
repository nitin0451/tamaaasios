//
//  HealthNewsModalClass.swift
//  Tamaas
//
//  Created by Apple on 13/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import Foundation
import ObjectMapper

class HealthNewsModalClass : Mappable {

    var articles : [ArticleModalClass]?
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    // Mappable
    open func mapping(map: Map) {
        articles        <- map["articles"]
    }
}


class ArticleModalClass : Mappable {

    var author : String?
    var content : String?
    var descriptionText : String?
    var publishedAt : String?
    var title : String?
    var url : String?
    var urlToImage : String?

    required init?(map: Map) {
        mapping(map: map)
    }
    
    open func mapping(map: Map) {
        author              <- map["author"]
        descriptionText     <- map["description"]
        content             <- map["content"]
        publishedAt         <- map["publishedAt"]
        title               <- map["title"]
        url                 <- map["url"]
        urlToImage          <- map["urlToImage"]

    }

}
