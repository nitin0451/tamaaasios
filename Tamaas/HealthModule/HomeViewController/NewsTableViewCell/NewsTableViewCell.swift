//
//  NewsTableViewCell.swift
//  Tamaas
//
//  Created by Apple on 12/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewNews: UIImageView!
    @IBOutlet weak var labelNews: UILabel!
    @IBOutlet weak var labelHeading: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
