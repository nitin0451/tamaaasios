//
//  HealthHomeVC.swift
//  HealthModule
//
//  Created by Mac-Mini- Nav on 09/08/19.
//  Copyright © 2019 Mac-Mini- Nav. All rights reserved.
//

import UIKit
import AANotifier
import DropDown
import CoreLocation
import ObjectMapper

class LocationManager: NSObject {
     
     // - Private
     private let locationManager = CLLocationManager()
     
     // - API
     public var exposedLocation: CLLocation? {
          return self.locationManager.location
     }
     
     override init() {
          super.init()
          self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
          self.locationManager.requestWhenInUseAuthorization()
     }
}
extension LocationManager {
     
     func getPlace(for location: CLLocation,
                   completion: @escaping (CLPlacemark?) -> Void) {
          
          let geocoder = CLGeocoder()
          geocoder.reverseGeocodeLocation(location) { placemarks, error in
               
               guard error == nil else {
                    print("*** Error in \(#function): \(error!.localizedDescription)")
                    completion(nil)
                    return
               }
               
               guard let placemark = placemarks?[0] else {
                    print("*** Error in \(#function): placemark is nil")
                    completion(nil)
                    return
               }
               
               completion(placemark)
          }
     }
}

class HealthHomeVC: BaseHealthViewController{
    
    //MARK:- IBOutlet
    @IBOutlet weak var labelWantToConsultText: UILabel! {
        didSet{
            labelWantToConsultText.text = "Want to consult with a doctor right now?".localized
        }
    }
    @IBOutlet weak var labelLatestNewsText: UILabel!{
        didSet{
            labelLatestNewsText.text = "Latest Health News".localized
        }
    }
    @IBOutlet weak var labelStayHomeText: UILabel!{
        didSet{
            labelStayHomeText.text = "Stay home, stay safe".localized
        }
    }
    @IBOutlet weak var labelConsultOnlineText: UILabel!{
        didSet{
            labelConsultOnlineText.text = "Consult online\nwith top doctors".localized
        }
    }
    @IBOutlet weak var buttonStart: UIButton!{
        didSet{
            buttonStart.setTitle("START".localized, for: .normal)
        }
    }
    @IBOutlet weak var labelSymMessageText: UILabel!{
        didSet{
            labelSymMessageText.text = "Use symptom checker to provide you with information that will help determine possible diagnosis.".localized
        }
    }
    @IBOutlet weak var labelSymCheckerText: UILabel!{
        didSet{
            labelSymCheckerText.text = "Symptom Checker".localized
        }
    }
    
    @IBOutlet weak var buttonChatNow: UIButton!{
        didSet {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 1.5
            let yourAttributes: [NSAttributedString.Key: Any] = [
                .font: UIFont(name: "CircularStd-Medium", size: 14.7) ?? UIFont.systemFont(ofSize: 14.7),
                .foregroundColor: UIColor(red: 28.0/255.0, green: 87.0/255.0, blue: 255.0/255.0, alpha: 1),
                .underlineStyle: NSUnderlineStyle.styleSingle.rawValue,NSAttributedString.Key.paragraphStyle: paragraphStyle]
            let attributeString = NSMutableAttributedString(string: "Chat Now".localized,
                                                                attributes: yourAttributes)
            buttonChatNow.setAttributedTitle(attributeString, for: .normal)
        }
    }
    @IBOutlet weak var constraintTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.estimatedRowHeight = 120
            tableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsTableViewCell")
        }
    }
    
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var emergencyTitleBar: UIBarButtonItem!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    private let locationManager = LocationManager()
    private var healthNewsArray : HealthNewsModalClass? {
        didSet{
            if healthNewsArray?.articles?.count ?? 0 > 0 {
                self.tableView.isHidden = false
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                        
                    var totalHeight: CGFloat = 0.0
                    let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! NewsTableViewCell
                    let height = cell.contentView.systemLayoutSizeFitting(UILayoutFittingExpandedSize).height + 1.0
                    totalHeight = height
                    let cell1 = self.tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! NewsTableViewCell
                    let height1 = cell1.contentView.systemLayoutSizeFitting(UILayoutFittingExpandedSize).height + 1.0
                    totalHeight = totalHeight + height1
                    self.constraintTableViewHeight.constant = totalHeight + 50
                    
                    self.view.layoutSubviews()
                    self.view.layoutIfNeeded()
                }
            } else {
                self.constraintTableViewHeight.constant = 0
                self.view.layoutSubviews()
                self.view.layoutIfNeeded()
                self.tableView.isHidden = true
            }
        }
    }

    lazy var HealthMainPopup: AANotifier = {
        
        let notifierView = UIView.fromNib(nibName: "HealthMainPopup")!
        let options: [AANotifierOptions] = [
            .position(.top),
            .preferedHeight(self.view.frame.size.height + 64),
            .margins(H: 0, V: 0),
            .transitionA(.fromTop,0.40),
            .transitionB(.toBottom,0.40),
//            .hideOnTap(false)
        ]
       
        let lblEmergencyText = notifierView.viewWithTag(99) as! UILabel
        lblEmergencyText.text = "Is this an emergency?".localized
        
        let msgLbl = notifierView.viewWithTag(100) as! UILabel
        let line1 = "Stop and call 911 or 119 if you are experiencing:".localized
        let line2 = "Severe, constant chest-pain or pressure".localized
        let line3 = "Extreme difficulty breathing".localized
        let line4 = "Severe, constant lightheadedness".localized
        let line5 = "Serious disorientation or unresponsiveness".localized
        
        let att1 = "911"
        let att2 = "119"
        
        let msg = "\(line1) \n\n• \(line2) \n• \(line3) \n• \(line4) \n• \(line5)"
       
        let longRange1 = (msg as NSString).range(of: att1)
        let longRange2 = (msg as NSString).range(of: att2)

        let attribString1 = NSMutableAttributedString(string: msg, attributes: [NSAttributedStringKey.font :  UIFont(name: "ProximaNova-Regular", size: 15.0)!])

        attribString1.setAttributes([NSAttributedStringKey.font : UIFont(name: "ProximaNova-SemiBold", size: 15)!, NSAttributedStringKey.foregroundColor : UIColor.black], range: longRange1)
        attribString1.setAttributes([NSAttributedStringKey.font : UIFont(name: "ProximaNova-SemiBold", size: 15)!, NSAttributedStringKey.foregroundColor : UIColor.black], range: longRange2)

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 1.5
        attribString1.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, msg.length))
        msgLbl.attributedText = attribString1
        msgLbl.textAlignment = .left
        
        let cancelBtn = notifierView.viewWithTag(11) as! UIButton
        cancelBtn.addTarget(self, action: #selector(hidepopup), for: .touchUpInside)
                      
        let emergencyBtn = notifierView.viewWithTag(22) as! UIButton
        let str = "I'm experiencing at least one of these symptoms".localized
        let font1: UIFont = UIFont(name: "ProximaNova-Regular", size: 15.0)!
        let attributes1 = [NSMutableAttributedString.Key.font: font1,NSAttributedStringKey.foregroundColor : UIColor.white]
        let atrStr = NSMutableAttributedString(string: str, attributes: attributes1)
        emergencyBtn.setAttributedTitle(atrStr, for: .normal)
        emergencyBtn.titleLabel?.textAlignment = .center
        emergencyBtn.addTarget(self, action: #selector(showEmergencyPopup), for: .touchUpInside)
        
        let okayBtn = notifierView.viewWithTag(33) as! UIButton
        okayBtn.setTitle("I don't have any of these symptoms".localized, for: .normal)
        okayBtn.addTarget(self, action: #selector(hidepopup), for: .touchUpInside)
        notifierView.backgroundColor = UIColor.white
        
        let notifier = AANotifier(notifierView, withOptions: options)
        return notifier
      }()
    
    //EmergencyCallPopup
    lazy var EmergencyCallPopup: AANotifier = {
        
        let notifierView = UIView.fromNib(nibName: "EmergencyCallPopup")!
        let options: [AANotifierOptions] = [
            .position(.top),
            .preferedHeight(self.view.frame.size.height + 64),
            .margins(H: 0, V: 0),
            .transitionA(.fromFade, 0.40),
            .transitionB(.toFade,0.40),
            //.hideOnTap(false)
        ]
     
        let emergencLabel = notifierView.viewWithTag(110) as! UILabel
        emergencLabel.text = "Emergency".localized
        
//        let msgLabel = notifierView.viewWithTag(101) as! UILabel
//        msgLabel.text = "Emergency".localized
        
        let callBtn = notifierView.viewWithTag(22) as! UIButton
        callBtn.addTarget(self, action: #selector(callAc), for: .touchUpInside)
        
        let call911Btn = notifierView.viewWithTag(100) as! UIButton
        call911Btn.addTarget(self, action: #selector(call911Ac), for: .touchUpInside)
        
        let cancelBtn = notifierView.viewWithTag(33) as! UIButton
        cancelBtn.addTarget(self, action: #selector(hideEmergencypopup), for: .touchUpInside)
       
        notifierView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let notifier = AANotifier(notifierView, withOptions: options)
        return notifier
        
    }()
    
    var navigatioObj: HealthHomeNavigationView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap_dismissKeyboard(_:)))
        self.view.addGestureRecognizer(tap)
        self.locationLabel.text = SessionManager.getCountry()
        
       // let gameTimer: Timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.showHealthPopup), userInfo: nil, repeats: false)

    }
  
     override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.setNavigationBarHidden(true, animated: animated)

        self.navigatioObj = HealthHomeNavigationView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 110))
        self.navigatioObj?.buttonEmergency.addTarget(self, action: #selector(self.emergencyAc), for: .touchUpInside)
        self.view.addSubview(self.navigatioObj ?? UIView())
        
        UIBarButtonItem.appearance().setTitleTextAttributes(
               [
                    NSAttributedStringKey.font : UIFont(name: "CircularStd-Book", size: 10)!,
                    NSAttributedStringKey.foregroundColor : UIColor.white,
               ], for: .normal)
          
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getHealthNews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
       
    }
    
    @IBAction func locationAction(_ sender: Any) {
        guard let exposedLocation = self.locationManager.exposedLocation else {
          print("*** Error in \(#function): exposedLocation is nil")
          return
     }
     
     self.locationManager.getPlace(for: exposedLocation) { placemark in
          guard let placemark = placemark else { return }
          
          if let country = placemark.country {
               self.locationLabel.text = "\(country)"
          }
        }
    }
    
    @objc func emergencyAc() {
        showEmergencyPopup()

    }
     
     @objc func handleTap_dismissKeyboard(_ sender: UITapGestureRecognizer) {
        if self.searchTextField != nil {
            self.searchTextField.resignFirstResponder()
        }
    }
    
    @IBAction func startAction(_ sender: Any) {
        if Connectivity.isConnectedToInternet(){
            self.performSegue(withIdentifier: "visitorList", sender: self)
            self.view.endEditing(true)
        } else{
          self.showError(message: "No Internet Connection")
        }
    }
    
    @objc func showHealthPopup() {
        HealthMainPopup.show()
    }
    
    @objc func showEmergencyPopup() {
        HealthMainPopup.hide()
        EmergencyCallPopup.show()
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SymptomsViewController") as! SymptomsViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func hidepopup() {
        HealthMainPopup.hide()
    }
    
    @objc func callAc() {
        EmergencyCallPopup.hide()
        guard let number = URL(string: "tel://119") else { return }
        UIApplication.shared.open(number)
     }
     
    @objc func call911Ac() {
       EmergencyCallPopup.hide()
       guard let number = URL(string: "tel://911") else { return }
       UIApplication.shared.open(number)
    }
    
    @objc func hideEmergencypopup() {
        EmergencyCallPopup.hide()
    }
    
    func getHealthNews() {
        
        if Connectivity.isConnectedToInternet() {
        
            self.showLoader(strForMessage: "loading..".localized)
            let webservice  = WebserviceSigleton ()
            webservice.GETServiceWithoutParameters(urlString: "http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=14e28b06e23d4eff88514fe7b317fb53") { (data, error) in
                       
                self.dismissLoader()
                let obj = Mapper<HealthNewsModalClass>().map(JSONObject: data)
                self.healthNewsArray = obj
                                       
            }
            
        } else {
            self.tableView.isHidden = true
            self.constraintTableViewHeight.constant = 0
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func buttonActionChatNow(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Health", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DoctorListingViewController") as! DoctorListingViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonActionDoctors(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Health", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DoctorListingViewController") as! DoctorListingViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func buttonActionAppointments(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Health", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyAppointmentViewController") as! MyAppointmentViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func buttonActionMedication(_ sender: UIButton) {
        
    }
    
    @objc func buttonActionSeeAll() {
        let storyboard = UIStoryboard(name: "Health", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NewsListingViewController") as! NewsListingViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

//MARK:- UITableViewDelegate,UITableViewDataSource
extension HealthHomeVC : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as! NewsTableViewCell
        cell.selectionStyle = .none
        if let data = self.healthNewsArray?.articles?[indexPath.row] {
            cell.labelHeading.text = data.title ?? ""
            cell.labelNews.text = data.descriptionText ?? ""
            if let imageUrl = data.urlToImage {
                cell.imageViewNews.isHidden = false
                cell.imageViewNews.sd_setShowActivityIndicatorView(true)
                cell.imageViewNews.sd_setIndicatorStyle(.gray)
                cell.imageViewNews.sd_setImage(with: URL(string: imageUrl),
                                               placeholderImage: nil)
                //cell.imageViewNews.sd_setShowActivityIndicatorView(false)
            } else {
                cell.imageViewNews.isHidden = true
            }
        }
        
        return cell
    }
    
    
    func calculateHeightForConfiguredSizingCell(cell: NewsTableViewCell) -> CGFloat {
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let height = cell.contentView.systemLayoutSizeFitting(UILayoutFittingExpandedSize).height + 1.0
        return height
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
        let seeAllButton = UIButton(frame: CGRect(x: tableView.frame.width - 70, y: 0, width: 50, height: 40))
        seeAllButton.setTitle("See All", for: .normal)
        seeAllButton.addTarget(self, action: #selector(self.buttonActionSeeAll), for: .touchUpInside)
        seeAllButton.titleLabel?.font = UIFont(name: "ProximaNova-SemiBold", size: 14.0)
        seeAllButton.setTitleColor(UIColor(red: 28/255, green: 87/255, blue: 255/255, alpha: 1), for: .normal)
        footerView.addSubview(seeAllButton)
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let data = self.healthNewsArray?.articles?[indexPath.row] {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
            vc.urlString = data.url ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

}
