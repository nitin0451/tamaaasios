//
//  MedicalTableViewCell.swift
//  Tamaas
//
//  Created by Apple on 23/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class MedicalTableViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var sideImage: UIImageView!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var constraintViewWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
