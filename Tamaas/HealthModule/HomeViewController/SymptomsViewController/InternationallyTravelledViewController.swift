//
//  InternationallyTravelledViewController.swift
//  Tamaas
//
//  Created by Apple on 23/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class InternationallyTravelledViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var labelHeadingText: UILabel!{
        didSet{
            labelHeadingText.text = "In the last 14 days, have you traveled internationally?".localized
        }
    }
    @IBOutlet weak var constraintButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var tableView: UITableView! {
        didSet{
            tableView.register(UINib(nibName: "InternationallyTableViewCell", bundle: nil), forCellReuseIdentifier: "InternationallyTableViewCell")
            tableView.tableFooterView = UIView()
            
        }
    }
    
    //MARK:- Variables
    let titleArray = ["I have travelled internationally".localized,"No, I have not travelled internationally".localized]
    var movingFor: String?
    var symptomsMessage = ""
    var isNoneOfAbove : Bool?
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    @IBAction func buttonActionNext(_ sender: Any) {
        
    }
    
}

//MARK:- UITableViewDelegate,UITableViewDataSource
extension InternationallyTravelledViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InternationallyTableViewCell", for: indexPath) as! InternationallyTableViewCell
        cell.labelTitle.text = titleArray[indexPath.row]
         cell.viewBackground.backgroundColor = .white
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let cell = tableView.cellForRow(at: indexPath) as! InternationallyTableViewCell
        UIView.animate(withDuration: 0.25) {
            cell.viewBackground.backgroundColor = .lightGray
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+0.50) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MedicalFacilityViewController") as! MedicalFacilityViewController
            var msg = ""
            if self.isNoneOfAbove ?? false {
                msg = "1. \(self.titleArray[indexPath.row])"
            } else {
                msg = "\(String(describing: self.symptomsMessage)) \n 3. \(self.titleArray[indexPath.row])"
            }
            vc.symptomsMessage = msg
            vc.movingFor = self.movingFor
            vc.isNoneOfAbove = self.isNoneOfAbove ?? false
            self.navigationController?.pushViewController(vc, animated: true)
        }
      
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! InternationallyTableViewCell
        cell.viewBackground.backgroundColor = .white
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
    }
    
}

