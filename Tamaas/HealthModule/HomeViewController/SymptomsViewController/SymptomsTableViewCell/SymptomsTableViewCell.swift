//
//  SymptomsTableViewCell.swift
//  Tamaas
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class SymptomsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewSide: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
