//
//  MedicalFacilityViewController.swift
//  Tamaas
//
//  Created by Apple on 23/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class MedicalFacilityViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var buttonContinue: UIButton!{
        didSet{
            buttonContinue.setTitle("Continue".localized, for: .normal)
        }
    }
    @IBOutlet weak var labelHeadingMessage: UILabel!{
        didSet{
            labelHeadingMessage.text = "Have you worked in a hospital or other care facilities in the past 14 days".localized
        }
    }
    @IBOutlet weak var labelHeadingText: UILabel!{
        didSet{
            labelHeadingText.text = "In the last 14 days, have you traveled internationally?".localized
        }
    }
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.register(UINib(nibName: "MedicalTableViewCell", bundle: nil), forCellReuseIdentifier: "MedicalTableViewCell")
            tableView.tableFooterView = UIView()
            tableView.estimatedRowHeight = 70
        }
    }
    
    //MARK:- Variables
    let titleArray = ["I have worked in a hospital or other care facility in past 14 days".localized,"I plan to work in a hospital or other care facility in the next 14 days".localized,"No, I don't work or plan to work in a care facility".localized]
    let subtitleArray = ["This includes volunteering".localized,"This includes volunteering".localized,""]
    var movingFor: String?
    var symptomsMessage = ""
    var selectedIndex : Int?
    var isNoneOfAbove : Bool?

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func buttonActionContinue(_ sender: Any) {
        if let _ = selectedIndex {
            
        } else {
            return
        }
        var msg = ""
        if isNoneOfAbove ?? false {
            msg = "\(String(describing: self.symptomsMessage)) \n 2. \(self.titleArray[selectedIndex ?? 0]) "
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BodyPartVC") as! BodyPartVC
                if self.movingFor == "self" {
                    if let selectedYr = UserDefaults.standard.value(forKey: "selected_year") as?Int{
                        vc.yearOfBirth = selectedYr
                    }
                    if let selector = UserDefaults.standard.value(forKey: "selector") as? Int {
                        vc.selector = selector
                    }
                } else if self.movingFor == "child" {
                    let currentYear = Calendar.current.component(.year, from: Date())

                    if let selectedYr = UserDefaults.standard.value(forKey: "childAge") as? String{
                        vc.yearOfBirth = currentYear - Int(selectedYr)!
                    }
                    if let selector = UserDefaults.standard.value(forKey: "childGender") as? Int {
                        vc.selector = selector
                    }
                } else if self.movingFor == "parents" {
                    let currentYear = Calendar.current.component(.year, from: Date())

                    if let selectedYr = UserDefaults.standard.value(forKey: "parentsAge") as? String{
                        vc.yearOfBirth = currentYear - Int(selectedYr)!
                    }
                    if let selector = UserDefaults.standard.value(forKey: "parentGender") as? Int {
                        vc.selector = selector
                    }
                }
            
            vc.symptomsMessage = msg
            vc.movingFor = self.movingFor
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            
            msg = "\(String(describing: self.symptomsMessage)) \n 4. \(self.titleArray[selectedIndex ?? 0])"
            let storyboard = UIStoryboard(name: "Health", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DoctorListingViewController") as! DoctorListingViewController
            vc.symptomsMessage = msg
            vc.movingFor = self.movingFor
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }

}

//MARK:- UITableViewDelegate,UITableViewDataSource
extension MedicalFacilityViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MedicalTableViewCell", for: indexPath) as! MedicalTableViewCell
        cell.labelTitle.text = titleArray[indexPath.row]
        cell.labelDesc.text = subtitleArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MedicalTableViewCell
        self.selectedIndex = indexPath.row
        cell.sideImage.image = UIImage(named: "checkbox-pressed")
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MedicalTableViewCell
        cell.sideImage.image = UIImage(named: "checkbox-normal")

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
}
