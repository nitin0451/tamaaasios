//
//  SymptomsViewController.swift
//  Tamaas
//
//  Created by Apple on 22/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
}

class SymptomsViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var buttonNext: UIButton!{
        didSet{
            buttonNext.setTitle("Next".localized, for: .normal)
        }
    }
    @IBOutlet weak var labelHeadingText: UILabel!{
        didSet{
            labelHeadingText.text = "Are you experiencing any of these symptoms?".localized
        }
    }
    @IBOutlet weak var constraintButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: "SymptomsTableViewCell", bundle: nil), forCellReuseIdentifier: "SymptomsTableViewCell")
            tableView.tableFooterView = UIView()
        }
    }
    
    //MARK:- Variables
    var titleArray = ["Fever, Chills or Sweating".localized,"Difficulty breathing (not severe)".localized,"New or worsening cough".localized,"Sore throat".localized,"Aching throughout the body".localized,"Vomit or Diarrhea".localized,"None of the above".localized]
    var indexArray = [IndexPath]()
    var movingFor: String?
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.indexArray.removeAll()
        self.tableView.reloadData()
    }
    
    @IBAction func buttonActionNext(_ sender: Any) {
        if indexArray.count == 0 {return}
        if indexArray.contains(IndexPath(row: 6, section: 0)) {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InternationallyTravelledViewController") as! InternationallyTravelledViewController
            vc.movingFor = self.movingFor
            vc.isNoneOfAbove = true
            self.navigationController?.pushViewController(vc, animated: true)
            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BodyPartVC") as! BodyPartVC
//            vc.symptomsMessage = self.getSymptonsMessage()
//
//            if self.movingFor == "self" {
//                if let selectedYr = UserDefaults.standard.value(forKey: "selected_year") as? Int{
//                    vc.yearOfBirth = selectedYr
//                }
//                if let selector = UserDefaults.standard.value(forKey: "selector") as? Int {
//                    vc.selector = selector
//                }
//            } else if self.movingFor == "child" {
//                let currentYear = Calendar.current.component(.year, from: Date())
//
//                if let selectedYr = UserDefaults.standard.value(forKey: "childAge") as? String{
//                    vc.yearOfBirth = currentYear - Int(selectedYr)!
//                }
//                if let selector = UserDefaults.standard.value(forKey: "childGender") as? Int {
//                    vc.selector = selector
//                }
//            } else if self.movingFor == "parents" {
//                let currentYear = Calendar.current.component(.year, from: Date())
//
//                if let selectedYr = UserDefaults.standard.value(forKey: "parentsAge") as? String{
//                    vc.yearOfBirth = currentYear - Int(selectedYr)!
//                }
//                if let selector = UserDefaults.standard.value(forKey: "parentGender") as? Int {
//                    vc.selector = selector
//                }
//            }
//            self.navigationController?.pushViewController(vc, animated: true)

        } else {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SymptomsTimeViewController") as! SymptomsTimeViewController
            vc.movingFor = self.movingFor
            vc.symptomsMessage = self.getSymptonsMessage()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getSymptonsMessage() -> String {
        var messageStr = ""
        for i in 0...indexArray.count-1 {
            let row = indexArray[i].row
            let msg = self.titleArray[row]
            messageStr = messageStr + "," + msg
            messageStr = messageStr.deletingPrefix(",")
        }
        messageStr = "1. " + messageStr
        return messageStr
    }
}

//MARK:- UITableViewDelegate,UITableViewDataSource
extension SymptomsViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SymptomsTableViewCell", for: indexPath) as! SymptomsTableViewCell
        cell.labelTitle.text = titleArray[indexPath.row]
        
        if self.indexArray.contains(indexPath) {
            cell.imageViewSide.image = UIImage(named: "checkbox-pressed")
        } else {
            cell.imageViewSide.image = UIImage(named: "checkbox-normal")
        }

        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if indexPath.row == self.titleArray.count-1 {
            if self.indexArray.contains(indexPath) {
                if let index = self.indexArray.lastIndex(where: {$0 == indexPath}) {
                    self.indexArray.remove(at: index)
                }
            } else {
                self.indexArray.removeAll()
                self.indexArray.append(indexPath)
            }
        } else{
            if indexArray.contains(IndexPath(row: 6, section: 0)) {
                self.indexArray.removeAll()
            }
            if self.indexArray.contains(indexPath) {
               if let index = self.indexArray.lastIndex(where: {$0 == indexPath}) {
                    self.indexArray.remove(at: index)
                }
            } else {
                self.indexArray.append(indexPath)
            }
        }
        
        UIView.animate(withDuration: 0.5) {
            if self.indexArray.count == 0 {
                self.constraintButtonHeight.constant = 0
            } else {
                self.constraintButtonHeight.constant = 60
            }
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
        }
        
        tableView.reloadData()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            if indexPath.row == self.titleArray.count-1 {
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "InternationallyTravelledViewController") as! InternationallyTravelledViewController
//                self.navigationController?.pushViewController(vc, animated: true)
//            } else {
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SymptomsTimeViewController") as! SymptomsTimeViewController
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
       
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
     
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        
    }
    
}
