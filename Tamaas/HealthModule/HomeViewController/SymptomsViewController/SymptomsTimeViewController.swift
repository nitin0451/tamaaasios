//
//  SymptomsTimeViewController.swift
//  Tamaas
//
//  Created by Apple on 23/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class SymptomsTimeViewController: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var labelMinimumValue: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var labelMaximumValue: UILabel!
    @IBOutlet weak var labelHeadingText: UILabel!{
        didSet{
            labelHeadingText.text = "How many days have you been feeling these symptoms?".localized
        }
    }
    @IBOutlet weak var buttonNext: UIButton!{
        didSet{
            buttonNext.setTitle("Next".localized, for: .normal)
        }
    }
    
    //MARK:- Variables
    let currentValue = UILabel()
    var movingFor: String?
    var symptomsMessage = ""

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.createLabel(slider)
    }
    
    @IBAction func sliderAction(_ sender: UISlider) {
        self.createLabel(sender)
    }
    
    @IBAction func buttonActionNextQuestion(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InternationallyTravelledViewController") as! InternationallyTravelledViewController
        vc.movingFor = self.movingFor
        let msg = "\(String(describing: symptomsMessage)) \n 2. Number of days - \(currentValue.text ?? "")"
        vc.symptomsMessage = msg
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func createLabel(_ sender: UISlider) {
        
        self.currentValue.removeFromSuperview()
        let _thumbRect: CGRect = sender.thumbRect(forBounds: sender.bounds, trackRect: sender.trackRect(forBounds: sender.bounds), value: sender.value)
        let thumbRect: CGRect = view.convert(_thumbRect, from: sender)

        currentValue.frame = CGRect(x: thumbRect.origin.x, y: thumbRect.origin.y - 35, width: 40, height: 30)
        currentValue.text =  String(Int(floor(sender.value)))
        currentValue.textAlignment = .center
        currentValue.textColor = .darkGray
        currentValue.font = UIFont(name: "ProximaNova-Regular", size: 14.0)
        self.view.addSubview(currentValue)
    }
}
