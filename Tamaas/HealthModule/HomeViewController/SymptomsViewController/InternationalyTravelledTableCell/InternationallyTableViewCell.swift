//
//  InternationallyTableViewCell.swift
//  Tamaas
//
//  Created by Apple on 27/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit

class InternationallyTableViewCell: UITableViewCell {

    @IBOutlet weak var viewBackground: CustomDesignView!
    @IBOutlet weak var labelTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
