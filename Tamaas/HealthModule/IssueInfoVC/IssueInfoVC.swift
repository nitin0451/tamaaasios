//
//  IssueInfoVC.swift
//  Tamaas
//
//  Created by osvinuser on 26/09/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class IssueInfoVC: UIViewController {

     @IBOutlet weak var webView: UIWebView!
     var issueInfo = HealthIssueInfo()
     
    override func viewDidLoad() {
        super.viewDidLoad()
 
        // Do any additional setup after loading the view.
        loadHtmlCode()
    }
//     override func viewWillAppear(_ animated: Bool) {
//     self.navigationController?.navigationBar.backgroundColor = UIColor.blue
//
//
//     }
//     override func viewWillDisappear(_ animated: Bool) {
//
//     self.navigationController?.navigationBar.backgroundColor = UIColor.clear
//
//     }
     func loadHtmlCode() {
          self.title = "\(String(describing: issueInfo.profName!))"
          
          var htmlCode = "<html><head><title></title></head><body><p><strong>\(String(describing: issueInfo.name!)) (\(String(describing: issueInfo.profName!)))</strong>"
         
          if String(describing: issueInfo.medicalCondition) != "" {
               htmlCode = htmlCode + "<body><p><strong>Medical Condition:</strong>"
               htmlCode = htmlCode + "\(String(describing: issueInfo.medicalCondition!))</>"
                }
               if String(describing: issueInfo.treatmentDescription) != ""{
                    htmlCode = htmlCode + "<body><p><strong>Treatment:</strong>"
                    htmlCode = htmlCode + "\(String(describing: issueInfo.treatmentDescription!))</>"
                    }
                 if String(describing: issueInfo.synonyms) != ""{
                    htmlCode = htmlCode + "<body><p><strong>Synonyms:</strong>"
                    htmlCode = htmlCode + "\(String(describing: issueInfo.synonyms!))</></body>"
                      }
         
          webView.loadHTMLString(htmlCode, baseURL: nil)
     }
     
     func loadIssueInfo(){
          
//          nameLabel.text = "\(String(describing: issueInfo.name)) (\(String(describing: issueInfo.profName)))"
//          descriptionLabel.text = issueInfo.description
//          if String(describing: issueInfo.medicalCondition) != "" {
//                medicalCondLabel.text = "Medical Condition: \(String(describing: issueInfo.medicalCondition))"
//          }
//          if String(describing: issueInfo.treatmentDescription) != ""{
//               treatmentLabel.text = "Treatment: \(String(describing: issueInfo.treatmentDescription))"
//          }
//          if String(describing: issueInfo.synonyms) != ""{
//                synonymsLabel.text = "Synonyms: \(String(describing: issueInfo.synonyms))"
//          }
         
     }
    
     @IBAction func backAction(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
     }
}
