//
//  BookAppointmentView.swift
//  DemoApp
//
//  Created by Apple on 31/03/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class BookAppointmentView: UIView {
    @IBOutlet weak var buttonCross: UIButton!
    @IBOutlet weak var buttonMyAppointments: CustomButtonView!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDateAddress: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
       
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }

    func loadViewFromNib() {
           
        let view = UINib(nibName: "BookAppointmentView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
          
    }

}
