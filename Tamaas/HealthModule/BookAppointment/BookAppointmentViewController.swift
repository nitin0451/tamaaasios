//
//  BookAppointmentViewController.swift
//  DemoApp
//
//  Created by Apple on 30/03/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit
import FSCalendar

class BookAppointmentViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var buttonMakeAppointment: UIButton!
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.estimatedRowHeight = 45
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.register(UINib(nibName: "AppointmentTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "AppointmentTimeTableViewCell")
        }
    }
    @IBOutlet weak var calender: FSCalendar!
    
    var imageArray = [UIImage(named: "morning"),UIImage(named: "afternoon"),UIImage(named: "evening"),UIImage(named: "night")]
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setGradientBackground()
    }

    func setGradientBackground() {
        let colorTop =  UIColor(red: 235.0/255.0, green: 64.0/255.0, blue: 44.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 217.0/255.0, green: 6.0/255.0, blue: 71.0/255.0, alpha: 1.0).cgColor

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds

        self.calender.layer.insertSublayer(gradientLayer, at:0)
    }
    
    @IBAction func buttonAction_bookAppointment(_ sender: UIButton) {
       let storyboard = UIStoryboard(name: "Health", bundle: nil)
       let vc = storyboard.instantiateViewController(withIdentifier: "AppointmentDetailViewController") as! AppointmentDetailViewController
       self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

//MARK:- UITableViewDelegate,UITableViewDataSource
extension BookAppointmentViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AppointmentTimeTableViewCell", for: indexPath) as? AppointmentTimeTableViewCell else {return UITableViewCell()}
        cell.selectionStyle = .none
        cell.sideImage.image = imageArray[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
