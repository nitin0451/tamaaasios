//
//  PaymentSelectionView.swift
//  DemoApp
//
//  Created by Apple on 01/04/20.
//  Copyright © 2020 Nitin Mukesh. All rights reserved.
//

import UIKit

class PaymentSelectionView: UIView {

    @IBOutlet weak var buttonPayViaCards: UIButton!
    @IBOutlet weak var buttonPayViaInsurance: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        
        let view = UINib(nibName: "PaymentSelectionView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view);
       
    }
    
}
