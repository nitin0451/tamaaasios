//
//  FeedbackViewController.swift
//  Tamaas
//
//  Created by Apple on 26/04/20.
//  Copyright © 2020 Krescent Global. All rights reserved.
//

import UIKit
import Cosmos

class FeedbackViewController: BaseHealthViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var labelCharactersCount: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var ratingView: CosmosView!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func buttonActionSubmit(_ sender: Any) {
        
        
    }
    
    
    func submitFeedback(){
       
        if Connectivity.isConnectedToInternet() {
             
            self.showLoader(strForMessage: "loading..".localized)
            let webservice  = WebserviceSigleton ()
                 
            let dict: Dictionary<String,Any> = ["rivew":self.textView.text as String,"rating":ratingView.rating as Double,"from":2, "to":3]
            
            webservice.POSTServiceWithParametersNew(urlString: constantVC.WebserviceName.URL_POST_DOCTOR_RATING, params: dict as Dictionary<String, AnyObject>) { (data, error) in
                     
                self.dismissLoader()
                print(data)
                
            }
        }
    }
}

extension FeedbackViewController: UITextViewDelegate {

    func textViewDidChange(_ textView: UITextView) {
        labelCharactersCount.text = "\("500/")\(500 - textView.text.count)"
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        self.view.endEditing(true)
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return textView.text.count + (text.count - range.length) <= 500
    }

}
