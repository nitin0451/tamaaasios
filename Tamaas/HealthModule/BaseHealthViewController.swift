//
//  BaseViewController.swift
//  HealthModule
//
//  Created by Mac-Mini- Nav on 09/08/19.
//  Copyright © 2019 Mac-Mini- Nav. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseHealthViewController: UIViewController, NVActivityIndicatorViewable {
    var client : DiagnosisClient!
    var alertDialogControllerClass = AlertDialogController.self
    let activityData = ActivityData()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
     
      if Connectivity.isConnectedToInternet() {
          if client == nil{
               client = DiagnosisClient.init("imransamed7@gmail.com", password: "a5BSe8f2K4Pwo7R3N", authServiceUrl: "https://sandbox-authservice.priaid.ch/login", language: "en-gb", healthServiceUrl: "https://sandbox-healthservice.priaid.ch")
               
               client.authorizeAccessToken({ DiagnosisClient, ApiMedicCommandStatus, AccessToken in
                    print(AccessToken?.token)
               })
          }
      }else{
          showError(message: "No Internet Connection")
          self.navigationController?.popViewController(animated: true)
     }
     
    }
    //MARK:- Alerts
    func showError(message:String) {
        alertDialogControllerClass.showAlertWithMessage(message: message, title: nil, presenter: self)
    }
    
    func setupNavigation(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func showLoader(strForMessage:String!){
        
        let size = CGSize(width: 50, height:50)
        startAnimating(size, message: strForMessage, type : NVActivityIndicatorType.ballScaleMultiple, backgroundColor: constantVC.GlobalColor.bgColor, textColor: UIColor.white)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    func dismissLoader(){
        stopAnimating()
    }
}

