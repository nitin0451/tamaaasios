//
//  RollingPitTabBar.swift
//  VBRRollingPit
//
//  Created by Viktor Braun on 27.07.2018.
//  Copyright © 2018 Viktor Braun - Software Development. All rights reserved.
//

import UIKit

class Tabbar: UITabBar,UITabBarDelegate{
    var middleButton = UIButton()
    @IBInspectable public var barBackColor : UIColor = UIColor.white
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
     // setupMiddleButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
        setupMiddleButton()
    }
    
    func setupMiddleButton() {
        middleButton.frame.size = CGSize(width: 100, height: 100)
        middleButton.setBackgroundImage(UIImage(named:"heartIcon"), for: UIControlState.normal)
        middleButton.layer.masksToBounds = true
        middleButton.center = CGPoint(x: UIScreen.main.bounds.width / 2, y:  (self.frame.height / 2)+5)
//        middleButton.isUserInteractionEnabled = false
//        middleButton.cornerRadius = 32
        middleButton.addTarget(self, action: #selector(test), for: .touchUpInside)

        addSubview(middleButton)
    }
    
    @objc func test() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyTab"), object: nil, userInfo: nil)
    }
    
    private func setup(){
     self.delegate = self
//        self.backgroundImage    = UIImage()
//        self.shadowImage        = UIImage()
//        self.backgroundColor    = UIColor.white
//        self.borderColor        = UIColor(red: 114.0/255.0, green: 125.0/255.0, blue: 136.0/255.0, alpha: 1.0)
//        self.borderWidth        = 0.3
     if isIphoneX(){
          for item in self.items ?? []{
                item.imageInsets = UIEdgeInsetsMake(4, 0, -4, 0)
               item.titlePositionAdjustment =  UIOffsetMake(0, 9)
          }
     }
     else{
          for item in self.items ?? []{
              
               item.titlePositionAdjustment =  UIOffsetMake(0, -2)
          }
     }
     
    }
     override func sizeThatFits(_ size: CGSize) -> CGSize {
          var sizeThatFits = super.sizeThatFits(size)
          if isIphoneX() == false{
                sizeThatFits.height = 63
          }
          
         
          
          return sizeThatFits
     }
     func isIphoneX() -> Bool{
          if UIDevice().userInterfaceIdiom == .phone {
               if UIScreen.main.nativeBounds.height == 2436 || UIScreen.main.nativeBounds.height == 2688 || UIScreen.main.nativeBounds.height == 1792  { //iPhoneX
                    return true
               }
          }
          return false
     }
     func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem){
          print(tabBar.tag)
          print(item)
     }
}
