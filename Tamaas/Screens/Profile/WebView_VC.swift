//
//  WebView_VC.swift
//  Tamaas
//
//  Created by Krescent Global on 16/07/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import WebKit

class WebView_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var navigation_title: UINavigationItem!
    @IBOutlet weak var web_view: UIWebView!
    
    
    //MARK:- Variables
    var strTitle:String = ""
    var strUrl:String = ""
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigation_title.title = self.strTitle.localized
        
        if strTitle == "Privacy Policy" {
           strUrl = "http://www.tamaaas.com/privacy-policy"
        }
        else {
           strUrl = "http://www.tamaaas.com/legal"
        }
        
        let url = URL (string: strUrl)
        let requestObj = URLRequest(url: url!)
        web_view.loadRequest(requestObj)
    }

    
    //MARK:- UIButton Actions
    @IBAction func barBtn_back_action(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true , completion: nil)
    }
    

}
