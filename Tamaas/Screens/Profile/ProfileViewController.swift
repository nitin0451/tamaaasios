//
//  ProfileViewController.swift
//  Tamaas
//
//  Created by Krescent Global on 25/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import AANotifier
import FTIndicator
import PKImagePicker
import Alamofire
import MessageUI
import CropViewController
import YYWebImage

class ProfileViewController: BaseViewController , MFMailComposeViewControllerDelegate , UITextViewDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var vw_blur: UIView!
    
    //MARK:- Variables
    let arrSettings = ["Telephone","Language","Feedback","Help","Tell a friend","Blocked Contacts", "Logout", "Delete account" , "Tamaaas Version"]
    let picker = PKImagePickerViewController()
    var chosenImage : UIImage!
    var isProfileImageUpdated:Bool = false
    var isDoctor = false
    
    //MARK:- Notifiers
     lazy var languageViewNotifier: AANotifier = {
          
          let notifierView = LanguageView()
          let options: [AANotifierOptions] = [
               .position(.middle),
               .preferedHeight(350),
               .margins(H: 20, V: nil),
               .transitionA(.fromTop, 0.40),
               .transitionB(.toBottom, 0.40)
          ]
          let englishBtn = notifierView.viewWithTag(100) as! UIButton
          englishBtn.tag = 1
          englishBtn.setTitle("English".localized , for: .normal)
          englishBtn.addTarget(self, action: #selector(hidePopupView(sender:)), for: .touchUpInside)
          let dariBtn = notifierView.viewWithTag(101) as! UIButton
          dariBtn.tag = 2
          dariBtn.setTitle("Dari".localized , for: .normal)
          dariBtn.addTarget(self, action: #selector(hidePopupView(sender:)), for: .touchUpInside)
          let pashtoBtn = notifierView.viewWithTag(102) as! UIButton
          pashtoBtn.tag = 3
          pashtoBtn.setTitle("Pashto".localized , for: .normal)
          pashtoBtn.addTarget(self, action: #selector(hidePopupView(sender:)), for: .touchUpInside)
          let turkishBtn = notifierView.viewWithTag(103) as! UIButton
          turkishBtn.tag = 5
          turkishBtn.setTitle("Turkish".localized , for: .normal)
          turkishBtn.addTarget(self, action: #selector(hidePopupView(sender:)), for: .touchUpInside)
          let cancelBtn = notifierView.viewWithTag(120) as! UIButton
          cancelBtn.tag = 4
          cancelBtn.addTarget(self, action: #selector(hidePopupView(sender:)), for: .touchUpInside)
          let notifier = AANotifier(notifierView, withOptions: options)
          
          let tapGesture = UITapGestureRecognizer(target: self, action: #selector(singleTap_languageView(gesture:)))
          notifierView.addGestureRecognizer(tapGesture)
          
          //set current language
          if SessionManager.get_localizeLanguage() == "en"{
               englishBtn.isSelected = true
               dariBtn.isSelected = false
               pashtoBtn.isSelected = false
               turkishBtn.isSelected = false
          }else if SessionManager.get_localizeLanguage() == "fa-AF" {
               englishBtn.isSelected = false
               dariBtn.isSelected = true
               pashtoBtn.isSelected = false
               turkishBtn.isSelected = false
          }
          else if SessionManager.get_localizeLanguage() == "tr" {
               englishBtn.isSelected = false
               dariBtn.isSelected = false
               pashtoBtn.isSelected = false
               turkishBtn.isSelected = true
          }else {
               englishBtn.isSelected = false
               dariBtn.isSelected = false
               pashtoBtn.isSelected = true
               turkishBtn.isSelected = false
          }
          return notifier
     }()
    
    var feedback_textView:UITextView!
    
    lazy var feedbackViewNotifier: AANotifier = {
        
        let notifierView = UIView.fromNib(nibName: "FeedbackView")!
        let options: [AANotifierOptions] = [
            .position(.top),
            .preferedHeight(300),
            .margins(H: 20, V: 20),
            .transitionA(.fromTop,0.40),
            .transitionB(.toBottom,0.40),
            .hideOnTap
        ]
        let share_lbl = notifierView.viewWithTag(99) as! UILabel
        share_lbl.text = "Want to share something with us?".localized
        
        let feedbackTextView = notifierView.viewWithTag(100) as! UITextView
        feedbackTextView.text = "Type here...".localized
        feedbackTextView.textColor = UIColor.lightGray
        self.feedback_textView = feedbackTextView
        self.feedback_textView.delegate = self
        self.feedback_textView.tintColor = UIColor.black
        
        let cancelBtn = notifierView.viewWithTag(101) as! UIButton
        cancelBtn.setTitle("Cancel".localized , for: .normal)
        cancelBtn.addTarget(self, action: #selector(hideFeedbackView), for: .touchUpInside)
        let submitFeedBack = notifierView.viewWithTag(102) as! UIButton
        submitFeedBack.setTitle("Submit".localized, for: .normal)
        submitFeedBack.addTarget(self, action: #selector(submitFeedbackView), for: .touchUpInside)
        let notifier = AANotifier(notifierView, withOptions: options)
        
       // let tapGesture = UITapGestureRecognizer(target: self, action: #selector(singleTap_FeedbackView(gesture:)))
       // notifierView.addGestureRecognizer(tapGesture)
        
        return notifier
    }()
    
    
    //MARK:- Tap Gestures
    @objc func singleTap_languageView(gesture: UITapGestureRecognizer) {
        languageViewNotifier.hide()
        self.vw_blur.isHidden = true
    }
    
    @objc func singleTap_FeedbackView(gesture: UITapGestureRecognizer) {
        feedbackViewNotifier.hide()
        self.vw_blur.isHidden = true
    }
    
    
    //MARK:- UIView life-cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vw_blur.isHidden = true
        self.tableView.reloadData()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap_dismissKeyboard(_:)))
        self.vw_blur.addGestureRecognizer(tap)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadViewOnTab(notification:)), name: NSNotification.Name(rawValue: "notifyReload"), object: nil)
     }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getUserProfile()
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    //MARK:- UIButton Actions
    @IBAction func btnEditProfile_action(_ sender: UIButton) {
        self.view.endEditing(true)
        picker.delegate = self
        picker.modalPresentationStyle = .fullScreen
        present(picker, animated: false, completion: nil)
    }
    
    @objc func handleTap_dismissKeyboard(_ sender: UITapGestureRecognizer) {
        if self.feedback_textView != nil {
            self.feedback_textView.resignFirstResponder()
        }
    }
   
     @objc func reloadViewOnTab(notification : Notification){
          
     }
    
    //MARK:- Web service implementation
    func callToUpdateProfileImageOnServer(){
        
        self.startAnimating()
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(UIImageJPEGRepresentation(self.chosenImage!, 0.1)!, withName: "photoURL", fileName: "\(SessionManager.getPhone()).jpeg", mimeType: "image/jpeg")
                multipartFormData.append("\(SessionManager.getPhone())".data(using: String.Encoding.utf8)!, withName: "phoneNumber")
                
        },
            to: "\(constantVC.GeneralConstants.BASE_URL)\(constantVC.WebserviceName.URL_UPDATE_PHOTO)",
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        guard response.result.error == nil else {
                            self.stopAnimating()
                            return
                        }
                        
                        if let value = response.result.value {
                            if let result = value as? Dictionary<String, AnyObject> {
                                
                                let resultMessage = result["msg"] as! String
                                
                                if resultMessage == "success"{
                                    
                                    //send notification to other members
                                    self.sendPushNotificationToContactUsers()
                                    
                                    constantVC.GlobalVariables.isMyProfileUpdated = true
                                    constantVC.GlobalVariables.isNeedToGetStories = true
                                    self.isProfileImageUpdated = true
                                    self.tableView.reloadData()
                                    self.stopAnimating()
                                }
                                else{
                                    self.stopAnimating()
                                    self.showError(message: result["msg_details"] as! String)
                                }
                            }
                        }
                        }
                        .uploadProgress { progress in // main queue by default
                    }
                    return
                case .failure(let encodingError):
                    self.stopAnimating()
                    debugPrint(encodingError)
                }
        })
    }
    
    
    func getUserProfile() {
        
        if Connectivity.isConnectedToInternet() {
        
            self.showLoader(strForMessage: "loading..".localized)
            let webservice  = WebserviceSigleton ()
            let dict: Dictionary<String,AnyObject> = ["userid":SessionManager.getUserId() as AnyObject,"action" : "getUser" as AnyObject]

            webservice.GETServiceWithParameters(urlString: constantVC.WebserviceName.URL_SAVE_FETCH_CARDDETAIL, params: dict) { (data, error) in
                
                self.dismissLoader()
                if let dict = data?["data"] as? [Dictionary<String,AnyObject>] {
                    if let doc = dict.first?["type"] as? String,doc == "DOCTOR"{
                        self.isDoctor = false
                    } else {
                        self.isDoctor = true
                    }
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
    func sendPushNotificationToContactUsers(){
        var tempArr:[String] = []
        var str_allContacts_IDs:String = ""
        var tempArrNos:[String] = []
        var str_allContacts_NOs:String = ""
        
        //get user's friend list
        QuickBloxManager().retriveRegisteredUsersFromAddressBook { (users) in
            if let arrUsers = users {
                for user in arrUsers {
                    if user.id != SessionManager.get_QuickBloxID()?.uintValue {
                       tempArr.append("\(user.id)")
                    }
                    if let number = user.phone {
                        if number != SessionManager.getPhone() {
                            tempArrNos.append(number)
                        }
                    }
                }
                
                //get all ids comma seperated
                str_allContacts_IDs = tempArr.joined(separator: ",")
                
                //get all Numbers comma seperated
                str_allContacts_NOs = tempArrNos.joined(separator: ",")
                
                //update profile status over server
                ProfileUpdateManager.shared.webserviceToupdateProfileStatus(memberNos: str_allContacts_NOs)
                
                //send push notification via event
                QuickBloxManager().sendSilentPushNotification_updateProfile(msg: "Profile Update" , users: str_allContacts_IDs , fromNo: SessionManager.getPhone())
            }
        }
    }
   
    func callTo_deleteAccount() {
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["user_id": SessionManager.getUserId()]
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_DELETE_ACCOUNT, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    self.dismissLoader()
                    
                    //clean DB
                    self.CleanDatabase()
                    self.webserviceToupdateContact()
                    YYWebImageManager.shared().cache?.memoryCache.removeAllObjects()
                    YYWebImageManager.shared().cache?.diskCache.removeAllObjects()
                    UserDefaults.standard.removeObject(forKey: constantVC.GeneralConstants.Identity)
                    let next = self.storyboard?.instantiateViewController(withIdentifier: "InitViewController") as! InitViewController
                    next.modalPresentationStyle = .fullScreen
                    self.present(next, animated: true, completion: nil)
                    
                }
                else{
                    self.dismissLoader()
                }
            }
            else{
                self.dismissLoader()
            }
        })
    }
    
    
    func webserviceToupdateContact(){
        
        var dictParam = Dictionary<String, String>()
        dictParam["UserNumber"] = SessionManager.getPhone()
        
        let webservice  = WebserviceSigleton ()
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_DELETE_CONTACTS, params: dictParam as Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    constantVC.GlobalVariables.userDetails?.removeAll()
                }
            }
        })
    }
    
    //MARK:- UITextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type here...".localized
            textView.textColor = UIColor.lightGray
        }
    }
    
    //MARK:- Helpers
     @objc func hidePopupView(sender: UIButton) {
          
          if sender.tag == 4 {
               languageViewNotifier.hide()
               self.vw_blur.isHidden = true
          }
          else {
               languageViewNotifier.hide()
               self.vw_blur.isHidden = true
               
               var from_lang:String = ""
               var to_lang:String = ""
               
               //set current language
               if SessionManager.get_localizeLanguage() == "en" {
                    from_lang = "English"
               }else if SessionManager.get_localizeLanguage() == "fa-AF" {
                    from_lang = "Dari"
               }
               else if SessionManager.get_localizeLanguage() == "tr" {
                    from_lang = "Turkish"
               }else {
                    from_lang = "Pashto"
               }
               
               if sender.tag == 1 {
                    to_lang = "English"
               }else if sender.tag == 2 {
                    to_lang = "Dari"
               }
               else if sender.tag == 5 {
                    to_lang = "Turkish"
               }else {
                    to_lang = "Pashto"
               }
               
               //show logout confirmation alert
               let alertController = UIAlertController(title: "Are you sure?".localized, message: "You want to change language from".localized + " " + from_lang.localized + " " + "to".localized + " " + to_lang.localized , preferredStyle: .alert)
               
               // Create the actions
               let okAction = UIAlertAction(title: "Yes".localized, style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    
                    if sender.tag == 1 {
                         SessionManager.save_localizeLanguage(lang: "en")
                    }else if sender.tag == 2 {
                         SessionManager.save_localizeLanguage(lang: "fa-AF")
                    }
                    else if sender.tag == 5 {
                         SessionManager.save_localizeLanguage(lang: "tr")
                    }else {
                         SessionManager.save_localizeLanguage(lang: "ps-AF")
                    }
                    
                    self.showLoader(strForMessage: "Setting Up your language..".localized)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                         self.dismissLoader()
                         let storyboard = UIStoryboard(name: "Main", bundle: nil)
                         let initialViewController = storyboard.instantiateViewController(withIdentifier: "tabBar")
                         let appDelegate = UIApplication.shared.delegate as! AppDelegate
                         appDelegate.window?.rootViewController = initialViewController
                    }
               }
               let cancelAction = UIAlertAction(title: "Cancel".localized, style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
               }
               
               // Add the actions
               alertController.addAction(okAction)
               alertController.addAction(cancelAction)
               
               // Present the controller
               self.present(alertController, animated: true, completion: nil)
          }
    }
    
    @objc func hideFeedbackView() {
        feedbackViewNotifier.hide()
        self.vw_blur.isHidden = true
    }
    
    @objc func submitFeedbackView() {
        
        if (self.feedback_textView.text! == "Type here...") {
            FTIndicator.showToastMessage("Enter your feedback".localized)
        }
        else {
           self.sendFeedbackEmail()
        }
    }
    
    func removeDuplicates(array: [String]) -> [String] {
        var encountered = Set<String>()
        var result: [String] = []
        for value in array {
            if encountered.contains(value) {
            }
            else {
                encountered.insert(value)
                result.append(value)
            }
        }
        return result
    }
    
    func sendFeedbackEmail() {
        if MFMailComposeViewController.canSendMail() {
            feedbackViewNotifier.hide()
            self.vw_blur.isHidden = true
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@tamaaas.com"])
            mail.setMessageBody("<p>\(self.feedback_textView.text!)</p>", isHTML: true)
            present(mail, animated: true)
        } else {
            FTIndicator.showInfo(withMessage: "Please log In to your email account")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    
    func CleanDatabase(){
        CoreData_BussinessLogic.deleteAllCalls()
        CoreData_BussinessLogic.deleteAllFeeds()
        CoreData_BussinessLogic.deleteAllComments()
        CoreData_BussinessLogic.deleteAllNotifications()
        CoreData_BussinessLogic.deleteAllStories()
        CoreData_BussinessLogic.deleteAllStoryBoard()
        SessionManager.save_unreadMsgCount(count: 0)
        SessionManager.save_unreadNotificationCount(count: 0)
    }
    
    func getAboutHeight() ->CGFloat {
        var height:CGFloat = 0.0
        if SessionManager.getAboutUser() != "" {
            height = SessionManager.getAboutUser().height(withConstrainedWidth: self.tableView.frame.size.width - 30 , font: UIFont (name: "CircularStd-Book", size: 15)! )
            height = height + 8
        }
        return height
    }
    
    /*func showAppVersion() -> NSMutableAttributedString{
        var myMutableString = NSMutableAttributedString()
        if let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String{
            let myString:NSString = "Tamaaas v\(appVersion)" as NSString
            myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedStringKey.font:UIFont.init(name:"Lato-Regular",size:18)!])
            myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray , range: NSRange(location:9,length: appVersion.length))
            return myMutableString
        }
        return myMutableString
    }*/
    
    func showAppVersion() -> String{
        if let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String{
            let myString:NSString = "Tamaaas v\(appVersion)" as NSString
            return myString as String
        }
        return ""
    }
   
    @objc func switchAction() {
        let alertController = UIAlertController(title: "Switch Profile".localized, message: "Are you sure you want to switch your profile? You will lose your chats data".localized, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "NO".localized, style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }

        let okAction = UIAlertAction(title: "YES".localized, style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            self.showLoader(strForMessage: "Loading...")
                       
            QuickBloxManager().deleteUser(completionHandler: { (success) in
                ServicesManager.instance().logout(completion: {
                   YYWebImageManager.shared().cache?.memoryCache.removeAllObjects()
                   YYWebImageManager.shared().cache?.diskCache.removeAllObjects()
                   self.CleanDatabase()
                   SessionManager.save_QuickBloxID(id: 0)
                   self.callTo_deleteAccount()
                })
            })
            
        }
                
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
}

extension ProfileViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 1{
            self.vw_blur.isHidden = false
            languageViewNotifier.show()
        }
        else if indexPath.row == 2{
            
            self.vw_blur.isHidden = false
            feedbackViewNotifier.show()
        }
        else if indexPath.row == 3{
            let next = self.storyboard?.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
            self.present(next, animated: true, completion: nil)
        }
        else if indexPath.row == 4 {
            let shareAll = [ "Check out this New Secure Chat App with free video chat, calls, stickers, money and more... Download Tamaaas NOW!  \(constantVC.GeneralConstants.appLiveLink)"] as [Any]
            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
        else if indexPath.row == 5 {
            self.performSegue(withIdentifier: "blockList", sender: self)
        }
        else if indexPath.row == 6{
            
            //show logout confirmation alert
            let alertController = UIAlertController(title: "Are you sure?".localized, message: "You want to logOut".localized, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Yes".localized, style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                ServicesManager.instance().logout(completion: {
                    
                })
                
                //clean DB
                constantVC.openPrivateChat.isActiveOpenPrivateChat = false
                constantVC.ActiveDataSource.isNavigatePostDetails = false
                YYWebImageManager.shared().cache?.memoryCache.removeAllObjects()
                YYWebImageManager.shared().cache?.diskCache.removeAllObjects()
                self.CleanDatabase()
                self.webserviceToupdateContact()
                UserDefaults.standard.removeObject(forKey: constantVC.GeneralConstants.Identity)
                constantVC.GlobalVariables.arrStories_global.removeAll()
                constantVC.GlobalVariables.isNeedToGetStories = true
                SessionManager.save_QuickBloxID(id: 0)
                        
                let next = self.storyboard?.instantiateViewController(withIdentifier: "InitViewController") as! InitViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = next
                appDelegate.window?.makeKeyAndVisible()
                self.present(next, animated: true, completion: nil)
                
            }

            let cancelAction = UIAlertAction(title: "Cancel".localized, style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
          
        }
        else if indexPath.row == 7{
            
            //show delete account confirmation alert
            let alertController = UIAlertController(title: "Delete Account".localized, message: "Are you sure,you want to delete account? You won't be able to recover any of your Chats,Stories,Posts.".localized, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "Yes".localized, style: UIAlertActionStyle.default) {
                UIAlertAction in
                
                self.showLoader(strForMessage: "Loading...")
                
                QuickBloxManager().deleteUser(completionHandler: { (success) in
                    ServicesManager.instance().logout(completion: {
                        YYWebImageManager.shared().cache?.memoryCache.removeAllObjects()
                        YYWebImageManager.shared().cache?.diskCache.removeAllObjects()
                        self.CleanDatabase()
                        SessionManager.save_QuickBloxID(id: 0)
                        self.callTo_deleteAccount()
                    })
                })
                
            }
            let cancelAction = UIAlertAction(title: "Cancel".localized, style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return isDoctor ? 260.0 : 210.0 + self.getAboutHeight()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
}

extension ProfileViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "myHeader") as! HeaderCell
        
        if (self.isProfileImageUpdated) {
          cell.userImage.image = self.chosenImage
          cell.userBgImage.image = self.chosenImage
          self.isProfileImageUpdated = false
        }
        else {
            let str_userImageURl = constantVC.GeneralConstants.ImageUrl + SessionManager.getPhone() + ".jpeg"
            cell.userImage.yy_setImage(with: URL.init(string: str_userImageURl) , placeholder: UIImage(named:"contact_big") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
            cell.userBgImage.yy_setImage(with: URL.init(string: str_userImageURl), placeholder: UIImage(named:"contact_big") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
        
        cell.userName.text = SessionManager.getUsername()
        if SessionManager.getAboutUser() != ""{
            cell.aboutUser.text = SessionManager.getAboutUser()
        }
        
        cell.switchProfile.addTarget(self, action: #selector(self.switchAction), for: .valueChanged)
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 8 {
            
            //app version
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "version", for: indexPath) as! ProfileCell
            cell.lblVersion.text = self.showAppVersion()
            cell.accessoryType = .none
            return cell
        }
        else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! ProfileCell
            cell.settingLab.text = arrSettings[indexPath.row].localized
            cell.telLab.text = "+\(SessionManager.getPhone())"
            if indexPath.row == 0{
                cell.telLab.isHidden = false
            }
            else{
                cell.telLab.isHidden = true
            }
            if indexPath.row == 7{
                cell.settingLab.textColor = UIColor.red
                cell.accessoryType = .none
            }
            else if indexPath.row == 6{
                cell.settingLab.textColor = UIColor(red: 28.0/255.0, green: 87.0/255.0, blue: 255.0/255.0, alpha: 1.0)
                cell.accessoryType = .none
            }
            else{
                cell.settingLab.textColor = UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 0.8)
                cell.accessoryType = .disclosureIndicator
            }
            
            return cell
        }
    }
  
}

extension ProfileViewController :PKImagePickerViewControllerDelegate {
    func imageSelected(_ img: UIImage!) {
        chosenImage = img
        
        let cropViewController = CropViewController(image: chosenImage)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    func imageSelectionCancelled() {
    }
}

extension ProfileViewController: CropViewControllerDelegate {
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.chosenImage = image
        self.callToUpdateProfileImageOnServer()
        dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        dismiss(animated: true, completion: nil)
    }
}
