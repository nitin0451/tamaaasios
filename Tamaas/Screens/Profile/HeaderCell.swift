//
//  HeaderCell.swift
//  Tamaas
//
//  Created by Krescent Global on 25/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    @IBOutlet weak var switchProfile: UISwitch!
    @IBOutlet weak var labelActivatePractiProfile: UILabel!
    @IBOutlet weak var aboutUser: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userBgImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
