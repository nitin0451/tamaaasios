//
//  HelpViewController.swift
//  Tamaas
//
//  Created by Krescent Global on 25/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import UserNotifications
import FTIndicator

class HelpViewController: BaseViewController {

    
    let arrSettings = ["Notification","FAQ","Privacy Policy","Terms of use"]
    
    @IBOutlet weak var naviBar: UINavigationBar!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
   
    @IBAction func backAc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override  func viewWillAppear(_ animated: Bool) {
        
        self.naviBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]
        self.naviBar.topItem?.title = "Help".localized
    }
    
    //MARK:- Web services
    
    func enablePushNotifications(){
       // AppDelegate().registerForPushNotifications()
    }
    
    func disablePushNotifications() {
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    @objc func switch_notificationStatus(Switch: UISwitch) {
        
        let deviceIdentifier: String = UIDevice.current.identifierForVendor!.uuidString
        
        if Switch.isOn {
            //on notifications
            SessionManager.save_notificationActiveStatus(isHaving: true)
            let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
            if let token = constantVC.GlobalVariables.updatedPushToken {
                appDelegate?.application(UIApplication.shared , didRegisterForRemoteNotificationsWithDeviceToken: token)
            }
          
        } else {
            //off notifications
            QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { (response) in
                if response.isSuccess {
                    SessionManager.save_notificationActiveStatus(isHaving: false)
                }
            }) { (error) in
            }
        }
    }
    
}
extension HelpViewController : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 2 {
            //privacy policies
            let next = self.storyboard?.instantiateViewController(withIdentifier: "WebView_VC") as! WebView_VC
            next.strTitle = "Privacy Policy"
            self.present(next, animated: true, completion: nil)
        }
        else if indexPath.row == 3 {
            //terms of use
            let next = self.storyboard?.instantiateViewController(withIdentifier: "WebView_VC") as! WebView_VC
            next.strTitle = "Terms of use"
            self.present(next, animated: true, completion: nil)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 140.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension HelpViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSettings.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "myHeader") as! HelpHeaderCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.adjustsFontSizeToFitWidth = true
        footerView.textAlignment = .left
        footerView.text = "   " + "© 2018 Tamaaas Inc. All Rights reserved".localized
        footerView.font =  UIFont.init(name: "CircularStd-Book", size: 12)
        footerView.textColor = UIColor.lightGray
        footerView.backgroundColor = UIColor.white
        return footerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! HelpCell
        
        cell.toggleSwitch.addTarget(self, action: #selector(switch_notificationStatus), for: UIControlEvents.valueChanged)
        cell.helpLab.text = arrSettings[indexPath.row].localized
            
        if indexPath.row == 0{
            cell.toggleSwitch.isHidden = false
            if SessionManager.get_notificationActiveStatus() {
                cell.toggleSwitch.setOn( true , animated: false)
            } else {
                cell.toggleSwitch.setOn( false , animated: false)
            }
            cell.accessoryType = .none
        }
        else{
            cell.toggleSwitch.isHidden = true
            cell.accessoryType = .disclosureIndicator
        }
        
        return cell
    }
}
