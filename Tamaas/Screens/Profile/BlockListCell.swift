//
//  BlockListCell.swift
//  Tamaas
//
//  Created by Krescent Global on 18/04/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
class Block {
    
    var user_name: String?
    var user_id: String?
    var userNo:String?
    var userQuickBloxID:String?
}
class BlockListCell: UITableViewCell {

    @IBOutlet weak var unblockBtn: UIButton!
    @IBOutlet weak var userName: UILabel!
    
    var block: Block? {
        didSet {
            updateView()
        }
    }
     func updateView() {
        if let number = block?.userNo {
            userName.text = SessionManager.getNameFromMyAddressBook(number: number)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.unblockBtn.layer.cornerRadius = 3.0
        self.unblockBtn.borderColor = UIColor.gray
        self.unblockBtn.borderWidth = 0.6
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
