//
//  BlockedUserVCViewController.swift
//  Tamaas
//
//  Created by Krescent Global on 18/04/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import AANotifier
class BlockedUserVC: BaseViewController,UITableViewDelegate, UITableViewDataSource {
    var arrBlock = [Block]()
    
    @IBOutlet weak var lbl_noUser_Msg: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    lazy var homeFetchingNotifier: AANotifier = {
        let notifierView = HomeFetchingData()
        let options: [AANotifierOptions] = [
           // .duration(0.4), // Time interval for animation
            .transitionA(.fromTop, 0.40), // Show notifier animation
            .transitionB(.toTop, 0.40), // Hide notifier animation
            .position(.top), // notifier position
            .preferedHeight(96), // notifier height
            .margins(H: 0, V: 0) // notifier margins
        ]
        let notifier = AANotifier(notifierView, withOptions: options)
        return notifier
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Blocked Contacts".localized
        self.lbl_noUser_Msg.text = "No Blocked Contacts!".localized
        tableView.delegate = self
        tableView.dataSource = self
        self.lbl_noUser_Msg.isHidden = true
        self.loadList()
        // Do any additional setup after loading the view.
    }
    
    func loadList() {
        
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_users_id":SessionManager.getUserId(), "detail_type":"full"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GETBLOCKED, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    if let userDetails = response["user_details"] as? [[String:Any]] {
                        
                        for item in userDetails {
                            let block = Block()
                            
                            if let user_id = item["id"] as? String{
                                block.user_id = user_id
                            }
                            if let name = item["name"] as? String{
                                block.user_name = name
                            }
                            if let number = item["phoneNumber"] as? String{
                                block.userNo = number
                            }
                            if let id = item["quickbloxID"] {
                                block.userQuickBloxID = "\(id)"
                            }
                            self.arrBlock.append(block)
                        }
                        self.tableView.reloadData()
                    }
                    
                    if self.arrBlock.count < 1 {
                        self.tableView.isHidden = true
                        self.lbl_noUser_Msg.isHidden = false
                    }
                }
                else{
                    self.view.alpha = 1.0
                    self.tableView.reloadData()
                    self.tableView.isHidden = true
                    self.lbl_noUser_Msg.isHidden = false
                }
            }
            else{
                self.tableView.reloadData()
            }
        })
    }

    override  func viewWillAppear(_ animated: Bool) {
       
       // self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: NSInteger) -> Int {
        
        return arrBlock.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell") as! BlockListCell
        cell.block = arrBlock[indexPath.row]
        //cell.unblockBtn.setTitle("UnBlock", for: .normal)
        cell.unblockBtn.tag = indexPath.row
        cell.unblockBtn.addTarget(self, action: #selector(unblockUser(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    
    @IBAction func backAc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func unblockUser(_ sender: UIButton!){
        
        var quickBloxID:String = ""
        var otherServerId:String = ""
        var otherPhoneNo:String = ""
        
        if let id = arrBlock[sender.tag].userQuickBloxID {
            quickBloxID = id
        }
        
        if let id = arrBlock[sender.tag].user_id {
            otherServerId = id
        }
        
        if let no = arrBlock[sender.tag].userNo {
            otherPhoneNo = no
        }
        
        var userID:String = ""
        if let id = SessionManager.get_QuickBloxID() {
            userID = "\(id)"
        }
        
        homeFetchingNotifier.show()
        self.view.alpha = 0.3
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_users_id":SessionManager.getUserId() , "other_users_id":otherServerId, "loginQuickBloxID": userID , "otherQuickBloxID": quickBloxID ,"status": "0"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_BLOCKUSER, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    //unblock to firebase
                    let document = SessionManager.getPhone() + "-" + otherPhoneNo
                    TamFirebaseManager.shared.deleteDocument(collection: firebaseCollection.Block.rawValue , document: document , completionHandler: { (success) in
                    })
                    
                    TamFirebaseManager.shared.deleteDocument(collection: firebaseCollection.Mute.rawValue , document: document , completionHandler: { (success) in
                    })
                    
                    QuickBloxManager().UnblockUserQuickBlox(userQuickBloxID: quickBloxID)
                    QuickBloxManager().loadBlockUsersList()
                    
                    self.arrBlock.remove(at: sender.tag)
                    self.view.alpha = 1.0
                    
                    if self.arrBlock.count == 0{
                    self.navigationController?.popViewController(animated: true)
                    }
                    else{
                        self.tableView.reloadData()
                    }
                    self.homeFetchingNotifier.hide()
                }
                else{
                    self.homeFetchingNotifier.hide()
                    self.view.alpha = 1.0
                }
            }
            else{
                self.homeFetchingNotifier.hide()
                self.view.alpha = 1.0
            }
        })
        
    }
    
    
}
