//
//  ProfileCell.swift
//  Tamaas
//
//  Created by Krescent Global on 25/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var telLab: UILabel!
    @IBOutlet weak var settingLab: UILabel!
    
    //version
    @IBOutlet weak var lblVersion: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
