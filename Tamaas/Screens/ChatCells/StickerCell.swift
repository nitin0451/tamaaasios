//
//  StickerCell.swift
//  Tamaas
//
//  Created by Krescent Global on 05/03/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

protocol Swipe_gestureDelegate {
    func leftSwipe_delegateMethod()
    func rightSwipe_delegateMethod()
}

class StickerCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    var delegate: Swipe_gestureDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    //add swipe gesture to sticker view
    self.imageView.isUserInteractionEnabled = true
    let left = UISwipeGestureRecognizer(target : self, action : #selector(self.leftSwipe))
    left.direction = .left
    self.imageView.addGestureRecognizer(left)
    
    let right = UISwipeGestureRecognizer(target : self, action : #selector(self.rightSwipe))
    right.direction = .right
    self.imageView.addGestureRecognizer(right)
    }
    
    
    @objc func leftSwipe(_ sender: UISwipeGestureRecognizer){
        delegate?.leftSwipe_delegateMethod()
    }
    
    @objc func rightSwipe(_ sender: UISwipeGestureRecognizer){
        delegate?.rightSwipe_delegateMethod()
    }
}
