//
//  GroupCell.swift
//  Tamaas
//
//  Created by Krescent Global on 23/02/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class GroupCell: UICollectionViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    
}
