//
//  RegisterPhoneVC.swift
//  Tamaas
//
//  Created by Krescent Global on 12/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator

class RegisterPhoneVC: BaseViewController, MICountryPickerDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var countryCodeLab: UILabel!
    @IBOutlet weak var welcomeTitle: UILabel!
    @IBOutlet weak var vw_keyboard: UIView!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var txtField_Phone: VSTextField!
    @IBOutlet weak var imgVw_flag: UIImageView!
    @IBOutlet weak var lbl_verifyUrPhone: UILabel!
    @IBOutlet weak var lbl_sendOTP_msg: UILabel!
    @IBOutlet weak var btn_getCountryCode: UIButton!
    
    //MARK:- UIVariables
    static let identifierForTesting = "VSTextField"
    var dialCodeStr = String()
    lazy var label = UILabel()
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localise_language()
        viewToLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 212.0/255.0, green: 25.0/255.0, blue: 74.0/255.0, alpha: 0.6)
    }
    
    //MARK:- Custom Methods
    func localise_language(){
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        self.doneBtn.setTitle("Done".localized, for: .normal)
        self.welcomeTitle.text = "Welcome!".localized
        self.lbl_verifyUrPhone.text = "Verify your phone.".localized
        self.lbl_sendOTP_msg.text = "Well send you a OTP to confirm your number. Carrier rates may apply".localized
        self.txtField_Phone.placeholder = "Enter Phone number".localized
        self.txtField_Phone.textAlignment = .left
        self.btn_getCountryCode.semanticContentAttribute = .forceLeftToRight
        self.countryCodeLab.textAlignment = .left
    }
    
    func validUserData() -> Bool {
        if txtField_Phone.text.count == 0 || txtField_Phone.text.count < 8 {
            showError(message: "Valid Phone no. is required".localized)
            return false
        }
        return true
    }
    
    func viewToLoad(){
        txtField_Phone.setFormatting("############", replacementChar: "#")
        txtField_Phone.accessibilityIdentifier = RegisterPhoneVC.identifierForTesting
        countryCodeLab.text = "\(self._countryName) \(self.dialCode)"
        dialCodeStr = self.dialCode
        self.imgVw_flag.image = UIImage(named:self._countryCode.lowercased() + ".png")
        countryCodeLab.adjustsFontSizeToFitWidth = true
    }
    
    //MARK:- UIButton Actions
    @IBAction func doneAc(_ sender: Any) {
        if (validUserData()) {
            var exchang = self.txtField_Phone.finalText
            if let rOriginal = exchang.range(of: " -") {
                exchang = exchang.replacingCharacters(in: rOriginal, with: ")")
            }
            
            var phoneNoStr:String = ""
            phoneNoStr = exchang
            
            let numberAsInt = Int(exchang)
            
            if let no = numberAsInt {
                phoneNoStr = "\(no)"
            }
            
            constantVC.GlobalVariables.ph_no = "\(dialCodeStr)\(phoneNoStr)"
            defaults.set(constantVC.GlobalVariables.ph_no, forKey: constantVC.GeneralConstants.UserPhone)
            
            //generate OTP via Firebase
            self.generateOTP()
        }
    }
    
    
    func generateOTP(){
        
        self.startAnimating()
        FirebaseOTP.shared.sendVerificationOTP(phoneno: "+" + constantVC.GlobalVariables.ph_no) { (success) in
            if success {
                let next = self.storyboard?.instantiateViewController(withIdentifier: "RegisterOtpVC") as! RegisterOtpVC
                 next.modalPresentationStyle = .overFullScreen
                self.present(next, animated: true, completion: nil)
            } else {
                FTIndicator.showError(withMessage: "Something went wrong.")
            }
            self.stopAnimating()
        }
    }
    
    @IBAction func getCodeAc(_ sender: Any) {
        let picker = MICountryPicker()
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    
    @IBAction func btn_keyboard_action(_ sender: UIButton) {
        var str = self.txtField_Phone.text
        self.txtField_Phone.text = ""
        self.txtField_Phone.text  = "\(str)\(sender.tag)"
        
        str = ""
    }
    
    @IBAction func btn_keyboardCross_action(_ sender: UIButton) {
        
        if self.txtField_Phone.text!.count > 0 {
            self.txtField_Phone.text!.remove(at: self.txtField_Phone.text!.index(before: self.txtField_Phone.text!.endIndex))
        }
    }
    
    //MARK:- UICountryPicker Delegates
    func countryPicker(picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        countryCodeLab.text = "\(name) \(dialCode)"
        dialCodeStr = dialCode
        SessionManager.saveCountryCode(code: code)
        print("\(name) \(dialCode)")
        self.imgVw_flag.image = UIImage(named:code.lowercased() + ".png")
        self.dismiss(animated: true, completion: nil)
    }
    
    func countryPicker(picker: MICountryPicker, didSelectCountryWithName name: String, code: String) {
    }
    
}
