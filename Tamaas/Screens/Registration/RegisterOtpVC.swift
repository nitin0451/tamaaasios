

import UIKit
import FTIndicator

class RegisterOtpVC: BaseViewController , UITextFieldDelegate {
     
     //MARK:- Outlets
     @IBOutlet weak var btn_next: UIButton!
     @IBOutlet weak var lbl_welcome: UILabel!
     @IBOutlet weak var lbl_verifyNumber: UILabel!
     @IBOutlet weak var lbl_didnotReceiveCode_msg: UILabel!
     @IBOutlet weak var btn_resend: UIButton!
     @IBOutlet weak var stLabel: UILabel!
     @IBOutlet weak var txtField_otp: VSTextField!
     
     //MARK:- UIView life-cycle Methods
     override func viewDidLoad() {
          super.viewDidLoad()
          
          txtField_otp.isUserInteractionEnabled = false
          txtField_otp.setFormatting("######", replacementChar: "#")
          self.localise_language()
          setupView()
          viewToLoad()
     }
     
     override func viewWillAppear(_ animated: Bool) {
          UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 217.0/255.0, green: 6.0/255.0, blue: 71.0/255.0, alpha: 0.6)
     }
     
     override func viewWillDisappear(_ animated: Bool) {
          UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
     }
     
     //MARK:- UITextFieldDelegates
     func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
          return false
     }
     
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          let maxLength = 4
          let currentString: NSString = textField.text! as NSString
          let newString: NSString =
               currentString.replacingCharacters(in: range, with: string) as NSString
          return newString.length <= maxLength
     }
     
     //MARK:- Custom Methods
     func localise_language(){
          self.btn_next.setTitle("Next".localized, for: .normal)
          self.lbl_welcome.text = "Welcome!".localized
          self.lbl_verifyNumber.text = "Verify your number.".localized
          self.lbl_didnotReceiveCode_msg.text = "Didn't receive the code?".localized
          self.btn_resend.setTitle( "Resend".localized, for: .normal)
          self.txtField_otp.placeholder = "Enter OTP".localized
     }
     
     func setupView(){
          
          if SessionManager.get_localizeLanguage() == "tr" {
               lbl_didnotReceiveCode_msg.frame.origin.x = 20
               lbl_didnotReceiveCode_msg.frame.size.width = 150
               btn_resend.frame.origin.x = 172
               lbl_didnotReceiveCode_msg.frame.size.width = 150
          }
          else{
               
               lbl_didnotReceiveCode_msg.frame.origin.x = 8
               btn_resend.frame.origin.x = 213
               lbl_didnotReceiveCode_msg.frame.size.width = 203
               btn_resend.frame.size.width = 98
          }
     }
     
     func viewToLoad(){
          let text = "Please enter verification code sent to your number".localized
          let formattedString = NSMutableAttributedString()
          formattedString
               .normal(text + " ")
               .bold(constantVC.GlobalVariables.ph_no)
          stLabel.attributedText = formattedString
          // txtField_otp.text = constantVC.GlobalVariables.otpString
     }
     
     func validUserData() -> Bool {
          if let otpEmpty = txtField_otp.text?.isEmpty, !otpEmpty {
               return true
          }
          showError(message: "OTP is required")
          return false
     }
     
     
     //MARK:- UIButton Actions
     @IBAction func nxtAc(_ sender: Any) {
          
          if (validUserData()) {
               self.startAnimating()
               //verify OTP
               FirebaseOTP.shared.verfiyOTP(verificationCode: txtField_otp.text) { (success) in
                    if success {
                         self.loginUser()
                         let next = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
                        next.modalPresentationStyle = .fullScreen
                        self.present(next, animated: true, completion: nil)
                    }
                    else {
                         FTIndicator.showError(withMessage: "Something went wrong.")
                    }
                    self.stopAnimating()
               }
          }
     }
     
     @IBAction func resendAc(_ sender: Any) {
          self.generateOTP()
     }
     
     func generateOTP(){
          
          self.startAnimating()
          FirebaseOTP.shared.sendVerificationOTP(phoneno: "+" + constantVC.GlobalVariables.ph_no) { (success) in
               if success {
                    self.txtField_otp.text = ""
               } else {
                    FTIndicator.showError(withMessage: "Something went wrong.")
               }
               self.stopAnimating()
          }
     }
     
     @IBAction func backAc(_ sender: Any) {
          self.dismiss(animated: true, completion: nil)
     }
     
     @IBAction func btn_keyboard_action(_ sender: UIButton) {
          
          self.txtField_otp.text = self.txtField_otp.text! + "\(sender.tag)"
     }
     
     @IBAction func btn_keyboardCross_action(_ sender: UIButton) {
          
          if self.txtField_otp.text!.count > 0 {
               self.txtField_otp.text!.remove(at: self.txtField_otp.text!.index(before: self.txtField_otp.text!.endIndex))
          }
     }
     
     
     
     //MARK:- Web services
     func loginUser() {
          view.isUserInteractionEnabled = false
          
          let user = QBUUser()
          user.login = SessionManager.getPhone()
          user.password = SessionManager.getPhone()
          user.phone = SessionManager.getPhone()
          //user.fullName = self.nameTextView.text.trimmingCharacters(in: .whitespaces)
          
          QuickBloxManager().logIn(user: user) { (success , msg) in
               self.dismissLoader()
               if success {
                    
               } else {
                    //FTIndicator.showError(withMessage: msg)
               }
          }
     }
     
}
