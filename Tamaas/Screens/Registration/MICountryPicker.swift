//
//  MICountryPicker.swift
//  MICountryPicker
//
//  Created by Ibrahim, Mustafa on 1/24/16.
//  Copyright © 2016 Mustafa Ibrahim. All rights reserved.
//

import UIKit

class MICountry: NSObject {
    @objc let name: String
    let code: String
    var section: Int?
    let dialCode: String!
    
    init(name: String, code: String, dialCode: String = " - ") {
        self.name = name
        self.code = code
        self.dialCode = dialCode
    }
}

struct Section {
    var countries: [MICountry] = []
    
    mutating func addCountry(country: MICountry) {
        countries.append(country)
    }
}

@objc public protocol MICountryPickerDelegate: class {
    func countryPicker(picker: MICountryPicker, didSelectCountryWithName name: String, code: String)
    @objc optional func countryPicker(picker: MICountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String)
}

public class MICountryPicker: UITableViewController, UISearchBarDelegate {
    
    
    
    
    
    public var customCountriesCode: [String]?
    
     var searchController = UISearchController()
     var filteredList = [MICountry]()
     var unsourtedCountries : [MICountry] {
        //let locale = NSLocale.current as NSLocale
        let locale = NSLocale.init(localeIdentifier: "en")
        var unsourtedCountries = [MICountry]()
        let countriesCodes = customCountriesCode == nil ? NSLocale.isoCountryCodes : customCountriesCode!
        
        for countryCode in countriesCodes {
            let displayName = locale.displayName(forKey: .countryCode, value: countryCode ?? "")
            let countryData = CallingCodes.filter { $0["code"] == countryCode }
            let country: MICountry
            
            if countryData.count > 0, let dialCode = countryData[0]["dial_code"] {
                country = MICountry(name: displayName!, code: countryCode, dialCode: dialCode)
                unsourtedCountries.append(country)
            }
        }
        
        return unsourtedCountries
    }
    
     var _sections: [Section]?
     var sections: [Section] {
        
        if _sections != nil {
            return _sections!
        }
        
        let countries: [MICountry] = unsourtedCountries.map { country in
            let country = MICountry(name: country.name, code: country.code, dialCode: country.dialCode)
            country.section = collation.section(for: country, collationStringSelector: #selector(getter: MICountry.name))
            return country
        }
        
        // create empty sections
        var sections = [Section]()
        for _ in 0..<self.collation.sectionIndexTitles.count {
            sections.append(Section())
        }
        
        // put each country in a section
        for country in countries {
            sections[country.section!].addCountry(country: country)
        }
        
        // sort each section
      /*  for section in sections {
//            var s = section
//            s.countries = collation.sortedArray(from: section.countries, collationStringSelector: #selector(getter: MTLFunction.name)) as! [MICountry]
            
            section.countries = section.countries.sorted(by: { (Obj1, Obj2) -> Bool in
                let Obj1_Name = Obj1.firstname ?? ""
                let Obj2_Name = Obj2.firstname ?? ""
                return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedAscending)
            })
        }*/
        
        
        for i in 0..<sections.count {
            sections[i].countries = sections[i].countries.sorted(by: { (Obj1, Obj2) -> Bool in
                let Obj1_Name = Obj1.name ?? ""
                let Obj2_Name = Obj2.name ?? ""
                return (Obj1_Name.localizedCaseInsensitiveCompare(Obj2_Name) == .orderedAscending)
            })
        }
        
        _sections = sections
        
        return _sections!
    }
     let collation = UILocalizedIndexedCollation.current()
        as UILocalizedIndexedCollation
    public weak var delegate: MICountryPickerDelegate?
    public var didSelectCountryClosure: ((String, String) -> ())?
    public var didSelectCountryWithCallingCodeClosure: ((String, String, String) -> ())?
    public var showCallingCodes = false

    convenience public init(completionHandler: @escaping ((String, String) -> ())) {
        self.init()
        self.didSelectCountryClosure = completionHandler
    }
    
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        createSearchBar()
        inititlizeBarButtons()
        tableView.reloadData()
        definesPresentationContext = true
    }
     public override func viewWillAppear(_ animated: Bool) {
            UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
     }
    // MARK: Methods
     public override func viewWillDisappear(_ animated: Bool) {
            UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
     }
    func inititlizeBarButtons() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(onTouchCancelButton))
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    @objc func onTouchCancelButton() {
        dismiss(animated: true, completion: nil)
    }
    
    private func createSearchBar() {
       searchController = ( {
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.hidesNavigationBarDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.delegate = self
            controller.searchBar.placeholder = "Search Country Code"
            self.tableView.tableHeaderView = controller.searchBar
            return controller
        })()
    }
    
     func filter(searchText: String) -> [MICountry] {
        filteredList.removeAll()
        
        sections.forEach { (section) -> () in
            section.countries.forEach({ (country) -> () in
                if country.name.count >= searchText.count {
                    let result = country.name.compare(searchText, options: [.caseInsensitive, .diacriticInsensitive], range: searchText.startIndex ..< searchText.endIndex)
                    if result == .orderedSame {
                        filteredList.append(country)
                    }
                }
            })
        }
        return filteredList
    }
}

// MARK: - Table view data source

extension MICountryPicker {
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.searchBar.isFirstResponder {
            return 1
        }
        return sections.count
    }
    
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.searchBar.isFirstResponder {
            return filteredList.count
        }
        return sections[section].countries.count
    }
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tempCell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell")
        
        if tempCell == nil {
            tempCell = UITableViewCell(style: .default, reuseIdentifier: "UITableViewCell")
        }
        
        let cell: UITableViewCell! = tempCell
        let country: MICountry!
        if searchController.searchBar.isFirstResponder {
            country = filteredList[indexPath.row]
        } else {
            country = sections[indexPath.section].countries[indexPath.row]
        }
        
        if let dialCode = country.dialCode {
            cell.textLabel?.text =  "\(country.name) \(dialCode)"
        }
        
        cell.imageView!.image = UIImage(named:country.code.lowercased() + ".png")
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchController.searchBar.isFirstResponder {
            return ""
        }
        if !sections[section].countries.isEmpty {
            return self.collation.sectionTitles[section] as String
        }
        return ""
    }
    
    
    override public func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return collation.sectionIndexTitles
    }
    
    
    override public func tableView(_ tableView: UITableView,
                                   sectionForSectionIndexTitle title: String,
                                   at index: Int)
        -> Int {
            return collation.section(forSectionIndexTitle: index)
    }
}

// MARK: - Table view delegate

extension MICountryPicker {
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        let country: MICountry!
        if searchController.searchBar.isFirstResponder {
            country = filteredList[indexPath.row]
        } else {
            country = sections[indexPath.section].countries[indexPath.row]
            
        }
        delegate?.countryPicker(picker: self, didSelectCountryWithName: country.name, code: country.code)
        delegate?.countryPicker?(picker: self, didSelectCountryWithName: country.name, code: country.code, dialCode: country.dialCode)
        didSelectCountryClosure?(country.name, country.code)
        didSelectCountryWithCallingCodeClosure?(country.name, country.code, country.dialCode)
    }
    
}

// MARK: - UISearchDisplayDelegate

extension MICountryPicker: UISearchResultsUpdating {
    public func updateSearchResults(for searchController: UISearchController) {
        filter(searchText: searchController.searchBar.text!)
        tableView.reloadData()
    }
    
}
