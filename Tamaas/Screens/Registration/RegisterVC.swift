//
//  RegisterVC.swift
//  Tamaas
//
//  Created by Krescent Global on 12/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import Alamofire
import Contacts
import AANotifier
import PKImagePicker
import Crashlytics
import GradientProgressBar
import FTIndicator
import CropViewController
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class PhoneContact: NSObject {
    var phonNumber:String = ""
    var name:String = ""
    
    init(name: String, phoneNo: String) {
        self.name = name
        self.phonNumber = phoneNo
    }
}

class RegisterVC: BaseViewController{
    
    //MARK:- Outlets
    @IBOutlet weak var buttonAreYouDoc: UIButton!
    @IBOutlet weak var btn_importMyContacts: UIButton!
    @IBOutlet weak var btn_skip: UIButton!
    @IBOutlet weak var lbl_fetchMsg: UILabel!
    @IBOutlet weak var lbl_Note: UILabel!
    @IBOutlet weak var vw_fetchContacts: UIView!
    @IBOutlet weak var vw_gradiantBar: GradientProgressBar!
    @IBOutlet weak var lbl_fetchingMsg: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var aboutMeTextView: UITextView!
    @IBOutlet weak var nameTextView: UITextView!
    @IBOutlet weak var lbl_setUpProfile: UILabel!
    @IBOutlet weak var btn_fb: UIButton!
    @IBOutlet weak var btn_done: UIButton!
    @IBOutlet weak var vw_blurBg: UIView!
    @IBOutlet weak var vw_importContacts: UIView!
    
    //MARK:- Variables
    let picker = PKImagePickerViewController()
    var chosenImage : UIImage!
    var contactsStore: CNContactStore?
    var isFBData:Bool = false
    var dictParam = Dictionary<String, String>()
    let gradient = CAGradientLayer()
    var gradientSet = [[CGColor]]()
    var currentGradient: Int = 0
    let gradientOne = UIColor.yellow.cgColor
    let gradientTwo = UIColor.black.cgColor
    let gradientThree = UIColor.yellow.cgColor
    var myTimer: Timer?
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vw_blurBg.isHidden = true
        self.vw_importContacts.isHidden = true
        self.vw_fetchContacts.isHidden = true
        self.localise_language()
        viewToLoad()
        if #available(iOS 13.0, *) {
            self.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }
    }
    
    //MARK:- Web services
    func webserviceForSignUp(){
        let actiView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
            actiView.backgroundColor = constantVC.GlobalColor.bgColor
            let activityIN : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50)) as UIActivityIndicatorView
            activityIN.center = self.view.center
            activityIN.hidesWhenStopped = true
            activityIN.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
            activityIN.startAnimating()
            actiView.addSubview(activityIN)
            self.view.addSubview(actiView)
        
        let token = UIDevice.current.identifierForVendor!.uuidString
        let nameField = self.nameTextView.text!
        let aboutField = self.aboutMeTextView.text!
        let type = self.buttonAreYouDoc.isSelected ? "DOCTOR" : ""
        if self.isFBData {
            self.chosenImage = self.profileImage.image
        }
        
     Alamofire.upload(
        multipartFormData: { multipartFormData in
        
           if self.chosenImage != nil {
             multipartFormData.append(UIImageJPEGRepresentation(self.chosenImage!, 0.2)!, withName: "photoURL", fileName: "\(SessionManager.getPhone()).jpeg", mimeType: "image/jpeg")
           }
        
            multipartFormData.append("\(nameField)".data(using: String.Encoding.utf8)!, withName: "name")
            multipartFormData.append("\(aboutField)".data(using: String.Encoding.utf8)!, withName: "about")
            multipartFormData.append("\(token)".data(using: String.Encoding.utf8)!, withName: "deviceToken")
            multipartFormData.append("\(SessionManager.getPhone())".data(using: String.Encoding.utf8)!, withName: "phoneNumber")
            multipartFormData.append("\(type)".data(using: String.Encoding.utf8)!, withName: "type")

        if SessionManager.get_QuickBloxID() != nil {
             multipartFormData.append("\(String(describing: SessionManager.get_QuickBloxID()!))".data(using: String.Encoding.utf8)!, withName: "quickbloxID")
        }
     },
     to: "\(constantVC.GeneralConstants.BASE_URL)\(constantVC.WebserviceName.URL_SIGNUP)",
     encodingCompletion: { encodingResult in
     
     switch encodingResult {
     case .success(let upload, _, _):
     upload.responseJSON { response in
     
    guard response.result.error == nil else {
        actiView.removeFromSuperview()
        return
    }
     
     if let value = response.result.value {
        if let result = value as? Dictionary<String, AnyObject> {

             let resultMessage = result["msg"] as! String
             if resultMessage == "success"{
                if let isDoc = result["doctor"] as? Int,isDoc == 1 {
                    self.showLoginDocView()
                    return
                }
                
                let tokenParts = constantVC.GlobalVariables.updatedPushToken?.map { data -> String in
                    return String(format: "%02.2hhx", data)
                }
                if let token_parts = tokenParts {
                    let deviceTokenString_ = token_parts.joined()
                    WebserviceSigleton().callToAddFCMToken(token: deviceTokenString_)
                }
                
                SessionManager.save_isUserLoggedInAfterUpdate(isHaving: true)
                constantVC.ActiveDataSource.isActiveSignUp = true
                QuickBloxManager().loginAndGetDialogs()
                constantVC.GlobalVariables.isNeedToShowInitialConnectingBar = false
                SessionManager.save_notificationActiveStatus(isHaving: true)
                
                let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                
                if let token = constantVC.GlobalVariables.updatedPushToken {
                    appDelegate?.application(UIApplication.shared , didRegisterForRemoteNotificationsWithDeviceToken: token)
                }
               
                self.defaults.set(SessionManager.getPhone(), forKey: constantVC.GeneralConstants.Identity)
                SessionManager.setUsername(userName: self.nameTextView.text!)
                SessionManager.setUserId(userId: result["user_id"] as! String)
                SessionManager.setAboutUser(about: self.aboutMeTextView.text!)
                actiView.removeFromSuperview()
                self.vw_blurBg.isHidden = false
                self.vw_importContacts.isHidden = false
                AppType.isProduction = true
                self.logUser()  //for crashlytics
                let updateParameters = QBUpdateUserParameters()
                updateParameters.fullName = self.nameTextView.text!
                updateParameters.externalUserID = UInt(SessionManager.getUserId()) ?? 0
                QuickBloxManager().updateUserInfo(param: updateParameters)
                
                //register for push notifications
                Core.instance.registerForRemoteNotifications(withDeviceToken: constantVC.GlobalVariables.updatedPushToken)
                
                
             }
             else{
                 actiView.removeFromSuperview()
                 self.showError(message: result["msg_details"] as! String)
             }
            }
        }
     }
     .uploadProgress { progress in // main queue by default
     
     }
     return
     case .failure(let encodingError):
         actiView.removeFromSuperview()
     debugPrint(encodingError)
        }
    })
    }
  
    func Webservice_addDeviceUUID(){
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["UUID": UIDevice.current.identifierForVendor!.uuidString]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_ADD_DEVICE_UUID, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
        })
    }
    
    func showLoginDocView() {
        let alertController = UIAlertController(title: "Log In Error", message: "You are not an approved Practitioner.\n Please email info@tamaaas.com to be added. Thank you for your service.", preferredStyle: .alert)
             
             let cancelAction = UIAlertAction(title: "OK".localized, style: UIAlertActionStyle.cancel) {
                 UIAlertAction in
                let next = self.storyboard?.instantiateViewController(withIdentifier: "InitViewController") as! InitViewController
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = next
                appDelegate.window?.makeKeyAndVisible()
                self.present(next, animated: true, completion: nil)
                 NSLog("Cancel Pressed")
             }
             
             // Add the actions
             alertController.addAction(cancelAction)
             // Present the controller
             self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- UIButton Actions - Import Contacts
    @IBAction func btn_importMyContacts(_ sender: UIButton) {
        self.vw_importContacts.isHidden = true
        
        DispatchQueue.main.async {
            self.vw_blurBg.isHidden = false
            self.vw_fetchContacts.isHidden = false
            self.vw_gradiantBar.progress = 0.0
            self.showProgressView()
        }
        
        DispatchQueue.global(qos: .background).async {
            self.importContacts()
        }
    }
    
    
    func importContacts(){
        let update_NS = Update_Get_allLinked_Contacts()
        update_NS.getContacts()
        
        ContactManager().getAddressBookAndUpdate { (success) in
            self.vw_blurBg.isHidden = true
            self.vw_fetchContacts.isHidden = true
            self.view.alpha = 1.0
            self.vw_blurBg.isHidden = true
            self.vw_fetchContacts.isHidden = true
            if !(success) {
                FTIndicator.showError(withMessage: "Error in import Contacts!")
            }
            let next = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! UITabBarController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = next
           // self.present(next, animated: true, completion: nil)
        }
    }
    
    @IBAction func buttonActionAreYouDoc(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btn_skip(_ sender: UIButton) {
        let update_NS = Update_Get_allLinked_Contacts()
        //update_NS.getContacts()
        self.vw_blurBg.isHidden = true
        self.vw_importContacts.isHidden = true
        
        let next = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! UITabBarController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = next
        //self.present(next, animated: true, completion: nil)
    }
    @IBAction func profileAc(_ sender: Any) {
        self.view.endEditing(true)
        picker.delegate = self
        present(picker, animated: false, completion: nil)
        
    }
    @IBAction func doneAc(_ sender: Any) {
        if (validUserData()) {
            self.view.endEditing(true)
            self.Webservice_addDeviceUUID()
            webserviceForSignUp()
        }
    }
    
    @IBAction func btn_getUserDetailsFrom_FB_action(_ sender: UIButton) {
        
        if let _ = AccessToken.current
        {
            self.getFBUserData()
        }
        else {
            let fbLoginManager :  LoginManager = LoginManager()
            fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
                if (error == nil){
                    let fbloginresult : LoginManagerLoginResult = result!
                    if fbloginresult.grantedPermissions != nil {
                        if(fbloginresult.grantedPermissions.contains("email"))
                        {
                            self.getFBUserData()
                            //fbLoginManager.logOut()
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Custom Methods
    func viewToLoad(){
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        nameTextView.placeholder = "Add your name and profile picture".localized
        aboutMeTextView.placeholder = "Write something about yourself to introduce the world".localized
        dictParam["action"] = "CreateContactList"
        dictParam["UserNumber"] = SessionManager.getPhone()
        //self.chosenImage = UIImage(named: "contact_big")
    }
    
    func localise_language(){
        self.lbl_setUpProfile.text = "Set up Your Profile".localized
        self.btn_fb.setTitle( "Import your profile details from Facebook".localized, for: .normal)
        self.btn_done.setTitle( "Done".localized, for: .normal)
        //import contacts
        self.lbl_Note.text = "Find your friends".localized
        self.lbl_fetchMsg.text = "importContactsMessage".localized
        self.btn_skip.setTitle("Skip".localized, for: .normal)
        self.btn_importMyContacts.setTitle("Import My Contacts".localized, for: .normal)
        self.lbl_fetchingMsg.text = "We are fetching your contacts".localized
    }
    
    func showProgressView(){
        if self.vw_gradiantBar.progress >= 1 {
            self.vw_gradiantBar.progress = 0.0
        }
        // schedule timer
        self.myTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
    }
    @objc func updateProgress() {
        self.vw_gradiantBar.progress += 0.01
        if self.vw_gradiantBar.progress >= 1 {
            self.myTimer?.invalidate()
            self.vw_gradiantBar.progress = 0.0
            showProgressView()
        }
    }
    
    func validUserData() -> Bool {

        if nameTextView.text.trimmingCharacters(in: .whitespaces) == ""{
            showError(message: "Please enter the username")
            return false
        }
        else{
            return true
        }
    }
    
    func logUser() {
        Crashlytics.sharedInstance().setUserName(SessionManager.getUsername())
        Crashlytics.sharedInstance().setUserIdentifier(SessionManager.getPhone())
    }

    func getFBUserData(){
        
        if((AccessToken.current) != nil){
            self.isFBData = true
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    let dict = result as! [String : AnyObject]
                    
                    if let userName = dict["name"] as? String
                    {
                        self.nameTextView.placeholder = ""
                        self.nameTextView.text = userName
                    }
                    
                    if let profilePictureObj = dict["picture"] as? NSDictionary
                    {
                        let data = profilePictureObj.value(forKey: "data") as! NSDictionary
                        let pictureUrlString  = data.value(forKey: "url") as! String
                        if let url = URL.init(string: pictureUrlString) {
                            self.profileImage.downloadedFrom(url: url, contentMode: .scaleAspectFill)
                        }
                        else {
                        }
                  }
                }
                else {
                }
            })
        }
    }
 
    
}
extension RegisterVC :PKImagePickerViewControllerDelegate{
    func imageSelected(_ img: UIImage!) {
        chosenImage = img
        
        let cropViewController = CropViewController(image: chosenImage)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    func imageSelectionCancelled() {
    }
}

extension RegisterVC: CropViewControllerDelegate {
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.chosenImage = image
        profileImage.image = self.chosenImage
        dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        dismiss(animated: true, completion: nil)
    }
}




