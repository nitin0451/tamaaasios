//
//  MakeCall_ContactPicker.swift
//  Tamaas
//
//  Created by Krescent Global on 12/06/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
import FTIndicator

class MakeCall_ContactPicker: BaseViewController  {


    //MARK:- Outlets
    @IBOutlet weak var tblVw: UITableView!
    @IBOutlet weak var barBtn_cancel: UIButton!
    @IBOutlet weak var lbl_title: UILabel!

    //MARK:- Variables
    var arrContact_section:[ContactSection] = []
    let collation = UILocalizedIndexedCollation.current()
        as UILocalizedIndexedCollation
    var contactsStore: CNContactStore?
    var baseVC = BaseViewController()
    var filteredList     = [MyContact]()
    var unsourtedContacts = [MyContact]()
    var searchController: UISearchController!
    var shouldShowSearchResults:Bool = false
    var dictToContacts : Dictionary<String, String>?
    var users : [QBUUser] = []
  
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.barBtn_cancel.setTitle("Cancel".localized, for: .normal)
        self.lbl_title.text = "New Call".localized
        self.configureSearchController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblVw.delegate = self
        self.tblVw.dataSource = self
        self.cleanQuickBloxCache()
        self.getQuickBloxRegisteredUsers()
    }
    
    
    // MARK: Methods
    func getQuickBloxRegisteredUsers(){
        
        // Fetching users from cache.
        let users = ServicesManager.instance().usersService.usersMemoryStorage.unsortedUsers()
        if users.count > 0 {
            guard let users = ServicesManager.instance().sortedUsers() else {
                self.baseVC.stopAnimating()
                return
            }
            self.setupUsers(users: users)
        }
            
        DispatchQueue.global(qos: .background).async {
            QuickBloxManager().retriveRegisteredUsersFromAddressBook { (users) in
                if (users?.count)! > 0 {
                    DispatchQueue.main.async {
                        self.setupUsers(users: users!)
                    }
                }
            }
        }
    }
    
    func getSortedRegisteredUsers(users: [QBUUser] , completionHandler: @escaping (Bool) -> Void) {
        
         self.unsourtedContacts.removeAll()
        
        if (users.count) > 0 {
            for item in users {
                
                let contact: MyContact
                var name:String = ""
                var phone:String = ""
                var photo:String = ""
               
                if let ph = item.login as? String {
                    phone = ph
                }
                if let _photo = item.website as? String {
                    photo = _photo
                }
                if phone.count > 4 {
                    name = SessionManager.getNameFromMyAddressBook(number: phone)
                    contact = MyContact(name: name , phoneNo: phone , id: item.id , photo: photo)
                    self.unsourtedContacts.append(contact)
                }
            }
            
            //sort users
            let contactsArr: [MyContact] = self.unsourtedContacts.map { contact in
                let contact = MyContact(name: contact.name, phoneNo: contact.phoneNo, id: contact.id! ,photo: contact.photo!)
                contact.section = self.collation.section(for: contact, collationStringSelector: #selector(getter: MyContact.name))
                return contact
            }
            
            // create empty sections
            var sectionscon = [ContactSection]()
            for _ in 0..<self.collation.sectionIndexTitles.count {
                sectionscon.append(ContactSection())
            }
            
            // put each country in a section
            for contact in contactsArr {
                sectionscon[contact.section!].addContact(contact: contact)
            }
            
            // sort each section
            for section in sectionscon {
                var s = section
                s.contacts = self.collation.sortedArray(from: section.contacts, collationStringSelector: Selector("name")) as! [MyContact]
            }
            self.arrContact_section = sectionscon
            completionHandler(true)
        } else{
            completionHandler(false)
        }
    }
    
    func setupUsers(users: [QBUUser]) {
        self.users = users
        self.getSortedRegisteredUsers(users: self.users) { (success) in
            if success {
                self.baseVC.stopAnimating()
                self.tblVw.reloadData()
            } else {
                self.baseVC.stopAnimating()
            }
        }
    }
    
    func cleanQuickBloxCache(){
        self.unsourtedContacts = []
    }

    //MARK:- UIButton Actions
    @IBAction func btnCancel_action(_ sender: UIButton) {
        self.dismiss(animated: true , completion: nil)
    }
   
    @objc func btn_makeVoiceCall_action(sender: CustomButton) {

        if QuickBloxManager().callsAllowed() {
            
            let contact: MyContact!
            
            if shouldShowSearchResults {
                contact = filteredList[sender.row]
            }
            else {
                contact = arrContact_section[sender.section].contacts[sender.row]
            }
            
            if let userID = contact.id {
                
                if let privateChatDialog = ServicesManager.instance().chatService.dialogsMemoryStorage.privateChatDialog(withOpponentID: userID) {
                    self.callTo = contact.phoneNo
                    self.callBy = SessionManager.getPhone()
                    
                    if !constantVC.ActiveSession.isStartedConnectingCall {
                        constantVC.ActiveSession.isStartedConnectingCall = true
                        CallConnectionManager.instance.prepareCall_AUDIO(opponentNo: self.callTo , dialog: privateChatDialog , groupName: "")
                    }
                }
                else {
                    ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: userID, contact.phoneNo + "-" + SessionManager.getPhone(), isDoctor: false, fees: "") { (response, chatDialog) in
                        if response.isSuccess {
                            
                            if let dialog_ = chatDialog {
                                QuickBloxManager().sendSystemMsg_updateDialog_toOccupants(dialog: dialog_)
                                
                                self.callTo = contact.phoneNo
                                self.callBy = SessionManager.getPhone()
                                
                                if !constantVC.ActiveSession.isStartedConnectingCall {
                                    constantVC.ActiveSession.isStartedConnectingCall = true
                                    CallConnectionManager.instance.prepareCall_AUDIO(opponentNo: self.callTo , dialog: dialog_ , groupName: "")
                                }
                            }
                        }
                    }
                }
            }
        } else {
            FTIndicator.showError(withMessage: "No Internet Connection!".localized)
        }
    }
    
    
    var callTo:String = ""
    var callBy:String = ""
    var isGroup:String = "false"

    @objc func btn_makeVideoCall_action(sender: CustomButton) {
        
        if QuickBloxManager().callsAllowed() {
            
            let contact: MyContact!
            
            if shouldShowSearchResults {
                contact = filteredList[sender.row]
            }
            else {
                contact = arrContact_section[sender.section].contacts[sender.row]
            }
            
            if let userID = contact.id {
                
                if let privateChatDialog = ServicesManager.instance().chatService.dialogsMemoryStorage.privateChatDialog(withOpponentID: userID) {
                    
                    self.callTo = contact.phoneNo
                    self.callBy = SessionManager.getPhone()
                    
                    if !constantVC.ActiveSession.isStartedConnectingCall {
                        constantVC.ActiveSession.isStartedConnectingCall = true
                        CallConnectionManager.instance.prepareCall_VIDEO(opponentNo: self.callTo , dialog: privateChatDialog , groupName: "")
                    }
                }
                else {
                    ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: userID, contact.phoneNo + "-" + SessionManager.getPhone(), isDoctor: false, fees: "") { (response, chatDialog) in
                        if response.isSuccess {
                            
                            if let dialog_ = chatDialog {
                                QuickBloxManager().sendSystemMsg_updateDialog_toOccupants(dialog: dialog_)
                                
                                self.callTo = contact.phoneNo
                                self.callBy = SessionManager.getPhone()
                                
                                if !constantVC.ActiveSession.isStartedConnectingCall {
                                    constantVC.ActiveSession.isStartedConnectingCall = true
                                    CallConnectionManager.instance.prepareCall_VIDEO(opponentNo: self.callTo , dialog: dialog_ , groupName: "")
                                }
                            }
                        }
                    }
                }
            }
        } else {
            FTIndicator.showError(withMessage: "No Internet Connection!".localized)
        }
    }

    //MARK:- Helpers
    func configureSearchController() {

        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Friends".localized
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()

        // Place the search bar view to the tableview headerview.
        tblVw.tableHeaderView = searchController.searchBar
    }

    func filter(searchText: String) -> [MyContact] {
        filteredList.removeAll()

        arrContact_section.forEach { (section) -> () in
            section.contacts.forEach({ (contact) -> () in
                if contact.name.count >= searchText.count {

                    let filterdObject = contact.name.contains(searchText) || contact.phoneNo.contains(searchText)

                    if (filterdObject) {
                        filteredList.append(contact)
                    }
                }
            })
        }
        return filteredList
    }

}


 //MARK: - Table view data source

extension MakeCall_ContactPicker: UITableViewDelegate , UITableViewDataSource {


     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        if shouldShowSearchResults {
            return 1
        }
        return arrContact_section.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if shouldShowSearchResults {
            return filteredList.count
        }
        return arrContact_section[section].contacts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MakeCall_ContactPicker_TableCell
        cell.accessoryType = UITableViewCellAccessoryType.none
        cell.imgVw_user.cornerRadius = cell.imgVw_user.frame.size.width / 2
        cell.imgVw_user.clipsToBounds = true

        let contact: MyContact!

        if shouldShowSearchResults {
            contact = filteredList[indexPath.row]
        }
        else {
            contact = arrContact_section[indexPath.section].contacts[indexPath.row]
        }

        cell.lbl_userName.text =  "\(contact.name)"
        cell.imgVw_user.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(contact.phoneNo).jpeg") , placeholder: UIImage(named:"contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)


        //add action
        cell.btn_makeVoiceCall.addTarget(self, action: #selector(btn_makeVoiceCall_action), for: .touchUpInside)
        cell.btn_makeVoiceCall.row = indexPath.row
        cell.btn_makeVoiceCall.section = indexPath.section
        cell.btn_makeVideoCall.addTarget(self, action: #selector(btn_makeVideoCall_action), for: .touchUpInside)
        cell.btn_makeVideoCall.row = indexPath.row
        cell.btn_makeVideoCall.section = indexPath.section

        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if shouldShowSearchResults {
            return ""
        }
        if !arrContact_section[section].contacts.isEmpty {
            return self.collation.sectionTitles[section] as String
        }
        return ""
    }
}

// MARK: - UISearchDisplayDelegate
extension MakeCall_ContactPicker: UISearchResultsUpdating , UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        filter(searchText: searchController.searchBar.text!)
        tblVw.reloadData()
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        shouldShowSearchResults = true
        tblVw.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        shouldShowSearchResults = false
        tblVw.reloadData()
    }
}


class MakeCall_ContactPicker_TableCell:UITableViewCell {
    
    @IBOutlet weak var imgVw_user: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var btn_makeVoiceCall: CustomButton!
    @IBOutlet weak var btn_makeVideoCall: CustomButton!
    
}
