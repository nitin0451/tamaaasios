import UIKit

class SessionManager {
   
    static let IsLoggedInKey: String = "loggedIn"
    static let defaults = UserDefaults.standard
    
    //MARK:- COUNTRY
    class func getCountry() -> String{
        if let code = defaults.object(forKey: "countryCode") as? String {
            return countryName(from: code)
        }
        return ""
    }
       
    class func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
             // Country name was found
             return name
        } else {
             // Country name cannot be found
             return countryCode
        }
    }
    
    class func loginWithUsername(username:String) {
       
        defaults.set(true, forKey: IsLoggedInKey)
        defaults.synchronize()
    }
    
    class func logout() {
        defaults.removeObject(forKey: constantVC.GeneralConstants.Identity)
        defaults.set(false, forKey: IsLoggedInKey)
        defaults.synchronize()
    }
    
    class func isLoggedIn() -> Bool {
        let isLoggedIn = defaults.bool(forKey: IsLoggedInKey)
        if (isLoggedIn) {
            return true
        }
        return false
    }
    
    class func getUserId() -> String {
        if let userId = defaults.object(forKey: constantVC.GeneralConstants.UserID) as? String {
            return userId
        }
        return ""
    }
    
    class func setUserId(userId : String) {
        defaults.set(userId, forKey: constantVC.GeneralConstants.UserID)
    }
    
    class func setUserProfile_URL(url : String) {
        defaults.set(url, forKey: constantVC.GeneralConstants.myProfile_URL)
    }
    
    class func getUsername() -> String {
        if let username = defaults.object(forKey: constantVC.GeneralConstants.UserName) as? String {
            return username
        }
        return ""
    }
    
    class func setUsername(userName : String) {
        defaults.set(userName, forKey: constantVC.GeneralConstants.UserName)
    }
    
    class func getAboutUser() -> String {
        if let about = defaults.object(forKey: constantVC.GeneralConstants.AboutUser) as? String {
            return about
        }
        return ""
    }
    
    class func getUserProfile_URL() -> String {
        if let url = defaults.object(forKey: constantVC.GeneralConstants.myProfile_URL) as? String {
            return url
        }
        return ""
    }
    
    class func setAboutUser(about : String) {
        defaults.set(about, forKey: constantVC.GeneralConstants.AboutUser)
    }
    
    class func getPhone() -> String {
        if let phone = defaults.object(forKey: constantVC.GeneralConstants.UserPhone) as? String {
            
            let phoneVar =  phone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
           
            
            return phoneVar
        }
        return ""
    }
    
    //MARK:- CONTACTS
   /* class func saveContacts(contacts : Dictionary<String, String>?) {
        defaults.set(contacts, forKey: "contacts")
    }
    
    class func getContacts() -> Dictionary<String, String>? {
        if let contacts = defaults.object(forKey: "contacts") as? Dictionary<String, String>? {
            return contacts
        }
        return [:]
    }*/
    
    
    //MARK:- ME HAVING STORY
    class func save_isHaingStory(isHaving:Bool) {
        defaults.set(isHaving, forKey: "isHavingStory")
    }
    
    class func get_isHaingStory() -> Bool {
        return defaults.bool(forKey: "isHavingStory")
    }
    
    
    //MARK:- MSG UNREAD COUNT
    class func save_unreadMsgCount(count:Int) {
        defaults.set(count , forKey: "msgUnreadCount")
    }
    
    class func get_unreadMsgCount() -> Int {
        if let count = defaults.object(forKey: "msgUnreadCount") as? Int {
            print(count)
            return count
        }
        return 0
    }
    
    //MARK:- NOTIFICATION UNREAD COUNT
    class func save_unreadNotificationCount(count:Int) {
        defaults.set(count , forKey: "notificationUnreadCount")
    }
    
    class func get_unreadNotificationCount() -> Int {
        if let count = defaults.object(forKey: "notificationUnreadCount") as? Int {
            return count
        }
        return 0
    }
    
    
    //MARK:- LOCALIZE LANGUAGE
    class func save_localizeLanguage(lang:String) {
        defaults.set(lang , forKey: "localizeLanguage")
    }
    
    class func get_localizeLanguage() -> String {
        if let lang = defaults.object(forKey: "localizeLanguage") as? String {
            return lang
        }
        return ""
    }
    
    class func get_localizeLanguage_completeName() ->String {
        if SessionManager.get_localizeLanguage() != "" {
            if SessionManager.get_localizeLanguage() == "en"{
                return "english"
            }
            if SessionManager.get_localizeLanguage() == "fa-AF"{
                return "persian"
            }
            if SessionManager.get_localizeLanguage() == "ps-AF"{
                return "pashto"
            }
        }
        return ""
    }
    
    
    class func get_countryDialCode() ->String{
        if SessionManager.getCountryCode() != "" {
            if let strDialCode = SessionManager.getCountryCallingCode(countryRegionCode: SessionManager.getCountryCode()) as? String {
                return strDialCode
            }
        }
        return ""
    }
    
    
   class func getCountryCallingCode(countryRegionCode:String)->String{
        
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
    }
    
    
    //MARK:- ME UNLOCK STICKERS
    class func save_isUnlockStickers_sports(isHaving:Bool) {
        defaults.set(isHaving, forKey: "isUnlockStickersSports")
    }
    
    class func get_isUnlockStickers_sports() -> Bool {
        return defaults.bool(forKey: "isUnlockStickersSports")
    }
    
    class func save_isUnlockStickers_famous(isHaving:Bool) {
        defaults.set(isHaving, forKey: "isUnlockStickersFamous")
    }
    
    class func get_isUnlockStickers_famous() -> Bool {
        return defaults.bool(forKey: "isUnlockStickersFamous")
    }
    
    
    //MARK:- NOTIFICATION ACTIVE STATUS
    class func save_notificationActiveStatus(isHaving:Bool) {
        defaults.set(isHaving, forKey: "notiActiveStatus")
    }
    
    class func get_notificationActiveStatus() -> Bool {
        return defaults.bool(forKey: "notiActiveStatus")
    }
    
    //MARK:- Print log data
    class func printLOG(data:String) {
       // print(data)
    }
    
    //MARK:- ELITE MATCH USER
    class func save_Elite_userID(lang:String) {
        defaults.set(lang , forKey: "eliteUserId")
    }
    
    class func get_Elite_userID() -> String {
        if let id = defaults.object(forKey: "eliteUserId") as? String {
            return id
        }
        return ""
    }
    
    //MARK:- QUICK BLOX - USER_ID
    class func save_QuickBloxID(id:NSNumber) {
        defaults.set(id , forKey: "QuickBloxID")
    }
    
    class func get_QuickBloxID() -> NSNumber? {
        if let id = defaults.object(forKey: "QuickBloxID") as? NSNumber {
            return id
        }
        return nil
    }
    
    //MARK:- QUICK BLOX - BLOB_ID
    class func save_QuickBloxBlobID(id:NSNumber) {
        defaults.set(id , forKey: "QuickBloxBlobID")
    }
    
    class func get_QuickBloxBlobID() -> NSNumber? {
        if let id = defaults.object(forKey: "QuickBloxBlobID") as? NSNumber {
            return id
        }
        return nil
    }
    
    //MARK:- UUID
    class func getAndSave_UUID() -> String {
        defaults.set(UIDevice.current.identifierForVendor!.uuidString , forKey: "device_UUID")
        return self.get_UUID()
    }
    
    class func get_UUID() -> String {
        if let id = defaults.object(forKey: "device_UUID") as? String {
            return id
        }
        return ""
    }
    
    
    //MARK:- COUNTRY CODE
    class func saveCountryCode(code:String){
        defaults.set(code , forKey: "countryCode")
    }
    
    class func getCountryCode() -> String {
        if let code = defaults.object(forKey: "countryCode") as? String {
            return code
        }
        return ""
    }
    
    //MARK:- OTP VERIFICATION CODE
    class func saveOTPVerificationID(code:String){
        defaults.set(code , forKey: "verificationID")
    }
    
    class func getOTPVerificationID() -> String {
        if let code = defaults.object(forKey: "verificationID") as? String {
            return code
        }
        return ""
    }
    
    //MARK:- MY CONTACTS DIC
    class func save_dictMyContacts(dict:Dictionary<String, String>?) {
        defaults.set(dict , forKey: "dictMyContacts")
    }
    
    class func get_dictMyContacts() -> Dictionary<String, String>? {
        if let dict = defaults.object(forKey: "dictMyContacts") as? Dictionary<String, String> {
            return dict
        }
        return nil
    }
    
    class func getNameFromMyAddressBook(number:String) ->String {
        if SessionManager.get_dictMyContacts() != nil {
            let no = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            if let name = SessionManager.get_dictMyContacts()?[no]{
                return name
            }
        }
        return "+" + number
    }
    
    
    class func checkUserSavedInPhoneBook(number:String) ->Bool {
        if SessionManager.get_dictMyContacts() != nil {
            let no = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
            if let name = SessionManager.get_dictMyContacts()?[no]{
                return true
            }
        }
        return false
    }
    
    
    //MARK:- Chat Blocked Users
    class func save_chatBlockedUsersID(IDS: [String]) {
        defaults.set(IDS , forKey: "chatBlockedUsers")
    }
    
    class func get_chatBlockedUsersID() -> [String]? {
        if let arr = defaults.object(forKey: "chatBlockedUsers") as? [String]? {
            return arr
        }
        return nil
    }
    
    class func isChatUserBlocked(userID: String) ->Bool {
        if let IDS = SessionManager.get_chatBlockedUsersID() {
            if IDS.contains(userID) {
                return true
            }
        }
        return false
    }
    
    
    //MARK:- LOGGED OUT
    class func save_isUserLoggedInAfterUpdate(isHaving:Bool) {
        defaults.set(isHaving, forKey: "isUserLoggedInAfterUpdate")
    }
    
    class func get_isUserLoggedInAfterUpdate() -> Bool {
        return defaults.bool(forKey: "isUserLoggedInAfterUpdate")
    }
    
    
    //MARK:- Skip New Version
    class func save_NewVersionSkipped(version:String) {
        defaults.set(version , forKey: "isNewVersionSkipped")
    }
    
    class func get_NewVersionSkipped() -> String {
        
        if let version = defaults.value(forKey: "isNewVersionSkipped") as? String {
            return version
        }
        return ""
    }
 
    
    //MARK:- OFFLINE MESSAGES
    class func saveUnSentMsgs_ids(ids : String) {
        defaults.set(ids, forKey: "UnSentMsgs_ids")
    }
    
    class func getUnSentMsgs_ids() -> String?{
        if let about = defaults.object(forKey: "UnSentMsgs_ids") as? String {
            return about
        }
        return nil
    }
    
}

struct PlatformUtils {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}


/*// constants
let APPLE_LANGUAGE_KEY = "AppleLanguages"
/// L102Language
class L102Language {
    /// get current Apple language
    class func currentAppleLanguage() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    /// set @lang to be the first in Applelanguages list
    class func setAppleLAnguageTo(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
}*/


