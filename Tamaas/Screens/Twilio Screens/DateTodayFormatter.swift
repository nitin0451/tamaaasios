import UIKit

class DateTodayFormatter {
    func stringFromDate(date: NSDate?) -> String? {
        guard let date = date else {
            return nil
        }
        let NxtDay = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        
        let messageDate = roundDateToDay(date: date)
        let todayDate = roundDateToDay(date: NSDate())
        let tomarrow  = roundDateToDay(date: NxtDay! as NSDate)
        let formatter = DateFormatter()
        
        if messageDate == todayDate {
            formatter.dateFormat = "hh:mm a"
        }
        else if messageDate == tomarrow{
           return "yesterday".localized
        }
        else {
            formatter.dateFormat = "dd, MMM"
        }
        
        return formatter.string(from: date as Date)
    }
    
    
    func MessageDateFormat_stringFromDate(date: NSDate?) -> String? {
        guard let date = date else {
            return nil
        }
        let NxtDay = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        
        let messageDate = roundDateToDay(date: date)
        let todayDate = roundDateToDay(date: NSDate())
        let tomarrow  = roundDateToDay(date: NxtDay! as NSDate)
        let formatter = DateFormatter()
        
        if messageDate == todayDate {
            formatter.dateFormat = "hh:mm a"
        }
        else if messageDate == tomarrow{
            return "yesterday".localized
        }
        else {
            formatter.dateFormat = "MMM dd,yy"
        }
        
        return formatter.string(from: date as Date)
    }
    
    func GetTime_stringFromDate(date: NSDate?) -> String? {
        guard let date = date else {
            return nil
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        
        return formatter.string(from: date as Date)
    }
    
    
    func GetMonth_stringFromDate(date: NSDate?) -> String? {
        guard let date = date else {
            return nil
        }
        let messageDate = roundDateToDay(date: date)
        let NxtDay = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let todayDate = roundDateToDay(date: NSDate())
        let backDay  = roundDateToDay(date: NxtDay! as NSDate)
        let formatter = DateFormatter()
        
        if messageDate == todayDate {
            return "Today".localized
        }
        else if messageDate == backDay{
            return "Yesterday".localized
        }
        else {
            formatter.dateFormat = "EEE,MMM dd"
        }
        
        return formatter.string(from: date as Date)
    }
    
    func timeAgoSinceDate(date:NSDate) -> String {
        
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now
        var components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        let NxtDay = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let messageDate = roundDateToDay(date: date)
        let todayDate = roundDateToDay(date: NSDate())
        let backDay  = roundDateToDay(date: NxtDay! as NSDate)
        let formatter = DateFormatter()
        
        if messageDate == todayDate {
            if (components.hour! >= 2) {
                return "\(components.hour!) \("hours ago".localized)"
            } else if (components.hour! >= 1){
                return "An hour ago".localized
            } else if (components.minute! >= 2) {
                return "\(components.minute!) \("minutes ago".localized)"
            } else if (components.minute! >= 1){
                return "A minute ago".localized
            } else if (components.second! >= 3) {
                return "\(components.second!) \("seconds ago".localized)"
            } else {
                return "Just now".localized
            }
        }
        else if messageDate == backDay{
            
            if (components.day! == 2) {
                return "\(components.day!) \("days ago".localized)"
            } else if (components.day! == 1){
                return "Yesterday".localized
            }
            else {
                return "Yesterday".localized
            }
        }
        else {
            formatter.dateFormat = "MMMM dd , yyyy"
            return formatter.string(from: date as Date)
        }
        
        return formatter.string(from: date as Date)
    }
    
    
    func roundDateToDay(date: NSDate) -> NSDate {
        let calendar  = Calendar.current
        let flags = Set<Calendar.Component>([.day, .month, .year])
        let components = calendar.dateComponents(flags, from: date as Date)
        
        return calendar.date(from:components)! as NSDate
    }
}

extension NSDate {
    class func dateWithISO8601String(dateString: String) -> NSDate? {
        var formattedDateString = dateString
        
        if dateString.hasSuffix("Z") {
            let lastIndex = dateString.characters.indices.last!
            formattedDateString = dateString.substring(to: lastIndex) + "-000"
        }
        return dateFromString(str: formattedDateString, withFormat:"yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    }
    
    class func dateFromString(str: String, withFormat dateFormat: String) -> NSDate? {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        return formatter.date(from: str) as NSDate?
    }
}

extension Date {
    func getElapsedInterval() -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        if let year = interval.year, year > 0 {
            return formatter.string(from: self as Date)
        } else if let month = interval.month, month > 0 {
            return formatter.string(from: self as Date)
        } else if let day = interval.day, day > 0 {
            
            if day == 1 {
                return "yesterday".localized
            }
            else if day <= 2 {
                return "\(day) \("days ago".localized)"
            }
            else {
                return formatter.string(from: self as Date)
            }
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago".localized  :
                "\(hour)" + " " + "hours ago".localized
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "min ago".localized  :
                "\(minute)" + " " + "mins ago".localized
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "sec ago".localized  :
                "\(second)" + " " + "secs ago".localized
        } else {
            return "Just now".localized
        }
    }
}

extension Date {
    func getElapsedInterval_FEEDS() -> String {
        
        let current_year = Calendar.current.component(.year, from: Date())
        let given_year = Calendar.current.component(.year, from: self)
        let formatter = DateFormatter()
        
        if current_year == given_year {
            formatter.dateFormat = "dd MMMM, h:mm a"
        }
        else {
            formatter.dateFormat = "dd MMMM, yyyy"
        }
        
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return formatter.string(from: self as Date)
        } else if let month = interval.month, month > 0 {
            return formatter.string(from: self as Date)
        } else if let day = interval.day, day > 0 {
            
            if day == 1 {
                formatter.dateFormat = "HH:mm a"
                return "yesterday".localized
            }
            else if day <= 2 {
                return "\(day) \("days ago".localized)"
            }
            else {
                return formatter.string(from: self as Date)
            }
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago".localized :
                "\(hour)" + " " + "hours ago".localized
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "min ago".localized  :
                "\(minute)" + " " + "mins ago".localized
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "sec ago".localized :
                "\(second)" + " " + "secs ago".localized
        } else {
            return "Just now".localized
        }
    }
}


extension Date {
    func getElapsedInterval_with_TIME() -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM,yy HH:mm a"
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        if let year = interval.year, year > 0 {
            return formatter.string(from: self as Date)
        } else if let month = interval.month, month > 0 {
            return formatter.string(from: self as Date)
        } else if let day = interval.day, day > 0 {
            
            if day == 1 {
                formatter.dateFormat = "HH:mm a"
                return "\("yesterday".localized) \(formatter.string(from: self as Date))"
            }
            else if day <= 2 {
                return "\(day) \("days ago".localized)"
            }
            else {
                return formatter.string(from: self as Date)
            }
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "hour ago".localized :
                "\(hour)" + " " + "hours ago".localized
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "min ago".localized :
                "\(minute)" + " " + "mins ago".localized
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "sec ago".localized :
                "\(second)" + " " + "secs ago".localized
        } else {
            return "Just now".localized
        }
    }
}
