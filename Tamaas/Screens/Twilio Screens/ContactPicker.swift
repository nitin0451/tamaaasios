//
//  ContactPicker.swift
//  Tamaas
//
//  Created by Krescent Global on 14/02/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import ContactsUI
import FTIndicator
import Contacts

typealias NewCompletion = ((Any?) -> Void)

class MyContact: NSObject {
    @objc let name: String
    let phoneNo: String
    var section: Int?
    var id: UInt?
    var photo: String?
    init(name: String, phoneNo: String , id: UInt , photo:String) {
        self.name = name
        self.phoneNo = phoneNo
        self.id = id
        self.photo = photo
    }
}

struct ContactSection {
    var contacts: [MyContact] = []
    
    mutating func addContact(contact: MyContact) {
        contacts.append(contact)
    }
}

@objc public protocol ContactPickerDelegate: class {
    func contactPicker(picker: ContactPicker, didSelectContactWithName name: String, phone: String)
   
}

public class ContactPicker: UITableViewController, UISearchBarDelegate , CNContactViewControllerDelegate {
    
    public var customCountriesCode: [String]?
    public weak var delegate: ContactPickerDelegate?
    var searchController = UISearchController()
    var filteredList     = [MyContact]()
    let arrExtraText     = ["Create New Contact".localized,"Create New Group".localized]
    let arrExtraImages   = ["contact_new","contactGroup_new"]
    var baseVC = BaseViewController()
    var unsourtedContacts = [MyContact]()
    var sectionscon = [ContactSection]()
    let collation = UILocalizedIndexedCollation.current()
        as UILocalizedIndexedCollation
    var pageNo:Int = 0
    var users : [QBUUser] = []
    var newBlock : NewCompletion?
    
    //MARK:- UIView life-cycle Methods
    override public func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        createSearchBar()
        definesPresentationContext = true
        self.refreshControl?.frame.origin.x -= 120
        registerContactCell()
    }
    
    func setupUsers(users: [QBUUser]) {
        self.users = users
        self.getSortedRegisteredUsers(users: self.users) { (success) in
            if success {
                self.baseVC.stopAnimating()
                self.tableView.reloadData()
            } else {
                self.baseVC.stopAnimating()
            }
        }
    }
 
    override  public func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:17)!]
        self.navigationController?.navigationBar.topItem?.title = "New Chat".localized
        self.cleanQuickBloxCache()
        self.getQuickBloxRegisteredUsers()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        self.inititlizeBarButtons()
    }
    
    func getQuickBloxRegisteredUsers(){
        
        self.baseVC.startAnimating()
        // Fetching users from cache.
       let users = ServicesManager.instance().usersService.usersMemoryStorage.unsortedUsers()
        
        if users.count > 0 {
            guard let users = ServicesManager.instance().sortedUsers() else {
                self.baseVC.stopAnimating()
                return
            }
            self.setupUsers(users: users)
        }
            
          
        DispatchQueue.global(qos: .background).async {
            QuickBloxManager().retriveRegisteredUsersFromAddressBook { (users) in
                if (users?.count)! > 0 {
                    DispatchQueue.main.async {
                        self.setupUsers(users: users!)
                    }
                } else {
                    self.baseVC.stopAnimating()
                }
            }
        }
     
      /*  QuickBloxManager().retriveRegisteredUsersFromAddressBook { (users) in
            if (users?.count)! > 0 {
                self.setupUsers(users: users!)
            } else {
                self.baseVC.stopAnimating()
            }
        }*/
        
    }
    
    
    
    //MARK:- Custom Methods
    func cleanQuickBloxCache(){
        self.pageNo = 0
        self.unsourtedContacts = []
    }
    
    func getSortedRegisteredUsers(users: [QBUUser] , completionHandler: @escaping (Bool) -> Void) {
        
        self.unsourtedContacts.removeAll()
        
        if (users.count) > 0 {
            for item in users {
                let contact: MyContact
                var name:String = ""
                var phone:String = ""
                var photo:String = ""
                
    //                    if let n = item.fullName as? String {
    //                        name = n
    //                    }
                if let ph = item.login {
                    phone = ph
                }
                if let _photo = item.website {
                    photo = _photo
                }
            
                if phone.count > 4 {
                    name = SessionManager.getNameFromMyAddressBook(number: phone)
                    contact = MyContact(name: name , phoneNo: phone , id: item.id , photo: photo)
                    self.unsourtedContacts.append(contact)
                }
                
            }
                
                //sort users
            let contactsArr: [MyContact] = self.unsourtedContacts.map { contact in
                let contact = MyContact(name: contact.name, phoneNo: contact.phoneNo, id: contact.id! ,photo: contact.photo!)
                contact.section = self.collation.section(for: contact, collationStringSelector: #selector(getter: MyContact.name))
                return contact
            }
                
                // create empty sections
                var sectionscon = [ContactSection]()
                for _ in 0..<self.collation.sectionIndexTitles.count {
                    sectionscon.append(ContactSection())
                }
                
                // put each country in a section
                for contact in contactsArr {
                    sectionscon[contact.section!].addContact(contact: contact)
                }
                
                // sort each section
                for section in sectionscon {
                    var s = section
                    s.contacts = self.collation.sortedArray(from: section.contacts, collationStringSelector: Selector("name")) as! [MyContact]
                }
                self.sectionscon = sectionscon
                completionHandler(true)
            } else{
                completionHandler(false)
        }
    }
    
    
    fileprivate func registerContactCell() {
        let cellNib = UINib(nibName: "EPContactCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "EPContactCell")
    }
   
    func inititlizeBarButtons() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(onTouchCancelButton))
        cancelButton.tintColor = .blue
        self.navigationItem.rightBarButtonItem = cancelButton
        let refreshButton = UIBarButtonItem(title: "Refresh".localized, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onTouchRefreshButton))
        refreshButton.tintColor = .blue
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!, NSAttributedStringKey.foregroundColor: constantVC.GlobalColor.titleColor]
        self.title = ""
        
        self.navigationItem.leftBarButtonItem = refreshButton
    }
    
    @objc func onTouchCancelButton() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func onTouchRefreshButton() {
        
       if Connectivity.isConnectedToInternet() {
        
        self.baseVC.startAnimating()
        self.cleanQuickBloxCache()
        
        switch CNContactStore.authorizationStatus(for: CNEntityType.contacts) {
        case CNAuthorizationStatus.denied, CNAuthorizationStatus.restricted:
            //User has denied the current app to access the contacts.
            self.baseVC.stopAnimating()
            let productName = Bundle.main.infoDictionary!["CFBundleName"]!
            FTIndicator.showError(withMessage: "Unable to access contacts. Tamaaas does not have access to contacts. Kindly enable it in privacy settings")
            
        case CNAuthorizationStatus.notDetermined:
            print("notDetermined")
            
        case  CNAuthorizationStatus.authorized:
            let update_NS = Update_Get_allLinked_Contacts()
            update_NS.getContacts()
        }
       
        //refresh synced contacts
        ContactManager().getAddressBookAndUpdate { (success) in
            if success {
                QuickBloxManager().retriveRegisteredUsersFromAddressBook(completionHandler: { (users) in
                    if (users?.count)! > 0 {
                        self.setupUsers(users: users!)
                    } else {
                        self.baseVC.stopAnimating()
                    }
                })
            }
        }
       } else {
          FTIndicator.showError(withMessage: "No Internet Connection!".localized)
        }
    }
    
    private func createSearchBar() {
        searchController = ( {
            let controller = UISearchController(searchResultsController: nil)
            controller.dimsBackgroundDuringPresentation = false
            controller.searchResultsUpdater = self
            controller.hidesNavigationBarDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.delegate = self
            controller.searchBar.placeholder = "Search Friends".localized
            controller.searchBar.contentMode = .center
            controller.searchBar.barStyle = .default
            controller.searchBar.backgroundColor = UIColor(red:237.0/255.0, green:237.0/255.0, blue:237.0/255.0, alpha:1.0)
            self.tableView.tableHeaderView = controller.searchBar
            return controller
        })()
        
    }
    
    func filter(searchText: String) -> [MyContact] {
        filteredList.removeAll()
        
        sectionscon.forEach { (section) -> () in
            section.contacts.forEach({ (contact) -> () in
                if contact.name.count >= searchText.count {
                   /* let result = contact.name.compare(searchText, options: [.caseInsensitive, .diacriticInsensitive], range: searchText.startIndex ..< searchText.endIndex)
                    if result == .orderedSame {
                        filteredList.append(contact)
                    } */
                    
                    let filterdObject = contact.name.contains(searchText) || contact.phoneNo.contains(searchText)
                   
                    if (filterdObject) {
                        filteredList.append(contact)
                    }
                }
            })
        }
        return filteredList
    }
    
    
    func showNewContactViewController() {
        let newContact = CNContact()
        let contactVC = CNContactViewController(forNewContact: newContact)
        contactVC.delegate = self
        contactVC.title = "Create New Contact"
        self.navigationController?.pushViewController(contactVC , animated: true )
    }
    
    
    public func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        
        self.navigationController?.popViewController(animated: true)
        if contact != nil {
            FTIndicator.showToastMessage("Contact created successfully!")
        }
        self.baseVC.startAnimating()
        self.cleanQuickBloxCache()
        self.getQuickBloxRegisteredUsers()
        
    }
}

// MARK: - Table view data source

extension ContactPicker {
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.searchBar.isFirstResponder {
            return 2
        }
        return sectionscon.count+1
    }
    override public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 2
        }
        if searchController.searchBar.isFirstResponder {
            return filteredList.count
        }
        return sectionscon[section-1].contacts.count
    }
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EPContactCell", for: indexPath) as! EPContactCell
        cell.accessoryType = UITableViewCellAccessoryType.none
     
        cell.contactInitialLabel.isHidden = true
        cell.contactDetailTextLabel.isHidden = true
        
        let contact: MyContact!
        if indexPath.section == 0{
            cell.contactTextLabel?.textColor = UIColor(red: 32.0/255.0, green: 102.0/255.0, blue: 246.0/255.0, alpha: 1.0)
            cell.contactTextLabel?.text = arrExtraText[indexPath.row]
            cell.contactImageView!.image = UIImage(named:arrExtraImages[indexPath.row])
            
        }else{
            cell.contactTextLabel?.textColor = constantVC.GlobalColor.titleColor
            if searchController.searchBar.isFirstResponder {
                contact = filteredList[indexPath.row]
            } else {
                contact = sectionscon[indexPath.section-1].contacts[indexPath.row]
            }
            cell.contactTextLabel?.text = contact.name //SessionManager.getNameFromMyAddressBook(number: contact.phoneNo)
            cell.contactImageView!.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(contact.phoneNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
        }
        cell.contactTextLabel?.font = UIFont.init(name:"CircularStd-Book",size:16)!
        
        
       /* if !searchController.searchBar.isFirstResponder && indexPath.section == sectionscon.count - 1 {
            self.pageNo = self.pageNo + 1
            self.getQuickBloxRegisteredUsers()
        }*/
        
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return ""
        }
        
        if section - 1 < sectionscon.count {
            if !sectionscon[section-1].contacts.isEmpty {
                return self.collation.sectionTitles[section-1] as String
            }
        }
        
        return ""
    }
    
    override public func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return collation.sectionIndexTitles
    }
    
    
    override public func tableView(_ tableView: UITableView,
                                   sectionForSectionIndexTitle title: String,
                                   at index: Int) -> Int {
        return collation.section(forSectionIndexTitle: index)
    }
}

// MARK: - Table view delegate

extension ContactPicker {
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        var contact: MyContact!
        
        if indexPath.section == 0{
            
            if (indexPath.row == 0) {
                self.showNewContactViewController()
            }
            if indexPath.row == 1{
                let groupViewController = CreateNewGroup()
                let navigationController = UINavigationController(rootViewController: groupViewController)
                groupViewController.modalPresentationStyle = .fullScreen

                self.present(navigationController, animated: true, completion: nil)
            }
        }
        else{
            if searchController.searchBar.isFirstResponder {
                contact = filteredList[indexPath.row]
            } else {
                contact = sectionscon[indexPath.section-1].contacts[indexPath.row]
            }
            
            if SessionManager.getPhone() == contact.phoneNo {
                FTIndicator.showToastMessage("This is your own number")
            } else {
                // Creating or fetching private chat.
                if let privateChatDialog = ServicesManager.instance().chatService.dialogsMemoryStorage.privateChatDialog(withOpponentID: contact.id!) {
                    constantVC.openPrivateChat.isActiveOpenPrivateChat = true
                    
                    self.dismiss(animated: true) {
                        guard let block = self.newBlock else {return}
                        block(privateChatDialog)
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
//                        controller.isPresented = true
//                        controller.modalPresentationStyle = .fullScreen
//                        controller.dialog = privateChatDialog
//                        self.present(controller , animated: true , completion: nil)
                    }
                } else {
                    ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: contact.id!,contact.phoneNo + "-" + SessionManager.getPhone() , isDoctor: false, fees: "") { (response, chatDialog) in
                        if response.isSuccess {
                            
                            if let dialog_ = chatDialog {
                                QuickBloxManager().sendSystemMsg_updateDialog_toOccupants(dialog: dialog_)
                            }
                            
                            constantVC.openPrivateChat.isActiveOpenPrivateChat = true
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                            controller.dialog = chatDialog
                            controller.isPresented = true
                            self.present(controller , animated: true , completion: nil)
                        
                        }
                    }
                }
            }
        }
    }
}

// MARK: - UISearchDisplayDelegate

extension ContactPicker: UISearchResultsUpdating {
    public func updateSearchResults(for searchController: UISearchController) {
        filter(searchText: searchController.searchBar.text!)
        tableView.reloadData()
    }
}
