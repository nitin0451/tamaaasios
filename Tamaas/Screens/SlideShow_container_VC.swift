//
//  SlideShow_container_VC.swift
//  Tamaas
//
//  Created by Apple on 23/10/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class SlideShow_container_VC: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var imageLoader: UIActivityIndicatorView!
    
    //MARK:- Variables
    var imageUrl = String()
    var msgID:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        if imageUrl != "" {
            loadImage()
        }
        self.view.backgroundColor = .clear
     }
    
     func loadImage(){
          self.showLoader()
          let loader = QMImageLoader.instance
          let url = URL.init(string: imageUrl)
          loader.downloadImage(with: url!, transform: nil, options: [.highPriority , .continueInBackground , .allowInvalidSSLCertificates ], progress: nil, completed: { image, transfomedImage, error, cacheType, finished, imageURL in
               
               if error == nil && image != nil {
                    self.imgVw.image = loader.originalImage(with: imageURL)
               }
               self.hideLoader()
          })
     }
     func showLoader(){
          self.imageLoader.isHidden = false
          self.imageLoader.startAnimating()
     }
     func hideLoader(){
          self.imageLoader.stopAnimating()
          self.imageLoader.isHidden = true
     }

}

let imageCache = NSCache<NSString, UIImage>()
extension UIImageView {
     func loadImageWithUrl(urlString: String,completion: @escaping (Bool) -> ()) {
          
          image = nil
          
          if let cachedImage = imageCache.object(forKey: urlString as NSString) {
               image = cachedImage
                completion(true)
               return
          }
          if let url = URL(string: urlString) {
               let session = URLSession.shared
               let dataTask = session.dataTask(with: url) { (data, response, error) in
                    if let unwrappedError = error {
                         print(unwrappedError)
                         completion(true)
                         return
                    }
                    if let unwrappedData = data, let downloadedImage = UIImage(data: unwrappedData) {
                         DispatchQueue.main.async(execute: {
                              imageCache.setObject(downloadedImage, forKey: urlString as NSString)
                              self.image = downloadedImage
                              completion(true)
                         })
                    }
               }
               dataTask.resume()
          }
     }
}
