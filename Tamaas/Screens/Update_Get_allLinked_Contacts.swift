//
//  Update_Get_allLinked_Contacts.swift
//  Tamaas
//
//  Created by Krescent Global on 02/07/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import Contacts
import FTIndicator
import Alamofire
import PhoneNumberKit

class Update_Get_allLinked_Contacts: NSObject {
    
    //MARK:- Variables
    var contactsStore: CNContactStore?
    var dictParam = Dictionary<String, String>()
    var arrPhoneNumbers:[String] = []
    let phoneNumberKit = PhoneNumberKit()
    
    func getContacts() ->[String]{
        
        if contactsStore == nil {
            //ContactStore is control for accessing the Contacts
            contactsStore = CNContactStore()
        }
        let error = NSError(domain: "EPContactPickerErrorDomain", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Contacts Access"])
        
        switch CNContactStore.authorizationStatus(for: CNEntityType.contacts) {
        case CNAuthorizationStatus.denied, CNAuthorizationStatus.restricted:
            //User has denied the current app to access the contacts.
            
            let productName = Bundle.main.infoDictionary!["CFBundleName"]!
            FTIndicator.showError(withMessage: "Unable to access contacts. Tamaaas does not have access to contacts. Kindly enable it in privacy settings")
            
        case CNAuthorizationStatus.notDetermined:
            
            //This case means the user is prompted for the first time for allowing contacts
            contactsStore?.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) -> Void in
                //At this point an alert is provided to the user to provide access to contacts. This will get invoked if a user responds to the alert
                if  (!granted ){
                }
                else{
                    self.getContacts()
                }
            })
            
        case  CNAuthorizationStatus.authorized:
            let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
            var name   = ""
            var phone  = ""
            
            do {
                
                // Get all the containers
                var allContainers: [CNContainer] = []
                do {
                    allContainers = (try contactsStore?.containers(matching: nil))!
                } catch {
                    print("Error fetching containers")
                }
                
                var results: [CNContact] = []
                self.arrPhoneNumbers = []
                
                // Iterate all containers and append their contacts to our results array
                for container in allContainers {
                    let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                    
                    do {
                        let containerResults = try contactsStore?.unifiedContacts(matching: fetchPredicate, keysToFetch: allowedContactKeys())
                        results.append(contentsOf: containerResults!)
                    } catch {
                        print("Error fetching results for container")
                    }
                }
                
                for contact in results {
                    
                    if contact.phoneNumbers.count > 0{
                        for number in contact.phoneNumbers {
                            var str = number.value.stringValue
                           // str = str.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                            phone = str
                            
                            if let username = contact.givenName as? String{
                                name = username
                            }
                            
                            //parse and format for country code
                            do {
                                let parsedPhNo = try phoneNumberKit.parse(phone , withRegion: SessionManager.getCountryCode() , ignoreType: true)
                                if let formatPhNo = phoneNumberKit.format(parsedPhNo, toType: .e164) as? String{
                                    phone = formatPhNo
                                }
                            } catch {
                               // print(error.localizedDescription)
                            }
                            
                            phone = phone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                            arrPhoneNumbers.append(phone)
                            self.dictParam[phone] = name
                        }
                    }
                }
                
                self.webserviceToupdateContact()
                
                return arrPhoneNumbers
            }
            catch let error as NSError {
               // print(error.localizedDescription)
            }
        }
        
        return []
    }
    
    func allowedContactKeys() -> [CNKeyDescriptor]{
        
        return [CNContactNamePrefixKey as CNKeyDescriptor,
                CNContactGivenNameKey as CNKeyDescriptor,
                CNContactImageDataKey as CNKeyDescriptor,
                CNContactThumbnailImageDataKey as CNKeyDescriptor,
                CNContactImageDataAvailableKey as CNKeyDescriptor,
                CNContactPhoneNumbersKey as CNKeyDescriptor]
    }
    
    func webserviceToupdateContact(){
        
        dictParam["UserNumber"] = SessionManager.getPhone()
        self.dictParam[SessionManager.getPhone()] = SessionManager.getUsername()
        
        let webservice  = WebserviceSigleton ()
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_CREATE_CONTACT_LIST, params: self.dictParam as Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
               // let resultMessage = response["msg"] as! String
               // if resultMessage == "success"{
                    //self.webserviceToGetAllUsers()
              //  }
            }
        })
    }
    
    func webserviceToGetAllUsers(){
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["UserNumber":SessionManager.getPhone()]
        
        
       
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GETALL_USERS, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    if let userDetails = response["user_details"] as? [[String:Any]] {
                        constantVC.GlobalVariables.userDetails = userDetails
                    }
                }
            }
        })
    }
}
