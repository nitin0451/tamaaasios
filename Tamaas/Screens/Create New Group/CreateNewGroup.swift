
import UIKit
import Contacts
import FTIndicator


public class CreateNewGroup: UITableViewController, UISearchBarDelegate {

    var baseVC = BaseViewController()
    var isSearchActive:Bool = false
    var isAddParticipants:Bool = false
    var dialog: QBChatDialog!
    public var customCountriesCode: [String]?
    public weak var delegate: ContactPickerDelegate?
    var searchController = UISearchController()
    var filteredList     = [MyContact]()
    var arrSelectedContacts = [MyContact]()
    var arrAlreadyAdded_groupMembersIDs = [NSNumber]()
    var unsourtedContacts = [MyContact]()
    var users : [QBUUser] = []
    var sectionscon = [ContactSection]()
    let collation = UILocalizedIndexedCollation.current()
        as UILocalizedIndexedCollation
    var contactsStore: CNContactStore?
    //    var refreshC: UIRefreshControl!
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        createSearchBar()
        inititlizeBarButtons()
        registerContactCell()

        //set already added members
        definesPresentationContext = true
    }
    override  public func viewWillAppear(_ animated: Bool) {
       // self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]
        self.cleanQuickBloxCache()
        self.getQuickBloxRegisteredUsers()
    }
    
    
    // MARK: Methods
    
    func getQuickBloxRegisteredUsers(){
        
        // Fetching users from cache.
        let users = ServicesManager.instance().usersService.usersMemoryStorage.unsortedUsers()
        if users.count > 0 {
            guard let users = ServicesManager.instance().sortedUsers() else {
                self.baseVC.stopAnimating()
                return
            }
            self.setupUsers(users: users)
        }
            
            
        DispatchQueue.global(qos: .background).async {
            QuickBloxManager().retriveRegisteredUsersFromAddressBook { (users) in
                if (users?.count)! > 0 {
                    DispatchQueue.main.async {
                        self.setupUsers(users: users!)
                    }
                }
            }
        }
    }
    
    func getSortedRegisteredUsers(users: [QBUUser] , completionHandler: @escaping (Bool) -> Void) {
        
        self.unsourtedContacts.removeAll()
        
        if (users.count) > 0 {
            for item in users {
                let contact: MyContact
                var name:String = ""
                var phone:String = ""
                var photo:String = ""
                
                if let ph = item.login as? String {
                    phone = ph
                }
                if let _photo = item.website as? String {
                    photo = _photo
                }
                
                if phone.count > 4 {
                    name = SessionManager.getNameFromMyAddressBook(number: phone)
                    contact = MyContact(name: name , phoneNo: phone , id: item.id , photo: photo)
                    self.unsourtedContacts.append(contact)
                }
            }
            
            //sort users
            let contactsArr: [MyContact] = self.unsourtedContacts.map { contact in
                let contact = MyContact(name: contact.name, phoneNo: contact.phoneNo, id: contact.id! ,photo: contact.photo!)
                contact.section = self.collation.section(for: contact, collationStringSelector: #selector(getter: MyContact.name))
                return contact
            }
            
            // create empty sections
            var sectionscon = [ContactSection]()
            for _ in 0..<self.collation.sectionIndexTitles.count {
                sectionscon.append(ContactSection())
            }
            
            // put each country in a section
            for contact in contactsArr {
                sectionscon[contact.section!].addContact(contact: contact)
            }
            
            // sort each section
            for section in sectionscon {
                var s = section
                s.contacts = self.collation.sortedArray(from: section.contacts, collationStringSelector: Selector("name")) as! [MyContact]
            }
            self.sectionscon = sectionscon
            completionHandler(true)
        } else{
            completionHandler(false)
        }
    }
    
    func setupUsers(users: [QBUUser]) {
        self.users = users
        self.getSortedRegisteredUsers(users: self.users) { (success) in
            if success {
                self.baseVC.stopAnimating()
                self.tableView.reloadData()
            } else {
                self.baseVC.stopAnimating()
            }
        }
    }
    
    func cleanQuickBloxCache(){
        self.unsourtedContacts = []
    }
    
    func inititlizeBarButtons() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(onTouchCancelButton))
        self.navigationItem.leftBarButtonItem = cancelButton

        var title = "Next"

        if isAddParticipants {
            title = "Add"
        }

        let nextButton = UIBarButtonItem(title: title, style: UIBarButtonItemStyle.plain, target: self, action: #selector(onTouchNextButton))
        self.navigationItem.rightBarButtonItem = nextButton
    }
    
    fileprivate func registerContactCell() {
       let cellNib = UINib(nibName: "NewGroupCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "NewGroupCell")
    }
    
    @objc func onTouchCancelButton() {
        dismiss(animated: true, completion: nil)
    }

    @objc func onTouchNextButton() {

        if arrSelectedContacts.count != 0{
            if isAddParticipants {

                var arrOccupantID:[NSNumber] = []
                var arrOccupantNo:[String] = []
                                
                baseVC.showLoader(strForMessage: "Adding participants".localized)
                
                for user in arrSelectedContacts {
                    if let id = user.id {
                       arrOccupantID.append(NSNumber(value: id))
                    }
                    if let no = user.phoneNo as? String {
                        arrOccupantNo.append(no)
                    }
                }
                
                ServicesManager.instance().chatService.joinOccupants(withIDs: arrOccupantID , to: self.dialog, completion: { (response , dialog) in
                    if response.isSuccess {
                        
                        
                        //notify new group members
                        for memberNo in arrOccupantNo {
                            let notificationText = SessionManager.getPhone() + " " + "added" + " " + memberNo
                            
                            ServicesManager.instance().chatService.sendSystemMessageAboutAdding(to: dialog! , toUsersIDs: (dialog?.occupantIDs)! , withText: notificationText , completion: { (error) in
                            
                            ServicesManager.instance().chatService.sendNotificationMessageAboutAddingOccupants((dialog?.occupantIDs)! , to: dialog! , withNotificationText: notificationText)
                            
                            })
                        }
                        
                        self.baseVC.dismissLoader()
                        FTIndicator.showToastMessage("Participants added successfully!")
                        self.dismiss(animated: true , completion: nil )
                    } else {
                        self.baseVC.dismissLoader()
                        FTIndicator.showError(withMessage: "Error in adding participants")
                        self.dismiss(animated: true , completion: nil )
                    }
                })
            }
            else {
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "AddGroupInfo") as! AddGroupInfo
                vc.modalPresentationStyle = .fullScreen
                vc.arrConatcts = arrSelectedContacts
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
        private func createSearchBar() {
        searchController = ( {
            let controller = UISearchController(searchResultsController: nil)
            controller.dimsBackgroundDuringPresentation = false
            controller.searchResultsUpdater = self
            controller.hidesNavigationBarDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.delegate = self
            controller.searchBar.placeholder = "Search Friends".localized
            controller.searchBar.contentMode = .center

            self.tableView.tableHeaderView = controller.searchBar
            return controller
        })()
    }

    func filter(searchText: String) -> [MyContact] {

        self.isSearchActive = true
        filteredList.removeAll()

        sectionscon.forEach { (section) -> () in
            section.contacts.forEach({ (contact) -> () in
                if contact.name.count >= searchText.count {

                    let filterdObject = contact.name.contains(searchText) || contact.phoneNo.contains(searchText)

                  //  let result = contact.name.compare(searchText, options: [.caseInsensitive, .diacriticInsensitive], range: searchText.startIndex ..< searchText.endIndex)
//                    if result == .orderedSame {
//                        filteredList.append(contact)
//                    }

                    if (filterdObject) {
                        filteredList.append(contact)
                    }
                }
            })
        }
        return filteredList
    }



    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.isSearchActive = false
    }

    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    }

}

 //MARK: - Table view data source

extension CreateNewGroup {

    override open func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.searchBar.isFirstResponder || isSearchActive {
            return 1
        }
        return sectionscon.count
    }


    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.searchBar.isFirstResponder || isSearchActive {
            return filteredList.count
        }
        return sectionscon[section].contacts.count
    }
    public override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63.0
    }
    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

      let cell = tableView.dequeueReusableCell(withIdentifier: "NewGroupCell", for: indexPath) as! NewGroupCell
         cell.accessView.backgroundColor = UIColor.clear
         cell.isUserInteractionEnabled = true

        let contact: MyContact!

            if searchController.searchBar.isFirstResponder || isSearchActive {
                contact = filteredList[indexPath.row]
            } else {
                contact = sectionscon[indexPath.section].contacts[indexPath.row]
            }


        //if already added
        if isAddParticipants {
            if let id = contact.id {
                if arrAlreadyAdded_groupMembersIDs.contains(NSNumber(value: id)) {
                    cell.accessView.backgroundColor = UIColor.lightGray
                    cell.isUserInteractionEnabled = false
                }
            }else {
                cell.accessView.backgroundColor = UIColor.clear
                cell.isUserInteractionEnabled = true
            }
        }

      //when select members
        if arrSelectedContacts.contains(contact){
            cell.accessView.backgroundColor = UIColor.blue
        }
        
        //show name
        cell.userNameLab.text = contact.name
       
        cell.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(contact.phoneNo).jpeg") , placeholder: UIImage(named:"contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
        cell.selectionStyle = .none
        
        return cell
    }

    override open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        if !sectionscon[section].contacts.isEmpty {
            return self.collation.sectionTitles[section] as String
        }
        return ""
    }

    override open func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return collation.sectionIndexTitles
    }


    override open func tableView(_ tableView: UITableView,
                                   sectionForSectionIndexTitle title: String,
                                   at index: Int)
        -> Int {
            return collation.section(forSectionIndexTitle: index)
    }
}

// MARK: - Table view delegate

extension CreateNewGroup {

    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     let cell = tableView.cellForRow(at: indexPath) as! NewGroupCell

        let contact: MyContact!
        if searchController.searchBar.isFirstResponder || isSearchActive {

            contact = filteredList[indexPath.row] 

            if self.arrSelectedContacts.contains(contact){
                if let index = arrSelectedContacts.index(of:contact) {
                    arrSelectedContacts.remove(at: index)
                    cell.accessView.backgroundColor = UIColor.clear
                }
            }
            else{
                arrSelectedContacts.append(contact)
                cell.accessView.backgroundColor = UIColor.blue
            }

        } else {
            contact = sectionscon[indexPath.section].contacts[indexPath.row]

            if arrSelectedContacts.contains(contact){
                if let index = arrSelectedContacts.index(of:contact) {
                    arrSelectedContacts.remove(at: index)
                    cell.accessView.backgroundColor = UIColor.clear
                }
            }
            else{
                arrSelectedContacts.append(contact)
                cell.accessView.backgroundColor = UIColor.blue
            }

        }

     //  tableView.reloadRows(at: [indexPath], with: .none)
       //tableView.deselectRow(at: indexPath, animated: false)
    }
}

 //MARK: - UISearchDisplayDelegate

extension CreateNewGroup: UISearchResultsUpdating {
    public func updateSearchResults(for searchController: UISearchController) {
        filter(searchText: searchController.searchBar.text!)
        tableView.reloadData()
    }

}

