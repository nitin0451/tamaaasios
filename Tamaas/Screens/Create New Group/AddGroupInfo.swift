//
//  AddGroupInfo.swift
//  Tamaas
//
//  Created by Krescent Global on 23/02/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import Alamofire
import FTIndicator
import YYImage
import YYWebImage
import PKImagePicker
import CropViewController

class AddGroupInfo: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate,
UINavigationControllerDelegate{

    //MARK:- Variables
    var arrConatcts = [MyContact]()
    let picker   = PKImagePickerViewController()
    var isEditGroup:Bool = false
    var dialog:QBChatDialog?
    var chosenImage : UIImage? = nil
    var imageName = String()
    
    //MARK:- IBOutlet
    @IBOutlet weak var navItem: UINavigationItem!
    @IBOutlet weak var barbtnCreate: UIBarButtonItem!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var groupNameTF: UITextField!
    @IBOutlet weak var colView: UICollectionView!
    @IBOutlet weak var groupImage: UIImageView!
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.isEditGroup {
            self.showGroupName()
            self.showGroupImg()
            self.navItem.title = ""
            self.barbtnCreate.title = "Save".localized
        } else {
            self.navItem.title = "Create New Group".localized
            self.barbtnCreate.title = "Save".localized
            colView.delegate = self
            colView.dataSource = self
        }
        
        imageName = "groupChat\(String(NSDate().timeIntervalSince1970))"
        imageName = imageName.replacingOccurrences(of: ".", with: "")
        
    }
    
    //MARK:- Custom Methods
    func showGroupName(){
        self.groupNameTF.text = self.dialog?.name
    }
    
    func showGroupImg(){
        if let photo = self.dialog?.photo {
            
            self.groupImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photo).jpeg") , placeholder: nil , options: [.progressiveBlur , .setImageWithFadeAnimation ] ) { (image , url , type , stage , error) in
            }
        }
    }
    
    //MARK:- UIButton Action
    @IBAction func backAc(_ sender: Any) {
         dismiss(animated: true, completion: nil)
    }
    
    func validUserData() -> Bool {
        
        if groupNameTF.text?.trimmingCharacters(in: .whitespaces) == ""{
            showError(message: "Please enter the group name")
            return false
        }
//        else if chosenImage == nil{
//            showError(message: "Image is required")
//            return false
//        }
        else{
            return true
        }
    }
    
    func webserviceForGroupImage(){
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(UIImageJPEGRepresentation(self.chosenImage!, 0.5)!, withName: "Image", fileName: "\(self.imageName).jpeg", mimeType: "image/jpeg")
                
        },
            to: "\(constantVC.GeneralConstants.BASE_URL)\(constantVC.WebserviceName.URL_FILEUPLOAD)",
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        guard response.result.error == nil else {
                            return
                        }
                        
                        }
                        .uploadProgress { progress in // main queue by default
                    }
                    return
                case .failure(let encodingError):
                    debugPrint(encodingError)
                }
        })
        
    }
    
    
    @IBAction func createAc(_ sender: Any) {
        
        self.groupNameTF.resignFirstResponder()
        if arrConatcts.count > 0{
        if self.isEditGroup {
            
            if groupNameTF.text?.trimmingCharacters(in: .whitespaces) == ""{
                showError(message: "Please enter the group name")
                return
            }
            
            
            if groupNameTF.text != self.dialog?.name &&  self.chosenImage != nil {
                self.startAnimating()
                //update both name & image
                self.updateGroupName { (success) in
                    self.updateGroupImage(completionHandler: { (success) in
                        self.stopAnimating()
                        if success {
                            FTIndicator.showToastMessage("Group updated successfully")
                            self.dismiss(animated: true, completion: nil)
                        } else {
                            FTIndicator.showToastMessage("Error in updating group")
                        }
                    })
                }
            }
            else if groupNameTF.text != self.dialog?.name {
                self.startAnimating()
                self.updateGroupName { (success) in
                    self.stopAnimating()
                    if success {
                        FTIndicator.showToastMessage("Group updated successfully")
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        FTIndicator.showToastMessage("Error in updating group")
                    }
                }
            } else {
                if self.chosenImage != nil {
                    self.startAnimating()
                    self.updateGroupImage { (success) in
                        self.stopAnimating()
                        if success {
                            FTIndicator.showToastMessage("Group updated successfully")
                            self.dismiss(animated: true, completion: nil)
                        } else {
                            FTIndicator.showToastMessage("Error in updating group")
                        }
                    }
                }
            }
        }
        else {
            self.CreateGroup()
        }
        }
        else{
            addAlert()
        }
    }
    func addAlert(){
        let alertController = UIAlertController(title:nil , message: "Please add group members", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
            self.dismiss(animated: true, completion : nil)
        }
        // Add the actions
        alertController.addAction(okAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func updateGroupName(completionHandler:@escaping (Bool) -> ()){
        if groupNameTF.text != self.dialog?.name {
            
            //update dilaog
            if let newGroupName = groupNameTF.text?.trimmingCharacters(in: .whitespaces) , let dialog_ = self.dialog {
                ServicesManager.instance().chatService.changeDialogName(newGroupName , for: dialog_) { (response , updatedDialog) in
                    if response.isSuccess {
                        self.dialog = updatedDialog
                        
                        //send local system message
                        QuickBloxManager().sendSystemMessageQB(dialog: dialog_)
                        
                        //send system notification to update dialog name
                        if let groupMembersIDs = QuickBloxManager().getOpponentsID(dialog: dialog_) {
                            ServicesManager.instance().chatService.sendNotificationMessageAboutChangingDialogName(dialog_ , withNotificationText: "Group name updated").continueOnSuccessWith(block: { (task) -> Any? in
                                
                                completionHandler(true)
                                return nil
                            })
                        }
                    }
                    else {
                        completionHandler(false)
                    }
                }
            }
        }
    }
    
    
    func updateGroupImage(completionHandler:@escaping (Bool) -> ()){
        if self.chosenImage != nil {
            
            if let dialog_ = self.dialog {
                //update group image
                Alamofire.upload(
                    multipartFormData: { multipartFormData in
                        multipartFormData.append(UIImageJPEGRepresentation(self.chosenImage!, 0.5)!, withName: "Image", fileName: "\(self.imageName).jpeg", mimeType: "image/jpeg")
                        
                },
                    to: "\(constantVC.GeneralConstants.BASE_URL)\(constantVC.WebserviceName.URL_FILEUPLOAD)",
                    encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                
                                guard response.result.error == nil else {
                                    return
                                }
                                
                                //update dialog
                                ServicesManager.instance().chatService.changeDialogAvatar(self.imageName , for: self.dialog! , completion: { (response , updatedDialog2) in
                                    if response.isSuccess {
                                        self.dialog = updatedDialog2
                                        
                                        //send local system message
                                        QuickBloxManager().sendSystemMessageQB(dialog: updatedDialog2)
                                        
                                        //send system notification to update dialog image
                                        if let groupMembersIDs = QuickBloxManager().getOpponentsID(dialog: dialog_) {
                                            
                                            ServicesManager.instance().chatService.sendNotificationMessageAboutChangingDialogPhoto(dialog_ , withNotificationText: "Group picture updated").continueOnSuccessWith(block: { (task) -> Any? in
                                                
                                                completionHandler(true)
                                                return nil
                                            })
                                        }
                                    }
                                })
                                }
                                .uploadProgress { progress in // main queue by default
                            }
                            return
                        case .failure(let encodingError):
                            completionHandler(false)
                            debugPrint(encodingError)
                        }
                })
            }
        }
    }
   
    
    func CreateGroup(){
        if (validUserData()) {
            
            self.startAnimating()
            self.view.endEditing(true)
            var arrQuickBloxUsers = [QBUUser]()
            
            for contact in arrConatcts{
                let user = QBUUser()
                user.fullName = contact.name
                user.id = contact.id!
                user.phone = contact.phoneNo
                arrQuickBloxUsers.append(user)
            }
            
            if chosenImage != nil{
                //self.chosenImage = UIImage(named: "addNewGroup")
                webserviceForGroupImage()
            } else {
                self.imageName = ""
            }
            
            // Creating group chat.
            ServicesManager.instance().chatService.createGroupChatDialog(withName: groupNameTF.text! , photo: self.imageName , "" , occupants: arrQuickBloxUsers) { [weak self] (response, chatDialog) -> Void in
                
                guard response.error == nil else {
                    self?.stopAnimating()
                    SVProgressHUD.showError(withStatus: response.error?.error?.localizedDescription)
                    return
                }
                
                guard let unwrappedDialog = chatDialog else {
                    return
                }
                
                guard let dialogOccupants = chatDialog?.occupantIDs else {
                    return
                }
                
                guard let strongSelf = self else { return }
                
                let notificationText = SessionManager.getPhone() + " " + "created new group"//strongSelf.updatedMessageWithUsers(users: arrQuickBloxUsers, isNewDialog: true)
                
                ServicesManager.instance().chatService.sendSystemMessageAboutAdding(to: unwrappedDialog , toUsersIDs: dialogOccupants , withText: notificationText , completion: { (error) in
                    
                    ServicesManager.instance().chatService.sendNotificationMessageAboutAddingOccupants(dialogOccupants , to: unwrappedDialog , withNotificationText: notificationText)
                })
                
                strongSelf.stopAnimating()
                constantVC.openPrivateChat.isActiveOpenPrivateChat = true
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                controller.modalPresentationStyle = .fullScreen
                controller.dialog = chatDialog
                self?.present(controller , animated: true , completion: nil)
            }
        }
    }
  
    func updateDialog(dialog:QBChatDialog, newUsers users:[QBUUser], completion: ((_ response: QBResponse?, _ dialog: QBChatDialog?) -> Void)?) {
        
        let usersIDs = users.map{ NSNumber(value: $0.id) }
        
        // Updates dialog with new occupants.
        ServicesManager.instance().chatService.joinOccupants(withIDs: usersIDs, to: dialog) { [weak self] (response, dialog) -> Void in
            
            guard response.error == nil else {
                SVProgressHUD.showError(withStatus: response.error?.error?.localizedDescription)
                
                completion?(response, nil)
                return
            }
            
            guard let unwrappedDialog = dialog else {
                return
            }
            guard let strongSelf = self else { return }
            let notificationText = "New Group Created"//strongSelf.updatedMessageWithUsers(users: users,isNewDialog: false)
            
            // Notifies users about new dialog with them.
            ServicesManager.instance().chatService.sendSystemMessageAboutAdding(to: unwrappedDialog, toUsersIDs: usersIDs, withText: notificationText, completion: { (error) in
                
                ServicesManager.instance().chatService.sendNotificationMessageAboutAddingOccupants(usersIDs, to: unwrappedDialog, withNotificationText: notificationText)
                completion?(response, unwrappedDialog)
            })
        }
    }
    
    func updatedMessageWithUsers(users: [QBUUser],isNewDialog:Bool) -> String {
        
        let dialogMessage = isNewDialog ? "SA_STR_CREATE_NEW".localized : "SA_STR_ADDED".localized
        var message: String = "\(QBSession.current.currentUser!.phone!) " + dialogMessage + " "
        for user: QBUUser in users {
            message = "\(message)\(user.login!),"
        }
        message.remove(at: message.index(before: message.endIndex))
        return message
    }
    
    @IBAction func addImageAc(_ sender: Any) {
        self.view.endEditing(true)
        picker.delegate = self
        present(picker, animated: false, completion: nil)
    }
    
    @IBAction func galleryAc(_ sender: Any) {
       /* self.view.endEditing(true)
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
        removeAnimate(popupView: popupView)*/
    }
    
    @IBAction func cameraAc(_ sender: Any) {
       /* self.view.endEditing(true)
        picker.allowsEditing = true
        picker.sourceType = .camera
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
        removeAnimate(popupView: popupView)*/
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let myImage = info[UIImagePickerControllerOriginalImage] {
            chosenImage = myImage as? UIImage
            groupImage.image = chosenImage
        }
        dismiss(animated: true, completion: nil)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrConatcts.count
       
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/4-4, height: self.view.frame.size.width/4-4);
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupCell", for: indexPath as IndexPath) as! GroupCell
        cell.userImage.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(arrConatcts[indexPath.row].phoneNo)" + ".jpeg") , placeholder: UIImage(named:"contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        cell.lbl_userName.text = SessionManager.getNameFromMyAddressBook(number: arrConatcts[indexPath.row].phoneNo)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         arrConatcts.remove(at: indexPath.row)
        collectionView.reloadData()
    }
    
}

extension AddGroupInfo :PKImagePickerViewControllerDelegate{
    func imageSelected(_ img: UIImage!) {
        chosenImage = img
        
        let cropViewController = CropViewController(image: chosenImage!)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    func imageSelectionCancelled() {
    }
}

extension AddGroupInfo: CropViewControllerDelegate {
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.chosenImage = image
        groupImage.image = chosenImage
        dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        dismiss(animated: true, completion: nil)
    }
}



