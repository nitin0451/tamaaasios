//
//  AddNewContact.swift
//  Tamaas
//
//  Created by Krescent Global on 30/03/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import ContactsUI

class AddNewContact: UIViewController, CNContactViewControllerDelegate {

    @IBOutlet weak var conatctNameLab: UITextField!
    @IBOutlet weak var contactNumberLab: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.navigationController?.navigationBar.isHidden = false
       // self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addContact(_ sender: Any) {
        if (validUserData()) {
            addPhoneNumber(phNo: contactNumberLab.text!)
        }
        
    }
    func addPhoneNumber(phNo : String) {
        if #available(iOS 9.0, *) {
            let store = CNContactStore()
            let contact = CNMutableContact()
            let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :phNo ))
            contact.phoneNumbers = [homePhone]
            
            let controller = CNContactViewController(forUnknownContact : contact)
            controller.contactStore = store
            controller.delegate = self
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController!.pushViewController(controller, animated: true)
        }
    }
    
    func validUserData() -> Bool {
        if let phoneEmpty = contactNumberLab.text?.isEmpty, !phoneEmpty {
            return true
        }
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
