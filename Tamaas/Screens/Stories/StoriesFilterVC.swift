//
//  StoriesFilterVC.swift
//  Tamaas
//
//  Created by Krescent Global on 19/04/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

public protocol StoriesFilterDelegate {
    func StoriesFilterImageDidFilter(image: UIImage)
    func StoriesFilterDidCancel()
}

open class StoriesFilterVC: UIViewController {
        public var delegate: StoriesFilterDelegate?
    @IBOutlet weak var imageView: UIImageView!
    fileprivate let context = CIContext(options: nil)
    var swipeGesture  = UISwipeGestureRecognizer()
    fileprivate var image: UIImage?
    fileprivate var filterIndex = 0
    fileprivate let filterNameList = [
        "No Filter",
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectMono",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer",
        "CILinearToSRGBToneCurve",
        "CISRGBToneCurveToLinear"
    ]
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       
    }
    override open func loadView() {
        if let view = UINib(nibName: "StoriesFilterVC", bundle: Bundle(for: self.classForCoder)).instantiate(withOwner: self, options: nil).first as? UIView {
            self.view = view
            if let image = self.image {
                imageView.image = image
            }
            
            let directions: [UISwipeGestureRecognizerDirection] = [.right, .left]
            
            for direction in directions {
                swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipwView(_:)))
                self.view.addGestureRecognizer(swipeGesture)
                swipeGesture.direction = direction
                imageView.isUserInteractionEnabled = true
                imageView.isMultipleTouchEnabled = true
            }
        }
        
    }
    
    override  open func viewWillAppear(_ animated: Bool) {
       
      //  self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]
    }

    public init(image: UIImage) {
        super.init(nibName: nil, bundle: nil)
        self.image = image
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func closeButtonTapped() {
        if let delegate = self.delegate {
            delegate.StoriesFilterDidCancel()
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtontapped() {
        if let delegate = self.delegate {
            delegate.StoriesFilterImageDidFilter(image: imageView.image!)
        }
      
        dismiss(animated: true, completion: nil)
    }
    
    
    @objc func swipwView(_ sender : UISwipeGestureRecognizer){
        UIView.animate(withDuration: 1.0) {
            if sender.direction == .right { // Swipe right action
                
                self.imageView.frame = CGRect(x: self.view.frame.size.width - self.imageView.frame.size.width, y: self.imageView.frame.origin.y , width: self.imageView.frame.size.width, height: self.imageView.frame.size.height)
                
                if self.filterIndex == 0 {
                    self.filterIndex = self.filterNameList.count - 1
                } else {
                    self.filterIndex -= 1
                }
                if self.filterIndex != 0 {
                    self.imageView.image = self.createFilteredImage(filterName: self.filterNameList[self.filterIndex], image: self.image!)
                } else {
                    self.imageView.image = self.image
                }
                
            }else if sender.direction == .left{ // Swipe left action
                
                self.imageView.frame = CGRect(x: 0, y: self.imageView.frame.origin.y , width: self.imageView.frame.size.width, height: self.imageView.frame.size.height)
                
                
                if self.filterIndex == self.filterNameList.count - 1 {
                    self.filterIndex = 0
                    self.imageView.image = self.image
                } else {
                    self.filterIndex += 1
                }
                if self.filterIndex != 0 {
                    self.applyFilter()
                }
                
            }
            self.imageView.layoutIfNeeded()
            self.imageView.setNeedsDisplay()
        }
    }
    func applyFilter() {
        let filterName = filterNameList[filterIndex]
        if let image = self.image {
            let filteredImage = createFilteredImage(filterName: filterName, image: image)
            self.imageView.image = filteredImage
        }
    }
    
    func createFilteredImage(filterName: String, image: UIImage) -> UIImage {
        // 1 - create source image
        let sourceImage = CIImage(image: image)
        
        // 2 - create filter using name
        let filter = CIFilter(name: filterName)
        filter?.setDefaults()
        
        // 3 - set source image
        filter?.setValue(sourceImage, forKey: kCIInputImageKey)
        
        // 4 - output filtered image as cgImage with dimension.
        let outputCGImage = context.createCGImage((filter?.outputImage!)!, from: (filter?.outputImage!.extent)!)
        //var newImage = UIImage(CIImage:outputImage, scale:imgScale, orientation:imgOrientation)

        
        // 5 - convert filtered CGImage to UIImage
        let filteredImage = UIImage(cgImage: outputCGImage!)
        
        return filteredImage
    }
}
