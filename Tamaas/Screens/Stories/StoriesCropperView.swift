
import UIKit
import MobileCoreServices
import Alamofire
import FTIndicator

class StoriesCropperView: BaseViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var barBtn_cancel: UIBarButtonItem!
    var assestUrl: URL? = nil
    var videoLength:String = ""
    // MARK: Public Properties
    
    var imageToDisplay = UIImage()
    var videoUrl : URL?
    var photos:[PHAsset]?
    var imageViewToDrag: UIImageView!
    var indexPathOfImageViewToDrag: IndexPath!
    let cellWidth = ((UIScreen.main.bounds.size.width)/3)-1
    
    
    // MARK: Private Properties
    
    private let imageLoader = FAImageLoader()
    private var croppedImage: UIImage? = nil
    
    @IBOutlet weak var videoBtn: UIButton!
    @IBOutlet weak var libraryBtn: UIButton!
    @IBOutlet weak var photoBtn: UIButton!
    // MARK: LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localise_language()
        // Do any additional setup after loading the view, typically from a nib.\
       
        photoAc(self);
        
    }
     override func viewWillAppear(_ animated: Bool) {
          print(constantVC.GlobalVariables.galleryClicked)
          if  constantVC.GlobalVariables.galleryClicked == false{
               self.navigationController?.popToRootViewController(animated: true)
               constantVC.GlobalVariables.galleryClicked = true
          }
     }
    
    func localise_language(){
        self.barBtn_cancel.title = "Cancel".localized
        self.libraryBtn.setTitle("Library" , for: .normal)
        self.photoBtn.setTitle("Photo" , for: .normal)
        self.videoBtn.setTitle("Video" , for: .normal)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cancelAc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       
    }
    
    @IBAction func libraryAc(_ sender: Any) {
        constantVC.GlobalVariables.shCamera = true
        libraryBtn.setTitleColor(UIColor(red: 54.0/255.0, green : 54.0/255.0, blue: 54.0/255.0, alpha:1.0), for: UIControlState.normal)
        photoBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        videoBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        checkForPhotosPermission()
    }
    @IBAction func photoAc(_ sender: Any) {
        photoBtn.setTitleColor(UIColor(red: 54.0/255.0, green : 54.0/255.0, blue: 54.0/255.0, alpha:1.0), for: UIControlState.normal)
        libraryBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        videoBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        
        pickFromCamera()
    }
     
    func pickFromCamera(){
     
     let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Photo")
     
     UIView.transition(with: self.view, duration: 0.5, options: .transitionFlipFromLeft, animations: {
          self.present(vc, animated: false)
     }, completion: { _ in
     self.libraryAc(self)
     
     })
     
    }
    
    
    @IBAction func backAc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true)
        if let myImage = info[UIImagePickerControllerOriginalImage] {
            constantVC.GlobalVariables.shCamera = true
            
            let temImg = (myImage as? UIImage)!
            imageToDisplay = temImg.fixOrientation()
            
            let vc = StoriesFilterVC(image: self.imageToDisplay)
            vc.delegate = self
            self.view.alpha = 0
            self.present(vc, animated: true)
           
        }
        else{
             self.view.alpha = 0
            let manager = FileManager.default
            constantVC.GlobalVariables.shCamera = false
            guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else {return}
            guard let mediaType = "mp4" as? String else {return}
            guard let url = info[UIImagePickerControllerMediaURL] as? NSURL else {return}
            if info[UIImagePickerControllerMediaType] as? String == (kUTTypeMovie as? String) {
                let asset = AVAsset(url: url as URL)
                let length = Float(asset.duration.value) / Float(asset.duration.timescale)
                
                self.videoLength = String(length)
                let start = 0
                let end = 10.0
                
                var outputURL = documentDirectory.appendingPathComponent("output")
                do {
                    try manager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                    let name = "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).mp4"
                    outputURL = outputURL.appendingPathComponent("\(name)")
                }catch let error {
                    print(error)
                }
                
                //Remove existing file
                _ = try? manager.removeItem(at: outputURL)
               
                guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {return}
                exportSession.outputURL = outputURL
                exportSession.outputFileType = AVFileType.mp4
                
                let startTime = CMTime(seconds: Double(start), preferredTimescale: 1000)
                let endTime = CMTime(seconds: Double(end), preferredTimescale: 1000)
                let timeRange = CMTimeRange(start: startTime, end: endTime)
                
                exportSession.timeRange = timeRange
                exportSession.exportAsynchronously{
                    switch exportSession.status {
                    case .completed:
                        
                        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mp4")
                        self.compressVideo(inputURL: outputURL as URL, outputURL: compressedURL) { (exportSession) in
                            self.assestUrl = compressedURL
                        }
                        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
                        assetImageGenerator.appliesPreferredTrackTransform = true
                        
                        var time = asset.duration
                        time.value = min(time.value, 2)
                        
                        do {
                            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
                            self.imageToDisplay = UIImage(cgImage: imageRef)
                        } catch {
                        }
                        
                        let vc = StoriesFilterVC(image: self.imageToDisplay)
                        vc.delegate = self
                        
                        self.present(vc, animated: true)
                        
                    case .failed:
                        print("failed \(exportSession.error)")
                        
                    case .cancelled:
                        print("cancelled \(exportSession.error)")
                        
                    default: break
                    }
                }
            }
        }
    }
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    @IBAction func videoAc(_ sender: Any) {
        constantVC.GlobalVariables.shCamera = false
        photoBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        videoBtn.setTitleColor(UIColor(red: 54.0/255.0, green : 54.0/255.0, blue: 54.0/255.0, alpha:1.0), for: UIControlState.normal)
        libraryBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        loadPhotos()
    }
    
    // MARK: Private Functions
    
    private func checkForPhotosPermission(){
        
        // Get the current authorization state.
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            loadPhotos()
        }
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
        }
        else if (status == PHAuthorizationStatus.notDetermined) {
            
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    
                    DispatchQueue.main.async {
                        self.loadPhotos()
                    }
                }
                
            })
        }
            
        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
    }
    
    private func loadPhotos(){
        imageLoader.loadPhotos { (assets) in
            self.configureImageCropper(assets: assets)
        }
    }
    
    private func configureImageCropper(assets:[PHAsset]){
        
        print(assets.count)
        if assets.count != 0{
            photos = assets
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.reloadData()
        }
    }
    
    private func isSquareImage() -> Bool{
        let image = imageToDisplay
        if image.size.width == image.size.height { return true }
        else { return false }
    }
    
    // MARK: Public Functions
    
    func selectImageFromAssetAtIndex(index:NSInteger){
        
     let assetss = photos![photos!.count - 1 - index]
        
        FAImageLoader.imageFrom(asset: photos![photos!.count - 1 - index], size: PHImageManagerMaximumSize) { (image) in
            DispatchQueue.main.async {
                self.displayImageInScrollView(image: image)
                let vc = StoriesFilterVC(image: self.imageToDisplay)
                vc.delegate = self
                self.view.alpha = 0
                self.present(vc, animated: true, completion: nil)
            }
        }
        
        if constantVC.GlobalVariables.shCamera == false{
            let manager = FileManager.default
            
            guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else {return}
            guard let mediaType = "mp4" as? String else {return}
            
            let start = 0
            let end = 10.0
            
            var outputURL = documentDirectory.appendingPathComponent("output")
            do {
                try manager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                let name = "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).mp4"
                outputURL = outputURL.appendingPathComponent("\(name)")
            }catch let error {
                print(error)
            }
            
            //Remove existing file
            _ = try? manager.removeItem(at: outputURL)
            
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: assetss, options: options, resultHandler: { (asset, audioMix, info) in
                if asset != nil{
                    guard let exportSession = AVAssetExportSession(asset: asset!, presetName: AVAssetExportPresetHighestQuality) else {return}
                    exportSession.outputURL = outputURL
                    exportSession.outputFileType = AVFileType.mp4
                    
                    let startTime = CMTime(seconds: Double(start), preferredTimescale: 1000)
                    let endTime = CMTime(seconds: Double(end), preferredTimescale: 1000)
                    let timeRange = CMTimeRange(start: startTime, end: endTime)
                    
                    exportSession.timeRange = timeRange
                    exportSession.exportAsynchronously{
                        switch exportSession.status {
                        case .completed:
                            
                            let asset = AVAsset(url: outputURL as URL)
                            let length = Float(asset.duration.value) / Float(asset.duration.timescale)
                            self.videoLength = String(length)
                            
                            self.assestUrl = outputURL
                        case .failed:
                            print("failed \(exportSession.error)")
                            
                        case .cancelled:
                            print("cancelled \(exportSession.error)")
                            
                        default: break
                        }
                    }
                }
            })
        }
    }
    
    func displayImageInScrollView(image:UIImage){
        imageToDisplay = image

    }
    
    func replicate(_ image:UIImage) -> UIImage? {
        
        guard let cgImage = image.cgImage?.copy() else {
            return nil
        }
        
        return UIImage(cgImage: cgImage,
                       scale: image.scale,
                       orientation: image.imageOrientation)
    }
    
    func updateView(){
        
        if constantVC.GlobalVariables.myStoriesCount >= 1{
            constantVC.GlobalVariables.isNewStoryAdded_moreThanOne = true
        }
        else {
            constantVC.GlobalVariables.isNeedToGetStories = true
        }
     
          self.navigationController?.popToRootViewController(animated: false)
    
    }
    
    func postAc() {
         self.view.alpha = 1.0
        if !Connectivity.isConnectedToInternet() {
            FTIndicator.showInfo(withMessage: "No Internet Connection!".localized )
           
        }
        else {
            
            let actiView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
            actiView.backgroundColor = UIColor.black
            let activityIN : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50)) as UIActivityIndicatorView
            activityIN.center = self.view.center
            activityIN.hidesWhenStopped = true
            activityIN.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
            activityIN.startAnimating()
            actiView.addSubview(activityIN)
            self.view.addSubview(actiView)
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                
                    if constantVC.GlobalVariables.shCamera == true{
                        let photo = self.imageToDisplay
                        var imageData : Data?
                        if UIImageJPEGRepresentation(photo, 1)!.count  < 5000000{
                            imageData = UIImageJPEGRepresentation(photo, 1)!
                        }
                        else if UIImageJPEGRepresentation(photo, 1)!.count  < 8000000 && UIImageJPEGRepresentation(photo, 1)!.count > 5000000{
                            imageData = UIImageJPEGRepresentation(photo, 0.2)
                        }
                        else{
                           imageData = UIImageJPEGRepresentation(photo, 0.5)
                        }
                        print(imageData!.count)
                        multipartFormData.append(imageData!, withName: "file", fileName: "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).jpeg", mimeType: "image/jpeg")
                        
                        multipartFormData.append("image".data(using: String.Encoding.utf8)!, withName: "type")
                        multipartFormData.append("6".data(using: String.Encoding.utf8)!, withName: "time_duration")
                    }
                    else{
                        let data = NSData(contentsOf: self.assestUrl!)
                        multipartFormData.append(data! as Data, withName: "file",fileName:"\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).mp4", mimeType: "video/mp4")
                        multipartFormData.append("video".data(using: String.Encoding.utf8)!, withName: "type")
                        multipartFormData.append(self.videoLength.data(using: String.Encoding.utf8)!, withName: "time_duration")
                    }
                    multipartFormData.append(SessionManager.getUserId().data(using: String.Encoding.utf8)!, withName: "user_id")
            },
                to: "\(constantVC.GeneralConstants.BASE_URL)\(constantVC.WebserviceName.URL_ADD_STORY)",
                encodingCompletion: { encodingResult in
                    
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            guard response.result.error == nil else {
                                actiView.removeFromSuperview()
                                return
                            }
                            if let value = response.result.value {
                                if let result = value as? Dictionary<String, AnyObject> {
                                    let resultMessage = result["msg"] as! String
                                    if resultMessage == "success"{
                                        actiView.removeFromSuperview()
                                        self.updateView()
                                    }
                                    else{
                                        actiView.removeFromSuperview()
                                        self.showError(message: result["msg"] as! String)
                                    }
                                }
                            }
                            }
                            .uploadProgress { progress in // main queue by default
                        }
                        return
                    case .failure(let encodingError):
                        actiView.removeFromSuperview()
                        debugPrint(encodingError)
                    }
            })
        }
    }
}

extension StoriesCropperView:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:FAImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FAImageCell", for: indexPath) as! FAImageCell
        cell.populateDataWith(asset: photos![photos!.count - 1 - indexPath.item])
        return cell
    }
}

extension StoriesCropperView:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if photos?.count == nil {
            return 0;
        }
        return photos!.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell:FAImageCell = collectionView.cellForItem(at: indexPath) as! FAImageCell
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor(red: 75.0/255.0, green: 104.0/255.0, blue: 228.0/255.0, alpha: 1.0).cgColor
       
        selectImageFromAssetAtIndex(index: indexPath.item)
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? FAImageCell {
            cell.layer.borderWidth = 0
        }
    }
}

extension StoriesCropperView:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: cellWidth)
    }
}
extension StoriesCropperView: StoriesFilterDelegate {
    func StoriesFilterImageDidFilter(image: UIImage) {
        imageToDisplay = image
        self.postAc()
    }
     
    func StoriesFilterDidCancel() {
        self.view.alpha = 1.0
    }
}
extension UIImage {
    func fixOrientation() -> UIImage {
        // No-op if the orientation is already correct
        if ( self.imageOrientation == UIImageOrientation.up ) {
            return self;
        }
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform: CGAffineTransform = CGAffineTransform.identity
        if ( self.imageOrientation == UIImageOrientation.down || self.imageOrientation == UIImageOrientation.downMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        }
        if ( self.imageOrientation == UIImageOrientation.left || self.imageOrientation == UIImageOrientation.leftMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2.0))
        }
        if ( self.imageOrientation == UIImageOrientation.right || self.imageOrientation == UIImageOrientation.rightMirrored ) {
            transform = transform.translatedBy(x: 0, y: self.size.height);
            transform = transform.rotated(by: CGFloat(-Double.pi / 2.0));
        }
        if ( self.imageOrientation == UIImageOrientation.upMirrored || self.imageOrientation == UIImageOrientation.downMirrored ) {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        if ( self.imageOrientation == UIImageOrientation.leftMirrored || self.imageOrientation == UIImageOrientation.rightMirrored ) {
            transform = transform.translatedBy(x: self.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx: CGContext = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height),
                                       bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0,
                                       space: self.cgImage!.colorSpace!,
                                       bitmapInfo: self.cgImage!.bitmapInfo.rawValue)!;
        ctx.concatenate(transform)
        if ( self.imageOrientation == UIImageOrientation.left ||
            self.imageOrientation == UIImageOrientation.leftMirrored ||
            self.imageOrientation == UIImageOrientation.right ||
            self.imageOrientation == UIImageOrientation.rightMirrored ) {
            ctx.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.height,height: self.size.width))
        } else {
            ctx.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.width,height: self.size.height))
        }
        return UIImage(cgImage: ctx.makeImage()!)
    }
}
