//
//  ViewController.swift
//  SegmentedProgressView
//
//  Created by isapozhnik on 05/12/2017.
//  Copyright (c) 2017 isapozhnik. All rights reserved.

class StoryFile: NSObject{
    var file     : String?
    var type     : String?
    var _id      : String?
    var created  : String?
    var views    : String?
    var duration :NSNumber?
    var isRead   :String?
    var quickBloxID :String?
    var createdDate :Date?
}

class StoryViews: NSObject{
    var created     : String?
    var id     : String?
    var name      : String?
    var phoneNumber  : String?
}

import UIKit
import MMPlayerView
import Player
import YYImage
import YYCache
import YYWebImage
import FTIndicator

class ShowStoriesVC: BaseViewController, SegmentedProgressBarDelegate , PlayerPlaybackDelegate , PlayerDelegate, UITextViewDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var vw_segmentBar: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var sendReplyBtn: UIButton!
    @IBOutlet weak var createdLab: UILabel!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var optionsBtn: UIButton!
    
    @IBOutlet weak var vw_storyViews: UIView!
    @IBOutlet weak var lbl_viewsCount: UILabel!
    @IBOutlet weak var tblVw_storyViews: UITableView!
    @IBOutlet weak var btn_storyView: UIButton!
    @IBOutlet weak var const_tblVW_height: NSLayoutConstraint!
    @IBOutlet weak var vw_blurBackground: UIView!
    @IBOutlet weak var loader_AI: UIActivityIndicatorView!
    var imageData:Data = Data()
     @IBOutlet weak var sendBtn: UIButton!
     @IBOutlet weak var subViewReply: UIView!
     @IBOutlet weak var replyTF: UITextView!
     @IBOutlet weak var replyViewHeight: NSLayoutConstraint!
     @IBOutlet weak var replyViewBottomConst: NSLayoutConstraint!
     var storyType = ""
     //tap gestures
    var tapGesture = UITapGestureRecognizer()
    var tapGesture_leftView = UITapGestureRecognizer()
    var longTapGesture = UILongPressGestureRecognizer()
    var longTapGesture_leftView = UILongPressGestureRecognizer()
     var videoThumbnail : UIImage!
    //MARK:- Variables
     var indexToLoadView = Int()
    var arrFile = [StoryFile]()
    var arrFile_DB = [Story_DB]()
    var arrViews = [StoryViews]()
    var arrStoryDictionary = [[String:Any]]()
     var singleStory = String()
    var arrUnread  = [Int]()
    var username = String()
    var userId = String()
    var userPhnNo = String()
    var userQuickBloxID:String = ""
    var imageURL = String()
    var globalIndex = Int()
    var globalVideoTime = NSNumber()
    var mmPlayerView = MMPlayerLayer()
    var spb: SegmentedProgressBar!
    var timer = Timer()
    var str_currentViewsCount:String = ""
    var player:Player?
    let baseVC = BaseViewController()
    var lastVideoIndex = Int()
    var TopgradientLayer: CAGradientLayer!
    var BottomgradientLayer: CAGradientLayer!
   // var shUpload = true
     var dialog: QBChatDialog!
    var i = 0
     var shShowSingleStory = false
     
    override var prefersStatusBarHidden: Bool {
        return false
     
    }
    var dicMsgToReply = NSMutableDictionary()
    //MARK:- UIview life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
    constantVC.GlobalVariables.isNeedToGetStories = true
    
    let notificationCenter = NotificationCenter.default
    notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
    notificationCenter.addObserver(self, selector: #selector(appMovedToForeGround), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
      //self.view
     tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tappedView))
     tapGesture.numberOfTapsRequired = 1
     longTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longTappedView))
     
    //self.leftView
     tapGesture_leftView = UITapGestureRecognizer(target: self, action: #selector(self.tappedLeftView))
     tapGesture_leftView.numberOfTapsRequired = 1
     longTapGesture_leftView = UILongPressGestureRecognizer(target: self, action: #selector(self.longTappedView_leftView))
        
    self.player?.playerDelegate = self
    self.player?.playbackDelegate = self
    self.player?.view.frame = self.imgView.bounds
  
     self.vw_blurBackground.isHidden = true
     self.tblVw_storyViews.isHidden = true
     self.tblVw_storyViews.layer.cornerRadius = 5.0
     self.tblVw_storyViews.clipsToBounds = true
     self.navigationController?.isNavigationBarHidden = true
     UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
      configInputBar()
     self.loadUpdatedView()
     
    }
     func configInputBar(){
              subViewReply.isHidden = true
               replyTF.delegate = self
               self.replyTF.text = "Type here...".localized
               self.replyTF.textColor = UIColor.lightGray
               replyTF.font = UIFont(name: "CircularStd-Book", size: 16)
         
          if userId == SessionManager.getUserId() || userId == String(describing: SessionManager.get_QuickBloxID()!) {
               sendBtn.isHidden = true
          }else{
               sendBtn.isHidden = false
          }
     }
     func loadUpdatedView() {
          let imgUrl: URL = URL(string: "\(constantVC.GeneralConstants.ImageUrl)\(imageURL).jpeg")!
          
          self.userImage.yy_setImage(with: imgUrl , placeholder: UIImage(named:"contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
          
          if userId == SessionManager.getUserId() || userId == String(describing: SessionManager.get_QuickBloxID()!) {
               sendBtn.isHidden = true
               optionsBtn.isHidden = false
               vw_storyViews.isHidden = false
               self.userName.text = "You".localized
          }
          else{
               vw_storyViews.isHidden = true
               sendBtn.isHidden = false
               optionsBtn.isHidden = true
               userName.text = SessionManager.getNameFromMyAddressBook(number:imageURL)
          }
          if shShowSingleStory == true{
               
               loadSingleStory()
          }else{
             loadStories()
          }
          
          if !(Connectivity.isConnectedToInternet()) {
               self.arrFile_DB = CoreData_BussinessLogic.getStories(userID: self.userId)
               
               self.spb = SegmentedProgressBar(numberOfSegments:self.arrFile_DB.count , duration: 6)
               self.spb.frame = CGRect(x: 20, y: self.vw_segmentBar.frame.minY , width: self.view.bounds.width - 40, height: 3)
               self.spb.delegate = self
               self.spb.topColor = UIColor.white
               self.spb.bottomColor = UIColor.white.withAlphaComponent(0.5)
               self.spb.padding = 2
               self.view.addSubview(self.spb)
               self.updateImage(index: 0)
               self.addTapGesturesToView()
          }
     }
    @objc func appMovedToBackground() {
        if self.player?.playbackState.description == "Playing" {
            self.player?.stop()
            self.spb.isPaused = true
        }
        else {
            self.spb.isPaused = true
        }
    }
    
    @objc func appMovedToForeGround() {
       /// self.spb.rewind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
     
       self.navigationController?.isNavigationBarHidden = true
      UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
        self.loader_AI.isHidden = true
        self.view.clipsToBounds = false
        self.view.layer.shadowColor = UIColor.black.cgColor
        self.view.layer.shadowOpacity = 1
        self.view.layer.shadowOffset = CGSize.zero
        self.view.layer.shadowRadius = 100
        self.view.layer.shadowPath = UIBezierPath(roundedRect: self.view.bounds, cornerRadius: 0).cgPath

        mmPlayerView.isHidden = false
     disableButton()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        if self.player?.playbackState.description == "Playing" {
            self.player?.stop()
        }
        self.loader_AI.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
     UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self , name: Notification.Name.UIApplicationWillResignActive , object: nil)
        notificationCenter.removeObserver(self , name: Notification.Name.UIApplicationDidBecomeActive , object: nil)
     if constantVC.ActiveSession.isCallActive == false {
          do {
               try AVAudioSession.sharedInstance().setActive(false, with: .notifyOthersOnDeactivation)
          } catch {
               print("error")
          }
     }
    }
     
     override func viewDidDisappear(_ animated: Bool) {
      self.navigationController?.isNavigationBarHidden = false
          UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
     }
     

    //MARK:- UIButton Actions
    @IBAction func doneAc(_ sender: Any) {
       
        self.Pop_ShowStories_VC()
        if self.player?.playbackState.description == "Playing" {
             self.player?.stop()
        }
    }
     
     func enableButton() {
          sendReplyBtn.alpha = 1.0
          sendReplyBtn.isEnabled = true
     }
     
     func disableButton() {
          sendReplyBtn.alpha = 0.2
          sendReplyBtn.isEnabled = false
     }
 
     
     @objc private func keyboardWillChangeFrame(_ notification: Notification) {
          if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
               var keyboardHeight = view.bounds.height - endFrame.origin.y
               if #available(iOS 11, *) {
                    if keyboardHeight > 0 {
                         if !self.isIphoneX() {
                              keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                         }
                    }
               }
               replyViewBottomConst.constant = keyboardHeight
               view.layoutIfNeeded()
          }
     }
     // MARK: - Firebase Save Operation
     func textViewDidChange(_ textView: UITextView) {
          
          if textView.textColor == UIColor.lightGray {
               textView.text = ""
               replyTF.textColor = UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 1.0)
          }
          
          guard let comment = textView.text, !comment.trimmingCharacters(in: .whitespaces).isEmpty else {
               // disable Send button if comment is blank and return
               disableButton()
               return
          }
          // otherwise enable the Send button
          enableButton()
          //set text view growing view
          if textView.contentSize.height > textView.frame.size.height {
               
               let fixedWidth = textView.frame.size.width
               textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
               var newFrame = textView.frame
               let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
               
               newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
               if newSize.height > self.subViewReply.frame.size.height && newSize.height < 100 {
                    self.replyViewHeight.constant = newSize.height
               }
          }
     }
     
     func textViewDidEndEditing(_ textView: UITextView) {
         
          if textView.text.isEmpty {
               textView.text = "Type here...".localized
               textView.textColor = UIColor.lightGray
          }
     }
     
     @IBAction func sendAc(_ sender: Any) {
           self.spb.isPaused = false
          self.player?.playFromCurrentTime()
          if replyTF.text?.trimmingCharacters(in: .whitespaces) != ""{

               self.view.endEditing(true)
               subViewReply.isHidden = true
               sendBtn.isHidden = false
               if Connectivity.isConnectedToInternet() {
                    self.loadChatInfo()
                    self.view.resignFirstResponder()
                     FTIndicator.showToastMessage("Sending Message...")
               }
               else {
                    FTIndicator.showToastMessage("No Internet Connection!")
               }
          }
          else{
                FTIndicator.showToastMessage("Enter message to send")
          }
         
     }
     
     func loadChatInfo(){
          if let privateChatDialog = ServicesManager.instance().chatService.dialogsMemoryStorage.privateChatDialog(withOpponentID: UInt(self.userQuickBloxID)!) {
               self.dialog = privateChatDialog
               constantVC.openPrivateChat.isActiveOpenPrivateChat = true
                sendMessagetoChatScreen()
          }
          else{
            
            ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: UInt(self.userQuickBloxID)!, userPhnNo + "-" + SessionManager.getPhone(), isDoctor: false, fees: "") { (response, chatDialog) in
                    self.dialog = chatDialog
                    QuickBloxManager().sendSystemMsg_updateDialog_toOccupants(dialog: chatDialog!)
                    constantVC.openPrivateChat.isActiveOpenPrivateChat = true
                    self.sendMessagetoChatScreen()
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                    controller.dialog = chatDialog
                  
                    ServicesManager.instance().chatService.addDelegate(controller)
                    ServicesManager.instance().chatService.chatAttachmentService.addDelegate(controller)
                  
                    QBChat.instance.addDelegate(controller)
                }
            
          }
        
     }
    
     func sendMessagetoChatScreen(){
          let message = QBChatMessage()
          message.text = replyTF.text
          message.senderID = ServicesManager.instance().currentUser.id
          message.deliveredIDs = [(NSNumber(value: ServicesManager.instance().currentUser.id))]
          message.readIDs = [(NSNumber(value: ServicesManager.instance().currentUser.id))]
          message.markable = true
          message.dateSent = Date()
          message.recipientID = UInt(self.userQuickBloxID) ?? 0
          
//          //to reply msg
           self.dicMsgToReply.setValue("" , forKey: MsgToReplyKeys.msg_id)
           self.dicMsgToReply.setValue(SessionManager.getPhone() , forKey: MsgToReplyKeys.senderNumber)
          self.dicMsgToReply.setValue(ServicesManager.instance().currentUser.id , forKey: MsgToReplyKeys.senderID)
          
          self.dicMsgToReply.setValue(userPhnNo , forKey: MsgToReplyKeys.replyNumber)
          self.dicMsgToReply.setValue(UInt(self.userQuickBloxID) , forKey: MsgToReplyKeys.replyId)
          self.dicMsgToReply.setValue(MsgToReplyKeys.IMAGE , forKey: MsgToReplyKeys.msgType)
          if storyType == "video"{
               let   thumbImage = self.videoThumbnail!.resized(withPercentage: 0.4)!
               self.imageData = UIImagePNGRepresentation(thumbImage)!
          }else{
                let thumbImage = self.videoThumbnail!.resized(withPercentage: 0.04)!
                self.imageData = UIImagePNGRepresentation(thumbImage)!
          }
          let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
          self.dicMsgToReply.setValue(strBase64 , forKey: MsgToReplyKeys.thumbnail)
       
          if self.dicMsgToReply.allKeys.count > 1 {
               let dicReply = NSMutableDictionary()
               dicReply.setValue(QuickBloxManager().convertDictionaryToJsonString(dict: self.dicMsgToReply) , forKey: MsgToReplyKeys.replyMsg)

               if globalIndex < arrStoryDictionary.count && globalIndex >= 0{

                    let dictionaryToReply = arrStoryDictionary[globalIndex]
                    let jsonData = try? JSONSerialization.data(withJSONObject: dictionaryToReply, options: [])
                    let jsonString = String(data: jsonData!, encoding: .utf8)

                    dicReply.setValue(jsonString, forKey: MsgToReplyKeys.isMsgFromStories)
               }
               dicReply.setValue(storyType, forKey: MsgToReplyKeys.storyType)
               message.customParameters = dicReply
          }
          replyTF.text = ""
          self.sendMessage(message:message)
     }
   
     func sendMessage(message: QBChatMessage){
          
          ServicesManager.instance().chatService.connect { (error) in
               
               if error == nil {
                    ServicesManager.instance().chatService.send(message, toDialogID: self.dialog.id!, saveToHistory: true, saveToStorage: true) { (error) ->
                         Void in
                         
                         if error != nil {
                             
                              if !Connectivity.isConnectedToInternet() {
                                   QMMessageNotificationManager.showNotification(withTitle: "SA_STR_ERROR".localized, subtitle: "Message failed to send. Check your connection".localized , type: QMMessageNotificationType.warning)
                              }
                              // Logging in to chat.
                              ServicesManager.instance().chatService.connect { (error) in
                                   print(error)
                              }
                         }else{
                              FTIndicator.showToastMessage("Message sent")
                         }
                         
                         //Send Push Notification
                         self.sendPushNotification(msg: message.text!)
               }
          }
               else {
                    print("chat not connected")
               }
     }
}
          
          
     func sendPushNotification(msg: String){
          if self.dialog.type == QBChatDialogType.group {
               if let opponentIDs = QuickBloxManager().getOpponentsID(dialog: self.dialog) {
                    var arrStrIDS = [String]()
                    
                    for id in opponentIDs {
                         arrStrIDS.append("\(id)")
                    }
                    TamFirebaseManager.shared.getGroupMutedUsersId(collection: firebaseCollection.MuteGroup.rawValue , doc: self.dialog.id!) { (success , ids) in
                         if success {
                              if ids != "" {
                                   let arrMuteIds = ids.components(separatedBy: ",")
                                   
                                   //get ids not muted
                                   let newArrayID  = arrStrIDS.filter { (string) -> Bool in
                                        return !arrMuteIds.contains(string)
                                   }
                                   
                                   let strIDS = newArrayID.joined(separator: ",")
                                   
                                   //send push notification
                                   QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: "" , dialogID: self.dialog.id!)
                                   
                              } else {
                                   let strIDS = arrStrIDS.joined(separator: ",")
                                   
                                   //send push notification
                                   QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: "" , dialogID: self.dialog.id!)
                              }
                         }
                         else {
                              //no muted
                              let strIDS = arrStrIDS.joined(separator: ",")
                              
                              //send push notification
                              QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: "" , dialogID: self.dialog.id!)
                         }
                    }
               }
          } else {
               
               let document = userPhnNo + "-" + SessionManager.getPhone()
               TamFirebaseManager.shared.getDocument(collection: firebaseCollection.Mute.rawValue , document: document) { (success) in
                    if !(success) {
                         //Not blocked - send custom notification
                         if let opponentID = QuickBloxManager().getOpponentID(dialog: self.dialog) {
                              
                              //silent push
                              QuickBloxManager().sendPushNotification_chat(msg: msg , users: "\(opponentID)" , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: self.userPhnNo , dialogID: self.dialog.id!)
                         }
                    }
               }
          }
     }
     
    @IBAction func sendMessageAc(_ sender: Any) {
     if  videoThumbnail != nil{
         self.spb.isPaused = true
          showTextField()
     }
    }
     
     func showTextField(){
          subViewReply.isHidden = false
           sendBtn.isHidden = true
          replyTF.becomeFirstResponder()
     }
     
    @IBAction func optionsAc(_ sender: Any) {
        
        if Connectivity.isConnectedToInternet() {
            
            if spb != nil {
                spb.isPaused = true
            }
            
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Create the actions
        let addAction = UIAlertAction(title: "Add Stories".localized, style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.addStory()
        }
        
        let deleteAction = UIAlertAction(title: "Delete Story".localized , style: UIAlertActionStyle.destructive) {
            UIAlertAction in
            self.deleteStory_confirmation()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized , style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            self.spb.isPaused = false
        }
        
        // Add the actions
            alertController.addAction(addAction)
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        }
        else {
            FTIndicator.showToastMessage("No Internet Connection!")
        }
        
    }
     
    func deleteStory_confirmation(){
        
        let alertController = UIAlertController(title: "Are you sure?".localized, message: "You want to delete this story".localized , preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes".localized , style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            self.removeStory()
            
        }
        let cancelAction = UIAlertAction(title: "Cancel".localized , style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            self.spb.isPaused = false
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func btn_openStoryView(_ sender: UIButton) {
        
        if Connectivity.isConnectedToInternet() {
        if spb != nil {
        spb.isPaused = true
        }
        self.str_currentViewsCount = arrFile[sender.tag].views! + " " + "Views".localized
        self.callToLoad_storyViews(storyId: arrFile[sender.tag]._id!)
        } else {
            FTIndicator.showToastMessage("No Internet Connection!")
        }
    }
    
    @objc func tblVw_btnClose_action(sender: UIButton!) {
        
        spb.isPaused = false
        self.vw_blurBackground.isHidden = true
        self.vw_storyViews.isHidden = false
        self.tblVw_storyViews.isHidden = true
        self.addTapGesturesToView()
    }
    
    //MARK:- Custom Methods
    func addStory(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "StoriesCropperView") as! StoriesCropperView
         self.navigationController?.pushViewController(controller, animated: false)
    }
    
    func removeStory(){
     if globalIndex < arrFile.count && globalIndex >= 0{
          let webservice  = WebserviceSigleton ()
          let dictParam : NSDictionary = ["login_id":SessionManager.getUserId(), "type":"stories","id": arrFile[globalIndex]._id]
          webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_REMOVEDATA, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
               if result != nil{
                    let response = result! as Dictionary
                    let resultMessage = response["msg"] as! String
                    if resultMessage == "success"{
                         
                         //delete story from DB
                         CoreData_BussinessLogic.deleteParticularStoryByID(storyID: self.arrFile[self.globalIndex]._id!)
                         self.arrFile.remove(at: self.globalIndex)
                         
                         if self.arrFile.count == 0 || self.globalIndex == self.arrFile.count {
                              constantVC.GlobalVariables.isNeedToGetStories = true
                              self.spb.isPaused = true
                              self.Pop_ShowStories_VC()
                         }
                         else{
                              DispatchQueue.main.async {
                                   // do something
                                   self.spb.removeFromSuperview()
                                   self.refresh_segmentView()
                              }
                         }
                    }
               }
          })
     }
    }
    
    func refresh_segmentView() {
        self.spb = SegmentedProgressBar(numberOfSegments: self.arrFile.count, duration: 6
        )
        
        self.spb.frame = CGRect(x: 20, y: self.vw_segmentBar.frame.minY , width: self.view.bounds.width - 40, height: 3)
        self.spb.delegate = self
        self.spb.topColor = UIColor.white
        self.spb.bottomColor = UIColor.white.withAlphaComponent(0.5)
        self.spb.padding = 2
        self.view.addSubview(self.spb)
        self.updateImage(index: self.globalIndex)
        self.addTapGesturesToView()
    }

    
    func readStory(storyId: String){
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["user_id":SessionManager.getUserId(), "stories_id": storyId,"status": "1"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_READSTORY, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                }
            }
        })
    }
     func updateStories(){
          self.spb.removeFromSuperview()
          indexToLoadView = indexToLoadView + 1
          
          if indexToLoadView < constantVC.GlobalVariables.arrStories_global.count{
               self.imageURL = constantVC.GlobalVariables.arrStories_global[indexToLoadView ].photoURL
               self.userId = constantVC.GlobalVariables.arrStories_global[indexToLoadView].id
               self.username = constantVC.GlobalVariables.arrStories_global[indexToLoadView].name
               self.userPhnNo = constantVC.GlobalVariables.arrStories_global[indexToLoadView].photoURL
               loadUpdatedView()
          }else{
               Pop_ShowStories_VC()
               if self.player?.playbackState.description == "Playing" {
                    self.player?.stop()
               }
          }
         
     }
     func loadSingleStory(){
          let jsonString = singleStory
          let jsonData = jsonString.data(using: .utf8)!
          let dictionary = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
         
          self.arrFile = []
          self.arrUnread = []
          if let item = dictionary as? [String:Any]{
          
          let storyFile = StoryFile()
          if let _id = item["id"] as? String{
               storyFile._id = _id
          }
          if let _id = item["quickbloxID"] as? String{
               storyFile.quickBloxID = _id
               self.userQuickBloxID = _id
          }
          if let type = item["type"] as? String{
               storyFile.type = type
          }
          //set duration
          if let type = item["time_duration"] as? String{
               if let n = NumberFormatter().number(from: type) {
                    storyFile.duration = n
               }
               else {
                    storyFile.duration = 6.0
               }
          }
          else {
               storyFile.duration = 6.0
          }
          if let created = item["created"] as? String{
               let localDate = constantVC.UTCToLocal(date: created)
               if localDate != "" {
                    let datefrmter = DateFormatter()
                    datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let date = datefrmter.date(from: localDate)
                    storyFile.createdDate = date
                    storyFile.created = date?.getElapsedInterval()
               }
          }
          
          if let file = item["file"] as? String{
               storyFile.file = file
          }
          if let isRead = item["isRead"] as? String, isRead == "0"{
               storyFile.isRead = isRead
               self.arrUnread.append(i)
          }
          var views:String = "0"
          if let v = item["views"] {
               views = String(describing: v)
          }
          storyFile.views = views
          i = i + 1;
          //save into CoreData
          CoreData_BussinessLogic.saveOrUpdateStory(storyFile: storyFile, userID: self.userId)
          self.arrFile.append(storyFile)
     }
     if self.arrUnread.count != 0{
     constantVC.GlobalVariables.completedStories = self.arrUnread[0]
     }else{
     constantVC.GlobalVariables.completedStories = 0
     }
     self.spb = SegmentedProgressBar(numberOfSegments: self.arrFile.count, duration: 6)
     self.spb.frame = CGRect(x: 20, y: self.vw_segmentBar.frame.minY , width: self.view.bounds.width - 40, height: 3)
     self.spb.delegate = self
     self.spb.topColor = UIColor.white
     self.spb.bottomColor = UIColor.white.withAlphaComponent(0.5)
     self.spb.padding = 2
     self.view.addSubview(self.spb)
     if self.arrUnread.count != 0{
     self.updateImage(index: self.arrUnread[0])
     self.spb.currentAnimationIndex = self.arrUnread[0]
     self.spb.updateSegments()
     }else{
     constantVC.GlobalVariables.completedStories = 0
     self.updateImage(index: 0)
     }
     }
    func loadStories(){
        if Connectivity.isConnectedToInternet() {
        //remove previous stories
        CoreData_BussinessLogic.deleteStoriesByUserID(userID: self.userId)
        }
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["user_id":userId, "login_id" : SessionManager.getUserId()]
            //        self.showLoader(strForMessage: "Loading...")
            webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_STORIES, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
                if result != nil{
                    let response = result! as Dictionary
                    let resultMessage = response["msg"] as! String
                    if resultMessage == "success"{
                       
                        if let storyDetails = response["story_details"] as? [[String:Any]] {
                         self.arrStoryDictionary = storyDetails
                            self.arrFile = []
                            self.arrUnread = []
                            var i = Int()
                            for item in storyDetails {
                                let storyFile = StoryFile()
                                
                                if let _id = item["id"] as? String{
                                    storyFile._id = _id
                                }
                                if let _id = item["quickbloxID"] as? String{
                                    storyFile.quickBloxID = _id
                                    self.userQuickBloxID = _id
                                }
                                if let type = item["type"] as? String{
                                    storyFile.type = type
                                }
                                //set duration
                                if let type = item["time_duration"] as? String{
                                    if let n = NumberFormatter().number(from: type) {
                                        storyFile.duration = n
                                    }
                                    else {
                                       storyFile.duration = 6.0
                                    }
                                }
                                else {
                                    storyFile.duration = 6.0
                                }
                                if let created = item["created"] as? String{
                                    let localDate = constantVC.UTCToLocal(date: created)
                                    if localDate != "" {
                                        let datefrmter = DateFormatter()
                                        datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                        let date = datefrmter.date(from: localDate)
                                        storyFile.createdDate = date
                                        storyFile.created = date?.getElapsedInterval()
                                    }
                                }
                              
                                if let file = item["file"] as? String{
                                    storyFile.file = file
                                }
                                if let isRead = item["isRead"] as? String, isRead == "0"{
                                     storyFile.isRead = isRead
                                     self.arrUnread.append(i)
                                }
                                var views:String = "0"
                                if let v = item["views"] {
                                    views = String(describing: v)
                                }
                                storyFile.views = views
                                i = i + 1;
                                //save into CoreData
                                CoreData_BussinessLogic.saveOrUpdateStory(storyFile: storyFile, userID: self.userId)
                                self.arrFile.append(storyFile)
                            }
                            if self.arrUnread.count != 0{
                                constantVC.GlobalVariables.completedStories = self.arrUnread[0]
                            }else{
                                constantVC.GlobalVariables.completedStories = 0
                            }
                            
                            self.spb = SegmentedProgressBar(numberOfSegments: self.arrFile.count, duration: 6)
                            self.spb.frame = CGRect(x: 20, y: self.vw_segmentBar.frame.minY , width: self.view.bounds.width - 40, height: 3)
                            self.spb.delegate = self
                            self.spb.topColor = UIColor.white
                            self.spb.bottomColor = UIColor.white.withAlphaComponent(0.5)
                            self.spb.padding = 2
                            self.view.addSubview(self.spb)
                            if self.arrUnread.count != 0{
                                self.updateImage(index: self.arrUnread[0])
                                self.spb.currentAnimationIndex = self.arrUnread[0]
                                self.spb.updateSegments()
                            }else{
                                constantVC.GlobalVariables.completedStories = 0
                                self.updateImage(index: 0)
                            }
                        }
                    }
                }
            })
    }
    
    func callToLoad_storyViews(storyId: String){
        
        let webservice  = WebserviceSigleton ()
        
        let dictParam : NSDictionary = ["storyid":storyId , "user_id": SessionManager.getUserId()]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_NUMBER_OF_VIEWS_ON_STORY, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                
                if resultMessage == "success"{
                if let storyDetails = response["data"] as? [[String:Any]] {
                    
                    self.arrViews = []
                    for item in storyDetails {
                        
                        var created:String = ""
                        var id:String  = ""
                        var name:String  = ""
                        var phnNo:String = ""
                        
                        if let c = item["created"] {
                            created = String(describing: c)
                            let localDate = constantVC.UTCToLocal(date: created)
                            if localDate != "" {
                                let datefrmter = DateFormatter()
                                datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let date = datefrmter.date(from: localDate)
                                created = (date?.getElapsedInterval())!
                            }
                        }
                        if let i = item["id"] {
                            id = String(describing: i)
                        }
                        if let phn = item["phoneNumber"] {
                            phnNo = String(describing: phn)
                            name = SessionManager.getNameFromMyAddressBook(number:phnNo)
                        }
                        let storyView = StoryViews()
                        storyView.created = created
                        storyView.id = id
                        storyView.name = name
                        storyView.phoneNumber = phnNo
                        self.arrViews.append(storyView)
                    }
                    
                    self.vw_blurBackground.isHidden = false
                    self.tblVw_storyViews.isHidden = false
                    self.tblVw_storyViews.delegate = self
                    self.tblVw_storyViews.dataSource = self
                    self.setTblVw_height()
                    self.removeTapGestures()
                    self.tblVw_storyViews.reloadData()
                }
                }
                else {
                    self.arrViews = []
                    self.setTblVw_height()
                    self.vw_blurBackground.isHidden = false
                    self.tblVw_storyViews.isHidden = false
                    self.tblVw_storyViews.delegate = self
                    self.tblVw_storyViews.dataSource = self
                    self.removeTapGestures()
                    self.tblVw_storyViews.reloadData()
                }
            }
        })
    }
    
    func setTblVw_height() {
        
        let totalCellheight:CGFloat = CGFloat(self.arrViews.count * 50)
        if totalCellheight + 45 < self.view.frame.size.height - 120 {
            self.const_tblVW_height.constant = totalCellheight + 45
            self.tblVw_storyViews.isScrollEnabled = false
        }
        else {
            self.tblVw_storyViews.isScrollEnabled = true
            self.const_tblVW_height.constant = self.view.frame.size.height - 120
        }
    }
    
    func updateTimer() {
        if (String(describing: self.mmPlayerView.currentPlayStatus) == "playing") {
            timer.invalidate()
            self.spb.isPaused = false
        }
    }
    
    func Pop_ShowStories_VC() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = kCATransitionPush
        transition.subtype = kCAOnOrderIn
        self.view.window?.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
    //MARK:- Video Player Delegates
    
    public func playerPlaybackWillStartFromBeginning(_ player: Player) {
    }
    
    public func playerPlaybackDidEnd(_ player: Player) {
    }
    
    public func playerCurrentTimeDidChange(_ player: Player) {
    }
    
    public func playerPlaybackWillLoop(_ player: Player) {
    }
    
    func playerReady(_ player: Player) {
       // baseVC.stopAnimating()
        self.loader_AI.stopAnimating()
        self.loader_AI.isHidden = true
        DispatchQueue.main.async{
            if self.lastVideoIndex == self.globalIndex {
                self.spb.duration = self.globalVideoTime as! TimeInterval
                self.spb.startAnimation(index_: self.globalIndex)
               if (self.replyTF.isFirstResponder) {
                    self.spb.isPaused = true
                    self.player?.pause()
               }
            }
        }
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
    }
    
    func playerBufferTimeDidChange(_ bufferTime: Double) {
    }
    
    private func  updateImage(index: Int) {
       self.spb.updateColors()
        var type:String = ""
        var file:String = ""
        globalIndex = index
        self.spb.currentAnimationIndex = index
        self.spb.updateSegments()
     
        self.imgView.image = nil
        if self.player?.view != nil && self.player != nil{
            self.loader_AI.stopAnimating()
            self.loader_AI.isHidden = true
            self.player?.view.removeFromSuperview()
        }
        self.btn_storyView.tag = index
        if Connectivity.isConnectedToInternet() {
            self.lbl_viewsCount.text =  arrFile[index].views! + " " + "Views".localized
            type = arrFile[index].type!
            file = arrFile[index].file!
            createdLab.text = arrFile[index].createdDate?.getElapsedInterval()
            readStory(storyId: arrFile[index]._id!)
        } else {
            if index < self.arrFile_DB.count {
                self.lbl_viewsCount.text = arrFile_DB[index].views! + " " + "Views".localized
                type = arrFile_DB[index].type!
                file = arrFile_DB[index].file!
                createdLab.text = (arrFile_DB[index].createdDate! as Date).getElapsedInterval()
            }
        }
        if type == "video"{
            self.player = Player()
            self.player?.playerDelegate = self
            self.player?.playbackDelegate = self
            self.player?.view.frame = self.view.bounds
            self.lastVideoIndex = self.globalIndex
            self.loader_AI.isHidden = false
            self.loader_AI.startAnimating()
            if Connectivity.isConnectedToInternet() {
               self.globalVideoTime = arrFile[index].duration!
            } else {
                self.globalVideoTime = 6.0
            }
            if let view = self.player?.view {
                self.imgView.addSubview(view)
            }
            let videoUrl: URL = URL(string: "\(constantVC.GeneralConstants.ImageUrl)\(file)")!
     
          if let imageto = createThumbnailOfVideoFromRemoteUrl(url: "\(constantVC.GeneralConstants.ImageUrl)\(file)")
          {
              videoThumbnail = imageto
               storyType = "video"
          }
            self.player?.url = videoUrl
            self.player?.playFromBeginning()
            self.player?.fillMode = PlayerFillMode.resizeAspectFit.avFoundationType
        }
        else{
            let imgUrl: URL = URL(string: "\(constantVC.GeneralConstants.ImageUrl)\(file)")!
          
            imgView.yy_setImage(with: imgUrl , placeholder: nil , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: { (image , url , type , stage , error ) in
                if error == nil {
                    self.videoThumbnail = image
                    self.storyType = "image"
                    self.spb.duration = 6.0
                    //self.arrFile[index].duration as! TimeInterval
                    self.spb.startAnimation(index_: index)
                     self.addTapGesturesToView()
                }
            })
        }
    }

    //MARK:- Segement Methods
    func segmentedProgressBarChangedIndex(index: Int) {
       print("indexInt \(index)")
            updateImage(index: index)
    }
    
    func segmentedProgressBarFinished() {
     if shShowSingleStory == true{
          shShowSingleStory = false
          self.Pop_ShowStories_VC()
          if self.player?.playbackState.description == "Playing" {
               self.player?.stop()
          }
     }else{
       self.updateStories()
     }
    }
    
    //MARK:- Tapped
    
    func removeTapGestures() {
        self.view.removeGestureRecognizer(tapGesture)
        self.view.removeGestureRecognizer(longTapGesture)
        self.leftView.removeGestureRecognizer(tapGesture_leftView)
        self.leftView.removeGestureRecognizer(longTapGesture_leftView)
    }
    
    func addTapGesturesToView() {
        self.view.isUserInteractionEnabled = true
        self.leftView.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapGesture)
        self.view.addGestureRecognizer(longTapGesture)
        self.leftView.addGestureRecognizer(tapGesture_leftView)
        self.leftView.addGestureRecognizer(longTapGesture_leftView)
    }
   
    @objc private func tappedView() {
     
     if replyTF.isFirstResponder == true{
          replyTF.resignFirstResponder()
          spb.isPaused = false
          subViewReply.isHidden = true
          sendBtn.isHidden = false
          replyTF.text = ""
     }
     else{
          if Connectivity.isConnectedToInternet() && self.arrFile.count > 0 && globalIndex < self.arrFile.count {
               if self.arrFile[globalIndex].type == "video" {
                    if self.player?.playbackState.description == "Playing" {
                         self.player?.stop()
                    }
               }
          }
          
          spb.skip()
     }
    }
    @objc private func tappedLeftView() {
     if replyTF.isFirstResponder == true{
          replyTF.resignFirstResponder()
          spb.isPaused = false
          subViewReply.isHidden = true
          sendBtn.isHidden = false
          replyTF.text = ""
     }else{
          spb.rewind()
     }
    }
    @objc func longTappedView(sender : UIGestureRecognizer){
     if replyTF.isFirstResponder == true{
          replyTF.resignFirstResponder()
          spb.isPaused = false
          subViewReply.isHidden = true
          sendBtn.isHidden = false
          replyTF.text = ""
     }else{
        if sender.state == .ended {
             spb.isPaused = false
        }
        else if sender.state == .began {
             spb.isPaused = true
        }
     }
    }
    
    @objc func longTappedView_leftView(sender : UIGestureRecognizer){
     
     if replyTF.isFirstResponder == true{
          replyTF.resignFirstResponder()
          spb.isPaused = false
          subViewReply.isHidden = true
          sendBtn.isHidden = false
          replyTF.text = ""
     }else{
        if sender.state == .ended {
            spb.isPaused = false
        }
        else if sender.state == .began {
            spb.isPaused = true
        }
     }
    }
     
     func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
         
          let asset = AVAsset(url: URL(string: url)!)
          let assetImgGenerate = AVAssetImageGenerator(asset: asset)
          assetImgGenerate.appliesPreferredTrackTransform = true
          //Can set this to improve performance if target size is known before hand
          //assetImgGenerate.maximumSize = CGSize(width,height)
          let time = CMTimeMakeWithSeconds(0.1, 1)
          do {
               let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
               let thumbnail = UIImage(cgImage: img)
               return thumbnail
          } catch {
               print(error.localizedDescription)
               return nil
          }
     }
}

extension ShowStoriesVC: UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrViews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ShowStories_TableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ShowStories_TableViewCell
        cell.setStory_view = arrViews[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! ShowStories_TableViewCell
        headerCell.lbl_viewsCount.text = self.str_currentViewsCount
        headerCell.btnClose.addTarget(self, action: #selector(tblVw_btnClose_action), for: .touchUpInside)
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}


class ShowStories_TableViewCell: UITableViewCell {
    
    //headerCell
    @IBOutlet weak var lbl_viewsCount: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    
    //cell
    @IBOutlet weak var imgVw_userImage: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_timeStamp: UILabel!
    
    var setStory_view: StoryViews? {
        didSet {
            updateView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateView() {
       lbl_userName.text = setStory_view?.name
       imgVw_userImage.layer.cornerRadius = self.imgVw_userImage.frame.size.width / 2
       imgVw_userImage.clipsToBounds = true
        
       if let photoURL = setStory_view?.phoneNumber {
        imgVw_userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL).jpeg") , placeholder: UIImage(named: "profile") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
        }
        lbl_timeStamp.text = (setStory_view?.created)!
    }
}
extension UIImage {
     func resized(withPercentage percentage: CGFloat) -> UIImage? {
          let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
          return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
               _ in draw(in: CGRect(origin: .zero, size: canvas))
          }
     }
     func resized(toWidth width: CGFloat) -> UIImage? {
          let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
          return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
               _ in draw(in: CGRect(origin: .zero, size: canvas))
          }
     }
}
