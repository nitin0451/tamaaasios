//
//  LoadingBar.swift
//  Tamaas
//
//  Created by Krescent Global on 17/04/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import AANotifier
import GradientProgressBar
class LoadingBar: AANibView {

    
    @IBOutlet weak var progressBar: GradientProgressBar!
    var notifer: AANotifier?
    var myTimer: Timer?
    override func viewDidLoad() {
        self.progressBar.progress = 0.0
        showProgressView()
    }
    
    func showProgressView(){
        
        // it progress reach 1 then reset it to 0.0
        if self.progressBar.progress >= 1 {
            self.progressBar.progress = 0.0
        }
        // schedule timer
        self.myTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
    }
    @objc func updateProgress() {
        
        // increase progress value
        self.progressBar.progress += 0.01
        
        // invalidate timer if progress reach to 1
        if self.progressBar.progress >= 1 {
            // enable/disable button
            
            self.myTimer?.invalidate()
            self.progressBar.progress = 0.0
            showProgressView()
        }
    }

}
