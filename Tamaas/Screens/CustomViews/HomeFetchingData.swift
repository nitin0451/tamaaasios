//
//  LoadingBar.swift
//  Tamaas
//
//  Created by Krescent Global on 17/04/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import AANotifier
import GradientProgressBar
class HomeFetchingData: AANibView {

    
    @IBOutlet weak var homeProgressBar: GradientProgressBar!
    
    var notifer: AANotifier?
    var homeTimer: Timer?
    override func viewDidLoad() {
         self.homeProgressBar.progress = 0.0
        showProgressView()
    }
    
    func showProgressView(){
        
        // it progress reach 1 then reset it to 0.0
        if self.homeProgressBar.progress >= 1 {
            self.homeProgressBar.progress = 0.0
        }
        // schedule timer
        self.homeTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateProgress), userInfo: nil, repeats: true)
    }
    @objc func updateProgress() {
        
        // increase progress value
        self.homeProgressBar.progress += 0.01
        
        // invalidate timer if progress reach to 1
        if self.homeProgressBar.progress >= 1 {
            // enable/disable button
            
            self.homeTimer?.invalidate()
            self.homeProgressBar.progress = 0.0
            showProgressView()
        }
    }

}
