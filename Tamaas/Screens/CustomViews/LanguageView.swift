//
//  LanguageView.swift
//  Tamaas
//
//  Created by Krescent Global on 03/08/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import AANotifier

class LanguageView: AANibView {
     var notifer: AANotifier?
     @IBOutlet weak var pastoBtn: UIButton!
     @IBOutlet weak var engBtn: UIButton!
     @IBOutlet weak var dariBtn: UIButton!
     @IBOutlet weak var turkishBtn: UIButton!
     @IBAction func dariAc(_ sender: Any) {
          engBtn.isSelected = false
          dariBtn.isSelected = true
          pastoBtn.isSelected = false
          turkishBtn.isSelected = false
     }
     
     @IBAction func engAc(_ sender: Any) {
          engBtn.isSelected = true
          dariBtn.isSelected = false
          pastoBtn.isSelected = false
          turkishBtn.isSelected = false
     }
     
     @IBAction func pastoAc(_ sender: Any) {
          engBtn.isSelected = false
          dariBtn.isSelected = false
          pastoBtn.isSelected = true
          turkishBtn.isSelected = false
     }
     @IBAction func turkishAc(_ sender: Any) {
          turkishBtn.isSelected = true
          engBtn.isSelected = false
          dariBtn.isSelected = false
          pastoBtn.isSelected = false
     }
     
}
