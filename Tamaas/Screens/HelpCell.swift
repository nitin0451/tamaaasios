//
//  HelpCell.swift
//  Tamaas
//
//  Created by Krescent Global on 17/04/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class HelpCell: UITableViewCell {

    @IBOutlet weak var toggleSwitch: UISwitch!
    @IBOutlet weak var helpLab: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
