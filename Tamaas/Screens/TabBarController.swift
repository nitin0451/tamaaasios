//
//  TabBarController.swift
//  Tamaas
//
//  Created by Krescent Global on 19/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController,UITabBarControllerDelegate {
     
     override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
   
        for item in self.tabBar.items!{
            item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
            item.image = item.image?.withRenderingMode(.alwaysOriginal)
            item.title = item.title?.localized
        }
        
        let attributesUnselected = [NSAttributedString.Key.font:
            UIFont(name: "CircularStd-Book", size: 12.0)!] as [NSAttributedStringKey: Any]
        let attributesSelected = [NSAttributedString.Key.font:
            UIFont(name: "CircularStd-Book", size: 12.0)!] as [NSAttributedStringKey: Any]
        
        UITabBarItem.appearance().setTitleTextAttributes( attributesUnselected , for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes( attributesSelected , for: .selected)
       
        NotificationCenter.default.addObserver(self, selector: #selector(updateTab(notification:)), name: NSNotification.Name(rawValue: "notifyTab"), object: nil)
    }
    
    @objc func updateTab(notification : Notification){
        self.selectedIndex = 2
        if let navVc = self.selectedViewController as? HomeNavigationController {
            if let vc = navVc.viewControllers[0] as? HealthHomeVC {
                vc.showHealthPopup()
            }
        }
        
    }
    
}
