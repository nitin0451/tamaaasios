//
//  NewContactVC.swift
//  Tamaas
//
//  Created by Krescent Global on 22/02/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import Contacts
import FTIndicator

class NewContactVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
   
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userContactNo: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var popupContact: UIView!
    var arrContacts = [CNContact]()
    var contactToSave:CNMutableContact!
    @IBOutlet weak var naviBar: UINavigationBar!
    var contactToRemove = Int()
     var store: CNContactStore!
    @IBAction func backAc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_closeContactPopUp_action(_ sender: UIButton) {
        self.removeAnimate(popupView: popupContact)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        store = CNContactStore()
        popupContact.cornerRadius = 5;
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return 0.01
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: NSInteger) -> Int {
        
        return arrContacts.count
    }
    @IBAction func addToContact(_ sender: Any) {
        
         self.removeAnimate(popupView: popupContact)
        
        do{
        let saveRequest = CNSaveRequest()
            saveRequest.add(contactToSave, toContainerWithIdentifier:nil)
            try store.execute(saveRequest)
            
            FTIndicator.showSuccess(withMessage: "Contact Saved!")
            arrContacts.remove(at: contactToRemove)
            if arrContacts.count > 0{
                tableView.reloadData()
            }
            else{
                //self.navigationController?.popViewController(animated: true)
               let currentVC = self.getCurrentViewController()
               currentVC?.dismiss(animated: true, completion: nil)
            }
    }
    catch{
        showError(message: "Failed, please try again")
    }
    }
    override  func viewWillAppear(_ animated: Bool) {
        
        self.naviBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]
    }
    
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell") as! NewContactCell
                let contact = arrContacts[indexPath.row]
        
                if let imageData = contact.imageData {
                    cell.contactImage.image = UIImage(data:imageData)
                }
                cell.contactNAme.text = contact.givenName
        
                return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        contactToSave = CNMutableContact()
        let contact = arrContacts[indexPath.row]
        contactToRemove = indexPath.row
        if let imageData = contact.imageData {
            userImage.image = UIImage(data:imageData)
        }
        userName.text = contact.familyName
        if contact.phoneNumbers.count > 0 {
            userContactNo.text = "\(contact.phoneNumbers[0].value.stringValue)"
        } else {
            userContactNo.text = contact.givenName
     }
     if userName.text == userContactNo.text{
          userName.text = ""
     }
        contactToSave.familyName = contact.familyName
        contactToSave.phoneNumbers = contact.phoneNumbers
        self.showAnimate(popupView: popupContact)
    }
}
