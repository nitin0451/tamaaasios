import UIKit
struct IconPagingCellViewModel {
  let image: UIImage?
  let selected: Bool
  let tintColor: UIColor
  let selectedTintColor: UIColor
  
  init(image: UIImage?, selected: Bool, options: PagingOptions) {
    self.image = image
    self.selected = selected
    self.tintColor = options.textColor
    self.selectedTintColor = options.selectedTextColor
  }
}

class IconPagingCell: PagingCell {
  
  fileprivate var viewModel: IconPagingCellViewModel?
  
  fileprivate lazy var imageView: UIImageView = {
    let imageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 25, height: 25))
    imageView.contentMode = .scaleAspectFit
    imageView.center.y = self.center.y
    return imageView
  }()
    fileprivate lazy var labelText: UILabel = {
    let labelText = UILabel(frame: CGRect(x: 35, y: 5, width: 50, height: 25))
     labelText.font = UIFont.init(name: "CircularStd-Book", size: 16)
        labelText.adjustsFontSizeToFitWidth = true
    labelText.center.y = self.center.y
    return labelText
}()
  override init(frame: CGRect) {
    super.init(frame: frame)
    contentView.addSubview(imageView)
    contentView.addSubview(labelText)

  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func setPagingItem(_ pagingItem: PagingItem, selected: Bool, options: PagingOptions) {
    if let item = pagingItem as? IconItem {

      let viewModel = IconPagingCellViewModel(
        image: item.image,
        selected: selected,
        options: options)
        
      labelText.text = item.icon
      imageView.image = viewModel.image
      
      if viewModel.selected {
        imageView.transform = CGAffineTransform(scaleX: 1, y: 1)
        imageView.tintColor = viewModel.selectedTintColor
      } else {
        imageView.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        imageView.tintColor = viewModel.tintColor
      }
      
      self.viewModel = viewModel
    }
  }
  
  open override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
    guard let viewModel = viewModel else { return }
    if let attributes = layoutAttributes as? PagingCellLayoutAttributes {
      let scale = (0.4 * attributes.progress) + 0.6
      imageView.transform = CGAffineTransform(scaleX: scale, y: scale)
      imageView.tintColor = UIColor.interpolate(
        from: viewModel.tintColor,
        to: viewModel.selectedTintColor,
        with: attributes.progress)
    }
  }
}




/*  private func setupConstraints() {
 imageView.translatesAutoresizingMaskIntoConstraints = false
 
 let topContraint = NSLayoutConstraint(
 item: imageView,
 attribute: .top,
 relatedBy: .equal,
 toItem: contentView,
 attribute: .top,
 multiplier: 1.0,
 constant: 15)
 
 let bottomConstraint = NSLayoutConstraint(
 item: imageView,
 attribute: .bottom,
 relatedBy: .equal,
 toItem: contentView,
 attribute: .bottom,
 multiplier: 1.0,
 constant: -15)
 
 let leadingContraint = NSLayoutConstraint(
 item: imageView,
 attribute: .leading,
 relatedBy: .equal,
 toItem: contentView,
 attribute: .leading,
 multiplier: 1.0,
 constant: 0)
 
 let trailingContraint = NSLayoutConstraint(
 item: imageView,
 attribute: .trailing,
 relatedBy: .equal,
 toItem: contentView,
 attribute: .trailing,
 multiplier: 1.0,
 constant: 0)
 
 contentView.addConstraints([
 topContraint,
 bottomConstraint,
 leadingContraint,
 trailingContraint])
 }*/
