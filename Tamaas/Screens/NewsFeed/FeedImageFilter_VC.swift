//
//  FeedImageFilter_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 10/30/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
public protocol FeedImageFilter_VC_Delegate {
    func FeedImageFilter_VC_ImageDidFilter(image: UIImage)
    func FeedImageFilter_VC_DidCancel()
}

class FeedImageFilter_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var collectionVw_filter: UICollectionView!
    
    
    //MARK:- Variables
    public var delegate: FeedImageFilter_VC_Delegate?
    var mainImg:UIImage?
    var arrFilterImgs:[UIImage] = []
    let filterNameList = [
        "No Filter",
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectMono",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer",
        "CILinearToSRGBToneCurve",
        "CISRGBToneCurveToLinear"
    ]
    
    let filterDisplayNameList = [
        "Normal",
        "Chrome",
        "Fade",
        "Instant",
        "Mono",
        "Noir",
        "Process",
        "Tonal",
        "Transfer",
        "Tone",
        "Linear"
    ]
    fileprivate let context = CIContext(options: nil)
    
    
    //MARK:- UIView life-Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionVw_filter.delegate = self
        self.collectionVw_filter.dataSource = self
        self.arrFilterImgs.append(self.mainImg!)
        self.imgVw.image = self.mainImg!
        self.collectionVw_filter.reloadData()
        
    }

    //MARK:- UIButton Actions
    @IBAction func btn_done_action(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.FeedImageFilter_VC_ImageDidFilter(image: self.imgVw.image!)
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_close_action(_ sender: UIButton) {
        if let delegate = self.delegate {
            delegate.FeedImageFilter_VC_DidCancel()
        }
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Filter creation Methods
    func createFilteredImage(filterName: String, image: UIImage) -> UIImage {
        // 1 - create source image
        let sourceImage = CIImage(image: image)
        
        // 2 - create filter using name
        let filter = CIFilter(name: filterName)
        filter?.setDefaults()
        
        // 3 - set source image
        filter?.setValue(sourceImage, forKey: kCIInputImageKey)
        
        // 4 - output filtered image as cgImage with dimension.
        let outputCGImage = context.createCGImage((filter?.outputImage!)!, from: (filter?.outputImage!.extent)!)
        
        // 5 - convert filtered CGImage to UIImage
        let filteredImage = UIImage(cgImage: outputCGImage!)
        
        return filteredImage
    }
}
extension FeedImageFilter_VC: UICollectionViewDelegate , UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterNameList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FeedImageFilter_CollectionCell
        if indexPath.row == 0 {
            cell.imgVw_filter.image = self.mainImg!
        } else {
            if indexPath.row < self.arrFilterImgs.count {
                cell.imgVw_filter.image = self.arrFilterImgs[indexPath.row]
            } else {
                if let filterImg = self.createFilteredImage(filterName: filterNameList[indexPath.row], image: self.mainImg!) as? UIImage{
                    self.arrFilterImgs.append(filterImg)
                    cell.imgVw_filter.image = filterImg
                }
            }
        }
        cell.lbl_filterName.text = filterDisplayNameList[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedCell = collectionView.cellForItem(at: indexPath) {
            let cell = selectedCell as! FeedImageFilter_CollectionCell
            self.imgVw.image = cell.imgVw_filter.image
        }
    }
    
    
}


class FeedImageFilter_CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_filterName: UILabel!
    @IBOutlet weak var imgVw_filter: UIImageView!
}
