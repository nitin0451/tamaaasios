//
//  Polling_tableViewCell.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 3/26/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class Polling_tableViewCell: UITableViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var collectionVw_polling: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
