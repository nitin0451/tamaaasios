//
//  PollingMultipleNotAns_collectionCell.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 4/10/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class PollingMultipleNotAns_collectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var vwContent: UIView!
    @IBOutlet weak var txtVwQuestion: UITextView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblOption1: UILabel!
    @IBOutlet weak var lblOption1Percentage: UILabel!
    @IBOutlet weak var lblOption2: UILabel!
    @IBOutlet weak var lblOption2Percentage: UILabel!
    @IBOutlet weak var lblOption3: UILabel!
    @IBOutlet weak var lblOption3Percentage: UILabel!
    @IBOutlet weak var constriantOption1Width: NSLayoutConstraint!
    
    @IBOutlet weak var constraintOption2Width: NSLayoutConstraint!
    @IBOutlet weak var constraintOption3Width: NSLayoutConstraint!
    
    
    
    var polling: Polling? {
        didSet {
            updateView()
        }
    }
    
    
    func updateView(){
        self.txtVwQuestion.text = polling?.question
        if let strDate = polling?.createdDate?.getElapsedInterval_FEEDS() {
            self.lblTime.text = "Tamaaas " + strDate
        }
        self.calculatePercentage()
    }
    
    
    func calculatePercentage(){
        var totalAnsCount:Int = 0
        
        if let arrAns = polling?.givenAns {
            for ans in arrAns {
                totalAnsCount = totalAnsCount + ans.value!
            }
            
            for var i in (0..<arrAns.count) {
                
                let ansObj = arrAns[i]
                
                let Percentage = ((ansObj.value)! * 100)/totalAnsCount
                let Width = (Percentage * Int(self.vwContent.frame.width)) / 100
                
                if i == 0 {
                    self.lblOption1.text = ansObj.answer
                    self.lblOption1Percentage.text = "\(Percentage)%"
                    if Percentage > 20 {
                        self.constriantOption1Width.constant = CGFloat(Width)
                    }
                }
                
                if i == 1 {
                    self.lblOption2.text = ansObj.answer
                    self.lblOption2Percentage.text = "\(Percentage)%"
                    if Percentage > 20 {
                        self.constraintOption2Width.constant = CGFloat(Width)
                    }
                }
                
                if i == 2 {
                    self.lblOption3.text = ansObj.answer
                    self.lblOption3Percentage.text = "\(Percentage)%"
                    if Percentage > 20 {
                        self.constraintOption3Width.constant = CGFloat(Width)
                    }
                }
            }
        }
        
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.vwContent.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        self.vwContent.layer.shadowOpacity = 5
        self.vwContent.layer.shadowOffset = CGSize.zero
        self.vwContent.layer.shadowRadius = 5
    }

}
