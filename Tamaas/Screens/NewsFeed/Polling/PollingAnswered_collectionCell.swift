//
//  PollingAnswered_collectionCell.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 3/26/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class PollingAnswered_collectionCell: UICollectionViewCell {

    //MARK:- Outlets
    @IBOutlet weak var vwContent: UIView!
    @IBOutlet weak var txtVwQuestion: UITextView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var vwYes: UIView!
    @IBOutlet weak var vwNo: UIView!
    @IBOutlet weak var lblYesPercentage: UILabel!
    @IBOutlet weak var lblNoPercentage: UILabel!
    @IBOutlet weak var constraintYesWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintNoWidth: NSLayoutConstraint!
    @IBOutlet weak var lblOption1: UILabel!
    @IBOutlet weak var lblOption2: UILabel!
    
    
    
    var polling: Polling? {
        didSet {
            self.constraintYesWidth.constant = 0
            self.constraintNoWidth.constant = 0
            updateView()
        }
    }
    
    
    func updateView(){
        self.txtVwQuestion.text = polling?.question
        if let strDate = polling?.createdDate?.getElapsedInterval_FEEDS() {
            self.lblTime.text = "Tamaaas " + strDate
        }
        self.setQuestionOption()
        self.calculatePercentage()
    }
    
    func setQuestionOption(){
        
        self.lblOption1.textColor = UIColor.black
        self.lblOption2.textColor = UIColor.black
        
        if polling?.type != "text" {
            if let arrQuestionOption = polling?.question_options {
                for var i in (0..<arrQuestionOption.count) {
                    
                    let optionObj = arrQuestionOption[i]
                    
                    if i == 0 {
                        self.lblOption1.text = optionObj.options
                    }
                    
                    if i == 1 {
                        self.lblOption2.text = optionObj.options
                    }
                }
            }
        } else {
            self.lblOption1.text = "Yes"
            self.lblOption2.text = "No"
        }
    }
    
    
    func calculatePercentage(){
        var totalAnsCount:Int = 0
        self.lblYesPercentage.text = ""
        self.lblNoPercentage.text = ""
        
        if let arrAns = polling?.givenAns {
            
            for ans in arrAns {
                totalAnsCount = totalAnsCount + ans.value!
            }
            
            for var i in (0..<arrAns.count) {
                
                let ansObj = arrAns[i]
                
                var Percentage = (Double((ansObj.value)!) * 100)/Double(totalAnsCount)
                Percentage = Percentage.rounded(.toNearestOrEven)
                var Width = (Percentage * Double(self.frame.width - 30)) / 100
                Width = Width.rounded(.toNearestOrEven)
                
                    if self.lblOption1.text == ansObj.answer {
                        self.lblOption1.textColor = UIColor.white
                        self.lblYesPercentage.text = "\(Percentage)%"
                        
                        if Percentage < 20 {
                            self.constraintYesWidth.constant = 80
                        } else {
                            self.constraintYesWidth.constant = CGFloat(Width) - 20.0
                        }
                    }
                
                    if self.lblOption2.text == ansObj.answer {
                        self.lblOption2.textColor = UIColor.white
                        self.lblNoPercentage.text = "\(Percentage)%"
                        
                        
                        if Percentage < 20 {
                            self.constraintNoWidth.constant = 80
                        } else {
                            self.constraintNoWidth.constant = CGFloat(Width) - 20.0
                        }
                    }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.vwContent.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        self.vwContent.layer.shadowOpacity = 5
        self.vwContent.layer.shadowOffset = CGSize.zero
        self.vwContent.layer.shadowRadius = 5
        
    }

}
