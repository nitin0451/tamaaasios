//
//  PollingNotAnswered_collectionCell.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 3/26/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class PollingNotAnswered_collectionCell: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var vwContent: UIView!
    @IBOutlet weak var txtVwQuestion: UITextView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    
    
    var polling: Polling? {
        didSet {
            updateView()
        }
    }
    
    
    func updateView(){
        self.txtVwQuestion.text = polling?.question
        if let strDate = polling?.createdDate?.getElapsedInterval_FEEDS() {
            self.lblTime.text = "Tamaaas " + strDate
        }
        
        if polling?.type != "text" {
            if let arrQuestionOption = polling?.question_options {
                for var i in (0..<arrQuestionOption.count) {
                    
                    let optionObj = arrQuestionOption[i]
                    
                    if i == 0 {
                        self.btnYes.setTitle( optionObj.options, for: .normal)
                    }
                    
                    if i == 1 {
                        self.btnNo.setTitle( optionObj.options, for: .normal)
                    }
                }
            }
        } else {
            self.btnYes.setTitle( "Yes", for: .normal)
            self.btnNo.setTitle( "No", for: .normal)
        }
        
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.vwContent.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        self.vwContent.layer.shadowOpacity = 5
        self.vwContent.layer.shadowOffset = CGSize.zero
        self.vwContent.layer.shadowRadius = 5
        
        self.btnYes.borderColor = UIColor.lightGray
        self.btnYes.borderWidth = 1.0
        self.btnNo.borderColor = UIColor.lightGray
        self.btnNo.borderWidth = 1.0
    }

}
