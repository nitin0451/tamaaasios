//
//  PollingMultipleAns_collectionCell.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 4/10/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class PollingMultipleAns_collectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var contentVw: UIView!
    @IBOutlet weak var txtViewQuestion: UITextView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnOption1: UIButton!
    @IBOutlet weak var btnOption2: UIButton!
    @IBOutlet weak var btnOption3: UIButton!
    
    var polling: Polling? {
        didSet {
            updateView()
        }
    }
    
    
    func updateView(){
        self.txtViewQuestion.text = polling?.question
        if let strDate = polling?.createdDate?.getElapsedInterval_FEEDS() {
            self.lblTime.text = "Tamaaas " + strDate
        }
        
        
        if let arrQuestionOption = polling?.question_options {
            for var i in (0..<arrQuestionOption.count) {
                
                let optionObj = arrQuestionOption[i]
                
                if i == 0 {
                    self.btnOption1.isHidden = false
                    self.btnOption1.setTitle( optionObj.options, for: .normal)
                }
                
                if i == 1 {
                    self.btnOption2.isHidden = false
                    self.btnOption2.setTitle( optionObj.options, for: .normal)
                }
                
                if i == 2 {
                    self.btnOption3.isHidden = false
                    self.btnOption3.setTitle( optionObj.options, for: .normal)
                }
            }
        }
        
        
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.contentVw.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        self.contentVw.layer.shadowOpacity = 5
        self.contentVw.layer.shadowOffset = CGSize.zero
        self.contentVw.layer.shadowRadius = 5
        
        self.btnOption1.borderColor = UIColor.lightGray
        self.btnOption1.borderWidth = 1.0
        self.btnOption2.borderColor = UIColor.lightGray
        self.btnOption2.borderWidth = 1.0
        self.btnOption3.borderColor = UIColor.lightGray
        self.btnOption3.borderWidth = 1.0
        self.btnOption1.isHidden = true
        self.btnOption2.isHidden = true
        self.btnOption3.isHidden = true
    }

}
