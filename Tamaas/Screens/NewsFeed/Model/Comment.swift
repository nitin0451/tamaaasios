//
//  Comment.swift
//  Blocstagram
//
//  Created by ddenis on 1/22/17.
//  Copyright © 2017 ddApps. All rights reserved.
//

import Foundation

class Comment {
    
    var commentText: String?
    var uid: String?
    var name: String?
    var image: String?
    var time : String?
    var commentID : String?
    
}
