//
//  Post.swift
//  Blocstagram
//
//  Created by ddenis on 1/16/17.
//  Copyright © 2017 ddApps. All rights reserved.
//

import Foundation

class Post {
    var polling:[Polling] = []
    var caption: String?
    var createdDate: Date?
    var photoURL: String?
    var uid: String?
    var user_id : String?
    var id: String?
    var name: String?
    var likeCount: Int?
    var commentCount: Int?
    var isLiked: Bool?
    var type : String?
    var created : String?
    var thumbnail : String?
    var height : CGFloat = 300.0
    var width : CGFloat = 300.0
    var captionHeight:CGFloat = 0.0
    var superViewWidth:CGFloat = 0.0
    var phoneNumber: String?
    var quickBloxID: String?
    
    init(caption: String?, photoURL: String? , uid: String?, user_id: String? , id: String?, name: String?, likeCount: Int?, commentCount: Int?, isLiked: Bool?, type: String?, created: String?, thumbnail: String?,height: CGFloat, width: CGFloat,captionHeight: CGFloat, superViewWidth: CGFloat, phoneNumber: String? , createdDate: Date? , quickBloxID:String? , polling: [Polling]) {
        
        self.caption = caption
        self.photoURL = photoURL
        self.uid = uid
        self.user_id = user_id
        self.id = id
        self.name = name
        self.likeCount = likeCount
        self.commentCount = commentCount
        self.isLiked = isLiked
        self.type = type
        self.created = created
        self.thumbnail = thumbnail
        self.height = height
        self.width = width
        self.captionHeight = captionHeight
        self.superViewWidth = superViewWidth
        self.phoneNumber = phoneNumber
        self.createdDate = createdDate
        self.quickBloxID = quickBloxID
        self.polling = polling
        
    }
}


class Polling {
    var createdDate: Date?
    var givenAns: [GivenAns] = []
    var polling_id: String?
    var question : String?
    var type: String?
    var user_response: Bool?
    var question_options: [QuestionOptions] = []
    
    init(createdDate: Date?, givenAns: [GivenAns] , polling_id: String?, question: String? , type: String?, user_response: Bool?, question_options: [QuestionOptions]) {
        
        self.createdDate = createdDate
        self.givenAns = givenAns
        self.polling_id = polling_id
        self.question = question
        self.type = type
        self.user_response = user_response
        self.question_options = question_options
    }
}


class GivenAns {
    var answer: String?
    var value: Int?
    
    init(answer: String?, value: Int?) {
        self.answer = answer
        self.value = value
    }
}

class QuestionOptions {
    var options: String?
    
    init(options: String?) {
        self.options = options
    }
}



class Story {
    var id: String = ""
    var photoURL = ""
    var name = ""
    var isLastRead = true
    var total_stories_count = ""
}


class MediaPreview {
     var image: UIImage!
     var caption:String = ""
     var MediaType:String = ""
     var videoUrl:String = ""
}
