//
//  Notification_VC.swift
//  Tamaas
//
//  Created by Krescent Global on 21/06/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator
import YYCache
import YYWebImage
import YYCache

class notification {
    
    var FromNumber: String?
    var title: String?
    var body: String?
    var flag: String?
    var post_id : String?
    var status : String?
    var message: String?
    var postImageUrl: String?
    var fromName: String?
    var comment: String?
    var createdAt : String?
    var generatedmessage : String?
    var height_title:CGFloat?
    var height_subTitle:CGFloat?
    var subTitle:String?
    
}

class Notification_VC: BaseViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- Variables
    var arrNotifications:[notification] = []
    var arrNotificationsDB:[Notification_DB] = []
    let webservice  = WebserviceSigleton ()
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_updateNotificationList), name: constantVC.NSNotification_name.NSNotification_updateNotificationList , object: nil)
        
        if Connectivity.isConnectedToInternet() {
            CoreData_BussinessLogic.deleteAllNotifications()
            self.call_getAllNotifications()
        } else {
           self.arrNotificationsDB = CoreData_BussinessLogic.getAllNotificatiobs()
        }
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.configure_backButton()
        self.navigationItem.title = "Notifications".localized
        self.call_setNotification_status_read()
    }
    
    //MARK:- NSNotification observer methods
    @objc func handleNotification_updateNotificationList(_ notificationObj: NSNotification) {
        if let dic = notificationObj.object as? NSDictionary {
           
                let new_noti = notification()
                
                if let from_no = dic.object(forKey: "FromNumber") as? String{
                    new_noti.FromNumber = from_no
                }
                if let title_ = dic.object(forKey: "title") as? String{
                    new_noti.title = title_
                }
                if let gen_msg = dic.object(forKey: "generatedmessage") as? String{
                    new_noti.generatedmessage = gen_msg
                }
                if let flag_ = dic.object(forKey: "flag") as? String{
                    new_noti.flag = flag_
                }
                if let createdTime = dic.object(forKey: "createdAt") as? String{
                    let localDate = constantVC.UTCToLocal(date: createdTime)
                    
                    if localDate != "" {
                        let datefrmter = DateFormatter()
                        datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        let date = datefrmter.date(from: localDate)
                        new_noti.createdAt = date?.getElapsedInterval_with_TIME()
                    }
                    
                }
                if let comment_ = dic.object(forKey: "comment") as? String{
                    new_noti.comment = comment_
                }
                
                //calculate generated message height
                var cell_captionHeight:CGFloat = 0.0
                
            
                if (new_noti.generatedmessage != "") {
                    cell_captionHeight = (new_noti.generatedmessage?.height(withConstrainedWidth: self.tableView.frame.size.width - 131 , font: UIFont (name: "CircularStd-Book", size: 16)!))!
                    new_noti.height_title = cell_captionHeight
                }
            
                //calculate subtitle height
                var cell_subtitleHeight:CGFloat = 0.0
                var strSubTitle:String = ""
                
                if new_noti.flag == "like" || new_noti.flag == "profileUpdate" {
                    strSubTitle = new_noti.createdAt!
                }
                else {
                    strSubTitle = ":" + new_noti.comment! + " " + new_noti.createdAt!
                }
                
                new_noti.subTitle = strSubTitle
            
                if (strSubTitle != "") {
                    cell_subtitleHeight = (strSubTitle.height(withConstrainedWidth: self.tableView.frame.size.width - 131 , font: UIFont (name: "CircularStd-Book", size: 15)!))
                    new_noti.height_subTitle = cell_subtitleHeight
                }
            
                if let body_ = dic.object(forKey: "body") as? String{
                    new_noti.body = body_
                }
                if let postID = dic.object(forKey: "post_id") as? String{
                    new_noti.post_id = postID
                }
                if let status_ = dic.object(forKey: "status") as? String{
                    new_noti.status = status_
                }
                if let msg = dic.object(forKey: "message") as? String{
                    new_noti.message = msg
                }
                if let from_Name = dic.object(forKey: "fromName") as? String{
                    new_noti.fromName = from_Name
                }
                if let postImg = dic.object(forKey: "postImageUrl") as? String{
                    new_noti.postImageUrl = postImg
                }
                self.arrNotifications.insert(new_noti , at: 0)
             //update view
            DispatchQueue.main.async {
                //clear cache
                YYWebImageManager.shared().cache?.memoryCache.removeObject(forKey: constantVC.GeneralConstants.ImageUrl + String(describing: new_noti.fromName) + ".jpeg")
                YYWebImageManager.shared().cache?.diskCache.removeObject(forKey: constantVC.GeneralConstants.ImageUrl + String(describing: new_noti.fromName) + ".jpeg")
                self.tableView.beginUpdates()
                self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                self.tableView.endUpdates()
            }
        }
    }
    
    //MARK:- Custom Methods
    func configure_backButton(){
        let backButton = UIButton()
        backButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        backButton.setImage(UIImage(named: "blackBack")?.withRenderingMode(.alwaysOriginal), for: .normal)
        backButton.addTarget(self, action: #selector(btn_back_action), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    //MARK:- UIButton Actions
    @objc func btn_back_action(sender: UIButton!) {
        if constantVC.ActiveDataSource.isNavigateNotification {
            constantVC.ActiveDataSource.isNavigateNotification = false
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "tabBar") as! TabBarController
            self.present(controller, animated: true, completion: nil)
        } else {
            self.navigationController!.popViewController(animated: true)
        }
        self.call_setNotification_status_read()
    }
    
    //MARK:- Web service implementations
    func call_getAllNotifications() {
        
        self.arrNotifications = []
        let dictParam : NSDictionary = ["identity":"test"+SessionManager.getPhone()]
        
     //   self.startAnimating()
       
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_NOTIFICATIONS, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    if let arrNotifications = response["data"] as? [[String:Any]] {
                        
                        for item in arrNotifications {
                            
                            var fromNo:String = ""
                            
                            let new_noti = notification()
                            
                            if let from_no = item["FromNumber"] as? String{
                                fromNo = from_no
                                new_noti.FromNumber = from_no
                            }
                            if let title_ = item["title"] as? String{
                                new_noti.title = title_
                            }
                            if let gen_msg = item["generatedmessage"] as? String{
                                new_noti.generatedmessage = gen_msg
                            }
                            if let flag_ = item["flag"] as? String{
                                new_noti.flag = flag_
                            }
                            if let createdTime = item["createdAt"] as? String{
                                
                                let localDate = constantVC.UTCToLocal(date: createdTime)
                                
                                if localDate != "" {
                                    let datefrmter = DateFormatter()
                                    datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                    let date = datefrmter.date(from: localDate)
                                    new_noti.createdAt = date?.getElapsedInterval_with_TIME()
                                }
                            }
                            
                            if let comment_ = item["comment"] as? String{
                                let string2 = comment_.replacingOccurrences(of: "-", with: "\\")
                                new_noti.comment = self.setCommentTextWithEmojis(comment: string2)
                            }
                            
                            if new_noti.flag == "like" || new_noti.flag == "comment" {
                                
                                var generatedMsg:String = ""
                                new_noti.fromName = "+" + fromNo
                                generatedMsg = "+" + fromNo
                                
                                if SessionManager.getNameFromMyAddressBook(number:  fromNo) != "" {
                                    new_noti.fromName = SessionManager.getNameFromMyAddressBook(number:  fromNo)
                                    generatedMsg = SessionManager.getNameFromMyAddressBook(number:  fromNo)
                                }
                                
                                if new_noti.flag == "like" {
                                    new_noti.generatedmessage = generatedMsg + " liked your Post"
                                } else {
                                    new_noti.generatedmessage = generatedMsg + " commented on your Post"
                                }
                            }
                            
                            //calculate generated message height
                            var cell_captionHeight:CGFloat = 0.0
                            
                            if (new_noti.generatedmessage != "") {
                                cell_captionHeight = (new_noti.generatedmessage?.height(withConstrainedWidth: self.tableView.frame.size.width - 131 , font: UIFont (name: "CircularStd-Book", size: 16)!))!
                                new_noti.height_title = cell_captionHeight
                            }
                            
                            //calculate subtitle height
                            var cell_subtitleHeight:CGFloat = 0.0
                            var strSubTitle:String = ""
                            
                            if new_noti.flag == "like" || new_noti.flag == "profileUpdate" {
                                strSubTitle = new_noti.createdAt!
                            }
                            else {
                                strSubTitle = ": " + new_noti.comment! + " " + new_noti.createdAt!
                            }
                            
                            new_noti.subTitle = strSubTitle
                            
                            if (strSubTitle != "") {
                                cell_subtitleHeight = (strSubTitle.height(withConstrainedWidth: self.tableView.frame.size.width - 131 , font: UIFont (name: "CircularStd-Book", size: 15)!))
                                new_noti.height_subTitle = cell_subtitleHeight
                            }
                           
                            if let body_ = item["body"] as? String{
                                new_noti.body = body_
                            }
                            if let postID = item["post_id"] as? String{
                                new_noti.post_id = postID
                            }
                            if let status_ = item["status"] as? String{
                                new_noti.status = status_
                            }
                            if let msg = item["message"] as? String{
                                new_noti.message = msg
                            }
                           /* if let from_Name = item["fromName"] as? String{
                                new_noti.fromName = from_Name
                            }*/
                            if let postImg = item["postImageUrl"] as? String{
                                new_noti.postImageUrl = postImg
                            }
                            self.arrNotifications.append(new_noti)
                            
                            
                            //save Over DB
                            CoreData_BussinessLogic.saveNotifications(notification: new_noti)
                        }
                        
                        self.arrNotifications.reverse()
                        //self.stopAnimating()
                        self.tableView.reloadData()
                    }
                }
                else{
                    //self.stopAnimating()
                    self.tableView.isHidden = true
                    FTIndicator.showInfo(withMessage: "No Notification found!")
                }
            }
            else{
               // self.stopAnimating()
                self.tableView.isHidden = true
                FTIndicator.showInfo(withMessage: "No Notification found!")
            }
        })
    }
    
    //MARK:- Web service implementations
    func call_setNotification_status_read() {
        
        let dictParam : NSDictionary = ["identity":"test"+SessionManager.getPhone()]
        
        WebserviceSigleton().POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_SET_NOTIFICATION_STATUS_READ, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
        })
    }

}

//MARK:- UITableView Delegates and Data-source
extension Notification_VC: UITableViewDelegate , UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Connectivity.isConnectedToInternet() {
            return self.arrNotifications.count
        }
        return self.arrNotificationsDB.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if Connectivity.isConnectedToInternet() {
            if let noti = arrNotifications[indexPath.row] as? notification {
                return noti.height_title! + noti.height_subTitle! + 32
            }
        }
        else {
            if let noti = arrNotificationsDB[indexPath.row] as? Notification_DB {
                return CGFloat((noti.height_title! as NSString).floatValue) + CGFloat((noti.height_subTitle! as NSString).floatValue) + 32
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:Notification_TableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! Notification_TableViewCell
        if Connectivity.isConnectedToInternet() {
            cell.notification = self.arrNotifications[indexPath.row]
        }
        else {
            cell.notificationDB = self.arrNotificationsDB[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if Connectivity.isConnectedToInternet() {
            if self.arrNotifications[indexPath.row].flag! == "like" || self.arrNotifications[indexPath.row].flag! == "comment" {
                if let id =  self.arrNotifications[indexPath.row].post_id {
                    self.moveToPostDetail(postId: id)
                }
            }
        }
        else {
            if self.arrNotificationsDB[indexPath.row].flag! == "like" || self.arrNotificationsDB[indexPath.row].flag! == "comment" {
                if let id =  self.arrNotificationsDB[indexPath.row].post_id {
                    self.moveToPostDetail(postId: id)
                }
            }
        }
      
    }
    
    func moveToPostDetail(postId: String) {
       
        let next = self.storyboard?.instantiateViewController(withIdentifier: "PostDetail_VC") as! PostDetail_VC
        next.post_id = postId
        self.present(next, animated: true, completion: nil)
        
    }

}



//MARK:- TABLE CELL
class Notification_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgVw_userImage: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_subtitle: UILabel!
    @IBOutlet weak var imgVw_post: UIImageView!
    
    var notification: notification? {
        didSet {
            updateView()
        }
    }
    
    var notificationDB: Notification_DB? {
        didSet {
            updateViewDB()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lbl_title.text = ""
        lbl_subtitle.text = ""
    }
    
    func updateView() {
        
        self.lbl_subtitle.textColor = UIColor.black
        
        if notification?.flag == "profile" {
            self.imgVw_post.isHidden = false
        }
        else {
            self.imgVw_post.isHidden = false
        }
        
        if let photoURL = notification?.FromNumber {
            self.imgVw_userImage.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(photoURL)" + ".jpeg") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
        
        if let postImgURL = notification?.postImageUrl {
            self.imgVw_post.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(postImgURL)") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
        
        if let title = notification?.generatedmessage {
            let range = (notification?.generatedmessage! as! NSString).range(of: (notification?.fromName)!)
            let attribute = NSMutableAttributedString.init(string: (notification?.generatedmessage)!)
            attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 38.0 / 255.0, green: 145.0 / 255.0, blue: 208.0 / 255.0, alpha: 1.0) , range: range)
            self.lbl_title.attributedText = attribute
        }
        
        if notification?.flag == "like" || notification?.flag == "profile" {
            self.lbl_subtitle.textColor = UIColor.gray
            if let subTitle = notification?.subTitle {
                self.lbl_subtitle.text = subTitle
            }
        }
        else {
            let range = (notification?.subTitle! as! NSString).range(of: (notification?.createdAt)!)
            let attribute = NSMutableAttributedString.init(string: (notification?.subTitle)!)
            attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.gray , range: range)
            self.lbl_subtitle.attributedText = attribute
        }
    }
    
    
    func updateViewDB() {
        
        self.lbl_subtitle.textColor = UIColor.black
        
        if notificationDB?.flag == "profile" {
            self.imgVw_post.isHidden = true
        }
        else {
            self.imgVw_post.isHidden = false
        }
        
        if let photoURL = notificationDB?.fromNumber {
            self.imgVw_userImage.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(photoURL)" + ".jpeg") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
        
        if let postImgURL = notificationDB?.postImageUrl {
            self.imgVw_post.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(postImgURL)") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
        
        if let title = notificationDB?.generatedmessage {
            let range = (notificationDB?.generatedmessage! as! NSString).range(of: (notificationDB?.fromName)!)
            let attribute = NSMutableAttributedString.init(string: (notificationDB?.generatedmessage)!)
            attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor(red: 38.0 / 255.0, green: 145.0 / 255.0, blue: 208.0 / 255.0, alpha: 1.0) , range: range)
            self.lbl_title.attributedText = attribute
        }
        
        if notificationDB?.flag == "like" || notificationDB?.flag == "profile" {
            self.lbl_subtitle.textColor = UIColor.gray
            if let subTitle = notificationDB?.subTitle {
                self.lbl_subtitle.text = subTitle
            }
        }
        else {
            let range = (notificationDB?.subTitle! as! NSString).range(of: (notificationDB?.createdAt)!)
            let attribute = NSMutableAttributedString.init(string: (notificationDB?.subTitle)!)
            attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.gray , range: range)
            self.lbl_subtitle.attributedText = attribute
        }
    }
}
