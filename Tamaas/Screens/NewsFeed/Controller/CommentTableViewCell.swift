//
//  CommentTableViewCell.swift
//  Blocstagram
//
//  Created by ddenis on 1/20/17.
//  Copyright © 2017 ddApps. All rights reserved.
//

import UIKit
import SwipeCellKit
import YYCache
import YYWebImage

class CommentTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var dateLab: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!

    var comment: Comment? {
        didSet {
            updateView()
        }
    }
    
    var commentDB: FeedComment_DB? {
        didSet {
            updateViewDB()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.text = ""
        dateLab.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateView() {
        var name_ = String()
        var comment_ = String()
        
        if SessionManager.getNameFromMyAddressBook(number:  (comment?.image)!) != "" {
            name_ = SessionManager.getNameFromMyAddressBook(number:  (comment?.image)!)
        }else {
            if let name = comment?.name{
                name_ = name
            }
        }
        
        if let comment = comment?.commentText!{
            comment_ = self.setCommentTextWithEmojis(comment: comment)
        }
        
        let formattedString = NSMutableAttributedString()
        formattedString
            .bold(name_)
            .normal(" \(comment_)\n")
        self.nameLabel.attributedText = formattedString
       
        if let time = comment?.time!{
             self.dateLab.text = time
        }
        
        if let photoURL = comment?.image {
            profileImageView.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(photoURL)" + ".jpeg") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
    }
    
    
    func updateViewDB() {
        var name_ = String()
        var comment_ = String()
        
        if SessionManager.getNameFromMyAddressBook(number:  (commentDB?.user_phoneNumber)!) != "" {
            name_ = SessionManager.getNameFromMyAddressBook(number:  (commentDB?.user_phoneNumber)!)
        }else {
            if let name = commentDB?.user_name{
                name_ = name
            }
        }
        
        
        if let comment = commentDB?.comment!{
            comment_ = self.setCommentTextWithEmojis(comment: comment)
        }
        
        let formattedString = NSMutableAttributedString()
        formattedString
            .bold(name_)
            .normal(" \(comment_)\n")
        self.nameLabel.attributedText = formattedString
        
        if let time = commentDB?.created! {
            self.dateLab.text = time
        }
        
        if let photoURL = (commentDB?.user_phoneNumber)! as? String {
            profileImageView.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(photoURL)" + ".jpeg") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
    }
    
    
    func setCommentTextWithEmojis(comment:String) -> String{
        let trimmedString = comment.trimmingCharacters(in:NSCharacterSet.whitespacesAndNewlines)
        let data2 = trimmedString.data(using: String.Encoding.utf8)!
        let messagestring = String(data: data2, encoding: String.Encoding.nonLossyASCII)
        return messagestring!
    }
    
    func updateUserInfo() {
        nameLabel.text = comment?.commentText
        profileImageView.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(comment?.image)" + ".jpeg") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
    }
    
    // flush the user profile image before a reuse
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.image = UIImage(named: "profile")
    }

}
