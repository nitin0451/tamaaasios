//
//  FAImageCropperVC.swift
//  FAImageCropper
//
//  Created by Fahid Attique on 11/02/2017.
//  Copyright © 2017 Fahid Attique. All rights reserved.
//

import UIKit
import MobileCoreServices
class FAImageCropperVC: BaseViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    // MARK: IBOutlets
    
    @IBOutlet weak var scrollContainerView: UIView!
    @IBOutlet weak var scrollView: FAScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var barBtn_cancel: UIBarButtonItem!
    @IBOutlet weak var barBtn_Next: UIBarButtonItem!
    
    var assestUrl: URL? = nil
    // MARK: Public Properties
    
    var photos:[PHAsset]!
    var imageViewToDrag: UIImageView!
    var indexPathOfImageViewToDrag: IndexPath!
    
    let cellWidth = ((UIScreen.main.bounds.size.width)/4)-1
  
    
    // MARK: Private Properties
    
    private let imageLoader = FAImageLoader()
//    private var croppedImage: UIImage? = nil
    var imageToPaas = UIImage()
    @IBOutlet weak var videoBtn: UIButton!
    @IBOutlet weak var libraryBtn: UIButton!
    @IBOutlet weak var photoBtn: UIButton!
    // MARK: LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localise_language()
        // Do any additional setup after loading the view, typically from a nib.\
        checkForPhotosPermission()
    }
    
    func localise_language(){
        self.barBtn_cancel.title = "Cancel".localized
        self.barBtn_Next.title = "Next".localized
        self.libraryBtn.setTitle("Library" , for: .normal)
        self.photoBtn.setTitle("Photo" , for: .normal)
        self.videoBtn.setTitle("Video" , for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "camera" {
            let cameraVC = segue.destination as? CameraViewController
            
            cameraVC?.selectedImage = imageToPaas
            if constantVC.GlobalVariables.shCamera == false{
                cameraVC?.videoUrl = assestUrl
            }
        }
    }
    @IBAction func doneAc(_ sender: Any) {
        
        /*let vc = SHViewController(image: captureVisibleRect())
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)*/
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "FeedImageFilter_VC") as! FeedImageFilter_VC
        controller.mainImg = captureVisibleRect()
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
        
    }
   
    
    @IBAction func cancelAc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func libraryAc(_ sender: Any) {
        constantVC.GlobalVariables.shCamera = true
        libraryBtn.setTitleColor(UIColor(red: 54.0/255.0, green : 54.0/255.0, blue: 54.0/255.0, alpha:1.0), for: UIControlState.normal)
        photoBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        videoBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        loadPhotos()
    }
    @IBAction func photoAc(_ sender: Any) {
        photoBtn.setTitleColor(UIColor(red: 54.0/255.0, green : 54.0/255.0, blue: 54.0/255.0, alpha:1.0), for: UIControlState.normal)
        libraryBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        videoBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        
        pickFromCamera()
    }
    
    func pickFromCamera(){
        let imagePicker  = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        imagePicker.cameraFlashMode = .off
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        imagePicker.cameraCaptureMode = .photo
     self.present(imagePicker, animated: true){
           UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
     }
    }
    
    func imageOrientation(_ src:UIImage)->UIImage {
        if src.imageOrientation == UIImageOrientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
         if let myImage = info[UIImagePickerControllerOriginalImage] {
            constantVC.GlobalVariables.shCamera = true
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "FeedImageFilter_VC") as! FeedImageFilter_VC
            controller.mainImg = self.imageOrientation(myImage as! UIImage)
            controller.delegate = self
            self.present(controller, animated: true, completion: nil)
        }
         else{
            
            let manager = FileManager.default
            constantVC.GlobalVariables.shCamera = false
            guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else {return}
            guard let mediaType = "mp4" as? String else {return}
            guard let url = info[UIImagePickerControllerMediaURL] as? NSURL else {return}
            if info[UIImagePickerControllerMediaType] as? String == (kUTTypeMovie as? String) {
                let asset = AVAsset(url: url as URL)
                let length = Float(asset.duration.value) / Float(asset.duration.timescale)
                let start = 0
                let end = 60.0
                
                var outputURL = documentDirectory.appendingPathComponent("output")
                do {
                    try manager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                    let name = "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).mp4"
                    outputURL = outputURL.appendingPathComponent("\(name)")
                }catch let error {
                    print(error)
                }
                
                //Remove existing file
                _ = try? manager.removeItem(at: outputURL)
                
                
                guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {return}
                exportSession.outputURL = outputURL
                exportSession.outputFileType = AVFileType.mp4
                
                let startTime = CMTime(seconds: Double(start), preferredTimescale: 1000)
                let endTime = CMTime(seconds: Double(end), preferredTimescale: 1000)
                let timeRange = CMTimeRange(start: startTime, end: endTime)
                
                exportSession.timeRange = timeRange
                exportSession.exportAsynchronously{
                    switch exportSession.status {
                    case .completed:
                        self.assestUrl = outputURL
                        
                        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
                        
                        var time = asset.duration
                        time.value = min(time.value, 1)
                        
                        do {
                            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
                            self.scrollView.imageToDisplay = UIImage(cgImage: imageRef)
                        } catch {
                            print("error")
                        }
                        
                    case .failed:
                        print("failed \(exportSession.error)")
                        
                    case .cancelled:
                        print("cancelled \(exportSession.error)")
                        
                    default: break
                    }
                }
            }
            
        }
        
    }
    private func captureVisibleRect() -> UIImage{
        
        var croprect = CGRect.zero
        let xOffset = (scrollView.imageToDisplay?.size.width)! / scrollView.contentSize.width;
        let yOffset = (scrollView.imageToDisplay?.size.height)! / scrollView.contentSize.height;
        
        croprect.origin.x = scrollView.contentOffset.x * xOffset;
        croprect.origin.y = scrollView.contentOffset.y * yOffset;
        
        let normalizedWidth = (scrollView?.frame.width)! / (scrollView?.contentSize.width)!
        let normalizedHeight = (scrollView?.frame.height)! / (scrollView?.contentSize.height)!
        
        croprect.size.width = scrollView.imageToDisplay!.size.width * normalizedWidth
        croprect.size.height = scrollView.imageToDisplay!.size.height * normalizedHeight
        
        let toCropImage = scrollView.imageView.image?.fixImageOrientation()
        let cr: CGImage? = toCropImage?.cgImage?.cropping(to: croprect)
        let cropped = UIImage(cgImage: cr!)
        
        return cropped
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    @IBAction func videoAc(_ sender: Any) {
        constantVC.GlobalVariables.shCamera = false
        photoBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
         videoBtn.setTitleColor(UIColor(red: 54.0/255.0, green : 54.0/255.0, blue: 54.0/255.0, alpha:1.0), for: UIControlState.normal)
        libraryBtn.setTitleColor(UIColor(red: 163.0/255.0, green : 163.0/255.0, blue: 163.0/255.0, alpha:1.0), for: UIControlState.normal)
        loadPhotos()
    }
    
    // MARK: Private Functions
    override  func viewWillAppear(_ animated: Bool) {
       
      UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
    }
    
    private func checkForPhotosPermission(){
        
        // Get the current authorization state.
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            loadPhotos()
        }
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
        }
        else if (status == PHAuthorizationStatus.notDetermined) {
            
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == PHAuthorizationStatus.authorized) {
                    
                    DispatchQueue.main.async {
                        self.loadPhotos()
                    }
                }
                else {
                    // Access has been denied.
                }
            })
        }
            
        else if (status == PHAuthorizationStatus.restricted) {
            // Restricted access - normally won't happen.
        }
    }
    
    private func loadPhotos(){
        imageLoader.loadPhotos { (assets) in
            self.configureImageCropper(assets: assets)
        }
    }
    
    private func configureImageCropper(assets:[PHAsset]){
        if assets.count != 0{
            photos = assets
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.reloadData()
            selectImageFromAssetAtIndex(index: 0)
        }
    }

        private func isSquareImage() -> Bool{
        let image = scrollView.imageToDisplay
        if image?.size.width == image?.size.height { return true }
        else { return false }
    }

    
    // MARK: Public Functions
    func selectImageFromAssetAtIndex(index:NSInteger){
     
        let assetss = photos[photos.count - 1 - index]
        FAImageLoader.imageFrom(asset: photos[photos.count  - 1 - index], size: PHImageManagerMaximumSize) { (image) in
            DispatchQueue.main.async {
                self.displayImageInScrollView(image: image)
            }
        }
        if constantVC.GlobalVariables.shCamera == false{
            let manager = FileManager.default

            guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else {return}
            guard let mediaType = "mp4" as? String else {return}

            let start = 0
            let end = 45.0

            var outputURL = documentDirectory.appendingPathComponent("output")
            do {
                try manager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                let name = "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).mp4"
                outputURL = outputURL.appendingPathComponent("\(name)")
            }catch let error {
                print(error)
            }

            //Remove existing file
            _ = try? manager.removeItem(at: outputURL)

            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: assetss, options: options, resultHandler: { (asset, audioMix, info) in

                if asset != nil{
                    guard let exportSession = AVAssetExportSession(asset: asset!, presetName: AVAssetExportPresetHighestQuality) else {return}
                    exportSession.outputURL = outputURL
                    exportSession.outputFileType = AVFileType.mp4
                    
                    let startTime = CMTime(seconds: Double(start), preferredTimescale: 1000)
                    let endTime = CMTime(seconds: Double(end), preferredTimescale: 1000)
                    let timeRange = CMTimeRange(start: startTime, end: endTime)
                    
                    exportSession.timeRange = timeRange
                    exportSession.exportAsynchronously{
                        switch exportSession.status {
                        case .completed:
                            self.assestUrl = outputURL
                            
                        case .failed:
                            print("failed \(exportSession.error)")
                            
                        case .cancelled:
                            print("cancelled \(exportSession.error)")
                            
                        default: break
                        }
                    }
                }
               
            })
        }
    }
    
    func displayImageInScrollView(image:UIImage){
        self.scrollView.imageToDisplay = image
    }
    func replicate(_ image:UIImage) -> UIImage? {
        
        guard let cgImage = image.cgImage?.copy() else {
            return nil
        }
        return UIImage(cgImage: cgImage,
                               scale: image.scale,
                               orientation: image.imageOrientation)
    }
    

    @objc func handleLongPressGesture(recognizer: UILongPressGestureRecognizer) {

        let location = recognizer.location(in: view)

        if recognizer.state == .began {

            let cell: FAImageCell = recognizer.view as! FAImageCell
            indexPathOfImageViewToDrag = collectionView.indexPath(for: cell)
            imageViewToDrag = UIImageView(image: replicate(cell.imageView.image!))
            imageViewToDrag.frame = CGRect(x: location.x - cellWidth/2, y: location.y - cellWidth/2, width: cellWidth, height: cellWidth)
            view.addSubview(imageViewToDrag!)
            view.bringSubview(toFront: imageViewToDrag!)
        }
        else if recognizer.state == .ended {
            
            if scrollView.frame.contains(location) {
                collectionView.selectItem(at: indexPathOfImageViewToDrag, animated: true, scrollPosition: UICollectionViewScrollPosition.centeredVertically)
              selectImageFromAssetAtIndex(index: indexPathOfImageViewToDrag.item)
            }
            
            imageViewToDrag.removeFromSuperview()
            imageViewToDrag = nil
            indexPathOfImageViewToDrag = nil
        }
        else{
            imageViewToDrag.center = location
        }
    }
}

extension FAImageCropperVC:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell:FAImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FAImageCell", for: indexPath) as! FAImageCell
       
        
        cell.populateDataWith(asset: photos[photos.count - 1 - indexPath.item])
        cell.configureGestureWithTarget(target: self, action: #selector(FAImageCropperVC.handleLongPressGesture))
        
        return cell
    }
}


extension FAImageCropperVC:UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     if let photos = photos{
          return photos.count
     }
     return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell:FAImageCell = collectionView.cellForItem(at: indexPath) as! FAImageCell
        cell.isSelected = true
        
        selectImageFromAssetAtIndex(index: indexPath.item)
    }
}


extension FAImageCropperVC:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: cellWidth)
    }
}

extension FAImageCropperVC: FeedImageFilter_VC_Delegate {
    func FeedImageFilter_VC_ImageDidFilter(image: UIImage) {
        imageToPaas = image
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CameraViewController") as! CameraViewController
                controller.selectedImage = imageToPaas
            if constantVC.GlobalVariables.shCamera == false{
                controller.videoUrl = assestUrl
         }
        self.navigationController!.pushViewController(controller, animated: true)
    }
    
    func FeedImageFilter_VC_DidCancel() {
        
    }
}
