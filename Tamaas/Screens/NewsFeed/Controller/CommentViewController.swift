//
//  CommentViewController.swift
//  Blocstagram
//
//  Created by ddenis on 1/20/17.
//  Copyright © 2017 ddApps. All rights reserved.
//

import UIKit
import SwipeCellKit
import YYWebImage
import FTIndicator

class CommentViewController: BaseViewController , UITextViewDelegate {
    
    @IBOutlet weak var textView_comment: UITextView!
    @IBOutlet weak var imgVw_user: UIImageView!
    @IBOutlet weak var inputBar_bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var inputBar_heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputBar: UIView!
    var postID: String!
    var userTo_id: String!
    var user_quickbloxID: String!
    var postImg: String!
    var comments = [Comment]()
    var commentsDB = [FeedComment_DB]()
    var isPushFrom_PostDetailVC:Bool = false
    @IBOutlet weak var sendButton: UIButton!
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Comments".localized
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        // for performance set an estimated row height
        tableView.estimatedRowHeight = 70
        // but also request to dynamically adjust to content using AutoLayout
        tableView.rowHeight = UITableViewAutomaticDimension
        textView_comment.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        if Connectivity.isConnectedToInternet() {
            CoreData_BussinessLogic.deleteAllCommentsOfFeed(postID: self.postID)
        } else {
            self.commentsDB = CoreData_BussinessLogic.getFeedComment_byPostID(postID: self.postID)
        }
        
        loadComments()
        configInputBar()
       prepareForNewComment()
        tableView.rowHeight = UITableViewAutomaticDimension;

    }
    //var  sendButton = UIButton()
    
    func configInputBar(){
        textView_comment.delegate = self
        self.textView_comment.text = "Type here...".localized
        self.textView_comment.textColor = UIColor.lightGray
        textView_comment.font = UIFont(name: "CircularStd-Book", size: 16)
        sendButton.addTarget(self, action: #selector(send), for: UIControlEvents.touchUpInside)
        sendButton.setTitleColor(UIColor(red: 32.0/255.0, green: 101.0/255.0, blue: 211.0/255.0, alpha: 1.0), for: UIControlState.normal)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.tapGestureHandler))
        tableView.addGestureRecognizer(tap)
        // *** Listen to keyboard show / hide ***
        
        self.imgVw_user.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(SessionManager.getPhone()).jpeg") , placeholder: UIImage(named:"contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        
        
    }
    
    @IBAction func backAc(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    override  func viewWillAppear(_ animated: Bool) {
       
       
      //  self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = view.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    if !self.isIphoneX() {
                        keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                    }
                }
            }
            inputBar_bottomConstraint.constant = keyboardHeight
            view.layoutIfNeeded()
        }
    }
    // MARK: - Firebase Save Operation
    func textViewDidChange(_ textView: UITextView) {
        guard let comment = textView.text, !comment.trimmingCharacters(in: .whitespaces).isEmpty else {
            // disable Send button if comment is blank and return
            disableButton()
            return
        }
        // otherwise enable the Send button
        enableButton()
        //set text view growing view
        if textView.contentSize.height > textView.frame.size.height {
            
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            var newFrame = textView.frame
            
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            
            
            if newSize.height > self.inputBar.frame.size.height && newSize.height < 100 {
                self.inputBar_heightConstraint.constant = newSize.height
            }
            
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView_comment.textColor = UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 1.0)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type here...".localized
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    
    @objc func send() {

     self.view.endEditing(true)
    
    if Connectivity.isConnectedToInternet() {
        let webservice  = WebserviceSigleton ()
        var strComment = ""
        
       let data = self.textView_comment.text.data(using: String.Encoding.nonLossyASCII)!
       strComment = String(data: data, encoding: String.Encoding.utf8)!
        
        let strComment_encode = strComment.replacingOccurrences(of: "\\", with: "-")
        
        self.prepareForNewComment()
    
        let dictParam : NSDictionary = ["post_id":postID, "user_id": SessionManager.getUserId(), "comment": strComment_encode]
      //  self.startAnimating()
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_POST_COMMENT, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    if self.isPushFrom_PostDetailVC {
                        constantVC.GlobalVariables.isNeedToRefreshPostDetails = true
                    }
                    
                    var comment_id:String = ""
                    
                    
                    if self.userTo_id != SessionManager.getPhone() {
                    //send notification
                    let id_to = "test" + self.userTo_id
                   
                    if let opponentID = self.user_quickbloxID {
                        if opponentID != "" {
                            QuickBloxManager().sendPushNotification(msg: "Post Comment" , users: opponentID , identity: id_to , title: "Post Comment" , fromNo: SessionManager.getPhone() , toNo: self.userTo_id , flag: "comment", postId: self.postID , postImgUrl: self.postImg , comment: strComment)
                        }
                    }
                    
                    webservice.send_notification(identityTo: id_to , title: "Post Comment" , msg: " commented on your post", toNo: self.userTo_id , flag: "comment" , post_id: self.postID ,postImageUrl: self.postImg , comment: strComment)
                    }
                    
                    UserDefaults.standard.set(true , forKey: "isNeedToReloadComments")
                    
                    
                    //insert new comment into local array
                    if let commentID = response["msg"] as? String {
                        comment_id = commentID
                    }
                        let newComment = Comment()
                        newComment.uid = SessionManager.getUserId()
                        newComment.commentID = comment_id
                        newComment.commentText = strComment
                       newComment.time = Date().getElapsedInterval()
                       newComment.image = SessionManager.getPhone()
                       newComment.name = SessionManager.getUsername()
                    
                       self.comments.insert(newComment, at: 0)
                    
                    //update view
                    DispatchQueue.main.async {
                         self.tableView.beginUpdates()
                         self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                         self.tableView.endUpdates()
                    }
                }
            }
        })
    } else {
          FTIndicator.showToastMessage("No Internet Connection!")
    }
    }
    func scrollToBottom() {
        if comments.count > 0 {
            let indexPath = IndexPath(row: 0, section: 0)
            tableView?.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    // MARK: - Load Comments from Firebase
    
    func loadComments() {
            self.comments = []
            let webservice  = WebserviceSigleton ()
            let dictParam : NSDictionary = ["post_id":postID]
            self.startAnimating()
            webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_COMMENTS, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
                if result != nil{
                    let response = result! as Dictionary
                    let resultMessage = response["msg"] as! String
                    if resultMessage == "success"{
                        if let commentDetails = response["comments_details"] as? [[String:Any]] {

                            for item in commentDetails {
                                let newComment = Comment()
                                if let _userid = item["user_id"] as? String{
                                    newComment.uid = _userid
                                }
                                
                                var commentID: String = ""
                                if item["id"] != nil {
                                    commentID = String(describing: item["id"]!)
                                }
                                
                                newComment.commentID = commentID

                                if let comment = item["comment"] as? String{
                                    let string2 = comment.replacingOccurrences(of: "-", with: "\\")
                                    newComment.commentText = string2
                                }
                                if let created = item["created"] as? String{
                                    
                                    let localDate = constantVC.UTCToLocal(date: created)
                                    
                                    if localDate != "" {
                                        let datefrmter = DateFormatter()
                                        datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                        let date = datefrmter.date(from: localDate)
                                        newComment.time = DateTodayFormatter().timeAgoSinceDate(date: (date as NSDate?)! )
                                    }
                                    
                                }
                                if let image = item["user_phoneNumber"] {
                                    newComment.image = "\(image)"
                                }
                                if let name = item["user_name"] as? String{
                                    newComment.name = name
                                }
                                
                                if SessionManager.getNameFromMyAddressBook(number: newComment.image!) != "" {
                                    newComment.name = SessionManager.getNameFromMyAddressBook(number:newComment.image!)
                                }
                                
                                //save comment over DB
                                CoreData_BussinessLogic.saveOrUpdateComment(comment: newComment , postID: self.postID)

                                self.comments.append(newComment)
                            }
                           
                            self.stopAnimating()
                            self.tableView.reloadData()
                            self.scrollToBottom()
                        }
                    }
                    else{
                        self.stopAnimating()
                    }
                }
                else{
                    self.stopAnimating()
                }
            })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    // MARK: - UI Methods
    
    func prepareForNewComment() {
        self.textView_comment.text = ""
        disableButton()
    }
    
    func enableButton() {
        sendButton.alpha = 1.0
        sendButton.isEnabled = true
    }

    func disableButton() {
        sendButton.alpha = 0.2
        sendButton.isEnabled = false
    }
    
    //MARK:- Web Servciec implementation 
    func removeComment(index: Int){
        
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_id":SessionManager.getUserId() , "type":"post_comments","id": comments[index].commentID!]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_REMOVEDATA, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    UserDefaults.standard.set(true , forKey: "isNeedToReloadComments")
                    self.comments.remove(at: index)
                    
                    if let indexPath = IndexPath(row: index , section: 0) as? IndexPath {
                        self.tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
                    }
                }
            }
            else{
                print(error?.localizedDescription)
            }
        })
    }
}


// MARK: - TableView Delegate and Data Source Methods

extension CommentViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Connectivity.isConnectedToInternet() {
            return comments.count
        }
        return self.commentsDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentTableViewCell
         if Connectivity.isConnectedToInternet() {
             cell.comment = comments[indexPath.row]
         } else {
             cell.commentDB = commentsDB[indexPath.row]
        }
        return cell
    }
    
}
extension CommentViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if Connectivity.isConnectedToInternet() {
            if comments[indexPath.row].uid == SessionManager.getUserId() {
                return true
            }
        }
        
        return false
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
              self.removeComment(index: indexPath.row)
        }
    }
}

