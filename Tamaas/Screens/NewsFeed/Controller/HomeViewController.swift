import UIKit
import MMPlayerView
import Alamofire
import AANotifier
import FTIndicator
import Shimmer



struct IconItem: PagingItem, Hashable, Comparable {
    
    let icon: String
    let index: Int
    let image: UIImage?
    
    init(icon: String, index: Int) {
        self.icon = icon
        self.index = index
        self.image = UIImage(named: icon)
    }
    
    var hashValue: Int {
        return icon.hashValue
    }
    
    static func <(lhs: IconItem, rhs: IconItem) -> Bool {
        return lhs.index < rhs.index
    }
    
    static func ==(lhs: IconItem, rhs: IconItem) -> Bool {
        return (
            lhs.index == rhs.index &&
                lhs.icon == rhs.icon &&
                lhs.image == rhs.image
        )
    }
}

class HomeViewController: BaseViewController, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout , UIActionSheetDelegate{
    
    //MARK:- Outlets
    @IBOutlet weak var reportView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayout: FlowLayout!
    @IBOutlet weak var btn_report: UIButton!
    @IBOutlet weak var vw_blur: UIView!
    
    //startUp View
    @IBOutlet weak var vwNoPost: UIView!
    @IBOutlet weak var lblExplore: UILabel!
    @IBOutlet weak var lblExploreMsg: UILabel!
    @IBOutlet weak var lblShareYourFirstPost: UILabel!
    
    
    //MARK:- Variables
    let notificationButton = Custom_Notification_BarButton()
    var refreshControl: UIRefreshControl!
    var posts = [Post]()
    var posts_DB = [Feed_DB]()
    var polling = [Polling]()
    var i = 1
    var totalPages = 1
    var pollingCount:Int = 0
    
    fileprivate let icons = [
        "Stories"
    ]
    let TAGS = ["Nudity", "Voilence", "Harassment", "Suicide", "False News", "Spam", "Unauthorized Sale", "Hate Speech", "Racism"]
    
    
    var sizingCell: TagCell?
    var tags = [Tag]()
    var isFinishingUpNotifier_active :Bool = false
    var shimmeringView = FBShimmeringView()
    var isMuted:Bool = true
    
    //MARK:- Notifiers
    lazy var homeFetchingNotifier: AANotifier = {
        let notifierView = HomeFetchingData()
        let options: [AANotifierOptions] = [
            //.duration(0.4), // Time interval for animation
            .transitionA(.fromTop, 0.40), // Show notifier animation
            .transitionB(.toTop, 0.40), // Hide notifier animation
            .position(.top), // notifier position
            .preferedHeight(96), // notifier height
            .margins(H: 0, V: 0) // notifier margins
        ]
        let notifier = AANotifier(notifierView, withOptions: options)
        return notifier
    }()
    
    
    lazy var mmPlayerLayer: MMPlayerLayer = {
        let l = MMPlayerLayer()
        l.cacheType = .memory(count: 5)
        return l
    }()
    
    //MARK:- UIView life-cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // for performance set an estimated row height
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeGround), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_postlikeComment), name: constantVC.NSNotification_name.NSNotification_postLikeComment , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_bellUnreadCount), name: constantVC.NSNotification_name.NSNotification_bellUnreadCount , object: nil)
        
        //init
        self.vw_blur.isHidden = true
        self.reportView.isHidden = true
        
        self.configure_topNotificationBellButton()
        refreshControl = UIRefreshControl()
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(self.refreshChannels), for: .valueChanged)
        refreshControl.tintColor = UIColor.lightGray
        tableView.dataSource = self
        tableView.delegate   = self
        
        //register nib for polling cell
        tableView.register(UINib(nibName: "Polling_tableViewCell", bundle: nil), forCellReuseIdentifier: "pollingCell")
        loadPaging()
        
        tableView.estimatedRowHeight = 410
        tableView.rowHeight = UITableViewAutomaticDimension
        
        let cellNib = UINib(nibName: "TagCell", bundle: nil)
        self.collectionView.register(cellNib, forCellWithReuseIdentifier: "TagCell")
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.sizingCell = (cellNib.instantiate(withOwner: nil, options: nil) as NSArray).firstObject as! TagCell?
        self.flowLayout.sectionInset = UIEdgeInsetsMake(8, 8, 8, 8)
        for name in TAGS {
            let tag = Tag()
            tag.name = name
            self.tags.append(tag)
        }
        self.localiseNoPostView()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadViewOnTab(notification:)), name: NSNotification.Name(rawValue: "notifyReload"), object: nil)
     }
    
    
    override func viewWillAppear(_ animated: Bool) {
        print(mmPlayerLayer.currentPlayStatus)
        mmPlayerLayer.player?.isMuted = self.isMuted
     
        //instant update noti count
        if constantVC.GlobalVariables.isfeedNotiTapped {
            constantVC.GlobalVariables.isfeedNotiTapped = false
            self.setTabBar_BadgeCount(badgeCount: 0)
            self.notificationButton.badge = nil
        }
        
        //get feeds from DB
        self.posts_DB = CoreData_BussinessLogic.getFeeds()
        
        if self.posts.count < 1 || constantVC.ActiveDataSource.isNeedToRefreshPost {
            constantVC.ActiveDataSource.isNeedToRefreshPost = false
            self.i = 1
            loadPosts(pageno: 1 , hideFinishingPopup: false , showShimmerEffect: true)
        }else {
           //self.updateTimeStamp()
        }
    
        //self.tableView.reloadData()
        
        if !Connectivity.isConnectedToInternet() {
            if SessionManager.get_unreadNotificationCount() > 0 {
                self.setTabBar_BadgeCount(badgeCount: SessionManager.get_unreadNotificationCount())
                self.notificationButton.addBadgeToButon(badge: "\(SessionManager.get_unreadNotificationCount())")
            }
        }
        
        //get notification unread count
        self.call_getNotification_UnreadCount()
        
        if (UserDefaults.standard.bool(forKey: "isNeedToReloadComments")) {
            UserDefaults.standard.set(false , forKey: "isNeedToReloadComments")
            self.refreshChannels()
        }
        else {
            tableView.allowsSelection = false;
            self.navigationController?.navigationBar.topItem?.title = "Explore".localized
           // self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Bold",size:18)!]
            
            if constantVC.GlobalVariables.shPost == true{
                constantVC.GlobalVariables.shPost = false
                
                isFinishingUpNotifier_active = true
                self.vw_blur.isHidden = false
                self.homeFetchingNotifier.show()
                
                if constantVC.GlobalVariables.shCamera == true{
                    postToTamaas()
                } else {
                    self.compressVideoData()
                }
                
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        mmPlayerLayer.player?.pause()
        mmPlayerLayer.autoPlay = false
        
        
        if (isFinishingUpNotifier_active) {
            isFinishingUpNotifier_active = false
            self.homeFetchingNotifier.hide()
        }
        if constantVC.ActiveSession.isCallActive == false {
            do {
                try AVAudioSession.sharedInstance().setActive(false, with: .notifyOthersOnDeactivation)
            } catch {
               print("error")
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopPlayer()
    }
    
    func stopPlayer(){
        if mmPlayerLayer.player != nil {
            mmPlayerLayer.isHidden = true
            mmPlayerLayer.player?.pause()
            mmPlayerLayer.player?.replaceCurrentItem(with: nil)
            mmPlayerLayer.removeAllObserver()
           // mmPlayerLayer.player = nil
        }
    }
        
    //MARK:- NSNotification observer methods
    @objc func appMovedToBackground() {
        if mmPlayerLayer.player != nil {
            mmPlayerLayer.isHidden = true
            mmPlayerLayer.player?.play()
            mmPlayerLayer.player?.replaceCurrentItem(with: nil)
            mmPlayerLayer.removeAllObserver()
            //mmPlayerLayer.player = nil
        }
    }
    
    @objc func appMovedToForeGround() {
              self.mmPlayerLayer = MMPlayerLayer()
         mmPlayerLayer.cacheType = .memory(count: 5)
    }
    
    @objc func handleNotification_postlikeComment(_ notification: NSNotification) {
        if let dic = notification.object as? [String : Any] {
            
            if let id = dic["post_id"] as? String {
                if constantVC.GlobalVariables.allpost_id.contains(id) {
                    if let row = constantVC.GlobalVariables.allpost_id.index(of: id) {
                        DispatchQueue.main.async {
                            
                            if let cell = self.tableView.cellForRow(at: IndexPath(row: row , section: 0)) as? HomeTableViewCell {
                            
                            if let isLike = dic["isLike"] as? Bool {
                                if isLike {
                                    //for like
                                    if let like_count = cell.likesCountLab.text as? String {
                                        cell.likesCountLab.text = "\(Int(like_count)! + 1)"
                                    }
                                }
                                else {
                                    //for comment
                                    if let comment_count = cell.commentCountLab.text as? String {
                                        cell.commentCountLab.text = "\(Int(comment_count)! + 1)"
                                    }
                                }
                            }}
                        }
                    }
                }
            }
        }
    }
    
    @objc func handleNotification_bellUnreadCount(_ notification: NSNotification) {
       self.call_getNotification_UnreadCount()
    }
    
    
    //MARK:- Custom Methods
    
    func localiseNoPostView(){
        self.lblExplore.text = "Explore".localized
        self.lblExploreMsg.text = "Share you special memories with your loved ones".localized
        self.lblShareYourFirstPost.text = "Share your first post".localized
    }
    
     @objc func reloadViewOnTab(notification : Notification){

          if tableView.numberOfSections > 0{
               if tableView.numberOfRows(inSection: 0)>0{
                    tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
               }
          }
     }
    
    func updateTimeStamp(){
        
        if !Connectivity.isConnectedToInternet() {
            DispatchQueue.main.async {
                
                for var i in (0..<self.posts_DB.count) {
                    if let cell = self.tableView.cellForRow(at: IndexPath(row: i , section: 0)) as? HomeTableViewCell {
                        if let created = self.posts_DB[i].createdDate {
                            if let strTime = ((created as! Date).getElapsedInterval_FEEDS()) as? String {
                                cell.createdLab.text = strTime
                            }
                        }
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                
                for  i in (0..<self.posts.count) {
                    if let cell = self.tableView.cellForRow(at: IndexPath(row: i , section: 0)) as? HomeTableViewCell {
                        cell.createdLab.text =  self.posts[i].createdDate?.getElapsedInterval_FEEDS()
                    }
                }
            }
        }
    }
        
    
    fileprivate func updateByContentOffset() {
        let p = CGPoint(x: self.tableView.frame.width/2, y: self.tableView.contentOffset.y + self.tableView.frame.width/2)

        if let path = self.tableView.indexPathForRow(at: p),
            self.presentedViewController == nil {
            self.updateCell(at: path)
        }
    }
    
    fileprivate func updateCell(at indexPath: IndexPath) {
        if let cell = self.tableView.cellForRow(at: indexPath) as? HomeTableViewCell {
            
            var type:String = ""
            var url:String = ""
            
            if Connectivity.isConnectedToInternet() {
                type = (cell.post?.type)!
                url = (cell.post?.photoURL)!
            } else {
                type = (self.posts_DB[indexPath.row].type)!
                url = (self.posts_DB[indexPath.row].file)!
            }

            if type == "video"{
                
                if !MMLandscapeWindow.shared.isKeyWindow {
                    mmPlayerLayer.playView = cell.postImageView
                }
                mmPlayerLayer.isHidden = false
                mmPlayerLayer.player?.isMuted = self.isMuted
                
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                    
                }
                catch {
                    SessionManager.printLOG(data: "Error in play sound ")
                }
                
                mmPlayerLayer.player?.play()
                
                
                if let photoURL = url as? String {
                    
                    mmPlayerLayer.set(url: URL(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL)"), state: { (status) in
                    })
                    mmPlayerLayer.startLoading()
                    
                }
            }
            else{
                mmPlayerLayer.isHidden = true
                mmPlayerLayer.player?.pause()
                if let photoURL = url as? String {
                    
                    cell.postImageView.image = nil
                    
                    if let image = SDImageCache.shared().imageFromDiskCache(forKey: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL)") {
                        cell.postImageView.image = image
                    } else {
                       cell.postImageView.sd_setImage(with: URL(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL)"), placeholderImage: nil)
                    }
                }
            }
        }
    }
 
    @objc func refreshChannels() {
        
        if !Connectivity.isConnectedToInternet() {
            FTIndicator.showToastMessage("No Internet Connection!".localized)
            self.refreshControl.endRefreshing()
        }
        else {
            refreshControl.beginRefreshing()
            let offsetPoint = CGPoint.init(x: 0, y: -refreshControl.frame.size.height)
            tableView.setContentOffset(offsetPoint, animated: true)
            
            i  = 1
          //  self.updateByContentOffset()
            self.posts = []
            loadPosts(pageno: i , hideFinishingPopup: false , showShimmerEffect: false)
        }
    }
    
    func loadPaging(){
        let pagingViewController = PagingViewController<IconItem>()
        pagingViewController.menuItemClass = IconPagingCell.self
        pagingViewController.menuItemSize = .fixed(width: 110, height: 60)
        pagingViewController.textColor = UIColor(red: 0.51, green: 0.54, blue: 0.56, alpha: 1)
        pagingViewController.selectedTextColor = UIColor(red: 0.14, green: 0.77, blue: 0.85, alpha: 1)
        pagingViewController.indicatorColor = UIColor(red: 24.0/255.0, green: 78.0/255.0, blue: 252.0/255.0, alpha: 1)
        pagingViewController.dataSource = self
        pagingViewController.select(pagingItem: IconItem(icon: icons[0], index: 0))
       // self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]

    }
    
    func removePost(index: Int){
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_id":posts[index].user_id, "type":"posts","id": posts[index].id]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_REMOVEDATA, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    self.posts.remove(at: index)
                    constantVC.GlobalVariables.allpost_id.removeAll()
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    func configure_topNotificationBellButton(){
        notificationButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        notificationButton.setImage(UIImage(named: "notification_bell")?.withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        notificationButton.addTarget(self, action: #selector(btn_notificationBell_action), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: notificationButton)
    }
    
    @objc func btn_notificationBell_action(sender: UIButton!) {
        self.pushToNavigationVc()
        
    }
    
    func pushToNavigationVc() {
        constantVC.GlobalVariables.isfeedNotiTapped = true
        let notification_vc = self.storyboard!.instantiateViewController(withIdentifier: "Notification_VC") as! Notification_VC
        self.navigationController!.pushViewController(notification_vc, animated: true)
        
    }
    
    func moveToPostDetail(postId: String) {
          let next = self.storyboard?.instantiateViewController(withIdentifier: "PostDetail_VC") as! PostDetail_VC
          next.post_id = postId
//          constantVC.ActiveDataSource.isNavigatePostDetails = true
          self.present(next, animated: true, completion: nil)
          
    }
    
    //MARK:- CollectionView Delegate and data-source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionView {
            return tags.count
        }
        return self.polling.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCell", for: indexPath) as! TagCell
            self.configureCell(cell, forIndexPath: indexPath)
            return cell
        }
        
        let pollingObj = self.polling[indexPath.row]
        
        if pollingObj.user_response! {
            
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PollingAnswered_collectionCell", for: indexPath) as! PollingAnswered_collectionCell
                cell.polling = pollingObj
                return cell
         
        }
        
 
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PollingNotAnswered_collectionCell", for: indexPath) as! PollingNotAnswered_collectionCell
        cell.btnYes.tag = indexPath.row
        cell.btnNo.tag = indexPath.row
        cell.btnYes.addTarget(self, action: #selector(didTapPollingAns_Yes(_:)), for: UIControlEvents.touchUpInside)
        cell.btnNo.addTarget(self, action: #selector(didTapPollingAns_No(_:)), for: UIControlEvents.touchUpInside)
        cell.polling = pollingObj
     
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collectionView {
            self.configureCell(self.sizingCell!, forIndexPath: indexPath)
            return self.sizingCell!.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        }
        
        return CGSize(width: self.view.frame.size.width , height: 220.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.collectionView {
            collectionView.deselectItem(at: indexPath, animated: false)
            tags[indexPath.row].selected = !tags[indexPath.row].selected
            self.collectionView.reloadData()
        }
    }
    
    func configureCell(_ cell: TagCell, forIndexPath indexPath: IndexPath) {
        let tag = tags[indexPath.row]
        cell.tagName.text = tag.name
        cell.backgroundColor = tag.selected ? UIColor(red: 255.0/255.0, green: 211.0/255.0, blue: 108.0/255.0, alpha: 1) : UIColor(red: 227.0/255, green: 227.0/255.0, blue: 227.0/255.0, alpha: 1)
    }
    
    //MARK:- Block a user
    func blockAUser(userQuickBloxID:String , userServerID: String , userPhoneNo: String){
        
        var userID:String = ""
        if let id = SessionManager.get_QuickBloxID() {
            userID = "\(id)"
        }
        isFinishingUpNotifier_active = true
        homeFetchingNotifier.show()
        
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_users_id":SessionManager.getUserId() , "other_users_id":userServerID, "loginQuickBloxID": userID , "otherQuickBloxID": userQuickBloxID ,"status": "1"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_BLOCKUSER, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    //block to firebase
                    let document = SessionManager.getPhone() + "-" + userPhoneNo
                    TamFirebaseManager.shared.saveDocument(collection: firebaseCollection.Block.rawValue , document: document, completionHandler: { (success) in
                    })

                    QuickBloxManager().blockUserQuickBlox(userQuickBloxID: userQuickBloxID)
                    QuickBloxManager().loadBlockUsersList()
                    
                    self.posts = []
                    self.loadPosts(pageno: 1 , hideFinishingPopup: true , showShimmerEffect: true)
                    self.vw_blur.isHidden = true
                }
                else{
                    self.vw_blur.isHidden = true
                    self.homeFetchingNotifier.hide()
                }
            }
            else{
                self.vw_blur.isHidden = true
                self.homeFetchingNotifier.hide()
            }
        })
    }
    
    //MARK:- Report UIButton Actions
    @IBAction func cancelReportAc(_ sender: Any) {
        self.closeReportView()
    }
    
    @IBAction func btn_report_action(_ sender: UIButton) {
        var str_type:String = ""
        for var i in (0..<self.tags.count) {
            if self.tags[i].selected {
                
                if str_type == "" {
                    str_type = self.tags[i].name!
                }
                else {
                    str_type = str_type + "," + self.tags[i].name!
                }
            }
        }
        self.callTo_reportPost(postId: self.posts[sender.tag].id! , type: str_type)
    }
    
    @IBAction func addPost(_ sender: Any) {
        if !Connectivity.isConnectedToInternet() {
            FTIndicator.showToastMessage("No Internet Connection!".localized)
        }
        else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "croperView") as! FAImageCropperVC
            self.navigationController!.pushViewController(controller, animated: true)
        }
    }
    
    func openReportView(){
        UIView.animate(withDuration: 0.25, animations: {
            self.vw_blur.isHidden = false
            self.reportView.isHidden = false
            self.collectionView.reloadData()
        })
    }
    
    func closeReportView(){
        UIView.animate(withDuration: 0.25, animations: {
            self.vw_blur.isHidden = true
            self.reportView.isHidden = true
        })
    }
    
    //MARK:- User Method
    @objc func handlemoreTap(_ sender: UIButton!) {
        
        if Connectivity.isConnectedToInternet() {
            if let post = posts[sender.tag] as? Post {
                if let userID = post.user_id {
                    if userID == SessionManager.getUserId() {
                        //show my more options
                        self.showMyMoreOptions(tag: sender.tag)
                    } else {
                        //show other's more options
                        self.showOthersMoreOptions(tag: sender.tag)
                    }
                }
            }
        } else {
            FTIndicator.showToastMessage("No Internet Connection!")
        }
        
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        if let post = posts[actionSheet.tag] as? Post {
            if let userID = post.user_id {
                if userID == SessionManager.getUserId() {
                    if buttonIndex == 0 {
                        //delete post
                       self.removePost(index: actionSheet.tag)
                    }
                } else {
                    if buttonIndex == 0 {
                        //report post
                        self.btn_report.tag = actionSheet.tag
                        self.openReportView()
                    }
                    
                    if buttonIndex == 2 {
                        //block
                        self.blockAUser(userQuickBloxID: post.quickBloxID! , userServerID: post.user_id! , userPhoneNo: post.phoneNumber!)
                    }
                }
            }
        }
    }
    
    
    
    func showMyMoreOptions(tag:Int){
        
        let actionSheet = UIActionSheet(title: nil , delegate: self , cancelButtonTitle: "Cancel".localized , destructiveButtonTitle: "Delete".localized)
        actionSheet.tag = tag
        actionSheet.show(in: self.view)
    }
    
    
    func showOthersMoreOptions(tag:Int){
        
        let actionSheet = UIActionSheet(title: nil , delegate: self, cancelButtonTitle: "Cancel".localized, destructiveButtonTitle: "Report".localized , otherButtonTitles: "Block".localized)
        actionSheet.tag = tag
        actionSheet.show(in: self.view)
    }
    
    func setTabBar_BadgeCount(badgeCount: Int){
        if SessionManager.get_unreadNotificationCount() > 0  {
            tabBarController?.tabBar.items?[1].badgeColor = UIColor(red: 62.0/255, green: 208.0/255, blue: 11.0/255, alpha: 1.0)
            tabBarController?.tabBar.items?[1].badgeValue = "\(SessionManager.get_unreadNotificationCount())"
        }
        else {
            tabBarController?.tabBar.items?[1].badgeColor = UIColor.clear
        }
    }
   
    //MARK:- Web service implementations
    func call_getNotification_UnreadCount() {
        
        let dictParam : NSDictionary = ["identity":"test"+SessionManager.getPhone()]
        
        WebserviceSigleton().POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_NOTIFICATION_UNREAD_COUNT, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    if let data = response["data"] as? [[String:Any]] {
                        var unreadCount:Int = 0
                        for item in data {
                            if let count = item["unread"]{
                                unreadCount = Int(String(describing: count))!
                                if unreadCount > 0 {
                                    SessionManager.save_unreadNotificationCount(count: unreadCount)
                                    self.setTabBar_BadgeCount(badgeCount: unreadCount)
                                    self.notificationButton.addBadgeToButon(badge: "\(unreadCount)")
                                }
                                else {
                                    SessionManager.save_unreadNotificationCount(count: 0)
                                    self.setTabBar_BadgeCount(badgeCount: 0)
                                    self.notificationButton.badge = nil
                                }
                            }
                        }
                    }
                }
                else{
                }
            }
            else{
            }
        })
    }
    
    func callTo_reportPost(postId:String , type: String) {
        self.view.endEditing(true)
        let webservice  = WebserviceSigleton ()
        
        let dictParam : NSDictionary = ["post_id":postId, "user_id": SessionManager.getUserId(), "type": type]
        self.showLoader(strForMessage: "loading..".localized)
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_SUBMIT_NEWS_REPORT, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    self.dismissLoader()
                    self.closeReportView()
                    FTIndicator.showToastMessage("Report submitted successfully!")
                }
                else{
                    self.dismissLoader()
                }
            }
            else{
                self.dismissLoader()
            }
        })
    }
    
    func loadPosts(pageno: Int, hideFinishingPopup : Bool , showShimmerEffect: Bool) {
        
        self.pollingCount = 0
        self.polling.removeAll()
        self.vwNoPost.isHidden = true
        
        if pageno == 1 && showShimmerEffect {
            
        if let customView = Bundle.main.loadNibNamed("Shimmer_View", owner: self, options: nil)?.first as? Shimmer_View {
            self.shimmeringView.frame = view.bounds
            view.addSubview(shimmeringView as? UIView ?? UIView())
            shimmeringView.contentView = customView
            self.shimmeringView.isHidden = false
            shimmeringView.isShimmering = true
        }}
        
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["user_id":SessionManager.getUserId(), "PageNo":pageno,"limit": "10" , "countrycode": SessionManager.get_countryDialCode() , "language": SessionManager.get_localizeLanguage_completeName()]
        
        if Connectivity.isConnectedToInternet() {
            
            webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_POST, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
                if result != nil{
                    let response = result! as Dictionary
                    
                    let resultMessage = response["msg"] as! String
                    if resultMessage == "success"{
                        
                        //polling
                        if let pages = response["polling_count"] as? Int{
                            self.pollingCount = Int(pages)
                        }
                        
                        if let pollingDetails = response["polling_post"] as? [[String:Any]] {
                            for item in pollingDetails {
                                var created_at = Date()
                                var givenAns = [GivenAns]()
                                var polling_id: String = ""
                                var question : String = ""
                                var type: String = ""
                                var user_response: Bool = false
                                var question_options = [QuestionOptions]()
                                
                                //data parsing
                                if let created_ = item["created_at"] as? String{
                                    
                                    let localDate = constantVC.UTCToLocal(date: created_)
                                    
                                    if localDate != "" {
                                        let datefrmter = DateFormatter()
                                        datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                        let date = datefrmter.date(from: localDate)
                                        created_at = date!
                                    }
                                }
                                
                                if let givenAnsDetails = item["givenAns"] as? [[String:Any]] {
                                    
                                    for ans in givenAnsDetails {
                                        
                                        var answer: String = ""
                                        var value : Int = 0
                                        
                                        if let answer_ = ans["answer"] as? String{
                                            answer = answer_
                                        }
                                        if let value_ = ans["value"] as? String{
                                            value = Int(value_)!
                                        }
                                        givenAns.append(GivenAns(answer: answer, value: value))
                                    }
                                }
                                
                                if let id = item["polling_id"] as? String{
                                    polling_id = id
                                }
                                
                                if let question_ = item["question"] as? String{
                                    question = question_
                                }
                                
                                if let question_ = item["question"] as? String{
                                    question = question_
                                }
                                
                                if let type_ = item["type"] as? String{
                                    type = type_
                                }
                                if let user_response_ = item["user_response"] as? String{
                                    if user_response_ == "false" {
                                        user_response = false
                                    } else {
                                        user_response = true
                                    }
                                }
//                                if let questionOptionDetails = item["question_options"] as? [[String:Any]] {
//                                    for ques in questionOptionDetails {
//
//                                        var options: String = ""
//
//                                        if let option_ = ques["options"] as? String{
//                                            options = option_
//                                        }
//                                        question_options.append(QuestionOptions(options: options))
//                                    }
//                                }
                                
                                
                                //options
                                if let option1 = item["option1"] as? String{
                                    question_options.append(QuestionOptions(options: option1))
                                }
                                
                                if let option2 = item["option2"] as? String{
                                    question_options.append(QuestionOptions(options: option2))
                                }
                                
//                                if let option3 = item["option3"] as? String{
//                                    question_options.append(QuestionOptions(options: option3))
//                                }
                               
                                //if type == "text" {
                                    let objPolling = Polling(createdDate: created_at , givenAns: givenAns , polling_id: polling_id , question: question , type: type , user_response: user_response , question_options: question_options)
                                    
                                    self.polling.append(objPolling)
                               // }
                            }
                        }
                        //POST
                        if let pages = response["post_count"] as? String{
                            self.totalPages = Int(pages)!/10 + 1
                        }
                        if let postDetails = response["post_details"] as? [[String:Any]] {
                            if self.i == 1 {
                                self.posts = []
                                
                                //add one item at 0 index for polling
                                if self.pollingCount > 0 {
                                    let pollingPost = Post(caption: "", photoURL: "", uid: "", user_id: "", id: "", name: "" , likeCount: 0 , commentCount: 0, isLiked: false , type: "", created: "", thumbnail: "" , height: 0.0 , width: 0.0 , captionHeight: 0.0 , superViewWidth: 0.0 , phoneNumber: "" , createdDate: Date() , quickBloxID: "" , polling: self.polling)
                                    self.posts.append(pollingPost)
                                }
                                
                                CoreData_BussinessLogic.deleteAllFeeds()
                            }
                            for item in postDetails {
                                print(item)
                                var caption: String = ""
                                var photoURL: String = ""
                                var uid: String = ""
                                var user_id_temp : String = ""
                                var id: String = ""
                                var name: String = ""
                                var likeCount: Int = 0
                                var commentCount: Int = 0
                                var isLiked: Bool = false
                                var type_temp : String = ""
                                var created : String = ""
                                var thumbnail : String = ""
                                var height : CGFloat = 300.0
                                var width : CGFloat = 300.0
                                var captionHeight:CGFloat = 0.0
                                var superViewWidth:CGFloat = 0.0
                                var phoneNumber:String = ""
                                var createdDate = Date()
                                var quickBloxID:String = "0"
                                
                                if let _id = item["id"] as? String{
                                    id = _id
                                }
                                if let no = item["phoneNumber"] as? String{
                                    phoneNumber = no
                                }
                                if let user_id = item["user_id"] as? String{
                                    user_id_temp = user_id
                                }
                                if let type = item["type"] as? String{
                                   type_temp = type
                                }
                                if let _userid = item["phoneNumber"] as? String{
                                    uid = _userid
                                }
                                if let height_ = item["height"] as? String{
                                    
                                    if let double = Double(height_), height_ != "0" {
                                        height = CGFloat(double)
                                    }
                                    else{
                                        height = 300.0
                                    }
                                }
                                if let width_ = item["width"] as? String{
                                    
                                    if let double = Double(width_), width_ != "0" {
                                       width = CGFloat(double)
                                    }
                                    else{
                                       width = 300.0
                                    }
                                }
                                if let _userid = item["phoneNumber"] as? String{
                                    uid = _userid
                                }
                                if let file = item["file"] as? String{
                                    photoURL = file
                                }
                                if let thumb = item["thumbnail"] as? String{
                                    thumbnail = thumb
                                }
                                if let capt = item["caption"] {
                                    caption = String(describing: capt)
                                }
                                if let name_ = item["name"] as? String{
                                    name = name_
                                }
                                if let created_ = item["created"] as? String{
                                    
                                    let localDate = constantVC.UTCToLocal(date: created_)
                                    
                                    if localDate != "" {
                                        let datefrmter = DateFormatter()
                                        datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                        let date = datefrmter.date(from: localDate)
                                        createdDate = date!
                                        created = (date?.getElapsedInterval_FEEDS())!
                                    }
                                }
                                
                                if let likeCount_ = item["likesCount"] as? String{
                                    likeCount = Int(likeCount_)!
                                }
                                if let commentCount_ = item["commentsCount"] as? String{
                                    commentCount = Int(commentCount_)!
                                }
                                if let isLiked_ = item["isLiked"] as? String{
                                    if isLiked_ == "FALSE"{
                                        isLiked = false
                                    }
                                    else{
                                        isLiked = true
                                    }
                                }
                                
                                
                                if let quickbloxID = item["quickbloxID"] as? String{
                                    quickBloxID = quickbloxID
                                }
                                
                                //calculate caption height
                                let strCaption:String = String(describing: caption)
                                var cell_captionHeight:CGFloat = 0.0
                              
                                if (strCaption.count > 0 && strCaption != "") {
                                    cell_captionHeight = (strCaption.height(withConstrainedWidth: self.tableView.frame.size.width - 16 , font: UIFont (name: "CircularStd-Book", size: 16)!))
                                }
                                
                                captionHeight = cell_captionHeight
                                superViewWidth = self.view.frame.size.width
                                
                                
                                let new_post = Post(caption: caption , photoURL: photoURL , uid: uid, user_id: user_id_temp, id: id, name: name, likeCount: likeCount, commentCount: commentCount, isLiked: isLiked , type: type_temp , created: created, thumbnail: thumbnail , height: height , width: width , captionHeight: captionHeight , superViewWidth: superViewWidth , phoneNumber: phoneNumber , createdDate: createdDate , quickBloxID: quickBloxID, polling: [])
                                
                                self.posts.append(new_post)
                            }
                            if hideFinishingPopup == true{
                                self.vw_blur.isHidden = true
                                self.homeFetchingNotifier.hide()
                            }
                            
                            self.refreshControl.endRefreshing()
                            if self.shimmeringView != nil && showShimmerEffect {
                                self.shimmeringView.isShimmering = false
                                self.shimmeringView.isHidden = true
                            }
                            
                           // SessionManager.set_posts(posts: self.posts)
                            constantVC.GlobalVariables.allpost_id.removeAll()
                            self.tableView.reloadData()
                        }
                    }
                    else{
                        if hideFinishingPopup == true{
                            self.vw_blur.isHidden = true
                            self.homeFetchingNotifier.hide()
                        }
                        
                        if self.shimmeringView != nil && showShimmerEffect {
                            self.shimmeringView.isShimmering = false
                            self.shimmeringView.isHidden = true
                           // self.shimmeringView.removeFromSuperview()
                        }
                        constantVC.GlobalVariables.allpost_id.removeAll()
                        self.tableView.reloadData()
                        self.refreshControl.endRefreshing()
                        self.showStartUpScreen()
                    }
                }
                else{
                    if hideFinishingPopup == true{
                        self.vw_blur.isHidden = true
                        self.homeFetchingNotifier.hide()
                    }
                    
                    if self.shimmeringView != nil && showShimmerEffect {
                        self.shimmeringView.isShimmering = false
                        self.shimmeringView.isHidden = true
                        //self.shimmeringView.removeFromSuperview()
                    }
                    
                    constantVC.GlobalVariables.allpost_id.removeAll()
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                    self.showStartUpScreen()
                }
            })
            
        }
        else {
            let alertController = UIAlertController(title: "Error!".localized , message: "Please check your internet connectivity".localized , preferredStyle: .alert)
            
            // Create the actions
            let addAction = UIAlertAction(title: "OK".localized , style: UIAlertActionStyle.default) {
                UIAlertAction in
                if self.shimmeringView != nil && showShimmerEffect {
                    self.shimmeringView.isShimmering = false
                    self.shimmeringView.isHidden = true
                    //self.shimmeringView.removeFromSuperview()
                }
            }
            // Add the actions
            alertController.addAction(addAction)
           
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }
 
    }
    
    
    func showStartUpScreen(){
        if self.i == 1 {
            self.vwNoPost.isHidden = false
        } else {
            self.vwNoPost.isHidden = true
        }
    }
  
    @objc open func handleshareTap(_ sender: UIButton!) {
        
        if Connectivity.isConnectedToInternet(){
            if posts.indices.contains(sender.tag) {
                let post = posts[sender.tag]
                if post.type == "image"{
                    guard let photoUrl = post.photoURL else {return}
                    
                    guard let catPictureURL = URL(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoUrl)") else {return}
//                    ImageLoader.image(for: catPictureURL) { (image) in
//                        guard let img = image else{return}
//
//                        let shareAll = [post.caption! , img] as [Any]
//                         let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
//                         activityViewController.popoverPresentationController?.sourceView = self.view
//                         self.present(activityViewController, animated: true, completion: nil)
//                    }
                    
                    let imageView = UIImageView()
                    imageView.loadImageWithUrl(urlString: "\(constantVC.GeneralConstants.ImageUrl)\(photoUrl)") { (isDone) in
                        print(imageView.image )
                        guard let img = imageView.image else {return}
                        let shareAll = [post.caption! , img] as [Any]
                        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                        activityViewController.popoverPresentationController?.sourceView = self.view
                        self.present(activityViewController, animated: true, completion: nil)
                    }
//                    let session = URLSession(configuration: .default)
//
//                    let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
//                        // The download has finished.
//                        if let _ = error {
//                        } else {
//                            // No errors found.
//                            // It would be weird if we didn't have a response, so check for that too.
//                            if let _ = response as? HTTPURLResponse {
//                                if let imageData = data {
//                                    // Finally convert that Data into an image and do what you wish with it.
//                                    let image = UIImage(data: imageData)
//                                    // Do something with your image.
//
//
//                                    let shareAll = [post.caption! , image!] as [Any]
//                                    let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
//                                    activityViewController.popoverPresentationController?.sourceView = self.view
//                                    self.present(activityViewController, animated: true, completion: nil)
//
//                                }
//                            }
//                        }
//                    }
//
//                    downloadPicTask.resume()
                    
                }
                else{
                    let videoURL = URL(string: "\(constantVC.GeneralConstants.ImageUrl)\(post.photoURL!)")
                    let activityItems = [videoURL, post.caption ] as [Any]
                    let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
                    
                    activityController.popoverPresentationController?.sourceView = self.view
                    activityController.popoverPresentationController?.sourceRect = self.view.frame
                    
                    self.present(activityController, animated: true, completion: nil)
                }
            }
        } else {
            FTIndicator.showToastMessage("No Internet Connection!")
        }
        
    }
    
    // MARK : - POSTING ON HOME
    var compressedData:Data?
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    
    func compressVideoData(){
        
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
        compressVideo(inputURL: constantVC.GlobalVariables.videoUrl!, outputURL: compressedURL) { (exportSession) in
            guard let session = exportSession else {
                return
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = NSData(contentsOf: compressedURL) else {
                    return
                }
                
                self.compressedData = compressedData as Data
                self.postToTamaas()
                
            case .failed:
                break
            case .cancelled:
                break
            }
        }
    }
    
    
    func postToTamaas(){
        
        var height = "300"
        var width = "300"
        
        //for emojis
        let data = constantVC.GlobalVariables.captionStr?.data(using: String.Encoding.nonLossyASCII)!
        let strCaption = String(data: data!, encoding: String.Encoding.utf8)!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                if constantVC.GlobalVariables.shCamera == true{
                    
                    
                    
                    if let photo = constantVC.GlobalVariables.selectedImage, let data = UIImageJPEGRepresentation(photo, 1) {
                       var imageData = Data()
                        if UIImageJPEGRepresentation(photo, 1)!.count  < 5000000{
                            imageData = UIImageJPEGRepresentation(photo, 1)!
                        }
                        else if UIImageJPEGRepresentation(photo, 1)!.count  < 8000000 && UIImageJPEGRepresentation(photo, 1)!.count > 5000000{
                            imageData = UIImageJPEGRepresentation(photo, 0.2)!
                        }
                        else{
                            imageData = UIImageJPEGRepresentation(photo, 0.5)!
                        }
                        
                        multipartFormData.append(imageData, withName: "file", fileName: "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).jpeg", mimeType: "image/jpeg")
                        width = String(describing: photo.size.width)
                        height = String(describing: photo.size.height)
                        
                    }
                }
                else{
                    
                    multipartFormData.append(self.compressedData as! Data, withName: "file",fileName:"\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).mp4", mimeType: "video/mp4")
                  
                    //if let data = NSData(contentsOf: constantVC.GlobalVariables.videoUrl!){
                    
                  //  }
                    
                }
                
                if let photo = constantVC.GlobalVariables.selectedImage, let data = UIImageJPEGRepresentation(photo, 1) {
                    
                    
                    var imageData = Data()
                    if UIImageJPEGRepresentation(photo, 1)!.count  < 5000000{
                        imageData = UIImageJPEGRepresentation(photo, 1)!
                    }
                    else if UIImageJPEGRepresentation(photo, 1)!.count  < 8000000 && UIImageJPEGRepresentation(photo, 1)!.count > 5000000{
                        imageData = UIImageJPEGRepresentation(photo, 0.2)!
                    }
                    else{
                        imageData = UIImageJPEGRepresentation(photo, 0.5)!
                    }
                    
                    
                    multipartFormData.append(imageData, withName: "thumbnail", fileName: "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).jpeg", mimeType: "image/jpeg")
                    
                }
                multipartFormData.append(SessionManager.getUserId().data(using: String.Encoding.utf8)!, withName: "user_id")
                
                multipartFormData.append((constantVC.GlobalVariables.typeStr?.data(using: String.Encoding.utf8)!)!, withName: "type")
                
                let string2 = strCaption.replacingOccurrences(of: "\\", with: "-")
                
                multipartFormData.append(string2.data(using: String.Encoding.utf8)! , withName: "caption")
                
                multipartFormData.append((width.data(using: String.Encoding.utf8)!), withName: "width")
                
                multipartFormData.append((height.data(using: String.Encoding.utf8)!), withName: "height")
                
        },
            to:"\(constantVC.GeneralConstants.BASE_URL)\(constantVC.WebserviceName.URL_ADD_POST)",
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        guard response.result.error == nil else {
                            self.vw_blur.isHidden = true
                            self.homeFetchingNotifier.hide()
                            return
                        }
                        
                        if let value = response.result.value {
                            
                            if let result = value as? Dictionary<String, AnyObject> {
                                
                                constantVC.GlobalVariables.isNeedToGetStories = true
                                let resultMessage = result["msg"] as! String
                                
                                if resultMessage == "success"{
                                    self.i = 1
                                    self.loadPosts(pageno: 1 , hideFinishingPopup: true , showShimmerEffect: false)
                                }
                                else{
                                    self.vw_blur.isHidden = true
                                    self.homeFetchingNotifier.hide()
                                    if let error = result["msg_details"] as? String {
                                        self.showError(message: error)
                                    }
                                }
                            }
                        }
                        }
                        .uploadProgress { progress in // main queue by default
                    }
                    return
                case .failure(let encodingError):
                    self.vw_blur.isHidden = true
                    self.homeFetchingNotifier.hide()
                    debugPrint(encodingError)
                }
        })
    }
    
    
    //MARK:- Polling
    @objc func didTapPollingAns_Yes(_ sender: UIButton!) {
        
        if self.polling[sender.tag].type == "text" {
             self.callToAddPollingAns(tag: sender.tag , ansText: "Yes")
        } else {
            var ans:String = ""
            if let ans_ = self.polling[sender.tag].question_options[0].options as? String {
                ans = ans_
            }
            self.callToAddPollingAns(tag: sender.tag , ansText: ans)
        }
    }
    
    @objc func didTapPollingAns_No(_ sender: UIButton!) {
        if self.polling[sender.tag].type == "text" {
            self.callToAddPollingAns(tag: sender.tag , ansText: "No")
        } else {
            var ans:String = ""
            if let ans_ = self.polling[sender.tag].question_options[1].options as? String {
                ans = ans_
            }
            self.callToAddPollingAns(tag: sender.tag , ansText: ans)
        }
    }
    
    //multiple
    @objc func didTapPollingAns_option1(_ sender: UIButton!) {
        var ans:String = ""
        if let ans_ = self.polling[sender.tag].question_options[0].options as? String {
            ans = ans_
        }
        self.callToAddPollingAns(tag: sender.tag , ansText: ans)
    }
    @objc func didTapPollingAns_option2(_ sender: UIButton!) {
        var ans:String = ""
        if let ans_ = self.polling[sender.tag].question_options[1].options as? String {
            ans = ans_
        }
        self.callToAddPollingAns(tag: sender.tag , ansText: ans)
    }
    @objc func didTapPollingAns_option3(_ sender: UIButton!) {
        var ans:String = ""
        if let ans_ = self.polling[sender.tag].question_options[2].options as? String {
            ans = ans_
        }
        self.callToAddPollingAns(tag: sender.tag , ansText: ans)
    }
    
    func callToAddPollingAns(tag: Int , ansText: String){
        
        var questionID:String = ""
        if let id = self.polling[tag].polling_id as? String {
            questionID = id
        }
       
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["user_id":SessionManager.getUserId() , "question_id":questionID , "answer": ansText]
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_SUBMIT_POLLING_ANS, params: (dictParam as? Dictionary<String, AnyObject>)!, callback:{ (result,error) -> Void in
            
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    self.stopAnimating()
                    
                    //refresh feeds
                    self.i = 1
                    self.loadPosts(pageno: 1 , hideFinishingPopup: false , showShimmerEffect: false)
                }
            }
            
        })
    }
}

// MARK: - TableView Delegate and Data Source Methods
extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Connectivity.isConnectedToInternet() {
            return posts.count
        }
        return self.posts_DB.count
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if pollingCount > 0 && indexPath.row == 0 {
            let pollingCell = tableView.dequeueReusableCell(withIdentifier: "pollingCell", for: indexPath) as! Polling_tableViewCell
            
            //register collection cell
            let cellNibAns = UINib(nibName: "PollingAnswered_collectionCell", bundle: nil)
            pollingCell.collectionVw_polling.register(cellNibAns, forCellWithReuseIdentifier: "PollingAnswered_collectionCell")
            
            let cellNibNotAns = UINib(nibName: "PollingNotAnswered_collectionCell", bundle: nil)
            pollingCell.collectionVw_polling.register(cellNibNotAns, forCellWithReuseIdentifier: "PollingNotAnswered_collectionCell")
            
            let cellNibNotAnsMulti = UINib(nibName: "PollingMultipleNotAns_collectionCell", bundle: nil)
            pollingCell.collectionVw_polling.register(cellNibNotAnsMulti, forCellWithReuseIdentifier: "PollingMultipleNotAns_collectionCell")
            
            let cellNibAnsMulti = UINib(nibName: "PollingMultipleAns_collectionCell", bundle: nil)
            pollingCell.collectionVw_polling.register(cellNibAnsMulti, forCellWithReuseIdentifier: "PollingMultipleAns_collectionCell")
            
            pollingCell.collectionVw_polling.delegate = self
            pollingCell.collectionVw_polling.dataSource = self
            pollingCell.collectionVw_polling.reloadData()
            
            return pollingCell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeTableViewCell
        cell.postImageView.image = nil
        
        //add button action
        cell.btnTapImage.tag = indexPath.row
        cell.btnTapImage.addTarget(self, action: #selector(self.didTapPostImageView(_:)), for: .touchUpInside)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapPostImageView(_:)))
        cell.postImageView.isUserInteractionEnabled = true
        cell.postImageView.tag = indexPath.row
        cell.postImageView.addGestureRecognizer(tapGestureRecognizer)
        
        //set border
        cell.profileImageView.layer.borderWidth = 1.0
        cell.profileImageView.layer.borderColor = UIColor.lightGray.cgColor
        
        if Connectivity.isConnectedToInternet() && self.posts.count > 0 {
            constantVC.GlobalVariables.allpost_id.append(posts[indexPath.row].id!)
            cell.post = posts[indexPath.row]
            
            if indexPath.row == posts.count - 2 {
                if i <= totalPages{
                    i = i+1
                    self.loadPosts(pageno: i , hideFinishingPopup: false , showShimmerEffect: false)
                }
            }
            
            //save seen feed to DB
            CoreData_BussinessLogic.saveOrUpdateFeed(feed: posts[indexPath.row])
        }
        else {
            if self.posts_DB.indices.contains(indexPath.row) {
                cell.postDB = self.posts_DB[indexPath.row]
            }
        }
        
        cell.homeVC = self
        //button actions
        cell.shareButton.addTarget(self, action: #selector(handleshareTap(_:)), for: UIControlEvents.touchUpInside)
        cell.moreBtn.tag = indexPath.row
        cell.moreBtn.addTarget(self, action: #selector(handlemoreTap(_:)), for: UIControlEvents.touchUpInside)
        cell.shareButton.tag = indexPath.row
        cell.btnMute.isHidden = true
        
        var type:String = ""
        if Connectivity.isConnectedToInternet() {
            if posts.indices.contains(indexPath.row) {
                type = (posts[indexPath.row].type)!
            }
        } else {
            if let type_ = posts_DB[indexPath.row].type{
                 type = type_
            }
        }
        if type == "video"{
            
             cell.btnMute.isHidden = false
            
            //show mute unmute
            if self.isMuted {
                cell.btnMute.isSelected = false
            } else {
                cell.btnMute.isSelected = true
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.updateByContentOffset()
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if pollingCount > 0 && indexPath.row == 0 {
            return 220.0
        }
        
        var cellHeight : CGFloat = 410
        
        if Connectivity.isConnectedToInternet() && self.posts.count > 0 {
            let imageHeight = posts[indexPath.row].height*self.view.frame.size.width/posts[indexPath.row].width
            cellHeight = imageHeight + 125 + posts[indexPath.row].captionHeight
        } else if indexPath.row < self.posts_DB.count {
            let imageHeight = CGFloat((posts_DB[indexPath.row].height! as NSString).floatValue)*self.view.frame.size.width/CGFloat((posts_DB[indexPath.row].width! as NSString).floatValue)
            cellHeight = imageHeight + 125 + CGFloat((posts_DB[indexPath.row].captionHeight! as NSString).floatValue)
        }
        
        if cellHeight.isNaN {
            cellHeight = 410
            return 410
        }
        
        return cellHeight
    }
    
    @IBAction func didTapPostImageView(_ sender:UIButton!) {
        let indexPath_ = IndexPath(row: sender.tag , section: 0)
        
        if let cell = self.tableView.cellForRow(at: indexPath_) as? HomeTableViewCell {
            var type:String = ""
            if Connectivity.isConnectedToInternet() {
                type = (cell.post?.type)!
            } else {
                type = (self.posts_DB[indexPath_.row].type)!
            }
            if type == "video"{
                
                if self.isMuted {
                    self.isMuted = false
                    mmPlayerLayer.player?.isMuted = self.isMuted
                } else {
                    self.isMuted = true
                    mmPlayerLayer.player?.isMuted = self.isMuted
                }
                
                //show mute unmute
                if self.isMuted {
                    cell.btnMute.isSelected = false
                } else {
                    cell.btnMute.isSelected = true
                }
            }
        }
    }
}

extension String{
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        return ceil(boundingBox.height)
    }
}

extension HomeViewController: PagingViewControllerDataSource {
 
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, viewControllerForIndex index: Int) -> UIViewController {
        let viewController = UIViewController()
        return viewController
    }
    
    func pagingViewController<T>(_ pagingViewController: PagingViewController<T>, pagingItemForIndex index: Int) -> T {
        return IconItem(icon: icons[index], index: index) as! T
    }
    
    func numberOfViewControllers<T>(in: PagingViewController<T>) -> Int {
        return icons.count
    } 
}
