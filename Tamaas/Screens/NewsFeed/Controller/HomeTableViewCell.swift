//
//  HomeTableViewCell.swift
//  Blocstagram
//
//  Created by ddenis on 1/17/17.
//  Copyright © 2017 ddApps. All rights reserved.
//

import UIKit
import FTIndicator

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var commentImageView: UIImageView!
//    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var likesCountLab: UILabel!
    @IBOutlet weak var commentCountLab: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var createdLab: UILabel!
    @IBOutlet weak var moreBtn: UIButton!
    @IBOutlet weak var lbl_caption: UILabel!
    @IBOutlet weak var constraint_lblCaptionHeight: NSLayoutConstraint!
    @IBOutlet weak var constraint_imgVwpost_height: NSLayoutConstraint!
    @IBOutlet weak var imgVw_post_placeHolder: UIImageView!
    @IBOutlet weak var btnTapImage: UIButton!
    @IBOutlet weak var btnMute: UIButton!
    
    var homeVC: UIViewController?
    
    var post: Post? {
        didSet {
            updateView()
        }
    }
    
    var postDB: Feed_DB? {
        didSet {
            updateViewDB()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        postImageView.image = nil
        nameLabel.text = ""
        updateUserInfo()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateView() {
//        captionLabel.text = post?.caption
        
        if let photothumbnail = post?.thumbnail, photothumbnail != "" {
            
            postImageView.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photothumbnail)") , placeholder: nil , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }

       else if let photoURL = post?.photoURL {
            
            postImageView.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL)") , placeholder: nil , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
        
        
        if let no = post?.phoneNumber {
            if no == SessionManager.getPhone() {
                //my number
               nameLabel.text = SessionManager.getUsername()
            }
            else {
                nameLabel.text = SessionManager.getNameFromMyAddressBook(number: no)
            }
        }
        
        if let strTime = (post?.createdDate?.getElapsedInterval_FEEDS())! as? String {
           createdLab.text = strTime
        }
        
        if post?.isLiked == true{
             likeImageView.image = UIImage(named: "likeSelected")
        }
        else{
             likeImageView.image = UIImage(named: "like")
        }
        
        commentCountLab.text = "\(post?.commentCount ?? 0)"
        likesCountLab.text = "\(post?.likeCount ?? 0)"
        
        
        if let caption = post?.caption as? String{
            let string2 = caption.replacingOccurrences(of: "-", with: "\\")
            lbl_caption.text = self.setCommentTextWithEmojis(comment: string2)
        }
        else {
            lbl_caption.text = ""
        }
        
       /* if let caption = post?.caption as? String {
            lbl_caption.text = self.setCommentTextWithEmojis(comment: caption)
        }
        else {
            lbl_caption.text = "rajni"
        }*/
        
        if let photoURL = post?.uid {
           profileImageView.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL).jpeg") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
        
        self.constraint_lblCaptionHeight.constant = (post?.captionHeight)!
        let imageHeight = (post?.height)!*(post?.superViewWidth)!/(post?.width)!
        self.constraint_imgVwpost_height.constant =  imageHeight
        
    }
    
    func updateViewDB(){
        //OFFLINE
        
        constraint_lblCaptionHeight.constant = CGFloat(((postDB?.captionHeight)! as NSString).floatValue)
        let imageHeight = (CGFloat(((postDB?.height)! as NSString).floatValue))*(CGFloat(((postDB?.superViewWidth)! as NSString).floatValue))/(CGFloat(((postDB?.width)! as NSString).floatValue))
        constraint_imgVwpost_height.constant =  imageHeight
        
        if let photothumbnail = postDB?.thumbnail, photothumbnail != "" {
            postImageView.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photothumbnail)") , placeholder: nil , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
            
        else if let photoURL = postDB?.file {
           postImageView.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL)") , placeholder: nil , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
        
        if let name = postDB?.name{
          nameLabel.text = name
        }
        
        if let created = postDB?.createdDate {
            if let strTime = ((created as! Date).getElapsedInterval_FEEDS()) as? String {
                createdLab.text = strTime
            }
        }
        
        if postDB?.isLiked == true{
          likeImageView.image = UIImage(named: "likeSelected")
        }
        else{
          likeImageView.image = UIImage(named: "like")
        }
        
        commentCountLab.text = postDB?.commentsCount
        likesCountLab.text = postDB?.likesCount
        
        //user image
        if let photoURL = postDB?.phoneNumber {
            
            let imgURL = constantVC.GeneralConstants.ImageUrl + photoURL + ".jpeg"
            if let img_url = imgURL as? String {
                profileImageView.yy_setImage(with: URL.init(string: img_url) , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
                
            }
        }
        
        if let caption = postDB?.caption as? String{
            let string2 = caption.replacingOccurrences(of: "-", with: "\\")
            lbl_caption.text = self.setCommentTextWithEmojis(comment: string2)
        }
        else {
            lbl_caption.text = ""
        }
        
    }
        
    
    func setCommentTextWithEmojis(comment:String) -> String{
        let trimmedString = comment.trimmingCharacters(in:NSCharacterSet.whitespacesAndNewlines)
        let data2 = trimmedString.data(using: String.Encoding.utf8)!
        let messagestring = String(data: data2, encoding: String.Encoding.nonLossyASCII)
        return messagestring!
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImageView.image = UIImage(named: "profile")
    }
    
    // fetch the values from the user variable
    func updateUserInfo() {
        
        if let photoURL = post?.uid {
            
            profileImageView.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL).jpeg") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
            
//            profileImageView.loadImage(urlString: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL).jpeg", placeholderImage: UIImage(named:"profile")){
//                success, error in
//            }
            
        }
        
        if let photoURL = postDB?.phoneNumber {
            
            profileImageView.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL).jpeg") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
            
//            profileImageView.loadImage(urlString: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL).jpeg", placeholderImage: UIImage(named:"profile")){
//                success, error in
//            }
        }
    }
    
    // MARK: - Like Tap Handler
    @IBAction func likeBtnAc(_ sender: Any) {
        
        if Connectivity.isConnectedToInternet() {
            if likeImageView.image == UIImage(named: "like"){
                likeImageView.image = UIImage(named: "likeSelected")
                post?.isLiked = true
                post?.likeCount = (post?.likeCount)! + 1
                likeAPI(isLike: "1")
            }
            else{
                post?.isLiked = false
                post?.likeCount = (post?.likeCount)! - 1
                likeImageView.image = UIImage(named: "like")
                likeAPI(isLike: "0")
            }
            
            if let count = post?.likeCount {
                if count > 0 {
                    likesCountLab.text = "\(post?.likeCount ?? 0)"
                } else {
                    likesCountLab.text = "0"
                }
            }
            
            
        } else {
            FTIndicator.showToastMessage("No Internet Connection!")
        }
        
    }
    // MARK: - Comment ImageView Segue
    @IBAction func commentBtnAc(_ sender: Any) {
        
        if Connectivity.isConnectedToInternet() {
            if let id = post?.id {
                
                let controller = homeVC?.storyboard?.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
               // controller.modalPresentationStyle = .fullScreen
                controller.postID = id
                controller.userTo_id = post?.phoneNumber
                if let id = post?.quickBloxID {
                    controller.user_quickbloxID = id
                }
                
                if self.post?.type == "video" {
                    controller.postImg = (self.post?.thumbnail)!
                }else {
                    controller.postImg = (self.post?.photoURL)!
                }
                
                if (homeVC?.isKind(of: PostDetail_VC.self))! {
                    controller.isPushFrom_PostDetailVC = true
                }
               
                let aObjNavi = UINavigationController(rootViewController: controller)
                aObjNavi.modalPresentationStyle = .fullScreen
                homeVC?.present(aObjNavi, animated: true, completion: nil)
                
            }
        }  else {
            if let id = postDB?.id {
                
                let controller = homeVC?.storyboard?.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
                controller.modalPresentationStyle = .fullScreen
                controller.postID = id
                controller.userTo_id = postDB?.phoneNumber
                controller.user_quickbloxID = ""
                
                
                if self.post?.type == "video" {
                    controller.postImg = (self.postDB?.thumbnail)!
                }else {
                    controller.postImg = (self.postDB?.file)!
                }
                
                if (homeVC?.isKind(of: PostDetail_VC.self))! {
                    controller.isPushFrom_PostDetailVC = true
                }
                let aObjNavi = UINavigationController(rootViewController: controller)
                aObjNavi.modalPresentationStyle = .fullScreen
                homeVC?.present(aObjNavi, animated: true, completion: nil)
                
            }
        }
        
    }
    
    func likeAPI(isLike : String){
        
            let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["user_id":SessionManager.getUserId(), "post_id":post?.id,"isLike": isLike]
            webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_LIKE_POST, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
                if result != nil{
                    let response = result! as Dictionary
                    let resultMessage = response["msg"] as! String
                    if resultMessage == "success"{
                        
                        if isLike == "1" && (self.post?.phoneNumber)! != SessionManager.getPhone() {
                        //send notification
                        let id_to = "test" + (self.post?.phoneNumber)!
                        var image_url:String = ""
                            
                        if self.post?.type == "video" {
                            image_url = (self.post?.thumbnail)!
                        }else {
                            image_url = (self.post?.photoURL)!
                        }
                      
                        if let opponentID = self.post?.quickBloxID {
                            if opponentID != "" {
                                QuickBloxManager().sendPushNotification(msg: "Post Like" , users: opponentID , identity: id_to , title: "Post Like" , fromNo: SessionManager.getPhone() , toNo: (self.post?.phoneNumber)! , flag: "like ", postId: (self.post?.id)! , postImgUrl: image_url , comment: "")
                            }
                        }
                            
                        webservice.send_notification(identityTo: id_to , title: "Post Like" , msg: " liked your post", toNo: (self.post?.phoneNumber)! , flag: "like" , post_id: (self.post?.id)! ,postImageUrl: image_url , comment: "")
                        }
                    }
                    else{
                        FTIndicator.showToastMessage("Error found in like post!")
                    }
                }
                else{
                    FTIndicator.showToastMessage("Error found in like post!")
                }
            })
    }
    
}
