//
//  PostDetail_VC.swift
//  Tamaas
//
//  Created by Krescent Global on 20/07/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator
import MMPlayerView

class PostDetail_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!

    //MARK:- Variables
    var post_id:String = ""
    var post:Post?
    
    lazy var mmPlayerLayer: MMPlayerLayer = {
        let l = MMPlayerLayer()
        l.cacheType = .memory(count: 5)
        return l
    }()
    
    //MARK:- UIView lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeGround), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if Connectivity.isConnectedToInternet() {
            self.call_getpostDetails()
        } else {
            FTIndicator.showToastMessage("No Internet Connection!")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if constantVC.GlobalVariables.isNeedToRefreshPostDetails {
            constantVC.GlobalVariables.isNeedToRefreshPostDetails = false
            self.call_getpostDetails()
        }
    }
    
    //MARK:- NSNotification observer methods
    @objc func appMovedToBackground() {
        if mmPlayerLayer.player != nil {
            mmPlayerLayer.isHidden = true
            mmPlayerLayer.player?.pause()
            mmPlayerLayer.player?.replaceCurrentItem(with: nil)
        }
    }
    
    @objc func appMovedToForeGround() {
        self.mmPlayerLayer = MMPlayerLayer()
        mmPlayerLayer.cacheType = .memory(count: 5)
    }
    
    
    //MARK:- UIButton Actions
    @IBAction func btn_back_action(_ sender: UIButton) {
        if constantVC.ActiveDataSource.isNavigatePostDetails {
            constantVC.ActiveDataSource.isNavigatePostDetails = false
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "tabBar") as! TabBarController
            controller.modalPresentationStyle = .fullScreen
            self.present(controller, animated: true, completion: nil)
        } else {
           self.dismiss(animated: true , completion: nil)
        }
    }
   
    //MARK:- Web service implementations
    func call_getpostDetails() {
        
        let dictParam : NSDictionary = ["postID":self.post_id]
        
        WebserviceSigleton().POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_POST_BY_ID, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    if let data = response["data"] as? [[String:Any]] {
                        
                        for item in data {
                            
                            var caption: String = ""
                            var photoURL: String = ""
                            var uid: String = ""
                            var user_id_temp : String = ""
                            var id: String = ""
                            var name: String = ""
                            var likeCount: Int = 0
                            var commentCount: Int = 0
                            var isLiked: Bool = false
                            var type_temp : String = ""
                            var created : String = ""
                            var thumbnail : String = ""
                            var height : CGFloat = 300.0
                            var width : CGFloat = 300.0
                            var captionHeight:CGFloat = 0.0
                            var superViewWidth:CGFloat = 0.0
                            var phoneNumber:String = ""
                            var createdDate = Date()
                            var quickBloxID:String = "0"
                            
                            if let _id = item["id"] as? String{
                                id = _id
                            }
                            if let no = item["phoneNumber"] as? String{
                                phoneNumber = no
                            }
                            if let user_id = item["user_id"] as? String{
                                user_id_temp = user_id
                            }
                            if let type = item["type"] as? String{
                                type_temp = type
                            }
                            if let _userid = item["phoneNumber"] as? String{
                                uid = _userid
                            }
                            if let height_ = item["height"] as? String{
                                
                                if let double = Double(height_), height_ != "0" {
                                    height = CGFloat(double)
                                }
                                else{
                                    height = 300.0
                                }
                            }
                            if let width_ = item["width"] as? String{
                                
                                if let double = Double(width_), width_ != "0" {
                                    width = CGFloat(double)
                                }
                                else{
                                    width = 300.0
                                }
                            }
                            if let _userid = item["phoneNumber"] as? String{
                                uid = _userid
                            }
                            
                            if let file = item["file"] as? String{
                                photoURL = file
                            }
                            if let thumb = item["thumbnail"] as? String{
                                thumbnail = thumb
                            }
                            if let capt = item["caption"] {
                                caption = String(describing: capt)
                            }
                            if let name_ = item["name"] as? String{
                                name = name_
                            }
                            if let created_ = item["created"] as? String{
                                let datefrmter = DateFormatter()
                                datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                let date = datefrmter.date(from: created_)
                                createdDate = date!
                                created = (date?.getElapsedInterval())!
                            }
                            
                            if let likeCount_ = item["likesCount"] as? String{
                                likeCount = Int(likeCount_)!
                            }
                            if let commentCount_ = item["commentsCount"] as? String{
                                commentCount = Int(commentCount_)!
                            }
                            if let isLiked_ = item["isLiked"] as? String{
                                if isLiked_ == "FALSE"{
                                    isLiked = false
                                }
                                else{
                                    isLiked = true
                                }
                            }
                            
                            if let quickbloxID = item["quickbloxID"] as? String{
                                quickBloxID = quickbloxID
                            }
                            
                            //calculate caption height
                            let strCaption:String = String(describing: caption)
                            var cell_captionHeight:CGFloat = 0.0
                            
                            if (strCaption.count > 0 && strCaption != "") {
                                cell_captionHeight = (strCaption.height(withConstrainedWidth: self.view.frame.size.width - 16 , font: UIFont (name: "CircularStd-Book", size: 16)!))
                            }
                            
                            captionHeight = cell_captionHeight
                            superViewWidth = self.view.frame.size.width
                            
                            
                            self.post = Post(caption: caption , photoURL: photoURL , uid: uid, user_id: user_id_temp, id: id, name: name, likeCount: likeCount, commentCount: commentCount, isLiked: isLiked , type: type_temp , created: created, thumbnail: thumbnail , height: height , width: width , captionHeight: captionHeight , superViewWidth: superViewWidth , phoneNumber: phoneNumber, createdDate: createdDate, quickBloxID: quickBloxID, polling: [])
                            self.tableView.reloadData()
                            
                        }
                    }
                }
                else{
                }
            }
            else{
            }
        })
    }
    
    //MARK:- Helpers
    @objc open func handleshareTap(_ sender: UIButton!) {
        let post = self.post
        if post?.type == "image"{
            let photoUrl = post?.photoURL
            
            let catPictureURL = URL(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoUrl!)")
            
            let session = URLSession(configuration: .default)
            
            let downloadPicTask = session.dataTask(with: catPictureURL!) { (data, response, error) in
                // The download has finished.
                if let e = error {
                    print("Error downloading cat picture: \(e)")
                } else {
                    // No errors found.
                    // It would be weird if we didn't have a response, so check for that too.
                    if let res = response as? HTTPURLResponse {
                        if let imageData = data {
                            // Finally convert that Data into an image and do what you wish with it.
                            let image = UIImage(data: imageData)
                            // Do something with your image.
                            
                            
                            let shareAll = [post?.caption! , image!] as [Any]
                            let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
                            activityViewController.popoverPresentationController?.sourceView = self.view
                            self.present(activityViewController, animated: true, completion: nil)
                            
                        }
                    }
                }
            }
            
            downloadPicTask.resume()
            
        }
        else{
            let videoURL = URL(string: "\(constantVC.GeneralConstants.ImageUrl)\(post?.photoURL!)")
            let activityItems = [videoURL, post?.caption ] as [Any]
            let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            
            activityController.popoverPresentationController?.sourceView = self.view
            activityController.popoverPresentationController?.sourceRect = self.view.frame
            
            self.present(activityController, animated: true, completion: nil)
        }
    }
    
    fileprivate func updateByContentOffset() {
        let p = CGPoint(x: self.tableView.frame.width/2, y: self.tableView.contentOffset.y + self.tableView.frame.width/2)
        
        if let path = self.tableView.indexPathForRow(at: p),
            self.presentedViewController == nil {
            self.updateCell(at: path)
        }
    }
    fileprivate func updateCell(at indexPath: IndexPath) {
        if let cell = self.tableView.cellForRow(at: indexPath) as? HomeTableViewCell {
            
            if cell.post?.type == "video"{
                if !MMLandscapeWindow.shared.isKeyWindow {
                    mmPlayerLayer.playView = cell.postImageView
                }
                mmPlayerLayer.isHidden = false
                mmPlayerLayer.player?.play()
                if let photoURL = cell.post?.photoURL {
                    
                    mmPlayerLayer.set(url: URL(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL)"), state: { (status) in
                    })
                    mmPlayerLayer.startLoading()
                    
                }
            }
            else{
                mmPlayerLayer.isHidden = true
                mmPlayerLayer.player?.pause()
                if let photoURL = cell.post?.photoURL {
                    cell.postImageView.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photoURL)") , placeholder: nil , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                }
            }
        }
    }
}

// MARK: - TableView Delegate and Data Source Methods
extension PostDetail_VC: UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.post != nil {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeTableViewCell
        cell.postImageView.image = nil
        print(self.post?.isLiked)
        cell.post = self.post
        cell.shareButton.addTarget(self, action: #selector(handleshareTap(_:)), for: UIControlEvents.touchUpInside)
        cell.shareButton.tag = indexPath.row
        cell.homeVC = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.updateByContentOffset()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        var cellHeight : CGFloat = 410
        let imageHeight = (self.post?.height)!*self.view.frame.size.width/(self.post?.width)!
        cellHeight = imageHeight + 125 + (self.post?.captionHeight)!
        return cellHeight
    }
    
}
