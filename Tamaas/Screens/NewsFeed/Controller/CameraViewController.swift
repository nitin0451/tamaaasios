//
//  CameraViewController.swift
//  Blocstagram
//
//  Created by ddenis on 1/1/17.
//  Copyright © 2017 ddApps. All rights reserved.
//

import UIKit
import IQKeyboardManager
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit

class CameraViewController: BaseViewController, LoginButtonDelegate , UITextViewDelegate {
    
    //MARK:- Variables
    let manager = LoginManager()
    var selectedImage: UIImage?
    var videoUrl : URL?
    var type = String()
    let loginButton = FBLoginButton()
    
    //MARK:- Outlets
    @IBOutlet weak var fbVideoImage: UIImageView!
    @IBOutlet weak var fbVideoPopup: UIView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var captionTextView: UITextView!
    @IBOutlet weak var clearBarButton: UIBarButtonItem!
    @IBOutlet weak var switchFB: UISwitch!
    @IBOutlet weak var barBtn_post: UIBarButtonItem!
    @IBOutlet weak var lbl_shareOnFB: UILabel!
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Make the button a Rounded Rect
        self.localise_language()
        photoImageView.image = selectedImage
        photoImageView.layer.cornerRadius = 2
        
        if constantVC.GlobalVariables.shCamera == true{
            type = "image"
        }
        else{
            type = "video"
        }
        
        loginButton.delegate = self;
        loginButton.permissions = ["publish_actions"]
        loginButton.center = fbVideoPopup.center
        fbVideoPopup.addSubview(loginButton)
    }
    
    func localise_language(){
        self.clearBarButton.title = "Back".localized
        self.barBtn_post.title = "Post".localized
        self.lbl_shareOnFB.text = "Share on facebook".localized
        self.captionTextView.text = "Write a caption..".localized
        self.captionTextView.textColor = UIColor.lightGray
        self.captionTextView.delegate = self
    }
    
    override  func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnabled = true
        
      //  self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]
    }
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnabled = false
    }
    
    //MARK:- UITextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write a caption..".localized
            textView.textColor = UIColor.lightGray
        }
    }
    
    //MARK:- FBSDKSharingDelegate
    func sharer(_ sharer: Sharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        self.updateView(isPostShareFbSuccessfully: true)
        self.backTwo()
    }
    
    func backTwo() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    
    func sharer(_ sharer: Sharing!, didFailWithError error: Error!) {

        if let _error = error{
            self.showError(message: _error.localizedDescription)
            
        }
        self.showError(message: "Error in posting to facebook")
        self.updateView(isPostShareFbSuccessfully: false)
        
    }

    func sharerDidCancel(_ sharer: Sharing!) {
        self.updateView(isPostShareFbSuccessfully: false)
    }
    
    func loginButton(_ loginButton: FBLoginButton!, didCompleteWith result: LoginManagerLoginResult!, error: Error!) {
        if ((error) != nil) {
            // Process error
            self.updateView(isPostShareFbSuccessfully: false)
        }
        else if result.isCancelled {
            // Handle cancellations
            self.updateView(isPostShareFbSuccessfully: false)
        }
        else {
            // Navigate to other view
            self.uploadVideoWithoutSettingsCredentials(videoUrl , withTitle: nil, withDesc: captionTextView.text, withPrivacy: "Test")
        }
        removeAnimate(popupView: fbVideoPopup)
    }
    func loginButtonDidLogOut(_ loginButton: FBLoginButton!) {
    }
    

    //MARK:- Helpers
    func shareOnFb(){
      
            if type == "video" {
                if (AccessToken.current != nil) {
                    if !((AccessToken.current?.hasGranted(permission: "publish_actions")) != nil) {
                        
                        
                        showLoader(strForMessage: "Posting..".localized );
                        self.uploadVideoWithoutSettingsCredentials(videoUrl , withTitle: nil, withDesc: captionTextView.text, withPrivacy: "Test")
                        
                    }
                    else{
                        // logout
                        manager.logOut()
                        showAnimate(popupView: fbVideoPopup)
                    }
                    }
                else{
                  showAnimate(popupView: fbVideoPopup)
                }
            }
            else{
                
                let fbInstalled = schemeAvailable(scheme: "fb://")
                
                if (fbInstalled == true) {
                
                let image: UIImage = selectedImage!
                
                    let photo : SharePhoto = SharePhoto()
                photo.image = image
                photo.isUserGenerated = true
                    let content : SharePhotoContent = SharePhotoContent()
                content.photos = [photo]
                    ShareDialog(fromViewController: self, content: content, delegate: nil)
                    //ShareDialog.show(from: self , with: content, delegate: self)
                }
            else{
            
            let alert = UIAlertController(title: nil, message: "You are not connected to facebook app. Please install the app and login into the App", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {  action in
                
                UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/in/app/facebook/id284882215?mt=8")! as URL)
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
        }
    }
    
    func uploadVideoWithoutSettingsCredentials(_ videoUrl: URL?, withTitle title: String?, withDesc desc: String?, withPrivacy privacy: String?) {
        DispatchQueue.global(qos: .default).async(execute: {() -> Void in
            var videoData: Data? = nil
            if let anUrl = videoUrl {
                videoData = try? Data(contentsOf: anUrl, options: [])
            }
            var params = [AnyHashable: Any](minimumCapacity: 4)
            params["\(title ?? "").MOV"] = videoData
            params["description"] = desc
            
            let uploadRequest: GraphRequest = GraphRequest(graphPath: "me/videos", parameters: params as! [String : Any], httpMethod: HTTPMethod(rawValue: "POST"))
            
            uploadRequest.start(completionHandler: { (connection, result, error) -> Void in
                
                if ((error) != nil)
                {
                    self.dismissLoader()
                }
                else
                {
                    self.updateView(isPostShareFbSuccessfully: false)
                    
                }
            })
        })
    }
    
    
    func schemeAvailable(scheme: String) -> Bool {
       
      let shOpen =  UIApplication.shared.canOpenURL(NSURL.init(string:"fb://")! as URL)
      return shOpen
    }
    
    func updateView(isPostShareFbSuccessfully:Bool){
        constantVC.GlobalVariables.shPost = true
        constantVC.GlobalVariables.typeStr = self.type
        
        if self.captionTextView.text != "Write a caption.." {
            constantVC.GlobalVariables.captionStr  = self.captionTextView.text
        } else {
            constantVC.GlobalVariables.captionStr = ""
        }
        constantVC.GlobalVariables.selectedImage = self.selectedImage
        constantVC.GlobalVariables.videoUrl  = self.videoUrl
        
        
        if (!isPostShareFbSuccessfully) {
       /* let next = self.storyboard?.instantiateViewController(withIdentifier: "tabBar") as! UITabBarController
        next.selectedIndex = 1
        self.present(next, animated: true, completion: nil)*/
            self.backTwo()
        }
    }
    
    
    //MARK:- UIButton Actions
    @IBAction func postAc(_ sender: Any) {
        
        if self.switchFB.isOn {
            
            self.shareOnFb()
        }
        else{
            self.updateView(isPostShareFbSuccessfully: false)
        }
    }
    
    @IBAction func clearInputs() {
        
        // clear photo, selected image and caption text after saving
        self.captionTextView.text = ""
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
   
    
}
//MARK:- SharingDelegate
//extension CameraViewController: SharingDelegate {
//    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
//
//    }
//
//    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
//
//    }
//
//    func sharerDidCancel(_ sharer: Sharing) {
//
//    }
//    
//}
/*
 func shareOnFb(){
 
 let fbInstalled = schemeAvailable(scheme: "fb://")
 if (fbInstalled == true) {
 
 if type == "video" {
 
 
 
 if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
 //                    let fbShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
 //                    fbShare.setInitialText(captionTextView.text)
 //                    fbShare.add(selectedImage)
 //                    self.present(fbShare, animated: true, completion: {
 //                        view in
 //                        self.updateView()
 //                    })
 
 let video = FBSDKShareVideo()
 
 video.videoURL = videoUrl
 let contentVideo : FBSDKShareVideoContent = FBSDKShareVideoContent()
 contentVideo.video = video
 FBSDKShareDialog.show(from: self, with: contentVideo, delegate: nil)
 
 
 } else {
 let alert = UIAlertController(title: nil, message: "Please login to a Facebook account to share", preferredStyle: UIAlertControllerStyle.alert)
 let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {  action in
 
 self.loginFb()
 })
 alert.addAction(okAction)
 self.present(alert, animated: true, completion: nil)
 
 
 }
 
 }
 else{
 
 if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
 let fbShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
 fbShare.add(selectedImage)
 fbShare.setInitialText(captionTextView.text)
 self.present(fbShare, animated: true, completion: {
 view in
 self.updateView()
 })
 
 } else {
 let alert = UIAlertController(title: nil, message: "Please login to a Facebook account to share", preferredStyle: UIAlertControllerStyle.alert)
 let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {  action in
 
 
 self.loginFb()
 })
 alert.addAction(okAction)
 self.present(alert, animated: true, completion: nil)
 }
 }
 }else{
 
 let alert = UIAlertController(title: nil, message: "Please install facebook app", preferredStyle: UIAlertControllerStyle.alert)
 let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {  action in
 
 UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/in/app/facebook/id284882215?mt=8")! as URL)
 self.updateView()
 })
 alert.addAction(okAction)
 self.present(alert, animated: true, completion: nil)
 }
 
 }
 
 
 */

