//
//  RollingPitTabBar.swift
//  VBRRollingPit
//
//  Created by Viktor Braun on 27.07.2018.
//  Copyright © 2018 Viktor Braun - Software Development. All rights reserved.
//

import UIKit

class Tabbar: UITabBar{
   static var middleButton = UIButton()
    
    @IBInspectable public var barBackColor : UIColor = UIColor.white
    
    
    private var cachedSafeAreaInsets = UIEdgeInsets.zero
    override var safeAreaInsets: UIEdgeInsets {
        let insets = super.safeAreaInsets
        if insets.bottom < bounds.height {
            cachedSafeAreaInsets = insets
        }
        return cachedSafeAreaInsets
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        setupMiddleButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
        setupMiddleButton()
    }
    
    @objc func addButton(notification : Notification){
        
        if Tabbar.middleButton.backgroundImage(for: UIControlState.normal) == UIImage(named: "start"){
            Tabbar.middleButton.setBackgroundImage(UIImage(named:"stop"), for: UIControlState.normal)
        }
        else{
            Tabbar.middleButton.setBackgroundImage(UIImage(named:"start"), for: UIControlState.normal)
        }
    }
    @objc func addStopButton(notification : Notification){
        Tabbar.middleButton.setBackgroundImage(UIImage(named:"stop"), for: UIControlState.normal)
    }
    @objc func normalButton(notification : Notification){
        Tabbar.middleButton.setBackgroundImage(UIImage(named:"plus"), for: UIControlState.normal)
        
    }
    
    func setupMiddleButton() {
        Tabbar.middleButton.frame.size = CGSize(width: 64, height: 48)
        Tabbar.middleButton.setBackgroundImage(UIImage(named:"plus"), for: UIControlState.normal)
        Tabbar.middleButton.layer.masksToBounds = true
        Tabbar.middleButton.center = CGPoint(x: UIScreen.main.bounds.width / 2, y:  (self.frame.height / 2))
        Tabbar.middleButton.addTarget(self, action: #selector(test), for: .touchUpInside)
        addSubview(Tabbar.middleButton)
    }
    
    private func setup(){
       // self.isTranslucent = true
        self.backgroundImage    = UIImage()
        self.shadowImage        = UIImage()
        self.backgroundColor    = UIColor.white
//        self.shadowColor      = UIColor.red
        self.borderColor        = UIColor(red: 114.0/255.0, green: 125.0/255.0, blue: 136.0/255.0, alpha: 1.0)
        self.borderWidth        = 0.3
        
        for item in self.items ?? []{
            item.imageInsets = UIEdgeInsetsMake(0, 0, -16, 0);
            
        }
        
        
        //Tabbar.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(startButtonAction(notification:)), name: NSNotification.Name(rawValue: "playButton"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(pauseButtonAction(notification:)), name: NSNotification.Name(rawValue: "pauseButton"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(normalButtonAction(notification:)), name: NSNotification.Name(rawValue: "NormalButton"), object: nil)
    }
    
    @objc private func startButtonAction(notification: Notification) {
        Tabbar.middleButton.setBackgroundImage(UIImage(named:"start"), for: UIControlState.normal)
        
    }
    @objc private func pauseButtonAction(notification: Notification) {
        Tabbar.middleButton.setBackgroundImage(UIImage(named:"stop"), for: UIControlState.normal)
    }
    @objc private func normalButtonAction(notification: Notification) {
        Tabbar.middleButton.setBackgroundImage(UIImage(named:"plus"), for: UIControlState.normal)
    }
    
    @objc func test() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyTab"), object: nil, userInfo: nil)
    }
}

extension UITabBar {
     
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        guard let window = UIApplication.shared.keyWindow else {
            return super.sizeThatFits(size)
        }
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = window.safeAreaInsets.bottom + 52
        return sizeThatFits
    }
}

/*
let pi = CGFloat.pi
let pi2 = CGFloat.pi / 2

extension CGFloat {
    public func toRadians() -> CGFloat {
        return self * CGFloat.pi / 180.0
    }
}

@IBDesignable class MainTabBar: UITabBar {
    
   
    @IBInspectable public var barHeight : CGFloat = 55
    @IBInspectable public var barTopRadius : CGFloat = 0
    @IBInspectable public var barBottomRadius : CGFloat = 0
    @IBInspectable var marginBottom : CGFloat = 0
    @IBInspectable var marginTop : CGFloat = 0
    
    let marginLeft : CGFloat = 0
    let marginRight : CGFloat = 0
    
    let pitCornerRad : CGFloat = 10
    let pitCircleDistanceOffset : CGFloat = 7
    private var barRect : CGRect{
        get{
            let h = self.barHeight
            let w = bounds.width - (marginLeft + marginRight)
            let x = bounds.minX + marginLeft
            let y = marginTop
            let rect = CGRect(x: x, y: y, width: w, height: h)
            return rect
        }
    }
 
//
//    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
//        if self.isHidden {
//            return super.hitTest(point, with: event)
//        }
//        let from = point
//        let to = middleButton.center
//
//        return sqrt((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)) <= 39 ? middleButton : super.hitTest(point, with: event)
//    }
    
   
    
    private func getCircleCenter() -> CGFloat{
        let totalWidth = self.bounds.width
        var x = totalWidth / 2
        if let v = getViewForItem(item: self.selectedItem){
            x = v.frame.minX + (v.frame.width / 2)
        }
        
        return x
    }
    
    func createPitMaskPath(rect: CGRect) -> CGMutablePath {
        let circleXcenter = getCircleCenter()
        let backRect = barRect
        let x : CGFloat = circleXcenter + pitCornerRad
        let y = backRect.origin.y
        
        let center = CGPoint(x: x, y: y)
        
        let maskPath = CGMutablePath()
        maskPath.addRect(rect)
        
        let pit = createPitPath(center: center)
        maskPath.addPath(pit)
        
        return maskPath
    }
    
    func createPitPath(center : CGPoint) -> CGPath{
        
        let x = center.x - pitCornerRad
        let y = center.y
        
        let result = UIBezierPath()
        result.lineWidth = 0
        result.move(to: CGPoint(x: x - 0, y: y + 0))
        
        result.addArc(withCenter: CGPoint(x: (x - pitCornerRad), y: (y + pitCornerRad)), radius: pitCornerRad, startAngle: CGFloat(270).toRadians(), endAngle: CGFloat(0).toRadians(), clockwise: true)
        
        
        result.addLine(to: CGPoint(x: 0, y: 0))
        
        result.close()
        
        return result.cgPath
    }
    
    private func createBackgroundPath() -> CGPath{
        
        let rect = barRect
        let topLeftRadius = self.barTopRadius
        let topRightRadius = self.barTopRadius
        let bottomRigtRadius = self.barBottomRadius
        let bottomLeftRadius = self.barBottomRadius
        
        let path = UIBezierPath()
        
        path.move(to: CGPoint(x: rect.minX + topLeftRadius, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX - topLeftRadius, y:rect.minY))
        path.addArc(withCenter: CGPoint(x: rect.maxX - topRightRadius, y: rect.minY + topRightRadius), radius: topRightRadius, startAngle:3 * pi2, endAngle: 0, clockwise: true)
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - bottomRigtRadius))
        path.addArc(withCenter: CGPoint(x: rect.maxX - bottomRigtRadius, y: rect.maxY - bottomRigtRadius), radius: bottomRigtRadius, startAngle: 0, endAngle: pi2, clockwise: true)
        path.addLine(to: CGPoint(x: rect.minX + bottomRigtRadius, y: rect.maxY))
        path.addArc(withCenter: CGPoint(x: rect.minX + bottomLeftRadius, y: rect.maxY - bottomLeftRadius), radius: bottomLeftRadius, startAngle: pi2, endAngle: pi, clockwise: true)
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY + topLeftRadius))
        path.addArc(withCenter: CGPoint(x: rect.minX + topLeftRadius, y: rect.minY + topLeftRadius), radius: topLeftRadius, startAngle: pi, endAngle: 3 * pi2, clockwise: true)
        path.close()
        
        return path.cgPath
    }
    
   
    
    override public func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = self.barHeight + marginTop + marginBottom
        return sizeThatFits
    }
    
    private func getTabBarItemViews() -> [(item : UITabBarItem, view : UIView)]{
        guard let items = self.items else{
            return[]
        }
        
        var result : [(item : UITabBarItem, view : UIView)] = []
        for item in items {
            if let v = getViewForItem(item: item) {
                result.append((item: item, view: v))
            }
        }
        
        return result
    }
    
    private func getViewForItem(item : UITabBarItem?) -> UIView?{
        if let item = item {
            let v = item.value(forKey: "view") as? UIView
            return v
        }
        
        return nil
        
    }
    
    private func positionItem(barRect : CGRect, totalCount : Int, idx : Int, item : UITabBarItem, view : UIView){
        let margin : CGFloat = 5
        let x = view.frame.origin.x
        var y = barRect.origin.y + margin
        let h = barHeight - (margin * 2)
        let w = view.frame.width
        if self.selectedItem == item {
            y = barRect.origin.y
        }
        view.frame = CGRect(x: x, y: y, width: w, height: h)
    }
    
    private func animateHideAndShowItem(itemView : UIView){
        itemView.alpha = 0
        itemView.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(400) , execute: {
            UIView.animate(withDuration: 0.4) {
                itemView.alpha = 1
            }
        })
    }
    private func createPathMoveAnimation(toVal : CGPath) -> CABasicAnimation{
        let animation = CABasicAnimation(keyPath: "path")
        animation.duration = 1
        //        animation.beginTime = CACurrentMediaTime() + 2
        animation.toValue = toVal
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.isRemovedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        return animation
    }
    
    private func replaceAnimation(layer :CAShapeLayer, withNew : CABasicAnimation, forKey: String){
        
        let existing = layer.animation(forKey: forKey) as? CABasicAnimation
        if existing != nil{
            withNew.fromValue = existing!.toValue
        }
        
        layer.removeAnimation(forKey: forKey)
        layer.add(withNew, forKey: forKey)
    }
    
//    private func layoutElements(selectedChanged : Bool){
//        self.background.path = self.createBackgroundPath()
//        if self.backgroundMask.path == nil {
//            self.backgroundMask.path = self.createPitMaskPath(rect: self.bounds)
//        }
//    }
    
//    override var selectedItem: UITabBarItem? {
//        get{
//            return super.selectedItem
//        }
//        set{
//            let changed = (super.selectedItem != newValue)
//            super.selectedItem = newValue
//            if changed {
//                layoutElements(selectedChanged: changed)
//            }
//        }
//    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        background.fillColor = self.barBackColor.cgColor
//        self.layoutElements(selectedChanged: false)
//    }
//
//    override func prepareForInterfaceBuilder() {
//        self.isTranslucent = true
//        self.backgroundColor = UIColor.clear
//        self.backgroundImage = UIImage()
//        self.shadowImage = UIImage()
//        background.fillColor = self.barBackColor.cgColor
//    }
    
   
   
}

*/

