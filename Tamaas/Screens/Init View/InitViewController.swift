//
//  InitViewController.swift
//  Tamaas
//
//  Created by Krescent Global on 18/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class InitViewController: BaseViewController {
    let colors = Colors()
   
    var segueString = "RegisterPhoneVC"
    @IBOutlet weak var ChooseLangLabel: UILabel!
    @IBOutlet weak var changeLangLabel: UILabel!
    @IBOutlet weak var btn_eng: UIButton!
    @IBOutlet weak var btn_dari: UIButton!
    @IBOutlet weak var btn_turkish: UIButton!
     
    @IBOutlet weak var lbl_poweredBy: UILabel!
    @IBOutlet weak var btn_pashto: UIButton!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view3: UIView!
     
     
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localise_language()
        refresh()
        ChooseLangLabel.adjustsFontSizeToFitWidth = true;
        changeLangLabel.adjustsFontSizeToFitWidth = true;
        view1.frame.size.height = 0.5
        view2.frame.size.height = 0.5
        view3.frame.size.height = 0.5
        
        // Do any additional setup after loading the view.
    }
    
    func localise_language(){
        self.ChooseLangLabel.text = "Choose your language to start with".localized
        self.changeLangLabel.text = "You can change the language later".localized
        self.btn_eng.setTitle( "English", for: .normal)
        self.btn_dari.setTitle( "دری" , for: .normal)
        self.btn_pashto.setTitle( "پښتو" , for: .normal)
        self.btn_turkish.setTitle( "Türkçe", for: .normal)
        self.lbl_poweredBy.text = "Powered by Tamaaas Inc.".localized
    }
   
    
    @IBAction func engAc(_ sender: Any) {
        SessionManager.save_localizeLanguage(lang: "en")
        let next = self.storyboard?.instantiateViewController(withIdentifier: segueString) as! RegisterPhoneVC
        next.modalPresentationStyle = .overFullScreen
        self.present(next, animated: true, completion: nil)
    }
    
    
    @IBAction func btnDari_action(_ sender: Any) {
        SessionManager.save_localizeLanguage(lang: "fa-AF")
        let next = self.storyboard?.instantiateViewController(withIdentifier: segueString) as! RegisterPhoneVC
        next.modalPresentationStyle = .overFullScreen
        self.present(next, animated: true, completion: nil)
    }
    
    @IBAction func btnPashto_action(_ sender: Any) {
        SessionManager.save_localizeLanguage(lang: "ps-AF")
        let next = self.storyboard?.instantiateViewController(withIdentifier: segueString) as! RegisterPhoneVC
        next.modalPresentationStyle = .overFullScreen
        self.present(next, animated: true, completion: nil)
    }
    
    @IBAction func btnTurkish_action(_ sender: Any) {
        SessionManager.save_localizeLanguage(lang: "tr")
        let next = self.storyboard?.instantiateViewController(withIdentifier: segueString) as! RegisterPhoneVC
        next.modalPresentationStyle = .overFullScreen
        self.present(next, animated: true, completion: nil)
          
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarView?.backgroundColor = UIColor(red: 217.0/255.0, green: 6.0/255.0, blue: 71.0/255.0, alpha: 1.0)
    }
    
    func refresh() {
        view.backgroundColor = UIColor.clear
        let backgroundLayer = colors.gl
        backgroundLayer?.frame = view.frame
        view.layer.insertSublayer(backgroundLayer!, at: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

class Colors {
    var gl:CAGradientLayer!
    
    init() {
        let colorTop = UIColor(red: 217.0 / 255.0, green: 6.0 / 255.0, blue: 71.0 / 255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 235.0 / 255.0, green: 64.0 / 255.0, blue: 44.0 / 255.0, alpha: 1.0).cgColor
        
        self.gl = CAGradientLayer()
        self.gl.colors = [colorTop, colorBottom]
        self.gl.locations = [0.0, 1.0]
    }
}
