//
//  ChatViewController.swift
//  sample-chat-swift
//
//  Created by Anton Sokolchenko on 4/1/15.
//  Copyright (c) 2015 quickblox. All rights reserved.
//

import CoreTelephony
import SafariServices
import Pulsator
import DropDown
import FTIndicator
import Contacts
import ContactsUI
import CallKit
import PhoneNumberKit
import QBImagePickerController
import IQKeyboardManager

enum CallViewControllerState : Int {
    case disconnected
    case connecting
    case connected
    case disconnecting
}

struct CallStateConstant {
    static let disconnected = "Disconnected"
    static let connecting = "Connecting..."
    static let connected = "Connected"
    static let disconnecting = "Disconnecting..."
}

struct CallConstant {
    static let opponentCollectionViewCellIdentifier = "OpponentCollectionViewCellIdentifier"
    static let unknownUserLabel = "Unknown user"
    static let sharingViewControllerIdentifier = "SharingViewController"
    static let refreshTimeInterval: TimeInterval = 1.0
    static let recordingTitle = "[Recording] "
    
    static let memoryWarning = NSLocalizedString("MEMORY WARNING: leaving out of call", comment: "")
    static let sessionDidClose = NSLocalizedString("Session did close due to time out", comment: "")
    static let savingRecord = NSLocalizedString("Saving record", comment: "")
}


var messageTimeDateFormatter: DateFormatter {
    struct Static {
        static let instance : DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "h:mm a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            return formatter
        }()
    }
    
    return Static.instance
}

extension String {
    var length: Int {
        return (self as NSString).length
    }
}

class ChatViewController: QMChatViewController, QMChatServiceDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, QMChatAttachmentServiceDelegate, QMChatConnectionDelegate, QMChatCellDelegate, QMDeferredQueueManagerDelegate, QMPlaceHolderTextViewPasteDelegate , ACTabScrollViewDelegate, ACTabScrollViewDataSource , SelectedStickerDelegate , QMMediaControllerDelegate , AVAudioRecorderDelegate , CNContactViewControllerDelegate , SwiftMultiSelectDelegate,SwiftMultiSelectDataSource , UIGestureRecognizerDelegate {
    
     
    @IBOutlet weak var viewChatInner: UIView!
    var arrReplyIds = [Int:String]()
    func view(for message: QBChatMessage) -> QMMediaViewDelegate? {
        
        let indexPath: IndexPath? = chatDataSource.indexPath(for: message)
        let visibleIndexPathes = collectionView?.indexPathsForVisibleItems
        var hasPath: Bool? = nil
        if let aPath = indexPath {
            hasPath = visibleIndexPathes?.contains(aPath)
        }
        var cell: UICollectionViewCell? = nil
        if let aPath = indexPath {
            cell = collectionView?.cellForItem(at: aPath)
        }
        
        if cell == nil && hasPath ?? false {
            assert(false, "Invalid parameter not satisfying: false")
        }
        return cell as? QMMediaViewDelegate
    }
    
    func dialogID() -> String {
        return self.dialog.id!
    }
    
    //call
    //Nitin

    private var nav: UINavigationController?
    let core = Core.instance
    var onlineTimer : Timer!
    var occupantID:NSNumber?
    private let kQMAttachmentCellSize: CGFloat = 120.0
    private let kQMMaxAttachmentDuration: TimeInterval = 30.0
    private let kQMWidthPadding: CGFloat = 40.0
    private let kQMAvatarSize: CGFloat = 28.0
    private let kQMTextAttachmentSpacing = "  "
    
    let maxCharactersNumber = 1024 // 0 - unlimited
    
    var failedDownloads: Set<String> = []
    var dialog: QBChatDialog!
    var willResignActiveBlock: AnyObject?
    var attachmentCellsMap: NSMapTable<AnyObject, AnyObject>!
    var detailedCells: Set<String> = []
    var inputToolBarHeight:CGFloat = 55
    var previousSelectedOption:UIView?
    var contentViews: [UIView] = []
    var selectedCategoryIndex:Int = 2
    let baseInstance = BaseViewController()
    let pulsator = Pulsator()
    var recordSeconds = 0
    var recordMinutes = 0
    var recordingSession : AVAudioSession!
    var audioRecorder    :AVAudioRecorder!
    var timer = Timer()
    var settings = [String : Int]()
    var StrTitle:String = ""
    let dropDown = DropDown()
    let groupDropDown = DropDown()
    var opponentNo:String = ""
    var items:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var selectedItems:[SwiftMultiSelectItem] = [SwiftMultiSelectItem]()
    var arrCOntactMessage = [CNContact]()
    let phoneNumberKit = PhoneNumberKit()
    var isUnreadCountAvailable = Bool()
    var typingTimer: Timer?
    var popoverController: UIPopoverController?
    var arrIndividualChat = ["Tamaaas Call".localized , "Create Group".localized , "Chat Info".localized]
    var arrGroupChat = ["Create Group".localized , "Add Participants".localized , "Group Info".localized]
    
    lazy var imagePickerViewController : UIImagePickerController = {
        let imagePickerViewController = UIImagePickerController()
        imagePickerViewController.delegate = self
        
        return imagePickerViewController
    }()
    
    var isHeaderLoaded:Bool = false
    var unreadMessages: [QBChatMessage]?
    var groupOwnerNo:String = ""
    var opponentServerID:String = ""
    var selectedIndexPath:IndexPath?
    var selectedViewClass:AnyClass?
    var finalPosition: CGFloat!
    var arrMsgsToForward:[QBChatMessage] = []
    var arrImgMedia:[QBChatMessage] = []
    deinit {
        NotificationCenter.default.removeObserver(self)
        ServicesManager.instance().chatService.removeDelegate(self)
        ServicesManager.instance().chatService.chatAttachmentService.removeDelegate(self)
        QBChat.instance.removeDelegate(self)
    }
    //Nitin
    var isPresented : Bool?
    var chatInnerViewObj: ChatInnerView?
    var doctorCallingPriceViewObj: DoctorCallingPricePopupView?
    var symptomsMessage : String?
    var movingFor: String?

    //MARK:- UIkeyboard handling methods
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        //nitin

        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            var keyboardHeight = view.bounds.height - endFrame.origin.y
            if #available(iOS 11, *) {
                if keyboardHeight > 0 {
                    // if !self.isIphoneX() {
                    keyboardHeight = keyboardHeight - view.safeAreaInsets.bottom
                    // }
                }
            }
            self.stickerView?.isHidden = true
            self.accessoryView?.isHidden = true
            self.inputBarView_bottom?.constant = keyboardHeight
            view.layoutIfNeeded()
        }
    }
    
    @objc func dismissKeyboard() {
        
        self.view.endEditing(true)
        self.showChatInnerView()
        inputBarView_bottom?.constant = 0
        inputBarView_height?.constant = self.inputToolBarHeight
        view.layoutIfNeeded()
        if self.accessoryView?.isHidden == false{
            UIView.transition(with: view, duration: 0.5, options: .curveEaseIn , animations: {
                self.accessoryView?.isHidden = true
            })
        }
        if stickerView?.isHidden == false{
            UIView.transition(with: view, duration: 0.5, options: .curveEaseIn , animations: {
                self.stickerView?.isHidden = true
            })
        }
        self.btnAddAccessory?.setImage(UIImage(named:"addContents"), for: .normal)
    }
    
    func configurePermissionView(){
        self.vwPermission?.layer.cornerRadius = 5.0
        self.btnAddToContacts?.layer.cornerRadius = 18.5
        self.btnSkipForNow?.layer.cornerRadius = 18.5
        self.btnBlockPermission?.layer.cornerRadius = 18.5
        self.btnAddToContacts?.addTarget(self, action: #selector(didPressAddToContacts), for: .touchUpInside)
        self.btnBlockPermission?.addTarget(self, action: #selector(didPressBlockPermission), for: .touchUpInside)
        self.btnSkipForNow?.addTarget(self, action: #selector(didPressSkipForNow), for: .touchUpInside)
        
        if let data = dialog?.data {
            if let isAllowed = data["isAllowed"] as? String {
                if isAllowed == "false" {
                    self.lblpermissionText?.text = "This sender is not in your contacts"
                    self.vwPermission?.isHidden = false
                    self.vwPermissionContainer?.isHidden = false
                    self.constraint_collectionViewTop?.constant = 145
                }
            }
        }
    }
    
    func configureTopBar(){
        self.btnBack?.addTarget(self, action: #selector(didPressbackButton), for: .touchUpInside)
        self.btnDots?.addTarget(self, action: #selector(didPressDotButton), for: .touchUpInside)
        if self.dialog.type == QBChatDialogType.private {
            self.btnVoiceCall?.isHidden = false
            self.btnVoiceCall?.addTarget(self, action: #selector(didPressVoiceCall), for: .touchUpInside)
        } else {
            self.btnVoiceCall?.isHidden = true
        }
        self.btnVideoCall?.addTarget(self, action: #selector(didPressVideoCall), for: .touchUpInside)
        
    }
    
    func configureRecordingView(){
        self.btnSendVoiceMsg?.addTarget(self, action: #selector(didPressSendAudioButton), for: .touchUpInside)
        self.btnCloseRecordingView?.addTarget(self, action: #selector(didPressCancelAudioButton), for: .touchUpInside)
    }
    
    func configureReplyView(){
        self.textMsgReplyContainer?.isHidden = true
        self.textMsgReplyContainer?.btnClose.addTarget(self, action: #selector(didPress_closeTextMsgReplyContainer), for: .touchUpInside)
        self.vwReply?.layer.cornerRadius = 6.0
        self.vwReply?.clipsToBounds = true
        self.vwReply_btnReply?.addTarget(self, action: #selector(didPress_replyMsg), for: .touchUpInside)
        self.vwReply_btnCopy?.addTarget(self, action: #selector(didPress_copyMsg), for: .touchUpInside)
        self.vwReply_btnDelete?.addTarget(self, action: #selector(didPress_deleteMsg), for: .touchUpInside)
        self.vwReply_btnForward?.addTarget(self, action: #selector(didPress_forwardMsg), for: .touchUpInside)
        
        //add tab gesture to blur view
        let tapGestureblur = UITapGestureRecognizer(target: self, action: #selector(self.handleTap_BlurView(_:)))
        self.vwBlur?.addGestureRecognizer(tapGestureblur)
        self.vwBlur?.isUserInteractionEnabled = true
    }
    
    func configureInputBar(){
        self.btnAddAccessory?.addTarget(self, action: #selector(didPressAccessoryButton), for: .touchUpInside)
        self.btnSmiley?.addTarget(self, action: #selector(didPressSmileyButton), for: .touchUpInside)
        self.btnMic_Send?.addTarget(self, action: #selector(didPressMicOrSend), for: .touchUpInside)
        self.btnChatInfo?.addTarget(self, action: #selector(didPressChatInfo), for: .touchUpInside)
    }
    
    func configureAccessoryView(){
        self.btnAlbum?.addTarget(self, action: #selector(didPressAlbumButton), for: .touchUpInside)
        self.btnCamera?.addTarget(self, action: #selector(didPressCameraButton), for: .touchUpInside)
        self.btnContact?.addTarget(self, action: #selector(didPressContactButton), for: .touchUpInside)
        if self.dialog.type == QBChatDialogType.private {
            self.btnAccessoryVoiceCall?.isUserInteractionEnabled = true
            self.btnAccessoryVoiceCall?.addTarget(self, action: #selector(didPressVoiceCall), for: .touchUpInside)
        } else {
            self.btnAccessoryVoiceCall?.isUserInteractionEnabled = false
        }
        self.btnAccessoryVideoCall?.addTarget(self, action: #selector(didPressVideoCall), for: .touchUpInside)
    }
    
    func configureForwardMessageView(){
        self.vwForward_btnForward?.addTarget(self, action: #selector(didPress_vwForward_BtnForward), for: .touchUpInside)
        self.vwForward_btnCancel?.addTarget(self, action: #selector(didPress_vwForward_BtnCancel), for: .touchUpInside)
    }
    
    func configureDotView(){
        
        arrIndividualChat = ["Tamaaas Call".localized , "Create Group".localized , "Chat Info".localized]
        //add new option of add contact
        dropDown.anchorView = self.btnDots
        dropDown.width = 170
        dropDown.textFont =  UIFont(name: "CircularStd-Book", size: 16)!
        if self.dialog.type == QBChatDialogType.private {
            if let data = dialog?.data {
                if let number = data["number"] as? String {
                    
                    if let dicCustomData = QuickBloxManager().convertJsonStringToDictionary(text: number) {
                        if let dicNo = dicCustomData["number"] as? String {
                            let arrNo = dicNo.components(separatedBy: "-")
                            for item in arrNo {
                                if item != SessionManager.getPhone() {
                                    self.opponentNo = item
                                }
                            }
                        }
                    } else {
                        let arrNo = number.components(separatedBy: "-")
                        for item in arrNo {
                            if item != SessionManager.getPhone() {
                                self.opponentNo = item
                            }
                        }
                    }
                }
            }
            
            if !(SessionManager.checkUserSavedInPhoneBook(number: opponentNo)) {
                self.arrIndividualChat.append("Add to contacts".localized)
            }
            dropDown.dataSource = self.arrIndividualChat
        } else {
            dropDown.dataSource = self.arrGroupChat
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            if index == 0 {
                
                if self.dialog.type == QBChatDialogType.private {
                    if Connectivity.isConnectedToInternet() {
                        //tamaaas call
                        if QuickBloxManager().callsAllowed() {
                            if !constantVC.ActiveSession.isStartedConnectingCall {
                                constantVC.ActiveSession.isStartedConnectingCall = true
                                CallConnectionManager.instance.prepareCall_AUDIO(opponentNo: self.opponentNo , dialog: self.dialog , groupName: "")
                            }
                        } else {
                            FTIndicator.showError(withMessage: "No Internet Connection!".localized)
                        }
                    }
                    else {
                        //OFFLINE
                        FTIndicator.showToastMessage("No Internet Connection!")
                    }
                } else {
                    //create group
                    let groupViewController = CreateNewGroup()
                    let navigationController = UINavigationController(rootViewController: groupViewController)
                    self.present(navigationController, animated: true, completion: nil)
                }
            }
            else if index == 1 {
                if self.dialog.type == QBChatDialogType.private {
                    
                    if CallConnectionManager.instance.isAuthorizedUser(dialog: self.dialog) {
                        //create group
                        let groupViewController = CreateNewGroup()
                        let navigationController = UINavigationController(rootViewController: groupViewController)
                        self.present(navigationController, animated: true, completion: nil)
                    } else {
                        CallConnectionManager.instance.showUserBlockedInfo()
                    }
                    
                } else {
                    //add participants
                    self.addParticipantsToGroup()
                }
            }
            else if index == 2 {
                
                if self.dialog.type == QBChatDialogType.private {
                    //chat info
                    self.showChatInfo()
                } else {
                    //group info
                    self.showGroupInfo()
                }
            }
            else if index == 3 {
                
                if self.dialog.type == QBChatDialogType.private {
                    
                    //add to contacts
                    if CallConnectionManager.instance.isAuthorizedUser(dialog: self.dialog) {
                        self.saveNewContact(number: self.opponentNo)
                    } else {
                        CallConnectionManager.instance.showUserBlockedInfo()
                    }
                }
            }
        }
        dropDown.direction = .bottom
    }
    
    func configureChatHeader(){
        collectionView?.register(UINib(nibName: "TamHeaderCell" , bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier:"TamHeaderCell")
        self.collectionView?.register(UINib(nibName: "TamHeaderCell" , bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier:"TamHeaderCell")
    }
    
    //MARK:- Ham Burger Functions
    func showChatInfo(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatInfo_VC") as! ChatInfo_VC
        controller.dialog = self.dialog
        controller.delegate = self
        controller.opponentNo = self.opponentNo
        
        if self.navigationController != nil {
            self.navigationController?.pushViewController(controller , animated: false)
        } else {
            let nav: UINavigationController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "navChat") as! UINavigationController
            controller.isNewNav = true
            nav.viewControllers = [controller]
            self.present(nav , animated: false , completion: nil)
        }
    }
    
    
    func showGroupInfo(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GroupInfo_VC") as! GroupInfo_VC
        controller.dialog = self.dialog
        controller.delegate = self
        controller.strGroupName = self.lblTitle?.text ?? ""
        
        if self.navigationController != nil {
            self.navigationController?.pushViewController(controller , animated: false)
        } else {
            let nav: UINavigationController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "navChat") as! UINavigationController
            controller.isNewNav = true
            nav.viewControllers = [controller]
            self.present(nav , animated: false , completion: nil)
        }
    }
    
    func clearChat() {
        //show delete confirmation alert
        let alertController = UIAlertController(title: "Are you sure?".localized , message: "You want to clear chat".localized, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            //delete message
            var selectedMsgs = Set<String>()
            for msg in self.chatDataSource.allMessages() {
                if let chatMsg = msg as? QBChatMessage {
                    if let msgID = chatMsg.id {
                        selectedMsgs.insert(msgID)
                    }
                }
            }
            QBRequest.deleteMessages(withIDs: selectedMsgs , forAllUsers: false , successBlock: { (success) in
                ServicesManager.instance().chatService.deleteMessagesLocally(self.chatDataSource.allMessages() as! [QBChatMessage] , forDialogID: self.dialog.id!)
                
                //update dialog
                QuickBloxManager().clearChat_UpdateDialog(dialog: self.dialog , completionHandler: { (success) in
                    
                })
                self.collectionView?.reloadData()
                if let allMsgs = self.chatDataSource.allMessages() {
                    
                    for m in allMsgs {
                        self.chatDataSource.delete((m as! QBChatMessage))
                    }
                }
            }, errorBlock: { (error) in
                
            })
     }
       
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func GetGroupInfo(){
        
        var arrOccupantPhn:[String] = []
        //  let users = ServicesManager.instance().usersService.usersMemoryStorage.unsortedUsers()
        
        //get group owner
        ServicesManager.instance().usersService.getUserWithID( self.dialog?.userID as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
            if let user = task.result {
                if let no = user.phone {
                    self.groupOwnerNo = no
                }
            }
            return nil
        })
        
        if let occupantIDS = self.dialog.occupantIDs {
            for id in occupantIDS {
                if let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: id as! UInt) {
                    if let no = user.phone {
                        arrOccupantPhn.append(SessionManager.getNameFromMyAddressBook(number: no))
                    }else{
                         if let user_ = ServicesManager.instance().usersService.getUserWithID(id as! UInt) as? QBUUser {
                               if let no = user_.phone {
                                   arrOccupantPhn.append(SessionManager.getNameFromMyAddressBook(number: no))
                              }
                         }
                    }
                }
                else {
                    ServicesManager.instance().usersService.getUserWithID(id as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                        if let user = task.result {
                            if let no = user.phone {
                                arrOccupantPhn.append(SessionManager.getNameFromMyAddressBook(number: no))
                            }
                        }
                        return nil
                    })
                }
            }
        }
        
        groupDropDown.anchorView = self.vwParticipantsDropDown
        groupDropDown.dataSource = arrOccupantPhn
        groupDropDown.direction = .bottom
    }
    
    func addParticipantsToGroup(){
        if let occupantIDs = self.dialog.occupantIDs {
            
            let groupViewController = CreateNewGroup()
            groupViewController.isAddParticipants = true
            groupViewController.dialog = self.dialog
            groupViewController.arrAlreadyAdded_groupMembersIDs = occupantIDs
            let navigationController = UINavigationController(rootViewController: groupViewController)
            self.present(navigationController, animated: true, completion: nil)
        }
    }
    
    func saveNewContact(number:String) {
        let store = CNContactStore()
        let contact = CNMutableContact()
        let homePhone = CNLabeledValue(label: CNLabelHome, value: CNPhoneNumber(stringValue :number))
        contact.phoneNumbers = [homePhone]
        let controller = CNContactViewController(forNewContact: contact)
        controller.contactStore = store
        controller.delegate = self
        controller.title = "Create New Contact"
        let pNavController = UINavigationController(rootViewController: controller)
        self.present(pNavController , animated: true , completion: nil)
    }
    
    func contactViewController(_ viewController: CNContactViewController, didCompleteWith contact: CNContact?) {
        self.dismiss(animated: true , completion: {
            
            if contact != nil {
                
                self.vwPermission?.isHidden = true
                self.constraint_collectionViewTop?.constant = 0
                
                var userName = ""
                
                if let name = contact?.givenName as? String{
                    userName = name
                }
                
                let addressBookQuick: NSMutableOrderedSet? = []
                constantVC.GlobalVariables.dictMyContacts = [:]
                
                if SessionManager.get_dictMyContacts() != nil {
                    constantVC.GlobalVariables.dictMyContacts = SessionManager.get_dictMyContacts()
                }
                
                var phone:String = self.opponentNo
                //parse and format for country code
                do {
                    let parsedPhNo = try self.phoneNumberKit.parse(phone, withRegion: SessionManager.getCountryCode() , ignoreType: true)
                    if let formatPhNo = self.phoneNumberKit.format(parsedPhNo, toType: .e164) as? String{
                        phone = formatPhNo
                    }
                } catch {
                    print("no country code")
                }
                
                phone = phone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                
                var contactQuick = QBAddressBookContact()
                contactQuick.name = userName
                contactQuick.phone = phone
                addressBookQuick?.add(contactQuick)
                
                constantVC.GlobalVariables.dictMyContacts![phone] = userName
                SessionManager.save_dictMyContacts(dict: constantVC.GlobalVariables.dictMyContacts)
                QuickBloxManager().AddNewContactToAddressBook(addressBookQuick: addressBookQuick! , completionHandler: { (success) in
                    if success {
                        constantVC.ActiveDataSource.isActiveRefresh = true
                        self.updateTitle()
                        
                        //update contacts over local server
                        let update_NS = Update_Get_allLinked_Contacts()
                        update_NS.getContacts()
                        ContactManager().getAddressBookAndUpdate { (success) in
                            if !(success) {
                            }
                        }
                    } else {
                        FTIndicator.showError(withMessage: "Error in saving contact")
                    }
                })
            }
        })
    }
    
    func configureStickerOptions(){
        self.btnStickerOption1?.addTarget(self, action: #selector(didPressStickerOption), for: .touchUpInside)
        self.btnStickerOption2?.addTarget(self, action: #selector(didPressStickerOption), for: .touchUpInside)
        self.btnStickerOption3?.addTarget(self, action: #selector(didPressStickerOption), for: .touchUpInside)
        self.btnStickerOption4?.addTarget(self, action: #selector(didPressStickerOption), for: .touchUpInside)
        self.btnStickerOption5?.addTarget(self, action: #selector(didPressStickerOption), for: .touchUpInside)
        self.btnStickerOption6?.addTarget(self, action: #selector(didPressStickerOption), for: .touchUpInside)
        self.btnStickerOption7?.addTarget(self, action: #selector(didPressStickerOption), for: .touchUpInside)
        self.btnStickerOption8?.addTarget(self, action: #selector(didPressStickerOption), for: .touchUpInside)
        self.btnStickerOption1?.tag = 0
        self.btnStickerOption2?.tag = 1
        self.btnStickerOption3?.tag = 2
        self.btnStickerOption4?.tag = 3
        self.btnStickerOption5?.tag = 4
        self.btnStickerOption6?.tag = 5
        self.btnStickerOption7?.tag = 6
        self.btnStickerOption8?.tag = 7
        
        self.vwStickerSelection1?.isHidden = true
        self.vwStickerSelection2?.isHidden = true
        self.previousSelectedOption = self.vwStickerSelection3
        self.vwStickerSelection3?.isHidden = false
        self.vwStickerSelection4?.isHidden = true
        self.vwStickerSelection5?.isHidden = true
        self.vwStickerSelection6?.isHidden = true
        self.vwStickerSelection7?.isHidden = true
        self.vwStickerSelection8?.isHidden = true
        
        self.vwStickerSelection1?.cornerRadius = (self.vwStickerSelection1?.frame.size.width)!/2
        self.vwStickerSelection2?.cornerRadius = (self.vwStickerSelection2?.frame.size.width)!/2
        self.vwStickerSelection3?.cornerRadius = (self.vwStickerSelection3?.frame.size.width)!/2
        self.vwStickerSelection4?.cornerRadius = (self.vwStickerSelection4?.frame.size.width)!/2
        self.vwStickerSelection5?.cornerRadius = (self.vwStickerSelection5?.frame.size.width)!/2
        self.vwStickerSelection6?.cornerRadius = (self.vwStickerSelection6?.frame.size.width)!/2
        self.vwStickerSelection7?.cornerRadius = (self.vwStickerSelection7?.frame.size.width)!/2
        self.vwStickerSelection8?.cornerRadius = (self.vwStickerSelection8?.frame.size.width)!/2
        
        //sticker view
        self.stcikerScollTab?.defaultPage = 0
        self.stcikerScollTab?.arrowIndicator = false
        self.stcikerScollTab?.tabSectionHeight = 0
        self.stcikerScollTab?.delegate = self
        self.stcikerScollTab?.dataSource = self
        
        // create content views from storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        for var i in 0..<6 {
            let vc = storyboard.instantiateViewController(withIdentifier: "StickerContainer_VC") as! StickerContainer_VC
            
            switch i {
            case 0:
                vc.arrStickers = constantVC.GlobalVariables.family
            case 1:
                vc.arrStickers = constantVC.GlobalVariables.food
            case 2:
                vc.arrStickers = constantVC.GlobalVariables.saying
            case 3:
                vc.arrStickers = constantVC.GlobalVariables.famous
            case 4:
                vc.arrStickers = constantVC.GlobalVariables.sport
            case 5:
                vc.arrStickers = constantVC.GlobalVariables.islam
            default:
                SessionManager.printLOG(data: "default")
            }
            
            vc.index = i
            vc.delegate = self
            vc.containerSize = self.stcikerScollTab?.frame.size
            addChildViewController(vc) // don't forget, it's very important
            contentViews.append(vc.view)
        }
        
    }
    
    //MARK:- Recording view Button Actions
    @objc func didPressSendAudioButton(sender: UIButton!) {
        if Connectivity.isConnectedToInternet() {
            self.finishRecording(success: true)
        }
        else {
            //OFFLINE
            // FTIndicator.showToastMessage("No Internet Connection!")
        }
        
        timer.invalidate()
        self.recordingVw?.isHidden = true
        self.inputBarView_bottom?.constant = 0
        view.layoutIfNeeded()
    }
    @objc func didPressCancelAudioButton(sender: UIButton!) {
        if audioRecorder.isRecording == true {
            audioRecorder.stop()
        }
        self.recordingVw?.isHidden = true
        pulsator.stop()
        timer.invalidate()
        self.inputBarView_bottom?.constant = 0
        view.layoutIfNeeded()
    }
    //MARK:- Top Bar Button Actions
    @objc func didPressbackButton(sender: UIButton!) {
        //mark all messages Read
        if let id = self.dialog.id {
            QBRequest.markMessages(asRead: nil , dialogID: id , successBlock: { (response) in
            }) { (error) in
               
            }
        }
        ServicesManager.instance().chatService.updateUnreadCount(inMemory: self.dialogID())
        NotificationCenter.default.removeObserver(self)
        ServicesManager.instance().chatService.removeDelegate(self)
        ServicesManager.instance().chatService.chatAttachmentService.removeDelegate(self)
        QBChat.instance.removeDelegate(self)
        
        if constantVC.openPrivateChat.isActiveOpenPrivateChat {
            constantVC.openPrivateChat.isActiveOpenPrivateChat = false
            if let _ = self.isPresented {
                self.dismiss(animated: true, completion: nil)
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "tabBar")
                if constantVC.openPrivateChat.isActiveOpenChatWithActiveCall {
                    constantVC.openPrivateChat.isActiveOpenChatWithActiveCall = false
                    CallActiveInBackgroundManager.shared.setActiveViewToFront()
                }
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = initialViewController
                
            }
           
        } else{
            //Nitin
            if let _ = self.isPresented {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    @objc func didPressDotButton(sender: UIButton!) {
        self.dropDown.show()
    }
    
    func showPricePopup(price:String) {
        self.doctorCallingPriceViewObj = DoctorCallingPricePopupView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        let priceStr = "$"+price+"/"
        let pc = "per call".localized
        let longStr = priceStr + " " + pc
        let longestWordRange = (longStr as NSString).range(of: priceStr)

        let attributedString = NSMutableAttributedString(string: longStr, attributes: [NSAttributedStringKey.font : UIFont(name: "CircularStd-Book", size: 14.0)!])

        attributedString.setAttributes([NSAttributedStringKey.font : UIFont(name: "CircularStd-Book", size: 20.0)!], range: longestWordRange)

        // set attributed text on a UILabel
        self.doctorCallingPriceViewObj?.labelPrice.attributedText = attributedString
        self.doctorCallingPriceViewObj?.buttonStartConsultancy.tag = Int(price) ?? 0
        self.doctorCallingPriceViewObj?.buttonStartConsultancy.addTarget(self, action: #selector(self.buttonActionStartConsultancy(_:)), for: .touchUpInside)
        let str = "Start Consultancy".localized
        self.doctorCallingPriceViewObj?.buttonStartConsultancy.setTitle("\(str)\(" - $")\(price)", for: .normal)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapForPriceView(_:)))
        self.doctorCallingPriceViewObj?.addGestureRecognizer(tap)
        
        self.view.addSubview(self.doctorCallingPriceViewObj ?? UIView())

    }
    
    @objc func handleTapForPriceView(_ sender: UITapGestureRecognizer? = nil) {
        self.doctorCallingPriceViewObj?.removeFromSuperview()
        
    }
    
    @objc func buttonActionStartConsultancy(_ sender: UIButton) {
        self.doctorCallingPriceViewObj?.removeFromSuperview()

        let storyboard = UIStoryboard(name: "Health", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CardListingViewController")as! CardListingViewController
        vc.fee = sender.tag
        vc.completionBlock = { [weak self] data in
             guard let self = self else {return}
             if let value = data as? Bool, value {
                 constantVC.ActiveSession.isStartedConnectingCall = true
                 CallConnectionManager.instance.prepareCall_VIDEO(opponentNo: self.opponentNo , dialog: self.dialog , groupName: self.lblTitle?.text ?? "")
             }
         }
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func didPressVideoCall(sender: UIButton!) {
         if Connectivity.isConnectedToInternet() {
            if !constantVC.ActiveSession.isStartedConnectingCall {
                //Nitin
                if let data = self.dialog.data {
                    if let isDoctor = data["isDoctor"] as? Bool,isDoctor {
                        var fees = ""
                        if let fee = data["fees"] as? String {
                            fees = fee
                        } else if let fee = data["fees"] as? Int {
                            fees = String(fee)
                        }
                        self.showPricePopup(price: fees)
                        
                    } else {
                        constantVC.ActiveSession.isStartedConnectingCall = true
                        CallConnectionManager.instance.prepareCall_VIDEO(opponentNo: self.opponentNo , dialog: self.dialog , groupName: self.lblTitle?.text ?? "")
                    }
                }
              
            }
        }
         else{
           FTIndicator.showError(withMessage: "No Internet Connection!".localized)
        }
    }
    
    @objc func didPressVoiceCall(sender: UIButton!) {
         if Connectivity.isConnectedToInternet() {
            if !constantVC.ActiveSession.isStartedConnectingCall {
                //Nitin
                if let data = self.dialog.data {
                    if let isDoctor = data["isDoctor"] as? Bool,isDoctor {
                        var fees = ""
                        if let fee = data["fees"] as? String {
                            fees = fee
                        } else if let fee = data["fees"] as? Int {
                            fees = String(fee)
                        }
                        self.showPricePopup(price: fees)
                        
                    } else {
                        constantVC.ActiveSession.isStartedConnectingCall = true
                        CallConnectionManager.instance.prepareCall_AUDIO(opponentNo: self.opponentNo , dialog: self.dialog , groupName: "")
                    }
                }
            }
        }
         else{
           FTIndicator.showError(withMessage: "No Internet Connection!".localized)
        }
    }
    
    //MARK:- Input bar Button Actions
    func getBottomPaddingSafeArea() ->CGFloat{
        var bottomPaddingSafeArea:CGFloat = 0.0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            if let bottomPadding = window?.safeAreaInsets.bottom {
               bottomPaddingSafeArea = bottomPadding
            }
        }
        return bottomPaddingSafeArea
    }
    
    
    @objc func didPressAccessoryButton(sender: UIButton!) {
        view.endEditing(true)
        
        if CallConnectionManager.instance.isAuthorizedUser(dialog: self.dialog) {
            if self.stickerView?.isHidden == false{
                self.stickerView?.isHidden = true
                self.btnAddAccessory?.setImage(UIImage(named:"addBlue"), for: .normal)  //addBlue
            }
            if self.accessoryView?.isHidden == true{
                self.accessoryView?.isHidden = false
                self.btnAddAccessory?.setImage(UIImage(named:"addBlue"), for: .normal)  //addBlue
            }
            self.inputBarView_bottom?.constant = (accessoryView?.frame.size.height)! - self.getBottomPaddingSafeArea()
            view.layoutIfNeeded()
        } else {
            CallConnectionManager.instance.showUserBlockedInfo()
        }
    }
    @objc func didPressSmileyButton(sender: UIButton!) {
        view.endEditing(true)
        
        if CallConnectionManager.instance.isAuthorizedUser(dialog: self.dialog) {
            if accessoryView?.isHidden == false{
                self.accessoryView?.isHidden = true
            }
            if stickerView?.isHidden == true{
                self.stickerView?.isHidden = false
            }
            self.inputBarView_bottom?.constant = (stickerView?.frame.size.height)! - self.getBottomPaddingSafeArea()
            view.layoutIfNeeded()
        } else {
            CallConnectionManager.instance.showUserBlockedInfo()
        }
    }
    @objc func didPressMicOrSend(sender: UIButton!) {
        
        if CallConnectionManager.instance.isAuthorizedUser(dialog: self.dialog) {
            //mic
            if sender.tag == 1 {
                self.view.endEditing(true)
                
                //check microPhone permission
                AVAudioSession.sharedInstance().requestRecordPermission () {
                    [unowned self] allowed in
                    if allowed {
                        if self.baseInstance.isIphoneX() {
                            if #available(iOS 11.0, *) {
                                DispatchQueue.main.async {
                                    self.inputBarView_bottom?.constant = (self.recordingVw?.frame.size.height)! - (self.inputBarView?.frame.size.height)! - self.view.safeAreaInsets.bottom
                                }
                            } else {
                                // Fallback on earlier versions
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.inputBarView_bottom?.constant =  (self.recordingVw?.frame.size.height)! - (self.inputBarView?.frame.size.height)!
                            }
                        }
                        
                        DispatchQueue.main.async {
                            if self.accessoryView?.isHidden == false{
                                UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn , animations: {
                                    self.accessoryView?.isHidden = true
                                })
                            }
                            if self.stickerView?.isHidden == false{
                                UIView.transition(with: self.view, duration: 0.5, options: .curveEaseIn , animations: {
                                    self.stickerView?.isHidden = true
                                })
                            }
                            self.audioInit()
                            self.recordMinutes = 0
                            self.recordSeconds = 0
                            self.recordingVw?.isHidden = false
                            self.view.layoutIfNeeded()
                            self.scrollToBottom(animated: true)
                            self.startAudioRecording()
                        }
                    } else {
                        // User denied microphone. Tell them off!
                        let alert = UIAlertController(title: "Unable to access microphone", message: "Tamaaas does not have access to microphone. Kindly enable it in privacy settings ", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {  action in
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            
            //send
            if sender.tag == 2 {
                if self.msgTextView?.text.trimmingCharacters(in: .whitespaces) != "" {
                    self.didPressSend(sender, withMessageText: self.msgTextView?.text.trimmingCharacters(in: .whitespaces) ?? "" , senderId: self.senderID , senderDisplayName: self.senderDisplayName, date: Date())
                }
            }
        } else {
            CallConnectionManager.instance.showUserBlockedInfo()
        }
    }
    
    
    @objc func didPressChatInfo(sender: UIButton!) {
        if self.dialog.type == QBChatDialogType.private {
            //chat info
            self.showChatInfo()
        } else {
            //group info
            self.showGroupInfo()
        }
    }
    
    func audioInit(){
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        SessionManager.printLOG(data: "Allow")
                    } else {
                        SessionManager.printLOG(data: "Dont Allow")
                    }
                }
            }
        } catch {
            SessionManager.printLOG(data: "failed to record!")
        }
        
        // Audio Settings
        settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue
        ]
    }
    
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let soundURL = documentDirectory.appendingPathComponent("\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).aac")
        
        return soundURL as NSURL?
    }
    
    func startAudioRecording() {
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } catch _ {
        }
        
        do {
            self.audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,
                                                     settings: self.settings)
            self.audioRecorder.delegate = self
            self.audioRecorder.prepareToRecord()
        } catch {
            self.finishRecording(success: false)
        }
        do {
            try audioSession.setActive(true)
            self.recordMinutes = 0
            self.recordSeconds = 0
            self.pulsator.start()
            self.countdown()
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.countdown) , userInfo: nil, repeats: true)
            self.audioRecorder.record()
        } catch {
        }
        
    }
    
    func finishRecording(success: Bool) {
        
        if audioRecorder.isRecording == true {
            audioRecorder.stop()
            pulsator.stop()
        }
        
        //send audio message
        let attachment = QBChatAttachment.audioAttachment(withFileURL: audioRecorder.url)
        attachment.duration = lround(self.audioRecorder.currentTime)
        sendMessage(with: attachment)
        
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(false)
        } catch {
            // SessionManager.printLOG(data: "\(error)")
        }
    }
    
    @objc func countdown() {
        
        var seconds = "\(recordSeconds)"
        if recordSeconds < 10 {
            seconds = "0\(recordSeconds)"
        }
        var minutes = "\(recordMinutes)"
        if recordMinutes < 10 {
            minutes = "0\(recordMinutes)"
        }
        self.lblAudioTimer?.text = "\(minutes):\(seconds)"
        recordSeconds += 1
        
        if recordSeconds == 60 {
            recordMinutes += 1
            recordSeconds = 0
        }
    }
    
    //MARK:- Permission UIButton Actions
    @objc func didPressAddToContacts(sender: UIButton!) {
        self.saveNewContact(number: self.opponentNo)
    }
    
    @objc func didPressSkipForNow(sender: UIButton!) {
        QuickBloxManager().UnknownUserPopUp_UpdateDialog(status: "skip", dialog: self.dialog) { (success) in
            if success {
                self.vwPermission?.isHidden = true
                self.constraint_collectionViewTop?.constant = 0
            }
        }
    }
    @objc func didPressBlockPermission(sender: UIButton!) {
        self.showBlockAlert()
    }
    
    
    func showBlockAlert(){
        let alertController = UIAlertController(title: "Are you sure?".localized , message: "You want to block this user".localized, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            self.doBlockUnblock(state: "Block", completionHandler: { (success , msg) in
            })
            
            QuickBloxManager().UnknownUserPopUp_UpdateDialog(status: "block", dialog: self.dialog) { (success) in
                if success {
                    self.vwPermission?.isHidden = true
                    self.constraint_collectionViewTop?.constant = 0
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- View Reply UIButton Action
    @objc func didPress_replyMsg(sender: UIButton!) {
        //define dic to reply
        self.show_HideReplyView(isHidden: true)
        if let MsgToReply = self.chatDataSource.message(for: self.selectedIndexPath) {
            self.defineDicMsgToReply(MsgToReply: MsgToReply)
            self.showTextReplyContainer_toReply()
        }
    }
    
    @objc func didPress_copyMsg(sender: UIButton!) {
        guard let item = self.chatDataSource.message(for: self.selectedIndexPath) else {
            return
        }
        if let msgText = item.text {
            UIPasteboard.general.string = msgText
            self.show_HideReplyView(isHidden: true)
        }
        if let indexPath_ = self.selectedIndexPath {
            if let cell = self.collectionView!.cellForItem(at: indexPath_)  {
                cell.backgroundColor = UIColor.clear
            }
        }
    }
    
    @objc func didPress_deleteMsg(sender: UIButton!) {
        
        self.show_HideReplyView(isHidden: true)
        
        guard let item = self.chatDataSource.message(for: self.selectedIndexPath) else {
            return
        }
        
        let alertController = UIAlertController(title: "Are you sure?".localized , message: "You want to delete this message".localized, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            var selectedMsgs = Set<String>()
            selectedMsgs.insert(item.id!)
            QBRequest.deleteMessages(withIDs: selectedMsgs , forAllUsers: false , successBlock: { (success) in
                ServicesManager.instance().chatService.deleteMessageLocally(item)
                self.chatDataSource.delete(item)
            }, errorBlock: { (error) in
                
            })
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func didPress_forwardMsg(sender: UIButton!) {
        self.show_HideReplyView(isHidden: true)
        self.show_HideMsgForwardContainer(isHidden: false)
        if let indexPath_ = self.selectedIndexPath {
            if let msg = self.chatDataSource.message(for: indexPath_) {
                self.arrMsgsToForward.append(msg)
                self.updateSelectedMsgsCountText()
            }
        }
    }
    
    @objc func handleTap_BlurView(_ sender: UITapGestureRecognizer) {
        if let indexPath_ = self.selectedIndexPath {
            if let cell = self.collectionView!.cellForItem(at: indexPath_)  {
                cell.backgroundColor = UIColor.clear
            }
        }
        self.show_HideReplyView(isHidden: true)
    }
    
    func show_HideReplyView(isHidden:Bool){
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.vwBlur?.isHidden = isHidden
            self.vwReply?.isHidden = isHidden
        })
    }
    var viewToSwipe:UIView!
    var dicMsgToReply = NSMutableDictionary()
    var cellWidth:CGFloat = 0.0
    
    func defineDicMsgToReply(MsgToReply: QBChatMessage){
        
        if let id = MsgToReply.id {
            self.dicMsgToReply.setValue(id , forKey: MsgToReplyKeys.msg_id)
        }
        self.dicMsgToReply.setValue("\(MsgToReply.senderID)" , forKey: MsgToReplyKeys.senderID)
        if self.dialog.type == QBChatDialogType.private {
            self.dicMsgToReply.setValue(MsgToReply.senderID == self.senderID ? SessionManager.getPhone() : self.opponentNo , forKey: MsgToReplyKeys.senderNumber)
        }
        if self.dialog.type == QBChatDialogType.group {
            if let senderNo = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: MsgToReply.senderID)?.login {
                self.dicMsgToReply.setValue(senderNo , forKey: MsgToReplyKeys.senderNumber)
            } else { // no user in memory storage
                if let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: MsgToReply.senderID) {
                    if let senderNo = user.phone{
                        self.dicMsgToReply.setValue(senderNo , forKey: MsgToReplyKeys.senderNumber)
                    }
                }
            }
        }
        
        self.dicMsgToReply.setValue("\(self.senderID)" , forKey: MsgToReplyKeys.replyId)
        self.dicMsgToReply.setValue(SessionManager.getPhone() , forKey: MsgToReplyKeys.replyNumber)
        
        if let msgText = MsgToReply.text {
            self.dicMsgToReply.setValue(msgText , forKey: MsgToReplyKeys.msg)
        }
        if MsgToReply.isMediaMessage() {
            if MsgToReply.isImageAttachment() {
                self.dicMsgToReply.setValue(MsgToReplyKeys.IMAGE , forKey: MsgToReplyKeys.msgType)
            }
            else if MsgToReply.isVideoAttachment(){
                self.dicMsgToReply.setValue(MsgToReplyKeys.VIDEO , forKey: MsgToReplyKeys.msgType)
            }
            else if MsgToReply.isAudioAttachment(){
                self.dicMsgToReply.setValue(MsgToReplyKeys.AUDIO , forKey: MsgToReplyKeys.msgType)
            }
            else {
                self.dicMsgToReply.setValue(MsgToReplyKeys.CONTACT , forKey: MsgToReplyKeys.msgType)
            }
        } else {
            if (MsgToReply.text?.contains(constantVC.GeneralConstants.stickerKey))! {
                self.dicMsgToReply.setValue(MsgToReplyKeys.STICKER , forKey: MsgToReplyKeys.msgType)
            } else {
                self.dicMsgToReply.setValue(MsgToReplyKeys.TEXT , forKey: MsgToReplyKeys.msgType)
            }
        }
    }
    
    @objc private func onDrag(sender: UIPanGestureRecognizer) {
        
        let velocity = sender.velocity(in: self.view)
        
        if(velocity.x > 0) //swipe right
        {
            if self.viewToSwipe == nil {
                let p = sender.location(in: self.collectionView)
                if let indexPath = self.collectionView?.indexPathForItem(at: p) {
                    if let cell = self.collectionView?.cellForItem(at: indexPath) as? QMChatCell {
                        self.selectedIndexPath = indexPath
                        viewToSwipe = cell.containerView
                        if finalPosition == nil {
                            cellWidth = cell.containerView.frame.width
                            finalPosition = cell.containerView.frame.minX
                        }
                        
                        //define dic to reply
                        if let MsgToReply = self.chatDataSource.message(for: self.selectedIndexPath) {
                            self.defineDicMsgToReply(MsgToReply: MsgToReply)
                        }
                    }
                } else {
                    print("couldn't find index path")
                }
            }
            
            if self.finalPosition != nil {
                let view = self.view
                let screenWidth = UIScreen.main.bounds.width
                var dragDistance:CGFloat = 0.0
                
                if finalPosition <= 10.0 {
                    dragDistance = sender.translation(in: view).x
                    dragDistance = dragDistance >= cellWidth + cellWidth/2 ? cellWidth + cellWidth/2 : dragDistance
                } else {
                    dragDistance = sender.translation(in: view).x + self.finalPosition//(self.view.frame.size.width - self.finalPosition)
                    dragDistance = dragDistance >= self.view.frame.size.width ? self.finalPosition : dragDistance
                }
                
                if viewToSwipe != nil {
                    
                    switch sender.state {
                    case .changed:
                        viewToSwipe.frame.origin.x = dragDistance
                    case .ended:
                        UIView.animate(withDuration: 0.3, animations: {
                            self.viewToSwipe.frame.origin.x = self.finalPosition
                            self.finalPosition = nil
                            self.viewToSwipe = nil
                            
                            if velocity.x >= self.cellWidth + self.cellWidth/2 {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    self.showTextReplyContainer_toReply()
                                }
                            }
                        })
                    default: break
                    }
                }
            }
        }
    }
    
    
    func getReplyToImageThumbnail(msg: QBChatMessage , attachment: QBChatAttachment , targetSize: CGSize , completionHandler:@escaping (UIImage?) -> ()){
        
        //show thumb nail for img and video
        let transform = QMImageTransform(size: targetSize , isCircle: false)
        if attachment.attachmentType == QMAttachmentType.contentTypeImage {
            
            let url = attachment.remoteURL(withToken: false)
            
            if url == nil {
                if ((attachment.image) != nil) {
                    transform.apply(for: (attachment.image)!) { (img) in
                        completionHandler(img)
                    }
                }
            } else {
                let cachedImage: UIImage? = QMImageLoader.instance.imageCache?.imageFromCache(forKey: transform.key(with: url!))
                if cachedImage != nil {
                    transform.apply(for: (cachedImage)!) { (img) in
                        completionHandler(img)
                    }
                } else {
                    
                    QMImageLoader.instance.downloadImage(with: url! , transform: transform , options: [.highPriority , .continueInBackground , .allowInvalidSSLCertificates ], progress: { (receivedSize , expectedSize , targetURL) in
                        
                    }) { (image, transfomedImage , error , cacheType , finished , imageURL) in
                        if transfomedImage != nil {
                            completionHandler(transfomedImage)
                        }
                        if error != nil {
                            FTIndicator.showToastMessage("Error in downloading image")
                        }
                    }
                }
            }
        }
        
        //video
        if attachment.attachmentType == QMAttachmentType.contentTypeVideo {
            
            var msgID:String = ""
            
            if let id = msg.id {
                msgID = id
            }
            
            let image = QMImageLoader.instance.imageCache?.imageFromCache(forKey: msgID)
            if image != nil {
                transform.apply(for: image!) { (img) in
                    completionHandler(img)
                }
            }
            else {
                
                ServicesManager.instance().chatService.chatAttachmentService.prepare(attachment , message: msg) { (thumbnailImage , durationSeconds , size , error , cancelled) in
                    
                    if cancelled {
                        return
                    } else if error != nil {
                        FTIndicator.showToastMessage("Error in downloading video")
                    }else {
                        if let attchmnt = attachment as? QBChatAttachment{
                            attchmnt.image = thumbnailImage
                            attchmnt.duration = lround(durationSeconds)
                            attchmnt.width = lround(Double(size.width))
                            attchmnt.height = lround(Double(size.height))
                            msg.attachments = [attchmnt]
                            
                        ServicesManager.instance().chatService.messagesMemoryStorage.update(msg)
                            
                            if thumbnailImage != nil {
                                transform.apply(for: thumbnailImage!) { (img) in
                                    completionHandler(img)
                                }
                            QMImageLoader.instance.imageCache!.store(thumbnailImage!, forKey: msgID, completion: nil)
                            }
                        }
                    }
                }
            }}
    }
    
    
    func showTextReplyContainer_toReply(){
        guard let item = self.chatDataSource.message(for: self.selectedIndexPath) else {
            return
        }
        
        if let msgText = item.text {
            self.textMsgReplyContainer?.msgText.text = msgText
        }
     
        if item.isMediaMessage() && item.isImageAttachment() || item.isVideoAttachment() {
            self.textMsgReplyContainer?.imgVwThumbnail.isHidden = false
            if let attachment = item.attachments?.first {
                self.getReplyToImageThumbnail(msg: item , attachment: attachment, targetSize: (self.textMsgReplyContainer?.imgVwThumbnail.bounds.size)!) { (image) in
                    let imageData:Data = UIImagePNGRepresentation(image!)!
                    let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
                    self.dicMsgToReply.setValue(strBase64 , forKey: MsgToReplyKeys.thumbnail)
                    self.textMsgReplyContainer?.imgVwThumbnail.image = image
                }
            }
        } else {
            
            //sticker
            if (item.text?.contains(constantVC.GeneralConstants.stickerKey))! {
                self.textMsgReplyContainer?.imgVwThumbnail.isHidden = false
                self.textMsgReplyContainer?.msgText.text = "Sticker"
                let msgTextArr = item.text?.components(separatedBy: "*")
                if msgTextArr?.count == 2 {
                    if let stickerName = msgTextArr![1] as? String {
                        self.dicMsgToReply.setValue(stickerName , forKey: MsgToReplyKeys.stickerName)
                        self.textMsgReplyContainer?.imgVwThumbnail.image = UIImage(named: stickerName)
                    }
                }
            } else {
                self.textMsgReplyContainer?.imgVwThumbnail.isHidden = true
            }
        }
        
        if item.senderID == self.senderID {
            self.textMsgReplyContainer?.userName.text = "You"
        } else {
            if let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: item.senderID){
               
               if let phone = user.phone{
                    self.textMsgReplyContainer?.userName.text = SessionManager.getNameFromMyAddressBook(number: phone)
               }
               else{
                    if let user_ = ServicesManager.instance().usersService.getUserWithID(item.senderID) as? QBUUser {
                         if SessionManager.getNameFromMyAddressBook(number:  user_.phone!) != "" {
                              self.textMsgReplyContainer?.userName.text = SessionManager.getNameFromMyAddressBook(number: user_.phone!)
                         }
                    }
               }
            }
        }
       
        self.msgTextView?.becomeFirstResponder()
        self.openReplyViewFromBottom()
        self.constraintCollectionVw_bottom?.constant = 60
    }
    @objc func didPress_closeTextMsgReplyContainer(sender: UIButton!) {
        self.closeChatReplyView()
    }
    
    func closeChatReplyView(){
        self.textMsgReplyContainer?.isHidden = true
        self.constraintCollectionVw_bottom?.constant = 0
        self.dicMsgToReply = [:]
    }
    
    
    func openReplyViewFromBottom(){
        if self.constraintCollectionVw_bottom?.constant == 0.0 {
            self.textMsgReplyContainer?.animShow()
        }
    }
    
    func hideReplyViewFromBottom(){
        let transitionAnimator = UIViewPropertyAnimator(duration: 1, dampingRatio: 1, animations: {
            self.textMsgReplyContainer?.isHidden = true
            self.view.layoutIfNeeded()
        })
        transitionAnimator.addCompletion { position in
        }
        transitionAnimator.startAnimation()
    }
    
    //MARK:- Message Forward Module
    @objc func didPress_vwForward_BtnForward(sender: UIButton!) {
        self.openForwardListVC()
    }
    
    @objc func didPress_vwForward_BtnCancel(sender: UIButton!) {
        for msg in self.arrMsgsToForward {
            if let indexPath_ = self.chatDataSource.indexPath(for: msg) {
                if let cell = self.collectionView!.cellForItem(at: indexPath_)  {
                    cell.backgroundColor = UIColor.clear
                }
            }
        }
        self.arrMsgsToForward.removeAll()
        self.show_HideMsgForwardContainer(isHidden: true)
    }
    
    func show_HideMsgForwardContainer(isHidden:Bool){
        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.vwForwardMsg?.isHidden = isHidden
        })
        self.collectionView?.reloadData()
    }
    
    @objc func handleTap_collectionCell(_ sender: UITapGestureRecognizer) {
        if !(self.vwForwardMsg?.isHidden)! {
            if let tag = sender.view?.tag {
                let indexPath_ = IndexPath(row: tag , section: 0)
                
                if let msg = self.chatDataSource.message(for: indexPath_) {
                    if self.arrMsgsToForward.contains(msg) {
                        if let indexOfMsg = self.arrMsgsToForward.firstIndex(of: msg) {
                            self.arrMsgsToForward.remove(at: indexOfMsg)
                        }
                        if let cell = self.collectionView!.cellForItem(at: indexPath_)  {
                            cell.backgroundColor = UIColor.clear
                        }
                    } else {
                        self.arrMsgsToForward.append(msg)
                        if let cell = self.collectionView!.cellForItem(at: indexPath_)  {
                            cell.backgroundColor = UIColor.black.withAlphaComponent(0.2)
                        }
                    }
                    self.updateSelectedMsgsCountText()
                }
            }
        }
    }
   
    
    func updateSelectedMsgsCountText(){
        self.vwForward_lblForwardMsgCount?.text = "\(self.arrMsgsToForward.count) Selected"
    }
    
    func openForwardListVC(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ForwardList_TableViewController") as! ForwardList_TableViewController
        controller.arrSlectedMsgs = self.arrMsgsToForward
        self.arrMsgsToForward.removeAll()
        self.show_HideMsgForwardContainer(isHidden: true)
        self.present(controller , animated: true , completion: nil)
    }
    
    
    //MARK:- AVAudioRecorder Delegates
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    //MARK:- Accessory Button Actions
    @objc func didPressAlbumButton(sender: UIButton!) {
        self.view.endEditing(true)
        /*self.imagePickerViewController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        self.imagePickerViewController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(self.imagePickerViewController , animated: true , completion: nil)*/
        
        let imagePickerController = QBImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsMultipleSelection = true
        imagePickerController.maximumNumberOfSelection = 10
        imagePickerController.showsNumberOfSelectedAssets = true
        self.present(imagePickerController, animated: true)
        
    }
    @objc func didPressCameraButton(sender: UIButton!) {
        self.view.endEditing(true)
        self.imagePickerViewController.sourceType = UIImagePickerControllerSourceType.camera
     self.present(self.imagePickerViewController , animated: true){
         UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
     }
    }
    @objc func didPressContactButton(sender: UIButton!) {
        if Connectivity.isConnectedToInternet(){
            self.inputBarView_bottom?.constant =  0
            self.view.layoutIfNeeded()
            self.accessoryView?.isHidden = true
            
            SwiftMultiSelect.initialSelected = []
            SwiftMultiSelect.Show(to: self)
        }
        else {
            //OFFLINE
            FTIndicator.showToastMessage("No Internet Connection!")
        }
    }
    
    func joinDialog() {
        if self.dialog.type == QBChatDialogType.group {
            if !self.dialog.isJoined() {
                self.dialog.join(completionBlock: { (error) in
                    if error == nil {
                    } else {
                    }
                })
            }
        }
    }
    
    @objc func appMovedToForeGround() {
        if TamOfflineMessagesManager.shared.countOfUnsentMsgs() > 0 {
            self.send_unsentMessages(dialogID: self.dialogID())
        }
    }
    
    
    func send_unsentMessages(dialogID:String){
        if let ids = SessionManager.getUnSentMsgs_ids() {
            let arr = ids.components(separatedBy: ",")
            for id in arr {
                let NotSendMsg = ServicesManager.instance().chatService.messagesMemoryStorage.message(withID: id , fromDialogID: dialogID)
                if NotSendMsg != nil && Connectivity.isConnectedToInternet() {
                    self.sendMessage(message: NotSendMsg!)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.appMovedToForeGround), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        
        SwiftMultiSelect.dataSourceType = .phone
        SwiftMultiSelect.dataSource = self
        SwiftMultiSelect.delegate = self
        view.backgroundColor = UIColor.white
        self.vwPermission?.isHidden = true
        self.constraint_collectionViewTop?.constant = 0
        
        self.configureChatHeader()
        self.show_HideReplyView(isHidden: true)
        self.configInputBar()
        self.configureInputBar()
        self.configureTopBar()
        self.configureAccessoryView()
        self.configureStickerOptions()
        self.configureRecordingView()
        self.configureDotView()
        self.configureReplyView()
        self.configureForwardMessageView()
        self.loadImageMedia { (success) in
        }
        self.msgTextView?.resignFirstResponder()
        self.view.endEditing(true)
        self.onlineView?.layer.cornerRadius = (self.onlineView?.frame.size.width)! / 2
        self.onlineView?.clipsToBounds = true
        self.onlineView?.isHidden = true
        //for unknown user
        if self.dialog.type == QBChatDialogType.private && self.dialog.userID != UInt(truncating: SessionManager.get_QuickBloxID()!) {
            if !(SessionManager.checkUserSavedInPhoneBook(number: opponentNo)) {
                self.configurePermissionView()
            }
        }
    
        if self.dialog.type == QBChatDialogType.group {
            self.GetGroupInfo()
            self.onlineView?.isHidden = true
        } else {
            //get opponent User details
            if let opponentID = QuickBloxManager().getOpponentID(dialog: self.dialog) {
                QuickBloxManager().getUserInfo(userID: "\(opponentID)") { (user) in
                    if let id = user.externalUserID as? UInt {
                        self.opponentServerID = "\(id)"
                    }
                }
            }
        }
        
        
        if let currentUser:QBUUser = ServicesManager.instance().currentUser {
            
            self.senderID = currentUser.id
            self.senderDisplayName = currentUser.login!
            
            mediaController = QMMediaController(viewController: self)
            mediaController.onError = { message, error in
            }
            
            ServicesManager.instance().chatService.addDelegate(self)
            ServicesManager.instance().chatService.chatAttachmentService.addDelegate(self)
            ServicesManager.instance().chatService.chatAttachmentService.addDelegate(self.mediaController)
            QBChat.instance.addDelegate(self)
            self.updateTitle()
            self.updateView()
            // self.inputToolbar?.contentView?.backgroundColor = UIColor.white
            //  self.inputToolbar?.contentView?.textView?.placeHolder = "SA_STR_MESSAGE_PLACEHOLDER".localized
            
            self.attachmentCellsMap = NSMapTable(keyOptions: NSPointerFunctions.Options.strongMemory, valueOptions: NSPointerFunctions.Options.weakMemory)
            
            if self.dialog.type == QBChatDialogType.private {
                
                self.dialog.onUserIsTyping = {
                    [weak self] (userID)-> Void in
                    
                    if ServicesManager.instance().currentUser.id == userID {
                        return
                    }
                    self?.showTypingText()
                }
                
                self.dialog.onUserStoppedTyping = {
                    [weak self] (userID)-> Void in
                    
                    if ServicesManager.instance().currentUser.id == userID {
                        return
                    }
                    self?.hideTypingText()
                }
            }
            
            // Retrieving messages
            let messagesCount = self.storedMessages()?.count
            if (messagesCount == 0) {
               // self.startSpinProgress()
            } else if (self.chatDataSource.messagesCount() == 0) {
                self.chatDataSource.add(self.storedMessages()!)
            }
            
            self.loadMessages()
            self.enableTextCheckingTypes = NSTextCheckingAllTypes
        }
    }
   
    func showTypingText(){
        self.lblTyping?.isHidden = false
    }
    func hideTypingText(){
        self.lblTyping?.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.joinDialog()
        self.hideTypingText()
        
        if (self.accessoryView?.isHidden ?? true  && self.stickerView?.isHidden ?? true) {
            inputBarView_bottom?.constant = 0
            inputBarView_height?.constant = self.inputToolBarHeight
        }
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.animatedFrame?.layer.addSublayer(pulsator)
        pulsator.numPulse = 4
        pulsator.backgroundColor = UIColor.white.withAlphaComponent(0.6).cgColor
        pulsator.duration = 7000
        
        self.queueManager().add(self)
         UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
        self.willResignActiveBlock = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillResignActive, object: nil, queue: nil) { [weak self] (notification) in
            
            self?.fireSendStopTypingIfNecessary()
        }
    }
    
    func didTapEditButton(sender: AnyObject){
    }
    
    func didTapSearchButton(sender: AnyObject){
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Saving current dialog ID.
        ServicesManager.instance().currentDialogID = self.dialog.id!
          onlineTimer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(updateView), userInfo: nil, repeats: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if let willResignActive = self.willResignActiveBlock {
            NotificationCenter.default.removeObserver(willResignActive)
        }
        
        // Resetting current dialog ID.
        ServicesManager.instance().currentDialogID = ""
        onlineTimer.invalidate()
        // clearing typing status blocks
        self.dialog.clearTypingStatusBlocks()
        self.queueManager().remove(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.endEditing(true)
        self.msgTextView?.resignFirstResponder()
     
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        /* if let chatInfoViewController = segue.destination as? ChatUsersInfoTableViewController {
         chatInfoViewController.dialog = self.dialog
         }*/
    }
    
    // MARK: Update
    func updateTitle() {
        
        if self.dialog.type == QBChatDialogType.private {
            self.vwGroupImg?.isHidden = true
            self.userImg_topTitle?.isHidden = false
            if let data = dialog?.data {
                if let number = data["number"] as? String {
                    
                    if let dicCustomData = QuickBloxManager().convertJsonStringToDictionary(text: number) {
                        if let dicNo = dicCustomData["number"] as? String {
                            let arrNo = dicNo.components(separatedBy: "-")
                            for item in arrNo {
                                if item != SessionManager.getPhone() {
                                    opponentNo = item
                                    if let isDoc = data["isDoctor"]as? Bool, isDoc{
                                        let name = self.dialog?.name
                                        self.lblTitle?.text = name
                                    } else {
                                        self.lblTitle?.text = SessionManager.getNameFromMyAddressBook(number: opponentNo)
                                    }
                                
                                    self.userImg_topTitle?.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(self.opponentNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                                }
                            }
                        }
                    } else {
                        let arrNo = number.components(separatedBy: "-")
                        for item in arrNo {
                            if item != SessionManager.getPhone() {
                                opponentNo = item
                                if let isDoc = data["isDoctor"]as? Bool, isDoc{
                                    let name = self.dialog?.name
                                    self.lblTitle?.text = name
                                } else {
                                    self.lblTitle?.text = SessionManager.getNameFromMyAddressBook(number: opponentNo)
                                }
                                
                                self.userImg_topTitle?.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(self.opponentNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                            }
                        }
                    }
                }
            }
            else {
                
                if let occupantIDS = dialog?.occupantIDs {
                    let IDS = occupantIDS.filter { $0 != SessionManager.get_QuickBloxID() }
                    let occupantID = IDS[0]
                    
                    ServicesManager.instance().usersService.getUserWithID( occupantID as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                        
                        if let no = task.result?.phone {
                            self.opponentNo = no
                        }
                        
                        self.lblTitle?.text = SessionManager.getNameFromMyAddressBook(number:  self.opponentNo)
                        self.userImg_topTitle?.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(self.opponentNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                        
                        return nil
                    })
                }
            }
        }
        else {
            self.lblTitle?.text = self.dialog?.name
            self.showGroupImg()
        }
    }
     @objc func updateView() {
          if self.dialog?.type == QBChatDialogType.group {
               self.onlineView?.isHidden = true
          }
          else {
            if let occupantIDS = dialog?.occupantIDs {
                let IDS = occupantIDS.filter { $0 != SessionManager.get_QuickBloxID() }
                self.occupantID = IDS[0]
                QBRequest.user(withID: self.occupantID as! UInt, successBlock: { (response , user) in
                    let currentTimeInterval = Int(Date().timeIntervalSince1970)
                    var userLastRequestAtTimeInterval = Int()
                    if let lastRequest = user.lastRequestAt {
                        userLastRequestAtTimeInterval = Int((lastRequest.timeIntervalSince1970))
                    }
                     
                    if((currentTimeInterval - userLastRequestAtTimeInterval) > 90){
                        // user is offline now
                        self.onlineView?.isHidden = true
                    } else {
                        // user is online now
                        self.onlineView?.isHidden = false
                    }
                }) { (response) in
                }
           }
     }
     }
    
    func showGroupImg(){
        if let photo = self.dialog?.photo {
            self.vwGroupImg?.isHidden = true
            self.userImg_topTitle?.isHidden = false
            self.userImg_topTitle?.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photo).jpeg") , placeholder: nil , options: [.progressiveBlur , .setImageWithFadeAnimation ] ) { (image , url , type , stage , error) in
            }
        } else {
            self.vwGroupImg?.isHidden = false
            self.userImg_topTitle?.isHidden = true
            self.showGroupUsersImages()
        }
    }
    
    func showGroupUsersImages(){
        if let groupOccupantsID = self.dialog?.occupantIDs {
            let IDS = groupOccupantsID.filter { $0 != SessionManager.get_QuickBloxID() }
            
            if IDS.indices.contains(0) {
                
                //user1
                ServicesManager.instance().usersService.getUserWithID( IDS[0] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                    if let no = task.result?.phone {
                        self.usergroupImg1?.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: { (image , url , type , stage , error) in
                            if error == nil && image != nil {
                                CommonFunctions.shared.addBorder(imageView: (self.usergroupImg1)! , colour: UIColor.white)
                            } else {
                                CommonFunctions.shared.addBorder(imageView: (self.usergroupImg1)! , colour: UIColor(red: 145.0/255.0, green: 159.0/255.0, blue: 185.0/255.0, alpha: 1.0))
                            }
                        })
                    }
                    return nil
                })
            }
            
            if IDS.indices.contains(1) {
                //user2
                ServicesManager.instance().usersService.getUserWithID( IDS[1] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                    
                    if let no = task.result?.phone {
                        self.usergroupImg2?.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: { (image , url , type , stage , error) in
                            if error == nil && image != nil {
                                CommonFunctions.shared.addBorder(imageView: (self.usergroupImg2)! , colour: UIColor.white)
                            }else {
                                CommonFunctions.shared.addBorder(imageView: (self.usergroupImg2)! , colour: UIColor(red: 145.0/255.0, green: 159.0/255.0, blue: 185.0/255.0, alpha: 1.0))
                            }
                        })
                    }
                    return nil
                })
            }
        }
    }
    
    
    func storedMessages() -> [QBChatMessage]? {
        return ServicesManager.instance().chatService.messagesMemoryStorage.messages(withDialogID: self.dialog.id!)
    }
    
    func loadMessages() {
        // Retrieving messages for chat dialog ID.
        guard let currentDialogID = self.dialog.id else {
            SVProgressHUD.dismiss()
            return
        }
        print(currentDialogID)
        ServicesManager.instance().chatService.messages(withChatDialogID: currentDialogID, completion: {
            [weak self] (response, messages) -> Void in
            guard let strongSelf = self else { return }
            guard response.error == nil else {
                //SVProgressHUD.showError(withStatus: response.error?.error?.localizedDescription)
               return
            }
            if messages?.count ?? 0 > 0 {
                if !(self?.progressView?.isHidden)! {
                    self?.stopSpinProgress()
                }
                strongSelf.chatDataSource.add(messages)
            } else {
                self?.showChatInnerView()
            }
            SVProgressHUD.dismiss()
            self?.isHeaderLoaded = true
            strongSelf.collectionView?.collectionViewLayout.invalidateLayout()
        })
    }
        
    func showChatInnerView() {
        
        if let data = self.dialog.data {
            if self.collectionView?.numberOfItems(inSection: 0) ?? 0 == 0 {
                if let isDoctor = data["isDoctor"] as? Bool,isDoctor {
                    self.chatInnerViewObj = ChatInnerView(frame: CGRect(x: 15, y: 150, width: self.view.frame.width-30, height: 400))
                    self.chatInnerViewObj?.showAttributedMsg()
                    self.view.addSubview(self.chatInnerViewObj ?? UIView())
                }
            }
        }
    }
    
    func sendReadStatusForMessage(message: QBChatMessage) {
        
        guard QBSession.current.currentUser != nil else {
            return
        }
        guard message.senderID != QBSession.current.currentUser?.id else {
            return
        }
        
        if self.messageShouldBeRead(message: message) {
            
            QBChat.instance.read(message) { (error) in
            }
            ServicesManager.instance().chatService.read(message, completion: { (error) -> Void in
                
                guard error == nil else {
                    SessionManager.printLOG(data: "Problems while marking message as read! Error:")
                    return
                }
                if UIApplication.shared.applicationIconBadgeNumber > 0 {
                    let badgeNumber = UIApplication.shared.applicationIconBadgeNumber
                    UIApplication.shared.applicationIconBadgeNumber = badgeNumber - 1
                }
            })
        }
    }
    
    func messageShouldBeRead(message: QBChatMessage) -> Bool {
        
        let currentUserID = NSNumber(value: QBSession.current.currentUser!.id as UInt)
        
        return !message.isDateDividerMessage
            && message.senderID != self.senderID
            && !(message.readIDs?.contains(currentUserID))!
    }
    
    func readMessages(messages: [QBChatMessage]) {
        
        if QBChat.instance.isConnected {
            
            for msg in messages {
                QBChat.instance.read(msg) { (error) in
                }
            }
            ServicesManager.instance().chatService.read(messages, forDialogID: self.dialog.id!, completion: nil)
        } else {
            self.unreadMessages = messages
        }
        
        var messageIDs = [String]()
        
        for message in messages {
            messageIDs.append(message.id!)
        }
    }
    
    
    //MARK:- Video Compression Methods
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            return
        }
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    //MARK: Actions
    
    override func didPickAttachmentVideoUrl(_ url: URL) {
        
        let attachment = QBChatAttachment.videoAttachment(withFileURL: url)
        
        DispatchQueue.global(qos: .default).async(execute: {
            
            self.getThumbnailImage(forUrl: url, completion: {(isSaved , image) in
                
                if (isSaved!) {
                    DispatchQueue.main.async {
                        attachment.image = image
                    }
                }
            })
            DispatchQueue.main.async(execute: {
                self.sendMessage(with: attachment)
            })
        })
    }
    
    func getThumbnailImage(forUrl url: URL, completion: @escaping ((_ isSaved: Bool? , _ thumbnailImg: UIImage?) -> Void)) -> Void {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: time , actualTime: nil)
            completion(true , UIImage(cgImage: thumbnailImage))
        } catch let _ {
            completion(false , nil)
        }
    }
    
    func sendMessage(with attachment: QBChatAttachment?) {
        
        let messageText = "\(attachment?.type?.capitalized ?? "") attachment"
        
        let message = QBChatMessage()
        message.text = messageText
        message.senderID = senderID
        message.markable = true
        message.deliveredIDs = [NSNumber(value: senderID)]
        message.readIDs = [NSNumber(value: senderID)]
        message.dialogID = self.dialog.id
        message.dateSent = Date()
        if (attachment != nil) {
            message.attachments = [attachment!]
        }
        
        //to reply msg
        if self.dicMsgToReply.allKeys.count > 1 {
            let dicReply = NSMutableDictionary()
            dicReply.setValue(QuickBloxManager().convertDictionaryToJsonString(dict: self.dicMsgToReply) , forKey: MsgToReplyKeys.replyMsg)
            message.customParameters = dicReply
            self.closeChatReplyView()
        }
        
        ServicesManager.instance().chatService.sendAttachmentMessage(message , to: self.dialog , with: attachment! ) { (error) in
            if error == nil {
                //Send Push Notification
                self.sendPushNotification(msg: messageText)
            }
        }
        if automaticallyScrollsToMostRecentMessage {
            scrollToBottom(animated: true)
        }
    }
    
    func sendMessage_image_Video(with attachment: QBChatAttachment? , caption: String) {
        
        let messageText = "\(attachment?.type?.capitalized ?? "") attachment"
        
        let message = QBChatMessage()
        
        if caption.count > 0 {
            message.text = caption
        } else {
            message.text = messageText
        }
        message.senderID = senderID
        message.markable = true
        message.deliveredIDs = [NSNumber(value: senderID)]
        message.readIDs = [NSNumber(value: senderID)]
        message.dialogID = self.dialog.id
        message.dateSent = Date()
        if (attachment != nil) {
            message.attachments = [attachment!]
        }
        
        //to reply msg
        if self.dicMsgToReply.allKeys.count > 1 {
            let dicReply = NSMutableDictionary()
            dicReply.setValue(QuickBloxManager().convertDictionaryToJsonString(dict: self.dicMsgToReply) , forKey: MsgToReplyKeys.replyMsg)
            message.customParameters = dicReply
            self.closeChatReplyView()
        }
        
        ServicesManager.instance().chatService.sendAttachmentMessage(message , to: self.dialog , with: attachment! ) { (error) in
            if error == nil {
                //Send Push Notification
                self.sendPushNotification(msg: messageText)
            }
        }
        if automaticallyScrollsToMostRecentMessage {
            scrollToBottom(animated: true)
        }
    }
   
     func resizedImage(from image: UIImage?) -> UIImage? {
          
          let largestSide: CGFloat? = (image?.size.width ?? 0.0) > (image?.size.height ?? 0.0) ? image?.size.width : image?.size.height
          let scaleCoefficient: CGFloat = (largestSide ?? 0.0) / 800.0
          let newSize = CGSize(width: (image?.size.width ?? 0.0) / scaleCoefficient, height: (image?.size.height ?? 0.0) / scaleCoefficient)
          
          UIGraphicsBeginImageContext(newSize)
          
          image?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
          let resizedImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
          
          UIGraphicsEndImageContext()
          
          return resizedImage
     }
    
    func sendAttachmentMessage(with image: UIImage) {
          DispatchQueue.global(qos: .default).async(execute: {
               let resizedImage: UIImage? = self.resizedImage(from: image)
               // Sending attachment to dialog.
               DispatchQueue.main.async(execute: {
                    let attachment = QBChatAttachment.imageAttachment(with: resizedImage!)
                    self.sendMessage(with: attachment)
               })
          })
     }
    
    override func didPickAttachmentImage(_ image: UIImage) {
     
        DispatchQueue.global(qos: .default).async(execute: {
            var newImage : UIImage! = image
            if self.imagePickerViewController.sourceType == UIImagePickerControllerSourceType.camera {
                newImage = newImage.fixOrientation()
            }
            
            DispatchQueue.main.async(execute: {
                self.sendAttachmentMessage(with: newImage)
            })
        })
    }
    
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: UInt, senderDisplayName: String, date: Date) {
        
//        if !self.queueManager().shouldSendMessagesInDialog(withID: self.dialog.id!) {
//            return
//        }
        
        self.fireSendStopTypingIfNecessary()
        
        //Means First Message // Nitin
        if let data = self.dialog.data {
            if let isDoctor = data["isDoctor"] as? Bool,isDoctor  {
                let message = QBChatMessage()
                var textMsg = ""
                let defaults = UserDefaults.standard
                
                if let symMsg = self.symptomsMessage {
                    
                    if let movingFor = self.movingFor {

                        var patientName = ""
                        var patientAge = ""
                        var patientGender = ""
                        if movingFor == "self" {
                            patientName = SessionManager.getUsername()
                            if let age = defaults.value(forKey: "selfAge") as? String {
                                patientAge = "Age " + age
                            }
                            if let gender = defaults.value(forKey: "selector") as? Int {
                                if gender == 1 {
                                     patientGender = "Male"
                                } else {
                                     patientGender = "Female"
                                }
                            }
                            textMsg =  "\(text)\n \n\(patientName)\(". ")\(patientAge)\(", ")\(patientGender) selected the following symptoms:- \n \n \(symMsg)"
                        } else if movingFor == "child" {
                            if let name = defaults.value(forKey: "childName") as? String {
                                patientName = name
                            }
                            if let age = defaults.value(forKey: "childAge") as? String {
                                patientAge = "Age " + age
                            }
                            if let gender = defaults.value(forKey: "childGender") as? Bool {
                                if gender {
                                     patientGender = "Male"
                                } else {
                                     patientGender = "Female"
                                }
                            }
                            textMsg =  "\(text)\n \n\(patientName)\(". ")\(patientAge)\(", ")\(patientGender) selected the following symptoms:- \n \n \(symMsg)"

                        } else if movingFor == "parents" {
                            if let name = defaults.value(forKey: "childName") as? String {
                                patientName = name
                            }
                            if let age = defaults.value(forKey: "parentsAge") as? String {
                                patientAge = "Age " + age
                            }
                            if let gender = defaults.value(forKey: "parentGender") as? Bool {
                                if gender {
                                     patientGender = "Male"
                                } else {
                                     patientGender = "Female"
                                }
                            }
                            textMsg =  "\(text)\n \n\(patientName)\(". ")\(patientAge)\(", ")\(patientGender) selected the following symptoms:- \n \n \(symMsg)"

                        }
                    }
                    
                    self.symptomsMessage = nil
                } else {
                    textMsg = text
                }
                
                message.text = textMsg
                message.senderID = self.senderID
                message.deliveredIDs = [(NSNumber(value: self.senderID))]
                message.readIDs = [(NSNumber(value: self.senderID))]
                message.markable = true
                message.dateSent = date
                
                //to reply msg
                if self.dicMsgToReply.allKeys.count > 1 {
                    let dicReply = NSMutableDictionary()
                    dicReply.setValue(QuickBloxManager().convertDictionaryToJsonString(dict: self.dicMsgToReply) , forKey: MsgToReplyKeys.replyMsg)
                    message.customParameters = dicReply
                    self.closeChatReplyView()
                }
                self.sendMessage(message: message)
            } else {
                
                let message = QBChatMessage()
                message.text = text
                message.senderID = self.senderID
                message.deliveredIDs = [(NSNumber(value: self.senderID))]
                message.readIDs = [(NSNumber(value: self.senderID))]
                message.markable = true
                message.dateSent = date
                
                //to reply msg
                if self.dicMsgToReply.allKeys.count > 1 {
                    let dicReply = NSMutableDictionary()
                    dicReply.setValue(QuickBloxManager().convertDictionaryToJsonString(dict: self.dicMsgToReply) , forKey: MsgToReplyKeys.replyMsg)
                    message.customParameters = dicReply
                    self.closeChatReplyView()
                }
                self.sendMessage(message: message)
                
            }
        }
        
    }
    
    override func didPressSend(_ button: UIButton!, withTextAttachments textAttachments: [Any], senderId: UInt, senderDisplayName: String!, date: Date) {
        
        if let attachment = textAttachments.first as? NSTextAttachment {
            
            if (attachment.image != nil) {
                let message = QBChatMessage()
                message.senderID = self.senderID
                message.dialogID = self.dialog.id
                message.dateSent = Date()
                ServicesManager.instance().chatService.sendAttachmentMessage(message, to: self.dialog, withAttachmentImage: attachment.image!, completion: {
                    [weak self] (error: Error?) -> Void in
                    
                    self?.attachmentCellsMap.removeObject(forKey: message.id as AnyObject?)
                    
                    guard error != nil else { return }
                    
                    // perform local attachment message deleting if error
                    ServicesManager.instance().chatService.deleteMessageLocally(message)
                    self?.chatDataSource.delete(message)
                })
                self.finishSendingMessage(animated: true)
            }
        }
    }
    
    func sendMessage(message: QBChatMessage) {
        
        // Sending message.
        // Logging in to chat.
        if QBChat.instance.isConnected {
            print("CHAT ACTIVE")
        } else {
            // Logging in to chat.
            ServicesManager.instance().chatService.connect { (error) in
            }
        }
        
        ServicesManager.instance().chatService.send(message, toDialogID: self.dialog.id!, saveToHistory: true, saveToStorage: true) { (error) ->
            Void in
            
            if error != nil {
                
                if !Connectivity.isConnectedToInternet() {
                    QMMessageNotificationManager.showNotification(withTitle: "SA_STR_ERROR".localized, subtitle: "Message failed to send. Check your connection".localized , type: QMMessageNotificationType.warning)
                }
                
                // Logging in to chat.
                ServicesManager.instance().chatService.connect { (error) in
                   
                }
            }
            
            //Send Push Notification
            self.sendPushNotification(msg: message.text!)
        }
        self.msgTextView?.text = ""
        self.inputBarView_height?.constant = self.inputToolBarHeight
        self.showMicHideSendButton()
        self.finishSendingMessage(animated: true)
    }
    
    func sendPushNotification(msg: String){
        if self.dialog.type == QBChatDialogType.group {
            if let opponentIDs = QuickBloxManager().getOpponentsID(dialog: self.dialog) {
                var arrStrIDS = [String]()
                
                for id in opponentIDs {
                    arrStrIDS.append("\(id)")
                }
                
                TamFirebaseManager.shared.getGroupMutedUsersId(collection: firebaseCollection.MuteGroup.rawValue , doc: self.dialogID()) { (success , ids) in
                    if success {
                        if ids != "" {
                            let arrMuteIds = ids.components(separatedBy: ",")
                            
                            //get ids not muted
                            let newArrayID  = arrStrIDS.filter { (string) -> Bool in
                                return !arrMuteIds.contains(string)
                            }
                            
                            let strIDS = newArrayID.joined(separator: ",")
                         
                            //silent push
                            // QuickBloxManager().sendPushNotification_chatSilent(msg: "SILENT" , users: strIDS , title: "SILENT", fromNo: SessionManager.getPhone() , toNo: "", dialogID: self.dialogID())
                            
                            //send push notification
                            QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: "" , dialogID: self.dialogID())
                            
                        } else {
                            let strIDS = arrStrIDS.joined(separator: ",")
                            
                            //silent push
                            //    QuickBloxManager().sendPushNotification_chatSilent(msg: "SILENT" , users: strIDS , title: "SILENT", fromNo: SessionManager.getPhone() , toNo: "", dialogID: self.dialogID())
                            
                            //send push notification
                            QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: "" , dialogID: self.dialogID())
                        }
                    }
                    else {
                        //no muted
                        let strIDS = arrStrIDS.joined(separator: ",")
                        
                        //silent push
                        //QuickBloxManager().sendPushNotification_chatSilent(msg: "SILENT" , users: strIDS , title: "SILENT", fromNo: SessionManager.getPhone() , toNo: "", dialogID: self.dialogID())
                        
                        //send push notification
                        QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: "" , dialogID: self.dialogID())
                    }
                }
            }
        } else {
            
            let document = opponentNo + "-" + SessionManager.getPhone()
            TamFirebaseManager.shared.getDocument(collection: firebaseCollection.Mute.rawValue , document: document) { (success) in
                if !(success) {
                    //Not blocked - send custom notification
                    if let opponentID = QuickBloxManager().getOpponentID(dialog: self.dialog) {
                        
                        //silent push
                        // QuickBloxManager().sendPushNotification_chatSilent(msg: "SILENT" , users: "\(opponentID)" , title: "SILENT", fromNo: SessionManager.getPhone() , toNo: "", dialogID: self.dialogID())
                        
                        QuickBloxManager().sendPushNotification_chat(msg: msg , users: "\(opponentID)" , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: self.opponentNo , dialogID: self.dialogID())
                    }
                }
            }
        }
    }
    
    // MARK: Helper
    func canMakeACall() -> Bool {
        var canMakeACall = false
        
        if (UIApplication.shared.canOpenURL(URL.init(string: "tel://")!)) {
            
            // Check if iOS Device supports phone calls
            let networkInfo = CTTelephonyNetworkInfo()
            let carrier = networkInfo.subscriberCellularProvider
            if carrier == nil {
                return false
            }
            let mnc = carrier?.mobileNetworkCode
            if mnc?.length == 0 {
                // Device cannot place a call at this time.  SIM might be removed.
            }
            else {
                // iOS Device is capable for making calls
                canMakeACall = true
            }
        }
        else {
            // iOS Device is not capable for making calls
        }
        
        return canMakeACall
    }
    
    func placeHolderTextView(_ textView: QMPlaceHolderTextView, shouldPasteWithSender sender: Any) -> Bool {
        
        if UIPasteboard.general.image != nil {
            
            let textAttachment = NSTextAttachment()
            textAttachment.image = UIPasteboard.general.image!
            textAttachment.bounds = CGRect(x: 0, y: 0, width: 100, height: 100)
            
            _ = NSAttributedString.init(attachment: textAttachment)
            //  self.inputToolbar?.contentView.textView.attributedText = attrStringWithImage
            // self.textViewDidChange((self.inputToolbar?.contentView.textView)!)
            
            return false
        }
        
        return true
    }
    
    func showCharactersNumberError() {
        let title  = "SA_STR_ERROR".localized;
        let subtitle = String(format: "The character limit is %lu.", maxCharactersNumber)
        QMMessageNotificationManager.showNotification(withTitle: title, subtitle: subtitle, type: .error)
    }
    
    /**
     Builds a string
     Read: login1, login2, login3
     Delivered: login1, login3, @12345
     
     If user does not exist in usersMemoryStorage, then ID will be used instead of login
     
     - parameter message: QBChatMessage instance
     
     - returns: status string
     */
    func statusStringFromMessage(message: QBChatMessage) -> String {
        
        var statusString = ""
        
        let currentUserID = NSNumber(value:self.senderID)
        
        var readLogins: [String] = []
        
        if message.readIDs != nil {
            
            let messageReadIDs = message.readIDs!.filter { (element) -> Bool in
                
                return !element.isEqual(to: currentUserID)
            }
            
            if !messageReadIDs.isEmpty {
                /*for readID in messageReadIDs {
                 let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(truncating: readID))
                 
                 guard let unwrappedUser = user else {
                 let unknownUserLogin = "@\(readID)"
                 readLogins.append(unknownUserLogin)
                 continue
                 }
                 
                 if SessionManager.getNameFromMyAddressBook(number:  unwrappedUser.login!) != "" {
                 readLogins.append(SessionManager.getNameFromMyAddressBook(number:  unwrappedUser.login!))
                 }else {
                 readLogins.append("+" + unwrappedUser.login!)
                 }
                 }*/
                
                if self.dialog.type == QBChatDialogType.group {
                    //statusString += ": " + readLogins.joined(separator: ", ")
                    statusString += "SA_STR_SENT_STATUS".localized
                } else {
                    statusString += message.isMediaMessage() ? "SA_STR_SEEN_STATUS".localized : "SA_STR_READ_STATUS".localized;
                }
            }
        }
        
        if message.deliveredIDs != nil {
            let deliveredLogins: [String] = []
            
            let messageDeliveredIDs = message.deliveredIDs!.filter { (element) -> Bool in
                return !element.isEqual(to: currentUserID)
            }
            
            if !messageDeliveredIDs.isEmpty {
                /*  for deliveredID in messageDeliveredIDs {
                 let user = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: UInt(truncating: deliveredID))
                 
                 guard let unwrappedUser = user else {
                 let unknownUserLogin = "@\(deliveredID)"
                 deliveredLogins.append(unknownUserLogin)
                 
                 continue
                 }
                 
                 if readLogins.contains(unwrappedUser.login!) {
                 continue
                 }
                 
                 if SessionManager.getNameFromMyAddressBook(number:  unwrappedUser.login!) != "" {
                 deliveredLogins.append(SessionManager.getNameFromMyAddressBook(number:  unwrappedUser.login!))
                 }else {
                 deliveredLogins.append("+" + unwrappedUser.login!)
                 }
                 
                 }*/
                
                if deliveredLogins.count > 0 {
                    
                    /*if self.dialog.type == QBChatDialogType.group {
                     
                     if readLogins.count > 0 && deliveredLogins.count > 0 {
                     statusString += "\n"
                     }
                     
                     statusString += "SA_STR_DELIVERED_STATUS".localized + ": " + deliveredLogins.joined(separator: ", ")
                     }*/
                    
                    if message.readIDs == nil {  //show only when no read
                        if self.dialog.type == QBChatDialogType.group {
                            statusString += "SA_STR_SENT_STATUS".localized
                            // statusString += "SA_STR_DELIVERED_STATUS".localized + ": " + deliveredLogins.joined(separator: ", ")
                        } else {
                            statusString += "SA_STR_DELIVERED_STATUS".localized
                        }
                    }
                }
            }
        }
        
        if statusString.isEmpty {
            
            let messageStatus: QMMessageStatus = self.queueManager().status(for: message)
            
            switch messageStatus {
            case .sent:
                statusString = "SA_STR_SENT_STATUS".localized
            case .sending:
                statusString = "SA_STR_SENDING_STATUS".localized
            case .notSent:
                statusString = "SA_STR_NOT_SENT_STATUS".localized
            }
        }
        
        return statusString
    }
    
    //MARK: Override
    
    override func viewClass(forItem item: QBChatMessage) -> AnyClass {
        // TODO: check and add QMMessageType.AcceptContactRequest, QMMessageType.RejectContactRequest, QMMessageType.ContactRequest
        
        if let customParam = item.customParameters {
            if let dialogUpdate = customParam.object(forKey: "dialog_update_info") as? String {
                if dialogUpdate == "1" || dialogUpdate == "2" || dialogUpdate == "3"{
                    return QMChatNotificationCell.self
                }
            }
        }
        
        if item.isNotificationMessage() || item.isDateDividerMessage {
            return QMChatNotificationCell.self
        }
        
        if (item.senderID != self.senderID) {
            
            if (item.isMediaMessage() && item.attachmentStatus != QMMessageAttachmentStatus.error) {
                
                if item.isVideoAttachment() {
                    return QMVideoIncomingCell.self
                } else if item.isAudioAttachment() {
                    return QMAudioIncomingCell.self
                } else if item.isImageAttachment() {
                    return QMImageIncomingCell.self
                }else if (item.text?.contains(constantVC.GeneralConstants.contactKey))! {
                    return TamContactIncomingCell.self
                } else {
                    return QMChatAttachmentIncomingCell.self
                }
            }
            else if (item.text! == "Call notification") {
                return QMChatNotificationCell.self
            }
            else if (item.text?.contains(constantVC.GeneralConstants.stickerKey))! {
                return QMImageIncomingCell.self
            }
            else {
                if QMReplyMsgManager.shared.isContainReply(message: item) {
                    return QMChatReplyIncomingCell.self
                } else {
                    return QMChatIncomingCell.self
                }
            }
        }
        else {
            if let itemText =  item.text{
                if (item.isMediaMessage() && item.attachmentStatus != QMMessageAttachmentStatus.error) {
                    
                    if item.isVideoAttachment() {
                        return QMVideoOutgoingCell.self
                    } else if item.isAudioAttachment() {
                        return QMAudioOutgoingCell.self
                    } else if item.isImageAttachment() {
                        return QMImageOutgoingCell.self
                    } else if (item.text?.contains(constantVC.GeneralConstants.contactKey))! {
                        return TamContactOutgoingCell.self
                    }
                    else {
                        return QMChatAttachmentOutgoingCell.self
                    }
                }
                else if (itemText == "Call notification") {
                    return QMChatNotificationCell.self
                }
                else if (itemText.contains(constantVC.GeneralConstants.stickerKey)) {
                    return QMImageOutgoingCell.self
                }
                else {
                    if QMReplyMsgManager.shared.isContainReply(message: item) {
                        return QMChatReplyOutgoingCell.self
                    } else {
                        return QMChatOutgoingCell.self
                    }
                }
                
            } else {
                if QMReplyMsgManager.shared.isContainReply(message: item) {
                    return QMChatReplyOutgoingCell.self
                } else {
                    return QMChatOutgoingCell.self
                }
            }
        }
    }
    
    //MARK: Strings builder
    override func attributedString(forItem messageItem: QBChatMessage!) -> NSAttributedString? {
        
        var message: String? = nil
        var iconImage: UIImage? = nil
        let paragraphStyle = NSMutableParagraphStyle()
        var font: UIFont? = nil
        var notiMsg:String = ""
        var dialogUpdate: String = ""
        
        
        guard messageItem.text != nil else {
            return nil
        }
        
        var attributes = Dictionary<NSAttributedStringKey, AnyObject>()
        
       var textColor = messageItem.senderID == self.senderID ? UIColor.white : UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 1.0)
       attributes[NSAttributedStringKey.font] = UIFont(name: "CircularStd-Book", size: 16)
     
        
        if let customParam = messageItem.customParameters {
            if let dialogUpdate_ = customParam.object(forKey: "dialog_update_info") as? String {
                dialogUpdate = dialogUpdate_
            }
        }
        
        
        if messageItem.isNotificationMessage() || messageItem.isDateDividerMessage || dialogUpdate == "1" || dialogUpdate == "2" || dialogUpdate == "3" {
            textColor = UIColor.darkGray
            attributes[NSAttributedStringKey.font] = UIFont(name: "CircularStd-Medium", size: 15)
        }
        if let msg = messageItem.text {
            notiMsg = msg
        }
        
        //for group
        if messageItem.isNotificationMessage() && self.dialog.type == QBChatDialogType.group {
            
            
            //add group member
            if notiMsg.contains("created new group") {
                
                if messageItem.senderID == self.senderID {
                    notiMsg = "You created new group"
                } else {
                    if let no = QuickBloxManager().getNoFromMessage(message: notiMsg , replaceText: " created new group") as? String {
                        if SessionManager.getNameFromMyAddressBook(number: no) != "" {
                            notiMsg = SessionManager.getNameFromMyAddressBook(number: no) + " " + "created new group"
                        }
                    }
                }
            }
            
            //left group
            if notiMsg.contains("has left the chat"){
                
                if let no = QuickBloxManager().getNoFromMessage(message: notiMsg , replaceText: " has left the chat.") as? String {
                    if SessionManager.getNameFromMyAddressBook(number: no) != "" {
                        notiMsg = SessionManager.getNameFromMyAddressBook(number: no) + " " + "has left the chat"
                    }
                }
            }
            
            //new participant added
            if notiMsg.contains(" added "){
                let arrNo = notiMsg.components(separatedBy: " ")
                if arrNo.count == 3 {
                    
                    if arrNo[0] == SessionManager.getPhone() {
                        notiMsg = "You" + " added " + SessionManager.getNameFromMyAddressBook(number: arrNo[2])
                    }
                    else if arrNo[2] == SessionManager.getPhone() {
                        notiMsg = SessionManager.getNameFromMyAddressBook(number: arrNo[0]) + " added " + "You"
                    }
                    else {
                        notiMsg = SessionManager.getNameFromMyAddressBook(number: arrNo[0]) + " added " + SessionManager.getNameFromMyAddressBook(number: arrNo[2])
                    }
                }
            }
        }
        
        attributes[NSAttributedStringKey.foregroundColor] = textColor
        
        var attributedString = NSAttributedString(string: notiMsg, attributes: attributes)
        
        if (messageItem.text == "Call notification") {
            
            if let dic = messageItem.customParameters {
                
                if let duration = dic.object(forKey: "callDuration") {
                    messageItem.callDuration = QuickBloxManager().parseDuration("\(duration)")
                }
                
                if let callNotificationState = dic.object(forKey: "callNotificationState") as? String {
                    if callNotificationState == constantVC.callState.missedNoAnswer.rawValue {
                        messageItem.callNotificationState = QMCallNotificationState.missedNoAnswer
                    }
                    else {
                        messageItem.callNotificationState = QMCallNotificationState.hangUp
                    }
                }
                
                if let callNotificationType = dic.object(forKey: "callNotificationType") as? String {
                    if callNotificationType == "audio" {
                        messageItem.callNotificationType = QMCallNotificationType.audio
                    }
                    else {
                        messageItem.callNotificationType = QMCallNotificationType.video
                    }
                }
            }
            
            paragraphStyle.alignment = NSTextAlignment.center
            
            font = UIFont(name: "CircularStd-Medium", size: 13)
            //25
            if messageItem.callNotificationState == QMCallNotificationState.missedNoAnswer {
                
                textColor = UIColor(red: 255.0 / 255, green: 108.0 / 255, blue: 108.0 / 255, alpha: 1)
            } else {
                textColor = UIColor(red: 119.0 / 255, green: 133.0 / 255, blue: 148.0 / 255, alpha: 1)
            }
            
            if let callNotificationItem = QMCallNotificationItem(callNotificationMessage: messageItem!) {
                if let msg = callNotificationItem.notificationText {
                    message = msg
                }
                iconImage = callNotificationItem.iconImage
            }
            
        }
        
        if iconImage != nil {
            
            attributes[NSAttributedStringKey.foregroundColor] = textColor
            
            if messageItem.callNotificationState == QMCallNotificationState.missedNoAnswer {
                let underLineColor: UIColor = UIColor(red: 255.0 / 255, green: 108.0 / 255, blue: 108.0 / 255, alpha: 1)
                attributes[NSAttributedStringKey.underlineColor] = underLineColor
            }
            else {
                let underLineColor: UIColor = UIColor(red: 119.0 / 255, green: 133.0 / 255, blue: 148.0 / 255, alpha: 1)
                attributes[NSAttributedStringKey.underlineColor] = underLineColor
            }
            
            
            var messageText = (message?.length)! > 0 ? "\(kQMTextAttachmentSpacing)\(message!)" : kQMTextAttachmentSpacing
            var mutableAttrStr = NSMutableAttributedString(string: messageText, attributes: attributes)
            
            var textAttachment = NSTextAttachment()
            textAttachment.image = iconImage
            
            let rect = CGRect(
                origin: CGPoint(x: 0, y: 0),
                size: iconImage!.size
            )
            
            textAttachment.bounds = rect
            
            //add underline
            let range = (messageText as NSString).range(of: message!)
            mutableAttrStr.addAttribute(NSAttributedStringKey.underlineStyle,
                                        value: NSUnderlineStyle.styleSingle.rawValue,
                                        range: range)
            
            let attrStringWithImage = NSAttributedString(attachment: textAttachment)
            mutableAttrStr.insert(attrStringWithImage, at: 0)
            attributedString = mutableAttrStr
        }
        
        return attributedString
    }
    
    
    /**
     Creates top label attributed string from QBChatMessage
     
     - parameter messageItem: QBCHatMessage instance
     
     - returns: login string, example: @SwiftTestDevUser1
     */
    override func topLabelAttributedString(forItem messageItem: QBChatMessage!) -> NSAttributedString? {
        
        guard messageItem.senderID != self.senderID else {
            return nil
        }
        
        guard self.dialog.type != QBChatDialogType.private else {
            return nil
        }
        
        let paragrpahStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragrpahStyle.lineBreakMode = NSLineBreakMode.byTruncatingTail
        var attributes = Dictionary<NSAttributedStringKey, AnyObject>()
        attributes[NSAttributedStringKey.foregroundColor] = UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 1.0)
        attributes[NSAttributedStringKey.font] = UIFont(name: "CircularStd-Medium", size: 15)
        attributes[NSAttributedStringKey.paragraphStyle] = paragrpahStyle
        
        var topLabelAttributedString : NSAttributedString?
        
        if let topLabelText = ServicesManager.instance().usersService.usersMemoryStorage.user(withID: messageItem.senderID)?.login {
            topLabelAttributedString = NSAttributedString(string: SessionManager.getNameFromMyAddressBook(number:  topLabelText), attributes: attributes)
        } else { // no user in memory storage
            if let user = ServicesManager.instance().usersService.getUserWithID(messageItem.senderID) as? QBUUser {
                if SessionManager.getNameFromMyAddressBook(number:  user.phone!) != "" {
                    topLabelAttributedString = NSAttributedString(string: SessionManager.getNameFromMyAddressBook(number:  user.phone!), attributes: attributes)
                }
            } else {
               
               ServicesManager.instance().usersService.getUserWithID(messageItem.senderID).continueOnSuccessWith { (task) -> Any? in
                    if task.isCompleted {
                         if let user = task.result {
                            
                            if let login = user.login {
                                topLabelAttributedString = NSAttributedString(string: SessionManager.getNameFromMyAddressBook(number:  login), attributes: attributes)
                            }
                         }
                    }
                    return nil
               } 
            }
        }
        return topLabelAttributedString
    }
    
    /**
     Creates bottom label attributed string from QBChatMessage using self.statusStringFromMessage
     
     - parameter messageItem: QBChatMessage instance
     
     - returns: bottom label status string
     */
    override func bottomLabelAttributedString(forItem messageItem: QBChatMessage!) -> NSAttributedString! {
        
        var textColor = messageItem.senderID == self.senderID ? UIColor.white : UIColor.black
        
        if (messageItem?.text?.contains(constantVC.GeneralConstants.stickerKey))! {
            textColor = messageItem.senderID == self.senderID ? UIColor.lightGray : UIColor.black
        }
        
        if (messageItem?.text?.contains(constantVC.GeneralConstants.stickerKey))! && QMReplyMsgManager.shared.isContainReply(message: messageItem) {
            textColor = messageItem.senderID == self.senderID ? UIColor.white : UIColor.black
        }
        
        let paragrpahStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragrpahStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        var attributes = Dictionary<NSAttributedStringKey, AnyObject>()
        attributes[NSAttributedStringKey.foregroundColor] = textColor
        attributes[NSAttributedStringKey.font] = UIFont(name: "CircularStd-Book", size: 13)
        attributes[NSAttributedStringKey.paragraphStyle] = paragrpahStyle
        
        var text = messageItem.dateSent != nil ? messageTimeDateFormatter.string(from: messageItem.dateSent!) : ""
        
        if messageItem.senderID == self.senderID {
            text =  self.statusStringFromMessage(message: messageItem) + " " +  text   //+ "\n" + self.statusStringFromMessage(message: messageItem)
        }
        
        let bottomLabelAttributedString = NSAttributedString(string: text, attributes: attributes)
        
        return bottomLabelAttributedString
    }
    
    //MARK:- Custom Methods
    func showMicHideSendButton(){
        self.btnMic_Send?.tag = 1
        self.btnMic_Send?.setImage(UIImage(named:"micChat"), for: .normal)
    }
    
    func hideMicShowSendButton(){
        self.btnMic_Send?.tag = 2
        self.btnMic_Send?.setImage(UIImage(named:"messageSendBtn"), for: .normal)
    }
    
    func configInputBar(){
        // *** Create Toolbar
        // *** Create GrowingTextView ***
        self.msgTextView?.delegate = self
        self.msgTextView?.text = "Type here...".localized
        self.msgTextView?.textColor = UIColor.lightGray
        self.msgTextView?.font = UIFont(name: "CircularStd-Book", size: 16)
        self.msgTextView?.textAlignment = .left
        
        self.showMicHideSendButton()
        
        self.inputBarView?.backgroundColor = UIColor.white
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(self.dismissKeyboard))
        self.collectionView?.addGestureRecognizer(tap)
        // *** Listen to keyboard show / hide ***
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    //MARK: - SwiftMultiSelectDelegate
    func userDidSearch(searchString: String) {
        if searchString == ""{
            selectedItems = items
        }else{
            selectedItems = items.filter({$0.title.lowercased().contains(searchString.lowercased()) || ($0.description != nil && $0.description!.lowercased().contains(searchString.lowercased())) })
        }
    }
    
    func numberOfItemsInSwiftMultiSelect() -> Int {
        return selectedItems.count
    }
    
    func swiftMultiSelect(didUnselectItem item: SwiftMultiSelectItem) {
    }
    
    func swiftMultiSelect(didSelectItem item: SwiftMultiSelectItem) {
    }
    
    func didCloseSwiftMultiSelect() {
        
    }
    
    func swiftMultiSelect(itemAtRow row: Int) -> SwiftMultiSelectItem {
        return selectedItems[row]
    }
    
    func swiftMultiSelect(didSelectItems items: [SwiftMultiSelectItem]) {
        // self.showLoader(strForMessage: "loading..".localized)
        var arrContacts = [CNContact]()
        
        for item in items {
            var contactCN = item.userInfo as! CNMutableContact
            if contactCN.givenName.count < 1 {
                contactCN.givenName = contactCN.familyName
            }
            arrContacts.append(contactCN)
        }
        var vcard = NSData()
        
        do {
            try vcard = CNContactVCardSerialization.data(with: arrContacts) as NSData
            
            DispatchQueue.global(qos: .default).async(execute: {
                
                // Sending attachment to dialog.
                DispatchQueue.main.async(execute: {
                    
                    let attachment = QBChatAttachment()
                    attachment.contentType = "vCard/vcf"
                    attachment.type = "Contact attachment"
                    attachment.fileData = vcard as Data
                    self.sendContactMessage(attachment: attachment)
                    // self.sendMessage(with: attachment)
                })
            })
        } catch {
            // self.dismissLoader()
        }
    }
    
    
    func sendContactMessage(attachment: QBChatAttachment?){
        
        let message = QBChatMessage()
        message.text = "Contact attachment"
        message.senderID = self.senderID
        message.markable = true
        message.deliveredIDs = [NSNumber(value: self.senderID)]
        message.readIDs = [NSNumber(value: self.senderID)]
        message.dialogID = self.dialog.id
        message.dateSent = Date()
        
        //to reply msg
        if self.dicMsgToReply.allKeys.count > 1 {
            let dicReply = NSMutableDictionary()
            dicReply.setValue(QuickBloxManager().convertDictionaryToJsonString(dict: self.dicMsgToReply) , forKey: MsgToReplyKeys.replyMsg)
            message.customParameters = dicReply
            self.closeChatReplyView()
        }
        
        let attachment_: QBChatAttachment = QBChatAttachment()
        attachment_.type = "Contact"
        attachment_.attachmentType = QMAttachmentType.contentTypeCustom
        
        if (attachment_ != nil) {
            message.attachments = [attachment_]
        }
        
        self.finishSendingMessage(animated: true)
        self.chatDataSource.add(message)
        
        if attachment?.fileData != nil {
            
            QBRequest.tUploadFile((attachment?.fileData)! , fileName: "contact.vcf" , contentType: "vCard/vcf" , isPublic: false , successBlock: { (response , uploadedBlob) in
                
                // Create and configure message
                let uploadedFileID: UInt = uploadedBlob.id
                let attachment: QBChatAttachment = QBChatAttachment()
                attachment.type = "Contact"
                attachment.id = String(uploadedFileID)
                attachment.url = uploadedBlob.privateUrl()
                attachment.attachmentType = QMAttachmentType.contentTypeCustom
                
                if (attachment != nil) {
                    message.attachments = [attachment]
                }
                
                self.chatDataSource.delete(message)
                
                
                // Logging in to chat.
                if QBChat.instance.isConnected {
                    print("CHAT ACTIVE")
                } else {
                    // Logging in to chat.
                    ServicesManager.instance().chatService.connect { (error) in
                    }
                }
                
                // Send message
                ServicesManager.instance().chatService.send(message, toDialogID: self.dialog.id!, saveToHistory: true, saveToStorage: true) { (error) ->
                    Void in
                    
                    if error != nil {
                        
                        if !Connectivity.isConnectedToInternet() {
                            QMMessageNotificationManager.showNotification(withTitle: "SA_STR_ERROR".localized, subtitle: "Message failed to send. Check your connection".localized , type: QMMessageNotificationType.warning)
                        }
                        
                        // Logging in to chat.
                        ServicesManager.instance().chatService.connect { (error) in
                        }
                        
                    } else {
                        
                        self.sendPushNotification(msg: message.text!)
                        //  self.chatDataSource.delete(message)
                        //
                    }
                }
                
            }, statusBlock: {(request: QBRequest?, status: QBRequestStatus?) in
                
            }, errorBlock: {(response: QBResponse!) in
            })
        }
    }
    
    // MARK: Collection View Datasource
    func videoSize(for message: QBChatMessage?) -> CGSize {
        
        let attachment: QBChatAttachment? = message?.attachments?.first
        
        var size = CGSize(width: 180.0, height: 95.0) //default video size for cell
        
        if !(CGSize(width: attachment?.width ?? Int(0.0), height: attachment?.height ?? Int(0.0)).equalTo(CGSize.zero)) {
            
            let isVerticalVideo: Bool = attachment!.width < attachment!.height
            
            size = isVerticalVideo ? CGSize(width: 170.0, height: 180.0) : size
        }
        
        return size
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView!, dynamicSizeAt indexPath: IndexPath!, maxWidth: CGFloat) -> CGSize {
        
        let item: QBChatMessage? = chatDataSource.message(for: indexPath)
        let viewClass: AnyClass = self.viewClass(forItem: item!)
        var size = CGSize.zero
        print("viewClass........ \(viewClass)")
        if viewClass == QMVideoIncomingCell.self || viewClass == QMVideoOutgoingCell.self {
            size = videoSize(for: item)
        }
        else if viewClass == QMChatAttachmentIncomingCell.self || viewClass == QMChatLocationIncomingCell.self || viewClass == QMChatAttachmentOutgoingCell.self || viewClass == QMChatLocationOutgoingCell.self{
            
            if let text = item?.text {
                if text.contains("TAMSTICKER*") && !QMReplyMsgManager.shared.isContainReply(message: item!){
                    size = CGSize(width: 100 , height: 100)
                }
                else if text.contains("TAMSTICKER*") && QMReplyMsgManager.shared.isContainReply(message: item!){
                    size = CGSize(width: 150 , height: 110)
                }
                else {
                    var attributedStringCaption: NSAttributedString?
                    attributedStringCaption = QMReplyMsgManager.shared.getAttributedString(forItem: item , msgText: text)
                    let sizeOfCaption = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringCaption, withConstraints: CGSize(width: self.view.frame.size.width - 150.0, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
                    size = CGSize(width: self.view.frame.size.width-kMessageContainerWidthPadding , height: sizeOfCaption.height/2 + kQMAttachmentCellSize + 30)
                }
            } else {
                size = CGSize(width: min(kQMAttachmentCellSize, maxWidth) , height: kQMAttachmentCellSize)
            }
        }
        else if viewClass ==  QMImageIncomingCell.self || viewClass ==  QMImageOutgoingCell.self{
          
          size = CGSize(width: self.view.frame.size.width/2 , height: self.view.frame.size.width/2)
        }
        else if viewClass == QMChatNotificationCell.self {
            
            let attributedString: NSAttributedString? = self.attributedString(forItem: item)
            
            if attributedString?.responds(to: #selector(NSAttributedString.boundingRect(with:options:context:))) ?? false {
                
                size = (attributedString?.boundingRect(with: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).size)!
            }
        }
            
        else if viewClass == QMChatReplyIncomingCell.self || viewClass == QMChatReplyOutgoingCell.self {
            var attributedStringMsgToReply: NSAttributedString?
            var attributedStoryMsg: NSAttributedString?
            var attributedString: NSAttributedString?
            var attributedStringUserName: NSAttributedString?
            var replyToMsgType:String = ""
          
            if let customParam = item?.customParameters {
                if let StrMsgDetail = customParam.object(forKey: MsgToReplyKeys.replyMsg) as? String {
                    attributedStoryMsg = QMReplyMsgManager.shared.getAttributedString(forItem: item, msgText: " ")
               
                    let dic = QuickBloxManager().convertJsonStringToDictionary(text: StrMsgDetail)
                    if let dicReply = dic {
                        if let msgToReplytext = dicReply[MsgToReplyKeys.msg] as? String {
                            attributedStringMsgToReply = QMReplyMsgManager.shared.getAttributedString(forItem: item, msgText: msgToReplytext)
                        }
                        if let senderNo = dicReply[MsgToReplyKeys.senderNumber] as? String {
                         if let storyDetail = customParam.object(forKey: MsgToReplyKeys.isMsgFromStories){
                              if senderNo == SessionManager.getPhone() {
                                  attributedStringUserName = QMReplyMsgManager.shared.getAttributedString(forItem: item, msgText: SessionManager.getNameFromMyAddressBook(number: senderNo))
                              } else {
                                   attributedStringUserName = QMReplyMsgManager.shared.getAttributedString(forItem: item, msgText: "You")
                              }
                         }
                         else{
                              if senderNo == SessionManager.getPhone() {
                                   attributedStringUserName = QMReplyMsgManager.shared.getAttributedString(forItem: item, msgText: "You")
                              } else {
                                   attributedStringUserName = QMReplyMsgManager.shared.getAttributedString(forItem: item, msgText: SessionManager.getNameFromMyAddressBook(number: senderNo))
                              }
                         }
                        }
                        if let type = dicReply[MsgToReplyKeys.msgType] as? String {
                            replyToMsgType = type
                        }
                    }
                }
               
               if customParam.object(forKey: MsgToReplyKeys.isMsgFromStories) != nil{
                    attributedStoryMsg = QMReplyMsgManager.shared.getAttributedString(forItem: item, msgText: ".📷 Story  ")
                  }
            }
          if let msgText = item?.text {
               
               attributedString = QMReplyMsgManager.shared.getAttributedString(forItem: item, msgText: msgText)
          }
            let sizeOfMsgToReply = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringMsgToReply, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
            let sizeOfStory = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStoryMsg, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
            let sizeOfMsg = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
            let attributedDateStr = self.bottomLabelAttributedString(forItem: item)
            let sizeTimeAndStatus = TTTAttributedLabel.sizeThatFitsAttributedString(attributedDateStr, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
          
            let sizeUserName = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringUserName, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
            let arrSizes = [sizeOfStory.width + 100.0, sizeOfMsgToReply.width + 20.0 , sizeOfMsg.width , sizeTimeAndStatus.width + 15.0 , sizeUserName.width + 100.0]
            var heightToSet:CGFloat = 69.0
          
            if replyToMsgType == MsgToReplyKeys.TEXT {
                if (sizeOfMsgToReply.height + 30.0) < ReplyContainer.maxReplyContainerHeight {
                    heightToSet = (sizeOfMsgToReply.height + 30.0)
                } else {
                    heightToSet = ReplyContainer.maxReplyContainerHeight
                }
            }
          if viewClass == QMChatReplyIncomingCell.self{
               if self.dialog.type == QBChatDialogType.group{
                    heightToSet =  heightToSet + 25
               }
          }
          
            if let maxWidth_ = arrSizes.max() {
                let maxWidth = maxWidth_

                size = CGSize(width: maxWidth , height: heightToSet + (sizeOfMsg.height)+20)
            } else {
                size = CGSize(width: maxWidth , height: heightToSet + (sizeOfMsg.height)+20)
            }
        }
        else {
            let attributedString: NSAttributedString? = self.attributedString(forItem: item)
            size = TTTAttributedLabel.sizeThatFitsAttributedString(attributedString, withConstraints: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
        }
        if (viewClass == QMImageIncomingCell.self || viewClass == QMImageOutgoingCell.self || viewClass == QMVideoIncomingCell.self || viewClass == QMVideoOutgoingCell.self) && QMReplyMsgManager.shared.isContainReply(message: item!) {
            size = CGSize(width: size.width-20 , height: size.height + 10 + QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: item!, view: self.view))
        }
        
        if (viewClass == QMImageIncomingCell.self || viewClass == QMImageOutgoingCell.self || viewClass == QMVideoIncomingCell.self || viewClass == QMVideoOutgoingCell.self) && QMReplyMsgManager.shared.isContainCaption(message: item!) {
            size = CGSize(width: size.width-20 , height: size.height + 10 + QMReplyMsgManager.shared.getCaptionHeightToSet(message: item!, view: self.view))
        }
        return size
    }
    override func collectionView(_ collectionView: QMChatCollectionView!, minWidthAt indexPath: IndexPath!) -> CGFloat {
        
        var size = CGSize.zero
        guard let item = self.chatDataSource.message(for: indexPath) else {
            return 0
        }
        let viewClass: AnyClass = self.viewClass(forItem: item) as AnyClass
     
        if viewClass == QMChatIncomingCell.self || viewClass == QMChatOutgoingCell.self || viewClass == QMChatReplyOutgoingCell.self || viewClass == QMChatReplyIncomingCell.self {
            
            let str = self.bottomLabelAttributedString(forItem: item)
            let frameWidth = collectionView.frame.width
            let maxHeight = CGFloat.greatestFiniteMagnitude
            size = TTTAttributedLabel.sizeThatFitsAttributedString(str, withConstraints: CGSize(width:frameWidth - kMessageContainerWidthPadding, height: maxHeight), limitedToNumberOfLines:0)
     }
        
        if self.dialog.type != QBChatDialogType.private {

            let topLabelSize = TTTAttributedLabel.sizeThatFitsAttributedString(self.topLabelAttributedString(forItem: item), withConstraints: CGSize(width: collectionView.frame.width - kMessageContainerWidthPadding, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines:0)

            if topLabelSize.width > size.width {
                size = topLabelSize
            }
        }
        return size.width
    }
    
    override func collectionView(_ collectionView: QMChatCollectionView!, layoutModelAt indexPath: IndexPath!) -> QMChatCellLayoutModel {
        
        var layoutModel: QMChatCellLayoutModel = super.collectionView(collectionView , layoutModelAt: indexPath)
        
        layoutModel.topLabelHeight = 0.0
        layoutModel.maxWidthMarginSpace = 20.0
        layoutModel.spaceBetweenTextViewAndBottomLabel = 0
        layoutModel.spaceBetweenTopLabelAndTextView = 0
        
        let item: QBChatMessage? = chatDataSource.message(for: indexPath)
        let viewClass: AnyClass = self.viewClass(forItem: item!) as AnyClass
        
        if viewClass == QMChatOutgoingCell.self || viewClass == QMChatAttachmentOutgoingCell.self || viewClass == QMChatLocationOutgoingCell.self || viewClass.isSubclass(of: QMMediaOutgoingCell.self) || viewClass == QMChatOutgoingLinkPreviewCell.self {
            
            layoutModel.avatarSize = CGSize.zero
            
            if viewClass != QMChatOutgoingCell.self {
                
                layoutModel.spaceBetweenTextViewAndBottomLabel = 5
            }
        }
        else if viewClass == QMChatAttachmentIncomingCell.self || viewClass == QMChatLocationIncomingCell.self || viewClass == QMChatIncomingCell.self || viewClass.isSubclass(of: QMMediaIncomingCell.self) || viewClass == QMChatIncomingLinkPreviewCell.self {
            
            let isAudioCell: Bool = viewClass == QMAudioOutgoingCell.self || viewClass == QMAudioIncomingCell.self
            
            if self.dialog.type != QBChatDialogType.private && !isAudioCell {
                
                layoutModel.topLabelHeight = 18
            }
            
            if viewClass != QMChatIncomingCell.self || viewClass == QMChatOutgoingCell.self {
                
                layoutModel.spaceBetweenTextViewAndBottomLabel = 5
                layoutModel.spaceBetweenTopLabelAndTextView = 5
            }
            
            layoutModel.avatarSize = CGSize.zero //CGSize(width: kQMAvatarSize, height: kQMAvatarSize)
        }
        
        var size = CGSize.zero
        
        if detailedCells.contains((item?.id!)!) || viewClass == QMChatAttachmentIncomingCell.self || viewClass == QMChatAttachmentOutgoingCell.self || viewClass == QMChatLocationIncomingCell.self || viewClass == QMChatLocationOutgoingCell.self || viewClass == QMVideoIncomingCell.self || viewClass == QMVideoOutgoingCell.self || viewClass == QMImageOutgoingCell.self || viewClass == QMImageIncomingCell.self || viewClass == QMChatIncomingLinkPreviewCell.self || viewClass == QMChatOutgoingLinkPreviewCell.self {
            
            let constraintsSize = CGSize(width: (collectionView?.frame.width)! - kQMWidthPadding, height: CGFloat.greatestFiniteMagnitude)
            
            size = TTTAttributedLabel.sizeThatFitsAttributedString(bottomLabelAttributedString(forItem: item), withConstraints: constraintsSize, limitedToNumberOfLines: 0)
        }
     
        if viewClass == QMChatIncomingCell.self || viewClass == QMChatOutgoingCell.self || viewClass == QMChatReplyIncomingCell.self || viewClass == QMChatReplyOutgoingCell.self {
            let constraintsSize = CGSize(width: (collectionView?.frame.width)! - kQMWidthPadding, height: CGFloat.greatestFiniteMagnitude)
            
            size = TTTAttributedLabel.sizeThatFitsAttributedString(bottomLabelAttributedString(forItem: item), withConstraints: constraintsSize, limitedToNumberOfLines: 0)
        }
        
        if viewClass == QMAudioOutgoingCell.self || viewClass == QMAudioIncomingCell.self {
            
            size = CGSize(width: 250 , height: 90) //default size
            if QMReplyMsgManager.shared.isContainReply(message: item!) {
                size = CGSize(width: size.width , height: size.height + QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: item!, view: self.view))
            }
            layoutModel.staticContainerSize = size
        }
        
        if viewClass == TamContactIncomingCell.self || viewClass == TamContactOutgoingCell.self {
            
            size = CGSize(width: 250 , height: 70) //default size
            if QMReplyMsgManager.shared.isContainReply(message: item!) {
                size = CGSize(width: size.width , height: size.height + QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: item!, view: self.view))
            }
            layoutModel.staticContainerSize = size
        }
        layoutModel.bottomLabelHeight = CGFloat(ceil(size.height))
        return layoutModel
        
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if let panGestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer {
            let translation = panGestureRecognizer.translation(in: self.viewToSwipe)
            if fabs(translation.x) > fabs(translation.y) {
                return true
            }
            return false
        }
        return false
    }
    func addTabGestureToReplyContainer(view: UIView, msg : String){
        
        let tap = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        tap.tag = view.tag
        arrReplyIds[view.tag] = msg
        tap.addTarget(self, action: #selector(self.handleTap(_:)), for: .touchUpInside)
        view.addSubview(tap)
    }
    
    @objc func handleTap(_ sender: UIButton) {
       let message_ =  ServicesManager.instance().chatService.messagesMemoryStorage.message(withID: arrReplyIds[sender.tag]!, fromDialogID: self.dialogID())
      if let customParam = message_?.customParameters {
     if let storyDetail = customParam.object(forKey: MsgToReplyKeys.isMsgFromStories){
          let controller = self.storyboard?.instantiateViewController(withIdentifier: "ShowStoriesVC") as! ShowStoriesVC
          
          if let StrMsgDetail = customParam.object(forKey: MsgToReplyKeys.replyMsg) as? String {
               
               let dic = QuickBloxManager().convertJsonStringToDictionary(text: StrMsgDetail)
               if let dicReply = dic {
                    controller.userId = String(describing: dicReply[MsgToReplyKeys.replyId]!)
                    controller.username = SessionManager.getNameFromMyAddressBook(number: dicReply[MsgToReplyKeys.replyNumber] as! String)
                    controller.userPhnNo = dicReply[MsgToReplyKeys.replyNumber] as! String
                    controller.imageURL = dicReply[MsgToReplyKeys.replyNumber] as! String

                    controller.singleStory = storyDetail as! String
                    controller.shShowSingleStory = true
                    self.navigationController?.pushViewController(controller , animated: false)
               }
          }  
     }
     }
        let index = self.chatDataSource.indexPath(for: message_)
        collectionView?.scrollToItem(at: index!, at: .centeredVertically, animated: true)
        self.view.layoutIfNeeded()
    }
    
    
    
    override func collectionView(_ collectionView: QMChatCollectionView!, configureCell cell: UICollectionViewCell!, for indexPath: IndexPath) {
        
        super.collectionView(collectionView, configureCell: cell, for: indexPath)
        // subscribing to cell delegate
        let chatCell = cell as! QMChatCell
        chatCell.delegate = self
        
        //drag for swipe right to left
        let dragRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.onDrag(sender:)))
        dragRecognizer.delegate = self
        chatCell.containerView.addGestureRecognizer(dragRecognizer)
        
        let message = self.chatDataSource.message(for: indexPath)
        
       // print("CHATMESSAGE" , message)
        
        //for forward selection
        if self.arrMsgsToForward.contains(message!) {
            chatCell.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        } else {
            chatCell.backgroundColor = UIColor.clear
        }
        
        let tapCell = UITapGestureRecognizer(target: self, action: #selector(self.handleTap_collectionCell(_:)))
        if !((self.vwForwardMsg?.isHidden)!) {
            chatCell.addGestureRecognizer(tapCell)
            chatCell.tag = indexPath.row
        } else {
            chatCell.removeGestureRecognizer(tapCell)
        }
        
        let status: QMMessageStatus = self.queueManager().status(for: message!)
        self.ConfigureChatContainer(status: status , chatCell: chatCell , message: message!)
        
        if let videoOutgoingCell = cell as? QMVideoOutgoingCell {
            
            if QMReplyMsgManager.shared.isContainReply(message: message!) {
                videoOutgoingCell.vwReplyContainer.isHidden = false
                videoOutgoingCell.vwReplyContainer.tag = indexPath.row
               
                
                if QMReplyMsgManager.shared.getReplyMsgId(message: message!) != "" {
                    self.addTabGestureToReplyContainer(view: videoOutgoingCell.vwReplyContainer, msg: QMReplyMsgManager.shared.getReplyMsgId(message: message!))
                }
                
                videoOutgoingCell.lblUserName?.text = QMReplyMsgManager.shared.getReplyUserName(message: message!)
                videoOutgoingCell.lblReplyToMsg?.text = QMReplyMsgManager.shared.getReplyMsgToText(message: message!)
                if QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!) != "" {
                    let dataDecoded : Data = Data(base64Encoded: QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!), options: .ignoreUnknownCharacters)!
                    let decodedimage = UIImage(data: dataDecoded)
                    videoOutgoingCell.imgVwThumbnail?.image = decodedimage
                }
                if QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!) != "" {
                    videoOutgoingCell.imgVwThumbnail?.image = UIImage(named: QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!))
                }
                videoOutgoingCell.constraint_replyContainerHeight.constant = QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: message!, view: self.view)
            }
            else {
                videoOutgoingCell.constraint_replyContainerHeight.constant = 0
                videoOutgoingCell.vwReplyContainer.isHidden = true
            }
            if QMReplyMsgManager.shared.isContainCaption(message: message!) {
                if let caption = message?.text {
                    videoOutgoingCell.lblCaption.text = caption
                    var attributedStringCaption: NSAttributedString?
                    attributedStringCaption = QMReplyMsgManager.shared.getAttributedString(forItem: message , msgText: caption)
                    let sizeOfCaption = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringCaption, withConstraints: CGSize(width: self.view.frame.size.width - 100.0, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
                    videoOutgoingCell.constraint_lblCaptionHeight.constant = sizeOfCaption.height
                }
            } else {
                videoOutgoingCell.constraint_lblCaptionHeight.constant = 0.0
            }
            
            mediaController.configureView(videoOutgoingCell , with: message!)
        }
        if let videoIncomingCell = cell as? QMVideoIncomingCell {
            
            if QMReplyMsgManager.shared.isContainReply(message: message!) {
                videoIncomingCell.vwReplyContainer.isHidden = false
                 videoIncomingCell.vwReplyContainer.tag = indexPath.row
               
                if QMReplyMsgManager.shared.getReplyMsgId(message: message!) != "" {
                    self.addTabGestureToReplyContainer(view: videoIncomingCell.vwReplyContainer, msg: QMReplyMsgManager.shared.getReplyMsgId(message: message!))
                }
                
                videoIncomingCell.lblUserName?.text = QMReplyMsgManager.shared.getReplyUserName(message: message!)
                videoIncomingCell.lblReplyToMsg?.text = QMReplyMsgManager.shared.getReplyMsgToText(message: message!)
                if QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!) != "" {
                    let dataDecoded : Data = Data(base64Encoded: QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!), options: .ignoreUnknownCharacters)!
                    let decodedimage = UIImage(data: dataDecoded)
                    videoIncomingCell.imgVwThumbnail?.image = decodedimage
                }
                if QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!) != "" {
                    videoIncomingCell.imgVwThumbnail?.image = UIImage(named: QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!))
                }
                videoIncomingCell.constraint_replyContainerHeight.constant = QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: message!, view: self.view)
            }
            else {
                videoIncomingCell.constraint_replyContainerHeight.constant = 0
                videoIncomingCell.vwReplyContainer.isHidden = true
            }
            
            chatCell.containerView?.bgColor = UIColor.white
            chatCell.containerView?.highlightColor = UIColor.white
            
            if QMReplyMsgManager.shared.isContainCaption(message: message!) {
            if let caption = message?.text {
                videoIncomingCell.lblCaption.text = caption
                var attributedStringCaption: NSAttributedString?
                attributedStringCaption = QMReplyMsgManager.shared.getAttributedString(forItem: message , msgText: caption)
                let sizeOfCaption = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringCaption, withConstraints: CGSize(width: self.view.frame.size.width - 100.0, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
                videoIncomingCell.constraint_lblCaptionHeight.constant = sizeOfCaption.height
            }
            } else {
                videoIncomingCell.constraint_lblCaptionHeight.constant = 0.0
            }
            mediaController.configureView(videoIncomingCell , with: message!)
        }
        
        if let imageOutgoingCell = cell as? QMImageOutgoingCell {
            
            if QMReplyMsgManager.shared.isContainReply(message: message!) {
                imageOutgoingCell.vwReplyContainer.isHidden = false
                 imageOutgoingCell.vwReplyContainer.tag = indexPath.row
                
                if QMReplyMsgManager.shared.getReplyMsgId(message: message!) != "" {
                    self.addTabGestureToReplyContainer(view: imageOutgoingCell.vwReplyContainer, msg: QMReplyMsgManager.shared.getReplyMsgId(message: message!))
                }
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                imageOutgoingCell.vwReplyContainer.addGestureRecognizer(tap)
                imageOutgoingCell.lblUserName?.text = QMReplyMsgManager.shared.getReplyUserName(message: message!)
                imageOutgoingCell.lblReplyToMsg?.text = QMReplyMsgManager.shared.getReplyMsgToText(message: message!)
                if QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!) != "" {
                    let dataDecoded : Data = Data(base64Encoded: QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!), options: .ignoreUnknownCharacters)!
                    let decodedimage = UIImage(data: dataDecoded)
                    imageOutgoingCell.imgVwThumbnail?.image = decodedimage
                }
                if QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!) != "" {
                    imageOutgoingCell.imgVwThumbnail?.image = UIImage(named: QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!))
                }
                imageOutgoingCell.constraint_replyContainerHeight.constant = QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: message!, view: self.view)
            }
            else {
                imageOutgoingCell.constraint_replyContainerHeight.constant = 0
                imageOutgoingCell.vwReplyContainer.isHidden = true
            }
            
            if (message?.text?.contains(constantVC.GeneralConstants.stickerKey))! {
                let msgTextArr = message?.text?.components(separatedBy: "*")
                if msgTextArr?.count == 2 {
                    if let stickerName = msgTextArr![1] as? String {
                        if QMReplyMsgManager.shared.isContainReply(message: message!) {
                            chatCell.containerView?.bgColor = UIColor(red: 67/255, green:179/255, blue: 255/255, alpha: 1.0)
                            chatCell.containerView?.highlightColor = UIColor(red: 67/255, green:179/255, blue: 255/255, alpha: 1.0)
                        }else {
                            chatCell.containerView?.bgColor = UIColor.clear
                            chatCell.containerView?.highlightColor = UIColor.clear
                        }
                        imageOutgoingCell.previewImageView.contentMode = .scaleAspectFit
                        imageOutgoingCell.previewImageView.image = UIImage(named: stickerName)
                    }
                }
            } else {
                
                if QMReplyMsgManager.shared.isContainCaption(message: message!) {
                    
                if let caption = message?.text {
                    imageOutgoingCell.lblCaption.text = caption
                    var attributedStringCaption: NSAttributedString?
                    attributedStringCaption = QMReplyMsgManager.shared.getAttributedString(forItem: message , msgText: caption)
                    let sizeOfCaption = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringCaption, withConstraints: CGSize(width: self.view.frame.size.width - 150.0, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
                   imageOutgoingCell.constraint_lblCaptionHeight.constant = sizeOfCaption.height
                }
                } else {
                    imageOutgoingCell.constraint_lblCaptionHeight.constant = 0
                      imageOutgoingCell.lblCaption.text = ""
                }
                mediaController.configureView(imageOutgoingCell , with: message!)
            }
        }
        
        if let imageIncomingCell = cell as? QMImageIncomingCell {
            
            if QMReplyMsgManager.shared.isContainReply(message: message!) {
                imageIncomingCell.vwReplyContainer.isHidden = false
                 imageIncomingCell.vwReplyContainer.tag = indexPath.row
                if QMReplyMsgManager.shared.getReplyMsgId(message: message!) != "" {
                    self.addTabGestureToReplyContainer(view: imageIncomingCell.vwReplyContainer, msg: QMReplyMsgManager.shared.getReplyMsgId(message: message!))
                }
                imageIncomingCell.lblUserName?.text = QMReplyMsgManager.shared.getReplyUserName(message: message!)
                imageIncomingCell.lblReplyToMsg?.text = QMReplyMsgManager.shared.getReplyMsgToText(message: message!)
                if QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!) != "" {
                    let dataDecoded : Data = Data(base64Encoded: QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!), options: .ignoreUnknownCharacters)!
                    let decodedimage = UIImage(data: dataDecoded)
                    imageIncomingCell.imgVwThumbnail?.image = decodedimage
                }
                if QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!) != "" {
                    imageIncomingCell.imgVwThumbnail?.image = UIImage(named: QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!))
                }
                imageIncomingCell.constraint_replyContainerHeight.constant = QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: message!, view: self.view)
            }
            else {
                imageIncomingCell.constraint_replyContainerHeight.constant = 0
                imageIncomingCell.vwReplyContainer.isHidden = true
            }
            
            if (message?.text?.contains(constantVC.GeneralConstants.stickerKey))! {
                let msgTextArr = message?.text?.components(separatedBy: "*")
                if msgTextArr?.count == 2 {
                    if let stickerName = msgTextArr![1] as? String {
                        if QMReplyMsgManager.shared.isContainReply(message: message!) {
                            chatCell.containerView?.bgColor = UIColor.white
                            chatCell.containerView?.highlightColor = UIColor.white
                        }else {
                            chatCell.containerView?.bgColor = UIColor.clear
                            chatCell.containerView?.highlightColor = UIColor.clear
                        }
                        imageIncomingCell.previewImageView.contentMode = .scaleAspectFit
                        imageIncomingCell.previewImageView.image = UIImage(named: stickerName)
                    }
                }
            } else {
                chatCell.containerView?.bgColor = UIColor.white
                chatCell.containerView?.highlightColor = UIColor.white
                
                if QMReplyMsgManager.shared.isContainCaption(message: message!) {
                if let caption = message?.text {
                    imageIncomingCell.lblCaption.text = caption
                    var attributedStringCaption: NSAttributedString?
                    attributedStringCaption = QMReplyMsgManager.shared.getAttributedString(forItem: message , msgText: caption)
                    let sizeOfCaption = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringCaption, withConstraints: CGSize(width: self.view.frame.size.width - 150.0 , height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
                    imageIncomingCell.constraint_lblCaptionHeight.constant = sizeOfCaption.height
                     }
                }else {
                    imageIncomingCell.constraint_lblCaptionHeight.constant = 0.0
                    imageIncomingCell.lblCaption.text = ""
                }
                mediaController.configureView(imageIncomingCell , with: message!)
            }
        }
        
        if let AudioOutgoingCell = cell as? QMAudioOutgoingCell {
            self.collectionView(collectionView , dynamicSizeAt: indexPath , maxWidth: self.view.frame.size.width)
            
            if QMReplyMsgManager.shared.isContainReply(message: message!) {
                AudioOutgoingCell.vwReplyContainer.isHidden = false
                 AudioOutgoingCell.vwReplyContainer.tag = indexPath.row
               
                if QMReplyMsgManager.shared.getReplyMsgId(message: message!) != "" {
                    self.addTabGestureToReplyContainer(view: AudioOutgoingCell.vwReplyContainer, msg: QMReplyMsgManager.shared.getReplyMsgId(message: message!))
                }
                AudioOutgoingCell.lblUserName?.text = QMReplyMsgManager.shared.getReplyUserName(message: message!)
                AudioOutgoingCell.lblReplyToMsg?.text = QMReplyMsgManager.shared.getReplyMsgToText(message: message!)
                if QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!) != "" {
                    let dataDecoded : Data = Data(base64Encoded: QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!), options: .ignoreUnknownCharacters)!
                    let decodedimage = UIImage(data: dataDecoded)
                    AudioOutgoingCell.imgVwThumbnail?.image = decodedimage
                }
                if QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!) != "" {
                    AudioOutgoingCell.imgVwThumbnail?.image = UIImage(named: QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!))
                }
                AudioOutgoingCell.constraint_replyContainerHeight.constant = QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: message!, view: self.view)
            }
            else {
                AudioOutgoingCell.constraint_replyContainerHeight.constant = 0
                AudioOutgoingCell.vwReplyContainer.isHidden = true
            }
            let text = message?.dateSent != nil ? messageTimeDateFormatter.string(from: (message?.dateSent!)!) : ""
            AudioOutgoingCell.lbl_Time.text = text
            AudioOutgoingCell.lbl_msgStatus.text = self.statusStringFromMessage(message: message!)
            AudioOutgoingCell.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(SessionManager.getPhone()).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
            mediaController.configureView(AudioOutgoingCell , with: message!)
        }
        if let AudioIncomingCell = cell as? QMAudioIncomingCell {
            
            if QMReplyMsgManager.shared.isContainReply(message: message!) {
                AudioIncomingCell.vwReplyContainer.isHidden = false
                 AudioIncomingCell.vwReplyContainer.tag = indexPath.row
               
                if QMReplyMsgManager.shared.getReplyMsgId(message: message!) != "" {
                    self.addTabGestureToReplyContainer(view: AudioIncomingCell.vwReplyContainer, msg: QMReplyMsgManager.shared.getReplyMsgId(message: message!))
                }
                AudioIncomingCell.lblUserName?.text = QMReplyMsgManager.shared.getReplyUserName(message: message!)
                AudioIncomingCell.lblReplyToMsg?.text = QMReplyMsgManager.shared.getReplyMsgToText(message: message!)
                if QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!) != "" {
                    let dataDecoded : Data = Data(base64Encoded: QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!), options: .ignoreUnknownCharacters)!
                    let decodedimage = UIImage(data: dataDecoded)
                    AudioIncomingCell.imgVwThumbnail?.image = decodedimage
                }
                if QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!) != "" {
                    AudioIncomingCell.imgVwThumbnail?.image = UIImage(named: QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!))
                }
                AudioIncomingCell.constraint_replyContainerHeight.constant = QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: message!, view: self.view)
            }
            else {
                AudioIncomingCell.constraint_replyContainerHeight.constant = 0
                AudioIncomingCell.vwReplyContainer.isHidden = true
            }
            
            chatCell.containerView?.bgColor = UIColor.white
            chatCell.containerView?.highlightColor = UIColor.white
            
            let text = message?.dateSent != nil ? messageTimeDateFormatter.string(from: (message?.dateSent!)!) : ""
            
            AudioIncomingCell.lbl_Time.text = text
            AudioIncomingCell.lbl_msgStatus.text = ""
            AudioIncomingCell.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(self.opponentNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
            mediaController.configureView(AudioIncomingCell , with: message!)
        }
        
        if let ChatReplyOutgoingCell = cell as? QMChatReplyOutgoingCell {
            var stickerName:String = ""
            var thumbnailBase64:String = ""
            var replyToMsgType:String = ""
            ChatReplyOutgoingCell.vwReplyContainer!.tag = indexPath.row
            if QMReplyMsgManager.shared.getReplyMsgId(message: message!) != "" {
                self.addTabGestureToReplyContainer(view: ChatReplyOutgoingCell.vwReplyContainer!, msg: QMReplyMsgManager.shared.getReplyMsgId(message: message!))
            }
            
            var attributedStringMsgToReply: NSAttributedString?
            if let customParam = message?.customParameters {
               if let StrMsgDetail = customParam.object(forKey: MsgToReplyKeys.replyMsg) as? String {
                    let dic = QuickBloxManager().convertJsonStringToDictionary(text: StrMsgDetail)
                    if let dicReply = dic {
                        
                         
                         
                         if let msgText = dicReply[MsgToReplyKeys.msg] as? String {
                              ChatReplyOutgoingCell.lblMsgToReply?.text = msgText
                         }
                         
                        if let msgToReplytext = dicReply[MsgToReplyKeys.msg] as? String {
                            attributedStringMsgToReply = QMReplyMsgManager.shared.getAttributedString(forItem: message , msgText: msgToReplytext)
                        }
                        if let senderNo = dicReply[MsgToReplyKeys.senderNumber] as? String {
                         if customParam.object(forKey: MsgToReplyKeys.isMsgFromStories) != nil{
                              if let replyNo = dicReply[MsgToReplyKeys.replyNumber] as? String {
                               ChatReplyOutgoingCell.lblUserName?.text = SessionManager.getNameFromMyAddressBook(number: replyNo)
                              }
                              
                              if customParam.object(forKey: MsgToReplyKeys.storyType) as? String == "image"{
                                   ChatReplyOutgoingCell.lblMsgToReply?.text = ".📷 Story"
                              }
                              else{
                                   ChatReplyOutgoingCell.lblMsgToReply?.text = ".🎥 Story"
                              }
                              ChatReplyOutgoingCell.lblMsgToReply!.font = UIFont(name: "CircularStd-Bold", size: 15)!
                              self.addTabGestureToReplyContainer(view: ChatReplyOutgoingCell.vwReplyContainer!, msg: message!.id!)
                         }
                         else{
                              if senderNo == SessionManager.getPhone() {
                                   ChatReplyOutgoingCell.lblUserName?.text = "You"
                              } else {
                                   ChatReplyOutgoingCell.lblUserName?.text = SessionManager.getNameFromMyAddressBook(number: senderNo)
                              }
                              ChatReplyOutgoingCell.lblMsgToReply!.font = UIFont(name: "CircularStd-Book", size: 14)!
                         }
                         
                        }
                         
                        if let type = dicReply[MsgToReplyKeys.msgType] as? String {
                            replyToMsgType = type
                        }
                        
                        if let thumbnail = dicReply[MsgToReplyKeys.thumbnail] as? String {
                            thumbnailBase64 = thumbnail
                        }
                        
                        if let stickerName_ = dicReply[MsgToReplyKeys.stickerName] as? String {
                            stickerName = stickerName_
                        }
                    }
                }
            }
          
            if let msg = message?.text {
                ChatReplyOutgoingCell.lblMsgText?.text = msg
            }
            let dateStr = self.bottomLabelAttributedString(forItem: message!)
            ChatReplyOutgoingCell.lblTime?.attributedText = dateStr
            if replyToMsgType == MsgToReplyKeys.TEXT {
                ChatReplyOutgoingCell.imgVwThumbnail?.isHidden = true
               ChatReplyOutgoingCell.imageWidth?.constant = 0
                var heightToSet:CGFloat = 0.0
                let sizeOfMsgToReply = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringMsgToReply, withConstraints: CGSize(width: self.view.frame.size.width - 100.0, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
                if (sizeOfMsgToReply.height + 30.0) < ReplyContainer.maxReplyContainerHeight {
                    heightToSet = (sizeOfMsgToReply.height + 30.0)
                } else {
                    heightToSet = ReplyContainer.maxReplyContainerHeight
                }
                ChatReplyOutgoingCell.constraintReplyVw_height?.constant = heightToSet
            }
            if replyToMsgType == MsgToReplyKeys.IMAGE || replyToMsgType == MsgToReplyKeys.VIDEO {
                ChatReplyOutgoingCell.imgVwThumbnail?.isHidden = false
               ChatReplyOutgoingCell.imageWidth?.constant = 50
                ChatReplyOutgoingCell.constraintReplyVw_height?.constant = ReplyContainer.maxReplyContainerHeight
                
                if thumbnailBase64 != "" {
                    let dataDecoded : Data = Data(base64Encoded: thumbnailBase64, options: .ignoreUnknownCharacters)!
                    let decodedimage = UIImage(data: dataDecoded)
                    ChatReplyOutgoingCell.imgVwThumbnail?.image = decodedimage
                }
            }
            if replyToMsgType == MsgToReplyKeys.STICKER {
                ChatReplyOutgoingCell.constraintReplyVw_height?.constant = ReplyContainer.minReplyContainerHeight
                ChatReplyOutgoingCell.lblMsgToReply?.text      = "Sticker"
                ChatReplyOutgoingCell.imgVwThumbnail?.isHidden = false
               ChatReplyOutgoingCell.imageWidth?.constant = 50
                ChatReplyOutgoingCell.imgVwThumbnail?.image    = UIImage(named: stickerName)
            }
            if replyToMsgType == MsgToReplyKeys.AUDIO || replyToMsgType == MsgToReplyKeys.CONTACT {
                ChatReplyOutgoingCell.imgVwThumbnail?.isHidden = true
                ChatReplyOutgoingCell.imageWidth?.constant = 0
                ChatReplyOutgoingCell.constraintReplyVw_height?.constant = ReplyContainer.maxReplyContainerHeight
            }
        }
        if let ChatReplyIncomingCell = cell as? QMChatReplyIncomingCell {
            chatCell.containerView?.bgColor = UIColor.white
            chatCell.containerView?.highlightColor = UIColor.white
             ChatReplyIncomingCell.vwReplyContainer!.tag = indexPath.row
            if QMReplyMsgManager.shared.getReplyMsgId(message: message!) != "" {
                self.addTabGestureToReplyContainer(view: ChatReplyIncomingCell.vwReplyContainer!, msg: QMReplyMsgManager.shared.getReplyMsgId(message: message!))
            }
            var attributedStringMsgToReply: NSAttributedString?
            var thumbnailBase64:String = ""
            var replyToMsgType:String = ""
            var stickerName:String = ""
            // yellow
            if let customParam = message?.customParameters {
               if let StrMsgDetail = customParam.object(forKey: MsgToReplyKeys.replyMsg) as? String {
                   
                    let dic = QuickBloxManager().convertJsonStringToDictionary(text: StrMsgDetail)
                    if let dicReply = dic {
                         if let msgText = dicReply[MsgToReplyKeys.msg] as? String {
                              ChatReplyIncomingCell.lblMsgToReply?.text = msgText
                         }
                        if let msgToReplytext = dicReply[MsgToReplyKeys.msg] as? String {
                            attributedStringMsgToReply = QMReplyMsgManager.shared.getAttributedString(forItem: message , msgText: msgToReplytext)
                        }
                        if let senderNo = dicReply[MsgToReplyKeys.senderNumber] as? String {
                         if customParam.object(forKey: MsgToReplyKeys.isMsgFromStories) != nil{
                              ChatReplyIncomingCell.lblUserName?.text = "You"
                              self.addTabGestureToReplyContainer(view: ChatReplyIncomingCell.vwReplyContainer!, msg: message!.id!)
                            // print(customParam.object(forKey: MsgToReplyKeys.storyType) as? String)
                              if customParam.object(forKey: MsgToReplyKeys.storyType) as? String == "image"{
                                   ChatReplyIncomingCell.lblMsgToReply?.text = ".📷 Story"
                              }
                              else{
                                   ChatReplyIncomingCell.lblMsgToReply?.text = ".🎥 Story"
                              }
                               ChatReplyIncomingCell.lblMsgToReply!.font = UIFont(name: "CircularStd-Bold", size: 15)!
                         }else{
                            if senderNo == SessionManager.getPhone() {
                                ChatReplyIncomingCell.lblUserName?.text = "You"
                            } else {
                                ChatReplyIncomingCell.lblUserName?.text = SessionManager.getNameFromMyAddressBook(number: senderNo)
                            }
                               ChatReplyIncomingCell.lblMsgToReply!.font = UIFont(name: "CircularStd-Book", size: 14)!
                         }
                        }
                        if let type = dicReply[MsgToReplyKeys.msgType] as? String {
                            replyToMsgType = type
                        }
                        if let thumbnail = dicReply[MsgToReplyKeys.thumbnail] as? String {
                            thumbnailBase64 = thumbnail
                        }
                        if let stickerName_ = dicReply[MsgToReplyKeys.stickerName] as? String {
                            stickerName = stickerName_
                        }
                    }
                }
            }
            
            if let msg = message?.text {
                ChatReplyIncomingCell.lblMsg?.text = msg
            }
            
            let text = message?.dateSent != nil ? messageTimeDateFormatter.string(from: (message?.dateSent!)!) : ""
            ChatReplyIncomingCell.lblTime?.text = text
            
            if replyToMsgType == MsgToReplyKeys.TEXT || replyToMsgType == MsgToReplyKeys.MEDIA {
                ChatReplyIncomingCell.imgVwthumbnail?.isHidden = true
               ChatReplyIncomingCell.imageWidth?.constant = 0
                let sizeOfMsgToReply = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringMsgToReply, withConstraints: CGSize(width: self.view.frame.size.width - 100.0, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
                
                var heightToSet:CGFloat = 0.0
                if (sizeOfMsgToReply.height + 30.0) < ReplyContainer.maxReplyContainerHeight {
                    heightToSet = (sizeOfMsgToReply.height + 30.0)
                } else {
                    heightToSet = ReplyContainer.maxReplyContainerHeight
                }
                ChatReplyIncomingCell.constraint_vwReplyContainer_height?.constant = heightToSet
            }
            
            if replyToMsgType == MsgToReplyKeys.IMAGE || replyToMsgType == MsgToReplyKeys.VIDEO {
                ChatReplyIncomingCell.imgVwthumbnail?.isHidden = false
               ChatReplyIncomingCell.imageWidth?.constant = 50
                
                if thumbnailBase64 != "" {
                    let dataDecoded : Data = Data(base64Encoded: thumbnailBase64, options: .ignoreUnknownCharacters)!
                    let decodedimage = UIImage(data: dataDecoded)
                    ChatReplyIncomingCell.imgVwthumbnail?.image = decodedimage
                }
            }
            
            if replyToMsgType == MsgToReplyKeys.STICKER {
                ChatReplyIncomingCell.lblMsgToReply?.text = "Sticker"
                ChatReplyIncomingCell.imgVwthumbnail?.isHidden = false
               ChatReplyIncomingCell.imageWidth?.constant = 50
                ChatReplyIncomingCell.imgVwthumbnail?.image = UIImage(named: stickerName)
            }
            
            //show username of message sender in group
            if self.dialog.type == QBChatDialogType.group {
                ServicesManager.instance().usersService.getUserWithID( message!.senderID as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                    if let no = task.result?.phone {
                        ChatReplyIncomingCell.constraint_vwReplyContainer_top?.constant = 22
                        ChatReplyIncomingCell.lblMsgSenderName?.text = SessionManager.getNameFromMyAddressBook(number:  no)
                    }
                    return nil
                })
            } else {
                ChatReplyIncomingCell.lblMsgSenderName?.text = ""
                ChatReplyIncomingCell.constraint_vwReplyContainer_top?.constant = 2
            }
        }
        
        if let ContactOutgoingCell = cell as? TamContactOutgoingCell {
            ContactOutgoingCell.lbl_contactText.text = ""
            ContactOutgoingCell.contactAI.isHidden = true
            
            if QMReplyMsgManager.shared.isContainReply(message: message!) {
                ContactOutgoingCell.vwReplyContainer.isHidden = false
                 ContactOutgoingCell.vwReplyContainer!.tag = indexPath.row
                
                if QMReplyMsgManager.shared.getReplyMsgId(message: message!) != "" {
                    self.addTabGestureToReplyContainer(view: ContactOutgoingCell.vwReplyContainer, msg: QMReplyMsgManager.shared.getReplyMsgId(message: message!))
                }
                
                ContactOutgoingCell.lblUserName?.text = QMReplyMsgManager.shared.getReplyUserName(message: message!)
                ContactOutgoingCell.lblReplyToMsg?.text = QMReplyMsgManager.shared.getReplyMsgToText(message: message!)
                if QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!) != "" {
                    let dataDecoded : Data = Data(base64Encoded: QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!), options: .ignoreUnknownCharacters)!
                    let decodedimage = UIImage(data: dataDecoded)
                    ContactOutgoingCell.imgVwThumbnail?.image = decodedimage
                }
                if QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!) != "" {
                    ContactOutgoingCell.imgVwThumbnail?.image = UIImage(named: QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!))
                }
                ContactOutgoingCell.constraint_replyContainerHeight.constant = QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: message!, view: self.view)
            }
            else {
                ContactOutgoingCell.constraint_replyContainerHeight.constant = 0
                ContactOutgoingCell.vwReplyContainer.isHidden = true
            }
            
            let text = message?.dateSent != nil ? messageTimeDateFormatter.string(from: (message?.dateSent!)!) : ""
          
            ContactOutgoingCell.lbl_Time.text = text
            ContactOutgoingCell.lbl_msgStatus.text = self.statusStringFromMessage(message: message!)
            ContactOutgoingCell.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(SessionManager.getPhone()).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
            
            //get contact data
            if let attachment = message?.attachments?.first {
                if let id = attachment.id {
                    if self.Is_FileAvailableIn_DocumentDirectory(fileName: id) {
                        //load from directory
                        if let path = self.getFilePath_DocumentDirectory(fileName: id) as? String {
                            
                            self.arrCOntactMessage = []
                            let contactData = NSData(contentsOf:  NSURL(fileURLWithPath: path) as URL)!
                            do {
                                let contacts = try CNContactVCardSerialization.contacts(with: contactData as Data)
                                ContactOutgoingCell.arrContacts = NSMutableArray(array: contacts)
                                
                                if contacts.count > 0 {
                                    let conTa = contacts[0]
                                    self.arrCOntactMessage = contacts
                                    if contacts.count > 1{
                                        ContactOutgoingCell.lbl_contactText.text = "\(conTa.givenName) & \(contacts.count-1) other"
                                    }
                                    else{
                                        ContactOutgoingCell.lbl_contactText.text = conTa.givenName
                                    }
                                }
                            } catch let error as NSError {
                            }
                        }
                    }
                    else {
                        //get from Url
                        ContactOutgoingCell.contactAI.isHidden = false
                        ContactOutgoingCell.contactAI.startAnimating()
                        
                        if let url = attachment.url {
                            let ContactUrl:URL = URL(string: url)!
                            
                            DispatchQueue.global(qos: .userInitiated).async {
                                let contactData:NSData = NSData(contentsOf: ContactUrl)!
                                
                                self.saveFile_DocumentDirectory(fileName: id, fileData: contactData as Data , completion: { (isSaved) in
                                    
                                    if isSaved! {
                                        // When from background thread, UI needs to be updated on main_queue
                                        DispatchQueue.main.async {
                                            do {
                                                //self.arrCOntactMessage = []
                                                ContactOutgoingCell.contactAI.isHidden = true
                                                ContactOutgoingCell.contactAI.stopAnimating()
                                                
                                                let contacts = try CNContactVCardSerialization.contacts(with: contactData as Data)
                                                ContactOutgoingCell.arrContacts = NSMutableArray(array: contacts)
                                                
                                                if contacts.count > 0 {
                                                    let conTa = contacts[0]
                                                    self.arrCOntactMessage = contacts
                                                    if contacts.count > 1{
                                                        ContactOutgoingCell.lbl_contactText.text = "\(conTa.givenName) & \(contacts.count-1) other"
                                                    }
                                                    else{
                                                        ContactOutgoingCell.lbl_contactText.text = conTa.givenName
                                                    }
                                                }
                                                
                                            } catch let error as NSError {
                                                ContactOutgoingCell.contactAI.isHidden = true
                                                ContactOutgoingCell.contactAI.stopAnimating()
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }
                } else {
                    ContactOutgoingCell.contactAI.isHidden = false
                    ContactOutgoingCell.contactAI.startAnimating()
                }
            }
        }
        
        if let ContactInComingCell = cell as? TamContactIncomingCell {
            
            ContactInComingCell.lbl_contactText.text = ""
            ContactInComingCell.contactAI.isHidden = true
            chatCell.containerView?.bgColor = UIColor.white
            chatCell.containerView?.highlightColor = UIColor.white
            
            if QMReplyMsgManager.shared.isContainReply(message: message!) {
                ContactInComingCell.vwReplyContainer.isHidden = false
                 ContactInComingCell.vwReplyContainer!.tag = indexPath.row
        
                if QMReplyMsgManager.shared.getReplyMsgId(message: message!) != "" {
                    self.addTabGestureToReplyContainer(view: ContactInComingCell.vwReplyContainer, msg: QMReplyMsgManager.shared.getReplyMsgId(message: message!))
                }
                ContactInComingCell.lblUserName?.text = QMReplyMsgManager.shared.getReplyUserName(message: message!)
                ContactInComingCell.lblReplyToMsg?.text = QMReplyMsgManager.shared.getReplyMsgToText(message: message!)
                if QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!) != "" {
                    let dataDecoded : Data = Data(base64Encoded: QMReplyMsgManager.shared.getReplyImageThumbnailStr(message: message!), options: .ignoreUnknownCharacters)!
                    let decodedimage = UIImage(data: dataDecoded)
                    ContactInComingCell.imgVwThumbnail?.image = decodedimage
                }
                if QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!) != "" {
                    ContactInComingCell.imgVwThumbnail?.image = UIImage(named: QMReplyMsgManager.shared.getReplyImageStickerNameStr(message: message!))
                }
                ContactInComingCell.constraint_replyContainerHeight.constant = QMReplyMsgManager.shared.getReplyContainerHeightToSet(message: message!, view: self.view)
            }
            else {
                ContactInComingCell.constraint_replyContainerHeight.constant = 0
                ContactInComingCell.vwReplyContainer.isHidden = true
            }
            
            let text = message?.dateSent != nil ? messageTimeDateFormatter.string(from: (message?.dateSent!)!) : ""
            
            ContactInComingCell.lbl_Time.text = text
            ContactInComingCell.lbl_msgStatus.text = self.statusStringFromMessage(message: message!)
            ContactInComingCell.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(self.opponentNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
            
            //get contact data
            if let attachment = message?.attachments?.first {
                if self.Is_FileAvailableIn_DocumentDirectory(fileName: "\(attachment.id!)") {
                    //load from directory
                    if let path = self.getFilePath_DocumentDirectory(fileName: "\(attachment.id!)") as? String {
                        
                        self.arrCOntactMessage = []
                        let contactData = NSData(contentsOf:  NSURL(fileURLWithPath: path) as URL)!
                        do {
                            let contacts = try CNContactVCardSerialization.contacts(with: contactData as Data)
                            ContactInComingCell.arrContacts = NSMutableArray(array: contacts)
                            var conTa = CNContact()
                            
                            if contacts.count > 0{
                                conTa = contacts[0]
                            }
                            
                            self.arrCOntactMessage = contacts
                            if contacts.count > 1{
                                ContactInComingCell.lbl_contactText.text = "\(conTa.givenName) & \(contacts.count-1) other"
                            }
                            else{
                                ContactInComingCell.lbl_contactText.text = conTa.givenName
                            }
                        } catch let error as NSError {
                        }
                    }
                }
                else {
                    //get from Url
                    ContactInComingCell.contactAI.isHidden = false
                    ContactInComingCell.contactAI.startAnimating()
                    
                    if let url = attachment.url {
                        let ContactUrl:URL = URL(string: url)!
                         print(attachment.url)
                        DispatchQueue.global(qos: .userInitiated).async {
                            
                            let contactData:NSData = NSData(contentsOf: ContactUrl)!
                            
                            self.saveFile_DocumentDirectory(fileName: "\(attachment.id!)", fileData: contactData as Data , completion: { (isSaved) in
                                
                                if isSaved! {
                                    // When from background thread, UI needs to be updated on main_queue
                                    DispatchQueue.main.async {
                                        do {
                                            //self.arrCOntactMessage = []
                                            ContactInComingCell.contactAI.isHidden = true
                                            ContactInComingCell.contactAI.stopAnimating()
                                            
                                            let contacts = try CNContactVCardSerialization.contacts(with: contactData as Data)
                                            ContactInComingCell.arrContacts = NSMutableArray(array: contacts)
                                            var conTa = CNContact()
                                            
                                            if contacts.count > 0{
                                                conTa = contacts[0]
                                            }
                                            self.arrCOntactMessage = contacts
                                            if contacts.count > 1{
                                                ContactInComingCell.lbl_contactText.text = "\(conTa.givenName) & \(contacts.count-1) other"
                                            }
                                            else{
                                                ContactInComingCell.lbl_contactText.text = conTa.givenName
                                            }
                                        } catch let error as NSError {
                                            ContactInComingCell.contactAI.isHidden = true
                                            ContactInComingCell.contactAI.stopAnimating()
                                        }
                                    }
                                }
                            })
                        }
                    }
                }
            }
        }
        // black
        if cell is QMChatIncomingCell || cell is QMChatAttachmentIncomingCell {
            chatCell.containerView?.bgColor = UIColor.white
            chatCell.containerView?.highlightColor = UIColor.white
        }
        else if cell is QMChatAttachmentOutgoingCell {
            chatCell.containerView?.bgColor = UIColor(red: 10.0/255.0, green: 95.0/255.0, blue: 255.0/255.0, alpha: 1.0)
            chatCell.containerView?.highlightColor = UIColor(red: 10.0/255.0, green: 95.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        }
        else if cell is QMChatNotificationCell {
            cell.isUserInteractionEnabled = false
            if message?.callNotificationState == QMCallNotificationState.missedNoAnswer {
                chatCell.containerView?.bgColor = UIColor(red: 253.0 / 255.0, green: 227.0 / 255.0, blue: 234.0 / 255.0, alpha: 1.0)
            } else {
                chatCell.containerView?.bgColor = UIColor.white //UIColor(red: 242.0 / 255.0, green: 244.0 / 255.0, blue: 245.0 / 255.0, alpha: 1.0)
            }
        }
    }
    
    func ConfigureChatContainer(status: QMMessageStatus , chatCell: QMChatCell? , message: QBChatMessage){
        
        switch status {
        case .sent:
            
            if let id = message.id {
                if TamOfflineMessagesManager.shared.isOfflineAddedMsg(msgID: id) {
                   let NotSendMsg = ServicesManager.instance().chatService.messagesMemoryStorage.message(withID: id , fromDialogID: self.dialogID())
                    if NotSendMsg != nil && Connectivity.isConnectedToInternet() {
                        self.sendMessage(message: NotSendMsg!)
                    }
                    chatCell?.containerView?.bgColor = UIColor(red: 254.6/255.0, green: 30.3/255.0, blue: 12.5/255.0, alpha: 1.0)
                    chatCell?.containerView?.highlightColor = UIColor(red: 254.6/255.0, green: 30.3/255.0, blue: 12.5/255.0, alpha: 1.0)
                }
                else {
                    chatCell?.containerView?.bgColor = UIColor(red: 67/255, green:179/255, blue: 255/255, alpha: 1.0)
                    chatCell?.containerView?.highlightColor = UIColor(red: 67/255, green:179/255, blue: 255/255, alpha: 1.0)
                }
            } else {
                chatCell?.containerView?.bgColor = UIColor(red: 67/255, green:179/255, blue: 255/255, alpha: 1.0)
                chatCell?.containerView?.highlightColor = UIColor(red: 67/255, green:179/255, blue: 255/255, alpha: 1.0)
            }
        case .sending:
            chatCell?.containerView?.bgColor = UIColor(red: 166.3/255.0, green: 171.5/255.0, blue: 171.8/255.0, alpha: 1.0)
            chatCell?.containerView?.highlightColor = UIColor(red: 166.3/255.0, green: 171.5/255.0, blue: 171.8/255.0, alpha: 1.0)
        case .notSent:
            if Connectivity.isConnectedToInternet() {
                chatCell?.containerView?.bgColor = UIColor(red: 166.3/255.0, green: 171.5/255.0, blue: 171.8/255.0, alpha: 1.0)
                chatCell?.containerView?.highlightColor = UIColor(red: 166.3/255.0, green: 171.5/255.0, blue: 171.8/255.0, alpha: 1.0)
            } else {
                if let id = message.id {
                    TamOfflineMessagesManager.shared.saveUnsentMsgsIds(msgID: id)
                }
                
                chatCell?.containerView?.bgColor = UIColor(red: 254.6/255.0, green: 30.3/255.0, blue: 12.5/255.0, alpha: 1.0)
                chatCell?.containerView?.highlightColor = UIColor(red: 254.6/255.0, green: 30.3/255.0, blue: 12.5/255.0, alpha: 1.0)
            }
        }
    }
    
        
    //MARK:- Document Directory Methods
    func saveFile_DocumentDirectory(fileName: String , fileData: Data , completion: @escaping ((_ isSaved: Bool?) -> Void)) -> Void{
        let fileManager = FileManager.default
        do {
            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
            let fileURL = documentDirectory.appendingPathComponent(fileName)
            if let data = fileData as? Data {
                try data.write(to: fileURL)
                completion(true)
            }
        } catch {
            completion(false)
        }
    }
    
    
    func Is_FileAvailableIn_DocumentDirectory(fileName: String) -> Bool{
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        
        let filePath = url.appendingPathComponent(fileName).path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath) {
            return true
        } else {
            return false
        }
    }
    
    
    func getFilePath_DocumentDirectory(fileName: String) -> String {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        
        let filePath = url.appendingPathComponent(fileName).path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath) {
            return filePath
        } else {
            return ""
        }
    }
    
    /**
     Allows to copy text from QMChatIncomingCell and QMChatOutgoingCell
     */
    
    func showOptionsForMediaMsg(){
       self.vwReplyCopy?.isHidden = true
       self.constraint_vwReplyCopy_height?.constant = 0
       self.constraint_replyContainerHeight?.constant = 120
    }
    
    func showOptionForTextMsg(){
        self.vwReplyCopy?.isHidden = false
        self.constraint_vwReplyCopy_height?.constant = 30
        self.constraint_replyContainerHeight?.constant = 150
    }
    
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        
        UIMenuController.shared.isMenuVisible = false
        
        guard let item = self.chatDataSource.message(for: indexPath) else {
            return false
        }
        let viewClass: AnyClass = self.viewClass(forItem: item) as AnyClass
        
        if  viewClass === QMChatNotificationCell.self ||
            viewClass === QMChatContactRequestCell.self {
            return false
        }
        
        //hide copy for media message
        if item.isMediaMessage() || (item.text?.contains(constantVC.GeneralConstants.stickerKey))! {
            self.showOptionsForMediaMsg()
        } else {
            self.showOptionForTextMsg()
        }
        
        self.selectedViewClass = viewClass
        self.selectedIndexPath = indexPath
        self.dismissKeyboard()
        self.show_HideReplyView(isHidden: false)
    
        if let cell = collectionView.cellForItem(at: indexPath)  {
            cell.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        }
      //  collectionView.deselectItem(at: indexPath , animated: true)
        
        return false
        // return super.collectionView(collectionView, canPerformAction: action, forItemAt: indexPath, withSender: sender)
    }
    
    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        
        print("performAction------")
        
        //UIMenuController.shared.isMenuVisible = false
        
        //        let item = self.chatDataSource.message(for: indexPath)
        //
        //        if (item?.isMediaMessage())! {
        //            ServicesManager.instance().chatService.chatAttachmentService.localImage(forAttachmentMessage: item!, completion: { (image) in
        //
        //                if image != nil {
        //                    guard let imageData = UIImageJPEGRepresentation(image!, 1) else { return }
        //
        //                    let pasteboard = UIPasteboard.general
        //
        //                    pasteboard.setValue(imageData, forPasteboardType:kUTTypeJPEG as String)
        //                }
        //            })
        //        }
        //        else {
        //            UIPasteboard.general.string = item?.text
        //        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let _ = collectionView.cellForItem(at: indexPath) else { return }
        print("didSelectItemAt")
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let lastSection = self.collectionView!.numberOfSections - 1
        
        if (indexPath.section == lastSection && indexPath.item == (self.collectionView?.numberOfItems(inSection: lastSection))! - 1) {
            // the very first message
            // load more if exists
            // Getting earlier messages for chat dialog identifier.
            guard let dialogID = self.dialog.id else {
                return super.collectionView(collectionView, cellForItemAt: indexPath)
            }
            DispatchQueue.global(qos: .background).async {
                ServicesManager.instance().chatService.loadEarlierMessages(withChatDialogID: dialogID).continueWith(block: {[weak self](task) -> Any? in
                    
                    guard let strongSelf = self else { return nil }
                    if (task.result?.count ?? 0 > 0) {
                        strongSelf.chatDataSource.add(task.result as! [QBChatMessage]!)
                    }
                    
                    return nil
                })
            }
        }
        
        //update unread count in cache
        if self.isUnreadCountAvailable {
            self.isUnreadCountAvailable = false
            ServicesManager.instance().chatService.updateUnreadCount(inMemory: self.dialogID())
        }
        
        // marking message as read if needed
        if let message = self.chatDataSource.message(for: indexPath) {
            self.sendReadStatusForMessage(message: message)
        }
        
        return super.collectionView(collectionView, cellForItemAt
            : indexPath)
    }
    
    override func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                 at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TamHeaderCell", for: indexPath)
            return headerView
            
        case UICollectionElementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TamHeaderCell", for: indexPath) as! TamHeaderCell
            footerView.vwSecure.layer.cornerRadius = 6.0
            footerView.btnSecureMsgText1.setTitle("Messages to this chat and calls are now".localized , for: .normal)
            footerView.lblSecureMsgText2.text = "secured with end-to-end encryption.".localized
            return footerView
        default:
            assert(false, "Unexpected element kind")
        }
        return UICollectionReusableView()
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 0, height: 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if !(self.isHeaderLoaded) {
            return CGSize(width: 0, height: 0)
        }
        return CGSize(width: collectionView.frame.size.width , height: 50.0)
    }
    
    
    // MARK: - STICKERS
    @objc func didPressStickerOption(sender: UIButton!) {
        
        //for previous category selection
        if self.previousSelectedOption != nil {
            self.previousSelectedOption?.isHidden = true
        } else {
            self.vwStickerSelection3?.isHidden = true
        }
        
        //for category selection
        self.selectedCategoryIndex = sender.tag
        
        switch sender.tag {
        case 0:
            self.msgTextView?.becomeFirstResponder()
            self.scrollToBottom(animated: true)
            self.previousSelectedOption = self.vwStickerSelection1
        case 1:
            self.previousSelectedOption = self.vwStickerSelection2
        case 2:
            self.stcikerScollTab?.changePageToIndex(0, animated: true)
            self.previousSelectedOption = self.vwStickerSelection3
        case 3:
            self.stcikerScollTab?.changePageToIndex(1, animated: true)
            self.previousSelectedOption = self.vwStickerSelection4
        case 4:
            self.stcikerScollTab?.changePageToIndex(2, animated: true)
            self.previousSelectedOption = self.vwStickerSelection5
        case 5:
            self.stcikerScollTab?.changePageToIndex(3, animated: true)
            self.previousSelectedOption = self.vwStickerSelection6
        case 6:
            self.stcikerScollTab?.changePageToIndex(4, animated: true)
            self.previousSelectedOption = self.vwStickerSelection7
        case 7:
            self.stcikerScollTab?.changePageToIndex(5, animated: true)
            self.previousSelectedOption = self.vwStickerSelection8
        default:
            SessionManager.printLOG(data: "default")
        }
        self.previousSelectedOption?.isHidden = false
    }
    
    func sendSelected_sticker(string: String) {
        if !self.queueManager().shouldSendMessagesInDialog(withID: self.dialog.id!) {
            return
        }
        self.fireSendStopTypingIfNecessary()
        let message = QBChatMessage()
        message.text = constantVC.GeneralConstants.stickerKey + string
        message.senderID = self.senderID
        message.deliveredIDs = [(NSNumber(value: self.senderID))]
        message.readIDs = [(NSNumber(value: self.senderID))]
        message.markable = true
        message.dateSent = Date()
        
        //to reply msg
        if self.dicMsgToReply.allKeys.count > 1 {
            let dicReply = NSMutableDictionary()
            dicReply.setValue(QuickBloxManager().convertDictionaryToJsonString(dict: self.dicMsgToReply) , forKey: MsgToReplyKeys.replyMsg)
            message.customParameters = dicReply
            self.closeChatReplyView()
        }
        self.sendMessage(message: message)
    
     }
    
     func loadImageMedia(completionHandler:@escaping (Bool) -> ()){
          let extendedRequest = ["sort_desc" : "date_sent", "attachments.type" : "image"]
          let resPage = QBResponsePage(limit:100, skip: 0)
          
          QBRequest.messages(withDialogID: self.dialogID() , extendedRequest: extendedRequest , for: resPage , successBlock: { (response , msgs , page ) in
                self.arrImgMedia = msgs
                print("IMAGE MEDIA ---- \(self.arrImgMedia.count)")
                completionHandler(true)
               
          }) { (error) in
               print("ERROR IMAGE MEDIA ---- \(error.debugDescription)")
               completionHandler(false)
          }
     }
     
     func getMediaMessages(currentMsg:QBChatMessage){
       
        self.loadImageMedia { (success) in
             let vc = self.storyboard?.instantiateViewController(withIdentifier: "SlideShow_VC") as! SlideShow_VC
             vc.dialogID = self.dialogID()
             vc.arrImgMedia = self.arrImgMedia
             if let index = self.arrImgMedia.index(of: currentMsg) {
                  vc.defaultPage = index
             }
              self.present(vc, animated: true, completion: nil)
             
        }
     }
     
    // MARK: ACTabScrollViewDelegate
    func tabScrollView(_ tabScrollView: ACTabScrollView, didChangePageTo index: Int) {
        if self.previousSelectedOption != nil {
            self.previousSelectedOption?.isHidden = true
        }
        
        switch index {
        case 0:
            self.vwStickerSelection3?.isHidden = false
            self.previousSelectedOption = self.vwStickerSelection3
        case 1:
            self.vwStickerSelection4?.isHidden = false
            self.previousSelectedOption = self.vwStickerSelection4
        case 2:
            self.vwStickerSelection5?.isHidden = false
            self.previousSelectedOption = self.vwStickerSelection5
        case 3:
            self.vwStickerSelection6?.isHidden = false
            self.previousSelectedOption = self.vwStickerSelection6
        case 4:
            self.vwStickerSelection7?.isHidden = false
            self.previousSelectedOption = self.vwStickerSelection7
        case 5:
            self.vwStickerSelection8?.isHidden = false
            self.previousSelectedOption = self.vwStickerSelection8
        default:
            SessionManager.printLOG(data: "default")
        }
        self.selectedCategoryIndex = index + 2
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, didScrollPageTo index: Int) {
    }
    
    // MARK: ACTabScrollViewDataSource
    func numberOfPagesInTabScrollView(_ tabScrollView: ACTabScrollView) -> Int {
        return 6
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, tabViewForPageAtIndex index: Int) -> UIView {
        // create a label
        let label = UILabel()
        label.textColor = UIColor(red: 77.0 / 255, green: 79.0 / 255, blue: 84.0 / 255, alpha: 1)
        label.textAlignment = .center
        
        // if the size of your tab is not fixed, you can adjust the size by the following way.
        label.sizeToFit() // resize the label to the size of content
        label.frame.size = CGSize(width: label.frame.size.width + 28, height: label.frame.size.height + 36) // add some paddings
        
        return label
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, contentViewForPageAtIndex index: Int) -> UIView {
        return contentViews[index]
    }
    
    // MARK: QMChatCellDelegate
    
    /**
     Removes size from cache for item to allow cell expand and show read/delivered IDS or unexpand cell
     */
    func chatCellDidTapContainer(_ cell: QMChatCell!) {
        
        let indexPath = self.collectionView?.indexPath(for: cell)
        
        guard let currentMessage = self.chatDataSource.message(for: indexPath) else {
            return
        }
        
        let messageStatus: QMMessageStatus = self.queueManager().status(for: currentMessage)
        
        if messageStatus == .notSent {
            self.handleNotSentMessage(currentMessage, forCell:cell)
            return
        }
        
     
        self.collectionView?.collectionViewLayout.removeSizeFromCache(forItemID: currentMessage.id)
        self.collectionView?.performBatchUpdates(nil, completion: nil)
        
        let viewClass: AnyClass = self.viewClass(forItem: currentMessage) as AnyClass
        
          if viewClass == QMImageIncomingCell.self || viewClass == QMImageOutgoingCell.self {
          self.getMediaMessages(currentMsg: currentMessage)
          }
        
        if viewClass == TamContactOutgoingCell.self || viewClass == TamContactIncomingCell.self {
            if let contactOutgoingCell = cell as? TamContactOutgoingCell {
                let next = self.storyboard?.instantiateViewController(withIdentifier: "NewContactVC") as! NewContactVC
                if let contacts = contactOutgoingCell.arrContacts as? [CNContact] {
                    next.arrContacts = contacts
                }
                self.present(next, animated: true, completion: nil) 
            }
            
            if let contactIncomingCell = cell as? TamContactIncomingCell {
                let next = self.storyboard?.instantiateViewController(withIdentifier: "NewContactVC") as! NewContactVC
                if let contacts = contactIncomingCell.arrContacts as? [CNContact] {
                    next.arrContacts = contacts
                }
                self.present(next, animated: true, completion: nil)
            }
        }
    }
    
    func chatCell(_ cell: QMChatCell!, didTapAtPosition position: CGPoint) {
        self.dismissKeyboard()
    }
    
    func chatCell(_ cell: QMChatCell!, didPerformAction action: Selector!, withSender sender: Any!) {
        print("didPerformAction")
    }
    
    
    func chatCell(_ cell: QMChatCell!, didTapOn result: NSTextCheckingResult) {
        
        print("didTapOn")
        
        switch result.resultType {
            
        case NSTextCheckingResult.CheckingType.link:
            
            let strUrl : String = (result.url?.absoluteString)!
            
            let hasPrefix = strUrl.lowercased().hasPrefix("https://") || strUrl.lowercased().hasPrefix("http://")
            
            if #available(iOS 9.0, *) {
                if hasPrefix {
                    
                    let controller = SFSafariViewController(url: URL(string: strUrl)!)
                    self.present(controller, animated: true, completion: nil)
                    
                    break
                }
            }
            // Fallback on earlier versions
            
            if UIApplication.shared.canOpenURL(URL(string: strUrl)!) {
                
                UIApplication.shared.openURL(URL(string: strUrl)!)
            }
            break
            
        case NSTextCheckingResult.CheckingType.phoneNumber:
            
            if !self.canMakeACall() {
                
                SVProgressHUD.showInfo(withStatus: "Your Device can't make a phone call".localized, maskType: .none)
                break
            }
            
            let urlString = String(format: "tel:%@",result.phoneNumber!)
            let url = URL(string: urlString)
            self.view.endEditing(true)
            
            let alertController = UIAlertController(title: "",
                                                    message: result.phoneNumber,
                                                    preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "SA_STR_CANCEL".localized, style: .cancel) { (action) in
                
            }
            
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "SA_STR_CALL".localized, style: .destructive) { (action) in
                UIApplication.shared.openURL(url!)
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true) {
            }
            break
        default:
            break
        }
    }
    
    func chatCellDidTapAvatar(_ cell: QMChatCell!) {
    }
    
    // MARK: QMDeferredQueueManager
    
    func deferredQueueManager(_ queueManager: QMDeferredQueueManager, didAddMessageLocally addedMessage: QBChatMessage) {
        
        if addedMessage.dialogID == self.dialog.id {
            self.chatDataSource.add(addedMessage)
        }
    }
    
    func deferredQueueManager(_ queueManager: QMDeferredQueueManager, didUpdateMessageLocally addedMessage: QBChatMessage) {
        
        if addedMessage.dialogID == self.dialog.id {
            self.chatDataSource.update(addedMessage)
        }
    }
    
    // MARK: QMChatServiceDelegate
    /*  var lastLoadIndex:Int = 0
     var messagesLimit:Int = 20
     var localMessages:[QBChatMessage] = []
     
     func getLastLoadIndex() ->Int{
     if self.localMessages.count < messagesLimit {
     self.lastLoadIndex = 0
     return 0
     } else {
     self.lastLoadIndex = self.localMessages.count-1 - messagesLimit
     return self.lastLoadIndex
     }
     }
     
     func setMessagesCells(){
     var tempArr:[QBChatMessage] = []
     
     for index in self.lastLoadIndex...self.lastLoadIndex + self.messagesLimit {
     tempArr.append(self.localMessages[index])
     }
     
     self.lastLoadIndex = self.lastLoadIndex - messagesLimit
     self.chatDataSource.add(tempArr)
     }*/
    
    func chatService(_ chatService: QMChatService, didLoadMessagesFromCache messages: [QBChatMessage], forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            
            if !(self.progressView?.isHidden)! {
                self.stopSpinProgress()
            }
            self.chatDataSource.add(messages)
        }
    }
    
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            
            if let dialog_ = ServicesManager.instance().chatService.dialogsMemoryStorage.chatDialog(withID: dialogID) as? QBChatDialog {
                
                //delete clear chat object from custom data
                if QuickBloxManager().isClearChat(dialog: dialog_) {
                    QuickBloxManager().UnClearChat_updateDialog(dialog: dialog_) { (success) in
                    }
                }
            }
            
            // Insert message received from XMPP or self sent
            if self.chatDataSource.messageExists(message) {
                self.chatDataSource.update(message)
            }
            else {
                self.chatDataSource.add(message)
            }
        }
    }
    
    
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        
        if self.dialog.type != QBChatDialogType.private && self.dialog.id == chatDialog.id {
            self.dialog = chatDialog
            
            //if group dialog update title
            if chatDialog.type == QBChatDialogType.group {
                self.lblTitle?.text = self.dialog?.name
            }
        }
        
        if self.dialog.type == QBChatDialogType.private && self.dialog.id == chatDialog.id{
            self.dialog = chatDialog
        }
        
    }
    
    func chatService(_ chatService: QMChatService, didUpdate message: QBChatMessage, forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            self.chatDataSource.update(message)
        }
        
        //for offline messages
        if Connectivity.isConnectedToInternet() {
            let status: QMMessageStatus = self.queueManager().status(for: message)
            if status == .sent && TamOfflineMessagesManager.shared.isOfflineAddedMsg(msgID: message.id ?? "") {
                TamOfflineMessagesManager.shared.removeUnsentMsgsIds(msgID: message.id ?? "")
            }
        }
    }
    
    func chatService(_ chatService: QMChatService, didUpdate messages: [QBChatMessage], forDialogID dialogID: String) {
        
        if self.dialog.id == dialogID {
            self.chatDataSource.update(messages)
        }
    }
    
    // MARK: UITextViewDelegate
    override func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.chatInnerViewObj?.removeFromSuperview()
        return true
    }
        
    override func textViewDidChange(_ textView: UITextView) {
        //super.textViewDidChange(textView)
        
        if textView.text.trimmingCharacters(in: .whitespaces) == ""{
            self.showMicHideSendButton()
            
        } else {
            self.hideMicShowSendButton()
            //set text view growing view
            if textView.contentSize.height > textView.frame.size.height {
                
                let fixedWidth = textView.frame.size.width
                textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                var newFrame = textView.frame
                let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
                newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                
                if newSize.height > (self.inputBarView?.frame.size.height)! && newSize.height < 100 {
                    self.inputBarView_height?.constant = newSize.height
                }
            }
        }
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // Prevent crashing undo bug
        let currentCharacterCount = textView.text?.length ?? 0
        
        if (range.length + range.location > currentCharacterCount) {
            return false
        }
        
        if !QBChat.instance.isConnected { return true }
        
        if let timer = self.typingTimer {
            timer.invalidate()
            self.typingTimer = nil
        } else {
            self.sendBeginTyping()
        }
        
        self.typingTimer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(ChatViewController.fireSendStopTypingIfNecessary), userInfo: nil, repeats: false)
        
        if maxCharactersNumber > 0 {
            
            if currentCharacterCount >= maxCharactersNumber && text.length > 0 {
                self.showCharactersNumberError()
                return false
            }
            
            let newLength = currentCharacterCount + text.length - range.length
            
            if  newLength <= maxCharactersNumber || text.length == 0 {
                return true
            }
            
            let oldString = textView.text ?? ""
            let numberOfSymbolsToCut = maxCharactersNumber - oldString.length
            var stringRange = NSMakeRange(0, min(text.length, numberOfSymbolsToCut))
            
            // adjust the range to include dependent chars
            stringRange = (text as NSString).rangeOfComposedCharacterSequences(for: stringRange)
            
            // Now you can create the short string
            let shortString = (text as NSString).substring(with: stringRange)
            
            let newText = NSMutableString()
            newText.append(oldString)
            newText.insert(shortString, at: range.location)
            textView.text = newText as String
            
            self.showCharactersNumberError()
            self.textViewDidChange(textView)
            
            return false
        }
        
        return true
    }
    
    override func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type here...".localized
            textView.textColor = UIColor.lightGray
        }
        
        // super.textViewDidEndEditing(textView)
        
        // self.fireSendStopTypingIfNecessary()
    }
    
    override func textViewDidBeginEditing(_ textView: UITextView) {
        self.chatInnerViewObj?.removeFromSuperview()

        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            self.msgTextView?.textColor = UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 1.0)
        }
        
    }
    
    @objc func fireSendStopTypingIfNecessary() -> Void {
        
        if let timer = self.typingTimer {
            timer.invalidate()
        }
        
        self.typingTimer = nil
        self.sendStopTyping()
    }
    
    func sendBeginTyping() -> Void {
        self.dialog.sendUserIsTyping()
    }
    
    func sendStopTyping() -> Void {
        self.dialog.sendUserStoppedTyping()
    }
    
    // MARK: QMChatAttachmentServiceDelegate
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChange status: QMMessageAttachmentStatus, for message: QBChatMessage) {
        
        if status != QMMessageAttachmentStatus.notLoaded {
            
            if message.dialogID == self.dialog.id {
                self.chatDataSource.update(message)
            }
        }
    }
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChangeLoadingProgress progress: CGFloat, for attachment: QBChatAttachment) {
        
        if let attachmentCell = self.attachmentCellsMap.object(forKey: attachment.id! as AnyObject?) {
            // attachmentCell.updateLoadingProgress(progress)
        }
    }
    
    func chatAttachmentService(_ chatAttachmentService: QMChatAttachmentService, didChangeUploadingProgress progress: CGFloat, for message: QBChatMessage) {
        
        guard message.dialogID == self.dialog.id else {
            return
        }
        var cell = self.attachmentCellsMap.object(forKey: message.id as AnyObject?)
        
        if cell == nil && progress < 1.0 {
            
            if let indexPath = self.chatDataSource.indexPath(for: message) {
                cell = self.collectionView?.cellForItem(at: indexPath) as? QMChatAttachmentCell
                self.attachmentCellsMap.setObject(cell, forKey: message.id as AnyObject?)
            }
        }
        cell?.updateLoadingProgress(progress)
    }
    
    // MARK : QMChatConnectionDelegate
    
    func refreshAndReadMessages() {
        
        //  SVProgressHUD.show(withStatus: "SA_STR_LOADING_MESSAGES".localized, maskType: SVProgressHUDMaskType.clear)
        self.loadMessages()
        
        if let messagesToRead = self.unreadMessages {
            self.readMessages(messages: messagesToRead)
        }
        self.unreadMessages = nil
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
        self.refreshAndReadMessages()
    }
    
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
        self.refreshAndReadMessages()
    }
    
    func queueManager() -> QMDeferredQueueManager {
        return ServicesManager.instance().chatService.deferredQueueManager
    }
    
    func handleNotSentMessage(_ message: QBChatMessage,
                              forCell cell: QMChatCell!) {
        
        let alertController = UIAlertController(title: "", message: "SA_STR_MESSAGE_FAILED_TO_SEND".localized, preferredStyle:.actionSheet)
        
        let resend = UIAlertAction(title: "SA_STR_TRY_AGAIN_MESSAGE".localized, style: .default) { (action) in
            self.queueManager().perfromDefferedAction(for: message, withCompletion: nil)
        }
        alertController.addAction(resend)
        
        let delete = UIAlertAction(title: "SA_STR_DELETE_MESSAGE".localized, style: .destructive) { (action) in
            self.queueManager().remove(message)
            self.chatDataSource.delete(message)
        }
        alertController.addAction(delete)
        
        let cancelAction = UIAlertAction(title: "SA_STR_CANCEL".localized, style: .cancel) { (action) in
        }
        
        alertController.addAction(cancelAction)
        
        if alertController.popoverPresentationController != nil {
            self.view.endEditing(true)
            alertController.popoverPresentationController!.sourceView = cell.containerView
            alertController.popoverPresentationController!.sourceRect = cell.containerView.bounds
        }
        self.present(alertController, animated: true) {
        }
    }
}


extension ChatViewController : QBChatDelegate {
    
    func chatDidSetPrivacyList(withName name: String) {
        print(name)

    }
    
    func chatDidNotSetPrivacyList(withName name: String, error: Error) {
        print(name,error)

    }
    
    func chatDidReceive(_ privacyList: QBPrivacyList) {
        print(privacyList)

    }
    
    func chatDidNotReceivePrivacyListNamesDue(toError error: Error) {
        print(error)

    }
    
    func chatDidNotReceivePrivacyList(withName name: String, error: Error) {
        print(name,error)

    }
    
    func chatDidReceivePrivacyListNames(_ listNames: [String]) {
        print(listNames)
    }
    
    
}

extension ChatViewController: ChatInfoDelegate {
    
    func clearChatDelegateMethod() {
        self.clearChat()
    }
    
    func doVoiceCall() {
        self.didPressVoiceCall(sender: self.btnVoiceCall)
    }
    
    func doVideoCall() {
        self.didPressVideoCall(sender: self.btnVideoCall)
    }
    
    func doBlockUnblock(state: String , completionHandler:@escaping (Bool , String) -> ()) {
        if state == "Block" {
            
            //block from server
            if let opponentID = QuickBloxManager().getOpponentID(dialog: self.dialog) {
                QuickBloxManager().blockAUser(userQuickBloxID: "\(opponentID)" , userServerID: self.opponentServerID, completionHandler: { (success) in
                })
            }
            
            //block from firebase
            let document = SessionManager.getPhone() + "-" + self.opponentNo
            TamFirebaseManager.shared.saveDocument(collection: firebaseCollection.Block.rawValue , document: document) { (success) in
                if success {
                    completionHandler(true , "UnBlock Contact".localized)
                } else {
                    completionHandler(true , "Block Contact".localized)
                }
            }
            
            //mute from firebase
            TamFirebaseManager.shared.saveDocument(collection: firebaseCollection.Mute.rawValue , document: document) { (success) in
            }
            
            
        }else {
            
            //unblock from server
            if let opponentID = QuickBloxManager().getOpponentID(dialog: self.dialog) {
                QuickBloxManager().UnblockUser(userQuickBloxID: "\(opponentID)" , userServerID: self.opponentServerID , completionHandler: { (success) in
                })
            }
            
            //unblock from firebase
            let document = SessionManager.getPhone() + "-" + self.opponentNo
            TamFirebaseManager.shared.deleteDocument(collection: firebaseCollection.Block.rawValue , document: document) { (success) in
                if success {
                    completionHandler(true , "Block Contact".localized)
                } else {
                    completionHandler(true , "UnBlock Contact".localized)
                }
            }
            
            //unmute from firebase
            TamFirebaseManager.shared.deleteDocument(collection: firebaseCollection.Mute.rawValue , document: document) { (success) in
            }
        }
    }
    
    func updateTitleAfterContactUpdate(){
        self.updateTitle()
    }
    
}


extension ChatViewController: QBImagePickerControllerDelegate {
    
    func qb_imagePickerController(_ imagePickerController: QBImagePickerController!, didFinishPickingAssets assets: [Any]!) {
        
//        for asset in assets as? [PHAsset] ?? [] {
//            print(asset.mediaType)
//        }
        
        //navigate to preview VC
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MultiMediaSelectionPreview_VC") as! MultiMediaSelectionPreview_VC
        controller.assets = assets as? [PHAsset] ?? []
        controller.Mediadelegate = self
        if let topVC = UIApplication.topViewController() {
            topVC.present(controller, animated: false , completion: nil)
        }
    }
    
    func qb_imagePickerControllerDidCancel(_ imagePickerController: QBImagePickerController!) {
        self.dismiss(animated: true , completion: nil)
    }
    
}

extension UIView{
    func animShow(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    func animHide(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()
                        
        },  completion: {(_ completed: Bool) -> Void in
            self.isHidden = true
        })
    }
}

extension ChatViewController: MultiMediaSelectionDelegate {
    
     func didPickedMedia(assets: [PHAsset], arrMediaPreview: [MediaPreview]) {
          //send multiple messages
          for media in arrMediaPreview {
               DispatchQueue.main.async(execute: {
                    if media.MediaType == "image" {
                         if let img = media.image {
                            if let resizedImage = self.resizedImage(from: img) {
                                let attachment = QBChatAttachment.imageAttachment(with: resizedImage)
                                self.sendMessage_image_Video(with: attachment , caption: media.caption)
                            }
                         }
                    }
                    else {
                         if let img = media.image {
                              let resizedImage: UIImage? = self.resizedImage(from: img)
                              let attachment = QBChatAttachment.videoAttachment(withFileURL: URL(string: media.videoUrl)!)
                              attachment.image = resizedImage
                              self.sendMessage_image_Video(with: attachment , caption: media.caption)
                         }
                    }
               })
          }
     }

}
