//
//  MultiImageSelectionTabContainer_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 6/19/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MultiImageSelectionTabContainer_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var tabImageView: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    
    
    //MARK:- Variables
    var imageToShow:UIImage!
    var isVideo:Bool = false
    var videoUrl:String = ""
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isVideo {
            self.btnPlay.isHidden = false
        }
        
        if let img = self.imageToShow {
            self.tabImageView.image = img
        }
    }
    
    
    @IBAction func didPress_play(_ sender: UIButton) {
        self.playVideoFromLocalPath(path: self.videoUrl)
    }
    
    func playVideoFromLocalPath(path: String) {
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
   
}
