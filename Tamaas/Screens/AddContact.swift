//
//  AddContact.swift
//  Tamaas
//
//  Created by Krescent Global on 28/04/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import MessageUI
import FTIndicator
import AANotifier
import Contacts

class friendContact {
    var user_id : String?
    var name: String?
    var photo : String?
    var phoneNo : String?
    var quickBloxID : String?
}
class AddContact: BaseViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    
    //MARK:- Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraint_vwTop_height: NSLayoutConstraint!
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var btn_cancel: UIBarButtonItem!
    @IBOutlet weak var lbl_invite_friendss: UILabel!
    @IBOutlet weak var vwBlur: UIView!
    
    //MARK:- Variables
    var arrFriends = [friendContact]()
    var isSearchActive:Bool = false
    var dictParam = Dictionary<String, String>()
    var contactsStore: CNContactStore?
    
    
    //MARK:- Notifier
    lazy var ImportViewNotifier: AANotifier = {
        
        let notifierView = UIView.fromNib(nibName: "ImportContact")!
        let options: [AANotifierOptions] = [
            .position(.middle),
            .preferedHeight(330),
            .margins(H: 20, V: nil),
            .transitionA(.fromTop,0.40),
            .transitionB(.toBottom, 0.40),
            .hideOnTap
        ]
        
        let importLabel = notifierView.viewWithTag(100) as! UILabel
        let note_lbl = notifierView.viewWithTag(99) as! UILabel
        note_lbl.text = "Find your friends".localized
        importLabel.text = "importContactsMessage".localized
        let skipBtn = notifierView.viewWithTag(101) as! UIButton
        skipBtn.setTitle("Skip".localized, for: .normal)
        skipBtn.addTarget(self, action: #selector(skipAc), for: .touchUpInside)
        let importBtn = notifierView.viewWithTag(102) as! UIButton
        importBtn.setTitle("Import My Contacts".localized, for: .normal)
        importBtn.addTarget(self, action: #selector(importAc), for: .touchUpInside)
        let notifier = AANotifier(notifierView, withOptions: options)
        return notifier
    }()
    
    lazy var loadViewNotifier: AANotifier = {
        let notifierView = LoadingBar()
        let options: [AANotifierOptions] = [
            .position(.middle),
            .preferedHeight(128),
            .margins(H: 20, V: nil),
            .transitionA(.fromTop, 0.40),
            .transitionB(.toBottom, 0.40)
        ]
        
        let fetchContactLabel = notifierView.viewWithTag(300) as! UILabel
        fetchContactLabel.text = "We are fetching your contacts".localized
        
        let notifier = AANotifier(notifierView, withOptions: options)
        return notifier
    }()
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localise_language()
        self.navigationItem.setHidesBackButton(true, animated:false)
        vwTop.isHidden = false
        constraint_vwTop_height.constant = 220
        isSearchActive = false
        searchBar.delegate   = self
        tableView.delegate   = self
        tableView.dataSource = self
        tableView.keyboardDismissMode = .onDrag
        searchBar.showsCancelButton = false
        self.setBlurView(isHidden: true)
    }
    
    func localise_language(){
        self.title = "Add Contact".localized
        self.searchBar.placeholder = "Search ID, Name, Number..".localized
        self.lbl_invite_friendss.text = "Invite your friends to Tamaaas!".localized
        self.btn_cancel.title = "Cancel".localized
    }
    
    //MARK:- Notifier action methods
    @objc func skipAc() {
       
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        self.setBlurView(isHidden: true)
        ImportViewNotifier.hide()
    }
    
    @objc func importAc() {
        
        DispatchQueue.main.async {
            self.ImportViewNotifier.hide()
            self.loadViewNotifier.show()
        }
        
        DispatchQueue.global(qos: .background).async {
            self.importContacts()
        }
    }
    
    func importContacts(){
        let update_NS = Update_Get_allLinked_Contacts()
        update_NS.getContacts()
        
        ContactManager().getAddressBookAndUpdate { (success) in
            self.setBlurView(isHidden: true)
            self.loadViewNotifier.hide()
            self.navigationController?.navigationBar.isUserInteractionEnabled = true
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
            constantVC.ActiveDataSource.isActiveRefresh = true
            if !(success) {
                FTIndicator.showError(withMessage: "Error in import Contacts!")
            }
        }
    }

    //MARK:- UIButton Actions
    @IBAction func backAc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func fbAc(_ sender: Any) {
        let fbInstalled = schemeAvailable(scheme: "fb://")
        
        if (fbInstalled == true) {
            let content = ShareLinkContent()
            content.quote = "Check out this New Secure Chat App with free video chat, calls, stickers, money and more... Download Tamaaas NOW!"
            if let url = URL(string:  constantVC.GeneralConstants.appLiveLink) {
                content.contentURL = url
                ShareDialog(fromViewController: self, content: content, delegate: nil)
               // ShareDialog.show(from: self, with: content, delegate: nil)
            }
        }
        else{
            let alert = UIAlertController(title: nil, message: "You are not connected to facebook app. Please install the app and login into the App", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {  action in
                
                UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/in/app/facebook/id284882215?mt=8")! as URL)
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func gmailAc(_ sender: Any) {
        if !MFMailComposeViewController.canSendMail() {
            print("Mail services are not available")
            return
        }
        sendEmail()
    }
    @IBAction func wtsAppAc(_ sender: Any) {
        
        let originalString = "Check out this New Secure Chat App with free video chat, calls, stickers, money and more... Download Tamaaas NOW! \(constantVC.GeneralConstants.appLiveLink)"
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
        
        let url  = URL(string: "whatsapp://send?text=\(escapedString!)")
        
        if UIApplication.shared.canOpenURL(url! as URL)
        {
            UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
        }
        else{
            let alert = UIAlertController(title: nil, message: "You are not connected to whatsapp app. Please install the app and login into the App", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {  action in
                UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/in/app/whatsapp-messenger/id310633997?mt=8")! as URL)
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
       
    }
    @IBAction func emailAc(_ sender: Any) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Check out this New Secure Chat App with free video chat, calls, stickers, money and more... Download Tamaaas NOW! \(constantVC.GeneralConstants.appLiveLink)"
            controller.recipients = []
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
        else{
            print("SMS services are not available")
        }
    }
    
    @IBAction func moreAc(_ sender: Any) {
        let text = "Check out this New Secure Chat App with free video chat, calls, stickers, money and more... Download Tamaaas NOW!  \(constantVC.GeneralConstants.appLiveLink)"
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
   
    
    //MARK:- Custom methods
    
    func setBlurView(isHidden:Bool) {
        self.vwBlur.isHidden = isHidden
    }
    
    func sendEmail() {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients([""])
        composeVC.setSubject("Tamaaas")
        composeVC.setMessageBody("Check out this New Secure Chat App with free video chat, calls, stickers, money and more... Download Tamaaas NOW! \(constantVC.GeneralConstants.appLiveLink)", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func schemeAvailable(scheme: String) -> Bool {
        let shOpen =  UIApplication.shared.canOpenURL(NSURL.init(string:"fb://")! as URL)
        return shOpen
    }
    
    
    //MARK:- MFMailComposeViewController delegates
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- MFMessageComposeViewController delegates
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- UISearchBar Delegates
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearchActive = true
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //isSearchActive = false
       //constraint_vwTop_height.constant = 220
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        vwTop.isHidden = false
        constraint_vwTop_height.constant = 220
        isSearchActive = false
        searchBar.text = ""
        self.view.endEditing(true)
        self.tableView.reloadData()
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        vwTop.isHidden = true
        constraint_vwTop_height.constant = 0
        isSearchActive = true
        getfriends()
    }
     
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (searchText.count < 1) {
            constraint_vwTop_height.constant = 220
            isSearchActive = false
            vwTop.isHidden = false
            self.tableView.reloadData()
        }
        else {
        vwTop.isHidden = true
        constraint_vwTop_height.constant = 0
        isSearchActive = true
        getfriends()
        
     }
    }
    
    //MARK:- Web service implementations
    func getfriends(){
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["keyword":searchBar.text!]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_SEARCH, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    if let userDetails = response["user_details"] as? [[String:Any]]{
                        self.arrFriends = []
                        
                        for item in userDetails {
                             let friendCon = friendContact()
                            if let userName = item["name"]{
                                friendCon.name = userName as? String
                            }
                            if let id_ = item["id"]{
                                friendCon.user_id = id_ as? String
                            }
                            if let photoURL = item["photoURL"]{
                                friendCon.photo = photoURL as? String
                            }
                            if let phoneNo = item["phoneNumber"]{
                                friendCon.phoneNo = phoneNo as? String
                            }
                            if let quickBloxid = item["quickbloxID"]{
                                friendCon.quickBloxID = quickBloxid as? String
                            }
                            self.arrFriends.append(friendCon)
                        }
                        self.tableView.reloadData()
                    }
                }
                else{
                   // self.tableView.isHidden = true
                }
            }
        })
    }
    
    //MARK:- Table View Delegates
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
         if (isSearchActive) {
            
            return self.arrFriends.count
        }
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (isSearchActive) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell2", for: indexPath) as! FriendContactCell
            
            cell.imgVw_user.layer.cornerRadius = cell.imgVw_user.frame.size.width / 2
            cell.imgVw_user.clipsToBounds = true
            
           /* if SessionManager.getNameFromMyAddressBook(number: arrFriends[indexPath.row].phoneNo! ) != "" {
               cell.lbl_userName.text = SessionManager.getNameFromMyAddressBook(number: arrFriends[indexPath.row].phoneNo! )
            }else {
                cell.lbl_userName.text = arrFriends[indexPath.row].name
            }*/
            
            cell.lbl_userName.text = arrFriends[indexPath.row].name
            cell.imgVw_user.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(String(describing: arrFriends[indexPath.row].photo!))") , placeholder: UIImage(named:"contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! FriendContactCell
            cell.lbl_title.text = "Mobile Contacts".localized
            cell.lbl_subTitle.text = "Add from Phone,LinkedIn etc.".localized
            return cell
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (isSearchActive) {
            self.view.endEditing(true)
            
            if let phNo = self.arrFriends[indexPath.row].phoneNo {
                if phNo == SessionManager.getPhone(){
                    FTIndicator.showToastMessage("This is your own number")
                } else {
                    // Creating or fetching private chat.
                    if let id = self.arrFriends[indexPath.row].quickBloxID {
                        if id != "" {
                            if let UInt_id = UInt(id) as? UInt {
                        if let privateChatDialog = ServicesManager.instance().chatService.dialogsMemoryStorage.privateChatDialog(withOpponentID: UInt_id) {
                                    constantVC.openPrivateChat.isActiveOpenPrivateChat = true
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                                    controller.dialog = privateChatDialog
                                    self.present(controller , animated: true , completion: nil)
                        } else {
                            ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: UInt_id, phNo + "-" + SessionManager.getPhone(), isDoctor: false, fees: "") { (response, chatDialog) in
                                    if response.isSuccess {
                                        if let dialog_ = chatDialog {
                                        QuickBloxManager().sendSystemMsg_updateDialog_toOccupants(dialog: dialog_)
                                        }
                                        constantVC.openPrivateChat.isActiveOpenPrivateChat = true
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                                        controller.dialog = chatDialog
                                        self.present(controller , animated: true , completion: nil)
                                    }
                                    else {
                                        print(response.error.debugDescription)
                                    }

                                }
                                }
                            }
                        }
                    }
                }
            }
        }
        else {
            self.setBlurView(isHidden: false)
            self.ImportViewNotifier.show()
            self.navigationController?.navigationBar.isUserInteractionEnabled = false
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
        tableView.deselectRow(at: indexPath , animated: true)
    }
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return 0.1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64.0
    }
}
