//
//  DialPad_VC.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 9/21/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class DialPad_VC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var txtFiled_enter: UITextField!
    
    //Hide status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK:- UIButton Actions
    
    @IBAction func btn_number_action(_ sender: UIButton) {
        var tappedText = ""
        
        if sender.tag == 10 {
            tappedText = "*"
        } else if sender.tag == 11 {
            tappedText = "10"
        } else if sender.tag == 12 {
            tappedText = "#"
        } else {
            tappedText = "\(sender.tag)"
        }
        self.txtFiled_enter.text = self.txtFiled_enter.text! + tappedText
    }
    
    @IBAction func btn_hide_action(_ sender: UIButton) {
        constantVC.GlobalVariables.isDialPadLaunched = false
        self.dismiss(animated: true , completion: nil)
    }
    
}
