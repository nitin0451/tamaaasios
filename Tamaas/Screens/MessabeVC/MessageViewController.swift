
import UIKit
import PushKit
import CallKit
import AANotifier
import SwipeCellKit
import FTIndicator
import YYImage
import YYCache
import YYWebImage
import Shimmer
import QuickbloxWebRTC


class MessageViewController: BaseViewController , SwipeTableViewCellDelegate, UITabBarControllerDelegate{
    
    //MARK:- Outlets
    @IBOutlet weak var viewChatInner: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var vw_noMsg: UIView!
    @IBOutlet weak var lbl_noInternetConnection_alert: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var constraint_collectionVw_Top: NSLayoutConstraint!
    @IBOutlet weak var vw_updator: UIView!
    @IBOutlet weak var AI_updator: UIActivityIndicatorView!
    @IBOutlet weak var lbl_noMsgs: UILabel!
    @IBOutlet weak var lbl_noMsgs_msg: UILabel!
    @IBOutlet weak var lblStartYourChat: UILabel!
    
     
    //MARK:- Variables
    //var arrStories = [Story]()
    var myStory = Story()
    var callKitCompletionCallback: ((Bool)->Swift.Void?)? = nil
    var callKitProvider:CXProvider?
    var callKitCallController:CXCallController?
    var deviceTokenString:String?
    var tabBadgeCount:Int = 0
    var refreshControl = UIRefreshControl()
    var havingMyStory = false
    var shimmeringView = FBShimmeringView()
    var storyBoardDB:[StoryBoard_DB] = []
    var connectingTimer: Timer!
    var disconnectingTimer : Timer!
    var isGetStoriesServiceCalled:Bool = false
    var gameTimer: Timer?
    
    //MARK: - Properties
    var CallByNo:String = ""
    var isGroup:String = ""
    let core = Core.instance
    lazy private var dataSource: UsersDataSource = {
        let dataSource = UsersDataSource(currentUser: Core.instance.currentUser)
        return dataSource
    }()
   // private weak var session: QBRTCSession?
    lazy private var voipRegistry: PKPushRegistry = {
        let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
        return voipRegistry
    }()
    lazy private var backgroundTask: UIBackgroundTaskIdentifier = {
        let backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
        return backgroundTask
    }()
    var completionBlock: Completion?
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    
    func CleanDatabase(){
        CoreData_BussinessLogic.deleteAllCalls()
        CoreData_BussinessLogic.deleteAllFeeds()
        CoreData_BussinessLogic.deleteAllComments()
        CoreData_BussinessLogic.deleteAllNotifications()
        CoreData_BussinessLogic.deleteAllStories()
        CoreData_BussinessLogic.deleteAllStoryBoard()
        SessionManager.save_unreadMsgCount(count: 0)
        SessionManager.save_unreadNotificationCount(count: 0)
    }
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //for the new update --TODO
     
        if !(SessionManager.get_isUserLoggedInAfterUpdate()) {
            
            ServicesManager.instance().logout(completion: {
            })
            
            //clean DB
            constantVC.openPrivateChat.isActiveOpenPrivateChat = false
            constantVC.ActiveDataSource.isNavigatePostDetails = false
            self.CleanDatabase()
            UserDefaults.standard.removeObject(forKey: constantVC.GeneralConstants.Identity)
            constantVC.GlobalVariables.arrStories_global.removeAll()
            constantVC.GlobalVariables.isNeedToGetStories = true
            SessionManager.save_QuickBloxID(id: 0)
          
            let next = self.storyboard?.instantiateViewController(withIdentifier: "InitViewController") as! InitViewController
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = next
            return
        }
        QBRTCClient.instance().add(self)
        QBChat.instance.addDelegate(self)
        voipRegistry.delegate = self
        voipRegistry.desiredPushTypes = Set<PKPushType>([.voIP])
        dataSource = UsersDataSource(currentUser: Core.instance.currentUser)
        CallKitManager.instance.usersDatasource = dataSource
        
        self.addNotificationObservers()
        if Connectivity.isConnectedToInternet() {
            if constantVC.ActiveDataSource.isActiveSignUp {
                constantVC.ActiveDataSource.isActiveSignUp = false
                self.getDialogsWhenSignUp()
            } else {
                 self.OnlineDataSource()
            }
        } else {
            self.OfflineDataSource()
        }
     
        self.viewToLoad()
        self.localiseNoMsgView()
     
        if Connectivity.isConnectedToInternet() {
            if constantVC.GlobalVariables.isNeedToShowInitialConnectingBar {
                constantVC.GlobalVariables.isNeedToShowInitialConnectingBar = false
                connectingTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(showInitialConnectingBar), userInfo: nil, repeats: false)
            }
        }else {
           constantVC.GlobalVariables.isNeedToShowInitialConnectingBar = false
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadViewOnTab(notification:)), name: NSNotification.Name(rawValue: "notifyReload"), object: nil)
        self.tabBarController?.delegate = self
        
    }
    
     func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notifyReload"), object: nil, userInfo: nil)
     }
    
     @objc func reloadViewOnTab(notification : Notification){
          
          if tableView.numberOfSections > 0{
               if tableView.numberOfRows(inSection: 0)>0{
                    tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
               }
          }
     }
    func popupUpdateDialogue(version: String){
        constantVC.GlobalVariables.appStoreVersion = version
        let versionInfo = constantVC.GlobalVariables.appStoreVersion
        let alertMessage = "A new version of Tamaaas Application is available,Please update to version "+versionInfo
        let alert = UIAlertController(title: "New Version Available", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "Update", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let url = URL(string: constantVC.GeneralConstants.appLiveLink),
                UIApplication.shared.canOpenURL(url){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        })
        let noBtn = UIAlertAction(title:"Skip this Version" , style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
            
            SessionManager.save_NewVersionSkipped(version: version)
        })
        alert.addAction(okBtn)
        alert.addAction(noBtn)
        self.present(alert, animated: true, completion: nil)
    }
      
    func getDialogsWhenSignUp(){
        ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
            
        }, completion: { (response: QBResponse?) -> Void in
            
            guard response != nil && response!.isSuccess else {
                //SVProgressHUD.showError(withStatus: "SA_STR_FAILED_LOAD_DIALOGS".localized)
                return
            }
            
            // SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
            ServicesManager.instance().lastActivityDate = NSDate()
            self.OnlineDataSource()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if constantVC.ActiveDataSource.isActiveRefresh {
            self.reloadTableViewIfNeeded()
            constantVC.ActiveDataSource.isActiveRefresh = false
        }
    
        self.loadViewWillAppear()
    }
    
    func loadViewWillAppear(){
        
        if constantVC.GlobalVariables.isNewStoryAdded_moreThanOne {
            constantVC.GlobalVariables.isNewStoryAdded_moreThanOne = false
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ShowStoriesVC") as! ShowStoriesVC
            controller.imageURL = SessionManager.getPhone()
            controller.userId = SessionManager.getUserId()
            controller.username = SessionManager.getUsername()
            controller.userPhnNo = SessionManager.getPhone()
            self.navigationController?.pushViewController(controller , animated: false)
            return
        }
        
        if constantVC.GlobalVariables.isNeedToGetStories {
            constantVC.GlobalVariables.isNeedToGetStories = false
            //get stories
            self.getStories { (success) in
            }
        }
        
        self.setTabBar_BadgeCount()
        self.setTabBar_BadgeCount_feeds(badgeCount: SessionManager.get_unreadNotificationCount())
        self.configureinitialView()
        self.hideUpdator()
     
        if !Connectivity.isConnectedToInternet() {
            self.constraint_collectionVw_Top.constant = 30
            self.lbl_noInternetConnection_alert.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
       NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    //MARK:- UIView Helpers
    func OfflineDataSource(){
        self.storyBoardDB = CoreData_BussinessLogic.getStoryBoard()
    }
    
    func OnlineDataSource(){
        if constantVC.GlobalVariables.isAppLauched && Connectivity.isConnectedToInternet() {
            CoreData_BussinessLogic.deleteAllStoryBoard()
          // self.showShimmerView()
        }
        
        ServicesManager.instance().chatService.addDelegate(self)
        ServicesManager.instance().authService.add(self)
        QBRTCClient.initializeRTC()
        QBRTCClient.instance().add(self)
  
     if let dialogs = self.dialogs() {
          if dialogs.count > 0 {
               self.vw_noMsg.isHidden = true
               self.tableView.reloadData()
          } else {
               self.vw_noMsg.isHidden = false
          }
     }
     
        self.gettotalUnreadChats()
    }
    
    func showShimmerView(){
        if let customView = Bundle.main.loadNibNamed("shimmer_messageVC_view", owner: self, options: nil)?.first as? shimmer_messageVC_view {
            self.shimmeringView.frame = view.bounds
            self.tableView.addSubview(shimmeringView as? UIView ?? UIView())
            shimmeringView.contentView = customView
            shimmeringView.isShimmering = true
        }
    }
    
    
    func configureinitialView(){
        navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.topItem?.title = "Messages".localized
        self.navigationController?.navigationBar.isHidden = false
        self.view.semanticContentAttribute = .forceLeftToRight
        collectionView.delegate = self;
        collectionView.dataSource = self;
        self.constraint_collectionVw_Top.constant = 0
        self.lbl_noInternetConnection_alert.font = UIFont.init(name:"Lato-Regular",size:17)!
        self.lbl_noInternetConnection_alert.text = "No Internet Connection!".localized
        self.view.endEditing(true)
        self.tabBarController?.tabBar.isHidden = false
     
    }
    
    func addNotificationObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_updateView_InternetDisconnected), name: constantVC.NSNotification_name.NSNotification_InternetDisconnected , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_updateView_InternetConnected), name: constantVC.NSNotification_name.NSNotification_InternetConnected , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_profileUpdate), name: constantVC.NSNotification_name.NSNotification_profileUpdate , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_Dialogs), name: constantVC.NSNotification_name.NSNotification_loadDialogs , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleNotification_bellUnreadCount), name: constantVC.NSNotification_name.NSNotification_bellUnreadCount , object: nil)
    }
    
    func hideUpdator(){
        self.vw_updator.isHidden = true
    }
    
    
    //MARK:- Custom Methods
    func localiseNoMsgView(){
        self.lbl_noMsgs.text = "Messages".localized
        self.lbl_noMsgs_msg.text = "Send direct messages, group messages, attachment and custom stickers".localized
        self.lblStartYourChat.text = "Start your chat".localized
    }
    
    @objc func showInitialConnectingBar(){
        //self.constraint_collectionVw_Top.constant = 30
        self.lbl_noInternetConnection_alert.isHidden = true
        self.vw_updator.isHidden = true
       // self.AI_updator.startAnimating()
    //   self.connectingTimer.invalidate()
        
        self.activityIndicator.hidesWhenStopped = true
        self.showIndicator_navigationTitle()
    }
    
    func showIndicator_navigationTitle() {
        self.navigationItem.titleView = self.activityIndicator
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func hideIndicator_navigationTitle() {
        self.navigationItem.titleView = nil
    }
    
     @objc func hideInitialConnectingBar(){
          //self.constraint_collectionVw_Top.constant = 0
          self.vw_updator.isHidden = true
          //self.AI_updator.stopAnimating()
          //self.disconnectingTimer.invalidate()
         self.hideIndicator_navigationTitle()
     }
    
    func setTabBar_BadgeCount(){
        if SessionManager.get_unreadMsgCount() > 0  {
            tabBarController?.tabBar.items?.first?.badgeColor = UIColor(red: 62.0/255, green: 208.0/255, blue: 11.0/255, alpha: 1.0)
            tabBarController?.tabBar.items?.first?.badgeValue = "\(SessionManager.get_unreadMsgCount())"
        }
        else {
            tabBarController?.tabBar.items?.first?.badgeColor = UIColor.clear
        }
    }
    
    func viewToLoad(){
        
        myStory.id = SessionManager.getUserId()
        myStory.name = SessionManager.getUsername()
        myStory.photoURL = SessionManager.getPhone()
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action: #selector(MessageViewController.refreshView_refreshControl), for: .valueChanged)
        refreshControl.tintColor = UIColor.lightGray
        
        //add right swipe
        let right = UISwipeGestureRecognizer(target : self, action : #selector(self.rightSwipe))
        right.direction = .right
        self.tableView.addGestureRecognizer(right)
        
        /*voipRegistry = PKPushRegistry.init(queue: DispatchQueue.main)
        voipRegistry?.delegate = self
        voipRegistry?.desiredPushTypes = Set([PKPushType.voIP])
        let configuration = CXProviderConfiguration(localizedName: "Tamaaas Calling...")
        configuration.maximumCallGroups = 1
        configuration.maximumCallsPerCallGroup = 1
        if let callKitIcon = UIImage(named: "phone") {
            configuration.iconTemplateImageData = UIImagePNGRepresentation(callKitIcon)
        }
        
        callKitProvider = CXProvider(configuration: configuration)
        callKitCallController = CXCallController()
        callKitProvider?.setDelegate(self, queue: nil)*/
        
//        let update = CXCallUpdate()
//        update.remoteHandle = CXHandle(type: .generic, value: "Pete Za")
//        callKitProvider?.reportNewIncomingCall(with: UUID(), update: update, completion: { error in })
       
        
        self.refreshControl.frame.origin.x -= 120
        
        if shimmeringView != nil {
            shimmeringView.isShimmering = false
            shimmeringView.removeFromSuperview()
        }
    }
 
    
    @objc func refreshView_refreshControl() {
        
       if Connectivity.isConnectedToInternet() {
           self.reloadTableViewIfNeeded()
           //self.arrStories.removeAll()
        //   constantVC.GlobalVariables.arrStories_global.removeAll()
        
           if !(self.isGetStoriesServiceCalled) {
                self.isGetStoriesServiceCalled = true
                self.getStories { (success) in
                }
            }
            gameTimer?.invalidate()
          self.refreshControl.endRefreshing()
        }
        else {
            refreshControl.endRefreshing()
            self.constraint_collectionVw_Top.constant = 30
        }
    }
    
    func gettotalUnreadChats() {
        
        self.tabBadgeCount = 0
        if let dialogs = self.dialogs() {
           
            for dialog in dialogs {
                 print(dialog.unreadMessagesCount)
                if let unreadCount = dialog.unreadMessagesCount as? UInt{
                    if unreadCount > 0 {
                        self.tabBadgeCount = self.tabBadgeCount + 1
                    }
                }
            }
        }
        print(self.tabBadgeCount)
        SessionManager.save_unreadMsgCount(count: self.tabBadgeCount)
        self.setTabBar_BadgeCount()
    }
    
    func setTabBar_BadgeCount_feeds(badgeCount: Int){
        //set tap badge count
        if SessionManager.get_unreadNotificationCount() > 0  {
            tabBarController?.tabBar.items?[1].badgeColor = UIColor(red: 62.0/255, green: 208.0/255, blue: 11.0/255, alpha: 1.0)
            tabBarController?.tabBar.items?[1].badgeValue = "\(SessionManager.get_unreadNotificationCount())"
        }
        else {
            tabBarController?.tabBar.items?[1].badgeColor = UIColor.clear
        }
    }
    
    func moveToChatVcfromNavigation(dialogue: QBChatDialog?) {
        guard let dialog = dialogue else {return}
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        controller.dialog = dialog
        
        let unreadCount = dialog.unreadMessagesCount
        if unreadCount > 0 {
            controller.isUnreadCountAvailable = true
        } else {
            controller.isUnreadCountAvailable = false
        }
        self.navigationController?.pushViewController(controller , animated: false)
    }
    
    func segueManually(_indexPath : IndexPath){
        if let dialogs = self.dialogs() {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            controller.dialog = dialogs[_indexPath.row]
            
            //check unread count
             let unreadCount = dialogs[_indexPath.row].unreadMessagesCount 
                if unreadCount > 0 {
                    controller.isUnreadCountAvailable = true
                } else {
                    controller.isUnreadCountAvailable = false
                }
            
            self.navigationController?.pushViewController(controller , animated: false)
        }
    }
    
    //MARK:- Story related Methods
    @objc func rightSwipe(){
        self.addStory()
    }
    
    func addStory(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "StoriesCropperView") as! StoriesCropperView
        let transition = CATransition()
        transition.duration = 0.30
        transition.type = kCATransitionPush
        transition.subtype = kCAOnOrderOut
        self.view.window?.layer.add(transition, forKey: kCATransition)
        self.navigationController!.pushViewController(controller, animated: false)
    }
    
    func getStories(completionHandler:@escaping (Bool) -> ()){
        var arrTemp:[Story] = []
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["user_id":SessionManager.getUserId(), "PageNo":"1","limit":"10"]
        
        if Connectivity.isConnectedToInternet() {
            CoreData_BussinessLogic.deleteAllStoryBoard()
            CoreData_BussinessLogic.saveOrUpdateStoryBoard(story: self.myStory)
        }
        
        if !constantVC.GlobalVariables.arrStories_global.indices.contains(0) {
            constantVC.GlobalVariables.arrStories_global.insert(self.myStory , at: 0)
        }
        
        //my own story
        arrTemp.append(constantVC.GlobalVariables.arrStories_global[0])
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_STORYBOARD, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                
                SessionManager.printLOG(data: "RESPONSE:- --- GET STORIES --------- \(String(describing: result))")
                
                print("RESPONSE:- --- GET STORIES --------- \(String(describing: result))")
                
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    if let isHavingStory = response["having_my_story"] {
                        if "\(isHavingStory)" == "0"{
                            self.havingMyStory = false
                        }
                        else{
                            self.havingMyStory = true
                        }
                        
                        SessionManager.save_isHaingStory(isHaving: self.havingMyStory)
                    }
                    if let storyDetails = response["story_details"] as? [[String:Any]] {
                        
                        for item in storyDetails {
                            let story = Story()
                            if let image = item["phoneNumber"] as? String{
                                story.photoURL = image
                            }
                            
                            story.name = SessionManager.getNameFromMyAddressBook(number: story.photoURL)
                            
                            if let userId = item["user_id"] as? String{
                                story.id = userId
                            }
                            
                            if let totalCount = item["total_stories_count"] {
                                story.total_stories_count = String(describing: totalCount)
                            }
                            
                            if let isReadStory = item["IsLastStoryRead"] as? String{
                                if isReadStory == "0"{
                                    story.isLastRead = false
                                }
                                else{
                                    story.isLastRead = true
                                }
                            }
                            if story.id != SessionManager.getUserId(){
                                //others
                                arrTemp.append(story)
                            }
                            else{
                                if constantVC.GlobalVariables.arrStories_global.indices.contains(0) {
                                    constantVC.GlobalVariables.arrStories_global[0].isLastRead = story.isLastRead
                                } else {
                                    constantVC.GlobalVariables.arrStories_global.insert(self.myStory , at: 0)
                                    constantVC.GlobalVariables.arrStories_global[0].isLastRead = story.isLastRead
                                }
                                arrTemp[0] = constantVC.GlobalVariables.arrStories_global[0]
                             
                            //    constantVC.GlobalVariables.arrStories_global[0].isLastRead = story.isLastRead
//                                if self.arrStories.count > 0 {
//                                    self.arrStories[0].isLastRead = story.isLastRead
//                                }
                            }
                            //save stories on DB
                            CoreData_BussinessLogic.saveOrUpdateStoryBoard(story: story)
                        }
                     //   constantVC.GlobalVariables.arrStories_global.removeAll()
                       // constantVC.GlobalVariables.arrStories_global = self.arrStories
                        
                        if self.havingMyStory {
                            constantVC.GlobalVariables.myStoriesCount = 1
                        }
                        else {
                            constantVC.GlobalVariables.myStoriesCount = 0
                        }
                        DispatchQueue.main.async {
                            constantVC.GlobalVariables.arrStories_global = arrTemp
                            self.collectionView.reloadData()
                            completionHandler(true)
                        }
                    }
                    else{
                        constantVC.GlobalVariables.myStoriesCount = 0
                        DispatchQueue.main.async {
                            constantVC.GlobalVariables.arrStories_global = arrTemp
                            self.collectionView.reloadData()
                            completionHandler(true)
                        }
                    }
                }
                else{
                    SessionManager.save_isHaingStory(isHaving: false)
                    constantVC.GlobalVariables.myStoriesCount = 0
                    self.havingMyStory = false
                    DispatchQueue.main.async {
                        constantVC.GlobalVariables.arrStories_global = arrTemp
                        self.collectionView.reloadData()
                        completionHandler(false)
                    }
                }
                
                self.isGetStoriesServiceCalled = false
            }
            else{
                SessionManager.save_isHaingStory(isHaving: false)
                constantVC.GlobalVariables.myStoriesCount = 0
                self.havingMyStory = false
                DispatchQueue.main.async {
                    constantVC.GlobalVariables.arrStories_global = arrTemp
                    self.collectionView.reloadData()
                    completionHandler(false)
                }
                self.isGetStoriesServiceCalled = false
            }
        })
    }
    
     
    // MARK: - DataSource Action
    func dialogs() -> [QBChatDialog]? {
        return ServicesManager.instance().chatService.dialogsMemoryStorage.dialogsSortByUpdatedAt(withAscending: false)
    }
           
    //MARK:- NSNotification observer methods
    @objc func handleNotification_Dialogs(_ notificationObj: NSNotification) {
        ServicesManager.instance().chatService.addDelegate(self)
        ServicesManager.instance().authService.add(self)
        self.collectionView.reloadData()
        
        if let dialogs = self.dialogs() {
            if dialogs.count > 0 {
                self.vw_noMsg.isHidden = true
                self.tableView.reloadData()
            } else {
                self.vw_noMsg.isHidden = false
            }
        }
    }
    
    @objc func handleNotification_updateView_InternetDisconnected(_ notificationObj: NSNotification) {
        self.constraint_collectionVw_Top.constant = 30
        self.lbl_noInternetConnection_alert.isHidden = false
        self.vw_updator.isHidden = true
    }
    
    @objc func handleNotification_updateView_InternetConnected(_ notificationObj: NSNotification) {
        self.constraint_collectionVw_Top.constant = 0
        gameTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.refreshView_refreshControl), userInfo: nil, repeats: false)
        NotificationCenter.default.post(name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    @objc func handleNotification_profileUpdate(_ notification: NSNotification) {
        if let fromNo = notification.object as? String {
            //for chat list
            if constantVC.GlobalVariables.allChannelsFrnds_phNO.contains(fromNo) {
                if let row = constantVC.GlobalVariables.allChannelsFrnds_phNO.index(of: fromNo) {
                    DispatchQueue.main.async {
                        if let cell = self.tableView.cellForRow(at: IndexPath(row: row , section: 0)) as? MessageTVCell {
                            cell.userImage.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(fromNo)" + ".jpeg") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation , .refreshImageCache] , completion: nil)
                        }
                    }
                }
            }
            
            //for stories
            if constantVC.GlobalVariables.allStoriesFrnds_phNO.contains(fromNo) {
                if let row = constantVC.GlobalVariables.allStoriesFrnds_phNO.index(of: fromNo) {
                    DispatchQueue.main.async {
                        let cell = self.collectionView.cellForItem(at: IndexPath(row: row , section: 0)) as! MessageCVCell
                        cell.userImage.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(fromNo)" + ".jpeg") , placeholder: UIImage(named:"profile") , options: [.progressiveBlur , .setImageWithFadeAnimation , .refreshImageCache] , completion: nil)
                    }
                }
            }
        }
    }
    
    @objc func handleNotification_bellUnreadCount(_ notification: NSNotification) {
        self.call_getNotification_UnreadCount()
    }
    
    //MARK:- TOP Blue Menu
    func addBlueBar_TopView() {
        
        if let customView = Bundle.main.loadNibNamed("PopupView", owner: self, options: nil)?.first as? PopupView {
            
            customView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width , height: 173)
            customView.backgroundColor = EPGlobalConstants.Colors.PopUpBlueColor
            
            let lbl_msg = customView.viewWithTag(120) as! UILabel
            lbl_msg.text = "Messages".localized
            let lbl_newChat = customView.viewWithTag(121) as! UILabel
            lbl_newChat.text = "New Chat".localized
            let lbl_addContacts = customView.viewWithTag(122) as! UILabel
            lbl_addContacts.text = "Add Contacts".localized
            let lbl_scanCode = customView.viewWithTag(123) as! UILabel
            lbl_scanCode.text = "Scan QR Code".localized
            
            let addContactBtn = customView.viewWithTag(101) as! UIButton
            addContactBtn.addTarget(self, action: #selector(self.addContactAc), for: .touchUpInside)
            let moneyBtn = customView.viewWithTag(102) as! UIButton
            moneyBtn.addTarget(self, action: #selector(self.moneyAc), for: .touchUpInside)
            let newChatBtn = customView.viewWithTag(100) as! UIButton
            newChatBtn.addTarget(self, action: #selector(self.newChatAc), for: .touchUpInside)
            let cancelBtn = customView.viewWithTag(103) as! UIButton
            cancelBtn.addTarget(self, action: #selector(self.cancelAc), for: .touchUpInside)
            
            let window = UIApplication.shared.keyWindow!
            customView.tag = 1001
            window.addSubview(customView)
        }
    }
    
    func removeBlueBar_topView() {
        let window = UIApplication.shared.keyWindow!
        window.viewWithTag(1001)?.removeFromSuperview()
    }
  
    //MARK:- Navigation Bar Button Actions
    @IBAction func barBtn_AddStrory_action(_ sender: UIBarButtonItem) {
        self.addStory()
    }
  
    @IBAction func addAc(_ sender: Any) {
        UIApplication.shared.statusBarView?.backgroundColor = EPGlobalConstants.Colors.PopUpBlueColor
        self.addBlueBar_TopView()
    }
    
    @IBAction func callListAc(_ sender: Any) {
        self.performSegue(withIdentifier: "callView", sender: self)
    }
    
    //MARK:- web services
    func call_getNotification_UnreadCount() {
        
        let dictParam : NSDictionary = ["identity":"test"+SessionManager.getPhone()]
        
        WebserviceSigleton().POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_NOTIFICATION_UNREAD_COUNT, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    if let data = response["data"] as? [[String:Any]] {
                        var unreadCount:Int = 0
                        for item in data {
                            if let count = item["unread"]{
                                unreadCount = Int(String(describing: count))!
                                if unreadCount > 0 {
                                    SessionManager.save_unreadNotificationCount(count: unreadCount)
                                    self.setTabBar_BadgeCount_feeds(badgeCount: unreadCount)
                                }
                                else {
                                    SessionManager.save_unreadNotificationCount(count: 0)
                                    self.setTabBar_BadgeCount_feeds(badgeCount: 0)
                                }
                            }
                        }
                    }
                }
                else{
                }
            }
            else{
            }
        })
    }
 
  
  //MARK:- UIButton Actions - TOP POP_UP
   @objc func newChatAc() {
        UIApplication.shared.statusBarView?.backgroundColor = .white
        self.removeBlueBar_topView()
        let picker = ContactPicker()
        picker.newBlock = { data in
            if let value = data as? QBChatDialog {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                controller.isPresented = true
                controller.modalPresentationStyle = .fullScreen
                //QBChatDialog
                controller.dialog = value
                self.present(controller , animated: true , completion: nil)
            }
        }
        let navigationController = UINavigationController(rootViewController: picker)
        navigationController.navigationBar.barTintColor = UIColor.white
        picker.modalPresentationStyle = .fullScreen
        self.present(navigationController, animated: true, completion: nil)
    }
    
  @objc func addContactAc() {
      UIApplication.shared.statusBarView?.backgroundColor = .white
      self.removeBlueBar_topView()
      self.performSegue(withIdentifier: "addContact", sender: self)
       
    }
    @objc func moneyAc() {
        UIApplication.shared.statusBarView?.backgroundColor = .white
        self.removeBlueBar_topView()
        self.showError(message: "We will notify you once this feature is ready. We are working hard to build something safe, secure and regulated, stay tuned.".localized)
    }
   @objc func cancelAc(){
        UIApplication.shared.statusBarView?.backgroundColor = .white
        self.removeBlueBar_topView()
    }
    
    @objc func didPressImagePreview(_ sender:AnyObject){
        
        let indexPath = IndexPath(row: sender.view.tag , section: 0)
        if let cell = self.tableView.cellForRow(at: indexPath) as? MessageTVCell {
            
            let url = URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(cell.opponentNo).jpeg")
            QMImagePreview.previewImage(with: url , in: self)
        }
    }
    
    @IBAction func didPress_startNewChat(_ sender: UIButton) {
        UIApplication.shared.statusBarView?.backgroundColor = .white
        self.removeBlueBar_topView()
        let picker = ContactPicker()
        let navigationController = UINavigationController(rootViewController: picker)
        navigationController.navigationBar.barTintColor = UIColor.white
        self.present(navigationController, animated: true, completion: nil)
    }
    

}
extension MessageViewController : UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        segueManually(_indexPath: indexPath)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        if orientation == .right {
            
         self.tableView.deselectRow(at: indexPath, animated: true)
         
         var isGroup:Bool = false
            
            if let dialogs = self.dialogs(){
                if dialogs[indexPath.row].type == QBChatDialogType.group {
                    isGroup = true
                }
            }
        
        var str_deletebtnTitle = "Delete".localized
        var str_confirmationMsg = "You want to delete chat".localized
        
        if isGroup {
            str_deletebtnTitle = "Exit".localized
            str_confirmationMsg = "You want to exit group".localized
        }
       
        //Delete Button
        let deleteAction = SwipeAction(style: .default , title: str_deletebtnTitle ) { action, indexPath in
            //show delete channel confirmation alert
            let alertController = UIAlertController(title: "Are you sure?".localized, message: str_confirmationMsg, preferredStyle: .alert)

            // Create the actions
            let okAction = UIAlertAction(title: "Yes".localized, style: UIAlertActionStyle.default) {
                UIAlertAction in

                if Connectivity.isConnectedToInternet() {
                    //delete code
                    guard let dialog = self.dialogs()?[indexPath.row] else {
                        return
                    }
                   
                    let deleteDialogBlock = { (dialog: QBChatDialog!) -> Void in
                            
                            // Deletes dialog from server and cache.
                        ServicesManager.instance().chatService.deleteDialog(withID: dialog.id!, completion: { (response) -> Void in
                                
                            guard response.isSuccess else {
                                SVProgressHUD.showError(withStatus: "SA_STR_ERROR_DELETING".localized)
                                return
                                }
                            
                            if dialog.type == QBChatDialogType.private {
                                SVProgressHUD.showSuccess(withStatus: "SA_STR_DELETED".localized)
                            } else {
                                SVProgressHUD.showSuccess(withStatus: "SA_STR_LEFT".localized)
                            }
                            
                            
                            })
                        }
                        
                        if dialog.type == QBChatDialogType.private {
                            deleteDialogBlock(dialog)
                        }
                        else {
                            // group
                            let occupantIDs = dialog.occupantIDs!.filter({ (number) -> Bool in
                                
                                return number.uintValue != ServicesManager.instance().currentUser.id
                            })
                            
                            dialog.occupantIDs = occupantIDs
                            let userLogin = ServicesManager.instance().currentUser.login ?? ""
                            let notificationMessage = "\(userLogin) " + "SA_STR_USER_HAS_LEFT".localized
                            // Notifies occupants that user left the dialog.
                            ServicesManager.instance().chatService.sendNotificationMessageAboutLeaving(dialog, withNotificationText: notificationMessage, completion: { (error) -> Void in
                                deleteDialogBlock(dialog)
                            })
                        }
                }
                else {
                    //OFFLINE
                    FTIndicator.showToastMessage("No Internet Connection!")
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel".localized, style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }

            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)

            // Present the controller
            self.present(alertController, animated: true, completion: nil)
        }

        deleteAction.backgroundColor = UIColor.red
        deleteAction.font = UIFont.boldSystemFont(ofSize: 11)
        deleteAction.image = UIImage(named: "delete")

        //More Button
        let moreAction = SwipeAction(style: .default, title: "More".localized) { action, indexPath in
            FTIndicator.showToastMessage("Under Development".localized)
            action.fulfill(with: .delete)
        }
        moreAction.font = UIFont.boldSystemFont(ofSize: 11)
        moreAction.textColor = UIColor.darkGray
        moreAction.image = UIImage(named: "more_option")
        return [deleteAction,moreAction]
        }
        else if orientation == .left {
            self.addStory()
            return []
        }
     return []
    }

    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
            var options = SwipeTableOptions()
            options.expansionStyle = .none
            options.transitionStyle = .border
            return options
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let dialogs = dialogs() {
            return dialogs.count
          
        }
     
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        guard let chatDialog = dialogs()?[indexPath.row] else {
            return cell
        }
   
        if chatDialog.type == QBChatDialogType.group && chatDialog.photo == nil {
            
            let menuGroupCell = tableView.dequeueReusableCell(withIdentifier: "myCellGroup", for: indexPath as IndexPath) as! MessageGroupCell
            menuGroupCell.delegate = self
            menuGroupCell.layoutIfNeeded()
            menuGroupCell.index = indexPath.row
            menuGroupCell.dialog = chatDialog
            if chatDialog.unreadMessagesCount > 0 {
                menuGroupCell.lblUnreadCount.isHidden = false
                menuGroupCell.lblUnreadCount.text = "\(chatDialog.unreadMessagesCount)"
            } else{
                menuGroupCell.lblUnreadCount.isHidden = true
            }
            return menuGroupCell
        }
      
        let menuCell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath as IndexPath) as! MessageTVCell
        menuCell.delegate = self
        menuCell.layoutIfNeeded()
        menuCell.index = indexPath.row
        menuCell.dialog = chatDialog
        
        if let data = chatDialog.data {
            if let isDoctor = data["isDoctor"] as? Bool,isDoctor {
                menuCell.chatHeaderDoctor.isHidden = false
//                menuCell.onlineView.isHidden = true
            } else {
                menuCell.chatHeaderDoctor.isHidden = true
            }
        }
    
        if  chatDialog.unreadMessagesCount > 0 {
            menuCell.unreadLab.isHidden = false
            menuCell.unreadLab.text = "\(chatDialog.unreadMessagesCount)"
        }
        else{
            menuCell.unreadLab.isHidden = true
        }
        //add tap gesture
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didPressImagePreview(_:)))
        menuCell.userImage.isUserInteractionEnabled = true
        menuCell.userImage.tag = indexPath.row
        menuCell.userImage.addGestureRecognizer(tapGestureRecognizer)
        
        return menuCell
    }
}

extension MessageViewController: UICollectionViewDelegate , UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if Connectivity.isConnectedToInternet() {
            return constantVC.GlobalVariables.arrStories_global.count
        }
        return CoreData_BussinessLogic.getStoryBoard().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath as IndexPath) as! MessageCVCell
        cell.row = indexPath.row
        
        if Connectivity.isConnectedToInternet() {
            cell.story = constantVC.GlobalVariables.arrStories_global[indexPath.row]
            constantVC.GlobalVariables.allStoriesFrnds_phNO.append(constantVC.GlobalVariables.arrStories_global[indexPath.row].photoURL)
            
            cell.userName.text = SessionManager.getNameFromMyAddressBook(number: constantVC.GlobalVariables.arrStories_global[indexPath.row].photoURL)
            CoreData_BussinessLogic.UpdateUserNameStoryBoard(name: cell.userName.text!, userID:constantVC.GlobalVariables.arrStories_global[indexPath.row].id)
            
        } else {
            if self.storyBoardDB.indices.contains(indexPath.row) {
                cell.storyDB = self.storyBoardDB[indexPath.row]
                cell.userName.text = self.storyBoardDB[indexPath.row].name
            }
        }
        
        if indexPath.row == 0 && !(SessionManager.get_isHaingStory()) {
            cell.addIcon.isHidden = false
            cell.borderView.borderWidth = 0
            cell.userName.text = "You".localized
            cell.userName.adjustsFontSizeToFitWidth = true
        }
        else if indexPath.row == 0 && SessionManager.get_isHaingStory(){
            cell.addIcon.isHidden = true
            cell.userName.text = "You".localized
        }
        else{
            cell.addIcon.isHidden = true
            cell.userName.adjustsFontSizeToFitWidth = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        
        self.storyBoardDB = CoreData_BussinessLogic.getStoryBoard()
        
        if indexPath.row == 0 && !(SessionManager.get_isHaingStory()){
            if Connectivity.isConnectedToInternet() {
                addStory()
            } else {
                FTIndicator.showToastMessage("No Internet Connection!")
            }
        }
        else if constantVC.GlobalVariables.arrStories_global.count > 0 && Connectivity.isConnectedToInternet() {
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ShowStoriesVC") as! ShowStoriesVC
            if indexPath.row < constantVC.GlobalVariables.arrStories_global.count {
               controller.indexToLoadView = indexPath.row
                controller.imageURL = constantVC.GlobalVariables.arrStories_global[indexPath.row].photoURL
                controller.userId = constantVC.GlobalVariables.arrStories_global[indexPath.row].id
                controller.username = constantVC.GlobalVariables.arrStories_global[indexPath.row].name
                controller.userPhnNo = constantVC.GlobalVariables.arrStories_global[indexPath.row].photoURL
                let transition = CATransition()
                transition.duration = 0.25
                transition.type = kCATransitionPush
                transition.subtype = kCAOnOrderOut
                self.view.window?.layer.add(transition, forKey: kCATransition)
                self.navigationController!.pushViewController(controller, animated: false)
            }
        }
        else if self.storyBoardDB.count > 0 && !(Connectivity.isConnectedToInternet()) && indexPath.row < self.storyBoardDB.count {
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ShowStoriesVC") as! ShowStoriesVC
            controller.imageURL = self.storyBoardDB[indexPath.row].phoneNumber!
            controller.userId = self.storyBoardDB[indexPath.row].user_id!
            controller.username = self.storyBoardDB[indexPath.row].name!
            controller.userPhnNo = self.storyBoardDB[indexPath.row].phoneNumber!
            let transition = CATransition()
            transition.duration = 0.25
            transition.type = kCATransitionPush
            transition.subtype = kCAOnOrderOut
            self.view.window?.layer.add(transition, forKey: kCATransition)
            self.navigationController!.pushViewController(controller, animated: false)
        }
    }
}


extension MessageViewController: QMChatServiceDelegate , QMChatConnectionDelegate , QMAuthServiceDelegate , QBChatDelegate {
    
    // MARK: - QMChatServiceDelegate
    func chatService(_ chatService: QMChatService, didUpdateChatDialogInMemoryStorage chatDialog: QBChatDialog) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService,didUpdateChatDialogsInMemoryStorage dialogs: [QBChatDialog]){
     self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogsToMemoryStorage chatDialogs: [QBChatDialog]) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddChatDialogToMemoryStorage chatDialog: QBChatDialog) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didDeleteChatDialogWithIDFromMemoryStorage chatDialogID: String) {
        self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessagesToMemoryStorage messages: [QBChatMessage], forDialogID dialogID: String) {
        //self.reloadTableViewIfNeeded()
    }
    
    func chatService(_ chatService: QMChatService, didAddMessageToMemoryStorage message: QBChatMessage, forDialogID dialogID: String){
        
        if let dialog_ = ServicesManager.instance().chatService.dialogsMemoryStorage.chatDialog(withID: dialogID) {
            
            //delete clear chat object from custom data
            if QuickBloxManager().isClearChat(dialog: dialog_) {
                QuickBloxManager().UnClearChat_updateDialog(dialog: dialog_) { (success) in
                }
            }
        }
       // self.reloadTableViewIfNeeded()
    }
    
    
    // MARK: QMChatConnectionDelegate
    func chatServiceChatDidFail(withStreamError error: Error) {
       // SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    func chatServiceChatDidAccidentallyDisconnect(_ chatService: QMChatService) {
        //SVProgressHUD.showError(withStatus: "SA_STR_DISCONNECTED".localized)
    }
    
    func chatServiceChatDidConnect(_ chatService: QMChatService) {
       // SVProgressHUD.showSuccess(withStatus: "SA_STR_CONNECTED".localized, maskType:.clear)
       // if !ServicesManager.instance().isProcessingLogOut! {
            QuickBloxManager().getDialogs()
           disconnectingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(hideInitialConnectingBar), userInfo: nil, repeats: false)
       // }
    }
    
    func chatService(_ chatService: QMChatService,chatDidNotConnectWithError error: Error){
        //SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
    
    
    func chatServiceChatDidReconnect(_ chatService: QMChatService) {
       // SVProgressHUD.showSuccess(withStatus: "SA_STR_CONNECTED".localized, maskType: .clear)
       // if !ServicesManager.instance().isProcessingLogOut! {
            QuickBloxManager().getDialogs()
      //  }
    }
    
    
    
    // MARK: - Helpers
    func reloadTableViewIfNeeded() {
        if !ServicesManager.instance().isProcessingLogOut! {
            self.vw_noMsg.isHidden = true
            self.gettotalUnreadChats()
            self.call_getNotification_UnreadCount()
            constantVC.GlobalVariables.allChannelsFrnds_phNO.removeAll()
            self.tableView.reloadData()
        }
    }
    
}

extension MessageViewController: QBRTCClientDelegate {
    
    // MARK: - QBRTCClientDelegate
    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
       
        if constantVC.ActiveSession.session != nil {
           if constantVC.ActiveSession.session?.id != session.id {
               session.rejectCall(["reject": "User is busy on another call"])
                return
            }
        }
        
        if let userInfo = userInfo {
            if userInfo["callBy"] != nil {
                if let no = userInfo["callBy"] {
                    CallByNo = no as! String
                }
            }
            
            if userInfo["isGroup"] != nil {
                if let group = userInfo["isGroup"] {
                    isGroup = group as! String
                }
            }
            
            if userInfo["dialogId"] != nil {
                if let id = userInfo["dialogId"] {
                    constantVC.ActiveCall.dialogID = id as! String
                }
            }
        }
        
        constantVC.ActiveSession.session = session
        constantVC.ActiveSession.activeCallUUID = UUID()
        var opponentIDs = [session.initiatorID]
        for userID in session.opponentsIDs {
            if userID.uintValue != core.currentUser?.id {
                opponentIDs.append(userID)
            }
        }
        
        CallKitManager.instance.reportIncomingCall(withUserIDs: opponentIDs,
                                                   session: session,
                                                   uuid: constantVC.ActiveSession.activeCallUUID , userInfo: userInfo ,
                                                   callBy: self.CallByNo ,
                                                   isGroup: self.isGroup,
                                                   onAcceptAction: { [weak self] in
                                                    guard let `self` = self else {
                                                        return
                                                    }
                                                    
                                                    
             if session.conferenceType == QBRTCConferenceType.audio {
                
                if let topController = UIApplication.topViewController() {
                    if let callViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TamAudioCall_VC") as? TamAudioCall_VC {
                        callViewController.session = session
                        callViewController.callUUID = constantVC.ActiveSession.activeCallUUID
                        callViewController.opponentNo = self.CallByNo
                        callViewController.userPhoto = self.CallByNo
                        topController.present(callViewController , animated: true , completion: nil)
                    }
                }
        }
        else {
                
                if let topController = UIApplication.topViewController() {
                    if let callViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TamVideoCall_VC") as? TamVideoCall_VC {
                        callViewController.session = session
                        callViewController.callUUID = constantVC.ActiveSession.activeCallUUID
                        callViewController.opponentNo = self.CallByNo
                        callViewController.isGroup = self.isGroup
                        topController.present(callViewController , animated: true , completion: nil)
                    }
                }
              }
                                  
        }, completion: { (end) in
                debugPrint("end")
    })
    }
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        print("hungUpByUser")
        
        if constantVC.ActiveSession.session == session {
            
            if isGroup == "true" && !(constantVC.ActiveSession.isCallActive) && userID == session.initiatorID {
                if let topController = UIApplication.topViewController() {
                    constantVC.ActiveSession.session = nil
                    constantVC.ActiveSession.isCallActive = false
                    constantVC().cleanActiveCall()
                    topController.dismiss(animated: true , completion: nil)
                }
                if CallKitManager.isCallKitAvailable() == true {
                    CallKitManager.instance.endCall(with: constantVC.ActiveSession.activeCallUUID) {
                        debugPrint("endCall")
                    }
                    constantVC.ActiveSession.activeCallUUID = nil
                    constantVC.ActiveSession.session = nil
                }
            }
        }
    }
    
    func session(_ session: QBRTCSession, rejectedByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        print("rejectedByUser")
        print(userInfo)
        if let info = userInfo as? [String : String] {
            if let rejectMsg = info["reject"] as? String {
                if let topController = UIApplication.topViewController() {
                    topController.dismiss(animated: true , completion: {
                        FTIndicator.showInfo(withMessage: rejectMsg)
                    })
                }
            }
        }
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        print("sessionDidClose")
        
        if constantVC.ActiveSession.session == session {
            if backgroundTask != UIBackgroundTaskInvalid {
                UIApplication.shared.endBackgroundTask(backgroundTask)
                backgroundTask = UIBackgroundTaskInvalid
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                if UIApplication.shared.applicationState == .background && self.backgroundTask == UIBackgroundTaskInvalid {
                    // dispatching chat disconnect in 1 second so message about call end
                    // from webrtc does not cut mid sending
                    // checking for background task being invalid though, to avoid disconnecting
                    // from chat when another call has already being received in background
                    QBChat.instance.disconnect(completionBlock: nil)
                }
            })
           
            //add call log
            if session.initiatorID == SessionManager.get_QuickBloxID() {
                
                if session.conferenceType == QBRTCConferenceType.audio {
                    
                    if session.initiatorID == SessionManager.get_QuickBloxID() {
                        //sender
                        if let senderID = SessionManager.get_QuickBloxID() {
                            
                            if constantVC.ActiveSession.isCallActive {
                                QuickBloxManager().sendCallNotificationMessage(with: .hangUp , duration: QuickBloxManager().parseDuration(constantVC.ActiveCall.duration) , opponentNo: constantVC.ActiveCall.receiverNo)
                                WebserviceSigleton().addCallLog(senderID: constantVC.ActiveCall.senderID , senderNo: constantVC.ActiveCall.senderNo , receiverNo: constantVC.ActiveCall.receiverNo , sessionID: session.id , membersID: constantVC.ActiveCall.membersID , isGroup: constantVC.ActiveCall.isGroup , isAudio: constantVC.ActiveCall.isAudio , dialogID: constantVC.ActiveCall.dialogID , GroupName: constantVC.ActiveCall.groupName , duration:  constantVC.ActiveCall.duration , callState: constantVC.callState.hangup.rawValue, groupPhoto: constantVC.ActiveCall.groupPhoto)
                            }
                            else {
                                QuickBloxManager().sendCallNotificationMessage(with: .missedNoAnswer , duration: QuickBloxManager().parseDuration("00:00:00") , opponentNo: constantVC.ActiveCall.receiverNo)
                                
                                WebserviceSigleton().addCallLog(senderID: constantVC.ActiveCall.senderID , senderNo: constantVC.ActiveCall.senderNo , receiverNo: constantVC.ActiveCall.receiverNo , sessionID: session.id , membersID: constantVC.ActiveCall.membersID , isGroup: constantVC.ActiveCall.isGroup, isAudio: constantVC.ActiveCall.isAudio, dialogID: constantVC.ActiveCall.dialogID , GroupName: constantVC.ActiveCall.groupName , duration: "00:00:00" , callState: constantVC.callState.missedNoAnswer.rawValue, groupPhoto: constantVC.ActiveCall.groupPhoto)
                            }
                            
                        }
                       
                    }
                }
                
                if session.conferenceType == QBRTCConferenceType.video {
                    
                    if session.initiatorID == SessionManager.get_QuickBloxID() {
                        //sender
                        if constantVC.ActiveSession.isCallActive {
                            QuickBloxManager().sendCallNotificationMessage(with: .hangUp , duration: QuickBloxManager().parseDuration(constantVC.ActiveCall.duration), opponentNo: constantVC.ActiveCall.receiverNo)
                            
                            WebserviceSigleton().addCallLog(senderID: constantVC.ActiveCall.senderID , senderNo: constantVC.ActiveCall.senderNo , receiverNo: constantVC.ActiveCall.receiverNo , sessionID: session.id , membersID: constantVC.ActiveCall.membersID , isGroup: constantVC.ActiveCall.isGroup, isAudio: constantVC.ActiveCall.isAudio , dialogID: constantVC.ActiveCall.dialogID , GroupName: constantVC.ActiveCall.groupName , duration: constantVC.ActiveCall.duration , callState: constantVC.callState.hangup.rawValue , groupPhoto: constantVC.ActiveCall.groupPhoto)
                        }
                        else {
                            QuickBloxManager().sendCallNotificationMessage(with: .missedNoAnswer , duration: QuickBloxManager().parseDuration("00:00:00"), opponentNo: constantVC.ActiveCall.receiverNo)
                            
                            WebserviceSigleton().addCallLog(senderID: constantVC.ActiveCall.senderID , senderNo: constantVC.ActiveCall.senderNo , receiverNo: constantVC.ActiveCall.receiverNo , sessionID: session.id , membersID: constantVC.ActiveCall.membersID , isGroup: constantVC.ActiveCall.isGroup, isAudio: constantVC.ActiveCall.isAudio , dialogID: constantVC.ActiveCall.dialogID , GroupName: constantVC.ActiveCall.groupName , duration: "00:00:00" , callState: constantVC.callState.missedNoAnswer.rawValue , groupPhoto: constantVC.ActiveCall.groupPhoto)
                        }
                        
                    }
                }
            }
            
           constantVC.ActiveSession.isCallActive = false
            if let topController = UIApplication.topViewController() {
                
                if session.conferenceType == QBRTCConferenceType.video {     //VIDEO
                    
                    //maintain view for active and background
                    if CallActiveInBackgroundManager.shared.isActiveVideoView() {
                        topController.dismiss(animated: true , completion: nil)
                    }else {
                        CallActiveInBackgroundManager.shared.hideActiveVideoCallView()
                    }
                    CallActiveInBackgroundManager.shared.closeCall()
                    
                } else {                                                     //AUDIO
                    
                    //maintain view for active and background
                    if CallActiveInBackgroundManager.shared.isActiveAudioView() {
                        topController.dismiss(animated: true , completion: nil)
                    }else {
                        CallActiveInBackgroundManager.shared.hideActiveAudioCallView()
                    }
                    CallActiveInBackgroundManager.shared.closeCallAudio()
                }
            }
            
            if CallKitManager.isCallKitAvailable() == true {
                
                CallKitManager.instance.endCall(with: constantVC.ActiveSession.activeCallUUID) {
                    debugPrint("endCall")
                }
                constantVC().cleanActiveSession()
                constantVC().cleanActiveCall()
            }
        }
    }
}

extension MessageViewController: PKPushRegistryDelegate {
    // MARK: - PKPushRegistryDelegate
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        //  New way, only for updated backend
        guard let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        let subscription: QBMSubscription! = QBMSubscription()
        subscription.notificationChannel = QBMNotificationChannel.APNSVOIP
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = voipRegistry.pushToken(for: .voIP)
        subscription.devicePlatform = "iOS"
        
        QBRequest.createSubscription(subscription, successBlock: { (response: QBResponse!, objects: [QBMSubscription]?) -> Void in
            //
            print(response)
        }) { (response: QBResponse!) -> Void in
            //
            print(response.error)
        }
    }
   
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        guard let deviceIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            return
        }
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: deviceIdentifier, successBlock: { response in
        }, errorBlock: { error in
        })
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        if CallKitManager.isCallKitAvailable() == true {
            if payload.dictionaryPayload[UsersConstant.voipEvent] != nil {
                let application = UIApplication.shared
                if application.applicationState == .background && backgroundTask == UIBackgroundTaskInvalid {
                    backgroundTask = application.beginBackgroundTask(expirationHandler: {
                        application.endBackgroundTask(self.backgroundTask)
                        self.backgroundTask = UIBackgroundTaskInvalid
                    })
                }
                if QBChat.instance.isConnected == false {
                    core.loginWithCurrentUser()
                }
            }
            
          /*  if payload.dictionaryPayload["callBy"] != nil {
                if let no = payload.dictionaryPayload["callBy"] {
                    CallByNo = no as! String
                }
            }
            
            if payload.dictionaryPayload["isGroup"] != nil {
                if let group = payload.dictionaryPayload["isGroup"] {
                    isGroup = group as! String
                }
            }*/
        }
    }
}
