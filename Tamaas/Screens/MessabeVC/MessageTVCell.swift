//
//  MessageCell.swift
//  Tamaas
//
//  Created by Krescent Global on 19/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import SwipeCellKit

class MessageTVCell: SwipeTableViewCell {

    @IBOutlet weak var chatHeaderDoctor: UIImageView!
    @IBOutlet weak var onlineView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var unreadLab: UILabel!
    @IBOutlet weak var dateLab: UILabel!
    @IBOutlet weak var messageTitle: UILabel!
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var imgVw_sticker: UIImageView!
    
    var isProfileUpdated:Bool = false
    var isGroup = Bool()
    var secondUser_no:String = ""
    var isHaveUnreadCount = Bool()
    var isNewUnSaved_contact = Bool()
    var isGroupCustomViewActive:Bool = false
    var occupantID:NSNumber?
    let instance = BaseViewController()
    var index = Int()
    var opponentNo:String = ""
    var groupOwner = QBUUser()
    
    var dialog: QBChatDialog? {
        didSet {
            self.UserName.text = ""
            self.userImage.image = nil
            updateView()
        }
    }
    
    func updateView() {
        
        self.unreadLab.isHidden = true
        self.userImage.image = nil
        self.isGroup = false
        
        if self.dialog?.type == QBChatDialogType.group {
            self.isGroup = true
            self.onlineView.isHidden = true
            
            if let name = dialog?.name {
                self.UserName.text = name
            }
            
            if let photo = self.dialog?.photo {
               self.opponentNo = photo
               constantVC.GlobalVariables.allChannelsFrnds_phNO.append(self.opponentNo)
                self.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(photo).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: nil)
            }
        }
        else {
            
            if let occupantIDS = dialog?.occupantIDs {
                let IDS = occupantIDS.filter { $0 != SessionManager.get_QuickBloxID() }
                self.occupantID = IDS[0]
                QBRequest.user(withID: self.occupantID as! UInt, successBlock: { (response , user) in
                    let currentTimeInterval = Int(Date().timeIntervalSince1970)
                    var userLastRequestAtTimeInterval = Int()
                    
                    if let lastRequest = user.lastRequestAt {
                        userLastRequestAtTimeInterval = Int((lastRequest.timeIntervalSince1970))
                    }
                    
                    if((currentTimeInterval - userLastRequestAtTimeInterval) > 90){
                        // user is offline now
                        self.onlineView.isHidden = true
                    } else {
                        // user is online now
                        self.onlineView.isHidden = false
                    }
                }) { (response) in
                }
            }
            
            if let data = dialog?.data {
                if let number = data["number"] as? String {
                    if let dicCustomData = QuickBloxManager().convertJsonStringToDictionary(text: number) {
                        if let isDoctor = data["isDoctor"] as? Bool,isDoctor {
                            let name = self.dialog?.name
                            self.UserName.text = name
                            let arrNo = number.components(separatedBy: "-")
                            for item in arrNo {
                                if item != SessionManager.getPhone() {
                                    opponentNo = item
                                    constantVC.GlobalVariables.allChannelsFrnds_phNO.append(self.opponentNo)
                                }
                            }// For Normal User
                        } else {
                            if let dicNo = dicCustomData["number"] as? String {
                                 let arrNo = dicNo.components(separatedBy: "-")
                                 for item in arrNo {
                                     if item != SessionManager.getPhone() {
                                         opponentNo = item
                                         constantVC.GlobalVariables.allChannelsFrnds_phNO.append(self.opponentNo)
                                         self.UserName.text = SessionManager.getNameFromMyAddressBook(number:  opponentNo)
                                     }
                                 }
                             }
                        }
                    } else {
                        // for Names
                        // For Doctor
                        if let data = dialog?.data {
                            if let name = self.dialog?.name {
                                self.UserName.text = name
                            }
                            if let isDoctor = data["isDoctor"] as? Bool,isDoctor {
                                let name = self.dialog?.name
                                self.UserName.text = name
                                let arrNo = number.components(separatedBy: "-")
                                for item in arrNo {
                                    if item != SessionManager.getPhone() {
                                        opponentNo = item
                                        constantVC.GlobalVariables.allChannelsFrnds_phNO.append(self.opponentNo)
                                    }
                                }// For Normal User
                            } else {
                                let arrNo = number.components(separatedBy: "-")
                                for item in arrNo {
                                    if item != SessionManager.getPhone() {
                                        opponentNo = item
                                        constantVC.GlobalVariables.allChannelsFrnds_phNO.append(self.opponentNo)
                                        self.UserName.text = SessionManager.getNameFromMyAddressBook(number:  opponentNo)
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                
                //self.UserName.text = ""
                //self.userImage.image = nil
                
                ServicesManager.instance().usersService.getUserWithID( self.occupantID as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                    
                    if let no = task.result?.phone {
                        self.opponentNo = no
                        constantVC.GlobalVariables.allChannelsFrnds_phNO.append(self.opponentNo)
                    }
                    
                    self.UserName.text = SessionManager.getNameFromMyAddressBook(number:  self.opponentNo)
                    self.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(self.opponentNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: nil)
                    return nil
                })
            }
            
            self.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(opponentNo).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: nil)
        }
        
        if let msg = dialog?.lastMessageText {
            if MessageDialogManager.shared.isSticker(msgText: msg).isSticker {
                self.imgVw_sticker.isHidden = false
                self.messageTitle.text = ""
                self.imgVw_sticker.image = UIImage(named: MessageDialogManager.shared.isSticker(msgText: msg).stickerImg)
            }
            else {
                self.imgVw_sticker.isHidden = true
                
                if let customData = self.dialog!.data {
                    if let d = customData["number"] as? String {
                        if let customDic = QuickBloxManager().convertJsonStringToDictionary(text: d) {
                            if let ids = customDic["clearChat"] as? String {
                                let arr = ids.components(separatedBy: ",")
                                
                                if !arr.contains(SessionManager.getPhone()) {
                                    MessageDialogManager.shared.setLastMsg(msgText: msg , isGroup: isGroup , dialog: dialog!) { (lastMsgText) in
                                        self.messageTitle.text = lastMsgText
                                    }
                                }
                                else {
                                    //Nitin
                                    if let data = self.dialog?.data {
                                        if let isDoctor = data["isDoctor"] as? Bool,isDoctor {
                                            self.imgVw_sticker.isHidden = true
                                            self.messageTitle.text = "Message your time first...".localized
                                        } else {
                                            self.imgVw_sticker.isHidden = true
                                            self.messageTitle.text = "Say hi to your new friend...".localized
                                        }
                                    }

                                }
                                
                            }else {
                                MessageDialogManager.shared.setLastMsg(msgText: msg , isGroup: isGroup , dialog: dialog!) { (lastMsgText) in
                                    self.messageTitle.text = lastMsgText
                                }
                            }
                        }
                        else {
                            //no updates
                            MessageDialogManager.shared.setLastMsg(msgText: msg , isGroup: isGroup , dialog: dialog!) { (lastMsgText) in
                                self.messageTitle.text = lastMsgText
                            }
                        }
                }
                    else {
                        MessageDialogManager.shared.setLastMsg(msgText: msg , isGroup: isGroup , dialog: dialog!) { (lastMsgText) in
                            self.messageTitle.text = lastMsgText
                        }
                    }
                }else {
                    MessageDialogManager.shared.setLastMsg(msgText: msg , isGroup: isGroup , dialog: dialog!) { (lastMsgText) in
                        self.messageTitle.text = lastMsgText
                    }
                }
            }
        } else {
            //Nitin
            if let data = self.dialog?.data {
                if let isDoctor = data["isDoctor"] as? Bool,isDoctor {
                    self.imgVw_sticker.isHidden = true
                    self.messageTitle.text = "Message your time first..."
                } else {
                    self.imgVw_sticker.isHidden = true
                    self.messageTitle.text = "Say hi to your new friend..."
                }
            }
           
        }
     
        if let unreadCount = dialog?.unreadMessagesCount {
            if unreadCount > 0 {
                self.unreadLab.isHidden = false
                self.unreadLab.text = "\(unreadCount)"
               self.UserName.font = UIFont.init(name:"CircularStd-Medium",size:19)!
               messageTitle.textColor = UIColor(red:28.0/255.0, green: 27.0/255.0, blue: 27.0/255.0, alpha: 1.0)
               dateLab.textColor = UIColor(red:0.0/255.0, green: 161.0/255.0, blue: 98.0/255.0, alpha: 1.0)
            } else {
                self.unreadLab.isHidden = true
                self.UserName.font = UIFont.init(name:"CircularStd-Book",size:19)!
                messageTitle.textColor = UIColor(red:146.0/255.0, green: 146.0/255.0, blue: 146.0/255.0, alpha: 1.0)
                dateLab.textColor = UIColor(red:185.0/255.0, green: 185.0/255.0, blue: 185.0/255.0, alpha: 1.0)
            }
        }
        if let date = dialog?.updatedAt {
            if let formattedDate = QMDateUtils.formattedShortDateString(date) {
                self.dateLab.text = formattedDate
            }
        }
        
        if let data = dialog?.data {
            if let isDoctor = data["isDoctor"] as? Bool,isDoctor {
                self.chatHeaderDoctor.isHidden = false
//                menuCell.onlineView.isHidden = true
            } else {
                self.chatHeaderDoctor.isHidden = true
            }
        }
}

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.onlineView.layer.cornerRadius = self.onlineView.frame.size.width / 2
        self.onlineView.clipsToBounds = true
        self.unreadLab.layer.cornerRadius = self.unreadLab.frame.size.width / 2
        self.unreadLab.clipsToBounds = true
        self.onlineView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}



class MessageGroupCell: SwipeTableViewCell {
    
    @IBOutlet weak var vwGroupImg: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMsgTitle: UILabel!
    @IBOutlet weak var imgVwSticker: UIImageView!
    @IBOutlet weak var lblUnreadCount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgVwUser1: UIImageView!
    @IBOutlet weak var imgVwUser2: UIImageView!
    
    var index = Int()
    
    var dialog: QBChatDialog? {
        didSet {
            updateView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.lblUnreadCount.layer.cornerRadius = self.lblUnreadCount.frame.size.width / 2
        self.lblUnreadCount.clipsToBounds = true
        self.lblUnreadCount.isHidden = true
        self.imgVwSticker.isHidden = true
    }
   
    
    func updateView() {
        
        if let name = dialog?.name {
            self.lblUserName.text = name
        }
        
        if let msg = dialog?.lastMessageText {
            if MessageDialogManager.shared.isSticker(msgText: msg).isSticker {
                self.imgVwSticker.isHidden = false
                self.lblMsgTitle.text = ""
                self.imgVwSticker.image = UIImage(named: MessageDialogManager.shared.isSticker(msgText: msg).stickerImg)
            }
            else {
                self.imgVwSticker.isHidden = true
                
                if let customData = self.dialog!.data {
                    if let d = customData["number"] as? String {
                        if let customDic = QuickBloxManager().convertJsonStringToDictionary(text: d) {
                            if let ids = customDic["clearChat"] as? String {
                                let arr = ids.components(separatedBy: ",")
                                
                                if !arr.contains(SessionManager.getPhone()) {
                                    MessageDialogManager.shared.setLastMsg(msgText: msg , isGroup: true , dialog: dialog!) { (lastMsgText) in
                                        self.lblMsgTitle.text = lastMsgText
                                    }
                                }
                                else {
                                    self.lblMsgTitle.text = "Say hi to your new friends..."
                                }
                            }
                            else {
                                //no updates
                                MessageDialogManager.shared.setLastMsg(msgText: msg , isGroup: true , dialog: dialog!) { (lastMsgText) in
                                    self.lblMsgTitle.text = lastMsgText
                                }
                            }
                        }
                    }
                    else {
                        //no updates
                        MessageDialogManager.shared.setLastMsg(msgText: msg , isGroup: true , dialog: dialog!) { (lastMsgText) in
                            self.lblMsgTitle.text = lastMsgText
                        }
                    }
                }else {
                    //no updates
                    MessageDialogManager.shared.setLastMsg(msgText: msg , isGroup: true , dialog: dialog!) { (lastMsgText) in
                        self.lblMsgTitle.text = lastMsgText
                    }
                }
            }
        } else {
            self.imgVwSticker.isHidden = true
            self.lblMsgTitle.text = "Say hi to your new friends..."
        }
     
        if let unreadCount = dialog?.unreadMessagesCount {
            if unreadCount > 0 {
                self.lblUnreadCount.isHidden = false
                self.lblUnreadCount.text = "\(unreadCount)"
               self.lblUserName.font = UIFont.init(name:"CircularStd-Medium",size:19)!
               lblMsgTitle.textColor = UIColor(red:28.0/255.0, green: 27.0/255.0, blue: 27.0/255.0, alpha: 1.0)
               lblDate.textColor = UIColor(red:0.0/255.0, green: 161.0/255.0, blue: 98.0/255.0, alpha: 1.0)
            } else {
               self.lblUnreadCount.isHidden = true
               self.lblUserName.font = UIFont.init(name:"CircularStd-Book",size:19)!
               lblMsgTitle.textColor = UIColor(red:146.0/255.0, green: 146.0/255.0, blue: 146.0/255.0, alpha: 1.0)
               lblDate.textColor = UIColor(red:185.0/255.0, green: 185.0/255.0, blue: 185.0/255.0, alpha: 1.0)}
        }
     
        if let date = dialog?.updatedAt {
            if let formattedDate = QMDateUtils.formattedShortDateString(date) {
                self.lblDate.text = formattedDate
            }
        }
        
        if let groupOccupantsID = self.dialog?.occupantIDs {
            let IDS = groupOccupantsID.filter { $0 != SessionManager.get_QuickBloxID() }
            if IDS.count >= 2 {
                
                //user1
                ServicesManager.instance().usersService.getUserWithID( IDS[0] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                    
                    if let no = task.result?.phone {
                        self.imgVwUser1.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: { (image , url , type , stage , error) in
                            if error == nil && image != nil {
                                CommonFunctions.shared.addBorder(imageView: self.imgVwUser1 , colour: UIColor.white)
                            } else {
                                CommonFunctions.shared.addBorder(imageView: self.imgVwUser1 , colour: UIColor(red: 145.0/255.0, green: 159.0/255.0, blue: 185.0/255.0, alpha: 1.0))
                            }
                        })
                    }
                    return nil
                })
                
                //user2
                ServicesManager.instance().usersService.getUserWithID( IDS[1] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                    
                    if let no = task.result?.phone {
                        self.imgVwUser2.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: currentYYImgotions.currentYYImg , completion: { (image , url , type , stage , error) in
                            if error == nil && image != nil {
                                CommonFunctions.shared.addBorder(imageView: self.imgVwUser2 , colour: UIColor.white)
                            }else {
                                CommonFunctions.shared.addBorder(imageView: self.imgVwUser2 , colour: UIColor(red: 145.0/255.0, green: 159.0/255.0, blue: 185.0/255.0, alpha: 1.0))
                            }
                        })
                    }
                    return nil
                })
            }
        }
    }
}

class MessageDialogManager: NSObject {
    
    static let shared = MessageDialogManager()
    
    func isSticker(msgText:String) ->(isSticker:Bool , stickerImg: String){
        if msgText.contains("TAMSTICKER") {
            let msgTextArr = msgText.components(separatedBy: "*")
            if msgTextArr.count == 2 {
                if let stickerName = msgTextArr[1] as? String {
                   return (true , stickerName)
                }
            }
        }
        return (false , "")
    }
    
    func setLastMsg(msgText:String , isGroup: Bool , dialog: QBChatDialog , completionHandler:@escaping (String) -> ()){
        
        //define last message
        if msgText == "Image attachment" || msgText == "Audio attachment" || msgText == "Video attachment"{
            completionHandler("Media".localized)
        }
        else if msgText.contains("created new group") && isGroup {
            
            if dialog.userID == UInt(truncating: SessionManager.get_QuickBloxID() ?? 0) {
                completionHandler("You created new group")
            }
            else {
                if let no = QuickBloxManager().getNoFromMessage(message: msgText , replaceText: " created new group") as? String {
                    completionHandler(SessionManager.getNameFromMyAddressBook(number: no) + " " + "created new group")
                }
            }
        }
        else if msgText.contains("has left the chat") && isGroup {
            
            if dialog.userID == UInt(truncating: SessionManager.get_QuickBloxID() ?? 0) {
                completionHandler("")
            }
            else {
                
                if let no = QuickBloxManager().getNoFromMessage(message: msgText , replaceText: " has left the chat.") as? String {
                    completionHandler(SessionManager.getNameFromMyAddressBook(number: no) + " " + "has left the chat")
                }
            }
        }
        else if msgText.contains(" added ") && isGroup {
            
            //new participant added
            let arrNo = msgText.components(separatedBy: " ")
            if arrNo.count == 3 {
                
                if arrNo[0] == SessionManager.getPhone() {
                    let str = "You" + " added " + SessionManager.getNameFromMyAddressBook(number: arrNo[2])
                    completionHandler(str)
                }
                else if arrNo[2] == SessionManager.getPhone() {
                    let str = SessionManager.getNameFromMyAddressBook(number: arrNo[0]) + " added " + "You"
                    completionHandler(str)
                }
                else {
                    let str = SessionManager.getNameFromMyAddressBook(number: arrNo[0]) + " added " + SessionManager.getNameFromMyAddressBook(number: arrNo[2])
                    completionHandler(str)
                }
            }
        }
        else {
            completionHandler(msgText)
        }
}
}


