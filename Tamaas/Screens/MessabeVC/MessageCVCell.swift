//
//  MessageCVCell.swift
//  Tamaas
//
//  Created by Krescent Global on 19/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import YYImage
import YYCache
import YYWebImage

//red colour - UIColor(red: 255.0/255.0, green: 50.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
//gray colour - UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 0.7).cgColor


class MessageCVCell: UICollectionViewCell {
    
    @IBOutlet weak var addIcon: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var loader_imgVw: UIActivityIndicatorView!
    var row = Int()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        borderView.layer.borderWidth = 0.5
        borderView.layer.borderColor = UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 0.7).cgColor
    }
    
    var story: Story? {
        didSet {
           self.userImage.image = nil
            self.loader_imgVw.isHidden = true
            updateView()
        }
    }
    
    var storyDB: StoryBoard_DB? {
        didSet {
            self.userImage.image = nil
            self.loader_imgVw.isHidden = true
            updateView_DB()
        }
    }
    
    func updateView() {
        
        if let photoURL = story?.photoURL {
            
            if constantVC.GlobalVariables.isMyProfileUpdated && row == 0 {
                constantVC.GlobalVariables.isMyProfileUpdated = false
                
                //clear cache
                YYWebImageManager.shared().cache?.memoryCache.removeObject(forKey: constantVC.GeneralConstants.ImageUrl + "\(photoURL)" + ".jpeg")
                YYWebImageManager.shared().cache?.diskCache.removeObject(forKey: constantVC.GeneralConstants.ImageUrl + "\(photoURL)" + ".jpeg")
            }
            
            if row == 0 {
                self.userImage.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(photoURL)" + ".jpeg") , placeholder: UIImage(named:"contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
            }
            else {
                self.userImage.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(photoURL)" + ".jpeg") , placeholder: UIImage(named:"contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
            }
        }
        
        if story?.isLastRead != true{
            borderView.layer.borderWidth = 2
            borderView.layer.borderColor = UIColor(red: 255.0/255.0, green: 50.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
        } else {
            borderView.layer.borderWidth = 0.5
            borderView.layer.borderColor = UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 0.7).cgColor
        }
    }
    
    func updateView_DB() {
        if let photoURL = storyDB?.photoURL {
            self.userImage.yy_setImage(with: URL.init(string: constantVC.GeneralConstants.ImageUrl + "\(photoURL)") , placeholder: UIImage(named:"contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
        }
        
        if storyDB?.isLastStoryRead != true{
            borderView.layer.borderWidth = 2
            borderView.layer.borderColor = UIColor(red: 255.0/255.0, green: 50.0/255.0, blue: 58.0/255.0, alpha: 1.0).cgColor
        } else {
            borderView.layer.borderWidth = 0.5
            borderView.layer.borderColor = UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 0.7).cgColor
        }
        
    }
}
