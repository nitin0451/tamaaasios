
import UIKit
import CountryPicker
import IQKeyboardManager

class YourWallet_VC: BaseViewController, CountryPickerDelegate, UITextFieldDelegate {
    
    //MARK:- IBOutlet
     @IBOutlet weak var lbl_comingSoon: UILabel!
     @IBOutlet weak var lbl_msg: UILabel!
     @IBOutlet weak var topSubView: UIView!
     @IBOutlet weak var currencyConverterLabel: UILabel!
     @IBOutlet weak var secondCountryFlag: UIImageView!
     @IBOutlet weak var secondCountryLabel: UILabel!
     @IBOutlet weak var secondCountryTf: UITextField!
     @IBOutlet weak var firstCountryFlag: UIImageView!
     @IBOutlet weak var firstCountryLabel: UILabel!
     @IBOutlet weak var firstCountryTf: UITextField!
     @IBOutlet weak var picker: CountryPicker!
     @IBOutlet weak var pickerHeight: NSLayoutConstraint!
     
    //MARK:- Variables
     var arr = [String]()
     var dicOfCurrencyData = [String:Any]()
     var detailDict  = NSDictionary()
     var detailDict2 = NSDictionary()
     var shCountry1 = true
     
     //MARK:- UIView life cycle Methods
     override func viewDidLoad() {
          super.viewDidLoad()
          self.localise_language()
          self.navigationController?.navigationBar.topItem?.title = "Your Wallet".localized
          self.navigationController?.navigationBar.isHidden = false
          picker.backgroundColor = UIColor.white
          readJson()
          setupView()
     }
    
     override func viewWillAppear(_ animated: Bool) {
          IQKeyboardManager.shared().isEnabled = true
          IQKeyboardManager.shared().isEnableAutoToolbar = true
     }
    
     override func viewWillDisappear(_ animated: Bool) {
          IQKeyboardManager.shared().isEnabled = false
          IQKeyboardManager.shared().isEnableAutoToolbar = false
          
     }
     func setupView(){
          
          let locale = Locale.current
          let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
          //init Picker
          picker.countryPickerDelegate = self
          picker.showPhoneNumbers = false
        
          picker.setCountry(code!)
          pickerHeight.constant = 0
          firstCountryTf.delegate = self
          
          currencyConverterLabel.text = "Currency Convertor".localized
     }
    
     func readJson(){
          if let path = Bundle.main.path(forResource: "currencySymbol", ofType: "json") {
               do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                    if let jsonResult = jsonResult as? Dictionary<String, AnyObject>{
                         // do stuff
                         dicOfCurrencyData = jsonResult
                         arr = Array(jsonResult.keys)
                         detailDict = dicOfCurrencyData["USD"] as! NSDictionary
                         detailDict2 = dicOfCurrencyData["AFN"] as! NSDictionary // nitin changed from india to Afghan Afghani
                         firstCountryTf.text = String(describing: self.detailDict["symbol_native"]!) + "1"
                         convertMoneyAc()
                    }
               } catch {
                    // handle error
               }
          }
     }
     
     func localise_language(){
          self.lbl_msg.text = "We will notify you once this feature is ready. We are working hard to build something safe, secure and regulated, stay tuned.".localized
          self.lbl_comingSoon.text = "Coming Soon".localized
     }
     
     @IBAction func swapAc(_ sender: Any) {
          let tempDic = detailDict
          detailDict = detailDict2
          detailDict2 = tempDic
          let amount = secondCountryTf.text?.replacingOccurrences(of: String(describing: self.detailDict2["symbol_native"]!), with: "")
          
          self.firstCountryFlag.image = UIImage(named: self.detailDict["code"]! as! String)
          firstCountryTf.text = String(describing: self.detailDict["symbol_native"]!) + "\(String(describing: amount!))"
          self.secondCountryFlag.image = UIImage(named: self.detailDict2["code"]! as! String)
        firstCountryLabel.text = self.detailDict["code"] as? String
        secondCountryLabel.text = self.detailDict2["code"] as? String
          convertMoneyAc()
     }
     
     @IBAction func firstCountryAc(_ sender: Any) {
          shCountry1 = true
          pickerHeight.constant = 236
     }
     
     @IBAction func secondCountryAc(_ sender: Any) {
          shCountry1 = false
          pickerHeight.constant = 236
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     public func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, flag: UIImage) {
          if shCountry1 == true{
               if let dicDetail = self.dicOfCurrencyData[countryCode] as? NSDictionary{
                    self.detailDict = dicDetail
                    
                    self.firstCountryFlag.image = UIImage(named: self.detailDict["code"]! as! String)
                    self.firstCountryLabel.text = self.detailDict["code"] as? String
               }
          }
          else{
               if let dicDetail = self.dicOfCurrencyData[countryCode] as? NSDictionary{
                    self.detailDict2 = dicDetail
                    self.secondCountryFlag.image = UIImage(named: self.detailDict2["code"] as? String ?? "")
                    self.secondCountryLabel.text = self.detailDict2["code"] as? String
               }
          }

     }
     @IBAction func donePicker(_ sender: Any) {
          
          pickerHeight.constant = 0
          firstCountryTf.text = String(describing: self.detailDict["symbol_native"]!) + "1"
          convertMoneyAc()
          
     }
    
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          let maxLength = 16
          let currentString: NSString = textField.text! as NSString
          let newString: NSString =
               currentString.replacingCharacters(in: range, with: string) as NSString
          return newString.length <= maxLength
     }
    
     @IBAction func textFieldEditingChanged(_ sender: UITextField) {
         convertMoneyAc()
     }
     
     func convertMoneyAc(){
          let money = firstCountryTf.text?.replacingOccurrences(of: String(describing: self.detailDict["symbol_native"]!), with: "")
          firstCountryTf.text = String(describing: self.detailDict["symbol_native"]!) + money!
          
          DispatchQueue.main.async {
               WebserviceSigleton().GETServiceWithoutParameters(urlString: "https://apilayer.net/api/convert?access_key=232a35393e96404629dcf376c4e92be8&from=\(self.firstCountryLabel.text!)&to=\(self.secondCountryLabel.text!)&amount=\(money ?? "0")", callback: { (result,error) -> Void in
                    if result != nil{
                         let response = result! as Dictionary
                         let successMessage = response["success"] as! Bool
                         
                         if successMessage == true{
                              if  let resultMessage = response["result"]{
                                   self.secondCountryTf.text = String(describing: self.detailDict2["symbol_native"]!) + String(describing: resultMessage)
                              }
                         }
                         
                    }
                    
               })
          }
     }
}

