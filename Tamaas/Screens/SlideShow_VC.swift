//
//  SlideShow_VC.swift
//  Tamaas
//
//  Created by Apple on 23/10/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class SlideShow_VC: UIViewController , ACTabScrollViewDelegate , ACTabScrollViewDataSource {
    
    @IBOutlet weak var scrollView: ACTabScrollView!
    
    var dialogID:String = ""
    var arrMediaToSend:[String] = []
    var contentViews: [UIView] = []
    var msgID:String?
    var imageToShare : UIImage!
    var defaultPage:Int = 0
    var arrImgMedia:[QBChatMessage] = []
    var indexToShare = Int()
     
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indexToShare = self.arrImgMedia.count-defaultPage-1
        self.configureScrollTab()
    }
    
     @IBAction func didPress_share(_ sender: Any) {
          print(indexToShare)
          let imageview = self.contentViews[indexToShare].viewWithTag(22) as! UIImageView
          if let image_ = imageview.image{
                let imageShare = [ image_ ]
                let activityViewController = UIActivityViewController(activityItems: imageShare , applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                self.present(activityViewController, animated: true, completion: nil)
          }
     }
    
     @IBAction func didPress_close(_ sender: UIButton) {
        self.dismiss(animated: true , completion: nil)
     }
    
     override func viewWillAppear(_ animated: Bool) {
          UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
     }
    
     override func viewWillDisappear(_ animated: Bool) {
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
     }
     
     func configureScrollTab(){
        self.scrollView?.defaultPage = defaultPage
        self.scrollView?.arrowIndicator = false
        self.scrollView?.tabSectionHeight = 0
        self.scrollView?.delegate = self
        self.scrollView?.dataSource = self
        // create content views from storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        for (_,msg) in self.arrImgMedia.enumerated().reversed() {
            
            let vc = storyboard.instantiateViewController(withIdentifier: "SlideShow_container_VC") as! SlideShow_container_VC
            if let attachments = msg.attachments {
                if attachments.indices.contains(0) {
                    if let imgUrl_ = attachments[0].url {
                        vc.imageUrl = imgUrl_
                    }
                }
            }
            addChildViewController(vc) // don't forget, it's very important
            contentViews.append(vc.view)
        }
    }
     /*
      
      class func previewImage(with url: URL?, in ivc: UIViewController?) {
      
      if url == nil {
      return
      }
      
      let photo = QMPhoto()
      
      let photoDataSource = NYTPhotoViewerSinglePhotoDataSource(photo: photo)
      
      let photosViewController = NYTPhotosViewController(dataSource: photoDataSource)
      
      if ivc is NYTPhotosViewControllerDelegate ?? false {
      photosViewController.delegate = ivc as? (UIViewController & NYTPhotosViewControllerDelegate)
      }
      ivc?.present(photosViewController, animated: true)
      
      let loader = QMImageLoader.instance()
      
      loader?.downloadImage(with: url, transform: nil, options: SDWebImageRefreshCached, progress: nil, completed: { image, transfomedImage, error, cacheType, finished, imageURL in
      
      if error == nil && image != nil {
      photo.image = loader?.originalImage(with: imageURL)
      let updatedPhotoDataSource = NYTPhotoViewerSinglePhotoDataSource(photo: photo)
      
      photosViewController.dataSource = updatedPhotoDataSource
      photosViewController.reloadPhotos(animated: true)
      }
      })
      }
      */
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, didChangePageTo index: Int) {
     
      indexToShare = self.arrImgMedia.count-index-1
         print("didChangePageTo \(index)")
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, didScrollPageTo index: Int) {
     
     indexToShare = self.arrImgMedia.count-index-1
        print("didScrollPageTo \(index)")
    }
    
    func numberOfPagesInTabScrollView(_ tabScrollView: ACTabScrollView) -> Int {
        return self.arrImgMedia.count
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, tabViewForPageAtIndex index: Int) -> UIView {
        // create a label
        let label = UILabel()
        label.textColor = UIColor(red: 77.0 / 255, green: 79.0 / 255, blue: 84.0 / 255, alpha: 1)
        label.textAlignment = .center
        
        // if the size of your tab is not fixed, you can adjust the size by the following way.
        label.sizeToFit() // resize the label to the size of content
        label.frame.size = CGSize(width: label.frame.size.width + 28, height: label.frame.size.height + 36) // add some paddings
        return label
    }
    
    func tabScrollView(_ tabScrollView: ACTabScrollView, contentViewForPageAtIndex index: Int) -> UIView {
        return contentViews[self.arrImgMedia.count-index-1]
    }
}
