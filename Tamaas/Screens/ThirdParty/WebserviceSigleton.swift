
import Foundation
import Alamofire
import FTIndicator

class WebserviceSigleton {

    // MARK:-- POST  Request Method
    //To POST data from Remote resource.Returns NSDictionary Object
    
    private let baseUrl = constantVC.GeneralConstants.BASE_URL
    
    func POSTServiceWithParameters(urlString : String , params : Dictionary<String, AnyObject> , callback:@escaping ( _ data: Dictionary<String, AnyObject>?,  _ error: NSError? ) -> Void)  {
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.session.configuration.timeoutIntervalForResource = 120
        
        let url =  "\(baseUrl)\(urlString)"
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        print("Request POST URL:\(url) PARAMS:\(params) HEADER: ")
        
        manager.request(url, method: .get, parameters: params, encoding:  URLEncoding.default, headers: headers).responseJSON { response in
            
            guard response.result.error == nil else {
                callback(nil , response.result.error! as NSError? )
                return
            }
            
            if let value = response.result.value {
                print("JSON: \(value)")
                if let result = value as? Dictionary<String, AnyObject> {
                    callback(result , nil )
                }
            }
        }
        
    }
    
    
    func POSTServiceWithParametersNew(urlString : String , params : Dictionary<String, AnyObject>? , isHeaders : Bool = true, isFormData: Bool = false,callback:@escaping ( _ data: Dictionary<String, AnyObject>?,  _ error: NSError? ) -> Void)  {
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.session.configuration.timeoutIntervalForResource = 120
        
        let url =  "\(baseUrl)\(urlString)"
        var headers : HTTPHeaders?
        if isHeaders {
            headers = ["Content-Type": "application/json"]
        }
        if isHeaders && isFormData {
            headers = ["Content-Type": "application/x-www-form-urlencoded"]
        }
        print("Request POST URL:\(url) PARAMS:\(params) HEADER: ")
        
        manager.request(urlString, method: .post, parameters: params, encoding:  URLEncoding.default, headers: isHeaders == true ? headers : nil).responseJSON { response in
            
            guard response.result.error == nil else {
                callback(nil , response.result.error! as NSError? )
                return
            }
            
            if let value = response.result.value {
                print("JSON: \(value)")
                if let result = value as? Dictionary<String, AnyObject> {
                    callback(result , nil )
                }
            }
        }
    }
    
    
    func userId() -> AnyObject {
        return UserDefaults.standard.object(forKey: constantVC.GeneralConstants.KEY_USER_ID)! as AnyObject
    }
    
    
    static func fetchToken(url : String) throws -> String {
        var token: String = ""
        let requestURL: URL = URL(string: url)!
        
        do {
            let data = try Data(contentsOf: requestURL)
            
            if let json = try JSONSerialization.jsonObject(with: data) as? [String: String]{
                
                let dicResponse:NSDictionary = json as NSDictionary
                
                if let t = dicResponse.object(forKey: "token") {
                    token = String(describing: t)
                }
            }
            
        } catch let error as NSError {
            throw error
        }
        return token
    }
     //GET SERVICE
     func GETServiceWithoutParameters(urlString : String , callback:@escaping ( _ data: Dictionary<String, AnyObject>?,  _ error: NSError? ) -> Void)  {
          let manager = Alamofire.SessionManager.default
          manager.session.configuration.timeoutIntervalForRequest = 120
          manager.session.configuration.timeoutIntervalForResource = 120
          
          let headers = ["Content-Type": "application/x-www-form-urlencoded"]
          
          print("Request POST URL:\(urlString) HEADER: ")
          
          manager.request(urlString, method: .get, parameters: nil, encoding:  URLEncoding.default, headers: headers).responseJSON { response in
               guard response.result.error == nil else {
                    print("Error for POST :\(urlString):\(response.result.error!)")
                    callback(nil , response.result.error! as NSError? )
                    return
               }
               
               if let value = response.result.value {
                    print("JSON: \(value)")
                    if let result = value as? Dictionary<String, AnyObject> {
                         callback(result , nil )
                    }
               }
          }
     }
    
    func GETServiceWithParameters(urlString : String , params : Dictionary<String, AnyObject> , callback:@escaping ( _ data: Dictionary<String, AnyObject>?,  _ error: NSError? ) -> Void)  {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.session.configuration.timeoutIntervalForResource = 120
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        print("Request POST URL:\(urlString) PARAMS:\(params) HEADER: ")
        
        manager.request(urlString, method: .get, parameters: params, encoding:  URLEncoding.default, headers: headers).responseJSON { response in
            guard response.result.error == nil else {
                print("Error for POST :\(urlString):\(response.result.error!)")
                callback(nil , response.result.error! as NSError? )
                return
            }
            
            if let value = response.result.value {
                print("JSON: \(value)")
                if let result = value as? Dictionary<String, AnyObject> {
                    callback(result , nil )
                }
            }
        }
    }
    
    
    //SEND NOTIFICATION
    func send_notification(identityTo:String , title:String , msg:String , toNo:String , flag:String , post_id:String , postImageUrl: String , comment:String)
    {
        
        let strComment_encode = comment.replacingOccurrences(of: "\\", with: "-")
        
        let Url: String = "\(constantVC.GeneralConstants.BASE_IP)salniazi-app/twiliovideo/CommonPushNotifications.php"
        
        var twilioKey:String = ""
        
        let params = ["identity": identityTo,
                      "title"   : title ,
                      "message" : msg,
                      "FromNumber": SessionManager.getPhone(),
                      "ToNumber": toNo,
                      "flag": flag,
                      "post_id": post_id,
                      "postImageUrl":postImageUrl,
                      "comment": strComment_encode,
                      "pushsid" : twilioKey]
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        Alamofire.request(Url , method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {
                }
                break
                
            case .failure(_):
                print(response.result.description)
                break
            }
        }
    }
    
    
    //MARK:- CALL LOGS (VOICE /VIDEO)
    func call_toUpdateVoice_OR_VideoCall_status(duration:String , status:String , logID: String , sid:String)
    {
        var idToSend = ""
        var param = ""
        
        if logID == "" { //voice
            param = "sid"
            idToSend = sid
        }
        else { //video
            param = "logid"
            idToSend = logID
        }
        
        let Url: String = "\(constantVC.GeneralConstants.BASE_URL)updateVideoLog"
        let params = ["status" : status,
                      "duration" : duration,
                       param: idToSend]
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        Alamofire.request(Url , method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {
                }
                break
                
            case .failure(_):
                print(response.result.description)
                break
            }
        }
    }
    
   
    func addCallLog(senderID:String , senderNo:String , receiverNo:String , sessionID: String , membersID:String , isGroup:String , isAudio:String , dialogID:String , GroupName: String , duration:String , callState:String , groupPhoto:String)
    {
        
        let Url: String = "\(constantVC.GeneralConstants.BASE_URL)\(constantVC.WebserviceName.URL_ADD_CALL_LOG)"
        let params = ["senderID" : senderID,
                      "senderNo" : senderNo,
                      "receiverNo" : receiverNo,
                      "sessionID" : sessionID,
                      "membersID" : membersID,
                      "isGroup" : isGroup,
                      "isAudio" : isAudio,
                      "dialogID" : dialogID,
                      "GroupName" : GroupName,
                      "duration" : duration ,
                      "callState" : callState,
                      "groupPhoto" : groupPhoto]
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        
        Alamofire.request(Url , method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {
                }
                break
                
            case .failure(_):
                print(response.result.description)
                break
            }
        }
    }
  
    //MARK:- FCM Token
    func callToAddFCMToken(token:String) {
        let Url: String = "\(constantVC.GeneralConstants.BASE_URL)\(constantVC.WebserviceName.URL_UPDATE_FCM_TOKEN)"
        let params = ["user_id" : SessionManager.getUserId() ,
                      "token" : token ,
                      "device_type" : "ios"]
        
        let headers = ["Content-Type": "application/x-www-form-urlencoded"]
        Alamofire.request(Url , method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let _ = response.result.value {
                }
                break
                
            case .failure(_):
                print(response.result.description)
                break
            }
        }
    }
    
}

//MARK:- Internet Connectivity
class Connectivity {
    class func isConnectedToInternet() ->Bool {
        
        return NetworkReachabilityManager()!.isReachable
    }
}


class VersionCheck {
    
    public static let shared = VersionCheck()
    
    var newVersionAvailable: Bool?
    var appStoreVersion: String?
    
    func checkAppStore(callback: ((_ versionAvailable: Bool?, _ version: String?)->Void)? = nil) {
        let ourBundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        Alamofire.request("https://itunes.apple.com/lookup?bundleId=\(ourBundleId)").responseJSON { response in
            var isNew: Bool?
            var versionStr: String?
            
            if let json = response.result.value as? NSDictionary,
                let results = json["results"] as? NSArray,
                let entry = results.firstObject as? NSDictionary,
                let appVersion = entry["version"] as? String,
                let ourVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            {
                isNew = ourVersion != appVersion
                versionStr = appVersion
            }
            
            self.appStoreVersion = versionStr
            self.newVersionAvailable = isNew
            callback?(isNew, versionStr)
        }
    }
}
