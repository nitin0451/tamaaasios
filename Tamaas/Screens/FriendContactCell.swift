//
//  FriendContactCell.swift
//  Tamaas
//
//  Created by Krescent Global on 28/04/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class FriendContactCell: UITableViewCell {
   
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_subTitle: UILabel!
    @IBOutlet weak var imgVw_user: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
    }

   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
