//
//  CallTVcell.swift
//  Tamaas
//
//  Created by Krescent Global on 24/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit

class CallTVcell: UITableViewCell {
    
    @IBOutlet weak var timeLab: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLab: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var imgVw_callType: UIImageView!
    @IBOutlet weak var vwGroupImage: UIView!
    @IBOutlet weak var groupImage1: UIImageView!
    @IBOutlet weak var groupImage2: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setGroupImage(isHidden:Bool) {
        self.vwGroupImage.isHidden = isHidden
    }
    
    func setUserImage(isHidden:Bool) {
        self.userImage.isHidden = isHidden
    }

}
