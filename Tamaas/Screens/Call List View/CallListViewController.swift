//
//  CallListViewController.swift
//  Tamaas
//
//  Created by Krescent Global on 24/01/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import Alamofire
import FTIndicator

class Call {
    var id : String?
    var receiverNo: String?
    var senderID: String?
    var callState: String?
    var createdAt: String?
    var senderNo : String?
    var sessionID : String?
    var GroupName : String?
    var dialogID: String?
    var duration: String?
    var isAudio: Bool?
    var isGroup: Bool?
    var membersID : String?
    var callType : String?
    var groupPhoto:String?
}

class CallListViewController: BaseViewController{
    
    //MARK:- Outlets
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lbl_noCalls: UILabel!
    @IBOutlet weak var lbl_noCall_msg: UILabel!
    
    //MARK:- Variables
   // var arrMissed = [Call]()
    var arrCallLogs_all = [Call]()
    var arrCallLogs_missed = [Call]()
    var arrCallLogs_all_DB = [Call_DB]()
    var arrCallLogs_missed_DB = [Call_DB]()
    var callTo:String = ""
    var callBy:String = ""
    var isGroup:String = ""
    
    
    //MARK:- UIview life-cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Calls".localized
        self.segmentControl.setTitle("All".localized, forSegmentAt: 0)
        self.segmentControl.setTitle("Missed".localized, forSegmentAt: 1)
        
        self.lbl_noCalls.text = "No calls".localized
        self.lbl_noCall_msg.text = "Your recent calls will appear here.\n To add a call please tap on “+” \n icon on top right".localized
        self.tableView.isHidden = true
        segmentControl.addTarget(self, action: #selector(self.segmentedValueChanged(_:)), for: .valueChanged)
        
        if Connectivity.isConnectedToInternet() {
            CoreData_BussinessLogic.deleteAllCalls()
            self.call_get_CallLogs()
        } else {
            self.arrCallLogs_all_DB = CoreData_BussinessLogic.getAllCalls()
            for call in self.arrCallLogs_all_DB {
                if !(call.log_from?.contains(SessionManager.getPhone()))! {
                    if let status = call.status as? String{
                        if status == "missed" {
                            self.arrCallLogs_missed_DB.append(call)
                        }
                    }
                }
            }
            if self.arrCallLogs_all_DB.count == 0{
                self.tableView.isHidden = true
            }
            else{
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }
        }
        
        
    }
    
    
    func call_get_CallLogs() {
        
        self.startAnimating()
        var senderID:String = ""
        
        if let id = SessionManager.get_QuickBloxID(){
            senderID = "\(id)"
        }
        
        let dictParam : NSDictionary = ["senderID":senderID]
        
        WebserviceSigleton().GETServiceWithParameters(urlString: constantVC.GeneralConstants.BASE_URL + constantVC.WebserviceName.URL_GET_CALL_LOG , params: dictParam as! Dictionary<String, AnyObject> , callback: { (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    if let data = response["data"] as? [[String:Any]] {
                        
                        for index in data {
                            
                            if let dic = index as? NSDictionary {
                                
                                let call = Call()
                                
                                if let id_ = dic["id"] {
                                    call.id = String(describing: id_)
                                }
                                
                                if let GroupName = dic["GroupName"] as? String {
                                    call.GroupName = GroupName
                                }
                                
                                if let callState = dic["callState"] as? String {
                                    call.callState = callState
                                }
                                
                                if let createdAt = dic["createdAt"] as? String {
                                    call.createdAt = createdAt
                                }
                                
                                if let dialogID = dic["dialogID"] as? String {
                                    call.dialogID = dialogID
                                }
                                
                                if let duration = dic["duration"] as? String {
                                    call.duration = duration
                                }
                                
                                if let isAudio = dic["isAudio"] as? String {
                                    
                                    if isAudio == "true" {
                                        call.isAudio = true
                                    } else {
                                        call.isAudio = false
                                    }
                                }
                                
                                if let isGroup = dic["isGroup"] as? String {
                                    
                                    if isGroup == "true" {
                                        call.isGroup = true
                                    } else {
                                        call.isGroup = false
                                    }
                                }
                                
                                if let membersID = dic["membersID"] as? String {
                                    call.membersID = membersID
                                }
                                
                                if let receiverNo = dic["receiverNo"] as? String {
                                    call.receiverNo = receiverNo
                                }
                                
                                if let senderID = dic["senderID"] as? String {
                                    call.senderID = senderID
                                }
                                if let senderNo = dic["senderNo"] as? String {
                                    call.senderNo = senderNo
                                }
                                if let sessionID = dic["sessionID"] as? String {
                                    call.sessionID = sessionID
                                }
                                
                                if let groupPhoto = dic["groupPhoto"] as? String {
                                    call.groupPhoto = groupPhoto
                                }
                                
                                //save over DB
                                CoreData_BussinessLogic.saveCallLog(call: call)
                                self.arrCallLogs_all.append(call)
                                
                                if call.senderNo != SessionManager.getPhone() {
                                    
                                    if call.callState == constantVC.callState.missedNoAnswer.rawValue {
                                        self.arrCallLogs_missed.append(call)
                                    }
                                }
                            }
                        }
                        
                        self.stopAnimating()
                        self.tableView.reloadData()
                        if self.arrCallLogs_all.count == 0{
                            self.tableView.isHidden = true
                        }
                        else{
                            self.tableView.isHidden = false
                        }
                    }
                   
                }
                else{
                    self.stopAnimating()
                    self.tableView.isHidden = true
                }
            }
            else{
                self.stopAnimating()
                self.tableView.isHidden = true
            }
        })
    }
    
    
    
    @objc func segmentedValueChanged(_ sender:UISegmentedControl!)
    {
        tableView.reloadData()
        //missed
        if sender.selectedSegmentIndex == 1{
            
            if Connectivity.isConnectedToInternet() {
                if self.arrCallLogs_missed.count == 0{
                    self.tableView.isHidden = true
                }
                else{
                    self.tableView.isHidden = false
                }
            }
            else {
                if self.arrCallLogs_missed_DB.count == 0{
                    self.tableView.isHidden = true
                }
                else{
                    self.tableView.isHidden = false
                }
            }
            
        }
        //all
        else{
            if Connectivity.isConnectedToInternet() {
                if self.arrCallLogs_all.count == 0{
                    self.tableView.isHidden = true
                }
                else{
                    self.tableView.isHidden = false
                }
            }
            else {
                if self.arrCallLogs_all_DB.count == 0{
                    self.tableView.isHidden = true
                }
                else{
                    self.tableView.isHidden = false
                }
            }
        }
        
    }
    
    @IBAction func backAc(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override  func viewWillAppear(_ animated: Bool) {
       // self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.init(name:"Lato-Regular",size:18)!]
    }
    @IBAction func addCallAc(_ sender: Any) {
        
         let controller = self.storyboard?.instantiateViewController(withIdentifier: "MakeCall_ContactPicker") as! MakeCall_ContactPicker
        self.present(controller, animated: true, completion: nil)
    }
    
    func performAudioCall(selectedCall: Call , dialog: QBChatDialog){
        
        if selectedCall.senderNo == SessionManager.getPhone() {
            self.callTo = (selectedCall.receiverNo)!
        } else {
            self.callTo = (selectedCall.senderNo)!
        }
        self.callBy = SessionManager.getPhone()
        
        //make call
        if !constantVC.ActiveSession.isStartedConnectingCall {
            constantVC.ActiveSession.isStartedConnectingCall = true
            CallConnectionManager.instance.prepareCall_AUDIO(opponentNo: self.callTo , dialog: dialog , groupName: "")
        }
    }
    
}

extension CallListViewController : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
         if QuickBloxManager().callsAllowed() {
            
            var selectedCall:Call?
            var opponentNo:String = ""
            
            if segmentControl.selectedSegmentIndex == 1{
                selectedCall = arrCallLogs_missed[indexPath.row]
            }
            else{
                selectedCall = arrCallLogs_all[indexPath.row]
            }
            
            //voice
            if (selectedCall?.isAudio)! {
                
                if let dialog_id = selectedCall?.dialogID {
                    let dialog = ServicesManager.instance().chatService.dialogsMemoryStorage.chatDialog(withID: dialog_id)
                    
                    if dialog != nil {
                        self.performAudioCall(selectedCall: selectedCall! , dialog: dialog!)
                    }
                    else {
                        //create dialog
                        var oppID:String = ""
                        var oppNo:String = ""
                        if let membersID_ = selectedCall?.membersID {
                            var arr = membersID_.components(separatedBy: ",")
                            let filtered = arr.filter { $0 != SessionManager.get_QuickBloxID()?.stringValue }
                            if filtered.indices.contains(0) {
                                oppID = filtered[0]
                            }
                        }
                        
                        if selectedCall?.senderNo == SessionManager.getPhone() {
                            oppNo = (selectedCall?.receiverNo)!
                        } else {
                            oppNo = (selectedCall?.senderNo)!
                        }
                        
                        if oppID != "" {
                            ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: UInt(truncating: NSNumber(value: Int(oppID)!)), oppNo + "-" + SessionManager.getPhone(), isDoctor: false, fees: "") { (response, chatDialog) in
                                if let dialog_ = chatDialog {
                                    self.performAudioCall(selectedCall: selectedCall! , dialog: dialog_)
                                }
                            }
                        }
                    }
                }
            }
            //video
            else {
                
                if let dialog_id = selectedCall?.dialogID {
                    let dialog = ServicesManager.instance().chatService.dialogsMemoryStorage.chatDialog(withID: dialog_id)
                    
                    if dialog != nil {
                        
                        if (selectedCall?.isGroup)! {
                            self.isGroup = "true"
                            self.callTo = (selectedCall?.GroupName)!
                            self.callBy = (selectedCall?.GroupName)!
                        } else {
                            self.isGroup = "false"
                            
                            if selectedCall?.senderNo == SessionManager.getPhone() {
                                self.callTo = (selectedCall?.receiverNo)!
                            } else {
                                self.callTo = (selectedCall?.senderNo)!
                            }
                            self.callBy = SessionManager.getPhone()
                        }
                        
                        //make call
                        if self.isGroup != "true" {
                            
                            if !constantVC.ActiveSession.isStartedConnectingCall {
                                constantVC.ActiveSession.isStartedConnectingCall = true
                                CallConnectionManager.instance.prepareCall_VIDEO(opponentNo: self.callTo , dialog: dialog! , groupName: "")
                            }
                            
                        } else {
                            CallConnectionManager.instance.call(with: QBRTCConferenceType.video , dialog: dialog! , groupName: self.callTo , callTo_: self.callTo)
                        }
                    }
                }
            }
         }
         else {
            FTIndicator.showError(withMessage: "No Internet Connection!".localized)
        }
        
       tableView.deselectRow(at: indexPath , animated: true)
    }
   
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0.1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
}

extension CallListViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentControl.selectedSegmentIndex == 1{
            if Connectivity.isConnectedToInternet() {
                return arrCallLogs_missed.count
            } else {
                return arrCallLogs_missed_DB.count
            }
        }
        else{
            if Connectivity.isConnectedToInternet() {
                return arrCallLogs_all.count
            } else {
                return arrCallLogs_all_DB.count
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! CallTVcell
        
        if Connectivity.isConnectedToInternet() {
            var call = Call()
            
            if segmentControl.selectedSegmentIndex == 1{
                call = arrCallLogs_missed[indexPath.row]
            }
            else{
                call = arrCallLogs_all[indexPath.row]
            }
            
            cell.setUserImage(isHidden: false)
            cell.setGroupImage(isHidden: true)
            
            //my calls
            if call.senderNo == SessionManager.getPhone() {
                
                if call.isGroup! {
                    
                    cell.nameLab.text = call.GroupName
                    
                    if call.groupPhoto != "" {
                        cell.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(call.groupPhoto!).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                    } else {
                        
                        cell.setUserImage(isHidden: true)
                        cell.setGroupImage(isHidden: false)
                        
                        if let groupOccupantsID = call.membersID {
                            
                            var IDS:[NSNumber] = []
                            var arr = groupOccupantsID.components(separatedBy: ",")
                            
                            for id in arr {
                                if let myInteger = Int(id) {
                                    let myNumber = NSNumber(value:myInteger)
                                    IDS.append(myNumber)
                                }
                            }
                            
                            IDS = IDS.filter { $0 != SessionManager.get_QuickBloxID() }
                            
                            if IDS.count > 0 && IDS.count >= 2  {
                                
                                //user1
                                ServicesManager.instance().usersService.getUserWithID( IDS[0] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                                    
                                    if let no = task.result?.phone {
                                        cell.groupImage1.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                                    }
                                    return nil
                                })
                                
                                //user2
                                ServicesManager.instance().usersService.getUserWithID( IDS[1] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                                    
                                    if let no = task.result?.phone {
                                        cell.groupImage2.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                                    }
                                    return nil
                                })
                            }
                        }
                    }
                  
                } else {
                    if SessionManager.getNameFromMyAddressBook(number:  call.receiverNo!) != "" {
                        cell.nameLab.text = SessionManager.getNameFromMyAddressBook(number:  call.receiverNo!)
                    }else {
                        cell.nameLab.text = "+" + call.receiverNo!
                    }
                    
                    if let no = call.receiverNo {
                     cell.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                    }
                }
            }
            //other
            else {
                
                if call.isGroup! {
                    cell.nameLab.text = call.GroupName
                    
                    if call.groupPhoto != "" {
                        cell.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(call.groupPhoto!).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                    } else {
                        
                        cell.setUserImage(isHidden: true)
                        cell.setGroupImage(isHidden: false)
                        
                        if let groupOccupantsID = call.membersID {
                            
                            var IDS:[NSNumber] = []
                            var arr = groupOccupantsID.components(separatedBy: ",")
                            
                            for id in arr {
                                if let myInteger = Int(id) {
                                    let myNumber = NSNumber(value:myInteger)
                                    IDS.append(myNumber)
                                }
                            }
                            
                            IDS = IDS.filter { $0 != SessionManager.get_QuickBloxID() }
                            
                            if IDS.count > 0 && IDS.count >= 2  {
                                
                                //user1
                                ServicesManager.instance().usersService.getUserWithID( IDS[0] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                                    
                                    if let no = task.result?.phone {
                                        cell.groupImage1.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                                    }
                                    return nil
                                })
                                
                                //user2
                                ServicesManager.instance().usersService.getUserWithID( IDS[1] as! UInt , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
                                    
                                    if let no = task.result?.phone {
                                        cell.groupImage2.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                                    }
                                    return nil
                                })
                            }
                        }
                    }
                    
                } else {
                    if SessionManager.getNameFromMyAddressBook(number:  call.senderNo!) != "" {
                        cell.nameLab.text = SessionManager.getNameFromMyAddressBook(number:  call.senderNo!)
                    }else {
                        cell.nameLab.text = "+" + call.senderNo!
                    }
                    
                    if let no = call.senderNo {
                        cell.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(no).jpeg") , placeholder: UIImage(named: "contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation ] , completion: nil)
                    }
                }
            }
            
            
            if call.isAudio! {
                cell.imgVw_callType.image = UIImage(named: "voice_fill")
            } else {
                cell.imgVw_callType.image = UIImage(named: "video_fill")
            }
            
            
            if call.senderNo == SessionManager.getPhone() {
                cell.statusImage.image = UIImage(named: "dialed")
            } else {
                if call.callState == constantVC.callState.missedNoAnswer.rawValue {
                    cell.statusImage.image = UIImage(named: "missed")
                } else {
                    cell.statusImage.image = UIImage(named: "received")
                }
            }
            
            let localDate = constantVC.UTCToLocal(date: call.createdAt!)
            
            if localDate != "" {
                let datefrmter = DateFormatter()
                datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = datefrmter.date(from: localDate)
                cell.timeLab.text = date?.getElapsedInterval_with_TIME()
            }
            
        }
        else {
            
            cell.setUserImage(isHidden: false)
            cell.setGroupImage(isHidden: true)
            
            var call = Call_DB()
            
            if segmentControl.selectedSegmentIndex == 1{
                call = arrCallLogs_missed_DB[indexPath.row]
            }
            else{
                call = arrCallLogs_all_DB[indexPath.row]
            }
            
            cell.userImage.yy_setImage(with: URL.init(string: "\(constantVC.GeneralConstants.ImageUrl)\(call.userImage!).jpeg") , placeholder: UIImage(named:"contactCard") , options: [.progressiveBlur , .setImageWithFadeAnimation] , completion: nil)
            
            if call.log_from != SessionManager.getPhone() {
                cell.nameLab.text = call.log_from
            }
            else {
                cell.nameLab.text = call.log_to
            }
            
            if call.type == "video" {
                cell.imgVw_callType.image = UIImage(named: "video_fill")
            }
            else {
                cell.imgVw_callType.image = UIImage(named: "voice_fill")
            }
            
            
            if let status = call.status as? String {
                cell.statusImage.image = UIImage(named: status)
            }
            let localDate = constantVC.UTCToLocal(date: call.startTime!)
            
            if localDate != "" {
                let datefrmter = DateFormatter()
                datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date = datefrmter.date(from: localDate)
                cell.timeLab.text = date?.getElapsedInterval_with_TIME()
            }
            
        }
        
        return cell
    }
    
}

