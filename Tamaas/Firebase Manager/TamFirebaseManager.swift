//
//  TamFirebaseManager.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 3/18/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

enum firebaseCollection: String {
    case Block = "Block"
    case Mute = "Mute"
    case MuteGroup = "MuteGroup"
    case clearChat = "ClearChat"
    case iosVersionNumber = "iOS_version_number"
    case iosVersion_document = "iosVersion"
}

class TamFirebaseManager: NSObject {
    
    static let shared = TamFirebaseManager()
    
    let db = Firestore.firestore()
    
    
    func saveDocument(collection: String , document: String , completionHandler:@escaping (Bool) -> ()){
        
         db.collection(collection).document(document).setData(["number": document], completion: { err in
            if let err = err {
                completionHandler(false)
            } else {
                completionHandler(true)
            }
        })
    }
    
    
    func getDocument(collection: String , document: String , completionHandler:@escaping (Bool) -> ()){
        let docRef = db.collection(collection).document(document)
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                completionHandler(true)
            } else {
                print("Document does not exist")
                completionHandler(false)
            }
        }
    }
    
    
    func deleteDocument(collection: String , document: String , completionHandler:@escaping (Bool) -> ()){
        db.collection(collection).document(document).delete() { err in
            if let err = err {
                completionHandler(false)
            } else {
                completionHandler(true)
            }
        }
    }
    
    
    //MARK:- Mute Group
    func saveDocument_groupMute(collection: String , doc: String ,newId: String , completionHandler:@escaping (Bool) -> ()){
        
        //get previous data
        let docRef = db.collection(collection).document(doc)
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                if let ids = document.data()!["number"] as? String {
                    var previousIds = ids
                    if previousIds != "" {
                        previousIds = previousIds + "," + newId
                    } else {
                        previousIds = newId
                    }
                    self.updateDocument_group(collection: collection , doc: doc , id: previousIds, completionHandler: { (success) in
                        if success {
                            completionHandler(true)
                        } else {
                            completionHandler(false)
                        }
                    })
                }
            } else {
                print("Document does not exist")
                
                self.updateDocument_group(collection: collection , doc: doc , id: newId , completionHandler: { (success) in
                    if success {
                        completionHandler(true)
                    } else {
                        completionHandler(false)
                    }
                })
            }
        }
    }
    
    
    func deleteDocument_groupMute(collection: String , doc: String , Id: String , completionHandler:@escaping (Bool) -> ()){
        
        //get previous data
        let docRef = db.collection(collection).document(doc)
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                if let ids = document.data()!["number"] as? String {
                    var previousIds = ids
                    if previousIds != "" {
                        let arrMuteIds = previousIds.components(separatedBy: ",")
                        let arrNew = arrMuteIds.filter{$0 != Id}
                        
                        if arrNew.count > 0 {
                            previousIds = arrNew.joined(separator: ",")
                            
                            self.updateDocument_group(collection: collection , doc: doc , id: previousIds, completionHandler: { (success) in
                                if success {
                                    completionHandler(true)
                                } else {
                                    completionHandler(false)
                                }
                            })
                        } else {
                            //delete group mute document
                            self.deleteDocument(collection: collection , document: doc , completionHandler: { (success) in
                            })
                        }
                    }
                }
            } else {
                print("Document does not exist")
            }
        }
    }
    
    
    func updateDocument_group(collection: String , doc: String , id: String , completionHandler:@escaping (Bool) -> ()){
        db.collection(collection).document(doc).setData(["number": id], completion: { err in
            if let err = err {
                completionHandler(false)
            } else {
                completionHandler(true)
            }
        })
    }
    
    
    func isGroupChatMuted(collection: String , doc: String , id: String , completionHandler:@escaping (Bool) -> ()){
        
        let docRef = db.collection(collection).document(doc)
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                if let ids = document.data()!["number"] as? String {
                    var previousIds = ids
                    if previousIds != "" {
                        let arrMuteIds = previousIds.components(separatedBy: ",")
                        
                        if arrMuteIds.contains(id) {
                            completionHandler(true)
                        } else {
                            completionHandler(false)
                        }
                    }
                }
            } else {
                print("Document does not exist")
            }
        }
    }
    
    
    func getGroupMutedUsersId(collection: String , doc: String , completionHandler:@escaping (Bool , String) -> ()){
        
        let docRef = db.collection(collection).document(doc)
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                if let ids = document.data()!["number"] as? String {
                    completionHandler(true , ids)
                }
            } else {
                print("Document does not exist")
                completionHandler(false , "")
            }
        }
    }
    
    
    //MARK:- Get version number
    func getDocument_appVersion(collection: String , document: String , completionHandler:@escaping (Bool , String) -> ()){
        let docRef = db.collection(collection).document(document)
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                
                if let version = document.data()!["version"] as? String {
                    completionHandler(true , version)
                }
                
            } else {
                print("Document does not exist")
                completionHandler(false , "")
            }
        }
    }
   

}
