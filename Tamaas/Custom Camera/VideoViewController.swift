//
//  VideoViewController.swift
//  CameraKitDemo
//
//  Created by Adrian Mateoaea on 17/01/2019.
//  Copyright © 2019 Wonderkiln. All rights reserved.
//

import UIKit
import AVKit
import FTIndicator
import Alamofire

class VideoPreviewViewController: BaseViewController{
    
    var url: URL?
    var image = UIImage()
      var assestUrl: URL? = nil
      var videoLength:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = self.url {
            let player = AVPlayerViewController()
            player.player = AVPlayer(url: url)
            player.view.frame = self.view.bounds
            
            self.view.addSubview(player.view)
          self.addChildViewController(player)
            
            player.player?.play()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    @IBAction func handleCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
     func updateView(){
          
          if constantVC.GlobalVariables.myStoriesCount >= 1{
               constantVC.GlobalVariables.isNewStoryAdded_moreThanOne = true
          }
          else {
               constantVC.GlobalVariables.isNeedToGetStories = true
          }
         constantVC.GlobalVariables.galleryClicked = false
     self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true)
     }
    
     func postAc() {
          self.view.alpha = 1.0
          if !Connectivity.isConnectedToInternet() {
               FTIndicator.showInfo(withMessage: "No Internet Connection!".localized )
          }
          else {
               let actiView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
               actiView.backgroundColor = UIColor.black
               let activityIN : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50)) as UIActivityIndicatorView
               activityIN.center = self.view.center
               activityIN.hidesWhenStopped = true
               activityIN.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
               activityIN.startAnimating()
               actiView.addSubview(activityIN)
               self.view.addSubview(actiView)
               
               Alamofire.upload(
                    multipartFormData: { multipartFormData in
                         
                       let data = NSData(contentsOf: self.assestUrl!)
                         multipartFormData.append(data! as Data, withName: "file",fileName:"\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).mp4", mimeType: "video/mp4")
                         multipartFormData.append("video".data(using: String.Encoding.utf8)!, withName: "type")
                         multipartFormData.append(self.videoLength.data(using: String.Encoding.utf8)!, withName: "time_duration")
                        
                         multipartFormData.append(SessionManager.getUserId().data(using: String.Encoding.utf8)!, withName: "user_id")
               },
                    to: "\(constantVC.GeneralConstants.BASE_URL)\(constantVC.WebserviceName.URL_ADD_STORY)",
                    encodingCompletion: { encodingResult in
                         
                         switch encodingResult {
                         case .success(let upload, _, _):
                              upload.responseJSON { response in
                                   guard response.result.error == nil else {
                                        actiView.removeFromSuperview()
                                        return
                                   }
                                   if let value = response.result.value {
                                        if let result = value as? Dictionary<String, AnyObject> {
                                             let resultMessage = result["msg"] as! String
                                             if resultMessage == "success"{
                                                  actiView.removeFromSuperview()
                                                  self.updateView()
                                             }
                                             else{
                                                  actiView.removeFromSuperview()
                                                  self.showError(message: result["msg"] as! String)
                                             }
                                        }
                                   }
                                   }
                                   .uploadProgress { progress in // main queue by default
                              }
                              return
                         case .failure(let encodingError):
                              actiView.removeFromSuperview()
                              debugPrint(encodingError)
                         }
               })
          }
     }
     
     func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
          let urlAsset = AVURLAsset(url: inputURL, options: nil)
          guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
               handler(nil)
               return
          }
          exportSession.outputURL = outputURL
          exportSession.outputFileType = AVFileType.mov
          exportSession.shouldOptimizeForNetworkUse = true
          exportSession.exportAsynchronously { () -> Void in
               handler(exportSession)
          }
     }
    @IBAction func handleSave(_ sender: Any) {
        if let url = self.url {
          //  UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, #selector(handleDidCompleteSavingToLibrary(path:error:contextInfo:)), nil)
          
          self.view.alpha = 0
          let manager = FileManager.default
          constantVC.GlobalVariables.shCamera = false
          guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else {return}
          guard let mediaType = "mp4" as? String else {return}
         
               let asset = AVAsset(url: url as URL)
               let length = Float(asset.duration.value) / Float(asset.duration.timescale)
               
               self.videoLength = String(length)
               let start = 0
               let end = 10.0
               
               var outputURL = documentDirectory.appendingPathComponent("output")
               do {
                    try manager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                    let name = "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).mp4"
                    outputURL = outputURL.appendingPathComponent("\(name)")
               }catch let error {
                    print(error)
               }
               
               //Remove existing file
               _ = try? manager.removeItem(at: outputURL)
               
               guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {return}
               exportSession.outputURL = outputURL
               exportSession.outputFileType = AVFileType.mp4
               
               let startTime = CMTime(seconds: Double(start), preferredTimescale: 1000)
               let endTime = CMTime(seconds: Double(end), preferredTimescale: 1000)
               let timeRange = CMTimeRange(start: startTime, end: endTime)
               
               exportSession.timeRange = timeRange
               exportSession.exportAsynchronously{
                    switch exportSession.status {
                    case .completed:
                         
                         let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mp4")
                         self.compressVideo(inputURL: outputURL as URL, outputURL: compressedURL) { (exportSession) in
                              self.assestUrl = compressedURL
                         }
                         let assetImageGenerator = AVAssetImageGenerator(asset: asset)
                         assetImageGenerator.appliesPreferredTrackTransform = true
                         
                         var time = asset.duration
                         time.value = min(time.value, 2)
                         
                         do {
                              let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
                              self.image = UIImage(cgImage: imageRef)
                         } catch {
                         }
                         
                         let vc = StoriesFilterVC(image: self.image)
                         vc.delegate = self
                         
                         self.present(vc, animated: true)
                         
                    case .failed:
                         print("failed \(exportSession.error)")
                         
                    case .cancelled:
                         print("cancelled \(exportSession.error)")
                         
                    default: break
                    }
               }
     }
    }
     
    
    @objc func handleDidCompleteSavingToLibrary(path: String?, error: Error?, contextInfo: Any?) {
        self.dismiss(animated: true, completion: nil)
    }
}

class VideoSettingViewController: UITableViewController {

    var previewView: CKFPreviewView!
}

class VideoViewController: UIViewController, CKFSessionDelegate {
    
     @IBOutlet weak var galleryBtn: UIButton!
     @IBOutlet weak var flashBtn: UIButton!
     @IBOutlet weak var zoomLabel: UILabel!
    @IBOutlet weak var captureButton: UIButton!
     private let imageLoader = FAImageLoader()
     
     var shSwap = true
     var shFlash = 2
    func didChangeValue(session: CKFSession, value: Any, key: String) {
        if key == "zoom" {
            self.zoomLabel.text = String(format: "%.1fx", value as! Double)
        }
    }
     private func loadPhotos(){
          imageLoader.loadPhotos { (assets) in
               self.configureImageCropper(assets: assets)
          }
     }
     private func configureImageCropper(assets:[PHAsset]){
          
          print(assets.count)
          if assets.count != 0{
               FAImageLoader.imageFrom(asset: assets[assets.count-1], size: FAImagePlaceHolderSize) { (image) in
                    DispatchQueue.main.async {
                         self.galleryBtn.setBackgroundImage(image, for: UIControlState.normal)
                    }
               }
               
          }
     }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? VideoSettingViewController {
            vc.previewView = self.previewView
        } else if let nvc = segue.destination as? UINavigationController, let vc = nvc.childViewControllers.first as? VideoPreviewViewController {
            vc.url = sender as? URL
        }
    }
    
    @IBOutlet weak var previewView: CKFPreviewView! {
        didSet {
            let session = CKFVideoSession()
            session.delegate = self
            
            self.previewView.autorotate = true
            self.previewView.session = session
            self.previewView.previewLayer?.videoGravity = .resizeAspectFill
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      loadPhotos()
    }
     override func viewWillAppear(_ animated: Bool) {
            UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
     }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.previewView.session?.start()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.previewView.session?.stop()
       UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
    }
     
     @IBAction func flashAc(_ sender: Any) {
          if shFlash == 0{
               shFlash = 1
               flashBtn.setImage(UIImage(named:"flashon"), for: .normal)
          }else if shFlash == 1{
               shFlash = 2
               flashBtn.setImage(UIImage(named:"flashoff"), for: .normal)
          }
          else{
               shFlash = 0
               flashBtn.setImage(UIImage(named:"flashauto"), for: .normal)
          }
          if let session = self.previewView.session as? CKFVideoSession {
               let values: [CKFVideoSession.FlashMode] = [.auto, .on, .off]
               session.flashMode = values[shFlash]
          }
     }
     
     @IBAction func closeAc(_ sender: Any) {
          constantVC.GlobalVariables.galleryClicked = false
          self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil);
    
     }
     
     @IBAction func galleryAc(_ sender: Any) {
          constantVC.GlobalVariables.galleryClicked = true
          self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil);
     
     }
     @IBAction func swapAc(_ sender: Any) {
          if let session = self.previewView.session as? CKFVideoSession {
               if shSwap == true{
                    shSwap = false
                    session.cameraPosition = .front
               }else{
                    shSwap = true
                    session.cameraPosition = .back
               }
          }
     }
     
    @IBAction func handleCapture(_ sender: UIButton) {
        if let session = self.previewView.session as? CKFVideoSession {
            if session.isRecording {
               sender.backgroundColor = UIColor.white
                session.stopRecording()
            } else {
                sender.backgroundColor = UIColor(red:255.0/255.0,green:38.0/255.0,blue:0.0,alpha:0.5)
                session.record({ (url) in
                    self.performSegue(withIdentifier: "Preview", sender: url)
                }) { (_) in
                    //
                }
            }
        }
    }
    
    @IBAction func handlePhoto(_ sender: Any) {
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
     
        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromLeft, animations: {
          self.dismiss(animated: true, completion: nil)
        }, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
extension VideoPreviewViewController: StoriesFilterDelegate {
     func StoriesFilterImageDidFilter(image: UIImage) {
          self.image = image
          self.postAc()
     }
     
     func StoriesFilterDidCancel() {
          self.view.alpha = 1.0
     }
}
