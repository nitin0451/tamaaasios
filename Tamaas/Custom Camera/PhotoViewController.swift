//
//  PhotoViewController.swift
//  CameraKitDemo
//
//  Created by Adrian Mateoaea on 08/01/2019.
//  Copyright © 2019 Wonderkiln. All rights reserved.
//

import UIKit
import FTIndicator
import Alamofire

class PhotoPreviewViewController: BaseViewController, UIScrollViewDelegate {
    
    var image: UIImage?
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.image = self.image
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func handleCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func handleSave(_ sender: Any) {
        if let image = self.image {
    
       self.image = image.fixOrientation()
          
        
          
          let vc = StoriesFilterVC(image: self.image!)
          vc.delegate = self
          self.view.alpha = 0
          self.present(vc, animated: true)
        }
    }
     func updateView(){
          
          if constantVC.GlobalVariables.myStoriesCount >= 1{
               constantVC.GlobalVariables.isNewStoryAdded_moreThanOne = true
          }
          else {
               constantVC.GlobalVariables.isNeedToGetStories = true
          }
          constantVC.GlobalVariables.galleryClicked = false
          self.presentingViewController?.presentingViewController?.dismiss(animated: true)
     }
     func postAc() {
          self.view.alpha = 1.0
          if !Connectivity.isConnectedToInternet() {
               FTIndicator.showInfo(withMessage: "No Internet Connection!".localized )
               
          }
          else {
               
               let actiView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
               actiView.backgroundColor = UIColor.black
               let activityIN : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50)) as UIActivityIndicatorView
               activityIN.center = self.view.center
               activityIN.hidesWhenStopped = true
               activityIN.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
               activityIN.startAnimating()
               actiView.addSubview(activityIN)
               self.view.addSubview(actiView)
               
               Alamofire.upload(
                    multipartFormData: { multipartFormData in
                         
                         if constantVC.GlobalVariables.shCamera == true{
                              let photo = self.image
                              var imageData : Data?
                              if UIImageJPEGRepresentation(photo!, 1)!.count  < 5000000{
                                   imageData = UIImageJPEGRepresentation(photo!, 1)!
                              }
                              else if UIImageJPEGRepresentation(photo!, 1)!.count  < 8000000 && UIImageJPEGRepresentation(photo!, 1)!.count > 5000000{
                                   imageData = UIImageJPEGRepresentation(photo!, 0.2)
                              }
                              else{
                                   imageData = UIImageJPEGRepresentation(photo!, 0.5)
                              }
                              print(imageData!.count)
                              multipartFormData.append(imageData!, withName: "file", fileName: "\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).jpeg", mimeType: "image/jpeg")
                              
                              multipartFormData.append("image".data(using: String.Encoding.utf8)!, withName: "type")
                              multipartFormData.append("6".data(using: String.Encoding.utf8)!, withName: "time_duration")
                         }
//                         else{
//                              let data = NSData(contentsOf: self.assestUrl!)
//                              multipartFormData.append(data! as Data, withName: "file",fileName:"\(String(NSDate().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")).mp4", mimeType: "video/mp4")
//                              multipartFormData.append("video".data(using: String.Encoding.utf8)!, withName: "type")
//                              multipartFormData.append(self.videoLength.data(using: String.Encoding.utf8)!, withName: "time_duration")
//                         }
                         multipartFormData.append(SessionManager.getUserId().data(using: String.Encoding.utf8)!, withName: "user_id")
               },
                    to: "\(constantVC.GeneralConstants.BASE_URL)\(constantVC.WebserviceName.URL_ADD_STORY)",
                    encodingCompletion: { encodingResult in
                         
                         switch encodingResult {
                         case .success(let upload, _, _):
                              upload.responseJSON { response in
                                   guard response.result.error == nil else {
                                        actiView.removeFromSuperview()
                                        return
                                   }
                                   if let value = response.result.value {
                                        if let result = value as? Dictionary<String, AnyObject> {
                                             let resultMessage = result["msg"] as! String
                                             if resultMessage == "success"{
                                                  actiView.removeFromSuperview()
                                                  self.updateView()
                                             }
                                             else{
                                                  actiView.removeFromSuperview()
                                                  self.showError(message: result["msg"] as! String)
                                             }
                                        }
                                   }
                                   }
                                   .uploadProgress { progress in // main queue by default
                              }
                              return
                         case .failure(let encodingError):
                              actiView.removeFromSuperview()
                              debugPrint(encodingError)
                         }
               })
          }
     }
}



class PhotoViewController: UIViewController, CKFSessionDelegate {
    var shSwap = true
     var shFlash = 2
    @IBOutlet weak var squareLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var wideLayoutConstraint: NSLayoutConstraint!
    @IBOutlet weak var zoomLabel: UILabel!
    @IBOutlet weak var captureButton: UIButton!
    private let imageLoader = FAImageLoader()
     
    func didChangeValue(session: CKFSession, value: Any, key: String) {
        if key == "zoom" {
            self.zoomLabel.text = String(format: "%.1fx", value as! Double)
        }
    }
     
     @IBAction func flashAc(_ sender: Any) {
         
          if shFlash == 0{
               shFlash = 1
               flashBtn.setImage(UIImage(named:"flashon"), for: .normal)
          }else if shFlash == 1{
               shFlash = 2
                flashBtn.setImage(UIImage(named:"flashoff"), for: .normal)
          }
          else{
               shFlash = 0
                flashBtn.setImage(UIImage(named:"flashauto"), for: .normal)
          }
          if let session = self.previewView.session as? CKFPhotoSession {
               let values: [CKFPhotoSession.FlashMode] = [.auto, .on, .off]
               session.flashMode = values[shFlash]
          }
     }
     @IBAction func closeAc(_ sender: Any) {
          constantVC.GlobalVariables.galleryClicked = false
          self.dismiss(animated: true, completion: nil)
     }
     @IBAction func libraryAc(_ sender: Any) {
          constantVC.GlobalVariables.galleryClicked = true
          self.dismiss(animated: true, completion: nil)
     }
     @IBAction func swapAc(_ sender: Any) {
          
          if let session = self.previewView.session as? CKFPhotoSession {
               if shSwap == true{
                    shSwap = false
                     session.cameraPosition = .front
               }else{
                    shSwap = true
                     session.cameraPosition = .back
               }
          }
     }
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         
          if let nvc = segue.destination as? UINavigationController, let vc = nvc.childViewControllers.first as? PhotoPreviewViewController {
            vc.image = sender as? UIImage
    }
}
     @IBOutlet weak var flashBtn: UIButton!
     @IBOutlet weak var galleryPhoto: UIButton!
     @IBOutlet weak var previewView: CKFPreviewView! {
        didSet {
            let session = CKFPhotoSession()
            session.resolution = CGSize(width: 3024, height: 4032)
            session.delegate = self
            
            self.previewView.autorotate = true
            self.previewView.session = session
            self.previewView.previewLayer?.videoGravity = .resizeAspectFill
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPhotos()
    }
     func loadSubView(){
         
     flashBtn.setImage(UIImage(named:"flashoff"), for: .normal)
         
     }
     private func loadPhotos(){
          imageLoader.loadPhotos { (assets) in
               self.configureImageCropper(assets: assets)
          }
     }
     
     private func configureImageCropper(assets:[PHAsset]){
         
          if assets.count != 0{
               FAImageLoader.imageFrom(asset: assets[assets.count-1], size: FAImagePlaceHolderSize) { (image) in
                    DispatchQueue.main.async {
                         self.galleryPhoto.setBackgroundImage(image, for: UIControlState.normal)
                    }
               }
          }
     }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.previewView.session?.start()
       UIApplication.shared.statusBarView?.backgroundColor = UIColor.black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.previewView.session?.stop()
       UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
    }
    
    @IBAction func handleCapture(_ sender: Any) {
        if let session = self.previewView.session as? CKFPhotoSession {
            session.capture({ (image, _) in
                self.performSegue(withIdentifier: "Preview", sender: image)
            }) { (_) in
                //
               self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func handleVideo(_ sender: Any) {
     guard let window = UIApplication.shared.keyWindow else {
          return
     }
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Video")
        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            self.present(vc, animated: true, completion: nil)
        }, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
}

extension PhotoPreviewViewController: StoriesFilterDelegate {
     func StoriesFilterImageDidFilter(image: UIImage) {
          self.image = image
          self.postAc()
     }
     
     func StoriesFilterDidCancel() {
          self.view.alpha = 1.0
     }
}
