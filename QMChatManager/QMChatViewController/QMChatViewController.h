//
//  QMChatViewController.h
//  QMChatViewController
//
//  Created by Andrey Ivanov on 06.04.15.
//  Copyright (c) 2015 QuickBlox Team. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QMChatCollectionView.h"
#import "QMChatCollectionViewDelegateFlowLayout.h"
#import "QMChatCollectionViewFlowLayout.h"
#import "QMChatActionsHandler.h"
#import "QMInputToolbar.h"
#import <Quickblox/Quickblox.h>

#import "QMChatContactRequestCell.h"
#import "QMChatIncomingCell.h"
#import "QMChatOutgoingCell.h"
#import "QMChatNotificationCell.h"
#import "QMChatAttachmentIncomingCell.h"
#import "QMChatAttachmentOutgoingCell.h"
#import "QMChatLocationIncomingCell.h"
#import "QMChatLocationOutgoingCell.h"
#import "QMChatDataSource.h"

#import "QMVideoIncomingCell.h"
#import "QMAudioIncomingCell.h"
#import "QMImageIncomingCell.h"

#import "QMVideoOutgoingCell.h"
#import "QMImageOutgoingCell.h"
#import "QMAudioOutgoingCell.h"

#import "TamContactIncomingCell.h"
#import "TamContactOutgoingCell.h"
#import "QMChatReplyOutgoingCell.h"
#import "QMChatReplyIncomingCell.h"

#import "QMMediaViewDelegate.h"
#import "QMChatBaseLinkPreviewCell.h"
#import "QMChatIncomingLinkPreviewCell.h"
#import "QMChatOutgoingLinkPreviewCell.h"
#import "QMMediaController.h"
#import "QMImagePreview.h"
#import "QMPhoto.h"
#import <NYTPhotoViewer/NYTPhotoViewer.h>
#import "QMImagePreview.h"
#import "QMCallNotificationItem.h"

@class ACTabScrollView;
@class QMMediaController;
@class TMTextMessageReplyView;


NS_ASSUME_NONNULL_BEGIN

@interface QMChatViewController : UIViewController <QMChatCollectionViewDataSource, QMChatCollectionViewDelegateFlowLayout, UITextViewDelegate>

@property (strong, nonatomic) QMMediaController *mediaController;
@property (weak, nonatomic) IBOutlet UIButton *btnDots;
@property (weak, nonatomic) IBOutlet UIButton *btnVideoCall;
@property (weak, nonatomic) IBOutlet UIButton *btnVoiceCall;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTyping;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIView *vwParticipantsDropDown;
@property (weak, nonatomic) IBOutlet UIImageView *userImg_topTitle;
@property (weak, nonatomic) IBOutlet UIView *vwGroupImg;
@property (weak, nonatomic) IBOutlet UIImageView *usergroupImg1;
@property (weak, nonatomic) IBOutlet UIImageView *usergroupImg2;
@property (weak, nonatomic) IBOutlet UIView *onlineView;

//input bar
@property (weak, nonatomic) IBOutlet UIView *inputBarView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputBarView_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputBarView_bottom;
@property (weak, nonatomic) IBOutlet UIButton *btnAddAccessory;
@property (weak, nonatomic) IBOutlet UITextView *msgTextView;
@property (weak, nonatomic) IBOutlet UIButton *btnSmiley;
@property (weak, nonatomic) IBOutlet UIButton *btnMic_Send;
@property (weak, nonatomic) IBOutlet UIButton *btnChatInfo;


//Accessory
@property (weak, nonatomic) IBOutlet UIView *accessoryView;
@property (weak, nonatomic) IBOutlet UIView *stickerView;
@property (weak, nonatomic) IBOutlet ACTabScrollView *stcikerScollTab;
@property (weak, nonatomic) IBOutlet UIButton *btnAlbum;
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;
@property (weak, nonatomic) IBOutlet UIButton *btnAccessoryVideoCall;
@property (weak, nonatomic) IBOutlet UIButton *btnAccessoryVoiceCall;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_vwAccessory_height;


//sticker
@property (weak, nonatomic) IBOutlet UIButton *btnStickerOption1;
@property (weak, nonatomic) IBOutlet UIButton *btnStickerOption2;
@property (weak, nonatomic) IBOutlet UIButton *btnStickerOption3;
@property (weak, nonatomic) IBOutlet UIButton *btnStickerOption4;
@property (weak, nonatomic) IBOutlet UIButton *btnStickerOption5;
@property (weak, nonatomic) IBOutlet UIButton *btnStickerOption6;
@property (weak, nonatomic) IBOutlet UIButton *btnStickerOption7;
@property (weak, nonatomic) IBOutlet UIButton *btnStickerOption8;
@property (weak, nonatomic) IBOutlet UIView *vwStickerSelection1;
@property (weak, nonatomic) IBOutlet UIView *vwStickerSelection2;
@property (weak, nonatomic) IBOutlet UIView *vwStickerSelection3;
@property (weak, nonatomic) IBOutlet UIView *vwStickerSelection4;
@property (weak, nonatomic) IBOutlet UIView *vwStickerSelection5;
@property (weak, nonatomic) IBOutlet UIView *vwStickerSelection6;
@property (weak, nonatomic) IBOutlet UIView *vwStickerSelection7;
@property (weak, nonatomic) IBOutlet UIView *vwStickerSelection8;
@property (weak, nonatomic) IBOutlet UIButton *btnSendVoiceMsg;
@property (weak, nonatomic) IBOutlet UILabel *lblAudioTimer;
@property (weak, nonatomic) IBOutlet UIButton *btnCloseRecordingView;
@property (weak, nonatomic) IBOutlet UIView *animatedFrame;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_vwSticker_height;



//recording View
@property (weak, nonatomic) IBOutlet UIView *recordingVw;
    
    
//Permission view
    @property (weak, nonatomic) IBOutlet UIView *vwPermission;
    @property (weak, nonatomic) IBOutlet UILabel *lblpermissionText;
    @property (weak, nonatomic) IBOutlet UIButton *btnBlockPermission;
    @property (weak, nonatomic) IBOutlet UIButton *btnAddToContacts;
    @property (weak, nonatomic) IBOutlet UIButton *btnSkipForNow;
@property (weak, nonatomic) IBOutlet UIView *vwPermissionContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_collectionViewTop;

//Reply View
@property (weak, nonatomic) IBOutlet UIView *vwReply;
@property (weak, nonatomic) IBOutlet UIButton *vwReply_btnReply;
@property (weak, nonatomic) IBOutlet UIButton *vwReply_btnCopy;
@property (weak, nonatomic) IBOutlet UIButton *vwReply_btnDelete;
@property (weak, nonatomic) IBOutlet UIView *vwBlur;
@property (weak, nonatomic) IBOutlet TMTextMessageReplyView *TextMsgReplyContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCollectionVw_bottom;
@property (weak, nonatomic) IBOutlet UIView *vwReplyCopy;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_vwReplyCopy_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_replyContainerHeight;
    @property (weak, nonatomic) IBOutlet UIButton *vwReply_btnForward;
    

//Security view
@property (weak, nonatomic) IBOutlet UIView *vwSecureMsg;

 //Forward View
    @property (weak, nonatomic) IBOutlet UIView *vwForwardMsg;
    @property (weak, nonatomic) IBOutlet UIButton *vwForward_btnForward;
    @property (weak, nonatomic) IBOutlet UILabel *vwForward_lblForwardMsgCount;
    @property (weak, nonatomic) IBOutlet UIButton *vwForward_btnCancel;    

@property (strong, nonatomic) QMChatDataSource *chatDataSource;
/**
 *  Cell's contact request delegate.
 */
@property (weak, nonatomic) id <QMChatActionsHandler> actionsHandler;

/**
 *  Returns the collection view object managed by this view controller.
 *  This view controller is the collection view's data source and delegate.
 */
@property (weak, nonatomic, readonly) QMChatCollectionView *collectionView;

/**
 *  Returns the input toolbar view object managed by this view controller.
 *  This view controller is the toolbar's delegate.
 */
//@property (weak, nonatomic, readonly) QMInputToolbar *inputToolbar;

/**
 Progress view. Is hiden by default.
 */
@property (weak, nonatomic) IBOutlet FFCircularProgressView *progressView;

/**
 *  The display name of the current user who is sending messages.
 *
 *  @discussion This value does not have to be unique. This value must not be `nil`.
 */
@property (copy, nonatomic) NSString *senderDisplayName;

/**
 *  The string identifier that uniquely identifies the current user sending messages.
 *
 *  @discussion This property is used to determine if a message is incoming or outgoing.
 *  All message data objects returned by `collectionView:messageDataForItemAtIndexPath:` are
 *  checked against this identifier. This value must not be `nil`.
 */
@property (assign, nonatomic) NSUInteger senderID;

/**
 *  Specifies whether or not the view controller should automatically scroll to the most recent message
 *  when the view appears and when sending, receiving, and composing a new message.
 *
 *  @discussion The default value is `YES`, which allows the view controller to scroll automatically to the most recent message.
 *  Set to `NO` if you want to manage scrolling yourself.
 */
@property (assign, nonatomic) BOOL automaticallyScrollsToMostRecentMessage;

/**
 *  Specifies an additional inset amount to be added to the collectionView's contentInsets.top value.
 *
 *  @discussion Use this property to adjust the top content inset to account for a custom subview at the top of your view controller.
 */
@property (assign, nonatomic) CGFloat topContentAdditionalInset;

/**
 *  Enable text checking types for cells. Must be set in view did load.
 */
@property (assign, nonatomic) NSTextCheckingTypes enableTextCheckingTypes;

/**
 *  Method to create chat message text attributed string. Have to be overriden in subclasses.
 *
 *  @param messageItem Chat message instance.
 *
 *  @return Configured attributed string.
 */
- (nullable NSAttributedString *)attributedStringForItem:(QBChatMessage *)messageItem;

/**
 *  Method to create chat message top label attributed string (Usually - chat message owner name). Have to be overriden in subclasses.
 *
 *  @param messageItem Chat message instance.
 *
 *  @return Configured attributed string.
 */
- (nullable NSAttributedString *)topLabelAttributedStringForItem:(QBChatMessage *)messageItem;

/**
 *  Method to create chat message bottom label attributed string (Usually - chat message date sent). Have to be overriden in subclasses.
 *
 *  @param messageItem Chat message instance.
 *
 *  @return Configured attributed string.
 */
- (nullable NSAttributedString *)bottomLabelAttributedStringForItem:(QBChatMessage *)messageItem;

/**
 *  Collection Cell View class for specific message. Have to be overriden in subclasses. Defaults cells are supplied with QMChatViewController.
 *
 *  @param item Chat message instance.
 *
 *  @return Collection Cell View Class
 */
- (Class)viewClassForItem:(QBChatMessage *)item;

- (void)collectionView:(QMChatCollectionView *)collectionView configureCell:(UICollectionViewCell *)cell forIndexPath:(NSIndexPath *)indexPath;

#pragma mark - Class methods

/**
 *  Returns the `UINib` object initialized for a `QMChatViewController`.
 *
 *  @return The initialized `UINib` object or `nil` if there were errors during initialization
 *  or the nib file could not be located.
 *
 *  @discussion You may override this method to provide a customized nib. If you do,
 *  you should also override `messagesViewController` to return your
 *  view controller loaded from your custom nib.
 */
+ (UINib *)nib;

/**
 *  Creates and returns a new `QMChatViewController` object.
 *
 *  @discussion This is the designated initializer for programmatic instantiation.
 *
 *  @return An initialized `QMChatViewController` object if successful, `nil` otherwise.
 */
+ (instancetype)messagesViewController;

#pragma mark - Messages view controller

/**
 *  This method is called when the user taps the send button on the inputToolbar
 *  after composing a message with the specified data.
 *
 *  @param button            The send button that was pressed by the user.
 *  @param text              The message text.
 *  @param senderId          The message sender identifier.
 *  @param senderDisplayName The message sender display name.
 *  @param date              The message date.
 */
- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSUInteger)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date;

/**
 *  This method is called when the user taps the send button on the inputToolbar
 *  after composing a message with the specified data.
 *
 *  @param button            The send button that was pressed by the user.
 *  @param textAttachments   Array of NSTextAttachment.
 *  @param senderId          The message sender identifier.
 *  @param senderDisplayName The message sender display name.
 *  @param date              The message date.
 */
- (void)didPressSendButton:(UIButton *)button
       withTextAttachments:(NSArray *)textAttachments
                  senderId:(NSUInteger)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date;
/**
 *  This method is called when the user taps the accessory button on the `inputToolbar`.
 *
 *  @param sender The accessory button that was pressed by the user.
 */
//- (void)didPressAccessoryButton:(UIButton *)sender;

/**
 *  This method is called when the user finishes picking attachment image.
 *
 *  @param image    image that was picked by user
 */
- (void)didPickAttachmentImage:(UIImage *)image;
- (void)didPickAttachmentVideoUrl:(NSURL *)url;


/**
 *  Animates the sending of a new message. See `finishSendingMessageAnimated:` for more details.
 *
 *  @see `finishSendingMessageAnimated:`.
 */
- (void)finishSendingMessage;

/**
 *  Completes the "sending" of a new message by resetting the `inputToolbar`, adding a new collection view cell in the collection view,
 *  reloading the collection view, and scrolling to the newly sent message as specified by `automaticallyScrollsToMostRecentMessage`.
 *  Scrolling to the new message can be animated as specified by the animated parameter.
 *
 *  @param animated Specifies whether the sending of a message should be animated or not. Pass `YES` to animate changes, `NO` otherwise.
 *
 *  @discussion You should call this method at the end of `didPressSendButton: withMessageText: senderId: senderDisplayName: date`
 *  after adding the new message to your data source and performing any related tasks.
 *
 *  @see `automaticallyScrollsToMostRecentMessage`.
 */
- (void)finishSendingMessageAnimated:(BOOL)animated;

/**
 *  Animates the receiving of a new message. See `finishReceivingMessageAnimated:` for more details.
 *
 *  @see `finishReceivingMessageAnimated:`.
 */
- (void)finishReceivingMessage;

/**
 *  Completes the "receiving" of a new message by adding a new collection view cell in the collection view,
 *  reloading the collection view, and scrolling to the newly sent message as specified by `automaticallyScrollsToMostRecentMessage`.
 *  Scrolling to the new message can be animated as specified by the animated parameter.
 *
 *  @param animated Specifies whether the receiving of a message should be animated or not. Pass `YES` to animate changes, `NO` otherwise.
 *
 *  @discussion You should call this method after adding a new "received" message to your data source and performing any related tasks.
 *
 *  @see `automaticallyScrollsToMostRecentMessage`.
 */
- (void)finishReceivingMessageAnimated:(BOOL)animated;

/**
 Input bar start pos
 */
- (NSUInteger)inputToolBarStartPos;

/**
 *  Scrolls the collection view such that the bottom most cell is completely visible, above the `inputToolbar`.
 *
 *  @param animated Pass `YES` if you want to animate scrolling, `NO` if it should be immediate.
 */
- (void)scrollToBottomAnimated:(BOOL)animated;

/**
 *  Hides keyboard
 *
 *  @param animated Pass `YES` if you want to animate hiding, `NO` if it should be immediate.
 */
- (void)hideKeyboard:(BOOL)animated;

/**
 Make the background layer to spin around its center. This should be called in the main thread.
 */
- (void)startSpinProgress;

/**
 Stop the spinning of the background layer. This should be called in the main thread.
 */
- (void)stopSpinProgress;

#pragma mark - Methods requiring super

- (void)viewDidLoad NS_REQUIRES_SUPER;
- (void)viewWillAppear:(BOOL)animated NS_REQUIRES_SUPER;

@end

NS_ASSUME_NONNULL_END
