//
//  QMChatReplyOutgoingCell.h
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 5/10/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QMChatCell.h"
#import "TTTAttributedLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface QMChatReplyOutgoingCell : QMChatCell

@property (weak, nonatomic) IBOutlet UIView *vwReplyContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblMsgToReply;
@property (weak, nonatomic) IBOutlet UILabel *lblMsgText;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintReplyVw_height;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwThumbnail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidth;

@end

NS_ASSUME_NONNULL_END
