//
//  QMAudioIncomingCell.h
//  Pods
//
//  Created by Vitaliy Gurkovsky on 2/13/17.
//
//

#import "QMMediaIncomingCell.h"
#import "QMProgressView.h"

@interface QMAudioIncomingCell : QMMediaIncomingCell

@property (weak, nonatomic) IBOutlet QMProgressView *progressView;
@property (weak, nonatomic) IBOutlet UISlider *slider;


@property (weak, nonatomic) IBOutlet UIView *vwReplyContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblReplyToMsg;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwThumbnail;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_replyContainerHeight;

@end
