//
//  QMChatReplyIncomingCell.h
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 5/10/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"
#import "QMChatCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface QMChatReplyIncomingCell : QMChatCell

@property (weak, nonatomic) IBOutlet UIView *vwReplyContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblMsgToReply;
@property (weak, nonatomic) IBOutlet UILabel *lblMsg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_vwReplyContainer_height;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwthumbnail;
@property (weak, nonatomic) IBOutlet UILabel *lblMsgSenderName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_vwReplyContainer_top;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userNameHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidth;





@end

NS_ASSUME_NONNULL_END
