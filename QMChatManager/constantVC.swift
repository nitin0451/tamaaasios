
import UIKit
import PushKit
import CallKit
import YYWebImage

let kChatPresenceTimeInterval:TimeInterval = 45
let kDialogsPageLimit:UInt = 100
let kMessageContainerWidthPadding:CGFloat = 90.0


class constantVC: NSObject {
    
    class var QB_USERS_ENVIROMENT: String {
        
        #if DEBUG
            return "dev"
        #elseif QA
            return "qbqa"
        #else
            assert(false, "Not supported build configuration")
            return ""
        #endif
    }
    
    struct GlobalColor {
        static let appColor = UIColor(red: 187.0 / 255.0, green: 138.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0)
        static let titleColor = UIColor(red: 54.0/255.0, green: 54.0/255.0, blue: 54.0/255.0, alpha: 1.0)
        static let lightBlackColor = UIColor(red: 54.0, green: 54.0, blue: 54.0, alpha: 0.8)
        static let bgColor = UIColor(red: 217.0/255.0, green: 6.0/255.0, blue: 71.0/255.0, alpha: 0.4)
    }
   
    // MARK: - URL
    struct WebserviceName {
        static let URL_GET_USER_PHONE       = "GetUserByPhone"
        static let URL_SIGNUP               = "SignUp"
        static let URL_ADD_POST             = "AddPost"
        static let URL_GET_POST             = "GetPosts"
        static let URL_GET_COMMENTS         = "GetPostComments"
        static let URL_POST_COMMENT         = "PostComment"
        static let URL_LIKE_POST            = "PostLike"
        static let URL_GETALL_USERS         = "GetAllContactByPhones"
        static let URL_CREATE_CONTACT_LIST  = "CreateContactList"
        static let URL_GET_STORIES          = "GetStoriesOfUser"
        static let URL_ADD_STORY            = "AddStory"
        static let URL_GET_STORYBOARD       = "GetStories"
        static let URL_FILEUPLOAD           = "FileUpload"
        static let URL_REMOVEDATA           = "RemoveData"
        static let URL_BLOCKUSER            = "BlockUnBlockUser"
        static let URL_GETBLOCKED           = "GetBlockList"
        static let URL_READSTORY            = "StoryRead"
        static let URL_SEARCH               = "Search"
        static let URL_NUMBER_OF_VIEWS_ON_STORY  = "NumberofViewsOnStory"
        static let URL_UPDATE_PHOTO              = "updatePhotoUrl"
        static let URL_DELETE_ACCOUNT            = "DeleteAccountWithDetails"
        static let URL_SUBMIT_NEWS_REPORT        = "SubmitNewsReport"
        static let URL_GET_NOTIFICATIONS        = "getAllNotifications"
        static let URL_GET_NOTIFICATION_UNREAD_COUNT = "notification_status"
        static let URL_SET_NOTIFICATION_STATUS_READ = "notification_status_read"
        static let URL_GET_POST_BY_ID = "getPostData"
        static let URL_DELETE_CONTACTS = "truncateContactList"
        static let URL_ADD_DEVICE_UUID = "addDeviceUUID"
        static let URL_UPDATE_USER_UNLOCK_STICKERS = "UpdateUserUnlockStickers"
        static let URL_GET_USER_UNLOCK_STICKERS = "getUserUnlockStickers"
        static let URL_SET_NOTIFICATION_ACTIVE_STATUS = "ChangeNotificationStatus"
        static let URL_ADD_CALL_LOG = "AddCallLog"
        static let URL_GET_CALL_LOG = "GetCallLog"
        static let URL_SUBMIT_GROUP_REPORT = "SubmitGroupReport"
        static let URL_SUBMIT_POLLING_ANS = "User_polling_post"
        static let URL_UPDATE_FCM_TOKEN = "updatetoken"
        static let URL_UPDATE_PROFILE_STATUS = "updateProfileStatus"
        static let URL_GET_UPDATED_PROFILE_STATUS = "getProfileUpdatedStatus"
        
        //Nitin Health
        static let URL_GET_DOCTOR_LISTING = "\(GeneralConstants.BASE_IP)tmwallet/public/api/doctorList"
        static let URL_POST_DOCTOR_RATING =  "\(GeneralConstants.BASE_IP)tmwallet/public/api/rating"
        static let URL_GET_RATING_LIST =  "\(GeneralConstants.BASE_IP)tmwallet/public/api/ratingList"
        static let URL_SEND_PAYMENT_DETAIL = "\(GeneralConstants.BASE_IP)salniazi-app/str/index.php"
        static let URL_SAVE_FETCH_CARDDETAIL = "\(GeneralConstants.BASE_IP)salniazi-app/api.php"
    }
    
    // MARK: - General Constants
    //http://18.218.233.174
    //http://18.220.29.159/  --changed on 15/05/2018
    //http://tamaahs.com/  --changed on 21/06/2019
    
    
    struct NSNotification_name {
         static let NSNotification_profileUpdate   = Notification.Name("profileUpdate")
         static let NSNotification_postLikeComment   = Notification.Name("postLikeComment")
         static let NSNotification_bellUnreadCount   = Notification.Name("bellUnreadCount")
         static let NSNotification_InternetDisconnected   = Notification.Name("InternetDisconnected")
         static let NSNotification_InternetConnected   = Notification.Name("InternetConnected")
         static let NSNotification_updateNotificationList   = Notification.Name("updateNotificationList")
         static let NSNotification_loadMsgs   = Notification.Name("loadMessages")
         static let NSNotification_loadDialogs   = Notification.Name("loadDialogs")
         static let NSNotification_updateTimer = Notification.Name("updateCallTimer")
    }
    
    struct GeneralConstants {
        static let BASE_IP     = "http://tamaahs.com/"
        static let KEY_USER_ID = "userId"
        static let UserPhone   = "phone"
        static let UserName    = "name"
        static let UserID      = "userID"
        static let AboutUser   = "aboutUser"
        static let msgOffline   = "msgOffline"
        static let AppLanguage   = "AppLanguage"
        static let Identity    = "identity"
        static let token       = "token"
        static let myProfile_URL = "profileURL"
        static let allContacts = "allContacts"
        static let userDetails = "userDetails"
        static let feeds_post = "post"
        static let ImageUrl    = "\(GeneralConstants.BASE_IP)salniazi-app/uploads/"
        static let BASE_URL    = "\(GeneralConstants.BASE_IP)salniazi-app/api.php?action="
        static let MATCH_BASE_URL    = "\(GeneralConstants.BASE_IP)salniazi-app/napi.php?action=" 
        static let RetrieveCalls = "\(GeneralConstants.BASE_IP)salniazi-app/twiliovoice/calllog.php?FilterKey=to&FilterValue="
        static let get_voice_call_logs = "\(GeneralConstants.BASE_IP)salniazi-app/twiliovoice/Calllogbyspecific.php?"
        static let arrFeet:[String] = ["Feet","1","2","3","4","5","6","7","8","9","10"]
        static let arrInches:[String] = ["Inch","0","1","2","3","4","5","6","7","8","9","10","11"]
        static let stickerKey   = "TAMSTICKER*"
        static let contactKey   = "Contact attachment"
        static let appLiveLink = "https://itunes.apple.com/us/app/tamaaas-messenger/id1449952054?mt=8"
        static let appLiveLinkAndroid = "https://play.google.com/store/apps/details?id=com.tamaaas"
        static let ReportTags = ["Nudity", "Voilence", "Harassment", "Suicide", "False News", "Spam", "Unauthorized Sale", "Hate Speech", "Racism"]
    }
    
    // MARK: - Global Variables
    struct GlobalVariables {
        
        static var appStoreVersion:String = ""
        static var dictMyContacts : Dictionary<String, String>?
        static var isAppLauched:Bool = false
        static var isNeedToGetStories:Bool = false
        static var galleryClicked = true
        static var arrStories_global:[Story] = []
        static var isNeedToRefreshPostDetails:Bool = false
        static var isNewStoryAdded_moreThanOne:Bool = false
        static var isDialPadLaunched:Bool = false
        static var selectedFeedImg_frame:CGRect?
        static var isNeedToShowInitialConnectingBar:Bool = true
        static var isAppLaunchedForImgCaching:Bool = false
        
        
        //
        static var myStoriesCount  = Int()
        static var ph_no        = String()
        static var dialCode     = String()
        static var shCamera     = true
        static var otpString    = String()
        static var userDetails    : [[String:Any]]?
        static var shPost = false
        
        //profile update
        static var isMyProfileUpdated = false
        static var allChannelsFrnds_phNO:[String] = []
        static var allStoriesFrnds_phNO:[String] = []
        static var allpost_id:[String] = []
        static var updatedPushToken: Data?
        
        //feeds
        static var typeStr : String?
        static var captionStr : String?
        static var selectedImage: UIImage?
        static var videoUrl : URL?
        
        static var isfeedNotiTapped:Bool = false
        static var superView:UIView?
        static var completedStories:Int = 0
      
        //stickers
        static let family = ["family_1","family_2","family_3","family_4","family_5","family_6","family_7","family_8","family_9","family_10","family_11","family_12","family_13","family_14","family_15","family_16","family_17","family_18","family_19","family_20","family_21","family_22","family_23","family_24","family_25","family_26","family_27","family_28","family_29","family_30","family_31","family_32"]
        
        static let food = ["food_1","food_2","food_3","food_4","food_5","food_6","food_7","food_8","food_9","food_10","food_11","food_12","food_13","food_14","food_15","food_16","food_23","food_17","food_18","food_19","food_20","food_21","food_22"]
        
        static let saying = ["famous_1","famous_2","famous_3","famous_5","famous_6","famous_7","famous_8","famous_9","famous_10","saying_1","saying_2","saying_3","saying_4","saying_5","saying_6","saying_54","saying_7","saying_8","saying_9","saying_10","saying_36","saying_37","saying_38","saying_33","saying_34","saying_35","saying_39","saying_40","saying_41","saying_42","saying_43","saying_44","saying_45","saying_46","saying_47","saying_48","saying_49","saying_50","saying_51","saying_52","saying_53","saying_11","saying_12","saying_13","saying_14","saying_15","saying_16","saying_17","saying_18","saying_19","saying_20","saying_21","saying_22","saying_23","saying_24","saying_25","saying_26","saying_27","saying_28","saying_29","saying_30","saying_31","saying_32"]
        
        static let famous = ["famous_11","famous_12","famous_13","famous_14","famous_15","famous_16","famous_17","famous_18","famous_19","famous_20","famous_21","famous_22","famous_23","famous_24","famous_25","famous_26","famous_27","famous_28","famous_29","famous_30","famous_31","famous_32","famous_33","famous_34","famous_35","famous_36","famous_37","famous_38","famous_39","famous_40","famous_41","famous_42","famous_43","famous_44","famous_45","famous_46","famous_47","famous_48","famous_49"]
        
        static let sport = ["sport_12" , "sport_13" ,"sport_14" , "sport_15","sport_16" , "sport_17","sport_1","sport_2","sport_18","sport_3","sport_4","sport_5","sport_6","sport_7","sport_8","sport_9","sport_10" , "sport_11","sport_19","sport_20"]
        
        static let islam = ["islam_13","islam_14","islam_15","islam_16","islam_17","islam_18","islam_19","islam_20","islam_21","islam_22","islam_23" , "islam_24","islam_25","islam_26","islam_27","islam_28" , "islam_29","islam_30","islam_1","islam_4","islam_5","islam_6","islam_7","islam_9","islam_10","islam_11" ,"islam_12"]
        
        static let arrStickers = GlobalVariables.family + GlobalVariables.food + GlobalVariables.saying + GlobalVariables.famous + GlobalVariables.sport + GlobalVariables.islam
      
        static let stickerOptions = [UIImage(named: "english_option"),UIImage(named: "sendSmiley"),UIImage(named: "family_option"),UIImage(named: "food_option"),UIImage(named: "moreMashalla"),UIImage(named: "specialPerson"),UIImage(named: "sport_option"),UIImage(named: "islam_option")]
    }
    
    static func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let date = dt {
            return dateFormatter.string(from: date)
        }
        
        return ""
    }
    
    //MARK:- QuickBlox
    struct ActiveDataSource {
        static var Messages:[QBChatMessage] = []
        static var isActiveRefresh:Bool = false
        static var isActiveSignUp:Bool = false
        static var isNavigatePostDetails:Bool = false
        static var isNavigateNotification:Bool = false
        static var isNeedToRefreshPost:Bool = false
    }
    
    struct ActiveCall {
        static var senderID:String = ""
        static var senderNo:String = ""
        static var receiverNo:String = ""
        static var membersID:String = ""
        static var isGroup:String = ""
        static var isAudio:String = ""
        static var dialogID:String = ""
        static var groupName:String = ""
        static var duration:String = ""
        static var groupPhoto:String = ""
        static var opponentsIDs:[NSNumber] = []
    }
    
    struct ActiveSession {
        static var session: QBRTCSession?
        static var isCallActive:Bool = false
        static var activeCallUUID: UUID?
        static var remoteActiveUsers:[User] = []
        static var videoViews = [UInt: UIView]()
        static var cameraCapture: QBRTCCameraCapture?
        static var isBackgroundViewActive:Bool = false
        static var callTimer: Timer?
        static var timeDuration: TimeInterval = 0.0
        static var activeDialog: QBChatDialog?
        static var isStartedConnectingCall:Bool = false
    }
    
    enum versionError: Error {
        case invalidBundleInfo
        case invalidResponse
    }
    
    func cleanActiveSession() {
        ActiveSession.session = nil
        ActiveSession.isCallActive = false
        ActiveSession.activeCallUUID = nil
        ActiveSession.remoteActiveUsers = []
        ActiveSession.videoViews.removeAll()
        ActiveSession.cameraCapture = nil
        ActiveSession.isBackgroundViewActive = false
        ActiveSession.timeDuration = 0.0
        ActiveSession.activeDialog = nil
        ActiveSession.isStartedConnectingCall = false
    }
    
    func cleanActiveCall(){
        ActiveCall.senderID = ""
        ActiveCall.senderNo = ""
        ActiveCall.receiverNo = ""
        ActiveCall.membersID = ""
        ActiveCall.isGroup = ""
        ActiveCall.isAudio = ""
        ActiveCall.dialogID = ""
        ActiveCall.groupName = ""
        ActiveCall.duration = ""
        ActiveCall.groupPhoto = ""
        ActiveCall.opponentsIDs = []
    }
    
    enum callState: String {
        case hangup = "hangup"
        case missedNoAnswer = "missedNoAnswer"
    }
    
    struct openPrivateChat{
        static var isActiveOpenPrivateChat:Bool = false
        static var Dialog: QBChatDialog!
        static var isActiveOpenChatWithActiveCall = false
    }
}

extension String {
    var localized: String {
        if SessionManager.get_localizeLanguage() == "" {
            SessionManager.save_localizeLanguage(lang: "en")
        }
        let path = Bundle.main.path(forResource: SessionManager.get_localizeLanguage(), ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

struct UsersAlertConstant {
    static let checkInternet = NSLocalizedString("Please check your Internet connection", comment: "")
    static let okAction = NSLocalizedString("Ok", comment: "")
    static let shouldLogin = NSLocalizedString("You should login to use chat API. Session hasn’t been created. Please try to relogin the chat.", comment: "")
    static let logout = NSLocalizedString("Logout...", comment: "")
}


struct YYImgCacheOptions {
    static let withRefreshCache:(YYWebImageOptions) = [ .progressiveBlur, .setImageWithFadeAnimation , .refreshImageCache]
    static let withOutRefreshCache:(YYWebImageOptions) = [.progressiveBlur , .setImageWithFadeAnimation]
}

struct currentYYImgotions {
    static var currentYYImg:(YYWebImageOptions) = YYImgCacheOptions.withOutRefreshCache
}


struct MsgToReplyKeys {
    static let msg_id = "msg_id"
    static let senderNumber = "senderNumber"
    static let replyId = "replyId"
    static let replyNumber = "replyNumber"
    static let msgType = "msgType"
    static let TEXT = "text"
    static let replyMsg = "replyMsgDetail"
    static let msg = "actual_msg"
    static let senderID = "senderID"
    static let IMAGE = "image"
    static let VIDEO = "video"
    static let CONTACT = "contact"
    static let STICKER = "sticker"
    static let AUDIO = "audio"
    static let MEDIA = "media"
    static let thumbnail = "thumbnail"
    static let stickerName = "stickerName"
    static let isMsgFromStories = "isMsgFromStories"
    static let storyType = "storyType"
}

struct ReplyContainer {
    static let maxReplyContainerHeight:CGFloat = 69.0
    static let minReplyContainerHeight:CGFloat = 60.0
}
