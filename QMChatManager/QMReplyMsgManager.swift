//
//  QMReplyMsgManager.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 5/16/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit

class QMReplyMsgManager: NSObject {
    
    static let shared = QMReplyMsgManager()
    
    func isContainReply(message: QBChatMessage) ->Bool{
        if let customParam = message.customParameters {
            if customParam.object(forKey: MsgToReplyKeys.replyMsg) != nil {
                return true
            }
        }
        return false
    }
    
    
    func isContainCaption(message: QBChatMessage) ->Bool{
        if let text = message.text {
            if text == "Image attachment" || text == "Video attachment"{
               return false
            }
        }
        return true
    }
    
    
    func getReplyImageThumbnailStr(message: QBChatMessage) ->String{
        if let customParam = message.customParameters {
            if let StrMsgDetail = customParam.object(forKey: MsgToReplyKeys.replyMsg) as? String {
                let dic = QuickBloxManager().convertJsonStringToDictionary(text: StrMsgDetail)
                if let dicReply = dic {
                    if let thumbnail = dicReply[MsgToReplyKeys.thumbnail] as? String {
                        return thumbnail
                    }
                }
            }
        }
        return ""
    }
    func getReplyMsgId(message: QBChatMessage) ->String{
        if let customParam = message.customParameters {
            if let StrMsgDetail = customParam.object(forKey: MsgToReplyKeys.replyMsg) as? String {
                let dic = QuickBloxManager().convertJsonStringToDictionary(text: StrMsgDetail)
                if let dicReply = dic {
                    if let msgId = dicReply[MsgToReplyKeys.msg_id] as? String {
                        return msgId
                    }
                }
            }
        }
        return ""
    }
    func getReplyImageStickerNameStr(message: QBChatMessage) ->String{
        if let customParam = message.customParameters {
            if let StrMsgDetail = customParam.object(forKey: MsgToReplyKeys.replyMsg) as? String {
                let dic = QuickBloxManager().convertJsonStringToDictionary(text: StrMsgDetail)
                if let dicReply = dic {
                    if let stickerName = dicReply[MsgToReplyKeys.stickerName] as? String {
                        return stickerName
                    }
                }
            }
        }
        return ""
    }
    
    func getReplyUserName(message: QBChatMessage) ->String{
        if let customParam = message.customParameters {
            if let StrMsgDetail = customParam.object(forKey: MsgToReplyKeys.replyMsg) as? String {
                let dic = QuickBloxManager().convertJsonStringToDictionary(text: StrMsgDetail)
                if let dicReply = dic {
                    if let senderNo = dicReply[MsgToReplyKeys.senderNumber] as? String {
                        if senderNo == SessionManager.getPhone() {
                            return "You"
                        } else {
                            return SessionManager.getNameFromMyAddressBook(number: senderNo)
                        }
                    }
                }
            }
        }
        return ""
    }
    
    func getReplyMsgToText(message: QBChatMessage) ->String{
        if let customParam = message.customParameters {
            if let StrMsgDetail = customParam.object(forKey: MsgToReplyKeys.replyMsg) as? String {
                let dic = QuickBloxManager().convertJsonStringToDictionary(text: StrMsgDetail)
                if let dicReply = dic {
                    if let msgToReplytext = dicReply[MsgToReplyKeys.msg] as? String {
                        if let type = dicReply[MsgToReplyKeys.msgType] as? String {
                            if type == MsgToReplyKeys.STICKER {
                                return "Sticker"
                            }
                        }
                        return msgToReplytext
                    }
                }
            }
        }
        return ""
    }
    
    func getReplyContainerHeightToSet(message: QBChatMessage , view: UIView) -> CGFloat{
        let attributedStringMsgToReply: NSAttributedString? = self.getAttributedString(forItem: message , msgText: self.getReplyMsgToText(message: message))
        var heightToSet:CGFloat = 0.0
        let sizeOfMsgToReply = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringMsgToReply, withConstraints: CGSize(width: view.frame.size.width - 100.0, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
        
        if self.getReplyMsgTypeFromChatMsg(msg: message) != "" && self.getReplyMsgTypeFromChatMsg(msg: message) == MsgToReplyKeys.TEXT {
            if (sizeOfMsgToReply.height + 30.0) < ReplyContainer.maxReplyContainerHeight {
                heightToSet = (sizeOfMsgToReply.height + 30.0)
            } else {
                heightToSet = ReplyContainer.maxReplyContainerHeight
            }
        } else {
            heightToSet = ReplyContainer.minReplyContainerHeight
        }
        return heightToSet
    }
    
    func getCaptionHeightToSet(message: QBChatMessage , view: UIView) -> CGFloat{
        
        if let caption = message.text {
            if caption == "" {
                return 0.0
            }
        }
        
        let attributedStringMsgToReply: NSAttributedString? = self.getAttributedString(forItem: message , msgText: message.text!)
        var heightToSet:CGFloat = 0.0
        let sizeOfMsgToReply = TTTAttributedLabel.sizeThatFitsAttributedString(attributedStringMsgToReply, withConstraints: CGSize(width: view.frame.size.width - 100.0, height: CGFloat.greatestFiniteMagnitude), limitedToNumberOfLines: 0)
        return sizeOfMsgToReply.height
    }
    
    func getAttributedString(forItem messageItem: QBChatMessage! , msgText: String) -> NSAttributedString? {
        var attributes = Dictionary<NSAttributedStringKey, AnyObject>()
        attributes[NSAttributedStringKey.font] = UIFont(name: "CircularStd-Book", size: 17)
        var attributedString = NSAttributedString(string: msgText, attributes: attributes)
        return attributedString
    }
    func getReplyMsgTypeFromChatMsg(msg: QBChatMessage) ->String{
        if let customParam = msg.customParameters {
            if let StrMsgDetail = customParam.object(forKey: MsgToReplyKeys.replyMsg) as? String {
                let dic = QuickBloxManager().convertJsonStringToDictionary(text: StrMsgDetail)
                if let dicReply = dic {
                    if let type = dicReply[MsgToReplyKeys.msgType] as? String {
                        return type
                    }
                }
            }
        }
        return ""
    }
}
