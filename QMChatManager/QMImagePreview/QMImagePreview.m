//
//  QMImagePreview.m
//  Q-municate
//
//  Created by Vitaliy Gorbachov on 8/30/16.
//  Copyright © 2016 Quickblox. All rights reserved.
//

#import "QMImagePreview.h"
#import "QMImageLoader.h"
#import <UIKit/UIKit.h>
#import <NYTPhotoViewer/NYTPhotoViewer.h>
#import "QMPhoto.h"
#import "QBChatMessage+QBDateDivider.h"

@implementation QMImagePreview

+ (void)previewImageWithURL:(NSURL *)url inViewController:(UIViewController *)ivc {
    
    if (url == nil) {
        return;  
    }
    
    QMPhoto *photo = [[QMPhoto alloc] init];
    
    NYTPhotoViewerSinglePhotoDataSource *photoDataSource = [NYTPhotoViewerSinglePhotoDataSource dataSourceWithPhoto:photo];
    
    NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithDataSource: photoDataSource];
    
    if ([ivc conformsToProtocol:@protocol(NYTPhotosViewControllerDelegate)]) {
           photosViewController.delegate = (UIViewController<NYTPhotosViewControllerDelegate> *)ivc;
     }
   [ivc presentViewController:photosViewController animated:YES completion:nil];
    
    QMImageLoader *loader = [QMImageLoader instance];
    
    [loader downloadImageWithURL:url
                       transform:nil
                         options:SDWebImageRefreshCached
                        progress:nil
                       completed:^(UIImage * _Nullable image,
                                   UIImage * _Nullable __unused transfomedImage,
                                   NSError * _Nullable  error,
                                   SDImageCacheType __unused cacheType,
                                   BOOL __unused finished,
                                   NSURL * _Nonnull __unused imageURL) {
                           
                           if (!error && image) {
                               photo.image = [loader originalImageWithURL:imageURL];
                               NYTPhotoViewerSinglePhotoDataSource *updatedPhotoDataSource =
                               [NYTPhotoViewerSinglePhotoDataSource dataSourceWithPhoto:photo];
                               
                               photosViewController.dataSource = updatedPhotoDataSource;
                               [photosViewController reloadPhotosAnimated:YES];
                           }
                       }];
}

+ (void)previewImageWithURLArray:(NSArray*)urlArray inViewController:(UIViewController *)ivc{
     [self newTestPhotos:urlArray inViewController:ivc];
}

+ (void)newTestPhotos: (NSArray *)arrCount inViewController:(UIViewController *)ivc{
 
     NSMutableArray *photosToDownload = [NSMutableArray array];
     
     for (int i = 0; i < arrCount.count; i++) {
          QMPhoto *photo = [[QMPhoto alloc] init];
          QBChatMessage *ChatMessage = arrCount[i];
          
          NSURL *fileURL = [[NSURL alloc] initWithString:ChatMessage.attachments.firstObject.url];
          
          QMImageLoader *loader = [QMImageLoader instance];
          [loader downloadImageWithURL:fileURL
                             transform:nil
                               options:SDWebImageRefreshCached
                              progress:nil
                             completed:^(UIImage * _Nullable image,
                                         UIImage * _Nullable __unused transfomedImage,
                                         NSError * _Nullable  error,
                                         SDImageCacheType __unused cacheType,
                                         BOOL __unused finished,
                                         NSURL * _Nonnull __unused imageURL) {
                                  
                                  if (!error && image) {
                                       photo.image = [loader originalImageWithURL:imageURL];
                                       [photosToDownload addObject:photo];
                                  }
                             }];
     }
   
     CGFloat updateImageDelay = 5.0;
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(updateImageDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          NSLog(@"dispatch_after function %d", photosToDownload.count);
            [self transferto:photosToDownload inViewController:ivc];
     });
}

+ (void)transferto: (NSArray *)arrCount inViewController:(UIViewController *)ivc{
     
     NYTPhotoViewerArrayDataSource *updatedPhotoDataSource =
     [NYTPhotoViewerArrayDataSource dataSourceWithPhotos:arrCount];
     NYTPhotosViewController *photosViewController = [[NYTPhotosViewController alloc] initWithDataSource:updatedPhotoDataSource initialPhoto:nil delegate:(UIViewController<NYTPhotosViewControllerDelegate> *)ivc];
     [photosViewController reloadPhotosAnimated:YES];
     [ivc presentViewController:photosViewController animated:YES completion:nil];
     [self updateImagesOnPhotosViewController:photosViewController afterDelayWithDataSource:updatedPhotoDataSource];
}

+(void)updateImagesOnPhotosViewController:(NYTPhotosViewController *)photosViewController afterDelayWithDataSource:(NYTPhotoViewerArrayDataSource *)dataSource {
     
     CGFloat updateImageDelay = 5.0;
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(updateImageDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          for (QMPhoto *photo in dataSource.photos) {
               if (!photo.image && !photo.imageData) {
                    photo.image = [UIImage imageNamed:@"NYTimesBuilding"];
                    [photosViewController updatePhoto:photo];
               }
          }
     });
}
@end
