//
//  ReachabilityManager.swift
//  Tamaas
//
//  Created by Krescent Global on 05/09/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator

class ReachabilityManager: NSObject {
    
    static let shared = ReachabilityManager()
    let reachability = Reachability()!
    var isCallConnect:Bool = false
    
    
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: .reachabilityChanged,
                                               object: reachability)
        do{
            try reachability.startNotifier()
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }
    
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: .reachabilityChanged,
                                                  object: reachability)
    }
    
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! Reachability
        switch reachability.connection {
        case .none:
            debugPrint("Network became unreachable")
            self.handle_internetNOTActive()
        case .wifi:
            debugPrint("Network reachable through WiFi")
            self.handle_internetActive()
        case .cellular:
            debugPrint("Network reachable through Cellular Data")
            self.handle_internetActive()
        }
    }
    
    func handle_internetActive() {
        NotificationCenter.default.post(name: constantVC.NSNotification_name.NSNotification_InternetConnected , object: nil)
    }
    
    func handle_internetNOTActive() {
         NotificationCenter.default.post(name: constantVC.NSNotification_name.NSNotification_InternetDisconnected , object: nil)
    }

    
}
