//
//  CommonFunctions.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 6/4/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import Foundation

class CommonFunctions: NSObject {
    
    static let shared = CommonFunctions()
    
    func addBorder(imageView: UIImageView , colour: UIColor){
        
        let maskPath = UIBezierPath(roundedRect: imageView.bounds, byRoundingCorners: ([.topLeft, .topRight, .bottomLeft, .bottomRight]), cornerRadii: imageView.bounds.size)
        
        let borderShape = CAShapeLayer()
        borderShape.frame = imageView.bounds
        borderShape.path = maskPath.cgPath
        borderShape.strokeColor = colour.cgColor
        borderShape.fillColor = nil
        borderShape.lineWidth = 3
        imageView.layer.addSublayer(borderShape)
    }
    func string(withTimeDuration timeDuration: TimeInterval) -> String {
        let minutes = Int(timeDuration / 60)
        let seconds = Int(timeDuration) % 60
        let timeStr = String(format:"%02i:%02i", minutes, seconds)
        return timeStr
    }
}
