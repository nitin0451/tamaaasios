//
//  CustomDesignView.swift
//  App411
//
//  Created by Mandeep Singh on 25/08/18.
//  Copyright © 2018 osvinuser. All rights reserved.
//

import Foundation

@IBDesignable class CustomDesignView : UIView {
        
    @IBInspectable override public var cornerRadius: CGFloat {
        get { return layer.cornerRadius }
        set { layer.cornerRadius = newValue }
    }
    
    @IBInspectable override public var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable override var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
    @IBInspectable override var shadowColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.shadowColor = uiColor.cgColor
        }
        get {
            guard let color = layer.shadowColor else { return nil }
            return UIColor(cgColor: color)
        }
        
    }
    
    @IBInspectable public var shadowRadius: CGFloat {
        get { return layer.shadowRadius }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get { return layer.shadowOffset }
        set {
            layer.shadowOffset = newValue
        }
    }
   
    @IBInspectable override var shadowOpacity: CGFloat {
        get { return CGFloat(layer.shadowOpacity) }
        set {
            layer.shadowOpacity = Float(newValue)
        }
    }
    
}
