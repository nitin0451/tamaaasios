//
//  CoreData_BussinessLogic.swift
//  Tamaas
//
//  Created by Krescent Global on 13/08/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import Foundation
import CoreData

class CoreData_BussinessLogic:NSObject {
    
    //Variables
   static let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
   
    //MARK:- STORYBOARD
    class func isStoryBoardAvailable(userID: String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "StoryBoard_DB")
        let predicate = NSPredicate(format: "user_id == %@", userID)
        request.predicate = predicate
        request.fetchLimit = 1
        
        do{
            let count = try context.count(for: request)
            if(count == 0){
                return false
            }
            else{
                return true
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    
    class func saveOrUpdateStoryBoard(story: Story) {
        if CoreData_BussinessLogic.isStoryBoardAvailable(userID: story.id)  {
            //update
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "StoryBoard_DB")
            fetchRequest.predicate = NSPredicate(format: "user_id = %@", story.id)
            
            do {
                let results = try context.fetch(fetchRequest) as? [NSManagedObject]
                if results?.count != 0 { // Atleast one was returned
                    results![0].setValue(story.id, forKey: "user_id")
                    results![0].setValue(story.photoURL + ".jpeg", forKey: "photoURL")
                    results![0].setValue(story.photoURL, forKey: "phoneNumber")
                    results![0].setValue(story.name, forKey: "name")
                    results![0].setValue(story.isLastRead, forKey: "isLastStoryRead")
                }
            } catch {
                print("Fetch Failed: \(error)")
            }
            do {
                try context.save()
            }
            catch {
                print("Saving Core Data Failed: \(error)")
            }
            
        } else {
            //save
            let storyBoard = StoryBoard_DB(context: context)
            storyBoard.user_id = story.id
            storyBoard.photoURL = story.photoURL + ".jpeg"
            storyBoard.phoneNumber = story.photoURL
            storyBoard.name = story.name
            storyBoard.isLastStoryRead = story.isLastRead
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    class func UpdateUserNameStoryBoard(name: String , userID:String) {
        if CoreData_BussinessLogic.isStoryBoardAvailable(userID: userID)  {
            //update
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "StoryBoard_DB")
            fetchRequest.predicate = NSPredicate(format: "user_id = %@", userID)
            
            do {
                let results = try context.fetch(fetchRequest) as? [NSManagedObject]
                if results?.count != 0 { // Atleast one was returned
                    results![0].setValue(name, forKey: "name")
                }
            } catch {
                print("Fetch Failed: \(error)")
            }
            do {
                try context.save()
            }
            catch {
                print("Saving Core Data Failed: \(error)")
            }
        }
    }
    
    class func deleteAllStoryBoard() {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "StoryBoard_DB")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try CoreData_BussinessLogic.context.execute(deleteRequest)
            try CoreData_BussinessLogic.context.save()
        } catch {
            print ("There is an error in deleting storyboard records")
        }
    }
    
    class func getStoryBoard() -> [StoryBoard_DB]{
        var storyBoard:[StoryBoard_DB] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "StoryBoard_DB")
        
        do {
            storyBoard = try context.fetch(request) as! [StoryBoard_DB]
        } catch {
            print("Fetching Failed")
        }
        
        return storyBoard
    }
    
    
    //MARK:- STORY
    
    class func isStoryAvailable(userID: String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Story_DB")
        let predicate = NSPredicate(format: "id == %@", userID)
        request.predicate = predicate
        request.fetchLimit = 1
        
        do{
            let count = try context.count(for: request)
            if(count == 0){
                return false
            }
            else{
                return true
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    
    class func saveOrUpdateStory(storyFile: StoryFile , userID: String) {
        if CoreData_BussinessLogic.isStoryAvailable(userID: storyFile._id!)  {
            //update
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Story_DB")
            fetchRequest.predicate = NSPredicate(format: "user_id = %@", userID)
            
            do {
                let results = try context.fetch(fetchRequest) as? [NSManagedObject]
                if results?.count != 0 { // Atleast one was returned
                    results![0].setValue(storyFile._id, forKey: "id")
                    results![0].setValue(storyFile.type, forKey: "type")
                    results![0].setValue("\(String(describing: storyFile.duration))", forKey: "time_duration")
                    results![0].setValue(storyFile.created, forKey: "created")
                    results![0].setValue(storyFile.file, forKey: "file")
                    results![0].setValue((storyFile.isRead != nil), forKey: "isRead")
                    results![0].setValue(storyFile.views, forKey: "views")
                    results![0].setValue(userID, forKey: "user_id")
                    results![0].setValue(storyFile.createdDate! as NSDate, forKey: "createdDate")
                }
            } catch {
                print("Fetch Failed: \(error)")
            }
            do {
                try context.save()
            }
            catch {
                print("Saving Core Data Failed: \(error)")
            }
            
        } else {
            //save
            let story = Story_DB(context: context)
            story.id = storyFile._id
            story.type = storyFile.type
            story.time_duration = "\(String(describing: storyFile.duration))"
            story.created = storyFile.created
            story.file = storyFile.file
            story.isRead = (storyFile.isRead != nil)
            story.views = storyFile.views
            story.user_id = userID
            if let date = storyFile.createdDate! as? NSDate{
                story.createdDate = date as Date
            }
            
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    class func deleteAllStories() {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Story_DB")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try CoreData_BussinessLogic.context.execute(deleteRequest)
            try CoreData_BussinessLogic.context.save()
        } catch {
            print ("There is an error in deleting storyboard records")
        }
    }
    
    class func getStories(userID: String) -> [Story_DB]{
        
        var stories:[Story_DB] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Story_DB")
        let predicate = NSPredicate(format: "user_id == %@", userID)
        request.predicate = predicate
        
        do{
            stories = try context.fetch(request) as! [Story_DB]
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return stories
    }
    
    class func getStories() -> [Story_DB]{
        var stories:[Story_DB] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Story_DB")
        
        do {
            stories = try context.fetch(request) as! [Story_DB]
        } catch {
            print("Fetching Failed")
        }
        
        return stories
    }
    
    class func deleteStoriesByUserID(userID: String){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Story_DB")
        let predicate = NSPredicate(format: "user_id == %@", userID)
        request.predicate = predicate
        do{
            let result = try context.fetch(request)
            
            if result.count > 0{
                for object in result {
                    context.delete(object as! NSManagedObject)
                }
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    class func deleteParticularStoryByID(storyID: String){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Story_DB")
        let predicate = NSPredicate(format: "id == %@", storyID)
        request.predicate = predicate
        do{
            let result = try context.fetch(request)
            
            if result.count > 0{
                for object in result {
                    context.delete(object as! NSManagedObject)
                }
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    //MARK:- NEWS FEEDS
    
    class func isFeedAvailable(feedID: String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Feed_DB")
        let predicate = NSPredicate(format: "id == %@", feedID)
        request.predicate = predicate
        request.fetchLimit = 1
        
        do{
            let count = try context.count(for: request)
            if(count == 0){
                return false
            }
            else{
                return true
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    
    class func saveOrUpdateFeed(feed: Post) {
        if CoreData_BussinessLogic.isFeedAvailable(feedID: feed.id!)  {
            //update
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Feed_DB")
            fetchRequest.predicate = NSPredicate(format: "id = %@", feed.id!)
            
            do {
                let results = try context.fetch(fetchRequest) as? [NSManagedObject]
                if results?.count != 0 { // Atleast one was returned
                    results![0].setValue(feed.id, forKey: "id")
                    results![0].setValue(feed.isLiked!, forKey: "isLiked")
                    results![0].setValue(feed.caption, forKey: "caption")
                    results![0].setValue(feed.created, forKey: "created")
                    results![0].setValue(feed.phoneNumber, forKey: "phoneNumber")
                    results![0].setValue("\(feed.height)", forKey: "height")
                    results![0].setValue("\(feed.width)", forKey: "width")
                    results![0].setValue(feed.photoURL, forKey: "file")
                    results![0].setValue(feed.thumbnail, forKey: "thumbnail")
                    results![0].setValue("\(String(describing: feed.likeCount!))", forKey: "likesCount")
                    results![0].setValue("\(String(describing: feed.commentCount!))", forKey: "commentsCount")
                    results![0].setValue(feed.user_id, forKey: "user_id")
                    results![0].setValue(feed.type, forKey: "type")
                    results![0].setValue(feed.name, forKey: "name")
                    results![0].setValue(feed.createdDate, forKey: "createdDate")
                }
            } catch {
                print("Fetch Failed: \(error)")
            }
            do {
                try context.save()
            }
            catch {
                print("Saving Core Data Failed: \(error)")
            }
            
        } else {
            //save
            let feedToAdd = Feed_DB(context: context)
            
            //init
            feedToAdd.id = ""
            feedToAdd.isLiked = false
            feedToAdd.caption = ""
            feedToAdd.created = ""
            feedToAdd.phoneNumber = ""
            feedToAdd.height = ""
            feedToAdd.width = ""
            feedToAdd.file = ""
            feedToAdd.thumbnail = ""
            feedToAdd.likesCount = ""
            feedToAdd.commentsCount = ""
            feedToAdd.user_id = ""
            feedToAdd.type = ""
            feedToAdd.name = ""
            feedToAdd.captionHeight = ""
            feedToAdd.superViewWidth = ""
            
            if let date = NSDate() as? NSDate{
                feedToAdd.createdDate = date as Date
            }
            
            feedToAdd.id = feed.id
            feedToAdd.isLiked = feed.isLiked!
            feedToAdd.caption = feed.caption
            feedToAdd.created = feed.created
            feedToAdd.phoneNumber = feed.phoneNumber
            feedToAdd.height = "\(feed.height)"
            feedToAdd.width = "\(feed.width)"
            feedToAdd.file = feed.photoURL
            feedToAdd.thumbnail = feed.thumbnail
            feedToAdd.likesCount = "\(String(describing: feed.likeCount!))"
            feedToAdd.commentsCount = "\(String(describing: feed.commentCount!))"
            feedToAdd.user_id = feed.user_id
            feedToAdd.type = feed.type
            feedToAdd.name = feed.name
            feedToAdd.captionHeight = "\(feed.captionHeight)"
            feedToAdd.superViewWidth = "\(feed.superViewWidth)"
            if let date = feed.createdDate! as? NSDate{
                feedToAdd.createdDate = date as Date
            }
            
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    
    
    class func getFeeds() -> [Feed_DB]{
        var feeds:[Feed_DB] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Feed_DB")
        
        do {
            feeds = try context.fetch(request) as! [Feed_DB]
        } catch {
            print("Fetching Failed")
        }
        
        return feeds
    }
    
    class func deleteAllFeeds() {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Feed_DB")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try CoreData_BussinessLogic.context.execute(deleteRequest)
            try CoreData_BussinessLogic.context.save()
        } catch {
            print ("There is an error in deleting storyboard records")
        }
    }
    
    
    //MARK:- NEWS FEEDS COMMENTS
    
    class func isCommentAvailable(commentID: String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FeedComment_DB")
        let predicate = NSPredicate(format: "id == %@", commentID)
        request.predicate = predicate
        request.fetchLimit = 1
        
        do{
            let count = try context.count(for: request)
            if(count == 0){
                return false
            }
            else{
                return true
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    
    class func saveOrUpdateComment(comment: Comment , postID:String) {
        if CoreData_BussinessLogic.isCommentAvailable(commentID: comment.commentID!)  {
            //update
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FeedComment_DB")
            fetchRequest.predicate = NSPredicate(format: "id = %@", comment.commentID!)
            
            do {
                let results = try context.fetch(fetchRequest) as? [NSManagedObject]
                if results?.count != 0 { // Atleast one was returned
                    results![0].setValue(comment.commentID, forKey: "id")
                    results![0].setValue(comment.commentText, forKey: "comment")
                    results![0].setValue(comment.time, forKey: "created")
                    results![0].setValue(postID, forKey: "postID")
                    results![0].setValue(comment.uid, forKey: "user_id")
                    results![0].setValue(comment.name, forKey: "user_name")
                    results![0].setValue(comment.image, forKey: "user_phoneNumber")
                }
            } catch {
                print("Fetch Failed: \(error)")
            }
            do {
                try context.save()
            }
            catch {
                print("Saving Core Data Failed: \(error)")
            }
            
        } else {
            //save
            let commentToAdd = FeedComment_DB(context: context)
            
            //init
            commentToAdd.id = ""
            commentToAdd.comment = ""
            commentToAdd.created = ""
            commentToAdd.postID = ""
            commentToAdd.user_id = ""
            commentToAdd.user_name = ""
            commentToAdd.user_phoneNumber = ""
            
            commentToAdd.id = comment.commentID
            commentToAdd.comment = comment.commentText
            commentToAdd.created = comment.time
            commentToAdd.postID = postID
            commentToAdd.user_id = comment.uid
            commentToAdd.user_name = comment.name
            commentToAdd.user_phoneNumber = comment.image
            
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    class func getFeedComment_byPostID(postID: String) -> [FeedComment_DB]{
        var feedComments:[FeedComment_DB] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FeedComment_DB")
        let predicate = NSPredicate(format: "postID == %@", postID)
        request.predicate = predicate
       
        do{
            feedComments = try context.fetch(request) as! [FeedComment_DB]
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return feedComments
    }
    
    class func deleteAllCommentsOfFeed(postID: String){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FeedComment_DB")
        let predicate = NSPredicate(format: "postID == %@", postID)
        request.predicate = predicate
        do{
            let result = try context.fetch(request)
            
            if result.count > 0{
                for object in result {
                    context.delete(object as! NSManagedObject)
                }
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    class func deleteAllComments(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "FeedComment_DB")
        
        do{
            let result = try context.fetch(request)
            
            if result.count > 0{
                for object in result {
                    context.delete(object as! NSManagedObject)
                }
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
   
    
    
    //MARK:- NOTIFICATION
    
    class func saveNotifications(notification: notification ) {
        
        //save
        let notiToAdd = Notification_DB(context: context)
            
        //init
        notiToAdd.post_id = ""
        notiToAdd.body = ""
        notiToAdd.comment = ""
        notiToAdd.fromName = ""
        notiToAdd.fromNumber = ""
        notiToAdd.postImageUrl = ""
        notiToAdd.generatedmessage = ""
        notiToAdd.height_title = ""
        notiToAdd.height_subTitle = ""
        notiToAdd.flag = ""
        notiToAdd.subTitle = ""
        notiToAdd.createdAt = ""
        
        notiToAdd.post_id = notification.post_id
        notiToAdd.body = notification.body
        notiToAdd.comment = notification.comment
        notiToAdd.fromName = notification.fromName
        notiToAdd.fromNumber = notification.FromNumber
        notiToAdd.postImageUrl = notification.postImageUrl
        notiToAdd.generatedmessage = notification.generatedmessage
        notiToAdd.height_title = "\(notification.height_title!)"
        notiToAdd.height_subTitle = "\(notification.height_subTitle!)"
        notiToAdd.flag = notification.flag
        notiToAdd.subTitle = notification.subTitle
         notiToAdd.createdAt = notification.createdAt
        
       (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    class func getAllNotificatiobs() -> [Notification_DB]{
        var notifications:[Notification_DB] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Notification_DB")
        
        do {
            notifications = try context.fetch(request) as! [Notification_DB]
        } catch {
            print("Fetching Failed")
        }
        
        return notifications
    }
    
    class func deleteAllNotifications(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Notification_DB")
       
        do{
            let result = try context.fetch(request)
            
            if result.count > 0{
                for object in result {
                    context.delete(object as! NSManagedObject)
                }
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    
    
    //MARK:- CALL LOGS
    
    class func isCallAvailable(callID: String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Call_DB")
        let predicate = NSPredicate(format: "id == %@", callID)
        request.predicate = predicate
        request.fetchLimit = 1
        
        do{
            let count = try context.count(for: request)
            if(count == 0){
                return false
            }
            else{
                return true
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    
    class func saveCallLog(call: Call) {
        if !(CoreData_BussinessLogic.isCallAvailable(callID: call.id!)) {
            
            //save
            let callToAdd = Call_DB(context: context)
            
            //init
            callToAdd.id = ""
            callToAdd.log_from = ""
            callToAdd.log_to = ""
            callToAdd.startTime = ""
            callToAdd.status = ""
            callToAdd.type = ""
            callToAdd.userImage = ""
            
            callToAdd.id = call.id
            callToAdd.log_from = call.senderNo
            callToAdd.log_to = call.receiverNo
            callToAdd.startTime = call.createdAt
            callToAdd.status = call.callState
            callToAdd.type = call.callType
             callToAdd.userImage = call.senderNo
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        }
    }
    
    
    class func getAllCalls() -> [Call_DB]{
        var calls:[Call_DB] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Call_DB")
        
        do {
            calls = try context.fetch(request) as! [Call_DB]
        } catch {
            print("Fetching Failed")
        }
        
        return calls
    }
    
    class func deleteAllCalls(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Call_DB")
        
        do{
            let result = try context.fetch(request)
            
            if result.count > 0{
                for object in result {
                    context.delete(object as! NSManagedObject)
                }
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    
    
    
    //MARK:- STORY VIEWS
    
    class func isViewAvailable(id: String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Views_DB")
        let predicate = NSPredicate(format: "id == %@", id)
        request.predicate = predicate
        request.fetchLimit = 1
        
        do{
            let count = try context.count(for: request)
            if(count == 0){
                return false
            }
            else{
                return true
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
 
}
