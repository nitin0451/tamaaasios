//
//  ProfileUpdateManager.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 5/24/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import UIKit
import YYWebImage

class ProfileUpdateManager: NSObject {
    
    static let shared = ProfileUpdateManager()
    func webserviceToupdateProfileStatus(memberNos: String){
        
        var dictParam = Dictionary<String, String>()
        dictParam["userNumber"] = SessionManager.getPhone()
        dictParam["memberNo"] = memberNos
        
        let webservice  = WebserviceSigleton ()
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_UPDATE_PROFILE_STATUS, params: dictParam as Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
            }
        })
    }
    
    func webserviceToGetProfileStatus(){
        var dictParam = Dictionary<String, String>()
        dictParam["memberNo"] = SessionManager.getPhone()
        let webservice  = WebserviceSigleton ()
        
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GET_UPDATED_PROFILE_STATUS, params: dictParam as Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                if let resultMessage = response["msg"] as? String {
                    if resultMessage == "success"{
                        
                        //users Updated Profile
                        if let arrUserNumbers = response["usernumber"] as? NSArray {
                            for item in arrUserNumbers {
                                YYWebImageManager.shared().cache?.memoryCache.removeObject(forKey: constantVC.GeneralConstants.ImageUrl + "\(item)" + ".jpeg")
                                YYWebImageManager.shared().cache?.diskCache.removeObject(forKey: constantVC.GeneralConstants.ImageUrl + "\(item)" + ".jpeg")
                            }
                        }
                        //post NSNotification refresh list
                        NotificationCenter.default.post(name: constantVC.NSNotification_name.NSNotification_loadDialogs , object: nil)
                    }
                    else {
                        print("No profile updates!")
                    }
                }
            }
        })
    }
}
