//
//  StickerContainer_VC.swift
//  Tamaas
//
//  Created by Krescent Global on 16/08/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator
import Branch

protocol SelectedStickerDelegate {
    func sendSelected_sticker(string: String)
}

class StickerContainer_VC: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource {

    //MARK:- Outlets
    @IBOutlet weak var collectionVw_sticker: UICollectionView!
    @IBOutlet weak var vw_blurUnlock: UIView!
    @IBOutlet weak var vw_unlockContent: UIView!
    
    
    //MARK:- Variables
    var arrStickers:[String] = []
    var delegate: SelectedStickerDelegate?
    var containerSize:CGSize?
    var index:Int = 0
    
    
    //MARK:- UIView life-cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.vw_blurUnlock.isHidden = true
        self.vw_unlockContent.isHidden = true
        self.collectionVw_sticker.delegate = self
        self.collectionVw_sticker.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       /* if (index == 4 && !(SessionManager.get_isUnlockStickers_sports()))  ||  (index == 3 && !(SessionManager.get_isUnlockStickers_famous())){
            self.vw_blurUnlock.isHidden = false
            self.vw_unlockContent.isHidden = false
        } else {
            self.vw_blurUnlock.isHidden = true
            self.vw_unlockContent.isHidden = true
        }*/
    }
    
    
    //MARK:- UIButton Actions
    @IBAction func btn_unlockStickers_action(_ sender: UIButton) {
        if index == 4 {
            self.createLink(identifier: "sports")
        } else {
            self.createLink(identifier: "famous")
        }
    }
    
    
    //MARK:- UICOllectionView delegates and data-source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrStickers.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.arrStickers[indexPath.row] == "family_30" {
           return CGSize(width: self.containerSize!.width/4-4 + 20 , height: self.containerSize!.width/4-4 + 20);
        }
        
        return CGSize(width: self.containerSize!.width/4-4, height: self.containerSize!.width/4-4);
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath as IndexPath) as! StickerCell
        cell.imageView.contentMode = .redraw
        cell.imageView.image = UIImage(named: self.arrStickers[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if Connectivity.isConnectedToInternet() {
        var message = String()
        message =  self.arrStickers[indexPath.row]
        delegate?.sendSelected_sticker(string: message)
        } else {
            //OFFLINE
            FTIndicator.showToastMessage("No Internet Connection!")
        }
    }
    
    
    //MARK:- Stickers Deep linking Methods
    
    func createLink(identifier:String){
        let buo = BranchUniversalObject.init(canonicalIdentifier: identifier)
        buo.title = "Tamaaas Stickers"
        buo.contentDescription = ""
      //  buo.imageUrl = "https://lorempixel.com/400/400"
        buo.publiclyIndex = true
        buo.locallyIndex = true
        buo.contentMetadata.customMetadata["isShared"] = "true"
        
        let shareLinkProperties = BranchLinkProperties()
        shareLinkProperties.feature = "Share"
        shareLinkProperties.channel = "Share"
        shareLinkProperties.campaign = "DealScreen"
        shareLinkProperties.controlParams = [
            "$deeplink_path": constantVC.GeneralConstants.appLiveLinkAndroid ,
            "$desktop_url": constantVC.GeneralConstants.appLiveLink,
            "$ios_url": constantVC.GeneralConstants.appLiveLink,
            "$android_url": constantVC.GeneralConstants.appLiveLinkAndroid,
            "$match_duration": "2000",
            "$custom_data": "yes",
            "$look_at": "this",
            "$nav_to": "over here",
            "$linkSendFrom": SessionManager.getPhone(),
            "$stickerSection": identifier,
            "$random": UUID.init().uuidString
        ]
        
//       let branchShareLink = BranchShareLink.init(
//            universalObject: buo,
//            linkProperties:  shareLinkProperties
//        )
//        branchShareLink.title = "Share your test link!"
//        branchShareLink.shareText = "Shared from Branch's Test-Swift at"
//        branchShareLink.presentActivityViewController(
//            from: self,
//            anchor: nil
//        )
       
        buo.getShortUrl(with: shareLinkProperties) { (url, error) in
        }
        
        let message = "Check out this New Secure Chat App with free video chat, calls, stickers, money and more... Download Tamaaas NOW!"
        buo.showShareSheet(with: shareLinkProperties, andShareText: message, from: self) { (activityType, completed) in
        }
    }
    

    
}
