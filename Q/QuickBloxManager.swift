//
//  QuickBloxManager.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 11/28/18.
//  Copyright © 2018 Krescent Global. All rights reserved.
//

import UIKit
import FTIndicator
import UserNotifications


class QuickBloxManager: NSObject {
    
    //Constants
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
     var callNotiState: String = ""
    //MARK:- Registration Methods
    func logIn(user: QBUUser , completionHandler: @escaping (Bool,String) -> Void) {
        
        QBRequest.logIn(withUserLogin: user.login! , password: user.password! , successBlock: { response, userQ in
            if response.isSuccess {
                
                //save quick blox id
                if let id = userQ.id as? NSNumber {
                    SessionManager.save_QuickBloxID(id: id)
                }
                completionHandler(true , "login Successful")
            } else {
                completionHandler(false , "login UnSuccessful")
            }
            
        }, errorBlock: { response in
            
            self.signUp(user: user , completionHandler: { (success , msg) in
                if success {
                    completionHandler(true , msg)
                } else {
                    completionHandler(false , msg)
                }
          })
        })
    }
    
    
    func signUp(user: QBUUser , completionHandler: @escaping (Bool,String) -> Void) {
        
        QBRequest.signUp(user, successBlock: { response, userQ in
            
            if response.isSuccess {
                
                //save quick blox id
                if let id = userQ.id as? NSNumber {
                    SessionManager.save_QuickBloxID(id: id)
                }
               completionHandler(true , "SignUp Successful")
            } else {
               completionHandler(false , "SignUp UnSuccessful")
            }
        }, errorBlock: { response in
            completionHandler(false , response.error.debugDescription)
        })
    }
    
    
    func deleteUser(completionHandler: @escaping (Bool) -> Void) {
        
        QBRequest.deleteCurrentUser(successBlock: { (response) in
            completionHandler(true)
        }) { (error) in
            completionHandler(false)
        }
    }
    
    
    //MARK:- Dialog Related Methods
    func loginAndGetDialogs() {
        
        self.loadBlockUsersList()
        
        if SessionManager.getPhone() != "" {
            let user = QBUUser()
            user.login = SessionManager.getPhone()
            user.password = SessionManager.getPhone()
            
            ServicesManager.instance().logIn(with: user, completion: {
                [weak self] (success,  errorMessage) -> Void in
                
                guard success else {
                    //FTIndicator.showError(withMessage: errorMessage)
                    return
                }
                
                //Core login
                Core.instance.currentUser = user
                Core.instance.loginWithCurrentUser()
                
                
                //save quick blox id
                guard let currentUser:QBUUser = ServicesManager.instance().currentUser  else {
                    return
                }
                
                if let id = currentUser.id as? NSNumber {
                    SessionManager.save_QuickBloxID(id: id)
                }
                
                let updateParameters = QBUpdateUserParameters()
                updateParameters.phone = SessionManager.getPhone()
                updateParameters.externalUserID = UInt(SessionManager.getUserId()) ?? 0
                QuickBloxManager().updateUserInfo(param: updateParameters)
                //self?.getUserInfo(userID: SessionManager.getUserId())
            
                if (QBChat.instance.isConnected) {
                    self?.getDialogs()
                }
            })
        }
    }
    
    func getUserInfo(userID: String , completionHandler: @escaping (QBUUser) -> Void){
        ServicesManager.instance().usersService.getUserWithID( UInt(userID) ?? 0 , forceLoad: true).continueOnSuccessWith(block: { (task) -> Any? in
            if let user = task.result as? QBUUser {
                completionHandler(user)
            }
            return nil
        })
    }
    
    // MARK: Remote notifications
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
           // center.delegate = self
            let openAction = UNNotificationAction(identifier: "OpenNotification", title: NSLocalizedString("Abrir", comment: ""), options: UNNotificationActionOptions.foreground)
            let deafultCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [openAction], intentIdentifiers: [], options: [])
            center.setNotificationCategories(Set([deafultCategory]))
            center.requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(grant, error)  in
                if error == nil {
                    if grant {
                        DispatchQueue.main.async(execute: {
                            UIApplication.shared.registerForRemoteNotifications()
                        })
                    } else {
                        SessionManager.printLOG(data: "User didn't grant permission")
                    }
                } else {
                    SessionManager.printLOG(data: "ERROR ---- \(error.debugDescription)")
                }
            })
        }
        else if #available(iOS 8.0, *) {
            let settings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
        else {
            // Register for push in iOS 7
            UIApplication.shared.registerForRemoteNotifications(matching: [UIRemoteNotificationType.badge, UIRemoteNotificationType.sound, UIRemoteNotificationType.alert])
        }
       
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    // MARK: - DataSource Action
    func getDialogs() {
        
        if let lastActivityDate = ServicesManager.instance().lastActivityDate {
            
            ServicesManager.instance().chatService.fetchDialogsUpdated(from: lastActivityDate as Date, andPageLimit: kDialogsPageLimit, iterationBlock: { (response, dialogObjects, dialogsUsersIDs, stop) -> Void in
                
            }, completionBlock: { (response) -> Void in
                
                if (response.isSuccess) {
                    
                    ServicesManager.instance().lastActivityDate = NSDate()
                }
            })
        }
        else {
            
            //  SVProgressHUD.show(withStatus: "SA_STR_LOADING_DIALOGS".localized, maskType: SVProgressHUDMaskType.clear)
            
            ServicesManager.instance().chatService.allDialogs(withPageLimit: kDialogsPageLimit, extendedRequest: nil, iterationBlock: { (response: QBResponse?, dialogObjects: [QBChatDialog]?, dialogsUsersIDS: Set<NSNumber>?, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                
            }, completion: { (response: QBResponse?) -> Void in
                
                guard response != nil && response!.isSuccess else {
                    //SVProgressHUD.showError(withStatus: "SA_STR_FAILED_LOAD_DIALOGS".localized)
                    return
                }
                
                // SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
                ServicesManager.instance().lastActivityDate = NSDate()
            })
        }
    }
 
    //MARK:- Chat Creation Methods
    func createPrivateChat(name: String ,data: [String:Any]? = nil,id: UInt ,photo:String , completionHandler: @escaping (Bool,QBChatDialog?) -> Void){
        let chatDialog = QBChatDialog(dialogID: nil , type: QBChatDialogType.private)
        chatDialog.name = name
        chatDialog.photo = photo
        chatDialog.occupantIDs = [NSNumber(value: id)]
        chatDialog.data = data
        QBRequest.createDialog(chatDialog, successBlock: { response, createdDialog in
            if response.isSuccess {
               completionHandler(true,createdDialog)
            } else {
               completionHandler(false,nil)
            }
        }, errorBlock: { response in
            completionHandler(false,nil)
        })
    }
    
    
    //MARK:- Address Book Related Methods
    func retriveAddressBook() {
        QBRequest.addressBook(withUdid: SessionManager.get_UUID() , successBlock: { (arrUsers) in
        }) { (response) in
        }
    }
    
    func retriveRegisteredUsersFromAddressBook(completionHandler: @escaping ([QBUUser]?) -> Void) {
        let user = QBUUser()
        user.login = SessionManager.getPhone()
        user.password = SessionManager.getPhone()
        
        ServicesManager.instance().logIn(with: user) { (success , msg ) in
            if success {
                
                //retrive address book
                QBRequest.registeredUsersFromAddressBook(withUdid: SessionManager.get_UUID() , isCompact: false , successBlock: { (users) in
                    //save users to cache
                    ServicesManager.instance().usersService.updateUsers(users)
                    completionHandler(users)
                }) { (response) in
                    if response.isSuccess {
                    } else {
                        completionHandler([])
                    }
                }
            } else {
                completionHandler([])
            }
        }
    }
    
    func updateAddressBook(addressBookQuick: NSMutableOrderedSet , completionHandler: @escaping (Bool) -> Void){
        
        let user = QBUUser()
        user.login = SessionManager.getPhone()
        user.password = SessionManager.getPhone()
        
        QuickBloxManager().logIn(user: user) { (success , msg) in
            if success {
                //update address book
                QBRequest.uploadAddressBook(withUdid: SessionManager.getAndSave_UUID() , addressBook: addressBookQuick , force: true , successBlock: { (addressBookUpdates ) in
                    
                    self.retriveRegisteredUsersFromAddressBook(completionHandler: { (users) in
                        
                    })
                    completionHandler(true)
                }, errorBlock: { (response) in
                    completionHandler(false)
                })
            } else {
               // FTIndicator.showError(withMessage: msg)
                completionHandler(false)
            }
        }
    }
    
    
    func AddNewContactToAddressBook(addressBookQuick: NSMutableOrderedSet , completionHandler: @escaping (Bool) -> Void){
        
        let user = QBUUser()
        user.login = SessionManager.getPhone()
        user.password = SessionManager.getPhone()
        
        QuickBloxManager().logIn(user: user) { (success , msg) in
            if success {
                //update address book
                QBRequest.uploadAddressBook(withUdid: SessionManager.getAndSave_UUID() , addressBook: addressBookQuick , force: false , successBlock: { (addressBookUpdates ) in
                    
                    self.retriveRegisteredUsersFromAddressBook(completionHandler: { (users) in
                        
                    })
                    completionHandler(true)
                }, errorBlock: { (response) in
                    completionHandler(false)
                })
            } else {
                //FTIndicator.showError(withMessage: msg)
                completionHandler(false)
            }
        }
    }
    
    
    //MARK:- User Related Methods
    func updateProfilePhoto(data: Data){
        
        let user = QBUUser()
        user.login = SessionManager.getPhone()
        user.password = SessionManager.getPhone()
        self.logIn(user: user) { (success , msg ) in
            if success {
                QBRequest.tUploadFile(data , fileName: SessionManager.getPhone() , contentType: "image/png" , isPublic: false , successBlock: { (response , blob ) in
                    if response.isSuccess {
                        let updateParameters = QBUpdateUserParameters()
                        updateParameters.website = blob.privateUrl()
                        self.updateUserInfo(param: updateParameters)
                    }
                }, statusBlock: { (request , status) in
                    
                }, errorBlock: { (response) in
                    
                })
            }
        }
    }
    
    func updateUserInfo(param: QBUpdateUserParameters){
        
        let user = QBUUser()
        user.login = SessionManager.getPhone()
        user.password = SessionManager.getPhone()
        self.logIn(user: user) { (success , msg) in
            if success {
                QBRequest.updateCurrentUser(param, successBlock: { response, user in
                    // User updated successfully
                }, errorBlock: { response in
                    // Handle error
                })
            }
        }
    }
   
   
    //MARK:- Call Related Methods
    func callsAllowed() -> Bool {
        if !(Connectivity.isConnectedToInternet()) {
            return false
        }
        return true
    }
    
    func getOpponentsID(dialog: QBChatDialog?) -> [NSNumber]?{
        if let occupantIDS = dialog?.occupantIDs {
            let IDS = occupantIDS.filter { $0 != SessionManager.get_QuickBloxID() }
            return IDS
        }
        return nil
    }
    
    func getOpponentID(dialog: QBChatDialog?) -> NSNumber?{
        if let occupantIDS = dialog?.occupantIDs {
            let IDS = occupantIDS.filter { $0 != SessionManager.get_QuickBloxID() }
            return IDS[0]
        }
        return nil
    }
    
    func getNoFromMessage(message:String , replaceText: String) -> String{
        let no = message.replacingOccurrences(of: replaceText , with: "")
        return no
    }
    
    
    //notifications
    
    func parseDuration(_ timeString:String) -> TimeInterval {
        guard !timeString.isEmpty else {
            return 0
        }
        
        var interval:Double = 0
        
        let parts = timeString.components(separatedBy: ":")
        for (index, part) in parts.reversed().enumerated() {
            interval += (Double(part) ?? 0) * pow(Double(60), Double(index))
        }
        
        return interval
    }
    
    func _callNotificationMessage(for session: QBRTCSession?, state: QMCallNotificationState) -> QBChatMessage? {
        
        let senderID = ServicesManager.instance().currentUser.id
        
        let message = QBChatMessage()
        message.text = "Call notification"
        message.senderID = senderID
        message.markable = true
        message.dateSent = Date()
        message.callNotificationType = session?.conferenceType == QBRTCConferenceType.audio ? QMCallNotificationType.audio : QMCallNotificationType.video;
        message.callNotificationState = state
        
        let initiatorID = Int(session?.initiatorID ?? 0)
        let opponentID = Int(session?.opponentsIDs.first ?? 0)
        let calleeID: Int = initiatorID == senderID ? opponentID : initiatorID
        
        message.callerUserID = UInt(initiatorID)
        message.calleeUserIDs = NSIndexSet(index: calleeID) as IndexSet!
        
        message.recipientID = UInt(calleeID)
        return message
    }
        
    func sendCallNotificationMessage(with state: QMCallNotificationState, duration: TimeInterval , opponentNo: String) {
        
        let message: QBChatMessage? = _callNotificationMessage(for: constantVC.ActiveSession.session , state: state)
        
        if duration > 0 {
            
            message?.callDuration = duration
        }
        
        //custom parameters
        var callNotiType:String = ""
       
        if message!.callNotificationType == QMCallNotificationType.audio {
            callNotiType = "audio"
        } else {
            callNotiType = "video"
        }
        
        if message!.callNotificationState == QMCallNotificationState.hangUp {
            callNotiState = "hangup"
        } else {
            callNotiState = "missedNoAnswer"
        }
        
        var dictCustomParam:NSMutableDictionary = ["callNotificationType" : callNotiType , "callDuration": duration , "callNotificationState": callNotiState]
        message?.customParameters = dictCustomParam
        
        
       if (constantVC.ActiveCall.isAudio == "false") {
            if constantVC.ActiveCall.isGroup == "true" {
                _sendNotificationMessage_Group(message)
            }
            else {
                _sendNotificationMessage(message , opponentNo: opponentNo)
            }
        } else {
            _sendNotificationMessage(message , opponentNo: opponentNo)
        }
    }
    
    
    func _sendNotificationMessage_Group(_ message: QBChatMessage?) {
        
        let chatDialog: QBChatDialog? = ServicesManager.instance().chatService.dialogsMemoryStorage.chatDialog(withID: constantVC.ActiveCall.dialogID)
            
        if chatDialog != nil {
            
            message?.dialogID = chatDialog?.id
            ServicesManager.instance().chatService.send(message!, to: chatDialog!, saveToHistory: true, saveToStorage: true)
        } else {
            ServicesManager.instance().chatService.loadDialog(withID: constantVC.ActiveCall.dialogID).continueWith(block: { (task) -> Any? in
                if task.isCompleted {
                    message?.dialogID = task.result?.id
                    ServicesManager.instance().chatService.send(message!, to: task.result!, saveToHistory: true, saveToStorage: true)
                }
                return nil
            })
        }
    }
    
    func _sendNotificationMessage(_ message: QBChatMessage? , opponentNo: String) {
        
        let chatDialog: QBChatDialog? = ServicesManager.instance().chatService.dialogsMemoryStorage.privateChatDialog(withOpponentID: (message?.recipientID)!)
        
        if chatDialog != nil {
            
            message?.dialogID = chatDialog?.id
            ServicesManager.instance().chatService.send(message!, to: chatDialog!, saveToHistory: true, saveToStorage: true)
            
            //send push notification
             if self.callNotiState == "missedNoAnswer"{
                self.sendPushNotificationForCall(msg: "Call Notification", dialog: chatDialog! , opponentNo: opponentNo)
            }
        } else {
            ServicesManager.instance().chatService.createPrivateChatDialog(withOpponentID: (message?.recipientID)!, "", isDoctor: false, fees: "") { (response, chatDialog) in
                if response.isSuccess {
                                   
                   if let dialog_ = chatDialog {
                       QuickBloxManager().sendSystemMsg_updateDialog_toOccupants(dialog: dialog_)
                   }
                   
                   message?.dialogID = chatDialog?.id
                   ServicesManager.instance().chatService.send(message!, to: chatDialog!, saveToHistory: true, saveToStorage: true)
                   //send push notification
                   if self.callNotiState == "missedNoAnswer"{
                        self.sendPushNotificationForCall(msg: "Call Notification", dialog: chatDialog!, opponentNo: opponentNo)
                   }
                  
               }
               else {
                   print(response.error.debugDescription)
               }
            }
        }
    }

    
    func sendPushNotificationForCall(msg: String , dialog: QBChatDialog , opponentNo: String){
        
        guard let dialogID = dialog.id else {
            return
        }
        
        if dialog.type == QBChatDialogType.group {
            if let opponentIDs = QuickBloxManager().getOpponentsID(dialog: dialog) {
                var arrStrIDS = [String]()
                
                for id in opponentIDs {
                    arrStrIDS.append("\(id)")
                }
                
                TamFirebaseManager.shared.getGroupMutedUsersId(collection: firebaseCollection.MuteGroup.rawValue , doc: dialogID) { (success , ids) in
                    if success {
                        if ids != "" {
                            let arrMuteIds = ids.components(separatedBy: ",")
                            
                            //get ids not muted
                            let newArrayID  = arrStrIDS.filter { (string) -> Bool in
                                return !arrMuteIds.contains(string)
                            }
                            
                            let strIDS = newArrayID.joined(separator: ",")
                            
                            //send push notification
                            QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: "" , dialogID: dialogID)
                            
                        } else {
                            let strIDS = arrStrIDS.joined(separator: ",")
                            
                            //send push notification
                            QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: "" , dialogID: dialogID)
                        }
                    }
                    else {
                        //no muted
                        let strIDS = arrStrIDS.joined(separator: ",")
                       
                        //send push notification
                        QuickBloxManager().sendPushNotification_chat(msg: msg , users: strIDS , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: "" , dialogID: dialogID)
                    }
                }
            }
        } else {
            
            let document = opponentNo + "-" + SessionManager.getPhone()
            TamFirebaseManager.shared.getDocument(collection: firebaseCollection.Mute.rawValue , document: document) { (success) in
                if !(success) {
                    //Not blocked - send custom notification
                    if let opponentID = QuickBloxManager().getOpponentID(dialog: dialog) {
                        
                        QuickBloxManager().sendPushNotification_chat(msg: msg , users: "\(opponentID)" , title: "New Message!", fromNo: SessionManager.getPhone() , toNo: opponentNo , dialogID: dialogID)
                    }
                }
            }
        }
    }
    
    
    
    
    func sendPushNotification(msg:String , users: String , identity:String,title:String,fromNo:String , toNo:String, flag:String , postId:String , postImgUrl:String , comment:String){
        
        let message = msg
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = users
        event.type = QBMEventType.oneShot
        
        // custom params
        var payloadDict = ["message": message ,"identity": identity, "title": title,"FromNumber": fromNo , "ToNumber": toNo ,"flag": flag , "post_id": postId , "postImageUrl": postImgUrl , "comment": comment , "ios_badge": "1", "ios_sound": "default" , "ios_mutable_content": "1"]
        
        var error: Error? = nil
        let sendData: Data? = try? JSONSerialization.data(withJSONObject: payloadDict , options: .prettyPrinted)
        var jsonString: String? = nil
        if let aData = sendData {
            jsonString = String(data: aData, encoding: .utf8)
        }
        
        event.message = jsonString
        
        QBRequest.createEvent(event, successBlock: { response, events in
            
        }, errorBlock: { response in
            
        })
    }
    
    
    func sendPushNotification_chat(msg:String , users: String ,title:String,fromNo:String , toNo:String , dialogID: String){
        
        let message = msg
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = users
        event.type = QBMEventType.oneShot
        
        // custom params
        var payloadDict = ["message": message , "title": message,"FromNumber": fromNo , "ToNumber": toNo , "dialog_id": dialogID , "ios_badge": "1", "ios_sound": "default" , "ios_mutable_content": "1"]
        
        var error: Error? = nil
        let sendData: Data? = try? JSONSerialization.data(withJSONObject: payloadDict , options: .prettyPrinted)
        var jsonString: String? = nil
        if let aData = sendData {
            jsonString = String(data: aData, encoding: .utf8)
        }
        
        event.message = jsonString
        
        QBRequest.createEvent(event, successBlock: { response, events in
            
        }, errorBlock: { response in
            
        })
    }
    
    
    func sendSilentPushNotification_updateProfile(msg:String , users: String ,fromNo:String){
        
        let message = msg
        let event = QBMEvent()
        event.notificationType = QBMNotificationType.push
        event.usersIDs = users
        event.type = QBMEventType.oneShot
        
        // custom params
        var payloadDict = ["FromNumber": fromNo , "ToNumber": users , "ios_content_available": "1" , "flag": "profile"]
        
        //["message": message , "title": message,"FromNumber": fromNo , "ToNumber": users , "ios_badge": "0", "ios_sound": "default" , "ios_mutable_content": "1" , "flag": "profile"]
        
        var error: Error? = nil
        let sendData: Data? = try? JSONSerialization.data(withJSONObject: payloadDict , options: .prettyPrinted)
        var jsonString: String? = nil
        if let aData = sendData {
            jsonString = String(data: aData, encoding: .utf8)
        }
        
        event.message = jsonString
        
        QBRequest.createEvent(event, successBlock: { response, events in
            
        }, errorBlock: { response in
            
        })
    }
    
    
    //MARK:- QB system Message
    
    func sendSystemMessageQB(dialog: QBChatDialog?){
        
        if let occupantIDS = QuickBloxManager().getOpponentsID(dialog: dialog) {
            for id in occupantIDS {
                
                var message: QBChatMessage = QBChatMessage()
                var params = NSMutableDictionary()
                params["notification_type"] = "2"
                params["_id"] = dialog?.id
                params["room_jid"] = dialog?.roomJID
                params["occupants_ids"] = dialog?.occupantIDs
                params["name"] = dialog?.name
                params["type"] = dialog?.type.rawValue
                params["photo"] = dialog?.photo
                
                message.dialogID = dialog?.id
                message.customParameters = params
                message.recipientID = UInt(truncating: id)
                
                QBChat.instance.sendSystemMessage(message) { (error) in
                    if error != nil {
                        print(error.debugDescription)
                    }
                }
            }
        }
    }
  
    //MARK:- Privacy Manager
    func retrievePrivacyList(){
        QBChat.instance.retrievePrivacyList(withName: "public")
    }
    
    //MARK:- BLOCK USER
    func blockAUser(userQuickBloxID:String , userServerID: String , completionHandler:@escaping (Bool) -> ()){
        
        var userID:String = ""
        if let id = SessionManager.get_QuickBloxID() {
            userID = "\(id)"
        }
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_users_id":SessionManager.getUserId() , "other_users_id":userServerID, "loginQuickBloxID": userID , "otherQuickBloxID": userQuickBloxID , "status": "1"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_BLOCKUSER, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    QuickBloxManager().addBlockedUserToSession(userQuickBloxID: userQuickBloxID)
                    completionHandler(true)
                    QuickBloxManager().blockUserQuickBlox(userQuickBloxID: userQuickBloxID)
                }
                else{
                    completionHandler(false)
                }
            }
            else{
                completionHandler(false)
            }
        })
    }
    
    
    func addBlockedUserToSession(userQuickBloxID: String){
        if let arrIDs = SessionManager.get_chatBlockedUsersID() {
            var arr = arrIDs
            arr.append(userQuickBloxID)
            SessionManager.save_chatBlockedUsersID(IDS: arr)
        } else {
            var arrIds:[String] = []
            arrIds.append(userQuickBloxID)
            SessionManager.save_chatBlockedUsersID(IDS: arrIds)
        }
    }
    
    
    func removeBlockedUserToSession(userQuickBloxID: String) {
        if let arrIDs = SessionManager.get_chatBlockedUsersID() {
            let arr = arrIDs.filter{$0 != userQuickBloxID}
            SessionManager.save_chatBlockedUsersID(IDS: arr)
        }
    }
    
    func UnblockUser(userQuickBloxID:String , userServerID: String , completionHandler:@escaping (Bool) -> ()) {
        
        var userID:String = ""
        if let id = SessionManager.get_QuickBloxID() {
            userID = "\(id)"
        }
        
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_users_id":SessionManager.getUserId() , "other_users_id":userServerID, "loginQuickBloxID": userID , "otherQuickBloxID": userQuickBloxID ,"status": "0"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_BLOCKUSER, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
                
                if result != nil{
                    let response = result! as Dictionary
                    let resultMessage = response["msg"] as! String
                    if resultMessage == "success"{
                        QuickBloxManager().removeBlockedUserToSession(userQuickBloxID: userQuickBloxID)
                        completionHandler(true)
                        QuickBloxManager().UnblockUserQuickBlox(userQuickBloxID: userQuickBloxID)
                    }
                    else{
                        completionHandler(false)
                    }
                }
                else{
                    completionHandler(false)
                }
            })
    }
    
    func loadBlockUsersList() {
        
        let webservice  = WebserviceSigleton ()
        let dictParam : NSDictionary = ["login_users_id": SessionManager.getUserId(), "detail_type":"full"]
        webservice.POSTServiceWithParameters(urlString: constantVC.WebserviceName.URL_GETBLOCKED, params: dictParam as! Dictionary<String, AnyObject>, callback:{ (result,error) -> Void in
            if result != nil{
                let response = result! as Dictionary
                let resultMessage = response["msg"] as! String
                if resultMessage == "success"{
                    
                    var arrIDS:[String] = []
                    
                    if let userDetails = response["user_details"] as? [[String:Any]] {
                        for item in userDetails {
                            
                            if let id = item["quickbloxID"] {
                                arrIDS.append("\(id)")
                            }
                        }
                    }
                    
                    //save blocked users to defaults
                    if arrIDS.count > 0 {
                        SessionManager.save_chatBlockedUsersID(IDS: arrIDS)
                    } else {
                        SessionManager.save_chatBlockedUsersID(IDS: [])
                    }
                }
                else{
                    SessionManager.save_chatBlockedUsersID(IDS: [])
                }
            }
            else{
                SessionManager.save_chatBlockedUsersID(IDS: [])
            }
        })
    }
    
    
    func blockUserQuickBlox(userQuickBloxID:String) {
       
        let item: QBPrivacyItem = QBPrivacyItem(privacyType: .userID , userID: UInt(userQuickBloxID) ?? 0 , allow: false)
        item.mutualBlock = true
        let list: QBPrivacyList = QBPrivacyList(name: "public", items: [item])
        QBChat.instance.setPrivacyList(list)
        QBChat.instance.setDefaultPrivacyListWithName("public")
        constantVC.ActiveDataSource.isNeedToRefreshPost = true
    }
    
    
    func UnblockUserQuickBlox(userQuickBloxID:String) {
        let item: QBPrivacyItem = QBPrivacyItem(privacyType: .userID , userID: UInt(userQuickBloxID) ?? 0, allow: true)
        item.mutualBlock = false
        let list: QBPrivacyList = QBPrivacyList(name: "public", items: [item])
        QBChat.instance.setPrivacyList(list)
        QBChat.instance.setDefaultPrivacyListWithName("public")
        constantVC.ActiveDataSource.isNeedToRefreshPost = true
    }
    
    //MARK:- Check blocked user
    func isUserBlocked(dialog: QBChatDialog , document:String , completionHandler:@escaping (Bool) -> ()) {
        
        if dialog.type == QBChatDialogType.private {
            TamFirebaseManager.shared.getDocument(collection: firebaseCollection.Block.rawValue , document: document) { (success) in
                if success {
                    completionHandler(true)
                } else {
                    completionHandler(false)
                }
            }
        }  else {
            completionHandler(false)
        }
    }
    
    //MARK:- Serialize Methods
    func convertDictionaryToJsonString(dict: NSMutableDictionary) -> String {
        let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions())
        if let jsonString = NSString(data: jsonData!, encoding: String.Encoding.utf8.rawValue) {
            return "\(jsonString)"
        }
        return ""
    }
    
    func convertJsonStringToDictionary(text: String) -> [String: Any]? {
        if let data = text.replacingOccurrences(of: "\n", with: "").data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    //MARK:- Clear chat - Update dialog
    func clearChat_UpdateDialog(dialog: QBChatDialog , completionHandler:@escaping (Bool) -> ()){
        if let customData = dialog.data {
            var data = customData
            
                if let d = data["number"] as? String {
                    
                    if let customDic = self.convertJsonStringToDictionary(text: d) {
                        
                        var dic = customDic
                        
                        if let ids = customDic["clearChat"] as? String {
                            if ids != "" {
                                dic["clearChat"] = ids + "," + SessionManager.getPhone()
                            }
                            else {
                                dic["clearChat"] = SessionManager.getPhone()
                            }
                        }
                        
                        let dictNSMutable = NSMutableDictionary(dictionary: dic)
                        
                        if let jsonStr = self.convertDictionaryToJsonString(dict: dictNSMutable) as? String {
                            data["number"] = jsonStr
                        }
                        
                    }
                    else {
                        let newDic = NSMutableDictionary()
                        newDic.setValue(d, forKey: "number")
                        newDic.setValue(SessionManager.getPhone(), forKey: "clearChat")
                        
                        if let jsonStr = self.convertDictionaryToJsonString(dict: newDic) as? String {
                            data["number"] = jsonStr
                        }
                    }
                }
                else { //for group
                    let newDic = NSMutableDictionary()
                    newDic.setValue(SessionManager.getPhone(), forKey: "clearChat")
                    
                    if let jsonStr = self.convertDictionaryToJsonString(dict: newDic) as? String {
                        data["number"] = jsonStr
                    }
            }
            
            
            dialog.data = data
        }
        
        QBRequest.update(dialog, successBlock: { (response , dialogNew) in
            
            //update in cache
           self.sendSystemMsg_updateDialog(dialog: dialogNew)
            
            completionHandler(true)
        }) { (error) in
            completionHandler(false)
        }
    }
    
    
    func UnClearChat_updateDialog(dialog: QBChatDialog , completionHandler:@escaping (Bool) -> ()){
        if let customData = dialog.data {
            var data = customData
            
                if let d = data["number"] as? String {
                    
                    if let customDic = self.convertJsonStringToDictionary(text: d) {
                        
                        var dic = customDic
                        
                        if let ids = customDic["clearChat"] as? String {
                            
                            let arrID = ids.components(separatedBy: ",")
                            let arrNew = arrID.filter{$0 != SessionManager.getPhone()}
                            
                            if arrNew.count > 0 {
                                let str = arrNew.joined(separator: ",")
                                dic["clearChat"] = str
                            } else {
                                dic["clearChat"] = ""
                            }
                        }
                        
                        let dictNSMutable = NSMutableDictionary(dictionary: dic)
                        
                        if let jsonStr = self.convertDictionaryToJsonString(dict: dictNSMutable) as? String {
                            data["number"] = jsonStr
                        }
                    }
                }
            
            dialog.data = data
        }
        
        QBRequest.update(dialog, successBlock: { (response , dialogNew) in
            
            //update in cache
            self.sendSystemMsg_updateDialog(dialog: dialogNew)
            
            completionHandler(true)
        }) { (error) in
            completionHandler(false)
        }
    }
    
    
    func sendSystemMsg_updateDialog(dialog: QBChatDialog){
        
        if let id = SessionManager.get_QuickBloxID() {
            if dialog.id != nil{
                
                var message: QBChatMessage = QBChatMessage()
                message.dialogID = dialog.id
                message.customParameters = NSMutableDictionary(dictionary: dialog.data!)
                message.recipientID = UInt(truncating: id)
                message.text = "updateDialog"
                
                QBChat.instance.sendSystemMessage(message) { (error) in
                    if error != nil {
                        print(error.debugDescription)
                    }
                }
            }
        }
    }
    
    
    func sendSystemMsg_updateDialog_toOccupants(dialog: QBChatDialog, customParam: NSMutableDictionary? = nil){
        
        if let id = QuickBloxManager().getOpponentID(dialog: dialog) {
            
            let message: QBChatMessage = QBChatMessage()
            message.dialogID = dialog.id
            //message.dialog?.name = 
            message.dialog?.data = dialog.data
            message.dialog?.name = dialog.name
            message.recipientID = UInt(truncating: id)
            message.text = "createNewDialog"
           // message.customParameters = customParam
            QBChat.instance.sendSystemMessage(message) { (error) in
                if error != nil {
                    print(error.debugDescription)
                }
            }
        }
    }
    
    
    func isClearChat(dialog: QBChatDialog) -> Bool{
        
        if let customData = dialog.data {
            if let d = customData["number"] as? String {
                if let customDic = QuickBloxManager().convertJsonStringToDictionary(text: d) {
                    if let ids = customDic["clearChat"] as? String {
                        
                        let arr = ids.components(separatedBy: ",")
                        if arr.contains(SessionManager.getPhone()) {
                            return true
                        }
                        else {
                            return false
                        }
                    }
                }
            }
        }
        return false
    }
    //MARK:- Permission
    func UnknownUserPopUp_UpdateDialog(status: String , dialog: QBChatDialog , completionHandler:@escaping (Bool) -> ()){
        if let customData = dialog.data {
            var data = customData
            
            if let isAllowed = data["isAllowed"] as? String {
                var statusToUpdate = isAllowed
                statusToUpdate = status
                data["isAllowed"] = statusToUpdate
           }
            
            dialog.data = data
        
            QBRequest.update(dialog, successBlock: { (response , dialogNew) in
            
            //update dialog in memory
            self.sendSystemMsg_updateDialog(dialog: dialogNew)
            
            completionHandler(true)
        }) { (error) in
            completionHandler(false)
        }
    }
}
    
    


}

