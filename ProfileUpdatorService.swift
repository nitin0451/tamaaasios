//
//  ProfileUpdatorService.swift
//  Tamaas
//
//  Created by Kitlabs-M-0002 on 4/3/19.
//  Copyright © 2019 Krescent Global. All rights reserved.
//

import Foundation


class ProfileUpdatorService: NSObject {
    
   static let shared = ProfileUpdatorService()
    
    let defaults = UserDefaults.init(suiteName: "group.com.TamaaasApp.defaults")
    
    func saveUsersToUpdateProfile(phnNos:String?) {
        defaults?.set(phnNos, forKey: "userToUpdateProfile")
    }
    
    
    func getUsersToUpdateProfile() ->String?{
        
        if let strPhns = defaults?.value(forKey: "userToUpdateProfile") as? String {
            return strPhns
        }
        return nil
    }
    
    
    
}
